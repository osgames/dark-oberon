@ECHO OFF

REM ==============================================================
REM  This helps you clean your Dark Oberon folder from unneeded
REM  MS Visual C++ 7.1 intermediate files.
REM ==============================================================

ECHO ----------------------------------------
ECHO MSVC71 - Deleting intermediate files...
ECHO.

IF NOT EXIST ..\..\bin\dark-oberon-debug.ilk GOTO debugilk
  ECHO Deleting dark-oberon-debug.ilk...
  DEL ..\..\bin\dark-oberon-debug.ilk
:debugilk

IF NOT EXIST ..\..\bin\dark-oberon-release.ilk GOTO releaseilk
  ECHO Deleting dark-oberon-release.ilk...
  DEL ..\..\bin\dark-oberon-release.ilk
:releaseilk

IF NOT EXIST dark-oberon.ncb GOTO ncb
  ECHO Deleting dark-oberon.ncb...
  DEL dark-oberon.ncb
:ncb

IF NOT EXIST dark-oberon.plg GOTO plg
  ECHO Deleting dark-oberon.plg...
  DEL dark-oberon.plg
:plg

IF NOT EXIST dark-oberon.bsc GOTO bsc
  ECHO Deleting dark-oberon.bsc...
  DEL dark-oberon.bsc
:bsc

DEL *.user

REM for WindowsNT, XP, 2k:
IF NOT "%OS%" == "Windows_NT" GOTO winnt
  IF NOT EXIST obj GOTO obj
    ECHO Deleting obj\...
    RMDIR /S /Q obj
  :obj

  IF NOT EXIST lib GOTO lib
    ECHO Deleting lib\...
    RMDIR /S /Q lib
  :lib

  GOTO win9x
:winnt

REM for Windows9x:
ECHO Deleting obj\...
DELtree /Y obj

ECHO Deleting lib\...
DELtree /Y lib
:win9x

ECHO.
ECHO MSVC71 - Done
