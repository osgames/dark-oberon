;Dark Oberon iNSTALLER

;--------------------------------
; Compiler

	SetCompressor /SOLID lzma


;--------------------------------
; Includes

	!include "MUI.nsh"
	!include "Sections.nsh"
	
	
;--------------------------------
; Variables

	Var TEMP_VAR
	Var STARTMENU_FOLDER
	
	Var PREV_VERSION
	Var PREV_SECTION_DOCUMENTS
	Var PREV_SECTION_SOURCES


;--------------------------------
; General

	!define VERSION "1.0.2.2"
	!define VERSION_TXT "1.0.2-RC2"
	!define FULL_NAME "Dark Oberon"
	!define REGISTRY_NAME "Software\${FULL_NAME}"
	!define UNREGISTRY_NAME "Software\Microsoft\Windows\CurrentVersion\Uninstall\${FULL_NAME}"
	!define HOMEPAGE "http://dark-oberon.sourceforge.net"

	;Name and file
	Name "Dark Oberon ${VERSION_TXT}"
	OutFile "..\..\dark-oberon-${VERSION_TXT}.exe"
	BrandingText " "

	;Default installation folder
	InstallDir "$PROGRAMFILES\${FULL_NAME}"
	InstallDirRegKey HKCU "${REGISTRY_NAME}" "Install Directory"
	
	;Installer info
	VIProductVersion "${VERSION}"
	VIAddVersionKey "ProductName" "Dark Oberon Installator"
	VIAddVersionKey "CompanyName" "Dark Oberon Group"
	VIAddVersionKey "LegalCopyright" "� 2002-2006 Dark Oberon Group"
	VIAddVersionKey "FileDescription" "Installs Dark Oberon game and source files."
	VIAddVersionKey "FileVersion" "${VERSION}"
	VIAddVersionKey "InternalName" "dark-oberon"
	VIAddVersionKey "OriginalFilename" "dark-oberon-${VERSION_TXT}.exe"


;--------------------------------
; Interface Settings

	!define MUI_ICON "..\..\dark-oberon.ico"
	!define MUI_UNICON "uninstall.ico"

	!define MUI_HEADERIMAGE
	!define MUI_HEADERIMAGE_RIGHT
	!define MUI_HEADERIMAGE_BITMAP "top_banner.bmp"
	
	!define MUI_WELCOMEFINISHPAGE_BITMAP "left_banner.bmp"
	!define MUI_UNWELCOMEFINISHPAGE_BITMAP "left_banner.bmp"
	
	!define MUI_ABORTWARNING
	
	
;--------------------------------
; Language Selection Dialog Settings

	;remember the installer language
	!define MUI_LANGDLL_ALWAYSSHOW
	!define MUI_LANGDLL_REGISTRY_ROOT "HKCU" 
	!define MUI_LANGDLL_REGISTRY_KEY "${REGISTRY_NAME}"
	!define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"


;--------------------------------
; Pages

	;istaller pages
	!insertmacro MUI_PAGE_WELCOME
	!insertmacro MUI_PAGE_LICENSE "..\..\docs\internal\gpl.txt"
	
	!define MUI_PAGE_CUSTOMFUNCTION_PRE onComponentsPre
	!insertmacro MUI_PAGE_COMPONENTS

	!define MUI_PAGE_CUSTOMFUNCTION_PRE onDirectoryPre	
	!insertmacro MUI_PAGE_DIRECTORY

	!define MUI_STARTMENUPAGE_DEFAULTFOLDER "${FULL_NAME}"
	!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU"
	!define MUI_STARTMENUPAGE_REGISTRY_KEY "${REGISTRY_NAME}"
	!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
	!insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER

	!insertmacro MUI_PAGE_INSTFILES

	!define MUI_FINISHPAGE_RUN "$INSTDIR\dark-oberon.exe"
	!define MUI_FINISHPAGE_LINK "${FULL_NAME} Homepage"
	!define MUI_FINISHPAGE_LINK_LOCATION "${HOMEPAGE}"
	!define MUI_FINISHPAGE_NOREBOOTSUPPORT
	!insertmacro MUI_PAGE_FINISH

	;unistaller pages
	!insertmacro MUI_UNPAGE_CONFIRM
	!insertmacro MUI_UNPAGE_INSTFILES


;--------------------------------
; Languages

	!insertmacro MUI_LANGUAGE "English"
	;!insertmacro MUI_LANGUAGE "German"
	!insertmacro MUI_LANGUAGE "Slovak"
	!insertmacro MUI_LANGUAGE "Czech"
	
	
;--------------------------------
; Language strings

	LangString STR_UninstallFirst ${LANG_ENGLISH} "Please uninstall old version of Dark Oberon game first.$\r$\n$\r$\nThis is necessary because we have changed the install system.$\r$\nThank you for understanding."
	;LangString STR_UninstallFirst ${LANG_GERMAN} "Please uninstall old version of Dark Oberon game first.$\r$\n$\r$\nThis is necessary because we have changed the install system.$\r$\nThank you for understanding."
	LangString STR_UninstallFirst ${LANG_SLOVAK} "Pros�m odin�talujte najprv star� verziu hry Dark Oberon.$\r$\n$\r$\nTo je nutn�, preto�e sme zmenili in�tala�n� syst�m.$\r$\n�akujeme za pochopenie."
	LangString STR_UninstallFirst ${LANG_CZECH} "Pros�m odinstalujte nejprve starou verzi hry Dark Oberon.$\r$\n$\r$\nTo je nutn�, proto�e jsme zm�nili instala�n� syst�m.$\r$\nD�kujeme za pochopen�."
	
	LangString STR_UninstallAll ${LANG_ENGLISH} "Do you also want to remove all extra files (including new maps, schemes, races, ...)?"
	;LangString STR_UninstallAll ${LANG_GERMAN} "Do you also want to remove all extra files (including new maps, schemes, races, ...)?"
	LangString STR_UninstallAll ${LANG_SLOVAK} "Prajete si odstr�ni� aj v�etky pridan� s�bory (nov� mapy, sch�my, rasy, ...)?"
	LangString STR_UninstallAll ${LANG_CZECH} "P�ejete si odstranit i v�echny p�idan� soubory (nov� mapy, sch�mata, rasy, ...)?"

	LangString STR_InstTypeTypical ${LANG_ENGLISH} "Typical"
	;LangString STR_InstTypeTypical ${LANG_GERMAN} "Typical"
	LangString STR_InstTypeTypical ${LANG_SLOVAK} "Typick�"
	LangString STR_InstTypeTypical ${LANG_CZECH} "Typick�"
	
	LangString STR_InstTypeFull ${LANG_ENGLISH} "Full"
	;LangString STR_InstTypeFull ${LANG_GERMAN} "Full"
	LangString STR_InstTypeFull ${LANG_SLOVAK} "Pln�"
	LangString STR_InstTypeFull ${LANG_CZECH} "Pln�"
	
	LangString STR_SecGame ${LANG_ENGLISH} "Game files"
	;LangString STR_SecGame ${LANG_GERMAN} "Game files"
	LangString STR_SecGame ${LANG_SLOVAK} "S�bory hry"
	LangString STR_SecGame ${LANG_CZECH} "Soubory hry"
	
	LangString STR_SecSource ${LANG_ENGLISH} "Source codes"
	;LangString STR_SecSource ${LANG_GERMAN} "Source codes"
	LangString STR_SecSource ${LANG_SLOVAK} "Zdrojov� s�bory"
	LangString STR_SecSource ${LANG_CZECH} "Zdrojov� soubory"
	
	LangString STR_SecDocuments ${LANG_ENGLISH} "Documentation"
	;LangString STR_SecDocuments ${LANG_GERMAN} "Documentation"
	LangString STR_SecDocuments ${LANG_SLOVAK} "Dokument�cia"
	LangString STR_SecDocuments ${LANG_CZECH} "Dokumentace"

	LangString DESC_SecGame ${LANG_ENGLISH} "Core game files, maps, schemes, races and quick guide."
	;LangString DESC_SecGame ${LANG_GERMAN} "Core game files, maps, schemes, races and quick guide."
	LangString DESC_SecGame ${LANG_SLOVAK} "Hlavn� s�bory hry, mapy, sch�my, rasy a kr�tky sprievodca."
	LangString DESC_SecGame ${LANG_CZECH} "Hlavn� soubory hry, mapy, sch�mata, rasy a kr�tk� pr�vodce."
	
	LangString DESC_SecSource ${LANG_ENGLISH} "Source and project files for various compilers."
	;LangString DESC_SecSource ${LANG_GERMAN} "Source and project files for various compilers."
	LangString DESC_SecSource ${LANG_SLOVAK} "Zdrojov� a projektov� s�bory pre r�zne preklada�e."
	LangString DESC_SecSource ${LANG_CZECH} "Zdrojov� a projektov� soubory pro r�zn� p�eklada�e."
	
	LangString DESC_SecDocuments ${LANG_ENGLISH} "Detailed documentation in Slovak language."
	;LangString DESC_SecDocuments ${LANG_GERMAN} "Detailed documentation in Slovak language."
	LangString DESC_SecDocuments ${LANG_SLOVAK} "Podrobn� dokument�cia v Slovenskom jazyku."
	LangString DESC_SecDocuments ${LANG_CZECH} "Podrobn� dokumentace ve Slovensk�m jazyce."
	
	
;--------------------------------
; Reserve Files

	;These files should be inserted before other files in the data block
	;Keep these lines before any File command

	!insertmacro MUI_RESERVEFILE_LANGDLL


;--------------------------------
; Callback functions

Function .onInit
	;prevent multiple instances
	System::Call 'kernel32::CreateMutexA(i 0, i 0, t "DarkOberonInstallerMutex") i .r1 ?e'
	Pop $R0
	StrCmp $R0 0 +3
	MessageBox MB_OK|MB_ICONEXCLAMATION "The installer is already running."
	Abort
	
	;get installed version
	ReadRegStr $PREV_VERSION HKCU "${REGISTRY_NAME}" "Version"
	
	;get installed sections
	ReadRegDWORD $PREV_SECTION_SOURCES HKCU "${REGISTRY_NAME}" "SectionSources"
	ReadRegDWORD $PREV_SECTION_DOCUMENTS HKCU "${REGISTRY_NAME}" "SectionDocuments"
	
	;load language dialog
	!insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd


Function un.onInit

	!insertmacro MUI_UNGETLANGUAGE

FunctionEnd


;--------------------------------
; Installer Sections

	InstType $(STR_InstTypeTypical)
	InstType $(STR_InstTypeFull)


Section "!$(STR_SecGame)" SectionGame

	SectionIn 1 2 RO
	
	Call TestOldVersion

	SetOutPath "$INSTDIR"
	File /oname=dark-oberon.exe ..\..\dark-oberon-release.exe
	File ..\..\fmod.dll
	File ..\..\dark-oberon.ico
	File /oname=ChangeLog.txt ..\..\ChangeLog
	File /r /x CVS ..\..\dat
	File /r /x CVS ..\..\maps
	File /r /x CVS ..\..\races
	File /r /x CVS ..\..\schemes
	
	SetOutPath "$INSTDIR\documents\guide"
	File /oname=QuickGuide_EN.txt ..\..\docs\documentation\Quick_Guide_EN.txt
	File /oname=QuickGuide_FR.txt ..\..\docs\documentation\Quick_Guide_FR.txt
	
	SetOutPath "$INSTDIR\documents\license"
	File /oname=GPL.txt ..\..\docs\internal\gpl.txt
	File /oname=License.txt ..\..\Licence

	SetOutPath "$INSTDIR\tools\mapedit"
	File ..\..\tools\mapedit\mapedit.exe
	File ..\..\tools\mapedit\scheme.col
	File ..\..\tools\mapedit\gpl.txt
	
	SetOutPath "$INSTDIR\tools\datedit"
	File ..\..\tools\datedit\datedit.exe
	File ..\..\tools\datedit\gpl.txt
	
	;Create uninstaller
	WriteUninstaller "$INSTDIR\uninstall.exe"
	
	;Create shortcuts
	CreateShortCut "$DESKTOP\${FULL_NAME}.lnk" "$INSTDIR\dark-oberon.exe" "" "$INSTDIR\dark-oberon.ico"
	
	!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
	CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
	CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER\Documentation"
	CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER\Tools"
	
	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Documentation\Quick Guide EN.lnk" "$INSTDIR\documents\guide\QuickGuide_EN.txt"
	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Tools\Data Editor.lnk" "$INSTDIR\tools\datedit\datedit.exe"
	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Tools\Map Editor.lnk" "$INSTDIR\tools\mapedit\mapedit.exe"
	
	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\${FULL_NAME}.lnk" "$INSTDIR\dark-oberon.exe" "" "$INSTDIR\dark-oberon.ico"
	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\uninstall.exe"
	
	!insertmacro MUI_STARTMENU_WRITE_END
	
	;store installation folder and version
	WriteRegStr HKCU "${REGISTRY_NAME}" "Install Directory" $INSTDIR
	WriteRegStr HKCU "${REGISTRY_NAME}" "Version" ${VERSION}
	
	;register uninstaller to Add/Remove Programs
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "DisplayName" "${FULL_NAME}"
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "DisplayIcon" "$INSTDIR\dark-oberon.ico"
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "DisplayVersion" "${VERSION_TXT}"
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "Publisher" "Dark Oberon Group"
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "URLInfoAbout" "${HOMEPAGE}"
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "URLUpdateInfo" "${HOMEPAGE}"
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "HelpLink" "${HOMEPAGE}"
	
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "InstallLocation" "$INSTDIR"
	WriteRegStr HKLM "${UNREGISTRY_NAME}" "UninstallString" "$INSTDIR\uninstall.exe"
	WriteRegDWORD HKLM "${UNREGISTRY_NAME}" "NoModify" 1
	WriteRegDWORD HKLM "${UNREGISTRY_NAME}" "NoRepair" 1
	
	;store installed section
	WriteRegDWORD HKCU "${REGISTRY_NAME}" "SectionGame" 1
	
SectionEnd


Section /o "$(STR_SecSource)" SectionSources

	SectionIn 2
	
	SetOutPath "$INSTDIR"
	File /oname=clean.bat ..\..\clean.bat
	File /oname=Makefile ..\..\Makefile
	File /oname=README ..\..\README

	;add game source, build and library files
	File /r /x CVS /x framework.* /x formats.* /x libtga.* /x openglmodule.* /x glfwmodule.* /x kernel.* /x luamodule.* /x openalmodule.* /x lib /x obj /x *.ncb /x *.suo /x *.plg /x *.opt /x .cvsignore /x tools ..\..\build
	File /r /x CVS /x framework  /x formats /x libtga /x openglmodule /x glfwmodule /x kernel /x luamodule /x openalmodule /x tools /x .cvsignore ..\..\src
	File /r /x CVS /x al /x lua /x ogg /x vorbis /x lua.lib /x lualib.lib /x ogg.lib /x openal32.lib /x vorbisfile.lib /x tools /x .cvsignore ..\..\libs
	
	;add tool source and build files
	SetOutPath "$INSTDIR\tools\datedit"
	File /r /x CVS /x *.~* /x *.dsk /x .cvsignore ..\..\tools\datedit\src
	File ..\..\tools\datedit\_clean.bat
	
	SetOutPath "$INSTDIR\tools\mapedit"
	File /r /x CVS /x *.~* /x *.dsk /x .cvsignore ..\..\tools\mapedit\src
	File ..\..\tools\mapedit\_clean.bat
	
	;!!! not in 1.0.2
	;SetOutPath "$INSTDIR\tools"
	;File /r /x CVS /x lib /x obj /x *.ncb /x *.suo /x *.plg /x *.opt /x *.ilk /x .cvsignore ..\..\tools\test
	
	;store installed section
	WriteRegDWORD HKCU "${REGISTRY_NAME}" "SectionSources" 1
	
SectionEnd


Section /o "$(STR_SecDocuments)" SectionDocuments

	SectionIn 2
	SetOutPath "$INSTDIR\documents"

	;copy files
	File ..\..\docs\documentation\final\Programmer_documentation_SK.pdf
	File ..\..\docs\documentation\final\User_documentation_SK.pdf
	
	;add shortcuts to start menu
	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Documentation\User Guide SK.lnk" "$INSTDIR\documents\User_documentation_SK.pdf"
	CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Documentation\Prog Manual SK.lnk" "$INSTDIR\documents\Programmer_documentation_SK.pdf"

	;store installed section
	WriteRegDWORD HKCU "${REGISTRY_NAME}" "SectionDocuments" 1

SectionEnd


;--------------------------------
; Installer Functions

;test old version (< 1.0.2-RC2)
Function TestOldVersion
	StrCmp $PREV_VERSION "" 0 lbl_NotOldVersion                              ;version is < 1.0.2.2 (1.0.2-RC2)
	IfFileExists $INSTDIR\doberon.exe lbl_OldVersion 0                       ;version 1.0.1
	IfFileExists $INSTDIR\dark-oberon.exe lbl_OldVersion lbl_NotOldVersion   ;version 1.0.2-RC1

	lbl_OldVersion:
	MessageBox MB_OK|MB_ICONINFORMATION $(STR_UninstallFirst)
	Quit

	lbl_NotOldVersion:
FunctionEnd


Function onComponentsPre
	;select installed sections
	IntCmpU $PREV_SECTION_SOURCES 0 lbl_NoSources 0
	!insertmacro SelectSection ${SectionSources}
	!insertmacro SetSectionFlag ${SectionSources} ${SF_RO}
	lbl_NoSources:
	
	IntCmpU $PREV_SECTION_DOCUMENTS 0 lbl_NoDocuments 0
	!insertmacro SelectSection ${SectionDocuments}
	!insertmacro SetSectionFlag ${SectionDocuments} ${SF_RO}
	lbl_NoDocuments:
FunctionEnd


Function onDirectoryPre
	StrCmp $PREV_VERSION "" +2 0
	Abort
FunctionEnd


;--------------------------------
; Descriptions

	;Assign language strings to sections
	!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${SectionGame} $(DESC_SecGame)
	!insertmacro MUI_DESCRIPTION_TEXT ${SectionSources} $(DESC_SecSource)
	!insertmacro MUI_DESCRIPTION_TEXT ${SectionDocuments} $(DESC_SecDocuments)
	!insertmacro MUI_FUNCTION_DESCRIPTION_END


;--------------------------------
; Uninstaller Section

Section "Uninstall"

	MessageBox MB_ICONQUESTION|MB_YESNO $(STR_UninstallAll) IDYES lbl_RemoveAll IDNO lbl_Remove

	lbl_RemoveAll:
		RMDir /r /REBOOTOK "$INSTDIR"
		goto lbl_RemoveDone
	
	lbl_Remove:
		Delete "$INSTDIR\dark-oberon.exe"
		Delete "$INSTDIR\dark-oberon.ico"
		Delete "$INSTDIR\fmod.dll"
		Delete "$INSTDIR\ChangeLog.txt"
		Delete "$INSTDIR\uninstall.exe"
		
		RMDir /r "$INSTDIR\build\msvc6"
		RMDir /r "$INSTDIR\build\msvc71"
		RMDir /r "$INSTDIR\build\nsis"
		RMDir "$INSTDIR\build"

		Delete "$INSTDIR\dat\cursors.dat"
		Delete "$INSTDIR\dat\fonts.dat"
		Delete "$INSTDIR\dat\gui.dat"
		RMDir "$INSTDIR\dat"
		
		RMDir /r "$INSTDIR\documents\license"
		RMDir /r "$INSTDIR\documents\guide"
		Delete "$INSTDIR\documents\Programmer_documentation_SK.pdf"
		Delete "$INSTDIR\documents\User_documentation_SK.pdf"
		RMDir "$INSTDIR\documents"
		
		RMDir /r "$INSTDIR\src"
		RMDir /r "$INSTDIR\libs"
		
		Delete "$INSTDIR\maps\sunnybay.map"
		Delete "$INSTDIR\maps\trial.map"
		RMDir "$INSTDIR\maps"
		
		RMDir /r "$INSTDIR\races\human"
		RMDir /r "$INSTDIR\races\human-blue"
		RMDir /r "$INSTDIR\races\human-yellow"
		RMDir "$INSTDIR\races"
		
		Delete "$INSTDIR\schemes\plastic.*"
		RMDir "$INSTDIR\schemes"
		
		RMDir /r "$INSTDIR\tools\datedit"
		RMDir /r "$INSTDIR\tools\mapedit"
		RMDir "$INSTDIR\tools"
	
	lbl_RemoveDone:
	
	Delete "$DESKTOP\${FULL_NAME}.lnk"
	
	!insertmacro MUI_STARTMENU_GETFOLDER Application $TEMP_VAR

	;Delete "$SMPROGRAMS\$TEMP_VAR\Uninstall.lnk"
	RMDir /r "$SMPROGRAMS\$TEMP_VAR"

	DeleteRegKey HKCU "${REGISTRY_NAME}"
	DeleteRegKey HKLM "${UNREGISTRY_NAME}"

SectionEnd


