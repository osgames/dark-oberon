

-- ============================
--  Dark Oberon - Debug Script
-- ============================

-- actions
function AddUnit(prototype, x, y, segment)
	local player, level, unit
		
	player = kernel.game:GetActivePlayer()
	level = kernel.game:GetActiveLevel()
	
	unit = player:NewUnit(prototype)
	if unit then
		unit:SetMapPosition(x, y, segment)
		level:AddUnitToMap(unit)
	end
end


function OnDebugAction(action)
	if action == "logtree" then
		logtree()
		
	elseif action == "togglewarfog" then
		local map
		map = kernel.game:GetActiveMap()
		map:SetWarfogEnabled(not map:IsWarfogEnabled())
		
	elseif action == "addunit" then
		local x, y, segment
		x, y, segment = kernel.game:GetMouseMapPosition()
		AddUnit("footman", x, y, segment)
		
	elseif action == "addbuilding" then
		local x, y, segment
		x, y, segment = kernel.game:GetMouseMapPosition()
		AddUnit("castle", x, y, segment)
		
	elseif action == "addunit100" then
		local x, y, segment
		x, y, segment = kernel.game:GetMouseMapPosition()
		
		for j = 0, 9, 1 do
			for i = 0, 9, 1 do
				AddUnit("footman", x + i, y + j, segment)
			end
		end
	end
end


function RegisterDebugActions()
	local input_server
	
	input_server = lookup("/sys/servers/input")

	if not input_server then
		logerror("Missing input server")
	else
		input_server:AddActionCallback("OnDebugAction")
		
		input_server:Bind("num.:down", "logtree")
		input_server:Bind("num0:down", "togglewarfog")
		input_server:Bind("num1:down", "addunit")
		input_server:Bind("num2:down", "addbuilding")
		input_server:Bind("num3:down", "addunit100")
	end
end


-- call functions
RegisterDebugActions()
