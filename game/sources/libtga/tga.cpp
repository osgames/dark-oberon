//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file tga.cpp
	@ingroup LibTga_Module

 	@author Marcus Geelnard, Peter Knut
	@date 2002 - 2006

	TGA Loader Library

	Supports the Truevision Targa version 1 fileformat (.TGA).
	Supported pixel formats are:
	- 8-bitgrayscale
	- 8-bitpaletted (24/32-bitcolor),
	- 24-bitt ruecolor and 32-bittruecolor + alpha

	Paletted images are translated into true color or truecolor+alpha pixel formats.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <stdlib.h>
#include <stdio.h>

#include "tga.h"


//=========================================================================
// TGA header and constants
//=========================================================================

/**
	TGA file header information.
*/
typedef struct {
	int idlen;                ///< Length of skipped image identifier. (1 byte)
	int cmaptype;             ///< Specifies, if there is a colormap. (1 byte)
	int imagetype;            ///< Specifies type of the image. (1 byte)
	int cmapfirstidx;         ///< Index of first map entry. (2 bytes)
	int cmaplen;              ///< Number of entries in color map. (2 bytes)
	int cmapentrysize;        ///< Bit-depth of a cmap entry. (1 byte)
	int xorigin;              ///< X-coordinate. (2 bytes)
	int yorigin;              ///< Y-coordinate. (2 bytes)
	int width;                ///< Width of image. (2 bytes)
	int height;               ///< Height of image. (2 bytes)
	int bitsperpixel;         ///< Pixel-depth of image. (1 byte)
	int imageinfo;            ///< Aplhabits + Origin. (1 byte)

	int _alphabits;           ///< Derived from imageinfo.
	int _origin;              ///< Derived from imageinfo.
} TGA_Header;


#define TGA_CMAPTYPE_NONE      0      ///< Value of TGA_Header::cmaptype, when there is no colormap present.
#define TGA_CMAPTYPE_PRESENT   1      ///< Value of TGA_Header::cmaptype, when there is a colormap present.

#define TGA_IMAGETYPE_NONE     0      ///< Value of TGA_Header::imagetype. @todo okomentovat vsetky TGA_IMAGETYPE_*
#define TGA_IMAGETYPE_CMAP     1      ///< Value of TGA_Header::imagetype.
#define TGA_IMAGETYPE_TC       2      ///< Value of TGA_Header::imagetype.
#define TGA_IMAGETYPE_GRAY     3      ///< Value of TGA_Header::imagetype.
#define TGA_IMAGETYPE_CMAP_RLE 9      ///< Value of TGA_Header::imagetype.
#define TGA_IMAGETYPE_TC_RLE   10     ///< Value of TGA_Header::imagetype.
#define TGA_IMAGETYPE_GRAY_RLE 11     ///< Value of TGA_Header::imagetype.

#define TGA_IMAGEINFO_ALPHA_MASK   0x0f    ///< Mask to get TGA_Header::_alphabits from TGA_Header::imageinfo;
#define TGA_IMAGEINFO_ALPHA_SHIFT  0       ///< Shift to get TGA_Header::_alphabits from TGA_Header::imageinfo;
#define TGA_IMAGEINFO_ORIGIN_MASK  0x30    ///< Mask to get TGA_Header::_origin from TGA_Header::imageinfo;
#define TGA_IMAGEINFO_ORIGIN_SHIFT 4       ///< Shift to get TGA_Header::_origin from TGA_Header::imageinfo;

#define _TGA_ORIGIN_BL 0      ///< Bottom left image origin.
#define _TGA_ORIGIN_BR 1      ///< Bottom right image origin.
#define _TGA_ORIGIN_UL 2      ///< Upper left image origin.
#define _TGA_ORIGIN_UR 3      ///< Upper right image origin.


//=========================================================================
// Preparing TGA structure
//=========================================================================

void tgaPrepare(TGA *tga)
{
	tga->bpp = 0;
	tga->data = NULL;
	tga->data_size = 0;
	tga->format = TGA_PIXFMT_UNKNOWN;
	tga->width = 0;
	tga->height = 0;

	tga->cb.seek_func  = (int (*)(void *, long, int))fseek;
	tga->cb.tell_func  = (long (*)(void *))ftell;
	tga->cb.read_func  = (size_t (*)(void *, size_t, size_t, void *))fread;
	tga->cb.write_func = (size_t (*)(const void *, size_t, size_t, void *))fwrite;
}


//=========================================================================
// Reading TGA image
//=========================================================================

/**
	Reads a TGA file header and checks that it is valid.

	@return @c 1 when the TGA header was valid, @c 0 otherwise.
*/
int tgaReadHeader(void *source, TGA *tga, TGA_Header *h)
{
	unsigned char buf[18];
	long pos;

	// read TGA file header from file
	pos = tga->cb.tell_func(source);
	tga->cb.read_func(buf, 18, 1, source);

	// interpret header (endian independent parsing)
	h->idlen         = (int)buf[0];
	h->cmaptype      = (int)buf[1];
	h->imagetype     = (int)buf[2];
	h->cmapfirstidx  = (int)buf[3]  | (((int)buf[4]) << 8);
	h->cmaplen       = (int)buf[5]  | (((int)buf[6]) << 8);
	h->cmapentrysize = (int)buf[7];
	h->xorigin       = (int)buf[8]  | (((int)buf[9]) << 8);
	h->yorigin       = (int)buf[10] | (((int)buf[11]) << 8);
	h->width         = (int)buf[12] | (((int)buf[13]) << 8);
	h->height        = (int)buf[14] | (((int)buf[15]) << 8);
	h->bitsperpixel  = (int)buf[16];
	h->imageinfo     = (int)buf[17];

	// extract alphabits and origin information
	h->_alphabits = (int)(h->imageinfo & TGA_IMAGEINFO_ALPHA_MASK) >> TGA_IMAGEINFO_ALPHA_SHIFT;
	h->_origin    = (int)(h->imageinfo & TGA_IMAGEINFO_ORIGIN_MASK) >> TGA_IMAGEINFO_ORIGIN_SHIFT;

	// validate TGA header (is this a TGA file?)
	if ((h->cmaptype == 0 || h->cmaptype == 1) &&
		((h->imagetype >= 1 && h->imagetype <= 3) || (h->imagetype >= 9 && h->imagetype <= 11)) &&
		(h->bitsperpixel == 8 || h->bitsperpixel == 24 || h->bitsperpixel == 32))
	{
		// skip the ID field
		tga->cb.seek_func(source, h->idlen, SEEK_CUR);
		return 1;
	}
	else {
		// restore file position
		tga->cb.seek_func(source, pos, SEEK_SET);
		return 0;
	}
}


/**
	Reads Run-Length Encoded data.
*/
void tgaReadRLE(void *source, TGA *tga, unsigned char *buf, int size, int bpp)
{
	int repcount, bytes, k, n;
	unsigned char pixel[ 4 ];
	unsigned char one;

	// Dummy check
	if (bpp > 4)
		return;

	while (size > 0) {
		// Get repetition count
		tga->cb.read_func(&one, 1, 1, source);
		repcount = one;
		bytes = ((repcount & 127) + 1) * bpp;
		if (size < bytes) bytes = size;

		// Run-Length packet
		if (repcount & 128) {
			for (k = 0; k < bpp; k ++) {
				tga->cb.read_func(&one, 1, 1, source);
				pixel[k] = one;
			}
		    
			for (n = 0; n < (repcount & 127) + 1; n++)
				for (k = 0; k < bpp; k++)
					*buf++ = pixel[k];
		}

		// It's a Raw packet
		else {
			tga->cb.read_func(buf, bytes, 1, source);
			buf += bytes;
		}

		size -= bytes;
	} // while
}


/**
	Reads a TGA image from a file.

	@param source Data source to read the TGA image from.
	@param tga    Pointer to information structure which will be filled by the
	              function.
	@param flags  Flags #TGA_ORIGIN_UL, ... .

	@return @c 1 on success, @c 0 otherwise.
*/
long tgaRead(void *source, TGA *tga, int flags)
{
	TGA_Header h;
	unsigned char *cmap, *pix, tmp;
	int cmapsize;
	long pixsize, pixsize2;
	int bpp, bpp2, k, m, n, swapx, swapy;

	// Read TGA header
	if (!tgaReadHeader(source, tga, &h))
		return 0;

	// Is there a colormap?
	cmapsize = (h.cmaptype == TGA_CMAPTYPE_PRESENT ? 1 : 0) * h.cmaplen *
				((h.cmapentrysize+7) / 8);

	if (cmapsize > 0) {
		// Is it a colormap that we can handle?
		if ((h.cmapentrysize != 24 && h.cmapentrysize != 32) || h.cmaplen == 0 || h.cmaplen > 256)
			return 0;

		// Allocate memory for colormap
		cmap = (unsigned char *)malloc(cmapsize);
		if (cmap == NULL)
			return 0;

		// Read colormap from file
		tga->cb.read_func(cmap, cmapsize, 1, source);
	}
	else
		cmap = NULL;

	// Bytes per pixel (pixel data - unexpanded)
	bpp = (h.bitsperpixel + 7) / 8;

	// Size of pixel data
	pixsize = h.width * h.height * bpp;

	// Bytes per pixel (expanded pixels - not colormap indeces)
	bpp2 = cmap ? (h.cmapentrysize + 7) / 8 : bpp;

	// For colormaped images, the RGB/RGBA image data may use more memory
	// than the stored pixel data
	pixsize2 = h.width * h.height * bpp2;

	// Allocate memory for pixel data
	pix = (unsigned char *)malloc(pixsize2);
	if (pix == NULL) {
		if (cmap)
			free(cmap);
		return 0;
	}

	// Read pixel data from file
	if (h.imagetype >= TGA_IMAGETYPE_CMAP_RLE)
		tgaReadRLE(source, tga, pix, pixsize, bpp);
	else
		tga->cb.read_func(pix, pixsize, 1, source);

	// If the image origin is not bottom left, re-arrange the pixels
	switch (h._origin) {
	default:
	case _TGA_ORIGIN_UL:
		swapx = 0;
		swapy = 1;
		break;

	case _TGA_ORIGIN_BL:
		swapx = 0;
		swapy = 0;
		break;

	case _TGA_ORIGIN_UR:
		swapx = 1;
		swapy = 1;
		break;

	case _TGA_ORIGIN_BR:
		swapx = 1;
		swapy = 0;
		break;
	} // switch

	if ((swapy && !(flags & TGA_ORIGIN_UL)) || (!swapy && (flags & TGA_ORIGIN_UL))) {
		for (n = 0; n < h.height / 2; n++)
			for (m = 0; m < h.width; m++)
				for (k = 0; k < bpp; k++) {
					tmp = pix[(n * h.width + m) * bpp + k];
					pix[(n * h.width + m) * bpp + k] = pix[((h.height - 1 - n) * h.width + m) * bpp + k];
					pix[((h.height - 1 - n) * h.width + m) * bpp + k] = tmp;
				}
	}

	if (swapx) {
		for (n = 0; n < h.height; n++)
			for (m = 0; m < h.width / 2; m++)
				for (k = 0; k < bpp; k++) {
					tmp = pix[ (n*h.width+m)*bpp + k ];
					pix[ (n*h.width+m)*bpp + k ] = pix[ (n*h.width+h.width-1-m)*bpp + k ];
					pix[ (n*h.width+h.width-1-m)*bpp + k ] = tmp;
				}
	}

	// If we have a colormap, convert pixel data to image (BGR/BGRA) data
	if (cmap) {
		for (m = h.width * h.height - 1; m >= 0; m--) {
			n = pix[m];
			for (k = 0; k < bpp2; k++)
				pix[ m*bpp2 + k ] = cmap[ n*bpp2 + k ];
		}

		free(cmap);
		cmap = NULL;
	}

	// convert BGR to RGB
	for (n = 0; n < h.height * h.width * bpp2; n += bpp2) {
		tmp = pix[n];
		pix[n] = pix[n + 2];
		pix[n + 2] = tmp;
	}

	// Set remaining TGA information
	tga->width     = h.width;
	tga->height    = h.height;
	tga->bpp       = bpp2;
	tga->data      = pix;
	tga->data_size = pixsize2;

	switch (bpp2) {
	case 1: tga->format = TGA_PIXFMT_GRAY; break;
	default:
	case 3: tga->format = TGA_PIXFMT_RGB;  break;
	case 4: tga->format = TGA_PIXFMT_RGBA; break;
	}

	return 1;
}


long tgaRead(const char *file_name, TGA *tga, int flags)
{
	FILE *fd;
	long result;

	fd = fopen(file_name, "rb");

	if (fd == NULL)
		return 0;
	
	result = tgaRead((void *)fd, tga, flags);

	fclose(fd);
	return result;
}


long tgaRead(FILE *fd, TGA *tga, int flags)
{
	return tgaRead((void *)fd, tga, flags);
}


//=========================================================================
// Writing TGA image
//=========================================================================

static int tgaWriteHeader(void *source, TGA *tga, TGA_Header *h)
{
	unsigned char buf[18];
	long pos;

	// strore position
	pos = tga->cb.tell_func(source);

	// interpret header (endian independent parsing)
	buf[0]  = (unsigned char)h->idlen;
	buf[1]  = (unsigned char)h->cmaptype;
	buf[2]  = (unsigned char)h->imagetype;
	buf[3]  = (unsigned char)(h->cmapfirstidx & 0x00FF);
	buf[4]  = (unsigned char)(h->cmapfirstidx >> 8);
	buf[5]  = (unsigned char)(h->cmaplen & 0x00FF);
	buf[6]  = (unsigned char)(h->cmaplen >> 8);
	buf[7]  = (unsigned char)h->cmapentrysize;
	buf[8]  = (unsigned char)(h->xorigin & 0x00FF);
	buf[9]  = (unsigned char)(h->xorigin >> 8);
	buf[10] = (unsigned char)(h->yorigin & 0x00FF);
	buf[11] = (unsigned char)(h->yorigin >> 8);
	buf[12] = (unsigned char)(h->width & 0x00FF);
	buf[13] = (unsigned char)(h->width >> 8);
	buf[14] = (unsigned char)(h->height & 0x00FF);
	buf[15] = (unsigned char)(h->height >> 8);
	buf[16] = (unsigned char)h->bitsperpixel;
	buf[17] = (unsigned char)h->imageinfo;

	if (tga->cb.write_func(buf, 1, 18, source) < 18) {
		// restore file position
		tga->cb.seek_func(source, pos, SEEK_SET);
		return 0;
	}
	
	return 1;
}


int tgaWrite(void *source, TGA *tga, int flags)
{
	if (!source || !tga || !tga->data)
		return false;

	TGA_Header h;

	h.cmapentrysize = 0;
	h.cmapfirstidx = 0;
	h.cmaplen = 0;
	h.cmaptype = 0;
	h.idlen = 0;
	h.xorigin = 0;
	h.yorigin = 0;

	h.bitsperpixel = tga->bpp * 8; // 8 24 32
	h.width = tga->width;
	h.height = tga->height;

	switch (tga->format) {
	case TGA_PIXFMT_GRAY:  h.imagetype = TGA_IMAGETYPE_GRAY; break;
	case TGA_PIXFMT_RGB:
	case TGA_PIXFMT_RGBA:
	default:               h.imagetype = TGA_IMAGETYPE_TC; break;
	}

	h._alphabits = 0; //(tga->format == TGA_PIXFMT_RGBA) ? 8 : 0;
	h._origin = _TGA_ORIGIN_BL;
	h.imageinfo = (h._alphabits << TGA_IMAGEINFO_ALPHA_SHIFT) | (h._origin << TGA_IMAGEINFO_ORIGIN_SHIFT);	

	if (!tgaWriteHeader(source, tga, &h))
		return 0;

	// convert BGR to RGB
	unsigned char *pix, tmp;
	int n, m, k;
	for (pix = tga->data, n = 0; n < tga->height * tga->width * tga->bpp; n += tga->bpp) {
		tmp = pix[n];
		pix[n] = pix[n + 2];
		pix[n + 2] = tmp;
	}

	// swap image
	if (flags & TGA_ORIGIN_UL) {
		for (n = 0; n < tga->height / 2; n++)
			for (m = 0; m < tga->width; m++)
				for (k = 0; k < tga->bpp; k++) {
					tmp = pix[(n * tga->width + m) * tga->bpp + k];
					pix[(n * tga->width + m) * tga->bpp + k] = pix[((tga->height - 1 - n) * tga->width + m) * tga->bpp + k];
					pix[((tga->height - 1 - n) * tga->width + m) * tga->bpp + k] = tmp;
				}
	}

	tga->cb.write_func(tga->data, 1, tga->data_size, source);

	return 1;
}


int tgaWrite(const char *file_name, TGA *tga, int flags)
{
	if (!file_name || !tga || !tga->data)
		return false;

	FILE *fd;
	long result;

	fd = fopen(file_name, "wb");

	if (fd == NULL)
		return 0;
	
	result = tgaWrite((void *)fd, tga, flags);

	fclose(fd);
	return result;
}


int tgaWrite(FILE *fd, TGA *tga, int flags)
{
	if (!fd || !tga || !tga->data)
		return false;

	return tgaWrite((void *)fd, tga, flags);
}


// vim:ts=4:sw=4:
