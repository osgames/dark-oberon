#ifndef __tga_h_
#define __tga_h_

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file tga.h
	@ingroup LibTga_Module

 	@author Marcus Geelnard, Peter Knut
	@date 2002 - 2006

	TGA Loader Library.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <stdio.h>
#pragma warning(disable: 4996)


//=========================================================================
// Structures
//=========================================================================

/**
	Valid pixel format.
*/
typedef enum {
	TGA_PIXFMT_UNKNOWN,   ///< Unknown format.
	TGA_PIXFMT_GRAY,      ///< Grayscale pixel format.
	TGA_PIXFMT_RGB,       ///< RGB pixel format.
	TGA_PIXFMT_RGBA       ///< RGB-Alpha pixel format.
} TGA_Format;


/**
	The function prototypes for the callbacks are basically the same as for
	the stdio functions fread, fseek, ftell.
	The one difference is that the FILE * arguments have been replaced with
	a void * - this is to be used as a pointer to whatever internal data these
	functions might need. In the stdio case, it's just a FILE * cast to a void *.
*/
typedef struct {
	int    (*seek_func)  (void *source, long offset, int whence);
	long   (*tell_func)  (void *source);
	size_t (*read_func)  (void *buffer, size_t size, size_t count, void *source);
	size_t (*write_func) (const void *buffer, size_t size, size_t count, void *source);
} TGA_Callbacks;


/**
	TGA image file information.
*/
typedef struct {
	TGA_Callbacks cb;         ///< Callback functions for access to TGA data source.

	int width;                ///< Width of the image.
	int height;               ///< Height of the image.
	TGA_Format format;        ///< Pixels' format.
	int bpp;                  ///< Bytes per one pixel.
	unsigned char *data;      ///< Pointer to image data.
	unsigned long data_size;  ///< Size of image data.
} TGA;


//========================================================================
// Flags for tgaRead / tgaWrite
//========================================================================

/**
	Forces the first pixel of the image to be the upper left corner of the
	image (default is the lower left corner of the image).
*/
#define TGA_ORIGIN_UL 0x00000004


//========================================================================
// Methods
//========================================================================

void tgaPrepare(TGA *tga);

long tgaRead(const char *file_name, TGA *tga, int flags);
long tgaRead(FILE *fd, TGA *tga, int flags);
long tgaRead(void *source, TGA *tga, int flags);

int tgaWrite(const char *file_name, TGA *tga, int flags);
int tgaWrite(FILE *fd, TGA *tga, int flags);
int tgaWrite(void *source, TGA *tga, int flags);


#endif // __tga_h_

// vim:ts=4:sw=4:
