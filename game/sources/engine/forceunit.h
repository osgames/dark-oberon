#ifndef __forceunit_h__
#define __forceunit_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file forceunit.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/unit.h"
#include "engine/mapstructures.h"


//=========================================================================
// ForceUnit
//=========================================================================

/**
	@class ForceUnit
	@ingroup Engine_Module
*/
    
class ForceUnit : public Unit
{
//--- methods
public:
	ForceUnit(const char *id);

	// actions
	bool CanHideToUnit(const Unit *unit, bool write_msg) const;

	// messages
	virtual void OnMessage(ActionEvent *pevent);
	virtual void ProcessEvent(StateEvent *pevent);

protected:
	// actions
	bool StartHiding(Unit *unit);
	virtual void StopAction();

	// models
	virtual bool SetModel(const string &model_id, byte_t num_id);
	virtual bool ActivateModel(ModelType model);


//--- variables
protected:
	byte_t models[MODEL_COUNT][MapEnums::DIRECTION_COUNT];

	// temporary
	Ref<Unit> hider;
};


#endif // __forceunit_h__

// vim:ts=4:sw=4:
