//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file enginemodule.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"


//=========================================================================
// Kernel module
//=========================================================================

extern "C" void EngineModule();


extern bool s_InitGame (Class *, KernelServer *);
extern Object *s_NewGame (const char *name);
extern bool s_InitPreloaded (Class *, KernelServer *);
extern Object *s_NewPreloaded (const char *name);
extern bool s_InitProfile (Class *, KernelServer *);
extern Object *s_NewProfile (const char *name);
extern bool s_InitGui (Class *, KernelServer *);
extern Object *s_NewGui (const char *name);
extern bool s_InitLevel (Class *, KernelServer *);
extern Object *s_NewLevel (const char *name);
extern bool s_InitMap (Class *, KernelServer *);
extern Object *s_NewMap (const char *name);
extern bool s_InitScheme (Class *, KernelServer *);
extern Object *s_NewScheme (const char *name);
extern bool s_InitRace (Class *, KernelServer *);
extern Object *s_NewRace (const char *name);
extern bool s_InitPrototype (Class *, KernelServer *);
extern Object *s_NewPrototype (const char *name);
extern bool s_InitForcePrototype (Class *, KernelServer *);
extern Object *s_NewForcePrototype (const char *name);
extern bool s_InitBuildingPrototype (Class *, KernelServer *);
extern Object *s_NewBuildingPrototype (const char *name);
extern bool s_InitOnScreenText (Class *, KernelServer *);
extern Object *s_NewOnScreenText (const char *name);
extern bool s_InitPlayerRemoteServer (Class *, KernelServer *);
extern Object *s_NewPlayerRemoteServer (const char *name);
extern bool s_InitPlayerRemoteClient (Class *, KernelServer *);
extern Object *s_NewPlayerRemoteClient (const char *name);
extern bool s_InitPlayer (Class *, KernelServer *);
extern Object *s_NewPlayer (const char *name);
extern bool s_InitUnit (Class *, KernelServer *);
extern Object *s_NewUnit (const char *name);
extern bool s_InitBuildingUnit (Class *, KernelServer *);
extern Object *s_NewBuildingUnit (const char *name);
extern bool s_InitForceUnit (Class *, KernelServer *);
extern Object *s_NewForceUnit (const char *name);
extern bool s_InitSelectionRenderer (Class *, KernelServer *);
extern Object *s_NewSelectionRenderer (const char *name);
extern bool s_InitRectSelRenderer (Class *, KernelServer *);
extern Object *s_NewRectSelRenderer (const char *name);
extern bool s_InitBoxSelRenderer (Class *, KernelServer *);
extern Object *s_NewBoxSelRenderer (const char *name);
extern bool s_InitSelection (Class *, KernelServer *);
extern Object *s_NewSelection (const char *name);
extern bool s_InitPathServer (Class *, KernelServer *);
extern Object *s_NewPathServer (const char *name);
extern bool s_InitWavePathServer (Class *, KernelServer *);
extern Object *s_NewWavePathServer (const char *name);


void EngineModule()
{
	KernelServer::GetInstance()->RegisterClass("Game", s_InitGame, s_NewGame);
	KernelServer::GetInstance()->RegisterClass("Preloaded", s_InitPreloaded, s_NewPreloaded);
	KernelServer::GetInstance()->RegisterClass("Profile", s_InitProfile, s_NewProfile);
	KernelServer::GetInstance()->RegisterClass("Gui", s_InitGui, s_NewGui);
	KernelServer::GetInstance()->RegisterClass("Level", s_InitLevel, s_NewLevel);
	KernelServer::GetInstance()->RegisterClass("Map", s_InitMap, s_NewMap);
	KernelServer::GetInstance()->RegisterClass("Scheme", s_InitScheme, s_NewScheme);
	KernelServer::GetInstance()->RegisterClass("Race", s_InitRace, s_NewRace);
	KernelServer::GetInstance()->RegisterClass("Prototype", s_InitPrototype, s_NewPrototype);
	KernelServer::GetInstance()->RegisterClass("ForcePrototype", s_InitForcePrototype, s_NewForcePrototype);
	KernelServer::GetInstance()->RegisterClass("BuildingPrototype", s_InitBuildingPrototype, s_NewBuildingPrototype);
	KernelServer::GetInstance()->RegisterClass("OnScreenText", s_InitOnScreenText, s_NewOnScreenText);
	KernelServer::GetInstance()->RegisterClass("PlayerRemoteServer", s_InitPlayerRemoteServer, s_NewPlayerRemoteServer);
	KernelServer::GetInstance()->RegisterClass("PlayerRemoteClient", s_InitPlayerRemoteClient, s_NewPlayerRemoteClient);
	KernelServer::GetInstance()->RegisterClass("Player", s_InitPlayer, s_NewPlayer);
	KernelServer::GetInstance()->RegisterClass("Unit", s_InitUnit, s_NewUnit);
	KernelServer::GetInstance()->RegisterClass("BuildingUnit", s_InitBuildingUnit, s_NewBuildingUnit);
	KernelServer::GetInstance()->RegisterClass("ForceUnit", s_InitForceUnit, s_NewForceUnit);
	KernelServer::GetInstance()->RegisterClass("SelectionRenderer", s_InitSelectionRenderer, s_NewSelectionRenderer);
	KernelServer::GetInstance()->RegisterClass("RectSelRenderer", s_InitRectSelRenderer, s_NewRectSelRenderer);
	KernelServer::GetInstance()->RegisterClass("BoxSelRenderer", s_InitBoxSelRenderer, s_NewBoxSelRenderer);
	KernelServer::GetInstance()->RegisterClass("Selection", s_InitSelection, s_NewSelection);
	KernelServer::GetInstance()->RegisterClass("PathServer", s_InitPathServer, s_NewPathServer);
	KernelServer::GetInstance()->RegisterClass("WavePathServer", s_InitWavePathServer, s_NewWavePathServer);
}


// vim:ts=4:sw=4:
