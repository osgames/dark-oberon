//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scheme.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/scheme.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializer.h"
#include "framework/gfxserver.h"

PrepareClass(Scheme, "Preloaded", s_NewScheme, s_InitScheme);


//=========================================================================
// Scheme
//=========================================================================

/**
	Constructor.
*/
Scheme::Scheme(const char *id) :
	Preloaded(id),
	name("Unnamed Scheme")
{
	//
}


/**
	Destructor.
*/
Scheme::~Scheme()
{
	this->UnloadResources();
	this->ClearHeader();
	this->ClearBody();
}


void Scheme::ClearHeader()
{
	//
}


void Scheme::ClearBody()
{
	// delete materials
	for (MaterialsMap::iterator it = this->materials.begin(); it != this->materials.end(); it++)
		delete it->second;
	this->materials.clear();

	// delete fragments
	for (FragmentsMap::iterator it = this->fragments.begin(); it != this->fragments.end(); it++)
		delete it->second;
	this->fragments.clear();

	// clear terrains
	this->terrains.clear();
}


//=========================================================================
// Resources
//=========================================================================

bool Scheme::LoadResources()
{
	Assert(this->models.empty());

	size_t i;
	byte_t j;

	FragmentsMap::iterator fragment_it;

	for (fragment_it = this->fragments.begin(); fragment_it != this->fragments.end(); fragment_it++)
	{
		for (j = 0; j < MapEnums::SEG_COUNT; j++)
		{
			for (i = 0; i < fragment_it->second->models[j].size(); i++)
			{
				// create model
				Model *model = GfxServer::GetInstance()->NewModel(fragment_it->second->models[j][i]);
				if (!model || !model->Load())
				{
					LogError1("Error loading fragment model in: %s", this->file_name.c_str());
					return false;
				}
				
				// store pointer to new model
				this->models.push_back(model);
			}
		}
	}

	return true;
}


void Scheme::UnloadResources()
{
	// release all models
	for (size_t i = 0; i < this->models.size(); i++)
		this->models[i]->Release();

	// clear buffer
	this->models.clear();
}


//=========================================================================
// Deserializing
//=========================================================================

bool Scheme::ReadHeader(Serializer &serializer)
{
	// read	name [optional]
	serializer.GetAttribute("name", this->name);

	// read	author [optional]
	if (serializer.GetGroup("author"))
	{
		serializer.GetAttribute("name", this->author_name);
		serializer.GetAttribute("contact", this->author_contact);
		serializer.EndGroup();
	}

	return true;
}


bool Scheme::ReadBody(Serializer &serializer)
{
	// read	materials [optional]
	this->ReadMaterials(serializer);

	// read	terrains [mandatory]
	if (!this->ReadTerrains(serializer))
	{
		LogError1("Error reading terrains in: %s", this->file_name.c_str());
		return false;
	}

	// read	fragments [mandatory]
	if (!this->ReadFragments(serializer))
	{
		LogError1("Error reading fragments in: %s", this->file_name.c_str());
		return false;
	}

	// call ancestor
	return Root::Deserialize(serializer, false);
}


bool Scheme::ReadMaterials(Serializer &serializer)
{
	byte_t count;
	byte_t num_id;

	// read	materials [optional]
	if (!serializer.GetGroup("materials"))
		return true;

	count = (byte_t)serializer.GetGroupsCount();
	if (!count)
		return true;

	// fill materials map
	num_id = 0;
	if (serializer.GetGroup() && serializer.CheckGroupName("material"))
	{
		string material_id;

		do {
			// create new material structure
			Material *material = NEW Material;
			if (!material)
				return false;

			// read	id [mandatory]
			if (!serializer.GetAttribute("id", material_id, false))
			{
				LogError1("Missing material identifier in: %s", this->file_name.c_str());
				delete material;
				return false;
			}

			// read	name [optional]
			serializer.GetAttribute("name", material->name);

			// read	image [mandatory]
			if (!serializer.GetAttribute("image", material->image, false))
			{
				LogError1("Missing material image in: %s", this->file_name.c_str());
				delete material;
				return false;
			}

			// set numerical identifier
			material->num_id = num_id++;

			// add new material to map
			this->materials[material_id] = material;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (num_id < count)
		LogWarning1("Materials mismatch in: %s", this->file_name.c_str());

	serializer.EndGroup();

	return true;
}


bool Scheme::ReadTerrains(Serializer &serializer)
{
	byte_t count;

	// read terrains [mandatory]
	if (!serializer.GetGroup("terrains"))
	{
		LogError1("Missing terrains in: %s", this->file_name.c_str());
		return false;
	}
	count = (byte_t)serializer.GetGroupsCount();
	if (!count)
	{
		LogError1("Missing terrains in: %s", this->file_name.c_str());
		return false;
	}

	byte_t terrain_id = 1;
	if (serializer.GetGroup() && serializer.CheckGroupName("terrain"))
	{
		string id;

		do {
			// read	id [mandatory]
			if (!serializer.GetAttribute("id", id, false))
			{
				LogError1("Missing terrain identifier in: %s", this->file_name.c_str());
				return false;
			}

			this->terrains[id] = terrain_id++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	serializer.EndGroup();
	return true;
}


bool Scheme::ReadFragments(Serializer &serializer)
{
	byte_t count;
	byte_t i;

	// read	fragments [mandatory]
	if (!serializer.GetGroup("fragments"))
	{
		LogError1("Missing fragments in: %s", this->file_name.c_str());
		return false;
	}

	count = (byte_t)serializer.GetGroupsCount();
	if (!count)
	{
		LogError1("Missing fragments in: %s", this->file_name.c_str());
		return false;
	}

	// create fragments array
	i = 0;
	if (serializer.GetGroup() && serializer.CheckGroupName("fragment"))
	{
		do {
			Fragment *fragent = new Fragment;

			// read	fragment [mandatory]
			if (!this->ReadFragment(serializer, *fragent))
			{
				LogError2("Error reading fragment %hu in: %s", i, this->file_name.c_str());
				delete fragent;
				return false;
			}

			this->fragments[fragent->id] = fragent;
			i++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (i < count)
	{
		LogError1("Fragments mismatch in: %s", this->file_name.c_str());
		return false;
	}

	serializer.EndGroup();
	return true;
}


bool Scheme::ReadFragment(Serializer &serializer, Fragment &fragment)
{
	// read	id [mandatory]
	if (!serializer.GetAttribute("id", fragment.id, false))
	{
		LogError1("Missing fragment identifier in: %s", this->file_name.c_str());
		return false;
	}

	// read	name [optional]
	serializer.GetAttribute("name", fragment.name);

	// read	size [mandatory]
	if (!serializer.GetAttribute("width", fragment.size.width) || !serializer.GetAttribute("height", fragment.size.height))
	{
		LogError1("Missing fragment size in: %s", this->file_name.c_str());
		return false;
	}
	if (!fragment.size.width || !fragment.size.height)
	{
		LogError1("Wrong fragment size in: %s", this->file_name.c_str());
		return false;
	}

	// read	models [optional]
	if (!this->ReadModels(serializer, fragment))
	{
		LogError1("Error reading fragment models in: %s", this->file_name.c_str());
		return false;
	}

	// read	surface [mandatory]
	if (!this->ReadSurface(serializer, fragment))
	{
		LogError1("Error reading fragment surface in: %s", this->file_name.c_str());
		return false;
	}

	return true;
}


bool Scheme::ReadModels(Serializer &serializer, Fragment &fragment)
{
	byte_t count;
	byte_t i = 0;

	// read models [optional]
	if (!serializer.GetGroup("models"))
		return true;

	count = (byte_t)serializer.GetGroupsCount();
	if (!count)
		return true;

	if (serializer.GetGroup() && serializer.CheckGroupName("model"))
	{
		string src, segment_id;

		do {
			// read	model [mandatory]
			if (!serializer.GetAttribute("src", src))
			{
				LogError1("Missing model source in: %s", this->file_name.c_str());
				return false;
			}

			// read	segment [mandatory]
			if (!serializer.GetAttribute("segment", segment_id))
			{
				LogError1("Missing model segment in: %s", this->file_name.c_str());
				return false;
			}
			MapEnums::Segment segment;
			if (!MapEnums::StringToSegment(segment_id, segment))
				return false;

			fragment.models[segment].push_back(src);

			i++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (i < count)
	{
		LogError1("Models mismatch in: %s", this->file_name.c_str());
		return false;
	}

	serializer.EndGroup();
	return true;
}


bool Scheme::ReadSurface(Serializer &serializer, Fragment &fragment)
{
	// create 2D surface
	fragment.surface = Create2DArray<byte_t>(fragment.size.width, fragment.size.height);
	if (!fragment.surface)
	{
		LogError1("Error creating fragment surface in: %s", this->file_name.c_str());
		return false;
	}

	byte_t count;
	byte_t i;

	// read surface [mandatory]
	if (!serializer.GetGroup("surface"))
	{
		LogError1("Missing fragment surface in: %s", this->file_name.c_str());
		return false;
	}
	count = (byte_t)serializer.GetGroupsCount();
	if (count != fragment.size.width * fragment.size.height)
	{
		LogError1("Wrong number of surface terrains in: %s", this->file_name.c_str());
		return false;
	}

	i = 0;
	if (serializer.GetGroup() && serializer.CheckGroupName("terrain"))
	{
		string id;

		do {
			// read	identifier [mandatory]
			if (!serializer.GetAttribute("id", id, false))
			{
				LogError1("Missing surface identifier in: %s", this->file_name.c_str());
				return false;
			}

			fragment.surface[i % fragment.size.width][i / fragment.size.width] = this->GetTerrain(id);
			i++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	serializer.EndGroup();
	return true;
}


//=========================================================================
// Serializing
//=========================================================================

bool Scheme::WriteHeader(Serializer &serializer)
{
	// write name
	if (!serializer.SetAttribute("name", this->name)) return false;

	// write author
	if (!serializer.AddGroup("author")) return false;
		if (!serializer.SetAttribute("name", this->author_name)) return false;
		if (!serializer.SetAttribute("contact", this->author_contact)) return false;
	serializer.EndGroup();

	/// @todo Implement
	LogError("Serializing is not implemented");
	return true;
}


bool Scheme::WriteBody(Serializer &serializer)
{
	/// @todo Implement
	LogError("Serializing is not implemented");
	return true;
}


// vim:ts=4:sw=4:
