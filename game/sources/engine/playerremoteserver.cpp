//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file playerremoteserver.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/playerremoteserver.h"
#include "kernel/kernelserver.h"

PrepareClass(PlayerRemoteServer, "RemoteServer", s_NewPlayerRemoteServer, s_InitPlayerRemoteServer);


//=========================================================================
// PlayerRemoteServer
//=========================================================================

PlayerRemoteServer::PlayerRemoteServer(const char *id) :
	RemoteServer(id)
{
	//
}


PlayerRemoteServer::~PlayerRemoteServer()
{
	//
}


bool PlayerRemoteServer::CreateRemoteClient(byte_t client_id)
{
	ErrorMsg("Create new player here");
	return false;
}


// vim:ts=4:sw=4:
