//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file selection.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/selection.h"
#include "engine/prototype.h"
#include "engine/forceunit.h"
#include "engine/player.h"
#include "engine/unitmessages.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"

PrepareScriptClass(Selection, "PlayerRemoteClient", s_NewSelection, s_InitSelection, s_InitSelection_cmds);


//=========================================================================
// Selection
//=========================================================================

Selection::Selection(const char *id) :
	PlayerRemoteClient(id),
	units_type(Unit::TYPE_NONE),
	selected_action(Unit::ACTION_NONE),
	action(Unit::ACTION_NONE),
	aggressivity(Unit::AGGRESSIVITY_IGNORE),
	builder(NULL),
	is_my(false),
	can_move(false),
	can_attack(false),
	can_mine(false),
	can_repair(false),
	can_build(false),
	can_hide(false)
{
	//
}


void Selection::AddUnit(Unit *unit)
{
	Assert(unit);

	// can be selected?
	if (unit->IsFlags(Unit::FLAG_SELECTED))
		return;

	this->Lock();
	Prototype *prototype = unit->GetPrototype();

	// only one enemy unit can be selected
	// selected enemy unit is always deselected
	// only one non-force unit can be selected
	// selected non-force unit is always deselected
	// only same types of units can be selected
	if (!unit->IsMy() || !this->is_my 
		|| !unit->TestType(Unit::TYPE_FORCE) || this->units_type != Unit::TYPE_FORCE
		|| unit->GetType() != this->units_type
	)
		this->UnselectAll();

	// update properties and possible actions
	this->units_type = unit->GetType();
	this->is_my = unit->IsMy();

	if (this->is_my)
	{
		this->can_move = this->units.empty() ? prototype->CanMove() : this->can_move && prototype->CanMove();
		this->can_attack = this->can_attack || prototype->CanAttack();
		this->can_mine = this->can_mine || prototype->CanMine();
		this->can_repair = this->can_repair || prototype->CanRepair();
		this->can_build = this->can_build || prototype->CanBuild();
		this->can_hide = this->can_hide || prototype->CanHide();

		this->builder = this->can_build ? prototype : NULL;

		if (this->units.empty())
		{
			this->action = unit->GetAction();
			this->aggressivity = unit->GetAggressivity();
		}
		else
		{
			if (!unit->TestAction(this->action))
				this->action = Unit::ACTION_NONE;
			if (!unit->TestAggressivity(this->aggressivity))
				this->aggressivity = Unit::AGGRESSIVITY_IGNORE;
		}
	}
	else
	{
		this->can_move = this->can_attack = this->can_mine = this->can_repair = this->can_build = this->can_hide = false;
		this->builder = NULL;
		this->action = Unit::ACTION_NONE;
		this->aggressivity = Unit::AGGRESSIVITY_IGNORE;
	}

	// add unit into the list
	this->units.push_back(unit);
	unit->SetFlags(Unit::FLAG_SELECTED);

	this->Unlock();

	// add meeting point of unit to the scene
	if (unit->IsMy())
		unit->ShowMeetingPoint(true);
}


void Selection::RemoveUnit(Unit *unit)
{
	Assert(unit);

	// can be deselected?
	if (!unit->IsFlags(Unit::FLAG_SELECTED))
		return;

	this->Lock();

	unit->ClearFlags(Unit::FLAG_SELECTED);

	// find unit in the list
	UnitsList::iterator it;
	for (it = this->units.begin(); it != this->units.end(); it++)
	{
		if ((*it) == unit)
			break;
	}

	// if unit is not found, return
	if (it == this->units.end())
	{
		this->Unlock();
		return;
	}
	
	// remove unit from the list
	this->units.erase(it);

	// update properties and possible actions
	this->can_attack = this->can_mine = this->can_repair = this->can_build = this->can_hide = false;
	this->builder = NULL;

	if (this->units.empty())
	{
		this->units_type = Unit::TYPE_NONE;
		this->is_my = false;
		this->can_move = false;
		this->action = Unit::ACTION_NONE;
		this->aggressivity = Unit::AGGRESSIVITY_IGNORE;
	}

	else
	{
		Prototype *prototype;

		this->action = this->units.front()->GetAction();
		this->aggressivity = this->units.front()->GetAggressivity();

		for (it = this->units.begin(); it != this->units.end(); it++)
		{
			prototype = (*it)->GetPrototype();

			this->can_attack = this->can_attack || prototype->CanAttack();
			this->can_mine = this->can_mine || prototype->CanMine();
			this->can_repair = this->can_repair || prototype->CanRepair();
			this->can_build = this->can_build || prototype->CanBuild();
			this->can_hide = this->can_hide || prototype->CanHide();

			this->builder = this->can_build ? prototype : NULL;

			if (!(*it)->TestAction(this->action))
				this->action = Unit::ACTION_NONE;
			if (!(*it)->TestAggressivity(this->aggressivity))
				this->aggressivity = Unit::AGGRESSIVITY_IGNORE;
		}
	}

	this->Unlock();

	// remove meeting point of unit from the scene
	if (unit->IsMy())
		unit->ShowMeetingPoint(false);
}


void Selection::AddRemoveUnit(Unit *unit)
{
	Assert(unit);

	if (unit->IsFlags(Unit::FLAG_SELECTED))
		this->RemoveUnit(unit);
	else
		this->AddUnit(unit);
}


void Selection::UnselectAll()
{
	if (this->units.empty())
		return;

	this->Lock();

	// remove meeting points of units from the scene
	if (this->is_my)
	{
		UnitsList::iterator it;
		for (it = this->units.begin(); it != this->units.end(); it++)
			(*it)->ShowMeetingPoint(false);
	}

	// remove all units from selection
	UnitsList::iterator it;
	for (it = this->units.begin(); it != this->units.end(); it++)
		(*it)->ClearFlags(Unit::FLAG_SELECTED);

	this->units.clear();

	// reset properties and actions
	this->units_type = Unit::TYPE_NONE;
	this->is_my = false;
	this->can_move = this->can_attack = this->can_mine = this->can_repair = this->can_build = this->can_hide = false;
	this->builder = NULL;
	this->action = Unit::ACTION_NONE;
	this->aggressivity = Unit::AGGRESSIVITY_IGNORE;

	this->Unlock();
}


void Selection::Update()
{
	if (this->units.empty() || !this->is_my)
		return;

	this->Lock();

	// update action and aggressivity
	this->action = this->units.front()->GetAction();
	this->aggressivity = this->units.front()->GetAggressivity();

	UnitsList::iterator it;
	for (it = this->units.begin(); it != this->units.end(); it++)
	{
		if (!(*it)->TestAction(this->action))
			this->action = Unit::ACTION_NONE;
		if (!(*it)->TestAggressivity(this->aggressivity))
			this->aggressivity = Unit::AGGRESSIVITY_IGNORE;
	}

	this->Unlock();
}


//=========================================================================
// Actions
//=========================================================================

void Selection::StartMoving(Unit *unit)
{
	Assert(unit);

	this->Lock();

	if (this->can_move)
	{
		ActionEvent *pevent;
		UnitsList::iterator it;
		for (it = this->units.begin(); it != this->units.end(); it++)
		{
			pevent = NEW ActionEvent();

			pevent->action = Unit::ACTION_MOVE;
			pevent->player_id = unit->GetPlayer()->GetRemoteID();
			pevent->unit_id = unit->GetRemoteID();

			pevent->destination_id = (*it)->GetRemoteID();
			this->SendRemoteMessageToUnit(pevent, *it);
		}
	}

	this->Unlock();
}


void Selection::StartMoving(const MapPosition3D &dest_position)
{
	this->Lock();

	if (this->can_move)
	{
		ActionEvent *pevent;
		UnitsList::iterator it;
		for (it = this->units.begin(); it != this->units.end(); it++)
		{
			pevent = NEW ActionEvent();

			pevent->action = Unit::ACTION_MOVE;
			pevent->position = dest_position;

			pevent->destination_id = (*it)->GetRemoteID();
			this->SendRemoteMessageToUnit(pevent, *it);
		}
	}

	this->Unlock();
}


void Selection::StartHiding(Unit *unit)
{
	Assert(unit);

	this->Lock();

	Assert(this->is_my && this->units_type == Unit::TYPE_FORCE);

	ActionEvent *pevent;
	UnitsList::iterator it;
	for (it = this->units.begin(); it != this->units.end(); it++)
	{
		// do not hide unit to itself
		if ((*it) == unit)
			continue;

		pevent = NEW ActionEvent();

		pevent->action = Unit::ACTION_HIDE;
		pevent->player_id = unit->GetPlayer()->GetRemoteID();
		pevent->unit_id = unit->GetRemoteID();

		pevent->destination_id = (*it)->GetRemoteID();
		this->SendRemoteMessageToUnit(pevent, *it);
	}

	this->Unlock();
}


void Selection::StartEjecting()
{
	this->Lock();

	Assert(this->is_my && this->can_hide);

	ActionEvent *pevent;
	UnitsList::iterator it;
	for (it = this->units.begin(); it != this->units.end(); it++)
	{
		pevent = NEW ActionEvent();

		pevent->action = Unit::ACTION_EJECT;

		pevent->destination_id = (*it)->GetRemoteID();
		this->SendRemoteMessageToUnit(pevent, *it);
	}

	this->Unlock();
}


void Selection::SetMeetingPoint(const MapPosition3D &meeting_point)
{
	this->Lock();

	Assert(this->is_my);

	InputEvent *pevent;
	UnitsList::iterator it;
	for (it = this->units.begin(); it != this->units.end(); it++)
	{
		pevent = NEW InputEvent();

		pevent->input = Unit::INPUT_MEETING_POINT;
		pevent->position = meeting_point;

		pevent->destination_id = (*it)->GetRemoteID();
		this->SendRemoteMessageToUnit(pevent, *it);
	}

	this->Unlock();
}


/**
	Check if at least one unit can hide into the given unit.
*/
bool Selection::CanHideToUnit(const Unit *unit) const
{
	this->Lock();

	bool ok = false;

	// quick check
	if (!this->units.empty() && this->units_type == Unit::TYPE_FORCE && this->can_move 
		&& (this->units.size() > 1 || this->units.front() != unit)
		&& unit->IsMy() && unit->GetPrototype()->CanHide() && !unit->IsFull())
	{
		// check each unit, stop if we find unit that can hide with full check
		UnitsList::const_iterator it;
		for (it = this->units.begin(); it != this->units.end() && !ok; it++)
			ok = (*it) != unit && static_cast<ForceUnit *>(*it)->CanHideToUnit(unit, false);
	}

	this->Unlock();

	return ok;
}

// vim:ts=4:sw=4:
