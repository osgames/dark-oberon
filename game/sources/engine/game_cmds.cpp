//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file game_cmds.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2006

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/game.h"


//=========================================================================
// Commands
//=========================================================================

static void s_SetTitle(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	self->SetTitle(string(cmd->In()->GetS()));
}


static void s_GetTitle(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	cmd->Out()->SetS(self->GetTitle().c_str());
}


static void s_IsRun(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	cmd->Out()->SetB(self->IsRun());
}


static void s_Quit(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	self->Quit();
}


static void s_GetActiveProfile(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	cmd->Out()->SetO(self->GetActiveProfile());
}


static void s_GetActiveGui(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	cmd->Out()->SetO(self->GetActiveGui());
}


static void s_GetActiveLevel(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	cmd->Out()->SetO(self->GetActiveLevel());
}


static void s_GetActiveMap(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	cmd->Out()->SetO(self->GetActiveMap());
}


static void s_GetActivePlayer(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	cmd->Out()->SetO(self->GetActivePlayer());
}


static void s_GetMouseMapPosition(void *slf, Cmd *cmd)
{
	Game *self = (Game *)slf;
	const MapPosition3D &position = self->GetMouseInfo().GetMapPosition();
	string segment;
	if (!MapEnums::SegmentToString(position.segment, segment))
		return;

	cmd->Out()->SetI(position.x);
	cmd->Out()->SetI(position.y);
	cmd->Out()->SetS(segment.c_str());
}


void s_InitGame_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("v_SetTitle_s",         'STIT', s_SetTitle);
	cl->AddCmd("s_GetTitle_v",         'GTIT', s_GetTitle);
	cl->AddCmd("b_IsRun_v",            'IRUN', s_IsRun);
	cl->AddCmd("v_Quit_v",             'QUIT', s_Quit);

	cl->AddCmd("o_GetActiveProfile_v", 'GAPR', s_GetActiveProfile);
	cl->AddCmd("o_GetActiveGui_v",     'GAGU', s_GetActiveGui);
	cl->AddCmd("o_GetActiveLevel_v",   'GALV', s_GetActiveLevel);
	cl->AddCmd("o_GetActiveMap_v",     'GAMA', s_GetActiveMap);
	cl->AddCmd("o_GetActivePlayer_v",  'GAPL', s_GetActivePlayer);

	cl->AddCmd("iis_GetMouseMapPosition_v", 'GMMP', s_GetMouseMapPosition);

	cl->EndCmds();
}

// vim:ts=4:sw=4:
