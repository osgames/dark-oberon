//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file engine.cpp
	@ingroup Engine_Module

	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added logging to file.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/engine.h"
#include "engine/game.h"
#include "glfwmodule/glfwkernelserver.h"
#include "kernel/scriptserver.h"
#include "kernel/fileserver.h"


UseModule(Framework);
UseModule(GlfwModule);
UseModule(LuaModule);
UseModule(OpenALModule);
UseModule(OpenGLModule);
UseModule(EngineModule);
UseModule(XercesModule);


//=========================================================================
// Engine
//=========================================================================

/**
	Constructor.
*/
Engine::Engine() :
	opened(false),
	kernel_server(NULL)
{
	//
}


/**
	Destructor.
*/
Engine::~Engine()
{
	//
}


/**
	Starts engine. First all core servers are created (KernelServer, FileServer and ScriptServer) and all available
	modules are registered. Then main script (app:setup.lua) is processed. In these script are created all servers
	that we will use. Logging into standart output is also enabled.

	@param argc Number of arguments.
	@param argv Array of argumetns that are given to executable file.

	@return @c True if successful.
*/
bool Engine::Open(int argc, char *argv[])
{
	Assert(argv);
	Assert(argc > 0);
	Assert(!this->opened);

	// create kernel server
	this->kernel_server = NEW GlfwKernelServer();
	Assert(this->kernel_server);

	// process application arguments
	this->kernel_server->ProcessArgs(argc, argv);

	// register all modules
	this->kernel_server->RegisterModule(Framework);
	this->kernel_server->RegisterModule(LuaModule);
	this->kernel_server->RegisterModule(OpenALModule);
	this->kernel_server->RegisterModule(OpenGLModule);
	this->kernel_server->RegisterModule(GlfwModule);
	this->kernel_server->RegisterModule(EngineModule);
	this->kernel_server->RegisterModule(XercesModule);

	// create file server
	if (!kernel_server->New("FileServer",  NOH_FILE_SERVER))
	{
		LogError("Error creating file server");
		SafeDelete(this->kernel_server);
		return false;
	}

#if !defined(WINDOWS) || defined(DEBUG)
	// register logging into standart streams
	LogServer::GetInstance()->RegisterFile(FileServer::GetStdOut(), LogServer::LL_ALL_STD);
	LogServer::GetInstance()->RegisterFile(FileServer::GetStdErr(), LogServer::LL_ALL_ERR);
#endif

	// create script server
	string result;
	ScriptServer *script_server = (ScriptServer *)kernel_server->New("ScriptServer",  NOH_SCRIPT_SERVER);
	if (!script_server)
	{
		LogError("Error creating script server");
		SafeDelete(this->kernel_server);
		return false;
	}

	// create lua loader
	if (!script_server->NewScriptLoader("LuaScriptLoader", string("lua")))
	{
		LogError("Error creating LUA script loader");
		SafeDelete(this->kernel_server);
		return false;
	}

	// run main setup script
	if (!script_server->RunScript(string("app:setup.lua"), result))
	{
		LogError1("Error running setup scipt: %s", "app:setup.lua");
		SafeDelete(this->kernel_server);
		return false;
	}

	// register logging to file
	LogServer::GetInstance()->RegisterFile(string("logs:full.log"), LogServer::LL_ALL);

	return this->opened = true;
}


/**
	Creates and runs game object of given class name. If game module is presented, this module will be registered
	first.

	@param game_class  Class name of game object.
	@param title       Game title.
	@param game_module Pointer to game module.

	@return @c True if successful.
*/
bool Engine::Run(const char *game_class, const string &title, void (*game_module) (void))
{
	Assert(game_class && *game_class);
	Assert(this->opened);

	// register game module
	if (game_module)
		this->kernel_server->RegisterModule(game_module);

	// create game object
	Game *game = (Game *)kernel_server->New(game_class,  NOH_GAME);
	if (!game)
		return false;

	// game properties
	game->SetTitle(title);
	bool ok = false;

	// open and run game
	if (game->Open())
	{
		ok = game->Run();
		game->Close();
	}

	// release game
	game->Release();
	return ok;
}


/**
	Stops engine. Releases kernel server (with all servers and objects in NOH).
*/
void Engine::Close()
{
	Assert(this->opened);

	// delete kernel server, other servers are released automatically
	SafeDelete(this->kernel_server);

	this->opened = false;
}


// vim:ts=4:sw=4:
