//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file selectionrenderer.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/selectionrenderer.h"
#include "engine/unit.h"
#include "engine/player.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/vector.h"

PrepareClass(SelectionRenderer, "Root", s_NewSelectionRenderer, s_InitSelectionRenderer);


//=========================================================================
// SelectionRenderer
//=========================================================================

SelectionRenderer::SelectionRenderer(const char *id) :
	Root(id)
{
	this->gfx_server = GfxServer::GetInstance();
	Assert(this->gfx_server);
}


void SelectionRenderer::RenderBackground(Unit *unit)
{
	//
}


void SelectionRenderer::RenderForeground(Unit *unit)
{
	//
}


//=========================================================================
// Tools
//=========================================================================

const vector4 &SelectionRenderer::GetHoverColor(Unit *unit)
{
	Assert(unit);

	static vector4 my_color(COLOR_MY_UNIT, 0.4f);
	static vector4 enemy_color(COLOR_ENEMY_UNIT, 0.4f);
	static vector4 hyper_color(COLOR_HYPER_UNIT, 0.4f);

	if (unit->GetPlayer()->IsActive())
		return my_color;
	else if (unit->GetPlayer()->IsHyper())
		return hyper_color;
	else
		return enemy_color;
}


const vector3 &SelectionRenderer::GetSelectionColor(Unit *unit)
{
	Assert(unit);

	static vector3 my_color(COLOR_MY_UNIT);
	static vector3 enemy_color(COLOR_ENEMY_UNIT);
	static vector3 hyper_color(COLOR_HYPER_UNIT);

	if (unit->GetPlayer()->IsActive())
		return my_color;
	else if (unit->GetPlayer()->IsHyper())
		return hyper_color;
	else
		return enemy_color;
}


const vector3 &SelectionRenderer::GetLifeColor(Unit *unit)
{
	Assert(unit);

	static vector3 color1(COLOR_LIFE1);
	static vector3 color2(COLOR_LIFE2);
	static vector3 color3(COLOR_LIFE3);

	float percent = unit->GetLifePercent();

	if (percent > LIFE_LIMIT1)
		return color1;
	else if (percent > LIFE_LIMIT2)
		return color2;
	else
		return color3;
}


const vector3 &SelectionRenderer::GetHidingColor(Unit *unit)
{
	Assert(unit);

	static vector3 color(COLOR_HIDING);
	return color;
}


// vim:ts=4:sw=4:
