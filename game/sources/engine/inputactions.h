#ifndef __inputactions_h__
#define __inputactions_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file inputactions.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"


//=========================================================================
// Constants
//=========================================================================

// game
#define IN_ACTION_QUIT          "quit"
#define IN_ACTION_SYSTEM_MOUSE  "systemmouse"
#define IN_ACTION_SCREENSHOT    "screenshot"
#define IN_ACTION_FULLSCREEN    "fullscreen"

// units
#define IN_ACTION_STAY        "stay"
#define IN_ACTION_MOVE        "move"
#define IN_ACTION_ATTACK      "attack"
#define IN_ACTION_BUILD       "build"
#define IN_ACTION_MINE        "mine"
#define IN_ACTION_REPAIR      "repair"

// player
#define IN_ACTION_SAY         "say"
#define IN_ACTION_SAY_TEAM    "sayteam"

// gui
#define IN_ACTION_CANCEL      "cancel"
#define IN_ACTION_MENU        "menu"
#define IN_ACTION_PANELS      "panels"
#define IN_ACTION_CONSOLE     "console"

// map
#define IN_ACTION_MOVE_MAP_UP      "movemapup"
#define IN_ACTION_MOVE_MAP_DOWN    "movemapdown"
#define IN_ACTION_MOVE_MAP_LEFT    "movemapleft"
#define IN_ACTION_MOVE_MAP_RIGHT   "movemapright"
#define IN_ACTION_MOVE_MAP         "movemap"
#define IN_ACTION_CATCH_MAP        "catchmap"
#define IN_ACTION_ZOOM_MAP_IN      "zoommapin"
#define IN_ACTION_ZOOM_MAP_OUT     "zoommapout"
#define IN_ACTION_ZOOM_MAP         "zoommap"
#define IN_ACTION_RESET_MAP_ZOOM   "resetmapzoom"

// segments
#define IN_ACTION_SHOW_UNDERGROUND "showunderground"
#define IN_ACTION_SHOW_GROUND      "showground"
#define IN_ACTION_SHOW_SKY         "showsky"
#define IN_ACTION_SHOW_MULTIPLE    "showmultiple"


#endif // __inputactions_h__

// vim:ts=4:sw=4:
