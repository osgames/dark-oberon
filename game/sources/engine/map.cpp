//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file map.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/map.h"
#include "engine/game.h"
#include "engine/unit.h"
#include "engine/inputactions.h"
#include "engine/prototype.h"
#include "engine/player.h"
#include "engine/playermap.h"
#include "framework/sceneserver.h"
#include "framework/isoscene.h"
#include "framework/inputserver.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializer.h"
#include "kernel/timeserver.h"

PrepareScriptClass(Map, "Preloaded", s_NewMap, s_InitMap, s_InitMap_cmds);


//=========================================================================
// Defienes
//=========================================================================

#define MAP_MAX_SIZE          240       ///< Maximal map width or height.
#define MAP_MAX_MOVE_SPEED    2000.0f   ///< Maximal moving speed.
#define MAP_MAX_ZOOM_SPEED    10.0f     ///< Maximal zoom speed.
#define MAP_MAX_ZOOM          5.0f      ///< Maximal zoom coeficient.
#define MAP_MIN_ZOOM          0.2f      ///< Minimal zoom coeficient.
#define MAP_MOVING_FRAME      3         ///< Width of frame around the window edge that is sensitive to map moving.

#define MAP_UNDERGROUND_TRANSPARENCY  0.5f   ///< Transparency used for drawing underground units in multiple segments.


//=========================================================================
// Embeded
//=========================================================================

Map::Fragment::Fragment() :
	ref(NULL)
{
	for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++)
		scene_objects[i] = NULL;
}


Map::Fragment::~Fragment()
{
	for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++)
		SafeRelease(this->scene_objects[i]);
}


//=========================================================================
// Map
//=========================================================================

/**
	Constructor.
*/
Map::Map(const char *id) :
	Preloaded(id),
	name("Unnamed Map"),
	move_speed(0.5f),
	zoom_speed(0.5f),
	zoom(1.0f),
	projection_coef_h(0.0f),
	projection_coef_v(0.0f),
	view_changed(false),
	change_direction(CD_NONE),
	surface(NULL),
	visible_segment(MapEnums::SEG_COUNT),    // multiple segments will by visible by default
	warfog_enabled(true),
	catched(false),
	frame_moving(true)
{
	//
}


/**
	Destructor.
*/
Map::~Map()
{
	this->UnloadResources();
	this->ClearHeader();
	this->ClearBody();
}


void Map::ClearHeader()
{
	//
}


void Map::ClearBody()
{
	for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++)
	{
		SafeRelease(this->segments[i].terrain_scene);
		SafeRelease(this->segments[i].units_scene);
		Delete2DArray<Field>(this->segments[i].fields, this->size.width);
		this->segments[i].fields = NULL;
	}

	this->fragments.clear();

	if (this->scheme.IsValid())
		this->scheme->Unload();

	Delete2DArray<byte_t>(this->surface, this->size.width);
	this->surface = NULL;
}


void Map::SetActive(bool active)
{
	Assert(InputServer::GetInstance());

	// de/activate scheme
	this->scheme->SetActive(active);

	// register callback funtion for actions
	if (active)
		InputServer::GetInstance()->AddActionCallback(this->OnAction, this);

	// unregister callback funtion for actions
	else
		InputServer::GetInstance()->RemoveActionCallback(this->OnAction);
}


void Map::UpdateViewport(ushort_t width, ushort_t height)
{
	// set new values to viewport
	this->viewport.Set(0, 0, width, height);

	this->UpdateProjection();
}


void Map::UpdateProjection()
{
	// preserve proper ratio in projection
	ushort_t w = n_min(this->viewport.width, ushort_t(this->viewport.height * PROJECTION_WIDTH / PROJECTION_HEIGHT));
	ushort_t h = n_min(this->viewport.height, ushort_t(this->viewport.width * PROJECTION_HEIGHT / PROJECTION_WIDTH));

	// compute center projection
	double right = (PROJECTION_WIDTH / 2) * (double(this->viewport.width) / w) * this->zoom;
	double top = (PROJECTION_HEIGHT / 2) * (double(this->viewport.height) / h) * this->zoom;
	double left = -right;
	double bottom = -top;

	right -= this->position.x;
	left -= this->position.x;
	top -= this->position.y;
	bottom -= this->position.y;

	this->projection.SetOrtho2D(left, right, bottom, top);

	// compute helper coeficients
	float zoom_h = (float(this->viewport.width) / w) * this->zoom;
	float zoom_v = (float(this->viewport.height) / h) * this->zoom;

	this->projection_coef_h = (PROJECTION_WIDTH * zoom_h) / this->viewport.width;
	this->projection_coef_v = (PROJECTION_HEIGHT * zoom_v) / this->viewport.height;

	// update all scenes
	vector<Segment>::iterator seg_it;
	float segment_shift = -SEGMENTS_SHIFT * ISO_SCENE_V_COEF * 2;
	for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++, segment_shift += SEGMENTS_SHIFT * ISO_SCENE_V_COEF * 2)
	{
		this->projection.SetTranslation(0.0f, segment_shift, 0.0f);

		this->segments[i].terrain_scene->SetViewport(this->viewport);
		this->segments[i].terrain_scene->SetProjection(this->projection);

		this->segments[i].units_scene->SetViewport(this->viewport);
		this->segments[i].units_scene->SetProjection(this->projection);
	}

	this->projection.SetTranslation(0.0f, 0.0f, 0.0f);

	// update visible area
	this->UpdateVisibleArea();
}


void Map::UpdateView()
{
	Assert(InputServer::GetInstance());

	// recognise map moving with mouse cursor in the edge of the window (only if with non-system cursor)
	if (!GfxServer::GetInstance()->TestCursorVisibility(GfxServer::CURSOR_SYSTEM) && this->frame_moving)
	{
		// get mouse position
		ushort_t x, y;
		InputServer::GetInstance()->GetMousePosition(x, y);

		// detect map moving
		if (x < MAP_MOVING_FRAME)
			this->ChangeView(Map::CD_MOVE_RIGHT);
		else if (x >= this->viewport.width - MAP_MOVING_FRAME)
			this->ChangeView(Map::CD_MOVE_LEFT);
		if (y < MAP_MOVING_FRAME)
			this->ChangeView(Map::CD_MOVE_DOWN);
		else if (y >= this->viewport.height - MAP_MOVING_FRAME)
			this->ChangeView(Map::CD_MOVE_UP);
	}

	if (!this->catched)
	{
		InputServer::GetInstance()->FixMouse(false);

		if (!this->frame_moving)
		{
			// get mouse position
			ushort_t x, y;
			InputServer::GetInstance()->GetMousePosition(x, y);

			this->frame_moving = x >= MAP_MOVING_FRAME
				&& x < this->viewport.width - MAP_MOVING_FRAME
				&& y >= MAP_MOVING_FRAME
				&& y < this->viewport.height - MAP_MOVING_FRAME;
		}
	}

	this->catched = false;

	if (!this->view_changed)
		return;

	// update position
	if (this->change_direction)
	{
		// map position
		if ((this->change_direction & CD_MOVE_UP) && !(this->change_direction & CD_MOVE_DOWN))
			this->position.y += float(MAP_MAX_MOVE_SPEED * TimeServer::GetInstance()->GetFrameTime() * this->move_speed);

		if ((this->change_direction & CD_MOVE_DOWN) && !(this->change_direction & CD_MOVE_UP))
			this->position.y -= float(MAP_MAX_MOVE_SPEED * TimeServer::GetInstance()->GetFrameTime() * this->move_speed);

		if ((this->change_direction & CD_MOVE_LEFT) && !(this->change_direction & CD_MOVE_RIGHT))
			this->position.x -= float(MAP_MAX_MOVE_SPEED * TimeServer::GetInstance()->GetFrameTime() * this->move_speed);

		if ((this->change_direction & CD_MOVE_RIGHT) && !(this->change_direction & CD_MOVE_LEFT))
			this->position.x += float(MAP_MAX_MOVE_SPEED * TimeServer::GetInstance()->GetFrameTime() * this->move_speed);

		// map zoom
		if ((this->change_direction & CD_ZOOM_IN) && !(this->change_direction & CD_ZOOM_OUT) && !(this->change_direction & CD_ZOOM_RESET))
		{
			float diff = 1.0f + float((MAP_MAX_ZOOM_SPEED - 1.0f) * TimeServer::GetInstance()->GetFrameTime() * this->zoom_speed);
			if (diff != 1.0f)
				this->zoom /= diff;
		}

		if ((this->change_direction & CD_ZOOM_OUT) && !(this->change_direction & CD_ZOOM_IN) && !(this->change_direction & CD_ZOOM_RESET))
		{
			float diff = 1.0f + float((MAP_MAX_ZOOM_SPEED - 1.0f) * TimeServer::GetInstance()->GetFrameTime() * this->zoom_speed);
			if (diff != 1.0f)
				this->zoom *= diff;
		}

		if (this->change_direction & CD_ZOOM_RESET)
		{
			this->zoom = 1.0f;
		}
	}

	// check position boundaries
	float maxx = this->size.width * ISO_SCENE_H_COEF;
	if (this->position.x > maxx)
		this->position.x = maxx;
	else if (this->position.x < -maxx)
		this->position.x = -maxx;

	float miny = -(this->size.width + this->size.height) * ISO_SCENE_V_COEF;
	if (this->position.y < miny)
		this->position.y = miny;
	else if (this->position.y > 0.0f)
		this->position.y = 0.0f;

	// check zoom boundaries
	if (this->zoom > MAP_MAX_ZOOM)
		this->zoom = MAP_MAX_ZOOM;
	else if (this->zoom < MAP_MIN_ZOOM)
		this->zoom = MAP_MIN_ZOOM;

	// reset changing flags
	this->change_direction = CD_NONE;
	this->view_changed = false;

	// update projection
	this->UpdateProjection();
}


void Map::UpdateVisibleArea()
{
	ushort_t w = this->viewport.width;
	ushort_t h = this->viewport.height;
	vector2 west, east, south, north;

	// compute position and size of visible area [mapels]
	this->ScreenToMap(MapEnums::SEG_GROUND, vector2(0, h), west);
	if (west.x < 0) west.x = 0;
	if (west.x > this->size.width) west.x = this->size.width;

	this->ScreenToMap(MapEnums::SEG_GROUND, vector2(w, 0), east);
	east.x++;
	if (east.x < west.x) east.x = west.x;
	if (east.x > this->size.width) east.x = this->size.width;

	this->ScreenToMap(MapEnums::SEG_GROUND, vector2(w, h), south);
	if (south.y < 0) south.y = 0;
	if (south.y > this->size.height) south.y = this->size.height;

	this->ScreenToMap(MapEnums::SEG_GROUND, vector2(0, 0), north);
	north.y++;
	if (north.y < south.y) north.y = south.y;
	if (north.y > this->size.height) north.y = this->size.height;

	// set new visible area
	this->visible_area.Set(mapel_t(west.x), mapel_t(south.y), mapel_t(east.x) - mapel_t(west.x), mapel_t(north.y) - mapel_t(south.y));
}


bool Map::UpdateSurface(const MapPosition2D &position, const Scheme::Fragment &fragment)
{
	if (!this->IsInMap(position, fragment.size))
		return false;

	mapel_t x, y;
	mapel_t segx, segy;
	
	for (x = 0, segx = position.x; x < fragment.size.width; x++, segx++)
	{
		for (y = 0, segy = position.y; y < fragment.size.height; y++, segy++)
		{
			// check fragments overlapping
			if (this->surface[segx][segy])
				return false;

			this->surface[segx][segy] = fragment.surface[x][y];
		}
	}

	return true;
}


void Map::Render()
{
	Assert(SceneServer::GetInstance());
	Assert(GfxServer::GetInstance());

	GfxServer *gfx_server = GfxServer::GetInstance();
	SceneServer *scene_server = SceneServer::GetInstance();

	// update view
	this->UpdateView();

	// render segments
	vector<Fragment>::iterator frag_it;
	list<Unit *>::iterator unit_it;

	Segment *segment = this->segments;

	this->Lock();

	if (this->visible_segment < MapEnums::SEG_COUNT)
	{
		// render scenes
		scene_server->RenderScene(this->segments[this->visible_segment].terrain_scene);
		if (this->warfog_enabled)
			Game::GetInstance()->GetActivePlayer()->GetPlayerMap()->RenderWarfog(this->visible_segment == MapEnums::SEG_SKY ? MapEnums::SEG_GROUND : this->visible_segment, this->visible_area);
		scene_server->RenderScene(this->segments[this->visible_segment].units_scene);
	}

	else
	{
		for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++, segment++)
		{
			if (i == MapEnums::SEG_GROUND)
			{
				// render mask
				gfx_server->SetDepthTest(true);
				gfx_server->ClearBuffers(GfxServer::BUFFER_DEPTH);
				gfx_server->SetProjection(this->projection);

				Game::GetInstance()->GetActivePlayer()->GetPlayerMap()->RenderMask(MapEnums::Segment(i), this->visible_area);
			}

			// render terrain
			SceneServer::GetInstance()->RenderScene(segment->terrain_scene);

			if (i == MapEnums::SEG_GROUND)
			{
				// render transparent underground units
				gfx_server->SetColorFilter(1.0f, 1.0f, 1.0f, MAP_UNDERGROUND_TRANSPARENCY);
				scene_server->RenderScene(this->segments[MapEnums::SEG_UNDERGROUND].units_scene);
				gfx_server->SetColorFilter(1.0f, 1.0f, 1.0f);

				gfx_server->SetDepthTest(false);

				// render warfog
				gfx_server->SetProjection(this->projection);
				if (this->warfog_enabled)
					Game::GetInstance()->GetActivePlayer()->GetPlayerMap()->RenderWarfog(MapEnums::SEG_SKY, this->visible_area);    // SEG_SKY means multiple warfog
			}

			// render units
			scene_server->RenderScene(segment->units_scene);
		}
	}

	gfx_server->ResetMatrix();

	this->Unlock();
}


bool Map::AddUnit(Unit *unit)
{
	Assert(unit);

	this->Lock();

	// get unit properties
	const MapPosition3D &unit_position = unit->GetMapPosition();
	const MapSize &unit_size = unit->GetPrototype()->GetSize();

	// check if non-ghost unit can be added to the map
	if (!unit->TestState(Unit::STATE_GHOST) && !this->IsFree(unit_position, unit_size))
	{
		this->Unlock();
		return false;
	}

	// add unit to the scene
	Assert(unit_position.segment < MapEnums::SEG_COUNT);
	Segment *segment = this->segments + unit_position.segment;
	segment->units_scene->AddObject(unit->GetSceneObject());

	// update fields
	for (mapel_t i = 0; i < unit_size.width; i++)
	{
		for (mapel_t j = 0; j < unit_size.height; j++)
		{
			if (unit->TestState(Unit::STATE_GHOST))
			{
				Assert(!segment->fields[unit_position.x + i][unit_position.y + j].ghost);
				segment->fields[unit_position.x + i][unit_position.y + j].ghost = unit;
			}
			else
			{
				Assert(!segment->fields[unit_position.x + i][unit_position.y + j].unit);
				segment->fields[unit_position.x + i][unit_position.y + j].unit = unit;
			}
		}
	}

	this->Unlock();
	return true;
}


void Map::RemoveUnit(Unit *unit)
{
	Assert(unit);

	this->Lock();

	const MapPosition3D &unit_position = unit->GetMapPosition();
	const MapSize &unit_size = unit->GetPrototype()->GetSize();

	// remove unit from segment
	Segment *segment = this->segments + unit_position.segment;
	segment->units_scene->RemoveObject(unit->GetSceneObject());

	// update fields
	Assert(this->IsInMap(unit_position, unit_size));

	for (mapel_t i = 0; i < unit_size.width; i++)
	{
		for (mapel_t j = 0; j < unit_size.height; j++)
		{
			if (unit->TestState(Unit::STATE_GHOST))
			{
				Assert(segment->fields[unit_position.x + i][unit_position.y + j].ghost == unit);
				segment->fields[unit_position.x + i][unit_position.y + j].ghost = NULL;
			}
			else
			{
				Assert(segment->fields[unit_position.x + i][unit_position.y + j].unit == unit);
				segment->fields[unit_position.x + i][unit_position.y + j].unit = NULL;
			}
		}
	}

	this->Unlock();
}


void Map::AddSceneObject(MapEnums::Segment segment_id, SceneObject *scene_object)
{
	Assert(segment_id < MapEnums::SEG_COUNT);

	Segment *segment = this->segments + segment_id;
	segment->units_scene->AddObject(scene_object);
}


void Map::RemoveSceneObject(MapEnums::Segment segment_id, SceneObject *scene_object)
{
	Assert(segment_id < MapEnums::SEG_COUNT);

	Segment *segment = this->segments + segment_id;
	segment->units_scene->RemoveObject(scene_object);
}


bool Map::OnAction(const string &action, int position, void *data)
{
	Assert(TimeServer::GetInstance());
	Assert(GfxServer::GetInstance());

	// get pointer to this object
	Assert(data);
	Map *map = (Map *)data;

	// moving
	if (action == IN_ACTION_MOVE_MAP_UP)
		map->ChangeView(CD_MOVE_UP);

	else if (action == IN_ACTION_MOVE_MAP_DOWN)
		map->ChangeView(CD_MOVE_DOWN);

	else if (action == IN_ACTION_MOVE_MAP_LEFT)
		map->ChangeView(CD_MOVE_LEFT);

	else if (action == IN_ACTION_MOVE_MAP_RIGHT)
		map->ChangeView(CD_MOVE_RIGHT);

	else if (action == IN_ACTION_CATCH_MAP)
	{
		if (!map->catched)
		{
			int dx, dy;

			InputServer::GetInstance()->GetRelMousePosition(dx, dy);
			if (dx || dy)
			{
				map->position.x += dx * map->projection_coef_h;
				map->position.y -= dy * map->projection_coef_v;
				map->view_changed = true;
			}

			map->catched = true;
			InputServer::GetInstance()->FixMouse(false);
			map->frame_moving = false;
		}
	}

	else if (action == IN_ACTION_MOVE_MAP)
	{
		if (!map->catched)
		{
			int dx, dy;

			InputServer::GetInstance()->GetRelMousePosition(dx, dy);
			if (dx || dy)
			{
				map->position.x -= dx * map->projection_coef_h;
				map->position.y += dy * map->projection_coef_v;
				map->view_changed = true;
			}

			InputServer::GetInstance()->FixMouse(true);
			map->catched = true;
		}
	}

	// zooming
	else if (action == IN_ACTION_ZOOM_MAP_IN)
		map->ChangeView(CD_ZOOM_IN);

	else if (action == IN_ACTION_ZOOM_MAP_OUT)
		map->ChangeView(CD_ZOOM_OUT);

	else if (action == IN_ACTION_ZOOM_MAP)
	{
		float diff = 1.0f + (MAP_MAX_ZOOM_SPEED - 1.0f) * map->zoom_speed * abs(position) * 0.1f;
		if (diff != 1.0f)
		{
			map->zoom = position > 0 ? map->zoom / diff : map->zoom * diff;
			map->view_changed = true;
		}
		return true;
	}

	else if (action == IN_ACTION_RESET_MAP_ZOOM)
		map->ChangeView(CD_ZOOM_RESET);

	else if (action == IN_ACTION_SHOW_UNDERGROUND)
		map->SetVisibleSegment(MapEnums::SEG_UNDERGROUND);

	else if (action == IN_ACTION_SHOW_GROUND)
		map->SetVisibleSegment(MapEnums::SEG_GROUND);

	else if (action == IN_ACTION_SHOW_SKY)
		map->SetVisibleSegment(MapEnums::SEG_SKY);

	else if (action == IN_ACTION_SHOW_MULTIPLE)
		map->SetVisibleSegment(MapEnums::SEG_COUNT);

	else
		return false;

	return true;
}


bool Map::IsFree(const MapPosition3D &position, const MapSize &size) const
{
	if (!this->IsInMap(position, size))
	{
		LogError("Cannot build there");
		return false;
	}

	// check fields
	Assert(position.segment < MapEnums::SEG_COUNT);
	const Segment *segment = this->segments + position.segment;

	for (mapel_t i = 0; i < size.width; i++)
	{
		for (mapel_t j = 0; j < size.height; j++)
		{
			if (segment->fields[position.x + i][position.y + j].unit ||
				segment->fields[position.x + i][position.y + j].ghost)
			{
				LogError("Position is engaged");
				return false;
			}
		}
	}

	return true;
}


/*
	Finds unit under given screen position and segment.

	@return Counted pointer to unit.
*/
Unit *Map::FindPickUnit(MapEnums::Segment segment, ushort_t screen_x, ushort_t screen_y) const
{
	static Unit *result;
	static vector2 mouse_map;
	this->ScreenToMap(segment, vector2(screen_x, screen_y), mouse_map);

	if (mouse_map.x < 0 || mouse_map.x > mapel_t(-1) || mouse_map.y < 0 || mouse_map.y >= mapel_t(-1))
		return NULL;

	// sets start position and direction
	static MapPosition2D act_position;
	static int steps;
	static bool left;
	
	act_position.x = (mapel_t)mouse_map.x;
	act_position.y = (mapel_t)mouse_map.y;
	left = ((mouse_map.y - (ushort_t)mouse_map.y) > (mouse_map.x - (ushort_t)mouse_map.x));

	// finds lowest position in the map from mouse position restrict to max. height of texture
	steps = n_min(act_position.x, act_position.y);
	if (steps > MAX_TEXTURE_MAPELS)
		steps = MAX_TEXTURE_MAPELS;
	steps = (steps + 1) * 2;

	// go down from actual position and collect all units
	static vector<Unit *>units;
	static vector<Unit *>::reverse_iterator it_unit;
	static Unit *act_unit, *last_unit;

	units.clear();
	act_unit = last_unit = NULL;
	
	Counted::LockCounted();
	do
	{
		if (this->IsInMap(act_position))
		{
			const Map::Field &field = this->segments[segment].fields[act_position.x][act_position.y];
			// unit is found
			if (
				((((act_unit = field.unit)) && act_unit->IsFlags(Unit::FLAG_VISIBLE)) || ((act_unit = field.ghost)))
				&& (act_unit != last_unit)
				&& act_unit->AcquirePointer()
			)
			{
				units.push_back(act_unit);
				last_unit = act_unit;
			}
		}

		// compute next step
		if ((left && !act_position.x) || (!left && !act_position.y))
			steps = 0;
		else
			steps--;

		// go to next mapel
		if (steps)
		{
			if (left)
				act_position.x--;
			else
				act_position.y--;

			left = !left;
		}
	} while (steps);

	Counted::UnlockCounted();

	result = NULL;

	// if we have some units, tests selection
	if (units.size())
	{
		GfxServer *gfx_server = GfxServer::GetInstance();

		// render a white quad under position
		static vector2 coords[4] = { vector2(-2.0f, -2.0f), vector2(-2.0f, 2.0f), vector2(2.0f, 2.0f), vector2(2.0f, -2.0f) };
		static byte_t pixel[3] = { 0, 0, 0 };

		gfx_server->PushScreenCoordinates();
		gfx_server->ResetMatrix();
		gfx_server->SetDepthTest(false);
		gfx_server->SetColor(vector3(1.0f, 1.0f, 1.0f));
		gfx_server->Translate(vector2(screen_x, float(-screen_y)));
		gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
		gfx_server->PopScreenCoordinates();
		gfx_server->SetColorFilter(0.0f, 0.0f, 0.0f);

		// draw units and test color under position
		for (it_unit = units.rbegin(); it_unit != units.rend() && !result; it_unit++)
		{
			// draws black silhouette of unit
			this->segments[segment].units_scene->RenderObject((*it_unit)->GetSceneObject(), false);

			// read color from back buffer
			gfx_server->ReadPixels(screen_x, this->viewport.height - screen_y, 1, 1, pixel);

			// if white quad was redrawn by unit, we are finished
			if (pixel[0] < 230)
				result = (Unit *)(*it_unit)->AcquirePointer();
		}

		gfx_server->SetColorFilter(1.0f, 1.0f, 1.0f);

		// release all counted pointers
		vector<Unit *>::iterator itUnit;
		for (itUnit = units.begin(); itUnit != units.end(); itUnit++)
			(*itUnit)->Release();
	}

	// check whether there is a base of not-moveable unit under cursor
	if (!result && mouse_map.x < this->size.width && mouse_map.y < this->size.height)
	{
				Counted::LockCounted();
		const Map::Field &field = this->segments[segment].fields[int(mouse_map.x)][int(mouse_map.y)];

		if (
			((((act_unit = field.unit)) && act_unit->IsFlags(Unit::FLAG_VISIBLE)) || ((act_unit = field.ghost)))
			&& act_unit->AcquirePointer()
		)
			result = act_unit;

		Counted::UnlockCounted();
	}

	return result;
}


/*
	Finds all units in given rectangle. All segments are serched if multiple segment view is active.

	@param corner1 Position of one corner in screen coordinates.
	@param corner2 Position of oposite corner in screen coordinates.
	@param units List of counted pointers to units. Will be filled with result.

	@return @c True if at least one unit has been found.
*/
bool Map::FindUnitsInRectangle(const vector2 &corner1, const vector2 &corner2, list<Unit *> &units) const
{
	vector2 ccw1, ccw2;                               // corners in screen coordinates
	vector2 rect[4];                                  // rectangle in map coordinates [mapels]
	float left, right, top, bottom;                   // full envelope of rectangle [mapels]
	mapel_t env_x1, env_x2, env_y2, env_y1;           // final envelope restricted to map size [mapels]

	mapel_t i, j;
	float unit_x, unit_y;
	Unit *unit;

	// converts mouse selection to clock-wise direction
	if (((corner2.x > corner1.x && corner2.y < corner1.y)) || ((corner2.x < corner1.x && corner2.y > corner1.y)))
	{
		ccw1 = corner1;
		ccw2 = corner2;
	}
	else
	{
		ccw1.set(corner1.x, corner2.y);
		ccw2.set(corner2.x, corner1.y);
	}

	for (byte_t seg = 0; seg < MapEnums::SEG_COUNT; seg++)
	{
		if (this->visible_segment != MapEnums::SEG_COUNT && seg != this->visible_segment)
			continue;

		// converts mouse selection into map axis rectangle
		this->ScreenToMap(MapEnums::Segment(seg), ccw1, rect[0]);
		this->ScreenToMap(MapEnums::Segment(seg), vector2(ccw1.x, ccw2.y), rect[1]);
		this->ScreenToMap(MapEnums::Segment(seg), ccw2, rect[2]);
		this->ScreenToMap(MapEnums::Segment(seg), vector2(ccw2.x, ccw1.y), rect[3]);

		// finds envelope
		left = right = rect[0].x;
		top = bottom = rect[0].y;

		for (int k = 1; k < 4; k++)
		{
			if (rect[k].x < left)    left = rect[k].x;
			if (rect[k].x > right)   right = rect[k].x;
			if (rect[k].y < bottom)  bottom = rect[k].y;
			if (rect[k].y > top)     top = rect[k].y;
		}

		// check if selection interfere with map
		if ((right <= 0.0f) || (left >= this->size.width) || (top <= 0.0f) || (bottom >= this->size.height))
			continue;

		// restricts envelope to the map
		env_x1 = mapel_t(n_max(0.0f, left));
		env_y1 = mapel_t(n_max(0.0f, bottom));
		env_x2 = mapel_t(n_min(this->size.width, right + 0.5f));
		env_y2 = mapel_t(n_min(this->size.height, top + 0.5f));

		Counted::LockCounted();

		// goes through whole envelope and tests units
		for (j = env_y1; j < env_y2; j++)
		{
			for (i = env_x1; i < env_x2; i++)
			{
				unit = this->segments[seg].fields[i][j].unit;

				// unit must by my, not selected and moveable
				if (unit && unit->IsMy() && unit->GetPrototype()->CanMove() && !unit->IsFlags(Unit::FLAG_PROCESSED)) 
				{
					unit_x = (float)unit->GetRealPosition().x + unit->GetPrototype()->GetSize().width / 2.0f;
					unit_y = (float)unit->GetRealPosition().y + unit->GetPrototype()->GetSize().height / 2.0f;

					// chek if unit is in selection rectangle
					if (((rect[1].x - rect[0].x) * (unit_y - rect[0].y) <= (rect[1].y - rect[0].y) * (unit_x - rect[0].x)) &&
					((rect[2].x - rect[1].x) * (unit_y - rect[1].y) <= (rect[2].y - rect[1].y) * (unit_x - rect[1].x)) &&
					((rect[3].x - rect[2].x) * (unit_y - rect[2].y) <= (rect[3].y - rect[2].y) * (unit_x - rect[2].x)) &&
					((rect[0].x - rect[3].x) * (unit_y - rect[3].y) <= (rect[0].y - rect[3].y) * (unit_x - rect[3].x)))
					{
						// get counted pointer and add unit to result list
						if (unit->AcquirePointer())
						{
							units.push_back(unit);
							unit->SetFlags(Unit::FLAG_PROCESSED);
						}
					}
				}
			}
		}

		Counted::UnlockCounted();
	}

	// clear processed flags
	list<Unit *>::iterator it;
	for (it = units.begin(); it != units.end(); it++)
		(*it)->ClearFlags(Unit::FLAG_PROCESSED);

	return !units.empty();
}


//=========================================================================
// Deserializing
//=========================================================================

bool Map::ReadHeader(Serializer &serializer)
{
	// read	name [optional]
	serializer.GetAttribute("name", this->name);

	// read	author [optional]
	if (serializer.GetGroup("author"))
	{
		serializer.GetAttribute("name", this->author_name);
		serializer.GetAttribute("contact", this->author_contact);
		serializer.EndGroup();
	}

	// read size [mandatory]
	if (
		!serializer.GetGroup("size") ||
		!serializer.GetAttribute("width", this->size.width) || 
		!serializer.GetAttribute("height", this->size.height)
	)
	{
		LogError1("Missing map size in: %s", this->file_name.c_str());
		return false;
	}
	serializer.EndGroup();

	// check map size
	if (this->size.width > MAP_MAX_SIZE || this->size.height > MAP_MAX_SIZE)
	{
		LogError1("Map size is too large in: %s", this->file_name.c_str());
		return false;
	}

	// read scheme [mandatory]
	string scheme_id;
	if (
		!serializer.GetGroup("scheme") ||
		!serializer.GetAttribute("id", scheme_id, false)
	)
	{
		LogError1("Missing map scheme in: %s", this->file_name.c_str());
		return false;
	}
	serializer.EndGroup();
	
	// find scheme [mandatory]
	this->scheme = Game::GetInstance()->GetScheme(scheme_id);
	if (!this->scheme.IsValid())
	{
		LogError1("Map scheme does not exists: %s", scheme_id.c_str());
		return false;
	}

	return true;
}


/**
*/
bool Map::ReadBody(Serializer &serializer)
{
	// preload whole scheme
	if (!this->scheme->Load())
	{
		LogError1("Error loading map scheme in: %s", this->file_name.c_str());
		return false;
	}

	// create 2D surface
	this->surface = Create2DArray<byte_t>(this->size.width, this->size.height);
	if (!this->surface)
	{
		LogError1("Error creating map surface in: %s", this->file_name.c_str());
		return false;
	}

	// initialize all segments
	for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++)
	{
		// create scenes
		this->segments[i].terrain_scene = SceneServer::GetInstance()->NewScene("IsoScene");
		this->segments[i].units_scene = SceneServer::GetInstance()->NewScene("IsoScene");
		if (!this->segments[i].terrain_scene || !this->segments[i].units_scene)
		{
			LogError("Error creating ISO scene");
			return false;
		}

		this->segments[i].terrain_scene->SetSorting(false);
		((IsoScene *)this->segments[i].terrain_scene)->SetCoefs(ISO_SCENE_H_COEF, ISO_SCENE_V_COEF);
		((IsoScene *)this->segments[i].units_scene)->SetCoefs(ISO_SCENE_H_COEF, ISO_SCENE_V_COEF);

		// create field array
		this->segments[i].fields = Create2DArray<Field>(this->size.width, this->size.height);
		Assert(this->segments[i].fields);
	}

	// read	fragments [mandatory]
	if (!this->ReadFragments(serializer))
	{
		LogError1("Error reading map fragments in: %s", this->file_name.c_str());
		return false;
	}

	return true;
}


bool Map::ReadFragments(Serializer &serializer)
{
	ushort_t count;
	ushort_t i;

	// read	fragments [mandatory]
	if (!serializer.GetGroup("fragments"))
	{
		LogError1("Missing fragments in: %s", this->file_name.c_str());
		return false;
	}

	count = serializer.GetGroupsCount();
	if (!count)
	{
		LogError1("Missing fragments in: %s", this->file_name.c_str());
		return false;
	}

	// create fragments array
	this->fragments.reserve(count);

	i = 0;
	if (serializer.GetGroup() && serializer.CheckGroupName("fragment"))
	{
		do {
			this->fragments.push_back(Fragment());

			// read	fragment [mandatory]
			if (!this->ReadFragment(serializer, this->fragments[i]))
			{
				LogError2("Error reading fragment %hu in: %s", i, this->file_name.c_str());
				return false;
			}

			i++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (i < count)
	{
		LogError1("Fragments mismatch in: %s", this->file_name.c_str());
		return false;
	}

	serializer.EndGroup();
	return true;
}


bool Map::ReadFragment(Serializer &serializer, Fragment &fragment)
{
	// read	id [mandatory]
	string fragment_id;
	if (!serializer.GetAttribute("id", fragment_id, false))
	{
		LogError1("Missing fragment identifier in: %s", this->file_name.c_str());
		return false;
	}

	// find fragment [mandatory]
	fragment.ref = this->scheme->GetFragment(fragment_id);
	if (!fragment.ref)
	{
		LogError1("Map fragment does not exists in: %s", this->file_name.c_str());
		return false;
	}

	// read	position [mandatory]
	MapPosition2D fragment_position;

	if (!serializer.GetAttribute("x", fragment_position.x) || !serializer.GetAttribute("y", fragment_position.y))
	{
		LogError1("Missing fragment position in: %s", this->file_name.c_str());
		return false;
	}
	if (fragment_position.x >= this->size.width || fragment_position.y >= this->size.height)
	{
		LogError1("Wrong fragment position in: %s", this->file_name.c_str());
		return false;
	}
	fragment.position.x = fragment_position.x;
	fragment.position.y = fragment_position.y;

	// check position
	if (!this->IsInMap(fragment_position, fragment.ref->size))
	{
		LogError1("Fragment position outside of map in: %s", this->file_name.c_str());
		return false;
	}

	// update segment surface
	if (!this->UpdateSurface(fragment_position, *fragment.ref))
	{
		LogError1("Fragments overlapping in: %s", this->file_name.c_str());
		return false;
	}

	// create scene object for fragment
	for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++)
	{
		if (this->scheme->HasModels(*fragment.ref, MapEnums::Segment(i)))
		{
			fragment.scene_objects[i] = SceneServer::GetInstance()->NewSceneObject("SceneObject", &this->scheme->GetRandomModel(*fragment.ref, MapEnums::Segment(i)));
			if (!fragment.scene_objects[i] || !fragment.scene_objects[i]->LoadResources())
			{
				LogError1("Error creating scene object of fragment from: %s", this->file_name.c_str());
				return false;
			}
			fragment.scene_objects[i]->SetPosition(fragment.position);
			this->segments[i].terrain_scene->AddObject(fragment.scene_objects[i]);
		}
	}

	return true;
}


//=========================================================================
// Serializing
//=========================================================================

bool Map::WriteHeader(Serializer &serializer)
{
	// write name
	if (!serializer.SetAttribute("name", this->name)) return false;

	// write author
	if (!serializer.AddGroup("author")) return false;
		if (!serializer.SetAttribute("name", this->author_name)) return false;
		if (!serializer.SetAttribute("contact", this->author_contact)) return false;
	serializer.EndGroup();

	/// @todo Implement
	LogError("Serializing is not implemented");
	return true;
}


bool Map::WriteBody(Serializer &serializer)
{
	/// @todo Implement
	LogError("Serializing is not implemented");
	return true;
}


// vim:ts=4:sw=4:
