#ifndef __race_h__
#define __race_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file race.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/preloaded.h"
#include "kernel/ref.h"

class Scheme;
class Prototype;


//=========================================================================
// Race
//=========================================================================

/**
	@class Race
	@ingroup Engine_Module
*/
    
class Race : public Preloaded
{
//--- methods
public:
	Race(const char *id);
	virtual ~Race();

	Scheme *GetScheme() const;
	Prototype *GetPrototype(const string &prototype_id) const;
	const string &GetMeetingModel() const;

protected:
	virtual void ClearHeader();
	virtual void ClearBody();

	virtual bool LoadResources();
	virtual void UnloadResources();

	// serializing
	virtual bool ReadHeader(Serializer &serializer);
	virtual bool ReadBody(Serializer &serializer);
	virtual bool WriteHeader(Serializer &serializer);
	virtual bool WriteBody(Serializer &serializer);

	bool ReadMeetingPoint(Serializer &serializer);


//--- variables
protected:
	// race header
	string name;                  ///< Full race name.
	string author_name;
	string author_contact;

	// race body
	Ref<Root> prototypes;
	Ref<Scheme> scheme;
	string meeting_model;         ///< File name of model of meeting point.
};


//=========================================================================
// Mathods
//=========================================================================

inline
Scheme *Race::GetScheme() const
{
	return this->scheme.Get();
}


inline
Prototype *Race::GetPrototype(const string &prototype_id) const
{
	if (!this->prototypes.IsValid())
		return NULL;

	return (Prototype *)this->prototypes->Find(prototype_id);
}


inline
const string &Race::GetMeetingModel() const
{
	return this->meeting_model;
}


#endif // __race_h__

// vim:ts=4:sw=4:
