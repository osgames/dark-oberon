#ifndef __engine_system_h__
#define __engine_system_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file engine/system.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2005

	@brief Common includes and defines.

	@version 1.0 - Initial.
*/


//=========================================================================
// Common header files
//=========================================================================

#include "framework/system.h"


//=========================================================================
// Common definitions
//=========================================================================

// NOH paths
#define NOH_GAME              string("/game")
#define NOH_PROFILES          (NOH_GAME + "/profiles")
#define NOH_GUIS              (NOH_GAME + "/guis")
#define NOH_LEVELS            (NOH_GAME + "/levels")
#define NOH_MAPS              (NOH_GAME + "/maps")
#define NOH_SCHEMES           (NOH_GAME + "/schemes")
#define NOH_RACES             (NOH_GAME + "/races")
#define NOH_SEL_RENDERERS     (NOH_GAME + "/selrenderers")
#define NOH_ON_SCREEN_TEXT    (NOH_GAME + "/ost")
#define NOH_SELECTION         (NOH_GAME + "/selection")

#define NOH_FILE_SERVER       (NOH_SERVERS + "/file")
#define NOH_SCRIPT_SERVER     (NOH_SERVERS + "/script")

// system
#define PROJECTION_WIDTH      1280.0f
#define PROJECTION_HEIGHT     960.0f
#define ISO_SCENE_H_COEF      20.0f
#define ISO_SCENE_V_COEF      10.0f
#define MAX_TEXTURE_MAPELS    (mapel_t(256.0f / (ISO_SCENE_V_COEF * 2)))  ///< Max. size of texture in mapels. [mapels]
#define SEGMENTS_SHIFT        3         ///< [mapels]
#define WARFOG_BOUNDARY       10        ///< Additional warfog boundary. Necessary to cover buildings at the edge of the map. Must be higher or equal than #SEGMENTS_SHIFT. [mapels]

// colors
#define COLOR_DEBUG           0.3f, 0.3f, 0.3f
#define COLOR_INFO            0.7f, 0.7f, 0.7f
#define COLOR_WARNING         1.0f, 0.5f, 0.0f
#define COLOR_ERROR           1.0f, 0.1f, 0.0f
#define COLOR_CRITICAL        1.0f, 0.1f, 0.0f

#define COLOR_MY_UNIT         0.5f, 0.9f, 0.5f
#define COLOR_ENEMY_UNIT      1.0f, 0.5f, 0.5f
#define COLOR_HYPER_UNIT      0.8f, 0.8f, 0.5f

#define COLOR_LIFE1           0.0f, 1.0f, 0.0f
#define COLOR_LIFE2           1.0f, 1.0f, 0.0f
#define COLOR_LIFE3           1.0f, 0.0f, 0.0f
#define COLOR_HIDING          1.0f, 1.0f, 1.0f
#define COLOR_FILTER_GHOST    0.6f, 0.6f, 0.6f, 1.0f

#define LIFE_LIMIT1           0.7f                 ///< Limit for COLOR_LIFE1.
#define LIFE_LIMIT2           0.3f                 ///< Limit for COLOR_LIFE2.


//=========================================================================
// Simple types
//=========================================================================

typedef byte_t mapel_t;


#endif // __engine_system_h__

// vim:ts=2:sw=2:et:
