#ifndef __playermap_h__
#define __playermap_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file playermap.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/warfog.h"
#include "engine/mapstructures.h"
#include "kernel/vector2.h"

class Unit;


//=========================================================================
// PlayerMap
//=========================================================================

/**
	@class PlayerMap
	@ingroup Engine_Module
*/
    
class PlayerMap
{
//--- variables
protected:
	MapSize map_size;
	MapSize warfog_size;
	byte_t **warfog_field[MapEnums::SEG_COUNT];   ///< 2D array with warfog value to each filed in the map + boundaries.
	Warfog *warfog[MapEnums::SEG_COUNT];          ///< Stores dynamic textures of warfog.	


//--- methods
public:
	PlayerMap(const MapSize &size, bool warfog_texture);
	virtual ~PlayerMap();

	bool IsInMap(const vector2 &map_position, MapEnums::Segment &segment) const;
	bool IsAreaVisible(const MapPosition3D &position, const MapSize &size) const;

	// warfog
	bool UpdateWarfog(const Unit *unit, bool added);
	void RenderWarfog(MapEnums::Segment segment, const MapArea &visible_area);
	void RenderMask(MapEnums::Segment segment, const MapArea &visible_area);
};


//=========================================================================
// Methods
//=========================================================================

/**
	@todo Test and return other visible segments according to warfog.
*/
inline
bool PlayerMap::IsInMap(const vector2 &map_position, MapEnums::Segment &segment) const
{
	if (map_position.x > 0 && map_position.x < this->map_size.width && map_position.y > 0 && map_position.y < this->map_size.height)
	{
		segment = MapEnums::SEG_GROUND;
		return true;
	}

	return false;
}


/**
	Checks if at least one field of area is visible.
*/
inline
bool PlayerMap::IsAreaVisible(const MapPosition3D &position, const MapSize &size) const
{
	Assert(position.x + size.width <= this->map_size.width);
	Assert(position.y + size.height <= this->map_size.height);
	Assert(position.segment < MapEnums::SEG_COUNT);

	// get appropriate segment
	byte_t **segment_field = this->warfog_field[position.segment == MapEnums::SEG_UNDERGROUND ? MapEnums::SEG_UNDERGROUND : MapEnums::SEG_GROUND];
	Assert(segment_field);

	// recalculate map position to warfog position
	MapPosition3D warfog_position = position;
	warfog_position.x += SEGMENTS_SHIFT + 1;
	warfog_position.y += SEGMENTS_SHIFT + 1;

	// try to find visible field
	mapel_t x, y;
	byte_t field;

	for (y = warfog_position.y; y < warfog_position.y + size.width; y++)
	{
		for (x = warfog_position.x; x < warfog_position.x + size.width; x++)
		{
			field = segment_field[x][y];
			if (field != Warfog::WARFOG_HIDED && field != Warfog::WARFOG_UNKNOWN)
				return true;
		}
	}

	return false;
}


#endif // __playermap_h__

// vim:ts=4:sw=4:
