//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file map_cmds.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/map.h"


//=========================================================================
// Commands
//=========================================================================

static void s_SetWarfogEnabled(void *slf, Cmd *cmd)
{
	Map *self = (Map *)slf;
	self->SetWarfogEnabled(cmd->In()->GetB());
}


static void s_IsWarfogEnabled(void *slf, Cmd *cmd)
{
	Map *self = (Map *)slf;
	cmd->Out()->SetB(self->IsWarfogEnabled());
}


void s_InitMap_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("v_SetWarfogEnabled_b",      'SFOG', s_SetWarfogEnabled);
	cl->AddCmd("b_IsWarfogEnabled_v",       'IFOG', s_IsWarfogEnabled);

	cl->EndCmds();
}

// vim:ts=4:sw=4:
