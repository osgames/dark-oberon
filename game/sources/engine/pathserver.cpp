//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file pathserver.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007, 2008

	@version 1.0 - Initial.
*/


//=========================================================================
// Defines
//=========================================================================

#define PATH_THREADS_COUNT 3


//=========================================================================
// Includes
//=========================================================================

#include "engine/pathserver.h"
#include "engine/unit.h"
#include "kernel/kernelserver.h"
#include "kernel/messageserver.h"
#include "kernel/timeserver.h"
#include "kernel/threadpool.h"

PrepareClass(PathServer, "Root", s_NewPathServer, s_InitPathServer);


//=========================================================================
// PathServer
//=========================================================================

PathServer::PathServer(const char *id) :
	RootServer<PathServer>(id),
	threads(NULL)
{
#ifdef USE_PROFILERS
	// create profiler
	if (TimeServer::GetInstance())
	{
		this->path_profiler = TimeServer::GetInstance()->NewProfiler(string("% pathfinding"));
		Assert(this->path_profiler);
	}
#endif
}


PathServer::~PathServer()
{
	Assert(!this->threads);
}


bool PathServer::Init()
{
	this->Lock();
	Assert(!this->threads);

	// create thread pool
	this->threads = NEW ThreadPool(PATH_THREADS_COUNT);
	Assert(this->threads);

	this->Unlock();
	return true;
}


void PathServer::Done()
{
	this->Lock();
	Assert(this->threads);

	// stop all threads and destroy them, pending queries will be lost
	this->threads->Stop(false);
	SafeDelete(this->threads);

	this->Unlock();
}


void PathServer::OnMessage(PathRequest *request)
{
	Assert(request);
	Assert(request->dest_area.width && request->dest_area.height);
	Assert(request->dest_segment < MapEnums::SEG_COUNT);

	// lock to prevent deleing threads in Done() method while processing the message
	this->Lock();

	if (!this->threads)
	{
		this->Unlock();
		return;
	}

	// check if sender still exists
	if (!request->GetSender())
		return;

	// create and fill new path query
	PathServer::PathQuery *query = NEW PathServer::PathQuery();

	Counted::LockCounted();
	query->sender = request->GetSender();
	query->unit = request->unit;
	Counted::UnlockCounted();

	query->request_id = request->GetRequestId();
	query->path->dest_area = request->dest_area;
	query->path->dest_segment = request->dest_segment;
	query->path->dest_unit = request->dest_unit;

	// run query in separate thread
	if (!this->threads->Run(this->ProcessQuery, NULL, query, this->CancelQuery, true))
		delete query;

	this->Unlock();
}


void PathServer::ProcessQuery(Thread *thread)
{
	PathServer::PathQuery *query = (PathQuery *)thread->GetData();
	Assert(query);

	// find path using specific algorithm
	PathServer::GetInstance()->FindPath(*query);

	// if sender is still alive, be sure that it will not die while sending message
	Root *sender = NULL;
	Counted::LockCounted();
	if (query->sender.IsValid())
		sender = (Root *)query->sender->AcquirePointer();
	Counted::UnlockCounted();

	// send message with path to the sender
	if (sender)
	{
		// create response with path
		PathResponse *response = NEW PathResponse(query->request_id);
		response->unit = query->unit;
		response->path = query->path;
		query->path = NULL;

		// send back to sender
		PathServer::GetInstance()->SendMessage(response, sender);

		// we dont need sender anymore
		sender->Release();
	}

	// path will be deleted with query if unit didn't get it
	delete query;
}


void PathServer::CancelQuery(void *data)
{
	Assert(data);

	PathServer::PathQuery *query = (PathQuery *)data;
	delete query;
}


void PathServer::FindPath(const PathQuery &query)
{
	ErrorMsg("Overwrite this method in subclass");
}


// vim:ts=4:sw=4:
