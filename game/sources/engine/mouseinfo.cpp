//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS this.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file mouseinfo.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/mouseinfo.h"
#include "engine/unit.h"
#include "engine/prototype.h"
#include "engine/map.h"
#include "engine/playermap.h"
#include "engine/selection.h"
#include "framework/inputserver.h"
#include "framework/gfxserver.h"


//=========================================================================
// Global variables
//=========================================================================

const string MouseInfo::cursor_name[CURSOR_COUNT] = 
{
	"arrow",
	"select",
	"add-remove",
	"can-move",
	"can-attack",
	"can-mine",
	"can-repair",
	"can-build",
	"cant-move",
	"cant-attack",
	"cant-mine",
	"cant-repair",
	"cant-build",
	"point",
	"hide",
	"eject"
};


//=========================================================================
// MouseInfo
//=========================================================================

MouseInfo::MouseInfo() :
	cursor_type(CURSOR_ARROW),
	pick_unit(NULL),
	rect_active(false),
	rect_select(false)
{
	this->Reset();
}


void MouseInfo::Reset()
{
	this->rect_corner.set(0.0f, 0.0f);
	this->rect_select = false;
	this->rect_active = false;

	SafeRelease(this->pick_unit);

	for (int i = 0; i < CURSOR_COUNT; i++)
		this->cursor_id[i] = CURSOR_ARROW;
}


void MouseInfo::AddCursor(CursorType type, const string &model_file)
{
	Assert(type < CURSOR_COUNT);
	Assert(GfxServer::GetInstance());

	this->cursor_id[type] = GfxServer::GetInstance()->AddCursor(model_file);
}


void MouseInfo::SetCursor(CursorType type)
{
	Assert(type < CURSOR_COUNT);
	Assert(GfxServer::GetInstance());

	this->cursor_type = type;
	GfxServer::GetInstance()->SetCursor(this->cursor_id[type]);
}


//=========================================================================
// Per-frame updating / rendering
//=========================================================================

void MouseInfo::UpdatePickUnit(const Map *map)
{
	Assert(map);

	Unit *unit = NULL;

	// find new pick unit
	if (!this->rect_select)
	{
		// get mouse position
		static ushort_t mouse_x, mouse_y;
		InputServer::GetInstance()->GetMousePosition(mouse_x, mouse_y);

		// get unit under cursor
		unit = map->FindPickUnit(mouse_x, mouse_y);

		// set flag
		if (unit && unit != this->pick_unit)
		{
			unit->SetFlags(Unit::FLAG_UNDER_CURSOR);

			// show meeting point if unit is not selected
			if (unit->IsMy() && !unit->IsFlags(Unit::FLAG_SELECTED))
				unit->ShowMeetingPoint(true);
		}
	}

	// unset old pick unit
	if (this->pick_unit && this->pick_unit != unit)
	{
		// hide meeting point if unit is not selected
		if (this->pick_unit->IsMy() && !this->pick_unit->IsFlags(Unit::FLAG_SELECTED))
			this->pick_unit->ShowMeetingPoint(false);

		// set flag
		this->pick_unit->ClearFlags(Unit::FLAG_UNDER_CURSOR);
	}

	SafeRelease(this->pick_unit);

	// set new pick unit
	this->pick_unit = unit;
}


void MouseInfo::UpdateCursor(const Map *map, const PlayerMap *player_map, const Selection *selection)
{
	CursorType cursor = MouseInfo::CURSOR_ARROW;

	// get mouse position
	static ushort_t mouse_x, mouse_y;
	InputServer::GetInstance()->GetMousePosition(mouse_x, mouse_y);

	// convert mouse to map position
	vector2 map_position;
	map->ScreenToMap(MapEnums::SEG_GROUND, vector2(mouse_x, mouse_y), map_position);

	// set new map position
	MapEnums::Segment segment;
	bool in_map = player_map->IsInMap(map_position, segment);
	if (in_map)
		this->map_position.Set(mapel_t(map_position.x), mapel_t(map_position.y), segment);

	// selection rectangle is drawn
	if (this->rect_select)
		cursor = InputServer::GetInstance()->IsKeyPressed(KEY_LCTRL) ? MouseInfo::CURSOR_ADD_REMOVE : MouseInfo::CURSOR_SELECT;

	// mouse cursor is over unit (my/enemy/ghost)
	else if (this->pick_unit)
	{
		switch (selection->GetSelectedAction())
		{
		case Unit::ACTION_MOVE:
			cursor = selection->CanHideToUnit(this->pick_unit) ? MouseInfo::CURSOR_HIDE : MouseInfo::CURSOR_CAN_MOVE;
			break;

		case Unit::ACTION_ATTACK:
			/*cursor = selection->CanAttackUnit(this->pick_unit) ? MouseInfo::CURSOR_CAN_ATTACK : MouseInfo::CURSOR_CANT_ATTACK;*/
			break;

		case Unit::ACTION_MINE:
			/*if (this->pick_unit->IsSource() && selection->CanMineInUnit(this->pick_unit))
				cursor = MouseInfo::CURSOR_CAN_MINE;
			else if (this->pick_unit->IsBuilding() && selection->CanUnloadToUnit(this->pick_unit))
				cursor = MouseInfo::CURSOR_CAN_MINE;
			else
				cursor = MouseInfo::CURSOR_CANT_ATTACK;*/
			break;

		case Unit::ACTION_REPAIR:
			/*cursor = selection->CanRepairUnit(this->pick_unit) ? MouseInfo::CURSOR_CAN_REPAIR : MouseInfo::CURSOR_CANT_REPAIR;*/
			break;

		case Unit::ACTION_STAY:
		case Unit::ACTION_NONE:
			// CTRL key is pressed
			if (InputServer::GetInstance()->IsKeyPressed(KEY_LCTRL) && !this->pick_unit->TestState(Unit::STATE_GHOST))
				cursor = MouseInfo::CURSOR_ADD_REMOVE;

		//// if units in selection are not my (or selection is empty)
		//else if (!selection->IsMy() || selection->IsEmpty())
		//	cursor = this->pick_unit->TestState(STATE_GHOST) ?  MouseInfo::CURSOR_ARROW : MouseInfo::CURSOR_SELECT;
		//
		//	// mining
		//	if (this->pick_unit->IsSource() && selection->CanMineInUnit(this->pick_unit)
		//		cursor = MouseInfo::CURSOR_CAN_MINE;

		//	// unloading
		//	else if (this->pick_unit->IsBuilding() && selection->CanUnloadToUnit(this->pick_unit)
		//		cursor = MouseInfo::CURSOR_CAN_MINE;

		//	// repairing
		//	else if (selection->CanRepairUnit(this->pick_unit)
		//		cursor = MouseInfo::CURSOR_CAN_REPAIR;

			// hiding
			else if (selection->CanHideToUnit(this->pick_unit))
				cursor = MouseInfo::CURSOR_HIDE;

			//// ejecting
			else if (this->pick_unit->IsFlags(Unit::FLAG_SELECTED) && this->pick_unit->CanEject())
				cursor = MouseInfo::CURSOR_EJECT;

		//	// attacking
		//	else if (!this->pick_unit->GetPlayer()->IsMe() && this->pick_unit->GetPlayer()->IsHyper() &&
		//		selection->CanAttackUnit(this->pick_unit)
		//	)
		//		cursor = MouseInfo::CURSOR_CAN_ATTACK;

			// moving
			else if (this->pick_unit->TestState(Unit::STATE_GHOST))
				cursor = selection->CanMove() ? MouseInfo::CURSOR_CAN_MOVE : MouseInfo::CURSOR_ARROW;

			// none special action could by done
			else
				cursor = MouseInfo::CURSOR_SELECT;
			break;

		default:
			ErrorMsg("Unhandled action");
			break;
		}
	}

	// mouse cursor is not over any unit
	else 
	{
		// mouse cursor is in the map
		if (in_map)
		{
			this->map_position.Set(mapel_t(map_position.x), mapel_t(map_position.y), segment);

			switch (selection->GetSelectedAction())
			{
			case Unit::ACTION_MOVE:    cursor = MouseInfo::CURSOR_CAN_MOVE; break;
			case Unit::ACTION_ATTACK:  cursor = MouseInfo::CURSOR_CANT_ATTACK; break;
			case Unit::ACTION_MINE:    cursor = MouseInfo::CURSOR_CANT_MINE; break;
			case Unit::ACTION_REPAIR:  cursor = MouseInfo::CURSOR_CANT_REPAIR; break;
			case Unit::ACTION_BUILD:
				//cursor = selection->CanBuildOnPosition(pos, segment) ? MouseInfo::CURSOR_CAN_BUILD : MouseInfo::CURSOR_CANT_BUILD;
				break;

			case Unit::ACTION_STAY:
			case Unit::ACTION_NONE:
				if (selection->CanMove())
					cursor = MouseInfo::CURSOR_CAN_MOVE;
				else
					cursor = selection->CanHide() ? MouseInfo::CURSOR_POINT : MouseInfo::CURSOR_ARROW;
				break;

			default:
				ErrorMsg("Unhandled action");
				break;
			}
		}

		// mouse cursor is not in the map
		else
		{
			switch (selection->GetSelectedAction())
			{
			case Unit::ACTION_MOVE:    cursor = MouseInfo::CURSOR_CANT_MOVE; break;
			case Unit::ACTION_ATTACK:  cursor = MouseInfo::CURSOR_CANT_ATTACK; break;
			case Unit::ACTION_MINE:    cursor = MouseInfo::CURSOR_CANT_MINE; break;
			case Unit::ACTION_REPAIR:  cursor = MouseInfo::CURSOR_CANT_REPAIR; break;
			case Unit::ACTION_BUILD:   cursor = MouseInfo::CURSOR_CANT_BUILD; break;
			case Unit::ACTION_STAY:
			case Unit::ACTION_NONE:    cursor = MouseInfo::CURSOR_ARROW; break;
			default:
				ErrorMsg("Unhandled action");
				break;
			}
		}
	}

	this->SetCursor(cursor);
}


void MouseInfo::RenderSelectionRectangle(const Map *map)
{
	static float RECT_VARIANCE = 7.0f;
	static vector2 corner_position;
	static ushort_t mouse_x;
	static ushort_t mouse_y;

	if (!this->rect_active)
		return;

	GfxServer *gfx_server = GfxServer::GetInstance();
	Assert(gfx_server);
	Assert(map);

	// update rectangle
	InputServer::GetInstance()->GetMousePosition(mouse_x, mouse_y);
	map->MapToScreen(MapEnums::SEG_GROUND, this->rect_corner, corner_position);

	// is rectangle large enough?
	this->rect_select = (abs(mouse_x - corner_position.x) > RECT_VARIANCE) || (abs(mouse_y - corner_position.y) > RECT_VARIANCE);
	if (!this->rect_select)
		return;

	// draw rectangle
	static vector2 coords[4];
	static float screen_height;

	screen_height = (float)gfx_server->GetDisplayMode().GetModeSize().height;
	coords[0].set(corner_position.x, -corner_position.y);
	coords[1].set(corner_position.x, -float(mouse_y));
	coords[2].set(mouse_x, -float(mouse_y));
	coords[3].set(mouse_x, -corner_position.y);

	gfx_server->SetColor(vector3(0.0f, 1.0f, 0.0f));
	gfx_server->PushScreenCoordinates();
	gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_LINE_LOOP, coords, 4);
	gfx_server->PopScreenCoordinates();
}


//=========================================================================
// Key callbacks
//=========================================================================

void MouseInfo::OnLeftKeyDown(const Map *map)
{
	Assert(InputServer::GetInstance());

	// get first corner of selection rectangle
	ushort_t down_x, down_y;
	InputServer::GetInstance()->GetMousePosition(down_x, down_y);

	map->ScreenToMap(MapEnums::SEG_GROUND, vector2(down_x, down_y), this->rect_corner);

	this->rect_active = true;
}


void MouseInfo::OnLeftKeyUp(const Map *map, Selection *selection)
{
	Assert(selection);
	Assert(InputServer::GetInstance());

	// rectangle selection
	if (this->rect_select)
	{
		// get second corner of selection rectangle
		vector2 down_pos, up_pos;
		ushort_t up_x, up_y;

		map->MapToScreen(MapEnums::SEG_GROUND, this->rect_corner, down_pos);
		InputServer::GetInstance()->GetMousePosition(up_x, up_y);
		up_pos.set(up_x, up_y);

		// find all units in selection rectangle
		list<Unit *> rect_units;
		map->FindUnitsInRectangle(down_pos, up_pos, rect_units);

		// clear selection if CTRL is not pressed
		if (!InputServer::GetInstance()->IsKeyPressed(KEY_LCTRL))
			selection->UnselectAll();

		// add all units to selection
		list<Unit *>::iterator it;
		for (it = rect_units.begin(); it != rect_units.end(); it++)
		{
			selection->AddUnit(*it);
			(*it)->Release();
		}
	}

	// single unit
	else
	{
		if (this->pick_unit && !this->pick_unit->TestState(Unit::STATE_GHOST))
		{
			if (InputServer::GetInstance()->IsKeyPressed(KEY_LCTRL))
				selection->AddRemoveUnit(this->pick_unit);
			else
				selection->SelectUnit(this->pick_unit);
		}
		else
			selection->UnselectAll();
	}

	// turn off rectangle selection
	this->rect_active = this->rect_select = false;
}


void MouseInfo::OnRightKeyDown(Selection *selection)
{
	Assert(selection);
	Assert(InputServer::GetInstance());

	ushort_t down_x, down_y;
	InputServer::GetInstance()->GetMousePosition(down_x, down_y);
	
	switch (this->cursor_type)
	{
	case CURSOR_SELECT:
	case CURSOR_CAN_MOVE:
		{
			if (this->pick_unit)
				selection->StartMoving(this->pick_unit);
			else
				selection->StartMoving(this->map_position);
		}
		break;

	case CURSOR_CANT_MOVE:
		selection->Say(false, "Cannot move outside of the map.");
		break;

	case CURSOR_CAN_ATTACK:
		//selection->StartAction(Unit::ACTION_ATTACK, this->pick_unit);
		break;

	case CURSOR_CANT_ATTACK:
		selection->Say(false, "Cannot attack there.");
		break;

	case CURSOR_CAN_MINE:
		//selection->StartAction(Unit::ACTION_MINE, this->pick_unit);
		break;

	case CURSOR_CANT_MINE:
		selection->Say(false, "Cannot mine there.");
		break;

	case CURSOR_CAN_REPAIR:
		//selection->StartAction(Unit::ACTION_REPAIR, this->pick_unit);
		break;

	case CURSOR_CANT_REPAIR:
		selection->Say(false, "Cannot repair there.");
		break;

	case CURSOR_CAN_BUILD:
		/// @todo Start building.
		break;

	case CURSOR_CANT_BUILD:
		selection->Say(false, "Cannot build there.");
		break;

	case CURSOR_POINT:
		selection->SetMeetingPoint(this->map_position);
		break;

	case CURSOR_HIDE:
		selection->StartHiding(this->pick_unit);
		break;

	case CURSOR_EJECT:
		selection->StartEjecting();
		break;

	case CURSOR_ARROW:
	case CURSOR_ADD_REMOVE:
		break;

	default:
		ErrorMsg("Unhandled cursor type");
		break;
    }
}

// vim:ts=4:sw=4:
