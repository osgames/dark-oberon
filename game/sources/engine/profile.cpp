//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file profile.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/profile.h"
#include "engine/inputactions.h"
#include "engine/selectionrenderer.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializer.h"

PrepareClass(Profile, "Preloaded", s_NewProfile, s_InitProfile);


//=========================================================================
// Profile
//=========================================================================

/**
	Constructor.
*/
Profile::Profile(const char *id) :
	Preloaded(id)
{
	// inicialize settings
	this->SetDefaultSettings();
}


/**
	Destructor.
*/
Profile::~Profile()
{
	this->ClearHeader();
	this->ClearBody();
}


void Profile::ClearHeader()
{
	//
}


void Profile::ClearBody()
{
	// input settings
	this->input.bind_map.clear();
}


void Profile::SetDefaultSettings()
{
	this->active = false;

	// player settings
	this->player.name = "Player";
	this->player.color.set(0.9f, 0.4f, 0.2f);

	// input settings
	this->input.sensitivity = 0.4f;
	this->input.bind_map.clear();
	this->input.bind_map[string("q:down")] = IN_ACTION_QUIT;
	this->input.bind_map[string("f11:down")] = IN_ACTION_SYSTEM_MOUSE;
	this->input.bind_map[string("f12:down")] = IN_ACTION_SCREENSHOT;
	this->input.bind_map[string("enter:down")] = IN_ACTION_FULLSCREEN;  ///< @todo Alt+
	this->input.bind_map[string("s:down")] = IN_ACTION_STAY;
	this->input.bind_map[string("m:down")] = IN_ACTION_MOVE;
	this->input.bind_map[string("a:down")] = IN_ACTION_ATTACK;
	this->input.bind_map[string("b:down")] = IN_ACTION_BUILD;
	this->input.bind_map[string("i:down")] = IN_ACTION_MINE;
	this->input.bind_map[string("r:down")] = IN_ACTION_REPAIR;
	this->input.bind_map[string("u:down")] = IN_ACTION_SAY;
	this->input.bind_map[string("y:down")] = IN_ACTION_SAY_TEAM;
	this->input.bind_map[string("escape:down")] = IN_ACTION_CANCEL;
	this->input.bind_map[string("f10:down")] = IN_ACTION_MENU;
	this->input.bind_map[string("tab:down")] = IN_ACTION_PANELS;
	this->input.bind_map[string("`:down")] = IN_ACTION_CONSOLE;
	this->input.bind_map[string("down:pressed")] = IN_ACTION_MOVE_MAP_UP;
	this->input.bind_map[string("up:pressed")] = IN_ACTION_MOVE_MAP_DOWN;
	this->input.bind_map[string("right:pressed")] = IN_ACTION_MOVE_MAP_LEFT;
	this->input.bind_map[string("left:pressed")] = IN_ACTION_MOVE_MAP_RIGHT;
	this->input.bind_map[string("lalt:pressed")] = IN_ACTION_MOVE_MAP;
	this->input.bind_map[string("mbutton:pressed")] = IN_ACTION_CATCH_MAP;
	this->input.bind_map[string("relwheel")] = IN_ACTION_ZOOM_MAP;
	this->input.bind_map[string("num+:pressed")] = IN_ACTION_ZOOM_MAP_IN;
	this->input.bind_map[string("num-:pressed")] = IN_ACTION_ZOOM_MAP_OUT;
	this->input.bind_map[string("space:down")] = IN_ACTION_RESET_MAP_ZOOM;
	this->input.bind_map[string("f5:down")] = IN_ACTION_SHOW_UNDERGROUND;
	this->input.bind_map[string("f6:down")] = IN_ACTION_SHOW_GROUND;
	this->input.bind_map[string("f7:down")] = IN_ACTION_SHOW_SKY;
	this->input.bind_map[string("f8:down")] = IN_ACTION_SHOW_MULTIPLE;
	
	// video settings
#ifdef DEBUG
	this->video.fullscreen = false;
#else
	this->video.fullscreen = true;
#endif
	this->video.vert_sync = false;
	this->video.refresh_rate = 0;   // means system default
	this->video.wnd_position.Set(0, 0);
	this->video.wnd_size.Set(800, 600);
	this->video.full_size.Set(1024, 768);

	this->video.gui = "classic";
	this->video.minmag_filter = GfxServer::FILTER_LINEAR;
	this->video.mipmap_filter = GfxServer::FILTER_NONE;
	this->video.warfog_color.set(0.2f, 0.12f, 0.06f, 0.25f);
	this->video.show_fps = false;
	this->video.map_move_speed = 0.5f;
	this->video.map_zoom_speed = 0.3f;
	this->video.max_fps = 60;
	this->video.unit_selection = "RectSelRenderer";
	this->video.building_selection = "BoxSelRenderer";

	// sound settings
	this->audio.master_volume = 1.0f;
	this->audio.menu_play_music = true;
	this->audio.menu_music_volume = 1.0f;
	this->audio.menu_sound_volume = 1.0f;
	this->audio.game_play_music = true;
	this->audio.game_play_speach = true;
	this->audio.game_music_volume = 1.0f;
	this->audio.game_sound_volume = 1.0f;

	// network settings
	this->network.last_address = "";
	this->network.server_port = 17000;

	// system settings
	this->system.font_path = "fonts:arial.ttf";
	this->system.font_color.set(0.6f, 0.7f, 1.0f);
	this->system.font_size = 12;
	this->system.log_to_ost = true;
	this->system.fps_show = true;
	this->system.fps_interval = 0.3f;
}


void Profile::LoadDefaultSettings()
{
	this->ClearHeader();

	this->SetDefaultSettings();

	// set status to loaded
	this->SetFileName(string("profiles:") + PROFILE_DEFAULT_ID + ".xml");
	this->loaded = true;
}



//=========================================================================
// Deserializing
//=========================================================================

bool Profile::ReadHeader(Serializer &serializer)
{
	this->SetDefaultSettings();

	// read profile settings
	serializer.GetAttribute("active", this->active);

	// read player settings
	if (serializer.GetGroup("player"))
	{
		serializer.GetAttribute("name", this->player.name);
		serializer.GetAttributeColor("color", this->player.color);
		serializer.EndGroup();
	}

	return true;
}


bool Profile::ReadBody(Serializer &serializer)
{
	// read input settings
	if (serializer.GetGroup("input"))
	{
		if (serializer.GetGroup("mouse"))
		{
			serializer.GetAttributePercent("sensitivity", this->input.sensitivity);
			serializer.EndGroup();
		}
		if (serializer.GetGroup("binding"))
		{
			bool unbind_all = false;
			serializer.GetAttribute("unbind-all", unbind_all);
			if (unbind_all)
				this->input.bind_map.clear();

			if (serializer.GetGroup() && serializer.CheckGroupName("bind"))
			{
				string definition, action;

				do {
					serializer.GetAttribute("definition", definition);
					serializer.GetAttribute("action", action);

					this->input.bind_map[definition] = action;
				} while (serializer.GetNextGroup());

				serializer.EndGroup();
			}

			serializer.EndGroup();
		}
		serializer.EndGroup();
	}

	// read video settings
	if (serializer.GetGroup("video"))
	{
		serializer.GetAttribute("gui", this->video.gui);

		if (serializer.GetGroup("display"))
		{
			string resolution;
			ushort_t w, h;

			serializer.GetAttribute("fullscreen", this->video.fullscreen);
			if (serializer.GetAttribute("resolution", resolution) && sscanf(resolution.c_str(), "%hux%hu", &w, &h) == 2)
			{
				this->video.full_size.width = w;
				this->video.full_size.height = h;
			}
			serializer.GetAttribute("vert-sync", this->video.vert_sync);
			serializer.GetAttribute("refresh-rate", this->video.refresh_rate);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("window"))
		{
			serializer.GetAttribute("left", this->video.wnd_position.left);
			serializer.GetAttribute("top", this->video.wnd_position.top);
			serializer.GetAttribute("width", this->video.wnd_size.width);
			serializer.GetAttribute("height", this->video.wnd_size.height);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("graphics"))
		{
			string filter;
			string selection;
			if (serializer.GetAttribute("texture-filter", filter))
				GfxServer::StringToFilter(filter, this->video.minmag_filter);
			if (serializer.GetAttribute("mipmap-filter", filter))
				GfxServer::StringToFilter(filter, this->video.mipmap_filter);
			serializer.GetAttribute("max-fps", this->video.max_fps);
			if (serializer.GetAttribute("unit-selection", selection, false))
				SelectionRenderer::StringToSelRenderer(selection, this->video.unit_selection);
			if (serializer.GetAttribute("building-selection", selection, false))
				SelectionRenderer::StringToSelRenderer(selection, this->video.building_selection);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("warfog"))
		{
			serializer.GetAttributeColor("color", this->video.warfog_color);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("show"))
		{
			serializer.GetAttribute("fps", this->video.show_fps);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("map"))
		{
			serializer.GetAttributePercent("move-speed", this->video.map_move_speed);
			serializer.GetAttributePercent("zoom-speed", this->video.map_zoom_speed);
			serializer.EndGroup();
		}

		serializer.EndGroup();
	}

	// read audio settings
	if (serializer.GetGroup("audio"))
	{
		serializer.GetAttribute("master-volume", this->audio.master_volume);

		if (serializer.GetGroup("menu"))
		{
			serializer.GetAttribute("play-music", this->audio.menu_play_music);
			serializer.GetAttributePercent("music-volume", this->audio.menu_music_volume);
			serializer.GetAttributePercent("sound-volume", this->audio.menu_sound_volume);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("game"))
		{
			serializer.GetAttribute("play-music", this->audio.game_play_music);
			serializer.GetAttribute("play-speach", this->audio.game_play_speach);
			serializer.GetAttributePercent("music-volume", this->audio.game_music_volume);
			serializer.GetAttributePercent("sound-volume", this->audio.game_sound_volume);
			serializer.EndGroup();
		}

		serializer.EndGroup();
	}

	// read network settings
	if (serializer.GetGroup("network"))
	{
		if (serializer.GetGroup("connection"))
		{
			serializer.GetAttribute("last-address", this->network.last_address);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("server"))
		{
			serializer.GetAttribute("port", this->network.server_port);
			serializer.EndGroup();
		}

		serializer.EndGroup();
	}

	// read system settings
	if (serializer.GetGroup("system"))
	{
		if (serializer.GetGroup("font"))
		{
			serializer.GetAttribute("src", this->system.font_path, false);
			serializer.GetAttribute("size", this->system.font_size);
			serializer.GetAttributeColor("color", this->system.font_color);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("fps"))
		{
			serializer.GetAttribute("show", this->system.fps_show);
			serializer.GetAttribute("interval", this->system.fps_interval);
			serializer.EndGroup();
		}

		if (serializer.GetGroup("logs"))
		{
			serializer.GetAttribute("log-to-ost", this->system.log_to_ost);
			serializer.EndGroup();
		}

		serializer.EndGroup();
	}

	return true;
}


bool Profile::WriteHeader(Serializer &serializer)
{
	// write profile settings
	if (!serializer.SetAttribute("active", this->active))
		return false;

	// write player settings
	if (!serializer.AddGroup("player")) return false;
		if (!serializer.SetAttribute("name", this->player.name)) return false;
		if (!serializer.SetAttributeColor("color", this->player.color)) return false;
	serializer.EndGroup();

	return true;
}


bool Profile::WriteBody(Serializer &serializer)
{
	// write input settings
	if (!serializer.AddGroup("input")) return false;
		if (!serializer.AddGroup("mouse")) return false;
			if (!serializer.SetAttributePercent("sensitivity", this->input.sensitivity)) return false;
		serializer.EndGroup();
		if (!serializer.AddGroup("binding")) return false;
			hash_map<string, string>::iterator it;

			if (!serializer.SetAttribute("unbind-all", true)) return false;

			for (it = this->input.bind_map.begin(); it != this->input.bind_map.end(); it++)
			{
				if (!serializer.AddGroup("bind")) return false;
					
					if (!serializer.SetAttribute("definition", it->first)) return false;
					if (!serializer.SetAttribute("action", it->second)) return false;
				serializer.EndGroup();
			}
		serializer.EndGroup();
	serializer.EndGroup();

	// write video settings
	if (!serializer.AddGroup("video")) return false;
		if (!serializer.SetAttribute("gui", this->video.gui)) return false;

		if (!serializer.AddGroup("display")) return false;
			char buff[50];
			if (!serializer.SetAttribute("fullscreen", this->video.fullscreen)) return false;
			sprintf(buff, "%hux%hu", this->video.full_size.width, this->video.full_size.height);
			if (!serializer.SetAttribute("resolution", buff)) return false;
			if (!serializer.SetAttribute("vert-sync", this->video.vert_sync)) return false;
			if (!serializer.SetAttribute("refresh-rate", this->video.refresh_rate)) return false;
		serializer.EndGroup();

		if (!serializer.AddGroup("window")) return false;
			if (!serializer.SetAttribute("left", this->video.wnd_position.left)) return false;
			if (!serializer.SetAttribute("top", this->video.wnd_position.top)) return false;
			if (!serializer.SetAttribute("width", this->video.wnd_size.width)) return false;
			if (!serializer.SetAttribute("height", this->video.wnd_size.height)) return false;
		serializer.EndGroup();

		if (!serializer.AddGroup("graphics")) return false;
			string filter;
			string selection;
			if (!GfxServer::FilterToString(this->video.minmag_filter, filter)) return false;
			if (!serializer.SetAttribute("texture-filter", filter)) return false;
			if (!GfxServer::FilterToString(this->video.mipmap_filter, filter)) return false;
			if (!serializer.SetAttribute("mipmap-filter", filter)) return false;
			if (!serializer.SetAttribute("max-fps", this->video.max_fps)) return false;

			if (!SelectionRenderer::SelRendererToString(this->video.unit_selection, selection)) return false;
			if (!serializer.SetAttribute("unit-selection", selection)) return false;
			if (!SelectionRenderer::SelRendererToString(this->video.building_selection, selection)) return false;
			if (!serializer.SetAttribute("building-selection", selection)) return false;
		serializer.EndGroup();

		if (!serializer.AddGroup("warfog")) return false;
			if (!serializer.SetAttributeColor("color", this->video.warfog_color)) return false;
		serializer.EndGroup();

		if (!serializer.AddGroup("show")) return false;
			if (!serializer.SetAttribute("fps", this->video.show_fps)) return false;
		serializer.EndGroup();

		if (!serializer.AddGroup("map")) return false;
			if (!serializer.SetAttributePercent("move-speed", this->video.map_move_speed)) return false;
			if (!serializer.SetAttributePercent("zoom-speed", this->video.map_zoom_speed)) return false;
		serializer.EndGroup();
	serializer.EndGroup();

	// write audio settings
	if (!serializer.AddGroup("audio")) return false;
		if (!serializer.SetAttributePercent("master-volume", this->audio.master_volume)) return false;

		if (!serializer.AddGroup("menu")) return false;
			if (!serializer.SetAttribute("play-music", this->audio.menu_play_music)) return false;
			if (!serializer.SetAttributePercent("music-volume", this->audio.menu_music_volume)) return false;
			if (!serializer.SetAttributePercent("sound-volume", this->audio.menu_sound_volume)) return false;
		serializer.EndGroup();

		if (!serializer.AddGroup("game")) return false;
			if (!serializer.SetAttribute("play-music", this->audio.game_play_music)) return false;
			if (!serializer.SetAttribute("play-speach", this->audio.game_play_speach)) return false;
			if (!serializer.SetAttributePercent("music-volume", this->audio.game_music_volume)) return false;
			if (!serializer.SetAttributePercent("sound-volume", this->audio.game_sound_volume)) return false;
		serializer.EndGroup();
	serializer.EndGroup();

	// write connection settings
	if (!serializer.AddGroup("network")) return false;
		if (!serializer.AddGroup("connection")) return false;
			if (!serializer.SetAttribute("last-address", this->network.last_address)) return false;
		serializer.EndGroup();

		// write server settings
		if (!serializer.AddGroup("server")) return false;
			if (!serializer.SetAttribute("port", this->network.server_port)) return false;
		serializer.EndGroup();
	serializer.EndGroup();

	// write system settings
	if (!serializer.AddGroup("system")) return false;
		if (!serializer.AddGroup("font")) return false;
			if (!serializer.SetAttribute("src", this->system.font_path)) return false;
			if (!serializer.SetAttribute("size", this->system.font_size)) return false;
			if (!serializer.SetAttributeColor("color", this->system.font_color)) return false;
		serializer.EndGroup();

		if (!serializer.AddGroup("fps")) return false;
			if (!serializer.SetAttribute("show", this->system.fps_show)) return false;
			if (!serializer.SetAttribute("interval", this->system.fps_interval)) return false;
		serializer.EndGroup();

		if (!serializer.AddGroup("logs")) return false;
			if (!serializer.SetAttribute("log-to-ost", this->system.log_to_ost)) return false;
		serializer.EndGroup();
	serializer.EndGroup();

	return true;
}


// vim:ts=4:sw=4:
