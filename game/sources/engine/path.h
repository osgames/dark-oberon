#ifndef __path_h__
#define __path_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file path.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "kernel/mathlib.h"
#include "kernel/ref.h"
#include "engine/mapstructures.h"

class Unit;


//=========================================================================
// Path
//=========================================================================

/**
	@class Path
	@ingroup Engine_Module
*/
    
class Path
{
//--- methods
public:
	Path();

	// queue of steps
	bool IsEmpty() const;
	void PushDirection(MapEnums::Direction direction);
	MapEnums::Direction PopDirection();

	// next steps
	void GetNextDirection(MapEnums::Direction &direction, bool &rotating_left) const;
	void GetNextPosition(MapPosition3D &position, MapEnums::Direction &direction) const;
	bool IsInDestination(const MapPosition3D &position, const MapSize &size);
	bool IsFinished();
	void SetFinished(bool finished);
	bool IsReachable();
	void SetReachable(bool reachable);

private:
	void UpdateDestination();


//--- variables
public:
	MapArea dest_area;
	MapEnums::Segment dest_segment;
	Ref<Unit> dest_unit;

protected:
	bool finished;
	bool reachable;
	list<MapEnums::Direction> steps;
};


//=========================================================================
// Methods
//=========================================================================

inline
Path::Path() :
	dest_segment(MapEnums::SEG_COUNT),
	finished(false),
	reachable(false)
{
	//
}


inline
void Path::PushDirection(MapEnums::Direction direction)
{
	this->steps.push_front(direction);
}


inline
MapEnums::Direction Path::PopDirection()
{
	MapEnums::Direction direction = this->steps.front();
	this->steps.pop_front();
	return direction;
}


inline
bool Path::IsEmpty() const
{
	return this->steps.empty();
}


inline
void Path::GetNextDirection(MapEnums::Direction &direction, bool &rotating_left) const
{
	Assert(!this->steps.empty());
	Assert(direction < MapEnums::DIRECTION_COUNT - 2);

	MapEnums::Direction front = this->steps.front();

	// look direction will be the same while moving in the same direction or through segments
	if (front == direction || front == MapEnums::DIRECTION_UP || front == MapEnums::DIRECTION_DOWN)
		return;

	// get next direction
	direction = MapEnums::GetNextDirection(direction, front, rotating_left);
}


inline
void Path::GetNextPosition(MapPosition3D &position, MapEnums::Direction &direction) const
{
	Assert(!this->steps.empty());
	Assert(position.segment < MapEnums::SEG_COUNT);

	direction = this->steps.front();

	switch (direction)
	{
	case MapEnums::DIRECTION_NORTH:
		Assert(position.y < mapel_t(-1));
		position.y++;
		break;

	case MapEnums::DIRECTION_NORTH_EAST:
		Assert(position.x < mapel_t(-1));
		Assert(position.y < mapel_t(-1));
		position.x++;
		position.y++;
		break;

	case MapEnums::DIRECTION_EAST:
		Assert(position.x < mapel_t(-1));
		position.x++;
		break;

	case MapEnums::DIRECTION_SOUTH_EAST:
		Assert(position.x < mapel_t(-1));
		Assert(position.y);
		position.x++;
		position.y--;
		break;

	case MapEnums::DIRECTION_SOUTH:
		Assert(position.y);
		position.y--;
		break;

	case MapEnums::DIRECTION_SOUTH_WEST:
		Assert(position.x);
		Assert(position.y);
		position.x--;
		position.y--;
		break;

	case MapEnums::DIRECTION_WEST:
		Assert(position.x);
		position.x--;
		break;

	case MapEnums::DIRECTION_NORTH_WEST:
		Assert(position.x);
		Assert(position.y < mapel_t(-1));
		position.x--;
		position.y++;
		break;

	case MapEnums::DIRECTION_UP:
		Assert(position.segment < MapEnums::SEG_SKY);
		position.segment = position.segment == MapEnums::SEG_UNDERGROUND ? MapEnums::SEG_GROUND : MapEnums::SEG_SKY;
		break;

	case MapEnums::DIRECTION_DOWN:
		Assert(position.segment > MapEnums::SEG_UNDERGROUND);
		position.segment = position.segment == MapEnums::SEG_SKY ? MapEnums::SEG_GROUND : MapEnums::SEG_UNDERGROUND;
		break;

	default:
		ErrorMsg("Invalid direction");
		break;
	}
}


inline
void Path::SetFinished(bool finished)
{
	this->finished = finished;
}


inline
void Path::SetReachable(bool reachable)
{
	this->reachable = reachable;
}


#endif // __path_h__

// vim:ts=4:sw=4:
