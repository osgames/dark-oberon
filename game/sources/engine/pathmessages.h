#ifndef __pathmessages_h__
#define __pathmessages_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file pathmessages.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/mapstructures.h"
#include "kernel/messages.h"
#include "kernel/ref.h"

class Unit;
class Path;


//=========================================================================
// PathRequest
//=========================================================================

/**
	@class PathRequest
	@ingroup Engine_Module
*/

class PathRequest : public Request<PathRequest>
{

//--- methods
public:
	PathRequest();
	virtual ~PathRequest();

//--- variables
public:
	Ref<Unit> unit;
	Ref<Unit> dest_unit;
	MapArea dest_area;
	MapEnums::Segment dest_segment;
};


//=========================================================================

inline
PathRequest::PathRequest() :
	Request<PathRequest>::Request()
{
	//
}


inline
PathRequest::~PathRequest()
{
	//
}


//=========================================================================
// PathResponse
//=========================================================================

/**
	@class PathResponse
	@ingroup Engine_Module
*/

class PathResponse : public Response<PathResponse>
{

//--- methods
public:
	PathResponse(uint_t request_id);
	virtual ~PathResponse();

//--- variables
public:
	Ref<Unit> unit;
	Path *path;
};


//=========================================================================

inline
PathResponse::PathResponse(uint_t request_id) :
	Response<PathResponse>::Response(request_id),
	path(NULL)
{
	//
}


inline
PathResponse::~PathResponse()
{
	SafeDelete(this->path);
}


#endif // __unit_h__

// vim:ts=4:sw=4:
