//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file level_cmds.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/level.h"


//=========================================================================
// Commands
//=========================================================================

static void s_AddUnitToMap(void *slf, Cmd *cmd)
{
	Level *self = (Level *)slf;
	bool result = self->AddUnitToMap((Unit *)cmd->In()->GetO());
	cmd->Out()->SetB(result);
}


static void s_RemoveUnitFromMap(void *slf, Cmd *cmd)
{
	Level *self = (Level *)slf;
	self->RemoveUnitFromMap((Unit *)cmd->In()->GetO());
}


void s_InitLevel_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("b_AddUnitToMap_o",          'AUTM', s_AddUnitToMap);
	cl->AddCmd("v_RemoveUnitFromMap_o",     'RUFM', s_RemoveUnitFromMap);

	cl->EndCmds();
}

// vim:ts=4:sw=4:
