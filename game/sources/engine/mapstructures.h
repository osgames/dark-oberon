#ifndef __mapstructures_h__
#define __mapstructures_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file mapstructures.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "kernel/vector2.h"


//=========================================================================
// MapEnums
//=========================================================================

class MapEnums
{
//--- embeded
public:
	enum Segment
	{
		SEG_UNDERGROUND,
		SEG_GROUND,
		SEG_SKY,
		SEG_COUNT,
		SEG_NONE
	};

	enum Direction
	{
		DIRECTION_NORTH,
		DIRECTION_NORTH_EAST,
		DIRECTION_EAST,
		DIRECTION_SOUTH_EAST,
		DIRECTION_SOUTH,
		DIRECTION_SOUTH_WEST,
		DIRECTION_WEST,
		DIRECTION_NORTH_WEST,
		DIRECTION_UP,
		DIRECTION_DOWN,
		DIRECTION_COUNT
	};

//--- methods
public:
	static bool SegmentToString(Segment segment, string &name);
	static bool StringToSegment(const string &name, Segment &segment);
	static bool DirectionToString(Direction direction, string &name);
	static bool StringToDirection(const string &name, Direction &direction);

	static float GetBaseLength(Direction direction);
	static void GetBaseCoefs(Direction direction, float &x_coef, float &y_coef);
	static int GetRotatingSteps(Direction old_direction, Direction new_direction);
	static Direction GetDirection(const vector2 &position1, const vector2 &position2);
	static Direction GetNextDirection(Direction old_direction, Direction new_direction, bool &rotating_left);
	static Direction GetOpositeDirection(Direction direction);
	static Direction GetRandomDirection();
};


//=========================================================================

inline
float MapEnums::GetBaseLength(Direction direction)
{
	Assert(direction < DIRECTION_COUNT);

	switch (direction)
	{
	case DIRECTION_NORTH_EAST:
	case DIRECTION_SOUTH_EAST:
	case DIRECTION_SOUTH_WEST:
	case DIRECTION_NORTH_WEST:
		return SQRT2;

	case DIRECTION_UP:
	case DIRECTION_DOWN:
		return 3.0f;

	default:
		return 1.0f;
	}
}


inline
void MapEnums::GetBaseCoefs(Direction direction, float &x_coef, float &y_coef)
{
	Assert(direction < DIRECTION_COUNT);

	// horizontal coeficient
	switch (direction)
	{
	case DIRECTION_NORTH:
	case DIRECTION_SOUTH:
		x_coef = 0.0f;
		break;

	case DIRECTION_EAST:
	case DIRECTION_UP:
		x_coef = 1.0f;
		break;

	case DIRECTION_NORTH_EAST:
	case DIRECTION_SOUTH_EAST:
		x_coef = 1.0f / SQRT2;
		break;

	case DIRECTION_NORTH_WEST:
	case DIRECTION_SOUTH_WEST:
		x_coef = -1.0f / SQRT2;
		break;

	default:
		x_coef = -1.0f;
		break;
	}

	// vertical coeficient
	switch (direction)
	{
	case DIRECTION_NORTH:
	case DIRECTION_UP:
		y_coef = 1.0f;
		break;

	case DIRECTION_EAST:
	case DIRECTION_WEST:
		y_coef = 0.0f;
		break;

	case DIRECTION_NORTH_EAST:
	case DIRECTION_NORTH_WEST:
		y_coef = 1.0f / SQRT2;
		break;

	case DIRECTION_SOUTH_EAST:
	case DIRECTION_SOUTH_WEST:
		y_coef = -1.0f / SQRT2;
		break;

	default:
		y_coef = -1.0f;
		break;
	}
}


inline
int MapEnums::GetRotatingSteps(Direction old_direction, Direction new_direction)
{
	Assert(old_direction < DIRECTION_COUNT - 2);
	Assert(new_direction < DIRECTION_COUNT - 2);

	// directions are the same
	if (old_direction == new_direction)
		return 0;

	// compute minimal difference
	int diff1 = new_direction - old_direction;
	int diff2 = diff1 > 0 ? diff1 - (DIRECTION_COUNT - 2) : diff1 + (DIRECTION_COUNT - 2);

	return abs(diff1) < abs(diff2) ? diff1 : diff2;
}


inline
MapEnums::Direction MapEnums::GetDirection(const vector2 &position1, const vector2 &position2)
{
	Assert(!position1.isequal(position2));

	float dx = position2.x - position1.x;
	float dy = position2.y - position1.y;

	if (!dy)
		return dx > 0.0f ? DIRECTION_EAST : DIRECTION_WEST;

	float ratio = dx / dy;

	if (ratio < -2.0f)
		return dy > 0.0f ? DIRECTION_WEST : DIRECTION_EAST;
	if (ratio >= -2.0f && ratio < -0.5f)
		return dy > 0.0f ? DIRECTION_NORTH_WEST : DIRECTION_SOUTH_EAST;
	if (ratio >= -0.5f && ratio < 0.5f)
		return dy > 0.0f ? DIRECTION_NORTH : DIRECTION_SOUTH;
	if (ratio >= 0.5f && ratio < 2.0f)
		return dy > 0.0f ? DIRECTION_NORTH_EAST : DIRECTION_SOUTH_WEST;
	if (ratio >= 2.0f)
		return dy > 0.0f ? DIRECTION_EAST : DIRECTION_WEST;

	Error();
	return DIRECTION_COUNT;
}


inline
MapEnums::Direction MapEnums::GetNextDirection(Direction old_direction, Direction new_direction, bool &rotating_left)
{
	Assert(old_direction < DIRECTION_COUNT - 2);
	Assert(new_direction < DIRECTION_COUNT - 2);

	// direction will be the same while moving in the same direction
	if (old_direction == new_direction)
		return old_direction;

	// compute minimal difference
	int diff = GetRotatingSteps(old_direction, new_direction);

	// compute new direction
	rotating_left = diff < 0;
	diff = rotating_left ? -1 : 1;
	diff += old_direction;

	if (diff < 0)
		diff = (DIRECTION_COUNT - 2) - 1;
	else if (diff == (DIRECTION_COUNT - 2))
		diff = 0;

	return (Direction)diff;
}


inline
MapEnums::Direction MapEnums::GetOpositeDirection(Direction direction)
{
	Assert(direction < DIRECTION_COUNT);

	switch (direction)
	{
	case DIRECTION_UP:
		return DIRECTION_DOWN;
	case DIRECTION_DOWN:
		return DIRECTION_UP;
	default:
		{
			int dir = direction + (DIRECTION_COUNT - 2) / 2;
			return Direction(dir < (DIRECTION_COUNT - 2) ? dir : dir - (DIRECTION_COUNT - 2));
		}
	}
}


inline
MapEnums::Direction MapEnums::GetRandomDirection()
{
	return Direction(n_irand(DIRECTION_COUNT - 2));
}


//=========================================================================
// MapSize, MapPosition2D
//=========================================================================

class MapSize
{
public:
	mapel_t width;
	mapel_t height;

	MapSize() : width(0), height(0) {}
	MapSize(mapel_t map_w, mapel_t map_h) : width(map_w), height(map_h) {}

	void Clear()
	{
		this->width = this->height = 0;
	}

	void Set(mapel_t map_w, mapel_t map_h)
	{
		this->width = map_w;
		this->height = map_h;
	}

	bool operator == (const MapSize &size)
	{
		return this->width == size.width && this->height == size.height;
	}

	bool operator != (const MapSize &size)
	{
		return !(*this == size);
	}
};


class MapPosition2D
{
//--- methods
public:
	MapPosition2D() : x(0), y(0) {}
	MapPosition2D(mapel_t map_x, mapel_t map_y) : x(map_x), y(map_y) {}

	virtual void Clear()
	{
		this->x = this->y = 0;
	}

	void Set(mapel_t map_x, mapel_t map_y)
	{
		this->x = map_x;
		this->y = map_y;
	}

	bool operator == (const MapPosition2D &position) const
	{
		return this->x == position.x && this->y == position.y;
	}

	bool operator != (const MapPosition2D &position) const
	{
		return !(*this == position);
	}

//--- variables
public:
	mapel_t x;
	mapel_t y;
};


class MapPosition3D : public MapPosition2D
{
//--- methods
public:
	MapPosition3D() : MapPosition2D(), segment(MapEnums::SEG_NONE) {}
	MapPosition3D(mapel_t map_x, mapel_t map_y, MapEnums::Segment map_segment) : MapPosition2D(map_x, map_y), segment(map_segment) {}

	virtual void Clear()
	{
		MapPosition2D::Clear();
		this->segment = MapEnums::SEG_NONE;
	}

	void Set(mapel_t map_x, mapel_t map_y, MapEnums::Segment map_segment)
	{
		MapPosition2D::Set(map_x, map_y);
		this->segment = map_segment;
	}

	bool operator == (const MapPosition3D &position) const
	{
		return MapPosition2D::operator == (position) && this->segment == position.segment;
	}

	bool operator != (const MapPosition3D &position) const
	{
		return !(*this == position);
	}


//--- variables
public:
	MapEnums::Segment segment;
};


/**
	@note We assume that position + size is not greater then maximal value of mapel_t.
*/
class MapArea
{
public:
	mapel_t x;
	mapel_t y;
	mapel_t width;
	mapel_t height;

	MapArea() : x(0), y(0), width(0), height(0) {}
	MapArea(mapel_t map_x, mapel_t map_y, mapel_t map_w, mapel_t map_h) : x(map_x), y(map_y), width(map_w), height(map_h)
	{
		Assert(map_x + map_w >= map_x);
		Assert(map_y + map_h >= map_y);
	}

	void Clear()
	{
		this->x = this->y = 0;
		this->width = this->height = 0;
	}

	void Set(mapel_t map_x, mapel_t map_y, mapel_t map_w, mapel_t map_h)
	{
		Assert(map_x + map_w >= map_x);
		Assert(map_y + map_h >= map_y);

		this->x = map_x;
		this->y = map_y;
		this->width = map_w;
		this->height = map_h;
	}

	bool operator == (const MapArea &area) const
	{
		return this->x == area.x && this->y == area.y && this->width == area.width && this->height == area.height;
	}

	bool operator != (const MapArea &area) const
	{
		return !(*this == area);
	}

	bool IsInArea(const MapPosition2D &position) const
	{
		return position.x >= this->x && position.x < this->x + this->width
			&& position.y >= this->y && position.y < this->y + this->height;
	}

	bool IsIntersectingArea(const MapPosition2D &position, const MapSize &size) const
	{
		Assert(position.x + size.width >= position.x);
		Assert(position.y + size.height >= position.y);

		return position.x + size.width > this->x && position.x < this->x + this->width
			&& position.y + size.height > this->y && position.y < this->y + this->height;
	}
};


#endif // __mapstructures_h__

// vim:ts=4:sw=4:
