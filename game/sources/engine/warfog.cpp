//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file warfog.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/warfog.h"
#include "engine/game.h"
#include "engine/profile.h"
#include "framework/gfxserver.h"
#include "framework/imagefile.h"


//=========================================================================
// Warfog
//=========================================================================

/**
	Creates warfog.
*/
Warfog::Warfog(ushort_t w, ushort_t h) :
	width(w),
	height(h)
{
	GfxServer *gfx_server = GfxServer::GetInstance();
	Assert(gfx_server);

	// set texture size
	if (!GfxServer::GetInstance()->IsExtensions(GfxServer::EXT_NPOT_TEXTURES))
	{
		this->width = n_next_p2(this->width);
		this->height = n_next_p2(this->height);
	}

	// create and set image file
	ulong_t data_size = this->width * this->height * 4;
	byte_t *init_buffer = NEW byte_t[data_size];
	ImageFile *image_file = NEW ImageFile;
	Assert(init_buffer && image_file);

	image_file->SetData(init_buffer, data_size, ImageFile::IF_RGBA, this->width, this->height);

	// create and load texture
	this->texture = GfxServer::GetInstance()->NewTexture(image_file);
	Assert(this->texture.IsValid());
	this->texture->AllowMipMap(false);
	this->texture->Load();

	// set colors
	this->color_unknown[0] = 0;
	this->color_unknown[1] = 0;
	this->color_unknown[2] = 0;
	this->color_unknown[3] = 255;

	const vector4 &warfog_color = Game::GetInstance()->GetActiveProfile()->video.warfog_color;
	this->color_hided[0] = byte_t(warfog_color.x * 255);
	this->color_hided[1] = byte_t(warfog_color.y * 255);
	this->color_hided[2] = byte_t(warfog_color.z * 255);
	this->color_hided[3] = byte_t(warfog_color.w * 255);

	this->color_visible[0] = this->color_hided[0];
	this->color_visible[1] = this->color_hided[1];
	this->color_visible[2] = this->color_hided[2];
	this->color_visible[3] = 0;

	// create buffer for live updating
	this->buffer = NEW byte_t[data_size];
	Assert(this->buffer);
}


/**
	Deletes warfog.
*/
Warfog::~Warfog()
{
	if (this->texture.IsValid())
	{
		this->texture->Unload();
		this->texture->Release();
	}

	SafeDeleteArray(this->buffer);
}


void Warfog::Render(byte_t **warfog_field, const MapArea &visible_area)
{
	if (!visible_area.width || !visible_area.height)
		return;

	GfxServer *gs = GfxServer::GetInstance();
	Assert(gs);

	// update warfog texture
	this->UpdateTexture(warfog_field, visible_area);
	this->UpdateCoords(visible_area);

	// static model
	gs->ResetMatrix();
	gs->SetColor(vector3(1, 1, 1));
	gs->SetTexture(this->texture.GetUnsafe());
	gs->SetMapping(this->mapping);
	gs->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, this->coords, 4);
}


void Warfog::UpdateTexture(byte_t **warfog_field, const MapArea &visible_area)
{
	Assert(ushort_t(visible_area.x) + visible_area.width <= this->width - 2);
	Assert(ushort_t(visible_area.y) + visible_area.height <= this->height - 2);

	mapel_t x, y;
	byte_t *color;
	ulong_t index;

	// update texture
	index = (visible_area.y) * this->width * 4 + (visible_area.x) * 4;

	for (y = visible_area.y; y < visible_area.y + visible_area.height + 2; y++, index += (this->width - visible_area.width - 2) * 4)
	{
		for (x = visible_area.x; x < visible_area.x + visible_area.width + 2; x++)
		{
			switch (warfog_field[x][y])
			{
			case WARFOG_UNKNOWN: color = this->color_unknown; break;
			case WARFOG_HIDED:   color = this->color_hided;   break;
			default:             color = this->color_visible; break;
			}
			
			this->buffer[index++] = color[0];
			this->buffer[index++] = color[1];
			this->buffer[index++] = color[2];
			this->buffer[index++] = color[3];
		}
	}

	this->texture->ReplaceData(this->buffer);
}


void Warfog::UpdateCoords(const MapArea &visible_area)
{
	MapArea area = visible_area;

	float x1 = float(area.x - SEGMENTS_SHIFT);
	float y1 = float(area.y - SEGMENTS_SHIFT);
	float x2 = float(area.x + area.width - SEGMENTS_SHIFT);
	float y2 = float(area.y + area.height - SEGMENTS_SHIFT);

	// set coords
	this->coords[0].set((x1 - y1) * ISO_SCENE_H_COEF, (x1 + y1) * ISO_SCENE_V_COEF);
	this->coords[1].set((x1 - y2) * ISO_SCENE_H_COEF, (x1 + y2) * ISO_SCENE_V_COEF);
	this->coords[2].set((x2 - y2) * ISO_SCENE_H_COEF, (x2 + y2) * ISO_SCENE_V_COEF);
	this->coords[3].set((x2 - y1) * ISO_SCENE_H_COEF, (x2 + y1) * ISO_SCENE_V_COEF);

	// set mapping
	x1 = float(area.x + 1) / this->width;
	y1 = float(area.y + 1) / this->height;
	x2 = float(area.x + 1 + area.width) / this->width;
	y2 = float(area.y + 1 + area.height) / this->height;

	this->mapping[0].set(x1, y1);
	this->mapping[1].set(x1, y2);
	this->mapping[2].set(x2, y2);
	this->mapping[3].set(x2, y1);
}


// vim:ts=4:sw=4:
