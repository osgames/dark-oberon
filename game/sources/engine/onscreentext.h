#ifndef __onscreentext_h__
#define __onscreentext_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file onscreentext.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "kernel/root.h"
#include "kernel/vector.h"


//=========================================================================
// OnScreenText
//=========================================================================

/**
	@class OnScreenText
	@ingroup Engine_Module

	@todo Script interface.
*/
    
class OnScreenText : public Root
{
//--- embeded
private:
	struct Message
	{
		string text;
		vector3 color;
		double time;
	};

	typedef list<Message> MessageList;


//--- variables
private:
	MessageList messages;         ///< List of active (visible) messages.

	vector3 default_color;        ///< Default color of messages.
	double display_time;          ///< Time of displaying of one message. [seconds]
	vector2 position;             ///< Position of rendering rectangle.
	vector2 size;                 ///< Size of rendering rectangle.

	byte_t max_count;             ///< Maximal number of messages. Computed from height of rectangle and height of font.


//--- methods
public:
	OnScreenText(const char *id);
	virtual ~OnScreenText();

	void AddMessage(const string &text);
	void AddMessage(const string &text, const vector3 &color);
	void AddMessage(const char *text);
	void AddMessage(const char *text, const vector3 &color);

	void SetDefaultColor(const vector3 &color);
	void SetDisplayTime(double display_time);
	void SetPosition(const vector2 &position);
	void SetSize(const vector2 &size);

	void Render();
};


//=========================================================================
// Methods
//=========================================================================

inline
void OnScreenText::SetDefaultColor(const vector3 &color)
{
	this->default_color = color;
}


inline
void OnScreenText::SetDisplayTime(double display_time)
{
	this->display_time = display_time;
}


inline
void OnScreenText::AddMessage(const string &text)
{
	this->AddMessage(text.c_str(), this->default_color);
}


inline
void OnScreenText::AddMessage(const string &text, const vector3 &color)
{
	this->AddMessage(text.c_str(), color);
}


inline
void OnScreenText::AddMessage(const char *text)
{
	this->AddMessage(text, this->default_color);
}


inline
void OnScreenText::SetPosition(const vector2 &position)
{
	this->position = position;
}


#endif // __onscreentext_h__

// vim:ts=4:sw=4:
