//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file preloaded.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/preloaded.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializeserver.h"

PrepareClass(Preloaded, "Root", s_NewPreloaded, s_InitPreloaded);


//=========================================================================
// Preloaded
//=========================================================================

/**
	Constructor.
*/
Preloaded::Preloaded(const char *id) :
	Root(id),
	loaded(false),
	load_header(false)
{
	//
}


/**
	Destructor.
*/
Preloaded::~Preloaded()
{
	//
}


void Preloaded::ClearHeader()
{
	//
}


void Preloaded::ClearBody()
{
	//
}


//=========================================================================
// Resources
//=========================================================================

bool Preloaded::LoadResources()
{
	return true;
}


void Preloaded::UnloadResources()
{
	//
}


//=========================================================================
// Loading
//=========================================================================

/**
	Load object from file.

	@return @c True if successful.
*/
bool Preloaded::Load()
{
	Assert(!this->file_name.empty());
	Assert(SerializeServer::GetInstance());

	if (this->loaded)
		return true;

	bool ok = true;
	this->ClearHeader();

	LogInfo2("Loading %s: %s", this->GetClass()->GetName().c_str(), this->GetID().c_str());

	// deserialize object
	if (!SerializeServer::GetInstance()->Deserialize(this->file_name, this))
	{
		LogError1("Error deserializing preloaded object from: %s", this->file_name.c_str());
		ok = false;
	}

	// load all included resources
	if (ok && !this->LoadResources())
	{
		LogError1("Error loading resources of preloaded object from: %s", this->file_name.c_str());
		ok = false;
	}

	if (!ok)
	{
		this->ClearBody();
		this->ClearHeader();
	}

	return this->loaded = ok;
}


/**
	Load object header from file.

	@return @c True if successful.
*/
bool Preloaded::LoadHeader()
{
	this->load_header = true;

	Assert(!this->file_name.empty());
	Assert(SerializeServer::GetInstance());

	if (this->loaded)
		return true;

	bool ok = true;
	this->ClearHeader();

	LogDebug2("Loading %s header: %s", this->GetClass()->GetName().c_str(), this->GetID().c_str());

	// deserialize header
	if (!SerializeServer::GetInstance()->Deserialize(this->file_name, this))
	{
		LogError1("Error deserializing preloaded object from: %s", this->file_name.c_str());
		this->ClearHeader();
		ok = false;
	}

	this->load_header = false;
	return ok;
}


/**
	Unload scheme.

	@return @c True if successful.
*/
bool Preloaded::Unload()
{
	if (!this->loaded)
		return false;

	LogInfo2("Unloading %s: %s", this->GetClass()->GetName().c_str(), this->GetID().c_str());

	// unload resoures first
	this->UnloadResources();

	// then clear body
	this->ClearBody();

	this->loaded = false;
	return true;
}


/**
	Saves preloaded to file.

	@return @c True if successful.
*/
bool Preloaded::Save()
{
	Assert(this->loaded);
	Assert(!this->file_name.empty());
	Assert(SerializeServer::GetInstance());

	LogDebug2("Saving %s: %s", this->GetClass()->GetName().c_str(), this->GetID().c_str());
	
	if (!SerializeServer::GetInstance()->Serialize(this, this->file_name))
	{
		LogError1("Error serializing preloaded object to: %s", this->file_name.c_str());
		return false;
	}

	return true;
}


/**
	Sets active state.
	@param active Active state.
*/
void Preloaded::SetActive(bool active)
{
	this->active = active;
}



//=========================================================================
// Deserializing
//=========================================================================

/**
*/
bool Preloaded::Deserialize(Serializer &serializer, bool first)
{
	Assert(!this->loaded);

	// read header first
	if (!this->ReadHeader(serializer))
		return false;

	// load only header and nothing else
	if (this->load_header)
		return true;

	// load body
	return this->ReadBody(serializer);
}


bool Preloaded::ReadHeader(Serializer &serializer)
{
	return true;
}


bool Preloaded::ReadBody(Serializer &serializer)
{
	// call ancestor
	return Root::Deserialize(serializer, false);
}


//=========================================================================
// Serializing
//=========================================================================

/**
*/
bool Preloaded::Serialize(Serializer &serializer)
{
	// call ancestor
	if (!Root::Serialize(serializer))
		return false;

	// save whole file
	return this->WriteHeader(serializer) && this->WriteBody(serializer);
}


bool Preloaded::WriteHeader(Serializer &serializer)
{
	return true;
}


bool Preloaded::WriteBody(Serializer &serializer)
{
	return true;
}


// vim:ts=4:sw=4:
