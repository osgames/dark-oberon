#ifndef __warfog_h__
#define __warfog_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file warfog.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/mapstructures.h"
#include "kernel/ref.h"
#include "kernel/vector.h"

class Unit;
class Texture;
class Mesh;


//=========================================================================
// Warfog
//=========================================================================

/**
	@class Warfog
	@ingroup Engine_Module
*/
    
class Warfog
{
//--- embeded
public:
	enum WarfogValue
	{
		WARFOG_HIDED = 0,
		WARFOG_UNKNOWN = 255,
	};


//--- variables
private:
	ushort_t width;
	ushort_t height;
	Ref<Texture> texture;
	vector2 coords[4];
	vector2 mapping[4];
	byte_t color_unknown[4];
	byte_t color_hided[4];
	byte_t color_visible[4];
	byte_t *buffer;


//--- methods
public:
	Warfog(ushort_t w, ushort_t h);
	virtual ~Warfog();

	void Render(byte_t **warfog_field, const MapArea &visible_area);

private:
	void UpdateTexture(byte_t **warfog_field, const MapArea &visible_area);
	void UpdateCoords(const MapArea &visible_area);
};


#endif // __warfog_h__

// vim:ts=4:sw=4:
