//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file onscreentext.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/onscreentext.h"
#include "engine/game.h"
#include "kernel/kernelserver.h"
#include "kernel/timeserver.h"
#include "framework/gfxserver.h"

PrepareClass(OnScreenText, "Root", s_NewOnScreenText, s_InitOnScreenText);


//=========================================================================
// OnScreenText
//=========================================================================

/**
	Constructor.
*/
OnScreenText::OnScreenText(const char *id) :
	Root(id),
	default_color(COLOR_INFO),
	display_time(5.0),
	position(10.0f, 10.0f),
	size(10000.0f, 10000.0f),
	max_count(10)
{
	//
}


/**
	Destructor.
*/
OnScreenText::~OnScreenText()
{
	//
}


void OnScreenText::SetSize(const vector2 &size)
{
	Assert(Game::GetInstance());

	// set size and compute maximal number of messages
	this->size = size;
	this->max_count = byte_t(size.y / Game::GetInstance()->GetSystemFont()->GetHeight());
}


void OnScreenText::AddMessage(const char *text, const vector3 &color)
{
	this->Lock();

	// add empty message to list
	this->messages.push_back(Message());

	// fill message structure
	Message &message = this->messages.back();
	message.text = text;
	message.color = color;
	message.time = TimeServer::GetInstance()->GetRealTime() + this->display_time;

	this->Unlock();
}


void OnScreenText::Render()
{
	Assert(GfxServer::GetInstance());
	Assert(Game::GetInstance());

	this->Lock();

	double real_time = TimeServer::GetInstance()->GetRealTime();

	// remove too old messages
	MessageList::iterator it = this->messages.begin();
	while (it != this->messages.end() && it->time < real_time)
	{
		it++;
		this->messages.pop_front();
	}

	// preserve max. number of messages
	MessageList::size_type count;
	for (count = this->messages.size(); count > max_count; count--)
		this->messages.pop_front();

	// draw messages
	if (!this->messages.empty())
	{
		Font *font = Game::GetInstance()->GetSystemFont();
		Assert(font);

		GfxServer::GetInstance()->SetFont(font);

		vector2 pos = this->position;
		pos.y += font->GetHeight();

		for (it = this->messages.begin(); it != this->messages.end(); it++)
		{
			GfxServer::GetInstance()->SetColor(it->color);
			GfxServer::GetInstance()->Print(pos, it->text);
			pos.y += font->GetHeight();
		}
	}

	this->Unlock();
}


// vim:ts=4:sw=4:
