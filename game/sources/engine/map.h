#ifndef __map_h__
#define __map_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file map.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/preloaded.h"
#include "engine/scheme.h"
#include "kernel/ref.h"
#include "kernel/vector.h"
#include "framework/gfxstructures.h"

class Scene;
class SceneObject;
class Unit;


//=========================================================================
// Map
//=========================================================================

/**
	@class Map
	@ingroup Engine_Module
*/
    
class Map : public Preloaded
{
//--- embeded
public:
	enum ChangeDirection
	{
		CD_NONE          = 0x00,
		CD_MOVE_UP       = 0x01,
		CD_MOVE_DOWN     = 0x02,
		CD_MOVE_LEFT     = 0x04,
		CD_MOVE_RIGHT    = 0x08,
		CD_ZOOM_IN       = 0x10,
		CD_ZOOM_OUT      = 0x20,
		CD_ZOOM_RESET    = 0x40
	};

	struct Field
	{
		Unit *unit;             ///< Unit that stays on this field;
		Unit *ghost;            ///< Ghost that stays on this field;

		Field() : unit(NULL), ghost(NULL) {};
	};

private:
	struct Fragment
	{
		Scheme::Fragment *ref;
		vector3 position;
		SceneObject *scene_objects[MapEnums::SEG_COUNT];

		Fragment();
		~Fragment();
	};

	struct Segment
	{
		Scene *terrain_scene;
		Scene *units_scene;
		Field **fields;

		Segment() : terrain_scene(NULL), units_scene(NULL), fields(NULL) {}
	};


//--- methods
public:
	Map(const char *id);
	virtual ~Map();

	virtual void SetActive(bool active);

	void Render();
	Scheme *GetScheme();
	const MapSize &GetSize();
	void UpdateViewport(ushort_t width, ushort_t height);
	void SetMoveSpeed(float move_speed);
	void SetZoomSpeed(float zoom_speed);
	void ChangeView(byte_t direction);
	void SetVisibleSegment(MapEnums::Segment segment);
	MapEnums::Segment GetVisibleSegment();
	const MapArea &GetVisibleArea();
	void SetWarfogEnabled(bool enabled);
	bool IsWarfogEnabled();

	bool AddUnit(Unit *unit);
	void RemoveUnit(Unit *unit);
	void AddSceneObject(MapEnums::Segment segment_id, SceneObject *scene_object);
	void RemoveSceneObject(MapEnums::Segment segment_id, SceneObject *scene_object);
	bool IsInMap(const MapPosition2D &position) const;
	bool IsInMap(const MapPosition2D &position, const MapSize &size) const;
	bool IsFree(const MapPosition3D &position, const MapSize &size) const;

	void ScreenToMap(MapEnums::Segment segment, const vector2 &screen_position, vector2 &map_position) const;
	void MapToScreen(MapEnums::Segment segment, const vector2 &map_position, vector2 &screen_position) const;
	Unit *FindPickUnit(ushort_t screen_x, ushort_t screen_y) const;
	bool FindUnitsInRectangle(const vector2 &corner1, const vector2 &corner2, list<Unit *> &units) const;

	byte_t GetSurface(const MapPosition2D &position) const;
	Field *GetField(const MapPosition3D &position) const;

protected:
	virtual void ClearHeader();
	virtual void ClearBody();

	// serializing
	virtual bool ReadHeader(Serializer &serializer);
	virtual bool ReadBody(Serializer &serializer);
	virtual bool WriteHeader(Serializer &serializer);
	virtual bool WriteBody(Serializer &serializer);

	bool ReadFragments(Serializer &serializer);
	bool ReadFragment(Serializer &serializer, Fragment &fragment);

	// callbacks
	static bool OnAction(const string &action, int position, void *data);

	// view
	void UpdateProjection();
	void UpdateView();
	void UpdateVisibleArea();

	// surface
	bool UpdateSurface(const MapPosition2D &position, const Scheme::Fragment &fragment);

	// other
	Unit *FindPickUnit(MapEnums::Segment segment, ushort_t screen_x, ushort_t screen_y) const;


//--- variables
protected:
	// map header
	string name;                ///< Full map name.
	string author_name;
	string author_contact;

	MapSize size;

	// map body
	Segment segments[MapEnums::SEG_COUNT];
	vector<Fragment> fragments;
	byte_t **surface;
	Ref<Scheme> scheme;

	// internal
	Projection projection;
	Viewport viewport;

	float projection_coef_h;
	float projection_coef_v;

	// view
	float move_speed;              ///< <0.0, 1.0>
	float zoom_speed;              ///< <0.0, 1.0>

	vector2 position;
	float zoom;

	bool view_changed;
	byte_t change_direction;

	MapEnums::Segment visible_segment;
	MapArea visible_area;
	bool warfog_enabled;

	// helper
	bool catched;
	bool frame_moving;
};


//=========================================================================
// Mathods
//=========================================================================

inline
Scheme *Map::GetScheme()
{
	return this->scheme.Get();
}


inline
const MapSize &Map::GetSize()
{
	return this->size;
}


inline
void Map::SetMoveSpeed(float move_speed)
{
	this->move_speed = n_clamp(move_speed, 0.0f, 1.0f);
}


inline
void Map::SetZoomSpeed(float zoom_speed)
{
	this->zoom_speed = n_clamp(zoom_speed, 0.0f, 1.0f);
}


inline
void Map::ChangeView(byte_t direction)
{
	this->change_direction |= direction;
	this->view_changed = true;
}


inline
bool Map::IsInMap(const MapPosition2D &position) const
{
	return (position.x < this->size.width) && (position.y < this->size.height);
}


inline
bool Map::IsInMap(const MapPosition2D &position, const MapSize &size) const
{
	return (position.x + size.width <= this->size.width) && (position.y + size.height <= this->size.height);
}


inline
void Map::SetVisibleSegment(MapEnums::Segment segment)
{
	Assert(segment <= MapEnums::SEG_COUNT);
	this->visible_segment = segment;
}


inline
MapEnums::Segment Map::GetVisibleSegment()
{
	return this->visible_segment;
}


inline
const MapArea &Map::GetVisibleArea()
{
	return this->visible_area;
}


inline
void Map::SetWarfogEnabled(bool enabled)
{
	this->warfog_enabled = enabled;
}


inline
bool Map::IsWarfogEnabled()
{
	return this->warfog_enabled;
}


inline
void Map::ScreenToMap(MapEnums::Segment segment, const vector2 &screen_position, vector2 &map_position) const
{
	// calculate map position
	float pomdx = ((screen_position.x - this->viewport.width / 2) * this->projection_coef_h - this->position.x) / (ISO_SCENE_H_COEF * 2);
	float pomdy = ((this->viewport.height / 2 - screen_position.y) * this->projection_coef_v - (this->position.y + 0.5f)) / (ISO_SCENE_V_COEF * 2);

	map_position.y = pomdy - pomdx;
	map_position.x = pomdy + pomdx;

	// shift map possition according to segment
	switch (segment)
	{
	case MapEnums::SEG_UNDERGROUND:
		map_position.x += SEGMENTS_SHIFT;
		map_position.y += SEGMENTS_SHIFT;
		break;
	case MapEnums::SEG_SKY:
		map_position.x -= SEGMENTS_SHIFT;
		map_position.y -= SEGMENTS_SHIFT;
		break;
	default:
		break;
	}
}


inline
void Map::MapToScreen(MapEnums::Segment segment, const vector2 &map_position, vector2 &screen_position) const
{
	// calculate screen position
	float pomdx = (map_position.x - map_position.y) / 2;
	float pomdy = (map_position.x + map_position.y) / 2;

	// shift map possition according to segment
	switch (segment)
	{
	case MapEnums::SEG_UNDERGROUND:
		pomdy -= SEGMENTS_SHIFT;
		break;
	case MapEnums::SEG_SKY:
		pomdy += SEGMENTS_SHIFT;
		break;
	default:
		break;
	}

	screen_position.x = (pomdx * ISO_SCENE_H_COEF * 2 + this->position.x) / this->projection_coef_h + this->viewport.width / 2;
	screen_position.y = (pomdy * ISO_SCENE_V_COEF * 2 + this->position.y + 0.5f) / this->projection_coef_v + this->viewport.height / 2;

	// flip y position
	screen_position.y = this->viewport.height - screen_position.y;
}


/*
	Finds unit under given screen position. All segments are serched if multiple segment view is active.

	@return Counted pointer to unit.
*/
inline
Unit *Map::FindPickUnit(ushort_t screen_x, ushort_t screen_y) const
{
	Unit *result = NULL;

	// find unit under cursor
	if (this->visible_segment < MapEnums::SEG_COUNT)
		result = this->FindPickUnit(this->visible_segment, screen_x, screen_y);
	else
	{
		byte_t i;
		for (i = MapEnums::SEG_COUNT; i > 0 && !result; i--)
			result = this->FindPickUnit(MapEnums::Segment(i - 1), screen_x, screen_y);
	}

	return result;
}


inline
byte_t Map::GetSurface(const MapPosition2D &position) const
{
	Assert(this->IsInMap(position));

	return this->surface[position.x][position.y];
}


inline
Map::Field *Map::GetField(const MapPosition3D &position) const
{
	Assert(this->IsInMap(position));
	Assert(position.segment < MapEnums::SEG_COUNT);

	return this->segments[position.segment].fields[position.x] + position.y;
}


#endif // __map_h__

// vim:ts=4:sw=4:
