//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file buildingunit.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/buildingunit.h"
#include "engine/prototype.h"
#include "engine/game.h"
#include "engine/selectionrenderer.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "framework/sceneobject.h"
#include "framework/gfxserver.h"

PrepareClass(BuildingUnit, "Unit", s_NewBuildingUnit, s_InitBuildingUnit);


//=========================================================================
// BuildingUnit
//=========================================================================

/**
	Constructor.
*/
BuildingUnit::BuildingUnit(const char *id) :
	Unit(id)
{
	this->type = TYPE_BUILDING;

	for (int i = 0; i < MODEL_COUNT; i++)
		this->models[i] = 0;
}


bool BuildingUnit::SetModel(const string &model_id, byte_t num_id)
{
	// convert id
	ModelType model_type;
	if (!this->StringToModelType(model_id, model_type))
	{
		LogWarning2("Undefined model id '%s' in building unit prototype: %s", model_id.c_str(), this->prototype->GetID().c_str());
		return false;
	}

	// set model
	this->models[model_type] = num_id;

	return true;
}


bool BuildingUnit::ActivateModel(ModelType model)
{
	Assert(model < MODEL_COUNT);

	byte_t model_id = this->models[model];
	return model_id ? this->scene_object->ActivateModel(model_id) : false;
}


SelectionRenderer *BuildingUnit::GetSelectionRenderer()
{
	return Game::GetInstance()->GetBuildingSelRenderer();
}


// vim:ts=4:sw=4:
