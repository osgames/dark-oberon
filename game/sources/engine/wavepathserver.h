#ifndef __wavepathserver_h__
#define __wavepathserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file wavepathserver.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/pathserver.h"


//=========================================================================
// WavePathServer
//=========================================================================

/**
	@class WavePathServer
	@ingroup Engine_Module

	Used only for testing. Very ineffecive and unfinished.
*/
    
class WavePathServer : public PathServer
{
//--- embeded
public:
	struct PathStep
	{
		float time;
		MapPosition3D position;
		MapEnums::Direction direction;
	};


//--- methods
public:
	WavePathServer(const char *id);
	virtual ~WavePathServer();

protected:
	virtual void FindPath(const PathQuery &query);
};


#endif // __wavepathserver_h__

// vim:ts=4:sw=4:
