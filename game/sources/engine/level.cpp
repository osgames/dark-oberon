//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file level.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/level.h"
#include "engine/map.h"
#include "engine/game.h"
#include "engine/profile.h"
#include "engine/player.h"
#include "engine/unit.h"
#include "framework/remoteserver.h"
#include "kernel/kernelserver.h"
#include "kernel/scriptserver.h"
#include "kernel/logserver.h"
#include "kernel/serializer.h"

PrepareScriptClass(Level, "Preloaded", s_NewLevel, s_InitLevel, s_InitLevel_cmds);


//=========================================================================
// Level
//=========================================================================

/**
	Checks whether remote server is present.
*/
Level::Level(const char *id) :
	Preloaded(id),
	name("Unnamed Level")
{
	if (!RemoteServer::GetInstance())
		ErrorMsg("RemoteServer has to be created before Level");
}


/**
	Cleares resources and loaded data.
*/
Level::~Level()
{
	this->UnloadResources();
	this->ClearHeader();
	this->ClearBody();
}


void Level::ClearHeader()
{
	this->map = NULL;
}


void Level::ClearBody()
{
	this->script_file = "";

	// delete all players
	if (this->players.IsValid())
		this->players->Release();
}


void Level::SetActive(bool active)
{
	// de/activate map
	this->map->SetActive(active);
}


void Level::Update()
{
	// update all players
	Player *player;
	for (player = (Player *)this->players->GetFront(); player; player = (Player *)player->GetNext())
		player->Update();
}


void Level::Render()
{
	Assert(this->map.IsValid());

	// render map
	this->map->Render();
}


Player *Level::NewPlayer(const string &name)
{
	Assert(!name.empty());
	Assert(!this->loaded);

	// create new object
	this->kernel_server->PushCwd(this->players);

	// generate identifier
	char txt[10];
	static byte_t num_id = 0;
	sprintf(txt, "player_%d", num_id++);
	string id = txt;

	Player *player = (Player *)this->kernel_server->New("Player", id);
	Assert(player);

	this->kernel_server->PopCwd();

	// set properties
	player->SetName(name);

	// register player as remote client
	if (!this->RegisterPlayer(player))
	{
		player->Release();
		return NULL;
	}

	return player;
}


inline
bool Level::RegisterPlayer(Player *player)
{
	Assert(RemoteServer::GetInstance() && player);

	// register player as remote client
	if (!RemoteServer::GetInstance()->RegisterClient(player))
		return false;

	LogInfo1("Player connected: %s", player->GetName().c_str());
	return true;
}


inline
bool Level::DeregisterPlayer(Player *player)
{
	Assert(RemoteServer::GetInstance() && player);

	// deregister player as remote client
	if (!RemoteServer::GetInstance()->DeregisterClient(player))
		return false;

	LogInfo1("Player disconnected: %s", player->GetName().c_str());
	return true;
}


//=========================================================================
// Tools
//=========================================================================

bool Level::AddUnitToMap(Unit *unit)
{
	Assert(this->map.IsValid());

	// add unit to global map
	if (!this->map->AddUnit(unit))
		return false;

	// ghosts are not updated in players map
	if (unit->TestState(Unit::STATE_GHOST))
		return true;

	// update local maps of all players
	Player *player;
	for (player = (Player *)this->players->GetFront(); player; player = (Player *)player->GetNext())
		player->UpdatePlayerMap(unit, true);

	return true;
}


void Level::RemoveUnitFromMap(Unit *unit)
{
	Assert(unit);
	Assert(this->map.IsValid());

	// add unit to global map
	this->map->RemoveUnit(unit);

	// ghosts are not updated in players map
	if (unit->TestState(Unit::STATE_GHOST))
		return;

	// update local maps of all players
	Player *player;
	for (player = (Player *)this->players->GetFront(); player; player = (Player *)player->GetNext())
		player->UpdatePlayerMap(unit, false);
}


//=========================================================================
// Resources
//=========================================================================

bool Level::LoadResources()
{
	// preload whole map [mandatory]
	if (!this->map->Load())
	{
		LogError1("Error loading level map in: %s", this->file_name.c_str());
		return false;
	}

	// players may be loaded automatically, we just need to save the reference
	this->players = this->Find(string("players"));

	// if there are no players defined, create storage node
	if (!this->players.IsValid())
	{
		this->kernel_server->PushCwd(this);
		this->players = this->kernel_server->New("Root", string("players"));
		this->kernel_server->PopCwd();
	}
	
	// some players were loaded from file
	else
	{
		// register players as remote clients
		Player *player, *next_player;
		for (player = (Player *)this->players->GetFront(); player && !this->active_player.IsValid(); )
		{
			next_player = (Player *)player->GetNext();

			if (!this->RegisterPlayer(player))
				player->Release();

			player = next_player;
		}

		// find active player
		for (player = (Player *)this->players->GetFront(); player && !this->active_player.IsValid(); player = (Player *)player->GetNext())
		{
			if (player->IsActive())
			{
				this->active_player = player;
				this->active_player->SetName(Game::GetInstance()->GetActiveProfile()->player.name);
			}
		}

		// set all other players as not active (there can be only one active player)
		for (; player; player = (Player *)player->GetNext())
			player->SetActive(false);
	}

	// if no active player was loaded from file, create new one
	if (!this->active_player.IsValid())
	{
		this->active_player = this->NewPlayer(Game::GetInstance()->GetActiveProfile()->player.name);
		this->active_player->SetActive(true);
	}

	// create local maps of all players and load resources
	Player *player;
	Unit *unit;
	for (player = (Player *)this->players->GetFront(); player; player = (Player *)player->GetNext())
	{
		player->LoadResources();
		player->CreatePlayerMap(this->map->GetSize());
	}

	// add all units to the map. Note: this cannot be in the previous for cycle.
	// Units that can not be added to the map are skipped
	for (player = (Player *)this->players->GetFront(); player; player = (Player *)player->GetNext())
	{
		Assert(player->units.IsValid());
		for (unit = (Unit *)player->units->GetFront(); unit; unit = (Unit *)unit->GetNext())
		{
			if (!this->AddUnitToMap(unit))
				LogWarning1("Can not add unit to the level map: %s", this->file_name.c_str());
		}
	}

	// run level script
	if (!this->script_file.empty())
	{
		string result;
		ScriptServer::GetInstance()->RunScript(this->script_file, result);
	}

	return true;
}


void Level::UnloadResources()
{
	// delete map
	if (this->map.IsValid())
		this->map->Unload();

	// deregister all players and unload their resources
	if (this->players.IsValid())
	{
		// deregister players first
		Player *player;
		for (player = (Player *)this->players->GetFront(); player; player = (Player *)player->GetNext())
			this->DeregisterPlayer(player);

		// delete local maps of all players and unload resources
		for (player = (Player *)this->players->GetFront(); player; player = (Player *)player->GetNext())
		{
			player->DeletePlayerMap();
			player->UnloadResources();
		}
	}

	this->active_player = NULL;
}


//=========================================================================
// Deserializing
//=========================================================================

bool Level::ReadHeader(Serializer &serializer)
{
	// read	name [optional]
	serializer.GetAttribute("name", this->name);

	// read	author [optional]
	if (serializer.GetGroup("author"))
	{
		serializer.GetAttribute("name", this->author_name);
		serializer.GetAttribute("contact", this->author_contact);
		serializer.EndGroup();
	}

	// read map [mandatory]
	string map_id;
	if (
		!serializer.GetGroup("map") ||
		!serializer.GetAttribute("id", map_id, false)
	)
	{
		LogError1("Missing level map in: %s", this->file_name.c_str());
		return false;
	}
	serializer.EndGroup();
	
	// find map [mandatory]
	this->map = Game::GetInstance()->GetMap(map_id);
	if (!this->map.IsValid())
	{
		LogError1("Level map does not exists in: %s", this->file_name.c_str());
		return false;
	}

	return true;
}


bool Level::ReadBody(Serializer &serializer)
{
	// read script [optional]
	if (serializer.GetGroup("script"))
	{
		if (!serializer.GetAttribute("src", this->script_file, false))
		{
			LogError1("Missing level script in: %s", this->file_name.c_str());
			return false;
		}
		serializer.EndGroup();
	}

	// call ancestor
	return Root::Deserialize(serializer, false);
}


//=========================================================================
// Serializing
//=========================================================================

bool Level::WriteHeader(Serializer &serializer)
{
	// write name
	if (!serializer.SetAttribute("name", this->name)) return false;

	// write author
	if (!serializer.AddGroup("author")) return false;
		if (!serializer.SetAttribute("name", this->author_name)) return false;
		if (!serializer.SetAttribute("contact", this->author_contact)) return false;
	serializer.EndGroup();

	/// @todo Implement
	LogError("Serializing is not implemented");

	return true;
}


bool Level::WriteBody(Serializer &serializer)
{
	/// @todo Implement
	LogError("Serializing is not implemented");
	return true;
}


// vim:ts=4:sw=4:
