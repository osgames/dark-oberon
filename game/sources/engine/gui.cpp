//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file gui.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/gui.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializer.h"

PrepareClass(Gui, "Preloaded", s_NewGui, s_InitGui);


//=========================================================================
// Gui
//=========================================================================

/**
	Constructor.
*/
Gui::Gui(const char *id) :
	Preloaded(id),
	name("Unnamed Gui")
{
	//
}


/**
	Destructor.
*/
Gui::~Gui()
{
	this->UnloadResources();
	this->ClearHeader();
	this->ClearBody();
}


void Gui::ClearHeader()
{
	//
}


void Gui::ClearBody()
{
	//
}


//=========================================================================
// Resources
//=========================================================================

bool Gui::LoadResources()
{
	return true;
}


void Gui::UnloadResources()
{
	//
}


//=========================================================================
// Deserializing
//=========================================================================

bool Gui::ReadHeader(Serializer &serializer)
{
	// read	name [optional]
	serializer.GetAttribute("name", this->name);

	// read	author [optional]
	if (serializer.GetGroup("author"))
	{
		serializer.GetAttribute("name", this->author_name);
		serializer.GetAttribute("contact", this->author_contact);
		serializer.EndGroup();
	}

	return true;
}


bool Gui::ReadBody(Serializer &serializer)
{
	// read cursors [mandatory]
	if (!this->ReadCursors(serializer))
	{
		LogError1("Error reading cursors in: %s", this->file_name.c_str());
		return false;
	}

	return true;
}


bool Gui::ReadCursors(Serializer &serializer)
{
	byte_t count;

	// read cursors [mandatory]
	if (!serializer.GetGroup("cursors"))
	{
		LogError1("Missing cursors in: %s", this->file_name.c_str());
		return false;
	}
	count = (byte_t)serializer.GetGroupsCount();
	if (!count)
	{
		LogError1("Missing cursors in: %s", this->file_name.c_str());
		return false;
	}

	// read cursors
	if (serializer.GetGroup() && serializer.CheckGroupName("cursor"))
	{
		string id, model_name;

		do {
			// read	id [mandatory]
			if (!serializer.GetAttribute("id", id, false))
			{
				LogError1("Missing cursor identifier in: %s", this->file_name.c_str());
				return false;
			}

			// read	src [mandatory]
			if (!serializer.GetAttribute("src", model_name, false))
			{
				LogError1("Missing cursor file name in: %s", this->file_name.c_str());
				return false;
			}

			this->cursors[id] = model_name;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	serializer.EndGroup();
	return true;
}


//=========================================================================
// Serializing
//=========================================================================

/**
*/
bool Gui::WriteHeader(Serializer &serializer)
{
	// write name
	if (!serializer.SetAttribute("name", this->name)) return false;

	// write author
	if (!serializer.AddGroup("author")) return false;
		if (!serializer.SetAttribute("name", this->author_name)) return false;
		if (!serializer.SetAttribute("contact", this->author_contact)) return false;
	serializer.EndGroup();

	return true;
}


bool Gui::WriteBody(Serializer &serializer)
{
	/// @todo Implement
	LogError("Serializing is not implemented");
	return true;
}


// vim:ts=4:sw=4:
