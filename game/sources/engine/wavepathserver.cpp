//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file wavepathserver.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/wavepathserver.h"
#include "engine/unit.h"
#include "engine/prototype.h"
#include "engine/map.h"
#include "engine/playermap.h"
#include "engine/game.h"
#include "kernel/kernelserver.h"
#include "kernel/profiler.h"

PrepareClass(WavePathServer, "PathServer", s_NewWavePathServer, s_InitWavePathServer);


//=========================================================================
// Defines
//=========================================================================

#define PATH_MAX_STEPS 500


//=========================================================================
// WavePathServer
//=========================================================================

WavePathServer::WavePathServer(const char *id) :
	PathServer(id)
{
	//
}


WavePathServer::~WavePathServer()
{
	//
}


inline
bool ProcessStep(const WavePathServer::PathStep &act_step, WavePathServer::PathStep &next_step, MapEnums::Direction direction, Prototype *prototype, list<WavePathServer::PathStep *> &steps, hash_map<Map::Field *, WavePathServer::PathStep> &visited_fields)
{
	Map *map = Game::GetInstance()->GetActiveMap();
	Map::Field *field = map->GetField(next_step.position);

	bool is_moveable, is_free;
	float speed, rotation;

	prototype->CheckPosition(map,
		next_step.position, direction,
		is_moveable, is_free
	);

	// check if next step is available and not processed
	if (!is_moveable || !is_free || visited_fields.find(field) != visited_fields.end())
		return false;

	// get current speeds
	prototype->GetSpeeds(map, 
		act_step.position, next_step.position.segment,
		speed, rotation
	);

	// update time and direction
	next_step.time += MapEnums::GetBaseLength(direction) / speed;
	if (direction < MapEnums::DIRECTION_COUNT - 2 && next_step.direction < MapEnums::DIRECTION_COUNT - 2)
		next_step.time += abs(MapEnums::GetRotatingSteps(next_step.direction, direction)) * 45 / rotation;
	next_step.direction = direction;

	// add step to visited fields
	visited_fields[field] = next_step;

	// add step to sorted list according to time
	list<WavePathServer::PathStep *>::iterator it;
	for (it = steps.begin(); it != steps.end(); it++)
	{
		if ((*it)->time > next_step.time)
			break;
	}
	steps.insert(it, &visited_fields[field]);
	return true;
}


void WavePathServer::FindPath(const PathQuery &query)
{
	// check if unit is still alive
	Counted::LockCounted();
	if (!query.unit.IsValid() || !query.unit->AcquirePointer())
	{
		Counted::UnlockCounted();
		return;
	}
	Counted::UnlockCounted();

	// unit is in destination
	const MapSize &unit_size = query.unit->GetPrototype()->GetSize();
	if (query.path->IsInDestination(query.unit->GetMapPosition(), unit_size))
	{
		query.unit->Release();
		query.path->SetFinished(true);
		query.path->SetReachable(true);
		return;
	}

#ifdef USE_PROFILERS
	if (this->path_profiler.IsValid())
		this->path_profiler->Start();
#endif

	Map *map = Game::GetInstance()->GetActiveMap();
	Prototype *prototype = query.unit->GetPrototype();

	hash_map<Map::Field *, PathStep> visited_fields;   // all visited fields (processed + not processed)
	list<PathStep *> steps;                            // not processed steps (at the edge of the wave)
	PathStep next_step;
	Map::Field *first_field;
	int steps_count = 0;
	bool processed = false;
	bool reached = false;

	// prepare first step
	next_step.position = query.unit->GetMapPosition();
	next_step.time = 0.0f;
	next_step.direction = query.unit->GetDirection();
	first_field = map->GetField(next_step.position);

	visited_fields[first_field] = next_step;
	steps.push_front(&visited_fields[first_field]);

	// we don't need unit anymore
	query.unit->Release();

	// find destination in wave
	while (!steps.empty() && steps_count < PATH_MAX_STEPS)
	{
		// get first step
		const PathStep &act_step = *steps.front();

		// go north
		next_step = act_step;
		if (next_step.position.y < map->GetSize().height - unit_size.height)
		{
			next_step.position.y++;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_NORTH, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go north east
		next_step = act_step;
		if (next_step.position.x < map->GetSize().width - unit_size.width && next_step.position.y < map->GetSize().height - unit_size.height)
		{
			next_step.position.x++;
			next_step.position.y++;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_NORTH_EAST, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go east
		next_step = act_step;
		if (next_step.position.x < map->GetSize().width - unit_size.width)
		{
			next_step.position.x++;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_EAST, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go south east
		next_step = act_step;
		if (next_step.position.x < map->GetSize().width - unit_size.width && next_step.position.y > 0)
		{
			next_step.position.x++;
			next_step.position.y--;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_SOUTH_EAST, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go south
		next_step = act_step;
		if (next_step.position.y > 0)
		{
			next_step.position.y--;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_SOUTH, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go south west
		next_step = act_step;
		if (next_step.position.x > 0 && next_step.position.y > 0)
		{
			next_step.position.x--;
			next_step.position.y--;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_SOUTH_WEST, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go west
		next_step = act_step;
		if (next_step.position.x > 0)
		{
			next_step.position.x--;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_WEST, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go north west
		next_step = act_step;
		if (next_step.position.x > 0 && next_step.position.y < map->GetSize().height - unit_size.height)
		{
			next_step.position.x--;
			next_step.position.y++;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_NORTH_WEST, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go up
		next_step = act_step;
		if (next_step.position.segment < MapEnums::SEG_SKY)
		{
			next_step.position.segment = next_step.position.segment == MapEnums::SEG_UNDERGROUND ? MapEnums::SEG_GROUND : MapEnums::SEG_SKY;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_UP, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go down
		next_step = act_step;
		if (next_step.position.segment > MapEnums::SEG_UNDERGROUND)
		{
			next_step.position.segment = next_step.position.segment == MapEnums::SEG_SKY ? MapEnums::SEG_GROUND : MapEnums::SEG_UNDERGROUND;
			processed = ProcessStep(act_step, next_step, MapEnums::DIRECTION_DOWN, prototype, steps, visited_fields);
			if ((reached = query.path->IsInDestination(next_step.position, unit_size)))
				break;
		}

		// go to next step
		steps.pop_front();
		steps_count++;
	}

	// find the closest step to the destination
	PathStep *closest_step = NULL;

	// if we have reached the destination and the destination was not processed, closest step is at the beginning of the list
	if (reached && !processed)
		closest_step = steps.front();

	// if reached destination was processed, find it
	else if (reached && processed)
	{
		list<PathStep *>::iterator step_it;

		for (step_it = steps.begin(); step_it != steps.end(); step_it++)
		{
			if (query.path->IsInDestination((*step_it)->position, unit_size))
			{
				closest_step = *step_it;
				break;
			}
		}
	}

	// destination was not not reached, find closest step
	else
	{
		const MapArea &dest_area = query.path->dest_area;
		hash_map<Map::Field *, PathStep>::iterator field_it;
		float distance, closest_distance = 99999.0f;
		bool left_x, left_y;
		bool in_x, in_y;
		mapel_t distance_x, distance_y;

		for (field_it = visited_fields.begin(); field_it != visited_fields.end(); field_it++)
		{
			const MapPosition2D &act_position = field_it->second.position;

			// calculate distance
			left_x = act_position.x + unit_size.width <= dest_area.x;
			left_y = act_position.y + unit_size.height <= dest_area.y;
			in_x = !left_x && act_position.x < dest_area.x + dest_area.width;
			in_y = !left_y && act_position.y < dest_area.y + dest_area.height;
			
			if (in_x && in_y)
				distance = 0.0f;

			else
			{
				distance_x = in_x ? 0 : (left_x ? dest_area.x - act_position.x - unit_size.width + 1 : act_position.x - dest_area.x - dest_area.width + 1);
				distance_y = in_y ? 0 : (left_y ? dest_area.y - act_position.y - unit_size.height + 1 : act_position.y - dest_area.y - dest_area.height + 1);

				if (in_x)
					distance = float(distance_y);

				else if (in_y)
					distance = float(distance_x);

				else
					distance = n_sqrt(n_sqr(float(distance_x)) + n_sqr(float(distance_y)));
			}
			distance += abs(field_it->second.position.segment - query.path->dest_segment);

			// update minimum
			if (distance < closest_distance || (distance == closest_distance && field_it->second.time < closest_step->time))
			{
				closest_distance = distance;
				closest_step = &field_it->second;
			}
		}
	}

	// path was not found
	if (!closest_step)
	{
		// set whether path is complete
		query.path->SetFinished(true);
		query.path->SetReachable(reached);

#ifdef USE_PROFILERS
	if (this->path_profiler.IsValid())
		this->path_profiler->Stop();
#endif
		return;
	}

	// search the shortest way from the destination to the start position
	// (just follow the directions)
	Map::Field *field = map->GetField(closest_step->position);
	hash_map<Map::Field *, PathStep>::iterator field_it;

	while (field != first_field)
	{
		// add step to final path
		field_it = visited_fields.find(field);
		PathStep &act_step = field_it->second;
		query.path->PushDirection(act_step.direction);

		// move to next step
		switch (act_step.direction)
		{
		case MapEnums::DIRECTION_NORTH:       act_step.position.y--; break;
		case MapEnums::DIRECTION_NORTH_EAST:  act_step.position.x--; act_step.position.y--; break;
		case MapEnums::DIRECTION_EAST:        act_step.position.x--; break;
		case MapEnums::DIRECTION_SOUTH_EAST:  act_step.position.x--; act_step.position.y++; break;
		case MapEnums::DIRECTION_SOUTH:       act_step.position.y++; break;
		case MapEnums::DIRECTION_SOUTH_WEST:  act_step.position.x++; act_step.position.y++; break;
		case MapEnums::DIRECTION_WEST:        act_step.position.x++; break;
		case MapEnums::DIRECTION_NORTH_WEST:  act_step.position.x++; act_step.position.y--; break;
		case MapEnums::DIRECTION_UP:          act_step.position.segment = act_step.position.segment == MapEnums::SEG_SKY ? MapEnums::SEG_GROUND : MapEnums::SEG_UNDERGROUND; break;
		case MapEnums::DIRECTION_DOWN:        act_step.position.segment = act_step.position.segment == MapEnums::SEG_UNDERGROUND ? MapEnums::SEG_GROUND : MapEnums::SEG_SKY; break;
		default:                          ErrorMsg("Invalid direction"); break;
		}

		field = map->GetField(act_step.position);
	}

	// set whether path is complete
	query.path->SetFinished(reached || query.path->IsEmpty());
	query.path->SetReachable(reached || !query.path->IsEmpty());

#ifdef USE_PROFILERS
	if (this->path_profiler.IsValid())
		this->path_profiler->Stop();
#endif
}


// vim:ts=4:sw=4:
