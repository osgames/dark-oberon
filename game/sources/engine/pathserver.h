#ifndef __pathserver_h__
#define __pathserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file pathserver.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/mapstructures.h"
#include "engine/path.h"
#include "engine/pathmessages.h"
#include "kernel/rootserver.h"
#include "kernel/ref.h"

class Unit;
class ThreadPool;
class Profiler;


//=========================================================================
// PathServer
//=========================================================================

/**
	@class PathServer
	@ingroup Engine_Module
*/
    
class PathServer : public RootServer<PathServer>, public MessageReceiver<PathRequest>
{
//--- embeded
protected:
	struct PathQuery
	{
		uint_t request_id;
		Ref<Root> sender;
		Ref<Unit> unit;
		Path *path;

		PathQuery();
		~PathQuery();
	};


//--- methods
public:
	PathServer(const char *id);
	virtual ~PathServer();

	bool Init();
	void Done();

protected:
	void OnMessage(PathRequest *request);

	static void ProcessQuery(Thread *thread);
	static void CancelQuery(void *data);
	virtual void FindPath(const PathQuery &query);

//--- valiables
protected:
	ThreadPool *threads;

#ifdef USE_PROFILERS
	Ref<Profiler> path_profiler;  ///< Profiler for pathfinding.
#endif
};


//=========================================================================
// Methods
//=========================================================================

inline
PathServer::PathQuery::PathQuery()
{
	this->path = NEW Path();
}


inline
PathServer::PathQuery::~PathQuery()
{
	delete this->path;
}


#endif // __pathserver_h__

// vim:ts=4:sw=4:
