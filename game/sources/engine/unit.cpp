//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file unit.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/unit.h"
#include "engine/forceunit.h"
#include "engine/prototype.h"
#include "engine/player.h"
#include "engine/playermap.h"
#include "engine/game.h"
#include "engine/map.h"
#include "engine/level.h"
#include "engine/race.h"
#include "engine/selection.h"
#include "engine/selectionrenderer.h"
#include "engine/unitmessages.h"
#include "engine/pathserver.h"
#include "engine/onscreentext.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/timeserver.h"
#include "kernel/serializer.h"
#include "framework/sceneserver.h"
#include "framework/remoteserver.h"
#include "framework/gfxserver.h"

PrepareScriptClass(Unit, "Root", s_NewUnit, s_InitUnit, s_InitUnit_cmds);


//=========================================================================
// Defines
//=========================================================================

#define UNIT_NEXT_STEP_WAITING  0.4    ///< How long will unit wait before next check of position engaged by another moving unit.
#define UNIT_NEXT_STEP_LIMIT    5      ///< Maximal number of next step iterations.
#define UNIT_MAX_MESSAGE_LENGTH 255    ///< Maximal length of message that can be printed to the OST as unit speach.


//=========================================================================
// Static
//=========================================================================

const char *Unit::state_description[STATE_COUNT] = 
{
	"STATE_NONE",
	"STATE_STAYING",
	"STATE_NEXT_STEP",
	"STATE_NEXT_STEP_WAITING",
	"STATE_MOVING",
	"STATE_ROTATING_LEFT",
	"STATE_ROTATING_RIGHT",
	"STATE_IS_BEING_BUILT",
	"STATE_NEXT_HIDING",
	"STATE_HIDING",
	"STATE_NEXT_EJECTING",
	"STATE_DYING",
	"STATE_ZOMBIE",
	"STATE_DELETE",
	"STATE_GHOST"
};


//=========================================================================
// Unit
//=========================================================================

/**
	Direction of unit is set to random number.
*/
Unit::Unit(const char *id) :
	Root(id),
	remote_id(0),
	type(TYPE_NONE),
	state(STATE_STAYING),
	scene_object(NULL),
	meeting_object(NULL),
	life(0.0f),
	life_percent(0.0f),
	speed(0.0f),
	rotation(0.0f),
	move_direction(MapEnums::DIRECTION_COUNT),
	aggressivity(AGGRESSIVITY_IGNORE),
	hiding_count(0),
	hiding_count_my(0),
	ghost_owner(NULL),
	sequence(0),
	path_request_id(0),
	path(NULL),
	state_time(0.0)
{
	direction = MapEnums::GetRandomDirection();
}


/**
	Releases scene objects, ghost owner, path and all waiting events.
*/
Unit::~Unit()
{
	// release objects
	SafeRelease(this->scene_object);
	SafeRelease(this->meeting_object);
	SafeRelease(this->ghost_owner);
	delete this->path;

	// release waiting events
	StateEvent *pevent;
	while (pevent = (StateEvent *)this->waiting_queue.PopFront())
		pevent->Release();
}


void Unit::SetState(State state)
{
	Assert(state < STATE_COUNT);
	Assert(this->state != state);
	//LogDebug3("unit: %u/%u, state: %s", this->player->GetRemoteID(), this->remote_id, this->StateToString(state));
	this->state = state;
}


void Unit::Say(bool possitive, const char *message, ...) const
{
	if (!this->IsMy())
		return;

	static char buffer1[UNIT_MAX_MESSAGE_LENGTH + 1];
	static char buffer2[UNIT_MAX_MESSAGE_LENGTH + 1];

	va_list arg;
	va_start(arg, message);

    snprintf(buffer1, UNIT_MAX_MESSAGE_LENGTH, "%s: %s", this->prototype->GetName().c_str(), message);
	buffer1[UNIT_MAX_MESSAGE_LENGTH] = '\0';

	vsnprintf(buffer2, UNIT_MAX_MESSAGE_LENGTH, buffer1, arg);
	buffer2[UNIT_MAX_MESSAGE_LENGTH] = '\0';

	va_end(arg);

	static const vector3 color_info(COLOR_INFO);
	static const vector3 color_warning(COLOR_WARNING);

	Game::GetInstance()->GetOnScreenText()->AddMessage(buffer2, possitive ? color_info : color_warning);
}


bool Unit::SetPrototype(Prototype *prototype)
{
	Assert(prototype);

	bool had_prototype = this->prototype.IsValid();

	// set new prototype
	this->prototype = prototype;

	// create new scene object
	this->scene_object = SceneServer::GetInstance()->NewSceneObject("SceneObject");
	if (!this->scene_object)
	{
		LogError1("Error creating scene object of unit: %s", this->id.c_str());
		return false;
	}

	const MapSize &my_size = this->prototype->GetSize();

	// set properties
	this->scene_object->SetOverlayColor(this->player->GetColor());
	this->scene_object->SetPosition(this->real_position);
	this->scene_object->SetSize(vector2(my_size.width, my_size.height));
	this->scene_object->SetRenderCallbacks(this->OnRenderBegin, this->OnRenderEnd);
	this->scene_object->SetData(this);

	// add all models and store numerical identifiers
	const Prototype::ModelsArray &models = this->prototype->GetModels();
	Prototype::ModelsArray::const_iterator it;
	byte_t num_id;

	for (it = models.begin(); it != models.end(); it++)
	{
		num_id = this->scene_object->AddModel(it->file_name);
		this->SetModel(it->id, num_id);
	}

	// load resources of scene object
	if (!this->scene_object->LoadResources())
	{
		LogError1("Error loading scene object of unit: %s", this->id.c_str());
		return false;
	}

	// create scene object for meeting point
	if (this->prototype->CanHide())
	{
		Race *race = Game::GetInstance()->GetRace(this->GetPlayer()->GetRaceId());

		this->meeting_object = SceneServer::GetInstance()->NewSceneObject("SceneObject", &race->GetMeetingModel());
		if (!this->scene_object)
		{
			LogError1("Error creating scene object for meeting point of unit: %s", this->id.c_str());
			return false;
		}

		// set properties
		this->meeting_object->SetOverlayColor(this->player->GetColor());
		this->meeting_object->SetSize(vector2(0.0f, 0.0f));

		// load resources of scene object
		if (!this->meeting_object->LoadResources())
		{
			LogError("Error loading scene object of meeting point");
			return false;
		}
	}

	// set life to maximum if prototype is the first one or unit has full life
	if (!had_prototype)
		this->life = (this->life_percent ? this->life_percent : 1.0f) * this->prototype->GetLife();

	else if (this->life == this->prototype->GetLife())
		this->life = this->prototype->GetLife();

	this->life_percent = this->life / this->prototype->GetLife();

	// set properties
	this->SetAggressivity(had_prototype ? this->aggressivity : this->prototype->GetAggressivity());

	return true;
}


/*
	@note Map position of unit is not set together with real position.
*/
void Unit::SetRealPosition(float x, float y)
{
	this->real_position.x = x;
	this->real_position.y = y;

	if (this->scene_object)
		this->scene_object->SetPosition(this->real_position);
}


void Unit::SetLife(float life)
{
	Assert(this->prototype);
	this->life = n_clamp(life, 0.0f, this->prototype->GetLife());
	this->life_percent = this->life / this->prototype->GetLife();
}


float Unit::GetHidingPercent() const
{
	Assert(this->prototype);
	return float(this->hiding_count) / this->prototype->GetHidingCapacity();
}


void Unit::GetCenterPosition(vector2 &position) const
{
	Assert(this->prototype);
	position.x = this->map_position.x + this->prototype->GetSize().width / 2.0f;
	position.y = this->map_position.y + this->prototype->GetSize().height / 2.0f;
}


/**
	The method sets new unit's aggressivity.
	If given aggressivity is not compatible, the highest possible is set.

	@return New aggressivity of the unit.
*/
Unit::Aggressivity Unit::SetAggressivity(Aggressivity aggressivity)
{
	switch (aggressivity)
	{
	case AGGRESSIVITY_OFFENSIVE:
	case AGGRESSIVITY_GUARD:
		if (this->prototype->CanMove() && this->prototype->CanAttack())
		{
			this->aggressivity = aggressivity;
			break;
		}

	case AGGRESSIVITY_PASSIVE:
		if (this->prototype->CanAttack())
		{
			this->aggressivity = AGGRESSIVITY_PASSIVE;
			break;
		}

	default:
		this->aggressivity = AGGRESSIVITY_IGNORE;
		break;
	}

	return this->aggressivity;
}


void Unit::SetMeetingPoint(const MapPosition3D &meeting_point)
{
	if (!this->prototype->CanHide())
		return;

	const MapSize &my_size = this->prototype->GetSize();

	// if new meeting point is inside unit, reset it to unit position
	if (meeting_point.x >= this->map_position.x && meeting_point.x < this->map_position.x + my_size.width
		&& meeting_point.y >= this->map_position.y && meeting_point.y < this->map_position.y + my_size.height
	)
	{
		this->meeting_point = this->map_position;
		this->ShowMeetingPoint(false);
	}

	// set new meeting point
	else
	{
		this->meeting_point = meeting_point;

		if (this->IsFlags(FLAG_SELECTED || FLAG_UNDER_CURSOR))
			this->ShowMeetingPoint(true);
	}
}


void Unit::ShowMeetingPoint(bool show) const
{
	if (!this->meeting_object || this->meeting_point == this->map_position)
		return;

	Map *map = Game::GetInstance()->GetActiveMap();
	Assert(map);

	if (show)
	{
		this->meeting_object->SetPosition(vector3(this->meeting_point.x, this->meeting_point.y, 0.0f));
		map->AddSceneObject(this->meeting_point.segment, this->meeting_object);
	}
	else
		map->RemoveSceneObject(this->meeting_point.segment, this->meeting_object);
}


bool Unit::SetModel(const string &model_id, byte_t num_id)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Unit::ActivateModel(ModelType model)
{
	Assert(model < MODEL_COUNT);

	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Unit::IsMy() const
{
	return this->player->IsActive();
}


bool Unit::IsFull() const
{
	return this->hiding_count == this->prototype->GetHidingCapacity();
}


bool Unit::CanHide(const ForceUnit *unit) const
{
	Assert(unit && unit != this);

	const MapSize &unit_size = unit->GetPrototype()->GetSize();

	return this->prototype->CanHide(unit->GetPrototype())
		&& this->hiding_count + unit_size.width * unit_size.height <= this->prototype->GetHidingCapacity();
}


/**
	Simply adds unit into the list. No checks and actions are done here.
	Use #CanHide method to test if unit can be hide to this one.
*/
void Unit::AddToHidingList(ForceUnit *unit)
{
	Assert(unit);
	const MapSize &unit_size = unit->GetPrototype()->GetSize();

	this->hiding_units.push_back(unit);
	this->hiding_count += unit_size.width * unit_size.height;

	if (unit->IsMy())
		this->hiding_count_my++;
}


/**
	Simply removes unit from list. No other actions are done here.
*/
void Unit::RemoveFromHidingList(ForceUnit *unit)
{
	Assert(unit);
	const MapSize &unit_size = unit->GetPrototype()->GetSize();

	HidingList::iterator it;
	for (it = this->hiding_units.begin(); it != this->hiding_units.end(); it++)
	{
		if (*it == unit)
		{
			this->hiding_units.erase(it);
			this->hiding_count -= unit_size.width * unit_size.height;
			if (unit->IsMy())
				this->hiding_count_my--;
			return;
		}
	}
}


void Unit::RenderPred()
{
	if (this->TestState(STATE_GHOST))
	{
		GfxServer::GetInstance()->SetColorFilter(COLOR_FILTER_GHOST);
		return;
	}

	if (this->IsAnyFlag(Unit::FLAG_SELECTED | Unit::FLAG_UNDER_CURSOR))
	{
		static SelectionRenderer *renderer;
		renderer = this->GetSelectionRenderer();
		if (renderer)
			renderer->RenderBackground(this);
	}
}


void Unit::RenderPost()
{
	if (this->TestState(STATE_GHOST))
	{
		GfxServer::GetInstance()->SetColorFilter(1.0f, 1.0f, 1.0f, 1.0f);
		return;
	}

	if (this->IsAnyFlag(Unit::FLAG_SELECTED | Unit::FLAG_UNDER_CURSOR))
	{
		static SelectionRenderer *renderer;
		renderer = this->GetSelectionRenderer();
		if (renderer)
			renderer->RenderForeground(this);
	}
}


SelectionRenderer *Unit::GetSelectionRenderer()
{
	return Game::GetInstance()->GetUnitSelRenderer();
}


void Unit::OnRenderBegin(void *data)
{
	if (data)
		((Unit *)data)->RenderPred();
}


void Unit::OnRenderEnd(void *data)
{
	if (data)
		((Unit *)data)->RenderPost();
}


//=========================================================================
// Tools
//=========================================================================

bool Unit::IsAroundUnit(const Unit *unit) const
{
	// units are in the same segment
	if (unit->GetMapPosition().segment == this->map_position.segment)
	{
		const MapPosition3D &his_position = unit->GetMapPosition();
		const MapSize &his_size = unit->GetPrototype()->GetSize();

		// check if we are next to the unit
		MapArea area(
			his_position.x > 0 ? his_position.x - 1 : his_position.x, 
			his_position.y > 0 ? his_position.y - 1 : his_position.y,
			his_position.x > 0 ? his_size.width + 2 : his_size.width + 1,
			his_position.y > 0 ? his_size.height + 2 : his_size.height + 1
		);

		return area.IsIntersectingArea(this->map_position, this->GetPrototype()->GetSize());
	}

	// we are over/under the unit
	else if (n_abs(unit->GetMapPosition().segment - this->map_position.segment) == 1)
	{
		const MapPosition3D &his_position = unit->GetMapPosition();
		const MapSize &his_size = unit->GetPrototype()->GetSize();

		// check if we intersect with the unit
		MapArea area(his_position.x, his_position.y, his_size.width, his_size.height);

		return area.IsIntersectingArea(this->map_position, this->GetPrototype()->GetSize());
	}
	
	// units segments are too far
	return false;
}


bool Unit::FindFreePositionAroundUnit(const Unit *unit, MapPosition3D &free_position, MapEnums::Direction &free_direction) const
{
	MapPosition3D start_position, act_position;
	bool is_moveable, is_free;
	const MapSize &my_size = this->prototype->GetSize();

	Map *map = Game::GetInstance()->GetActiveMap();

	// get first ejecting position according to meeting point
	unit->GetFirstEjectingPosition(start_position, my_size);
	act_position = start_position;

	do
	{
		if (map->IsInMap(act_position, my_size))
		{
			// if actual position is free, we are finished
			this->prototype->CheckPosition(map, act_position, MapEnums::DIRECTION_UP, is_moveable, is_free);
			if (is_moveable && is_free)
			{
				vector2 my_center, unit_center;
				this->GetCenterPosition(my_center);
				unit->GetCenterPosition(unit_center);

				free_position = act_position;
				free_direction = MapEnums::GetOpositeDirection(MapEnums::GetDirection(my_center, unit_center));
				return true;
			}
		}

		// get next ejecting position around unit
		unit->GetNextEjectingPosition(act_position, my_size);
	}
	while (act_position != start_position);

	return false;
}


void Unit::GetFirstEjectingPosition(MapPosition3D &position, const MapSize &unit_size) const
{
	position.segment = this->map_position.segment;
	const MapSize &my_size = this->prototype->GetSize();

	// if meeting point is not set, ejecting position will be next to south-west corner
	if (this->meeting_point == this->map_position)
	{
		position.x = unit_size.width <= this->map_position.x ? this->map_position.x - unit_size.width : this->map_position.x + my_size.width;
		position.y = unit_size.height <= this->map_position.y ? this->map_position.y - unit_size.height : this->map_position.y + my_size.height;
		return;
	}

	// meeting point is set, find closest position
	if (this->meeting_point.x < this->map_position.x)
		position.x = this->map_position.x - unit_size.width;
	else if (this->meeting_point.x >= this->map_position.x + my_size.width)
		position.x = this->map_position.x + my_size.width;
	else
		position.x = this->meeting_point.x;

	if (this->meeting_point.y < this->map_position.y)
		position.y = this->map_position.y - unit_size.height;
	else if (this->meeting_point.y >= this->map_position.y + my_size.height)
		position.y = this->map_position.y + my_size.height;
	else
		position.y = this->meeting_point.y;
}


void Unit::GetNextEjectingPosition(MapPosition3D &position, const MapSize &unit_size) const
{
	const MapSize &my_size = this->prototype->GetSize();

	// position is under unit, try to move right
	if (position.y + unit_size.height <= this->map_position.y)
	{
		if (position.x < this->map_position.x + my_size.width)
			position.x++;
		else
			position.y++;
	}

	// position is over unit, try to move left
	else if (position.y >= this->map_position.y + my_size.height)
	{
		if (position.x + unit_size.width > this->map_position.x)
		{
			if (position.x)
				position.x--;
			else if (unit_size.height <= this->map_position.y)
				position.y = this->map_position.y - unit_size.height;
			else
			{
				position.x = this->map_position.x + my_size.width;
				position.y = 0;
			}
		}
		else
			position.y--;
	}

	// position is left, try to move down
	else if (position.x < this->map_position.x)
	{
		if (position.y)
			position.y--;
		else
			position.x = this->map_position.x + my_size.width;
	}

	// position is right, move up
	else
		position.y++;
}


bool Unit::StringToModelType(const string &name, ModelType &type)
{
	if (name == "stay")          type = MODEL_STAY;
	else if (name == "move")     type = MODEL_MOVE;
	else if (name == "attack")   type = MODEL_ATTACK;
	else if (name == "build")    type = MODEL_BUILD;
	else if (name == "repair")   type = MODEL_REPAIR;
	else if (name == "mine")     type = MODEL_MINE;
	else if (name == "zombie")   type = MODEL_ZOMBIE;
	else
	{
		LogWarning1("Invalid model type: %s", name.c_str());
		return false;
	}

	return true;
}


bool Unit::ModelTypeToString(ModelType type, string &name)
{
	switch (type)
	{
	case MODEL_STAY:    name = "stay";    return true;
	case MODEL_MOVE:    name = "move";    return true;
	case MODEL_ATTACK:  name = "attack";  return true;
	case MODEL_BUILD:   name = "build";   return true;
	case MODEL_REPAIR:  name = "repair";  return true;
	case MODEL_MINE:    name = "mine";    return true;
	case MODEL_ZOMBIE:  name = "zombie";  return true;
	default:
		LogWarning1("Invalid mdel type: %d", type);
		return false;
	}
}


const char *Unit::StateToString(State state)
{
	Assert(state < STATE_COUNT);
	return this->state_description[state];
}


//=========================================================================
// Per frame updating
//=========================================================================

void Unit::Update(bool &create_ghost, bool &to_delete)
{
	// deleted unit is not updated
	if (this->TestState(STATE_DELETE))
	{
		to_delete = true;
		return;
	}

	// update visibility
	bool visible = !Game::GetInstance()->GetActiveMap()->IsWarfogEnabled() || this->IsMy() || this->hiding_count_my ||
		Game::GetInstance()->GetActivePlayer()->GetPlayerMap()->IsAreaVisible(this->map_position, this->prototype->GetSize());

	// non-moveble unit becomes invisible, create ghost
	create_ghost = this->IsFlags(FLAG_VISIBLE) && !visible && !this->prototype->CanMove();

	// visible ghosts are deleted (but state is not set to STATE_DELETE)
	if (this->TestState(STATE_GHOST))
	{
		to_delete = visible;
		return;
	}

	to_delete = false;

	// set visibility
	if (visible)
		this->SetFlags(FLAG_VISIBLE);
	else
		this->ClearFlags(FLAG_VISIBLE);
	this->scene_object->SetVisible(visible);

	// unselect unit if it is under warfog
	if (!this->IsFlags(FLAG_VISIBLE) && this->IsFlags(FLAG_SELECTED))
		Game::GetInstance()->GetSelection()->RemoveUnit(this);

	// update real position if unit is visible
	if (this->TestState(STATE_MOVING) && this->state_time)
	{
		this->state_time -= TimeServer::GetInstance()->GetFrameTime();
		if (this->state_time < 0.0)
			state_time = 0.0;

		if (visible)
		{
			float x_coef, y_coef;
			MapEnums::GetBaseCoefs(this->move_direction, x_coef, y_coef);
			float d = state_time ? float(this->speed * state_time) : 0.0f;
			this->SetRealPosition(this->map_position.x - d * x_coef, this->map_position.y - d * y_coef);
		}
	}
}


//=========================================================================
// Actions
//=========================================================================

bool Unit::StartMoving(Unit *unit, bool stop_action)
{
	Assert(unit);

	// fill destination area with unit position and size
	MapArea dest_area;
	dest_area.Set(unit->GetMapPosition().x, unit->GetMapPosition().y, 
		unit->GetPrototype()->GetSize().width, unit->GetPrototype()->GetSize().height
	);

	// move to this position and remember destination unit
	return this->StartMoving(dest_area, unit->GetMapPosition().segment, unit, stop_action);
}


bool Unit::StartMoving(const MapPosition3D &dest_position, bool stop_action)
{
	Assert(dest_position.segment < MapEnums::SEG_COUNT);

	// fill destination area
	MapArea dest_area;
	dest_area.Set(dest_position.x, dest_position.y, 1, 1);

	// move to this position and remember destination unit
	return this->StartMoving(dest_area, dest_position.segment, NULL, stop_action);
}


bool Unit::StartMoving(const MapArea &dest_area, MapEnums::Segment dest_segment, Unit *unit, bool stop_action)
{
	Assert(dest_area.width && dest_area.height);
	Assert(dest_segment < MapEnums::SEG_COUNT);

	// check if unit can move
	if (!this->prototype->CanMove())
		return false;

	// create path request
	PathRequest *request = NEW PathRequest();

	request->unit = this;
	request->dest_unit = unit;
	request->dest_area = dest_area;
	request->dest_segment = dest_segment;

	// stop current action
	if (stop_action)
		this->StopAction();

	// or just stop moving
	else
		SafeDelete(this->path);

	// send path request
	this->path_request_id = request->GetRequestId();
	this->SendMessage(request, PathServer::GetInstance());

	return true;
}


bool Unit::StartEjecting()
{
	// check if we can hide
	if (!this->prototype->CanHide())
		return false;

	// check if there are some hided units
	if (!this->hiding_count_my)
		return true;

	// eject units
	HidingList::iterator it;
	StateEvent *pevent;
	for (it = this->hiding_units.begin(); it != this->hiding_units.end(); it++)
	{
		// eject only my units
		if (!(*it)->IsMy())
			continue;

		pevent = NEW StateEvent();
		pevent->state = STATE_NEXT_EJECTING;
		this->SendMessage(pevent, *it);
	}

	return true;
}


void Unit::StopAction()
{
	// stop moving
	SafeDelete(this->path);
}


//=========================================================================
// Messages
//=========================================================================

void Unit::OnMessage(ActionEvent *pevent)
{
	Assert(pevent && pevent->destination_id == this->remote_id);

	Player *player = pevent->player_id ? (Player *)RemoteServer::GetInstance()->GetClient(pevent->player_id) : NULL;
	Unit *unit = player && pevent->unit_id ? player->GetUnit(pevent->unit_id) : NULL;
	
	switch (pevent->action)
	{
	case ACTION_MOVE:
		if (unit)
			this->StartMoving(unit);
		else
			this->StartMoving(pevent->position);
		break;

	case ACTION_EJECT:
		this->StartEjecting();
		break;

	default:
		ErrorMsg("Unhandled action event");
		break;
	}
}


bool Unit::PreProcessEvent(StateEvent *pevent)
{
	Assert(pevent);

	// if unit is not idle, event with new sequence must wait until current sequence is finished
	if (pevent->GetSequence() != this->sequence && !this->IsIdle())
	{
		this->waiting_queue.PushBack(pevent);
		return false;
	}

	// update current sequence
	this->sequence = pevent->GetSequence();

	return true;
}


void Unit::ProcessEvent(StateEvent *pevent)
{
	Assert(pevent);

	// process event
	switch (pevent->state)
	{
	case STATE_STAYING:
		// correct real position after movement
		this->SetRealPosition(this->map_position.x, this->map_position.y);
		break;

	case STATE_NEXT_STEP:
		{
			// if there is no next step in the path, stop
			if (!this->path || this->path->IsEmpty())
			{
				SafeDelete(this->path);
				pevent->state = STATE_STAYING;
				this->SendMessage(pevent, this);
				break;
			}

			// check if we need to rotate
			MapEnums::Direction next_direction = this->direction;
			bool rotating_left = false;
			this->path->GetNextDirection(next_direction, rotating_left);
			if (next_direction != this->direction)
			{
				pevent->state = rotating_left ? STATE_ROTATING_LEFT : STATE_ROTATING_RIGHT;
				pevent->direction = next_direction;
				this->SendMessage(pevent, this);
				break;
			}

			// get next position from path
			MapPosition3D next_position = this->map_position;

			this->path->GetNextPosition(next_position, next_direction);

			// check if next position is available
			bool is_moveable, is_free;

			this->prototype->CheckPosition(Game::GetInstance()->GetActiveMap(), 
				next_position, this->direction,
				is_moveable, is_free
			);

			// if next position is not moveable for this unit, find another
			if (!is_moveable)
			{
				if (this->path->IsInDestination(next_position, this->GetPrototype()->GetSize()))
				{
					SafeDelete(this->path);
					pevent->state = STATE_STAYING;
					this->SendMessage(pevent, this);
				}
				else
				{
					this->StartMoving(this->path->dest_area, this->path->dest_segment, this->path->dest_unit.GetUnsafe(), false);
					pevent->state = STATE_STAYING;
					this->SendMessage(pevent, this);
				}
				break;
			}

			// if next position is engaged by moving unit, wait for a while
			if (!is_free)
			{
				// if unit cannot wait more, find another way
				if (pevent->next_step_counter >= UNIT_NEXT_STEP_LIMIT)
				{
					LogWarning("stop");
					this->StartMoving(this->path->dest_area, this->path->dest_segment, this->path->dest_unit.GetUnsafe(), false);
					pevent->state = STATE_STAYING;
					this->SendMessage(pevent, this);
					break;
				}

				// wait for a while
				pevent->state = STATE_NEXT_STEP_WAITING;
				this->SendMessage(pevent, this);
				break;
			}

			// move unit to next position
			this->path->PopDirection();

			pevent->state = STATE_MOVING;
			pevent->position = next_position;
			pevent->direction = next_direction;
			this->SendMessage(pevent, this);
			break;
		}

	case STATE_NEXT_STEP_WAITING:
		// increase waiting counter and try to do next step after a while
		pevent->next_step_counter++;
		pevent->state = STATE_NEXT_STEP;
		this->SendMessage(pevent, this, UNIT_NEXT_STEP_WAITING);
		break;

	case STATE_ROTATING_LEFT:
	case STATE_ROTATING_RIGHT:
		{
			// get speeds for current position
			this->prototype->GetSpeeds(Game::GetInstance()->GetActiveMap(), 
				this->map_position, this->map_position.segment,
				this->speed, this->rotation
			);
			Assert(this->rotation);

			// rotate
			this->SetDirection(pevent->direction);

			// do the next step
			pevent->state = STATE_NEXT_STEP;
			pevent->next_step_counter = 0;
			this->SendMessage(pevent, this, 45.0f / this->rotation);
		}
		break;

	case STATE_MOVING:
		{
			// get speeds for current position
			this->prototype->GetSpeeds(Game::GetInstance()->GetActiveMap(),
				this->map_position, pevent->position.segment,
				this->speed, this->rotation
			);
			Assert(this->speed);

			// move unit to next position
			Game::GetInstance()->GetActiveLevel()->RemoveUnitFromMap(this);  /// @todo Removing unit from map and adding back while moving is really ineffecive.
			this->SetMapPosition(pevent->position, false);
			Verify(Game::GetInstance()->GetActiveLevel()->AddUnitToMap(this));

			// set direction of movement (can be up or down)
			this->move_direction = pevent->direction;

			// synchronise looking direction (cannot be up or down)
			if (pevent->direction < MapEnums::DIRECTION_COUNT - 2)
				this->direction = pevent->direction;

			// do the next step
			this->state_time = MapEnums::GetBaseLength(this->move_direction) / this->speed;
			pevent->next_step_counter = 0;
			pevent->state = STATE_NEXT_STEP;
			this->SendMessage(pevent, this, this->state_time);

			// if path is empty now and unit is not in destination, compute next part of path
			if (this->path->IsEmpty() && !this->path->IsFinished())
				this->StartMoving(this->path->dest_area, this->path->dest_segment, this->path->dest_unit.GetUnsafe(), false);
		}
		break;

	default:
		ErrorMsg("Unhandled state event");
		break;
	}
}


void Unit::PostProcessEvent()
{
	// if unit is idle, resent all waiting messages
	if (this->IsIdle())
	{
		BaseMessage *message;
		while ((message = this->waiting_queue.PopFront()))
			this->SendMessage(message, this);
	}
}


void Unit::OnMessage(StateEvent *pevent)
{
	Assert(pevent);

	// unit state cannot be changed while unit is updated in main thread (visibility, real positions, ...)
	// we lock whole list of units, because it is not good idea to have a lock for each unit
	this->GetParent()->Lock();

	// check if we can process event first
	if (!this->PreProcessEvent(pevent))
		return;

	// set new state
	this->SetState(pevent->state);

	// process event
	this->ProcessEvent(pevent);

	// finish processing
	this->PostProcessEvent();

	this->GetParent()->Unlock();
}


void Unit::OnMessage(InputEvent *pevent)
{
	Assert(pevent && pevent->destination_id == this->remote_id);
	
	switch (pevent->input)
	{
	case INPUT_MEETING_POINT:
		this->SetMeetingPoint(pevent->position);
		break;

	default:
		ErrorMsg("Unhandled input event");
		break;
	}
}


void Unit::OnMessage(PathResponse *response)
{
	Assert(response);
	Assert(response->unit.GetUnsafe() == this);
	Assert(response->path);

	// check if we are waiting for this response
	if (response->GetRequestId() != this->path_request_id)
		return;

	// if destination is not reachable, stop current action (but move to the closest position)
	if (!response->path->IsReachable())
		this->StopAction();

	// if unit is currently moving, change the path silently
	if (this->IsMoving(this->path_request_id))
		SafeDelete(this->path);
	else
	{
		Assert(!this->path);
		StateEvent *pevent = NEW StateEvent();
		pevent->state = STATE_NEXT_STEP;
		pevent->next_step_counter = 0;

		this->SendMessage(pevent, this);
	}

	// set new path
	this->path_request_id = 0;
	this->path = response->path;
	response->path = NULL;
}


//=========================================================================
// Deserializing
//=========================================================================

bool Unit::Deserialize(Serializer &serializer, bool first)
{
	// read	prototype [mandatory]
	if (!serializer.GetAttribute("prototype", this->prototype_id, false))
	{
		LogError1("Error reading prototype of unit: %s", this->id.c_str());
		return false;
	}

	// read	life [optional]
	serializer.GetAttributePercent("life", this->life_percent);

	// read	position [mandatory]
	if (!this->ReadPosition(serializer))
	{
		LogError1("Error reading position of unit: %s", this->id.c_str());
		return false;
	}
	
	// call ancestor
	return Root::Deserialize(serializer, first);
}


bool Unit::ReadPosition(Serializer &serializer)
{
	// read	position [mandatory]
	if (!serializer.GetGroup("position"))
	{
		LogError1("Missing position of unit: %s", this->id.c_str());
		return false;
	}

	MapPosition3D position;
	string segment_id;
	if (!serializer.GetAttribute("x", position.x)
		|| !serializer.GetAttribute("y", position.y)
		|| !serializer.GetAttribute("segment", segment_id, false)
	)
	{
		LogError1("Missing position of unit: %s", this->id.c_str());
		return false;
	}

	// convert segment id [mandatory]
	if (!MapEnums::StringToSegment(segment_id, position.segment))
	{
		LogError1("Wrong segment of unit: %s", this->id.c_str());
		return false;
	}

	this->SetMapPosition(position);

	serializer.EndGroup();
	return true;
}


//=========================================================================
// Serializing
//=========================================================================

/**
	@todo Implement.
*/
bool Unit::Serialize(Serializer &serializer)
{
	// call ancestor
	return Root::Serialize(serializer);
}


// vim:ts=4:sw=4:
