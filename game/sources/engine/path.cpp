//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file path.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/path.h"
#include "engine/unit.h"
#include "engine/prototype.h"


//=========================================================================
// Path
//=========================================================================

inline
void Path::UpdateDestination()
{
	// update destination area if real destination is an unit
	if (this->dest_unit.IsValid())
	{
		// get unit area
		MapArea area;
		area.Set(this->dest_unit->GetMapPosition().x, this->dest_unit->GetMapPosition().y, 
			this->dest_unit->GetPrototype()->GetSize().width, this->dest_unit->GetPrototype()->GetSize().height
		);

		// if unit has moved, set new destination area
		if (area != this->dest_area || this->dest_unit->GetMapPosition().segment != this->dest_segment)
		{
			this->dest_area = area;
			this->dest_segment = this->dest_unit->GetMapPosition().segment;
			this->finished = false;
			this->reachable = true;
		}
	}
}


bool Path::IsInDestination(const MapPosition3D &position, const MapSize &size)
{
	this->UpdateDestination();

	return this->dest_area.IsIntersectingArea(position, size) && position.segment == this->dest_segment;
}


bool Path::IsFinished()
{
	Assert(this->IsEmpty());

	this->UpdateDestination();

	return this->finished;
}


bool Path::IsReachable()
{
	this->UpdateDestination();

	return this->reachable;
}


// vim:ts=4:sw=4:
