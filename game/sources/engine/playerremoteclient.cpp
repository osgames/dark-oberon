//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file playerremoteclient.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/playerremoteclient.h"
#include "engine/player.h"
#include "engine/unit.h"
#include "kernel/kernelserver.h"

PrepareClass(PlayerRemoteClient, "RemoteClient", s_NewPlayerRemoteClient, s_InitPlayerRemoteClient);


//=========================================================================
// PlayerRemoteClient
//=========================================================================

PlayerRemoteClient::PlayerRemoteClient(const char *id) :
	RemoteClient(id)
{
	//
}


void PlayerRemoteClient::SendRemoteMessageToUnit(BaseMessage *message, Unit *destination, double delay)
{
	Assert(destination);

	Player *player = destination->GetPlayer();
	Assert(player);

	// if player is local, we can send message directly to the unit
	if (player->IsLocal())
		this->SendMessage(message, destination, delay);

	// else we need to send message remotely
	else
		this->SendRemoteMessage(message, player, delay);
}


// vim:ts=4:sw=4:
