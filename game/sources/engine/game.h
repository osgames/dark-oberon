#ifndef __game_h__
#define __game_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file game.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/mouseinfo.h"
#include "kernel/rootserver.h"
#include "kernel/ref.h"
#include "kernel/logserver.h"
#include "framework/inputtypes.h"


class FileServer;
class SerializeServer;
class GfxServer;
class SceneServer;
class TimeServer;
class MessageServer;
class InputServer;
class PathServer;
class AudioServer;
class RemoteServer;

class Preloaded;
class Profile;
class Gui;
class Level;
class Map;
class Scheme;
class Race;
class Font;
class Profiler;
class OnScreenText;
class Player;
class Unit;
class SelectionRenderer;
class Selection;


//=========================================================================
// Game
//=========================================================================

/**
	@class Game
	@ingroup Engine_Module

	@brief Opens application window a perform main game loop.
*/

class Game : public RootServer<Game>
{
//--- methods
public:
	Game(const char *id);
	virtual ~Game();

	// engine
	bool Open();
	bool Run();
	void Close();

	bool IsRun() const;
	void Quit();

	void SaveScreenshot();

	// properties
	void SetTitle(const string &title);
	const string &GetTitle() const;
	Font *GetSystemFont() const;
	OnScreenText *GetOnScreenText() const;
	Selection *GetSelection() const;
	const MouseInfo &GetMouseInfo() const;

	// acive objects
	bool ActivateProfile(const string &id);
	bool ActivateGui(const string &id);
	bool ActivateLevel(const string &id);

	Profile *GetActiveProfile() const;
	Gui *GetActiveGui() const;
	Level *GetActiveLevel() const;
	Map *GetActiveMap() const;
	Player *GetActivePlayer() const;

	Profile *GetProfile(const string &id) const;
	Gui *GetGui(const string &id) const;
	Level *GetLevel(const string &id) const;
	Map *GetMap(const string &id) const;
	Scheme *GetScheme(const string &id) const;
	Race *GetRace(const string &id) const;

	// selection renderers
	bool SetUnitSelRenderer(const string &class_name);
	bool SetBuildingSelRenderer(const string &class_name);
	SelectionRenderer *GetUnitSelRenderer() const;
	SelectionRenderer *GetBuildingSelRenderer() const;

protected:
	// preloaded objects
	Preloaded *NewPreloaded(const char *class_name, Root *root, const string &file_name);

	// profiles
	bool LoadProfiles();
	bool ActivateProfile(Profile *profile);
	void CreateDefaultProfile();
	bool ActivateLastProfile();

	// GUI
	bool LoadGuis();
	bool ActivateGui(Gui *gui);

	// levels
	bool LoadLevels();
	bool ActivateLevel(Level *level);

	// other preloaded objects
	bool LoadMaps();
	bool LoadSchemes();
	bool LoadRaces();

	// selection renderers
	SelectionRenderer *NewSelRenderer(const string &class_name);

	// callbacks
	static void OnSize(ushort_t width, ushort_t height, bool fullscreen);
	static bool OnKey(KeyCode code, KeyState state);
	static bool OnAction(const string &action, int position, void *data);
	static void OnLog(LogServer::LogLevel level, const string &file_name, int line, const string &text);

private:
	void Update();
	void Render();
	void UpdateOstDimensions();


//--- variables
protected:
	string title;     ///< Game title.
	bool opened;      ///< Whether game is opened.
	bool run;         ///< Whether game is running.

	Ref<FileServer> file_server;              ///< Pointer to file server.
	Ref<SerializeServer> serialize_server;    ///< Pointer to serialize server.
	Ref<GfxServer> gfx_server;                ///< Pointer to gfx server.
	Ref<SceneServer> scene_server;            ///< Pointer to scene server.
	Ref<TimeServer> time_server;              ///< Pointer to time server.
	Ref<MessageServer> message_server;        ///< Pointer to message server.
	Ref<InputServer> input_server;            ///< Pointer to input server.
	Ref<PathServer> path_server;              ///< Pointer to path server.
	Ref<AudioServer> audio_server;            ///< Pointer to audio server.
	Ref<RemoteServer> remote_server;          ///< Pointer to remote server.

	Ref<Root> variables;            ///< Root node of all system variables.
	Ref<Root> profiles;             ///< Root node of all profiles.
	Ref<Root> guis;                 ///< Root node of all GUIs.
	Ref<Root> maps;                 ///< Root node of all maps.
	Ref<Root> schemes;              ///< Root node of all schemes.
	Ref<Root> races;                ///< Root node of all races.
	Ref<Root> levels;               ///< Root node of all levels.
	Ref<Root> sel_renderers;        ///< Root node of all selection renderers. In fact, there will be only two renderers: for units and for buildings.

	Ref<Font> system_font;          ///< System font.
	Ref<Profile> active_profile;    ///< Active profile.
	Ref<Gui> active_gui;            ///< Active GUI.
	Ref<Level> active_level;        ///< Active level.
	Ref<Map> active_map;            ///< Map of active level.
	Ref<Player> active_player;      ///< Active player.

	Ref<SelectionRenderer> unit_sel_renderer;       ///< Active selection renderer for units.
	Ref<SelectionRenderer> building_sel_renderer;   ///< Active selection renderer for buildings;

#ifdef USE_PROFILERS
	Ref<Profiler> render_profiler;  ///< Profiler for rendering.
	Ref<Profiler> pick_profiler;    ///< Profiler for picking.
	Ref<Profiler> buffers_profiler; ///< Profiler for switching video buffers.
#endif

	MouseInfo mouse_info;
	Ref<OnScreenText> ost;          ///< Pointer to on-screen-text object.
	Ref<Selection> selection;       ///< Pointer to selection object.
};


//=========================================================================
// Methods
//=========================================================================

/**
	Returns game title.

	@return Game title.
*/
inline
const string &Game::GetTitle() const
{
	return this->title;
}


inline
Font *Game::GetSystemFont() const
{
	return this->system_font.Get();
}


inline
OnScreenText *Game::GetOnScreenText() const
{
	return this->ost;
}


inline
Selection *Game::GetSelection() const
{
	return this->selection;
}


inline
const MouseInfo &Game::GetMouseInfo() const
{
	return this->mouse_info;
}


/**
	Returns running state.
	@return Whether game is running.
*/
inline
bool Game::IsRun() const
{
	return this->run;
}


inline
Profile *Game::GetActiveProfile() const
{
	return this->active_profile;
}


inline
Gui *Game::GetActiveGui() const
{
	return this->active_gui;
}


inline
Level *Game::GetActiveLevel() const
{
	return this->active_level;
}


inline
Map *Game::GetActiveMap() const
{
	return this->active_map;
}


inline
Player *Game::GetActivePlayer() const
{
	return this->active_player;
}


inline
Profile *Game::GetProfile(const string &id) const
{
	return (Profile *)this->profiles->Find(id);
}


inline
Gui *Game::GetGui(const string &id) const
{
	return (Gui *)this->guis->Find(id);
}


inline
Level *Game::GetLevel(const string &id) const
{
	return (Level *)this->levels->Find(id);
}


inline
Map *Game::GetMap(const string &id) const
{
	return (Map *)this->maps->Find(id);
}


inline
Scheme *Game::GetScheme(const string &id) const
{
	return (Scheme *)this->schemes->Find(id);
}


inline
Race *Game::GetRace(const string &id) const
{
	return (Race *)this->races->Find(id);
}


inline
SelectionRenderer *Game::GetUnitSelRenderer() const
{
	return this->unit_sel_renderer.GetUnsafe();
}


inline
SelectionRenderer *Game::GetBuildingSelRenderer() const
{
	return this->building_sel_renderer.GetUnsafe();
}


#endif // __game_h__

// vim:ts=4:sw=4:
