//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file unit_cmds.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/unit.h"


//=========================================================================
// Commands
//=========================================================================

static void s_SetMapPosition(void *slf, Cmd *cmd)
{
	Unit *self = (Unit *)slf;

	MapPosition3D position;
	position.x = cmd->In()->GetI();
	position.y = cmd->In()->GetI();
	if (!MapEnums::StringToSegment(string(cmd->In()->GetS()), position.segment))
		return;

	self->SetMapPosition(position);
}


void s_InitUnit_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("v_SetMapPosition_iis", 'SMPO', s_SetMapPosition);
	
	cl->EndCmds();
}

// vim:ts=4:sw=4:
