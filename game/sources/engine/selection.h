#ifndef __selection_h__
#define __selection_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file selection.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/unit.h"
#include "engine/playerremoteclient.h"

class Prototype;


//=========================================================================
// Selection
//=========================================================================

/**
	@class Selection
	@ingroup Engine_Module
*/

class Selection : public PlayerRemoteClient
{
//--- embeded
protected:
	typedef list<Unit *> UnitsList;


//--- methods
public:
	Selection(const char *id);

	void SelectUnit(Unit *unit);
	void AddUnit(Unit *unit);
	void RemoveUnit(Unit *unit);
	void AddRemoveUnit(Unit *unit);
	void UnselectAll();
	void Update();

	void SetSelectedAction(Unit::Action action);
	Unit::Action GetSelectedAction() const;
	Unit::Action GetAction() const;
	Unit::Aggressivity GetAggressivity() const;

	void Say(bool possitive, const char *message) const;

	// properties of selected units
	bool IsMy() const;
	bool CanMove() const;
	bool CanAttack() const;
	bool CanMine() const;
	bool CanRepair() const;
	bool CanBuild() const;
	bool CanHide() const;

	// actions
	void StartMoving(Unit *unit);
	void StartMoving(const MapPosition3D &dest_position);
	void StartHiding(Unit *unit);
	void StartEjecting();
	void SetMeetingPoint(const MapPosition3D &meeting_point);

	bool CanHideToUnit(const Unit *unit) const;


//--- variables
protected:
	UnitsList units;
	Unit::Action selected_action;
	
	Unit::Type units_type;
	Unit::Action action;
	Unit::Aggressivity aggressivity;
	Prototype *builder;

	// properties of selected units
	struct
	{
		bool is_my : 1;
		bool can_move : 1;
		bool can_attack : 1;
		bool can_mine : 1;
		bool can_repair : 1;
		bool can_build : 1;
		bool can_hide : 1;
	};
};


//=========================================================================
// Methods
//=========================================================================

inline
void Selection::SelectUnit(Unit *unit)
{
	Assert(unit);

	this->Lock();
	this->UnselectAll();
	this->AddUnit(unit);
	this->Unlock();
}


inline
void Selection::SetSelectedAction(Unit::Action action)
{
	this->selected_action = action;
}


inline
Unit::Action Selection::GetSelectedAction() const
{
	return this->selected_action;
}


inline
Unit::Action Selection::GetAction() const
{
	return this->action;
}


inline
Unit::Aggressivity Selection::GetAggressivity() const
{
	return this->aggressivity;
}


inline
void Selection::Say(bool possitive, const char *message) const
{
	if (!this->units.empty())
		this->units.front()->Say(possitive, message);
}


inline
bool Selection::IsMy() const
{
	return this->is_my;
}


inline
bool Selection::CanMove() const
{
	return this->can_move;
}


inline
bool Selection::CanAttack() const
{
	return this->can_attack;
}


inline
bool Selection::CanMine() const
{
	return this->can_mine;
}


inline
bool Selection::CanRepair() const
{
	return this->can_repair;
}


inline
bool Selection::CanBuild() const
{
	return this->can_build;
}


inline
bool Selection::CanHide() const
{
	return this->can_hide;
}


#endif // __selection_h__

// vim:ts=4:sw=4:
