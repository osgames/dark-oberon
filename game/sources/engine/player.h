#ifndef __player_h__
#define __player_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file player.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/mapstructures.h"
#include "engine/playerremoteclient.h"
#include "kernel/messagereceiver.h"
#include "kernel/ref.h"
#include "kernel/vector3.h"

class Race;
class Unit;
class Prototype;
class PlayerMap;
class ActionEvent;
class InputEvent;


//=========================================================================
// Player
//=========================================================================

/**
	@class Player
	@ingroup Engine_Module
*/
    
class Player : public PlayerRemoteClient,
	public MessageReceiver<ActionEvent>,
	public MessageReceiver<InputEvent>
{
	friend class Level;

//--- embeded
private:
	typedef hash_map<ushort_t, Unit *> UnitsMap;


//--- methods
public:
	Player(const char *id);
	virtual ~Player();

	const string &GetName() const;
	void SetName(const string &name);
	const string &GetRaceId() const;
	void SetRaceId(const string &race_id);
	const vector3 &GetColor() const;
	void SetColor(const vector3 &color);
	void SetActive(bool active);
	bool IsActive() const;
	bool IsHyper() const;

	bool LoadResources();
	void UnloadResources();

	void CreatePlayerMap(const MapSize &map_size);
	void DeletePlayerMap();
	bool UpdatePlayerMap(const Unit *unit, bool added);
	PlayerMap *GetPlayerMap() const;

	Unit *NewUnit(const string &prototype_id);
	Unit *NewUnit(Prototype *prototype);

	bool RegisterUnit(Unit *unit);
	bool DeregisterUnit(Unit *unit);
	Unit *GetUnit(ushort_t remote_id) const;

	void Update();

protected:
	// messages
	virtual void OnMessage(ActionEvent *pevent);
	virtual void OnMessage(InputEvent *pevent);

	// serializing
	virtual bool Serialize(Serializer &serializer);
	virtual bool Deserialize(Serializer &serializer, bool first);


//--- variables
protected:
	string name;                 ///< Full player name.
	string race_id;              ///< Race selected during game establishing.
	vector3 color;               ///< Color selected during game establishing.

	Ref<Race> race;              ///< Player's race.
	Ref<Root> units;             ///< Root node of all units owned by player.
	UnitsMap units_map;          ///< Hash map for translation remote identifiers of units.

	bool active;                 ///< Where player is active (= is myself).
	PlayerMap *map;              ///< Local map of player that contains information like warfog etc.
};


//=========================================================================
// Methods
//=========================================================================

inline
const string &Player::GetName() const
{
	return this->name;
}


inline
void Player::SetName(const string &name)
{
	this->name = name;
}


inline
const string &Player::GetRaceId() const
{
	return this->race_id;
}


inline
void Player::SetRaceId(const string &race_id)
{
	this->race_id = race_id;
}


inline
const vector3 &Player::GetColor() const
{
	return this->color;
}


inline
void Player::SetColor(const vector3 &color)
{
	this->color = color;
}


inline
PlayerMap *Player::GetPlayerMap() const
{
	return this->map;
}


inline
void Player::SetActive(bool active)
{
	this->active = active;
}


inline
bool Player::IsActive() const
{
	return this->active;
}


inline
bool Player::IsHyper() const
{
	/// @todo Detect hyper player.
	return false;
}


inline
Unit *Player::GetUnit(ushort_t remote_id) const
{
	Assert(remote_id);
	UnitsMap::const_iterator itUnit = this->units_map.find(remote_id);
	return itUnit == this->units_map.end() ? NULL : itUnit->second;
}


#endif // __player_h__

// vim:ts=4:sw=4:
