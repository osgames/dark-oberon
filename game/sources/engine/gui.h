#ifndef __gui_h__
#define __gui_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file gui.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/preloaded.h"


//=========================================================================
// Constants
//=========================================================================

#define GUI_DEFAULT_ID string("classic")


//=========================================================================
// Gui
//=========================================================================

/**
	@class Gui
	@ingroup Engine_Module

	@todo Maybe move this class to Framework.
*/
    
class Gui : public Preloaded
{
//--- embeded
private:
	typedef hash_map<string, string> CursorsMap;


//--- methods
public:
	Gui(const char *id);
	virtual ~Gui();

	const string &GetCursor(const string &id) const;
	byte_t GetCursorsCount() const;

protected:
	virtual void ClearHeader();
	virtual void ClearBody();

	virtual bool LoadResources();
	virtual void UnloadResources();

	// serializing
	virtual bool ReadHeader(Serializer &serializer);
	virtual bool ReadBody(Serializer &serializer);
	virtual bool WriteHeader(Serializer &serializer);
	virtual bool WriteBody(Serializer &serializer);

	bool ReadCursors(Serializer &serializer);


//--- variables
private:
	// GUI header
	string name;                      ///< Full GUI name.
	string author_name;
	string author_contact;

	// GUI body
	CursorsMap cursors;
};


//=========================================================================
// Mathods
//=========================================================================

inline
const string &Gui::GetCursor(const string &id) const
{
	Assert(!this->cursors.empty());

	CursorsMap::const_iterator it = this->cursors.find(id);
	return it == this->cursors.end() ? this->cursors.begin()->second : it->second;
}


inline
byte_t Gui::GetCursorsCount() const
{
	return (byte_t)this->cursors.size();
}



#endif // __gui_h__

// vim:ts=4:sw=4:
