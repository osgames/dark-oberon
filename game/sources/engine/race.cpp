//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file race.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/race.h"
#include "engine/prototype.h"
#include "engine/game.h"
#include "engine/scheme.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializer.h"

PrepareClass(Race, "Preloaded", s_NewRace, s_InitRace);


//=========================================================================
// Race
//=========================================================================

/**
	Constructor.
*/
Race::Race(const char *id) :
	Preloaded(id),
	name("Unnamed Race")
{
	//
}


/**
	Destructor.
*/
Race::~Race()
{
	this->UnloadResources();
	this->ClearHeader();
	this->ClearBody();
}


void Race::ClearHeader()
{
	//
}


void Race::ClearBody()
{
	// unload scheme
	if (this->scheme.IsValid())
		this->scheme->Unload();

	// clear prototypes
	if (this->prototypes.IsValid())
	{
		this->prototypes->Release();
		this->prototypes = NULL;
	}
}


//=========================================================================
// Resources
//=========================================================================

bool Race::LoadResources()
{
	return true;
}


void Race::UnloadResources()
{
	//
}


//=========================================================================
// Deserializing
//=========================================================================

bool Race::ReadHeader(Serializer &serializer)
{
	// read	name [optional]
	serializer.GetAttribute("name", this->name);

	// read	author [optional]
	if (serializer.GetGroup("author"))
	{
		serializer.GetAttribute("name", this->author_name);
		serializer.GetAttribute("contact", this->author_contact);
		serializer.EndGroup();
	}

	// read scheme [mandatory]
	string scheme_id;
	if (
		!serializer.GetGroup("scheme") ||
		!serializer.GetAttribute("id", scheme_id, false)
	)
	{
		LogError1("Missing race scheme in: %s", this->file_name.c_str());
		return false;
	}
	serializer.EndGroup();
	
	// find scheme [mandatory]
	this->scheme = Game::GetInstance()->GetScheme(scheme_id);
	if (!this->scheme.IsValid())
	{
		LogError1("Race scheme does not exists: %s", scheme_id.c_str());
		return false;
	}

	return true;
}


bool Race::ReadBody(Serializer &serializer)
{
	// preload whole scheme [mandatory]
	if (!this->scheme->Load())
	{
		LogError1("Error loading race scheme in: %s", this->file_name.c_str());
		return false;
	}

	// read meeting point [mandatory]
	if (!this->ReadMeetingPoint(serializer))
	{
		LogError1("Error reading meeting-point in: %s", this->file_name.c_str());
		return false;
	}

	/// @todo Reading common textures.
	/// @todo Reading shared sounds.

	// call ancestor
	if (!Root::Deserialize(serializer, false))
		return false;

	// prototypes are loaded automatically, we just need to save the reference
	this->prototypes = this->Find(string("prototypes"));
	return true;
}


bool Race::ReadMeetingPoint(Serializer &serializer)
{
	// read	meeting point [mandatory]
	if (!serializer.GetGroup("meeting-point"))
	{
		LogError1("Missing meeting point in: %s", this->file_name.c_str());
		return false;
	}

	if (!serializer.GetAttribute("src", this->meeting_model, false))
	{
		LogError1("Missing source of meeting point model in: %s", this->file_name.c_str());
		return false;
	}

	serializer.EndGroup();
	return true;
}


//=========================================================================
// Serializing
//=========================================================================

/**
*/
bool Race::WriteHeader(Serializer &serializer)
{
	// write name
	if (!serializer.SetAttribute("name", this->name)) return false;

	// write author
	if (!serializer.AddGroup("author")) return false;
		if (!serializer.SetAttribute("name", this->author_name)) return false;
		if (!serializer.SetAttribute("contact", this->author_contact)) return false;
	serializer.EndGroup();

	return true;
}


bool Race::WriteBody(Serializer &serializer)
{
	/// @todo Implement
	LogError("Serializing is not implemented");
	return true;
}


// vim:ts=4:sw=4:
