#ifndef __unit_h__
#define __unit_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file unit.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/mapstructures.h"
#include "kernel/root.h"
#include "kernel/ref.h"
#include "kernel/vector.h"
#include "kernel/flagged.h"
#include "kernel/messagereceiver.h"
#include "kernel/messages.h"

class Prototype;
class SceneObject;
class StateEvent;
class ActionEvent;
class InputEvent;
class PathResponse;
class ForceUnit;
class Path;
class SelectionRenderer;


//=========================================================================
// Unit
//=========================================================================

/**
	@class Unit
	@ingroup Engine_Module
*/

class Unit : public Root, public Flagged8, 
	public MessageReceiver<ActionEvent>, 
	public MessageReceiver<StateEvent>,
	public MessageReceiver<InputEvent>,
	public MessageReceiver<PathResponse>
{
	friend class Player;

//--- embeded
public:
	enum Type
	{
		TYPE_NONE,
		TYPE_FORCE,
		TYPE_BUILDING,
		TYPE_SOURCE
	};

	enum State
	{
		// idle
		STATE_NONE,
		STATE_STAYING,

		// moving
		STATE_NEXT_STEP,
		STATE_NEXT_STEP_WAITING,
		STATE_MOVING,
		STATE_ROTATING_LEFT,
		STATE_ROTATING_RIGHT,

		// building
		STATE_IS_BEING_BUILT,

		// hiding
		STATE_NEXT_HIDING,
		STATE_HIDING,

		// ejecting
		STATE_NEXT_EJECTING,

		// death
		STATE_DYING,
		STATE_ZOMBIE,
		STATE_DELETE,

		// ghost
		STATE_GHOST,

		STATE_COUNT
	};

	enum Flag
	{
		FLAG_NONE         = 0,
		FLAG_SELECTED     = (1 << 0),   ///< Whether unit is selected.
		FLAG_UNDER_CURSOR = (1 << 1),   ///< Whether unit is under mouse cursor (pick unit).
		FLAG_VISIBLE      = (1 << 2),   ///< Whether unit is visible (= not under warfog).
		FLAG_PROCESSED    = (1 << 3),
		FLAG_ALL          = (FLAG_SELECTED | FLAG_UNDER_CURSOR | FLAG_VISIBLE | FLAG_PROCESSED)
	};

	enum Action
	{
		ACTION_NONE,
		ACTION_STAY,
		ACTION_MOVE,
		ACTION_ATTACK,
		ACTION_MINE,
		ACTION_REPAIR,
		ACTION_BUILD,
		ACTION_HIDE,
		ACTION_EJECT,
	};

	enum Input
	{
		INPUT_NONE,
		INPUT_MEETING_POINT
	};

	/// Aggresivity of the unit when detects enemy.
	enum Aggressivity
	{
		AGGRESSIVITY_IGNORE,        ///< Ignores enemy - doesn't take any action.
		AGGRESSIVITY_PASSIVE,       ///< Attack enemy when it is possible to aim him but doesn't follow him.
		AGGRESSIVITY_GUARD,         ///< Attack and follow enemy, but not too far. Come back if run away.
		AGGRESSIVITY_OFFENSIVE      ///< Attack and follow enemy if run away.
	};

	enum ModelType
	{
		MODEL_STAY,
		MODEL_MOVE,
		MODEL_ATTACK,
		MODEL_BUILD,
		MODEL_REPAIR,
		MODEL_MINE,
		MODEL_ZOMBIE,
		MODEL_COUNT
	};

private:
	typedef list<ForceUnit *> HidingList;


//--- methods
public:
	Unit(const char *id);
	virtual ~Unit();

	void Say(bool possitive, const char *message, ...) const;

	// properties
	ushort_t GetRemoteID() const;
	Type GetType() const;
	bool TestType(Type type) const;
	State GetState() const;
	bool TestState(State state) const;
	void SetLife(float life);
	float GetLife() const;
	float GetLifePercent() const;
	Player *GetPlayer() const;
	Prototype *GetPrototype() const;
	bool SetPrototype(Prototype *prototype);
	const MapPosition3D &GetMapPosition() const;
	void SetMapPosition(const MapPosition3D &position, bool change_real = true);
	const vector3 &GetRealPosition() const;
	void SetRealPosition(float x, float y);
	MapEnums::Direction GetDirection() const;
	void SetDirection(MapEnums::Direction direction);
	void GetCenterPosition(vector2 &position) const;
	bool GetMeetingPoint(MapPosition3D &meeting_point) const;
	SceneObject *GetSceneObject() const;
	Aggressivity SetAggressivity(Aggressivity aggressivity);
	Aggressivity GetAggressivity() const;
	bool TestAggressivity(Aggressivity aggressivity) const;
	float GetHidingPercent() const;
	byte_t GetHidingCount() const;

	// shortcuts
	bool IsMy() const;
	bool IsIdle() const;
	bool IsMoving(int path_request_id = 0) const;
	bool IsFull() const;

	// rendering
	void Update(bool &create_ghost, bool &delete_unit);

	// actions
	Action GetAction() const;
	bool TestAction(Action action) const;

	bool CanEject() const;
	bool CanHide(const ForceUnit *unit) const;
	void AddToHidingList(ForceUnit *unit);
	void RemoveFromHidingList(ForceUnit *unit);
	void ShowMeetingPoint(bool show) const;

	static bool StringToModelType(const string &name, ModelType &type);
	static bool ModelTypeToString(ModelType type, string &name);

protected:
	void SetState(State state);	
	bool PreProcessEvent(StateEvent *pevent);
	virtual void ProcessEvent(StateEvent *pevent);
	void PostProcessEvent();
	void SetGhostOwner(Unit *owner);

	// actions
	bool StartMoving(Unit *unit, bool stop_action = true);
	bool StartMoving(const MapPosition3D &dest_position, bool stop_action = true);
	bool StartEjecting();
	virtual void StopAction();
	void SetMeetingPoint(const MapPosition3D &meeting_point);

	// messages
	virtual void OnMessage(ActionEvent *pevent);
	virtual void OnMessage(StateEvent *pevent);
	virtual void OnMessage(InputEvent *pevent);
	virtual void OnMessage(PathResponse *response);

	// tools
	bool IsAroundUnit(const Unit *unit) const;
	bool FindFreePositionAroundUnit(const Unit *unit, MapPosition3D &free_position, MapEnums::Direction &free_direction) const;
	void GetFirstEjectingPosition(MapPosition3D &position, const MapSize &unit_size) const;
	void GetNextEjectingPosition(MapPosition3D &position, const MapSize &unit_size) const;
	const char *StateToString(State state);

	virtual bool SetModel(const string &model_id, byte_t num_id);
	virtual bool ActivateModel(ModelType model);
	virtual void RenderPred();
	virtual void RenderPost();
	virtual SelectionRenderer *GetSelectionRenderer();

	static void OnRenderBegin(void *data);
	static void OnRenderEnd(void *data);

	// serializing
	virtual bool Serialize(Serializer &serializer);
	virtual bool Deserialize(Serializer &serializer, bool first);

	bool ReadPosition(Serializer &serializer);

private:
	bool StartMoving(const MapArea &dest_area, MapEnums::Segment dest_segment, Unit *unit, bool stop_action);


//--- variables
protected:
	ushort_t remote_id;

	Type type;
	State state;                      ///< State of the unit. Can be set only by unit itself.
	Ref<Player> player;
	string prototype_id;
	Ref<Prototype> prototype;
	SceneObject *scene_object;
	SceneObject *meeting_object;

	// poperties
	float life;
	float life_percent;
	MapPosition3D map_position;
	vector3 real_position;
	MapEnums::Direction direction;    ///< Look direction.
	Aggressivity aggressivity;
	HidingList hiding_units;
	byte_t hiding_count;              ///< Total number of fields engaged by hiding units.
	byte_t hiding_count_my;           ///< Number of my units that are hiding here.
	MapPosition3D meeting_point;
	Unit *ghost_owner;

	// waiting events
	List<BaseMessage> waiting_queue;

	// temporary
	uint_t sequence;
	uint_t path_request_id;
	Path *path;
	float speed;                        ///< Current unit speed. This value is valid only if unit has STATE_MOVING state.
	float rotation;                     ///< Current unit rotation speed. This value is valid only if unit has STATE_ROTATING_* state.
	MapEnums::Direction move_direction; ///< Direction of unit movement. This value is valid only if unit has STATE_MOVING state.
	double state_time;

	static const char *state_description[STATE_COUNT];
};


//=========================================================================
// Methods
//=========================================================================

inline
ushort_t Unit::GetRemoteID() const
{
	return this->remote_id;
}


inline
Unit::Type Unit::GetType() const
{
	return this->type;
}


inline
bool Unit::TestType(Type type) const
{
	return this->type == type;
}


inline
Unit::State Unit::GetState() const
{
	return this->state;
}


inline
bool Unit::TestState(State state) const
{
	Assert(state < STATE_COUNT);
	return this->state == state;
}


inline
float Unit::GetLife() const
{
	return this->life;
}


inline
float Unit::GetLifePercent() const
{
	return this->life_percent;
}


inline
Prototype *Unit::GetPrototype() const
{
	return this->prototype.GetUnsafe();
}


inline
Player *Unit::GetPlayer() const
{
	return this->player.GetUnsafe();
}


inline
const MapPosition3D &Unit::GetMapPosition() const
{
	return this->map_position;
}


/**
	Sets new map position. Real position is changed only if second paramater is set to @c true.
	Meeting point is reset to the same value as map position.
*/
inline
void Unit::SetMapPosition(const MapPosition3D &position, bool change_real)
{
	this->map_position = position;
	this->meeting_point = position;
	if (change_real)
		this->SetRealPosition(float(position.x), float(position.y));
}


inline
const vector3 &Unit::GetRealPosition() const
{
	return this->real_position;
}


inline
MapEnums::Direction Unit::GetDirection() const
{
	return this->direction;
}


inline
void Unit::SetDirection(MapEnums::Direction direction)
{
	Assert(direction < MapEnums::DIRECTION_COUNT);
	this->direction = direction;
}


inline
bool Unit::GetMeetingPoint(MapPosition3D &meeting_point) const
{
	if (this->meeting_point == this->map_position)
		return false;

	meeting_point = this->meeting_point;
	return true;
}


inline
SceneObject *Unit::GetSceneObject() const
{
	return this->scene_object;
}


inline
Unit::Aggressivity Unit::GetAggressivity() const
{
	return this->aggressivity;
}


inline
bool Unit::TestAggressivity(Unit::Aggressivity aggressivity) const
{
	return this->aggressivity == aggressivity;
}


inline
byte_t Unit::GetHidingCount() const
{
	return this->hiding_count;
}


inline
void Unit::SetGhostOwner(Unit *owner)
{
	Assert(owner);
	Assert(this->TestState(STATE_GHOST));

	this->ghost_owner = (Unit *)owner->AcquirePointer();
}


inline
bool Unit::IsIdle() const
{
	return this->TestState(STATE_STAYING) || this->TestState(STATE_HIDING);
}


inline
bool Unit::IsMoving(int path_request_id) const
{
	return this->path_request_id != path_request_id
		|| this->TestState(STATE_MOVING) || this->TestState(STATE_NEXT_STEP) || this->TestState(STATE_NEXT_STEP_WAITING)
		|| this->TestState(STATE_ROTATING_LEFT) || this->TestState(STATE_ROTATING_RIGHT);
}


inline
Unit::Action Unit::GetAction() const
{
	// @todo target -> ACTION_ATTACK, repair_unit -> ACTION_REPAIR, source -> ACTION_MINE
	if (this->IsMoving())
		return ACTION_MOVE;
	else
		return ACTION_STAY;
}


inline
bool Unit::TestAction(Unit::Action action) const
{
	return this->GetAction() == action;
}


inline
bool Unit::CanEject() const
{
	return this->hiding_count_my > 0;
}


#endif // __unit_h__

// vim:ts=4:sw=4:
