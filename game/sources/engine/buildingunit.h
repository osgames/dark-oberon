#ifndef __buildingunit_h__
#define __buildingunit_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file buildingunit.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/unit.h"


//=========================================================================
// BuildingUnit
//=========================================================================

/**
	@class BuildingUnit
	@ingroup Engine_Module
*/
    
class BuildingUnit : public Unit
{
//--- variables
protected:
	byte_t models[MODEL_COUNT];

//--- methods
public:
	BuildingUnit(const char *id);

protected:
	virtual bool SetModel(const string &model_id, byte_t num_id);
	virtual bool ActivateModel(ModelType model);

	virtual SelectionRenderer *GetSelectionRenderer();
};


#endif // __buildingunit_h__

// vim:ts=4:sw=4:
