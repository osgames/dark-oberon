#ifndef __rectselrenderer_h__
#define __rectselrenderer_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file rectselrenderer.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/selectionrenderer.h"


//=========================================================================
// RectSelRenderer
//=========================================================================

/**
	@class RectSelRenderer
	@ingroup Engine_Module
*/
    
class RectSelRenderer : public SelectionRenderer
{
//--- methods
public:
	RectSelRenderer(const char *id);

	virtual void RenderBackground(Unit *unit);
	virtual void RenderForeground(Unit *unit);

protected:
	void RenderStatusBar(float x, float y, float width, float percent, const vector3 &color);
};


#endif // __rectselrenderer_h__

// vim:ts=4:sw=4:
