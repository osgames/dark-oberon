//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file mapstructures.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/mapstructures.h"
#include "kernel/logserver.h"


//=========================================================================
// MapEnums
//=========================================================================

bool MapEnums::SegmentToString(Segment segment, string &name)
{
	switch (segment)
	{
	case SEG_UNDERGROUND:   name = "underground";   return true;
	case SEG_GROUND:        name = "ground";        return true;
	case SEG_SKY:           name = "sky";           return true;
	default:
		LogWarning1("Invalid map segment: %d", segment);
		return false;
	}
}


bool MapEnums::StringToSegment(const string &name, Segment &segment)
{
	if (name == "underground")  segment = SEG_UNDERGROUND;
	else if (name == "ground")  segment = SEG_GROUND;
	else if (name == "sky")     segment = SEG_SKY;
	else
	{
		LogWarning1("Invalid map segment: %s", name.c_str());
		return false;
	}

	return true;
}


bool MapEnums::DirectionToString(Direction direction, string &name)
{
	switch (direction)
	{
	case DIRECTION_NORTH:         name = "north";       return true;
	case DIRECTION_NORTH_WEST:    name = "north-west";  return true;
	case DIRECTION_WEST:          name = "west";        return true;
	case DIRECTION_SOUTH_WEST:    name = "south-west";  return true;
	case DIRECTION_SOUTH:         name = "south";       return true;
	case DIRECTION_SOUTH_EAST:    name = "south-east";  return true;
	case DIRECTION_EAST:          name = "east";        return true;
	case DIRECTION_NORTH_EAST:    name = "north-east";  return true;
	case DIRECTION_UP:            name = "up";          return true;
	case DIRECTION_DOWN:          name = "down";        return true;
	default:
		LogWarning1("Invalid map direction: %d", direction);
		return false;
	}
}


bool MapEnums::StringToDirection(const string &name, Direction &direction)
{
	if (name == "north")            direction = DIRECTION_NORTH;
	else if (name == "north-west")  direction = DIRECTION_NORTH_WEST;
	else if (name == "west")        direction = DIRECTION_WEST;
	else if (name == "south-west")  direction = DIRECTION_SOUTH_WEST;
	else if (name == "south")       direction = DIRECTION_SOUTH;
	else if (name == "south-east")  direction = DIRECTION_SOUTH_EAST;
	else if (name == "east")        direction = DIRECTION_EAST;
	else if (name == "north-east")  direction = DIRECTION_NORTH_EAST;
	else if (name == "up")          direction = DIRECTION_UP;
	else if (name == "down")        direction = DIRECTION_DOWN;
	else
	{
		LogWarning1("Invalid map direction: %s", name.c_str());
		return false;
	}

	return true;
}


// vim:ts=4:sw=4:
