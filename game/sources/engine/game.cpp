//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file game.cpp
	@ingroup Engine_Module

	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/game.h"
#include "engine/inputactions.h"
#include "engine/profile.h"
#include "engine/gui.h"
#include "engine/level.h"
#include "engine/map.h"
#include "engine/scheme.h"
#include "engine/race.h"
#include "engine/player.h"
#include "engine/onscreentext.h"
#include "engine/selectionrenderer.h"
#include "engine/selection.h"
#include "engine/pathserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "kernel/scriptserver.h"
#include "kernel/timeserver.h"
#include "kernel/messageserver.h"
#include "kernel/serializeserver.h"
#include "kernel/env.h"
#include "kernel/profiler.h"
#include "framework/resourceserver.h"
#include "framework/remoteserver.h"
#include "framework/gfxserver.h"
#include "framework/sceneserver.h"
#include "framework/inputserver.h"
#include "framework/audioserver.h"

#include <time.h>

PrepareScriptClass(Game, "Root", s_NewGame, s_InitGame, s_InitGame_cmds);


//=========================================================================
// Game
//=========================================================================

/**
	File server is required to be created before.
*/
Game::Game(const char *id) :
	RootServer(id),
	opened(false),
	run(false)
{
	this->file_server = FileServer::GetInstance();
	Assert(this->file_server);
}


/**
	Empty.
*/
Game::~Game()
{
	//
}


/**
	Opens game. Methods loads game settings, runs setup script and opens application window. Setup script is not
	mandatory, it can be presented on scripts:setup_game.lua path.

	@return @c True if successful.
*/
bool Game::Open()
{
	Assert(!this->opened);

	LogInfo("Opening game");

	// get servers
	this->gfx_server = GfxServer::GetInstance();
	if (!this->gfx_server.IsValid())
	{
		LogError("Missing GFX server");
		return false;
	}
	this->scene_server = SceneServer::GetInstance();
	if (!this->scene_server.IsValid())
	{
		LogError("Missing scene server");
		return false;
	}
	this->serialize_server = SerializeServer::GetInstance();
	if (!this->serialize_server.IsValid())
	{
		LogError("Missing serialize server");
		return false;
	}
	this->time_server = TimeServer::GetInstance();
	if (!this->time_server.IsValid())
	{
		LogError("Missing time server");
		return false;
	}
	this->message_server = MessageServer::GetInstance();
	if (!this->message_server.IsValid())
	{
		LogError("Missing message server");
		return false;
	}
	this->input_server = InputServer::GetInstance();
	if (!this->input_server.IsValid())
	{
		LogError("Missing input server");
		return false;
	}
	this->path_server = PathServer::GetInstance();
	if (!this->path_server.IsValid())
	{
		LogError("Missing path server");
		return false;
	}
	this->remote_server = RemoteServer::GetInstance();
	if (!this->remote_server.IsValid())
	{
		LogError("Missing remote server");
		return false;
	}
	this->audio_server = AudioServer::GetInstance();
	if (!this->audio_server.IsValid())
		LogWarning("Missing audio server, audio will be disabled");

	// create root nodes
	this->profiles = this->kernel_server->New("Root", NOH_PROFILES);
	this->guis = this->kernel_server->New("Root", NOH_GUIS);
	this->levels = this->kernel_server->New("Root", NOH_LEVELS);
	this->maps = this->kernel_server->New("Root", NOH_MAPS);
	this->schemes = this->kernel_server->New("Root", NOH_SCHEMES);
	this->races = this->kernel_server->New("Root", NOH_RACES);

	this->sel_renderers = this->kernel_server->New("Root", NOH_SEL_RENDERERS);
	this->variables = this->kernel_server->Lookup(NOH_VARIABLES);

	// create profilers
#ifdef USE_PROFILERS
	this->render_profiler = this->time_server->NewProfiler(string("% rendering"));
	this->pick_profiler = this->time_server->NewProfiler(string("% picking"));
	this->buffers_profiler = this->time_server->NewProfiler(string("% buffers+sleeping"));
#endif

	// load guis
	LogInfo("Loading headers of all GUIs");
	if (!this->LoadGuis())
	{
		LogError("Error loading GUIs");
		return false;
	}

	// load profiles
	LogInfo("Loading headers of all profiles");
	if (!this->LoadProfiles())
	{
		LogError("Error loading profiles");
		return false;
	}

	// run setup script
	if (this->file_server->FileExists(string("scripts:setup_game.lua")))
	{
		Assert(ScriptServer::GetInstance());
		string result;

		if (!ScriptServer::GetInstance()->RunScript(string("scripts:setup_game.lua"), result))
		{
			LogError1("Error running game setup script: %s", "scripts:setup_game.lua");
			return false;
		}
	}

	// set window properties
	this->gfx_server->SetWindowTitle(this->title);
    this->gfx_server->SetWindowSizeCallback(this->OnSize);

	// set timer properties
	this->time_server->SetMinFrameTime(this->active_profile->video.max_fps > 0 ? 1.0 / double(this->active_profile->video.max_fps) : 0);

	// set input properties
	this->input_server->SetKeyCallback(this->OnKey);
	this->input_server->AddActionCallback(this->OnAction, this);

	// open display
	if (!this->gfx_server->OpenDisplay())
	{
		LogError("Error opening GFX display");
		return false;
	}
	this->gfx_server->SetBlending(true);

	// open audio device
	if (this->audio_server.IsValid())
	{
		this->audio_server->SetDeviceName("Generic Hardware");  // to avoid crasheh on old HW

		if (this->audio_server->OpenDevice())
			this->audio_server->SetDistanceModel(AudioServer::DM_INVERSE_CLAMPED);

		else
		{
			LogWarning("Error opening audio device, audio will be disabled");
			this->audio_server->Release();
			this->audio_server = NULL;
		}
	}

	// set mouse cursor
	this->mouse_info.SetCursor(MouseInfo::CURSOR_ARROW);
	this->gfx_server->SetCursorVisibility(GfxServer::CURSOR_CUSTOM);

	// preload system font !!!
	FontFile::CharSize char_size;
	char_size.width = char_size.height = this->active_profile->system.font_size;
	char_size.resolution_x = char_size.resolution_y = 96;

	this->system_font = this->gfx_server->NewFont(this->active_profile->system.font_path, char_size);
	this->system_font->SetShadow(1, 1);
	ResourceServer::GetInstance()->LoadResources(Resource::RES_FONT);

	// create OST
	this->ost = (OnScreenText *)this->kernel_server->New("OnScreenText", NOH_ON_SCREEN_TEXT);
	this->UpdateOstDimensions();
	if (this->active_profile->system.log_to_ost)
		LogServer::GetInstance()->RegisterFunction(this->OnLog, LogServer::LL_ALL);

	// create selection
	this->selection = (Selection *)this->kernel_server->New("Selection", NOH_SELECTION);
	this->remote_server->RegisterClient(this->selection);

	// load schemes
	LogInfo("Loading headers of all schemes");
	if (!this->LoadSchemes())
	{
		LogError("Error loading schemes");
		return false;
	}

	// load maps
	LogInfo("Loading headers of all maps");
	if (!this->LoadMaps())
	{
		LogError("Error loading maps");
		return false;
	}

	// load races
	LogInfo("Loading headers of all races");
	if (!this->LoadRaces())
	{
		LogError("Error loading races");
		return false;
	}

	// load levels
	LogInfo("Loading headers of all levels");
	if (!this->LoadLevels())
	{
		LogError("Error loading levels");
		return false;
	}

	// start messaging queue
	if (!this->message_server->Run())
		return false;

	// start path-finding
	this->path_server->Init();

	return this->opened = true;
}


/**
	Runs game. Method starts time server and provides main game loop. In this loop whole game is drawn and all
	servers are triggered.

	@return @c True if successful.
*/
bool Game::Run()
{
	Assert(this->opened);
	Assert(!this->run);

	LogInfo("Game is running");

	// start time server
	this->time_server->StartTime();

	// set runnning state
	this->run = true;

	// @todo Set level from Menu.
	this->ActivateLevel(string("test"));

	if (!this->active_level.IsValid())
	{
		LogError("No level to run");
		this->gfx_server->CloseDisplay();
		return false;
	}

	ResourceServer *rs = ResourceServer::GetInstance();
	LogDebug2("Textures: %u, Size: %u", rs->GetResourcesCount(Resource::RES_TEXTURE), rs->GetResourcesSize(Resource::RES_TEXTURE));
	LogDebug2("Meshes: %u, Size: %u", rs->GetResourcesCount(Resource::RES_MESH), rs->GetResourcesSize(Resource::RES_MESH));
	LogDebug2("Models: %u, Size: %u", rs->GetResourcesCount(Resource::RES_MODEL), rs->GetResourcesSize(Resource::RES_MODEL));
	LogDebug2("Animations: %u, Size: %u", rs->GetResourcesCount(Resource::RES_ANIMATION), rs->GetResourcesSize(Resource::RES_ANIMATION));
	LogDebug2("Fonts: %u, Size: %u", rs->GetResourcesCount(Resource::RES_FONT), rs->GetResourcesSize(Resource::RES_FONT));

	this->variables->Sort(true);

	// run while window is opened
	while (this->gfx_server->IsDisplayOpened())
	{
		// update per-frame data
		this->Update();

		// draw game
		this->Render();

		// trigger all servers
		this->gfx_server->Trigger();
		this->input_server->Trigger();
		this->time_server->Trigger();
		this->kernel_server->Trigger();
	}

	// set runnning state
	this->run = false;

	LogInfo("Game has stopped");
	return true;
}


/**
	Closes game. Saves active profile and closes audio device.
*/
void Game::Close()
{
	Assert(this->opened);

	// close game
	LogInfo("Closing game");

	// reset mouse informations
	this->mouse_info.Reset();

	// stop path-finding
	this->path_server->Done();

	// stop messaging queue
	this->message_server->Stop();

	// save active profile
	this->active_profile->Save();

	// close audio device
	if (this->audio_server.IsValid())
		this->audio_server->CloseDevice();

	this->opened = false;
}


/**
	Closes the game.
*/
void Game::Quit()
{
	this->gfx_server->CloseDisplay();
}


inline
void Game::Update()
{
	if (this->active_level.IsValid())
		this->active_level->Update();

#ifdef USE_PROFILERS
	this->pick_profiler->Start();
#endif

	this->mouse_info.UpdatePickUnit(this->active_map);

#ifdef USE_PROFILERS
	this->pick_profiler->Stop();
#endif

	this->mouse_info.UpdateCursor(this->active_map, this->active_level->GetActivePlayer()->GetPlayerMap(), this->selection);
	this->selection->Update();
}


inline
void Game::Render()
{
#ifdef USE_PROFILERS
	this->render_profiler->Start();
#endif

	// prepare drawing
	this->gfx_server->ClearBuffers(GfxServer::BUFFER_COLOR);

	// draw level
	if (this->active_level.IsValid())
		this->active_level->Render();

	// draw selection rectangle
	this->mouse_info.RenderSelectionRectangle(this->active_map);

	// draw gui
	/// @todo Draw GUI.

	// draw system variables
	if (this->system_font.IsValid())
	{
		this->gfx_server->SetColor(this->active_profile->system.font_color);
		this->gfx_server->SetFont(this->system_font);

		Env *variable;
		vector2 position(10.0f, this->system_font->GetHeight() + 5.0f);
		
		for (variable = (Env *)this->variables->GetFront(); variable; variable = (Env *)variable->GetNext())
		{
			this->gfx_server->Print(position, variable->GetID() + ": " + variable->GetAsString());
			position.y += this->system_font->GetHeight();
		}
	}

	// draw OST
	if (this->ost.IsValid())
		this->ost->Render();

#ifdef USE_PROFILERS
	this->render_profiler->Stop();
	this->buffers_profiler->Start();
#endif

	// switch buffers
	this->gfx_server->PresentScene();

#ifdef USE_PROFILERS
	this->buffers_profiler->Stop();
#endif
}


inline
void Game::UpdateOstDimensions()
{
	this->ost->SetPosition(vector2(10.0f, this->gfx_server->GetWindowSize().height - 200.0f));
	this->ost->SetSize(vector2(this->gfx_server->GetWindowSize().width - 20.0f, 190.0f));
}


/**
	Sets game title.

	@param title Game title.
*/
void Game::SetTitle(const string &title)
{
	this->title = title;

	// set new title as window title
	if (GfxServer::GetInstance() && GfxServer::GetInstance()->IsDisplayOpened())
		GfxServer::GetInstance()->SetWindowTitle(this->title);
}


void Game::SaveScreenshot()
{
	char file_name[100];
	static int counter = 0;

	// generate unique file name
	time_t ltime;
	time(&ltime);
	sprintf(file_name, "screenshots:shot%010ld%02d.tga", (long)ltime, counter++);

	this->gfx_server->SaveScreenshot(string(file_name));
}


void Game::OnSize(ushort_t width, ushort_t height, bool fullscreen)
{
	Game *game = Game::GetInstance();

	if (!game)
		return;

	// store changes to profile
	game->active_profile->video.fullscreen = fullscreen;

	if (fullscreen)
		game->active_profile->video.full_size.Set(width, height);
	else
		game->active_profile->video.wnd_size.Set(width, height);

	// update map viewport
	if (game->active_map.IsValid())
		game->active_map->UpdateViewport(width, height);
}


bool Game::OnKey(KeyCode code, KeyState state)
{
	if (!Game::GetInstance())
		return false;

	Game *game = Game::GetInstance();

	switch (code)
	{
	case MOUSE_BUTTON_LEFT:
		// begin rectangle selection
		if (state == KS_DOWN)
			game->mouse_info.OnLeftKeyDown(game->active_map);

		// select/unselect units
		else
			game->mouse_info.OnLeftKeyUp(game->active_map, game->selection);

		return true;

	case MOUSE_BUTTON_RIGHT:
		// start action
		if (state == KS_DOWN)
			game->mouse_info.OnRightKeyDown(game->selection);
		return true;
	}

	return false;
}


bool Game::OnAction(const string &action, int position, void *data)
{
	if (!Game::GetInstance())
		return false;

	// get pointer to this object
	Assert(data);
	Game *game = (Game *)data;

	// process action
	if (action == IN_ACTION_QUIT)
	{
		game->Quit();
		return true;
	}
	else if (action == IN_ACTION_SYSTEM_MOUSE)
	{
		game->gfx_server->SetCursorVisibility (
			game->gfx_server->GetCursorVisibility() == GfxServer::CURSOR_SYSTEM ? GfxServer::CURSOR_CUSTOM : GfxServer::CURSOR_SYSTEM
		);
		return true;
	}
	else if (action == IN_ACTION_SCREENSHOT)
		game->SaveScreenshot();

	else if (action == IN_ACTION_FULLSCREEN)
	{
		//< @todo Alt modifier from key binding
		if (game->input_server->IsKeyPressed(KEY_LALT))
		{
			game->gfx_server->ToggleFullscreen();
			game->input_server->RegisterCallbacks();
			game->UpdateOstDimensions();
		}
	}

	return false;
}


void Game::OnLog(LogServer::LogLevel level, const string &file_name, int line, const string &text)
{
	Game *game = Game::GetInstance();

	if (!game || !game->ost.IsValid())
		return;

	// colors for messages
	static const vector3 color_debug(COLOR_DEBUG);
	static const vector3 color_info(COLOR_INFO);
	static const vector3 color_warning(COLOR_WARNING);
	static const vector3 color_error(COLOR_ERROR);
	static const vector3 color_critical(COLOR_CRITICAL);

	// log to OST
	switch (level)
	{
	case LogServer::LL_DEBUG:    game->ost->AddMessage(text, color_debug); break;
	case LogServer::LL_INFO:     game->ost->AddMessage(text, color_info); break;
	case LogServer::LL_WARNING:  game->ost->AddMessage(text, color_warning); break;
	case LogServer::LL_ERROR:    game->ost->AddMessage(text, color_error); break;
	case LogServer::LL_CRITICAL: game->ost->AddMessage(text, color_critical); break;
	default:                     game->ost->AddMessage(text); break;
	}
}


//=========================================================================
// Preloaded
//=========================================================================

/**
	Creates a Preloaded object and stores it in given path.

	@return Pointer to new Preloaded object.
*/
Preloaded *Game::NewPreloaded(const char *class_name, Root *root, const string &file_name)
{
	Assert(root);

	// create new object
	this->kernel_server->PushCwd(root);

	string id;
	this->file_server->ExtractFileName(file_name, id);

	Preloaded *object = (Preloaded *)this->kernel_server->New(class_name, id);
	Assert(object);
	this->kernel_server->PopCwd();

	// set properties
	object->SetFileName(file_name);

	return object;
}


//=========================================================================
// Profiles
//=========================================================================

void Game::CreateDefaultProfile()
{
	// create new profile object
	this->kernel_server->PushCwd(this->profiles.Get());
	Profile *profile = (Profile *)this->kernel_server->New("Profile", PROFILE_DEFAULT_ID);
	Assert(profile);
	this->kernel_server->PopCwd();

	// set properties
	profile->LoadDefaultSettings();
	profile->SetActive(true);

	// save profile
	if (!profile->Save())
		LogWarning("Can not save default profile");
}


/**
	Creates all profiles from "profiles:" directory. If there is no profile or some error occures,
	the default one is created. Old profiles are released first. Last active profile is loaded and activated.
*/
bool Game::LoadProfiles()
{
	// released old profiles
	if (this->profiles.IsValid())
	{
		if (!this->profiles->ReleaseChildren())
		{
			LogError("Error deleting profiles");
			return false;
		}
	}

	// load all profiles
	if (this->file_server->DirectoryExists(string("profiles:")))
	{
		vector<string> file_list;

		// get all files
		if (this->file_server->ListFiles(file_list, string("profiles:")))
		{
			vector<string>::iterator it;
			string file_name;
			Profile *profile;

			// create object for every profile
			for (it = file_list.begin(); it != file_list.end(); it++)
			{
				file_name = string("profiles:") + *it;

				// create profile and preload header
				profile = (Profile *)this->NewPreloaded("Profile", this->profiles.Get(), file_name);
				if (!profile)
					continue;

				if (!profile->LoadHeader())
					profile->Release();
			}
		}
		else
			LogWarning("Profiles has not been loaded");
	}

	// if no profiles were loaded, create default one
	if (this->profiles->IsEmpty())
	{
		LogInfo("Using default profile");
		this->CreateDefaultProfile();
	}

	// activate last profile
	if (!this->ActivateLastProfile())
	{
		LogError("Error activating last active profile");
		SafeRelease(this->profiles);
		return false;
	}

	return true;
}


/**
	Re-activates first profile with true active flag.

	@return @c True if successful.
*/
bool Game::ActivateLastProfile()
{
	Assert(this->profiles.IsValid());

	bool activated = false;

	// find first profile with true active flag and make it active
	// true active flags of other profiles will be set to false
	Profile *profile = (Profile *)this->profiles->GetFront();
	for (; profile; profile = (Profile *)profile->GetNext())
	{
		if (profile->IsActive())
		{
			// reset active flag
			profile->SetActive(false);
			
			// re-activate profile
			if (!activated)
			{
				activated = this->ActivateProfile(profile);

				// if there is something wrong, set active flag to false
				if (!activated)
					LogWarning1("Can not activate profile: %s", profile->GetID().c_str());
			}
		}
	}

	// if we haven't activated any profile, try to activate default one...
	if (!activated)
	{
		activated = this->ActivateProfile(PROFILE_DEFAULT_ID);
		if (!activated)
			LogWarning("Can not activate default profile");
	}

	// ...or first one
	if (!activated)
	{
		Profile *profile = (Profile *)this->profiles->GetFront();
		if (profile)
		{
			if (profile->GetID() == PROFILE_DEFAULT_ID)
				profile = (Profile *)this->GetNext();

			if (profile)
			{
				activated = this->ActivateProfile(profile);
				if (!activated)
					LogWarning("Can not activate first profile");
			}
		}
	}

	return activated;
}


/**
	Activates given profile.

	@param id Profile identifier.
	@return @c True if successful.
*/
bool Game::ActivateProfile(const string &id)
{
	Assert(!id.empty());
	Assert(this->profiles.IsValid());

	// find profile by id
	Profile *profile = (Profile *)this->profiles->Find(id);
	if (!profile)
	{
		LogWarning1("Can not find profile: %s", id.c_str());
		return false;
	}

	// activate profile
	if (!this->ActivateProfile(profile))
	{
		LogWarning1("Can not activate profile: %s", id.c_str());
		return false;
	}

	return true;
}


/**
	Activates given profile.

	@param profile Pointer to profile.
	@return @c True if successful.
*/
bool Game::ActivateProfile(Profile *profile)
{
	Assert(profile);
	Assert(!this->active_level.IsValid());

	// profile is already active
	if (profile == this->active_profile.GetUnsafe())
		return true;

	// load profile
	if (!profile->IsLoaded() && !profile->Load())
	{
		LogWarning1("Error loading profile: %s", profile->GetID().c_str());
		return false;
	}

	// deactivate active profile
	if (this->active_profile.IsValid())
	{
		this->active_profile->SetActive(false);
		this->active_profile->Save();
		this->active_profile->Unload();
	}

	LogInfo1("Activating profile: %s", profile->GetID().c_str());

	// activate new profile
	this->active_profile = profile;
	profile->SetActive(true);

	//--- apply profile settings ----------------------------------------------

	// prepare display mode
	DisplayMode mode;
	mode.SetFullscreen(this->active_profile->video.fullscreen);
	mode.SetFullscreenSize(this->active_profile->video.full_size);
	mode.SetWindowPosition(this->active_profile->video.wnd_position);
	mode.SetWindowSize(this->active_profile->video.wnd_size);

	mode.SetRefreshRate(this->active_profile->video.refresh_rate);
	mode.SetVerticalSync(this->active_profile->video.vert_sync);

	// set window properties
	this->gfx_server->SetMinMagFilter(this->active_profile->video.minmag_filter);
    this->gfx_server->SetMipMapFilter(this->active_profile->video.mipmap_filter);
	this->gfx_server->SetDisplayMode(mode);

	// set input properties
	hash_map<string, string>::iterator it;

	for (it = this->active_profile->input.bind_map.begin(); it != this->active_profile->input.bind_map.end(); it++)
		this->input_server->Bind(it->first.c_str(), it->second.c_str());

	// set system properties
	this->time_server->ShowFPS(this->active_profile->system.fps_show);
	this->time_server->SetFPSInterval(this->active_profile->system.fps_interval);

	// create selection renderers
	this->SetUnitSelRenderer(this->active_profile->video.unit_selection);
	this->SetBuildingSelRenderer(this->active_profile->video.building_selection);

	// activate GUI
	if (!this->ActivateGui(this->active_profile->video.gui))
	{
		if (this->ActivateGui(GUI_DEFAULT_ID))
			this->active_profile->video.gui = GUI_DEFAULT_ID;
	}

	return true;
}


//=========================================================================
// Guis
//=========================================================================

/**
	Creates all GUIs from "guis:" directory. Old GUIs are released first. No GUI is loaded.
*/
bool Game::LoadGuis()
{
	// released old GUIs
	if (this->guis.IsValid())
	{
		if (!this->guis->ReleaseChildren())
		{
			LogError("Error deleting GUIs");
			return false;
		}
	}

	// load all GUIs
	if (this->file_server->DirectoryExists(string("guis:")))
	{
		vector<string> dir_list;
		vector<string> file_list;

		// get all files
		if (this->file_server->ListDirectories(dir_list, string("guis:")))
		{
			vector<string>::iterator it;
			vector<string>::iterator it2;
			string file_name;
			Gui *gui;

			// create object for every GUI
			for (it = dir_list.begin(); it != dir_list.end(); it++)
			{
				if ((*it)[0] == '.')
					continue;

				if (!this->file_server->ListFiles(file_list, string("guis:") + *it))
					continue;

				for (it2 = file_list.begin(); it2 != file_list.end(); it2++)
				{
					this->file_server->ExtractFileName(*it2, file_name);

					if (file_name == *it)
					{
						file_name = string("guis:") + *it + "/" + *it2;

						// create GUI and preload heder
						gui = (Gui *)this->NewPreloaded("Gui", this->guis.Get(), file_name);
						if (!gui)
							continue;
						
						if (!gui->LoadHeader())
							gui->Release();

						break;
					}
				}
			}
		}
		else
			LogWarning("GUIs has not been loaded");
	}

	return true;
}


/**
	Activates given GUI.

	@param id GUI identifier.
	@return @c True if successful.
*/
bool Game::ActivateGui(const string &id)
{
	Assert(!id.empty());
	Assert(this->guis.IsValid());

	// find GUI by id
	Gui *gui = (Gui *)this->guis->Find(id);
	if (!gui)
	{
		LogWarning1("Can not find GUI: %s", id.c_str());
		return false;
	}

	// activate GUI
	if (!this->ActivateGui(gui))
	{
		LogWarning1("Can not activate GUI: %s", id.c_str());
		return false;
	}

	return true;
}


/**
	Activates given gui.

	@param gui Pointer to GUI.
	@return @c True if successful.
*/
bool Game::ActivateGui(Gui *gui)
{
	Assert(gui);

	// gui is already active
	if (gui == this->active_gui.GetUnsafe())
		return true;

	// load gui
	if (!gui->IsLoaded() && !gui->Load())
	{
		LogWarning1("Error loading GUI: %s", gui->GetID().c_str());
		return false;
	}

	// unload and deactivate active GUI (there can be only one loaded GUI)
	if (this->active_gui.IsValid())
	{
		this->active_gui->SetActive(false);
		this->active_gui->Unload();
	}

	LogInfo1("Activating GUI: %s", gui->GetID().c_str());

	// activate new GUI
	this->active_gui = gui;
	this->active_gui->SetActive(true);

	//--- apply GUI -----------------------------------------------------------

	// get cursors identifiers
	MouseInfo::CursorType cursor_type;
	for (int i = 0; i < MouseInfo::CURSOR_COUNT; i++)
	{
		cursor_type = MouseInfo::CursorType(i);
		this->mouse_info.AddCursor(cursor_type, this->active_gui->GetCursor(this->mouse_info.GetCursorName(cursor_type)));
	}

	/// @todo Apply new GUI here (dialogs, menu, ...).

	return true;
}


//=========================================================================
// Levels
//=========================================================================

/**
	Creates all levels from "levels:" directory. Old levels are released first. No level is loaded.
*/
bool Game::LoadLevels()
{
	// released old levels
	if (this->levels.IsValid())
	{
		if (!this->levels->ReleaseChildren())
		{
			LogError("Error deleting levels");
			return false;
		}
	}

	// load all levels
	if (this->file_server->DirectoryExists(string("levels:")))
	{
		vector<string> dir_list;
		vector<string> file_list;

		// get all files
		if (this->file_server->ListDirectories(dir_list, string("levels:")))
		{
			vector<string>::iterator it;
			vector<string>::iterator it2;
			string file_name;
			Level *level;

			// create object for every level
			for (it = dir_list.begin(); it != dir_list.end(); it++)
			{
				if ((*it)[0] == '.')
					continue;

				if (!this->file_server->ListFiles(file_list, string("levels:") + *it))
					continue;

				for (it2 = file_list.begin(); it2 != file_list.end(); it2++)
				{
					this->file_server->ExtractFileName(*it2, file_name);

					if (file_name == *it)
					{
						file_name = string("levels:") + *it + "/" + *it2;

						// create level and preload heder
						level = (Level *)this->NewPreloaded("Level", this->levels.Get(), file_name);
						if (!level)
							continue;

						if (!level->LoadHeader())
							level->Release();

						break;
					}
				}
			}
		}
		else
			LogWarning("Levels has not been loaded");
	}

	return true;
}


/**
	Activates given level.

	@param id Level identifier.
	@return @c True if successful.
*/
bool Game::ActivateLevel(const string &id)
{
	Assert(!id.empty());
	Assert(this->levels.IsValid());

	// find level by id
	Level *level = (Level *)this->levels->Find(id);
	if (!level)
	{
		LogWarning1("Can not find level: %s", id.c_str());
		return false;
	}

	// activate level
	if (!this->ActivateLevel(level))
	{
		LogWarning1("Can not activate level: %s", id.c_str());
		return false;
	}

	return true;
}


/**
	Activates given level.

	@param level Pointer to level.
	@return @c True if successful.
*/
bool Game::ActivateLevel(Level *level)
{
	Assert(level);

	// level is already active
	if (level == this->active_level.GetUnsafe())
		return true;

	// load level
	if (!level->IsLoaded() && !level->Load())
	{
		LogWarning1("Error loading level: %s", level->GetID().c_str());
		return false;
	}

	// unload and deactivate active level (there can be only one loaded level)
	if (this->active_level.IsValid())
	{
		this->active_level->SetActive(false);
		this->active_level->Unload();
	}

	LogInfo1("Activating level: %s", level->GetID().c_str());

	// activate new level
	this->active_level = level;
	this->active_level->SetActive(true);
	this->active_map = this->active_level->GetMap();
	Assert(this->active_map.IsValid());
	this->active_player = this->active_level->GetActivePlayer();
	Assert(this->active_player.IsValid());

	// update map viewport
	const DisplayMode::WidowSize &dm = this->gfx_server->GetDisplayMode().GetModeSize();

	this->active_map->UpdateViewport(dm.width, dm.height);
	this->active_map->SetMoveSpeed(this->active_profile->video.map_move_speed);
	this->active_map->SetZoomSpeed(this->active_profile->video.map_zoom_speed);

	return true;
}


//=========================================================================
// Other preloaded objects
//=========================================================================

/**
	Creates all maps from "maps:" directory. Old maps are released first. No map is loaded.
*/
bool Game::LoadMaps()
{
	// released old maps
	if (this->maps.IsValid())
	{
		if (!this->maps->ReleaseChildren())
		{
			LogError("Error deleting maps");
			return false;
		}
	}

	// load all maps
	if (this->file_server->DirectoryExists(string("maps:")))
	{
		vector<string> dir_list;
		vector<string> file_list;

		// get all files
		if (this->file_server->ListDirectories(dir_list, string("maps:")))
		{
			vector<string>::iterator it;
			vector<string>::iterator it2;
			string file_name;
			Map *map;

			// create object for every map
			for (it = dir_list.begin(); it != dir_list.end(); it++)
			{
				if ((*it)[0] == '.')
					continue;

				if (!this->file_server->ListFiles(file_list, string("maps:") + *it))
					continue;

				for (it2 = file_list.begin(); it2 != file_list.end(); it2++)
				{
					this->file_server->ExtractFileName(*it2, file_name);

					if (file_name == *it)
					{
						file_name = string("maps:") + *it + "/" + *it2;

						// create map and preload heder
						map = (Map *)this->NewPreloaded("Map", this->maps.Get(), file_name);
						if (!map)
							continue;
						
						if (!map->LoadHeader())
							map->Release();

						break;
					}
				}
			}
		}
		else
			LogWarning("Maps has not been loaded");
	}

	return true;
}


/**
	Creates all schemes from "schemes:" directory. Old schemes are released first. No scheme is loaded.
*/
bool Game::LoadSchemes()
{
	// released old schemes
	if (this->schemes.IsValid())
	{
		if (!this->schemes->ReleaseChildren())
		{
			LogError("Error deleting schemes");
			return false;
		}
	}

	// load all schemes
	if (this->file_server->DirectoryExists(string("schemes:")))
	{
		vector<string> dir_list;
		vector<string> file_list;

		// get all files
		if (this->file_server->ListDirectories(dir_list, string("schemes:")))
		{
			vector<string>::iterator it;
			vector<string>::iterator it2;
			string file_name;
			Scheme *scheme;

			// create object for every scheme
			for (it = dir_list.begin(); it != dir_list.end(); it++)
			{
				if ((*it)[0] == '.')
					continue;

				if (!this->file_server->ListFiles(file_list, string("schemes:") + *it))
					continue;

				for (it2 = file_list.begin(); it2 != file_list.end(); it2++)
				{
					this->file_server->ExtractFileName(*it2, file_name);

					if (file_name == *it)
					{
						file_name = string("schemes:") + *it + "/" + *it2;

						// create scheme and preload heder
						scheme = (Scheme *)this->NewPreloaded("Scheme", this->schemes.Get(), file_name);
						if (!scheme)
							continue;
						
						if (!scheme->LoadHeader())
							scheme->Release();

						break;
					}
				}
			}
		}
		else
			LogWarning("Schemes has not been loaded");
	}

	return true;
}


/**
	Creates all races from "races:" directory. Old races are released first. No race is loaded.
*/
bool Game::LoadRaces()
{
	// released old races
	if (this->races.IsValid())
	{
		if (!this->races->ReleaseChildren())
		{
			LogError("Error deleting races");
			return false;
		}
	}

	// load all races
	if (this->file_server->DirectoryExists(string("races:")))
	{
		vector<string> dir_list;
		vector<string> file_list;

		// get all files
		if (this->file_server->ListDirectories(dir_list, string("races:")))
		{
			vector<string>::iterator it;
			vector<string>::iterator it2;
			string file_name;
			Race *race;

			// create object for every race
			for (it = dir_list.begin(); it != dir_list.end(); it++)
			{
				if ((*it)[0] == '.')
					continue;

				if (!this->file_server->ListFiles(file_list, string("races:") + *it))
					continue;

				for (it2 = file_list.begin(); it2 != file_list.end(); it2++)
				{
					this->file_server->ExtractFileName(*it2, file_name);

					if (file_name == *it)
					{
						file_name = string("races:") + *it + "/" + *it2;

						// create race and preload heder
						race = (Race *)this->NewPreloaded("Race", this->races.Get(), file_name);
						if (!race)
							continue;
						
						if (!race->LoadHeader())
							race->Release();

						break;
					}
				}
			}
		}
		else
			LogWarning("Races has not been loaded");
	}

	return true;
}


//=========================================================================
// Selection renderers
//=========================================================================

SelectionRenderer *Game::NewSelRenderer(const string &class_name)
{
	Assert(!class_name.empty());
	Assert(this->sel_renderers.IsValid());

	// try to find existing renderer
	SelectionRenderer *renderer = (SelectionRenderer *)this->Find(class_name);
	if (renderer)
		return renderer;

	// create new one
	this->kernel_server->PushCwd(this->sel_renderers.Get());
	renderer = (SelectionRenderer *)this->kernel_server->New(class_name, class_name);
	this->kernel_server->PopCwd();

	return renderer;
}


bool Game::SetUnitSelRenderer(const string &class_name)
{
	if (this->unit_sel_renderer.IsValid())
	{
		// if renderer is active, do nothing
		if (this->unit_sel_renderer->GetClass()->GetName() == class_name)
			return true;

		// delete old renderer
		SafeRelease(this->unit_sel_renderer);
	}

	// no renderer will be used
	if (class_name.empty())
		return true;

	// create and set new renderer
	SelectionRenderer *renderer = this->NewSelRenderer(class_name);
	if (!renderer)
		return false;

	this->unit_sel_renderer = renderer;
	return true;
}


bool Game::SetBuildingSelRenderer(const string &class_name)
{
	if (this->building_sel_renderer.IsValid())
	{
		// if renderer is active, do nothing
		if (this->building_sel_renderer->GetClass()->GetName() == class_name)
			return true;

		// delete old renderer
		SafeRelease(this->building_sel_renderer);
	}

	// no renderer will be used
	if (class_name.empty())
		return true;

	// create and set new renderer
	SelectionRenderer *renderer = this->NewSelRenderer(class_name);
	if (!renderer)
		return false;

	this->building_sel_renderer = renderer;
	return true;
}


// vim:ts=4:sw=4:
