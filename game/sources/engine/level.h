#ifndef __level_h__
#define __level_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file level.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/preloaded.h"
#include "kernel/ref.h"

class Map;
class Unit;
class Player;


//=========================================================================
// Level
//=========================================================================

/**
	@class Level
	@ingroup Engine_Module
*/
    
class Level : public Preloaded
{
//--- methods
public:
	Level(const char *id);
	virtual ~Level();

	virtual void SetActive(bool active);

	void Update();
	void Render();
	Map *GetMap() const;

	// players
	Player *NewPlayer(const string &name);
	Player *GetActivePlayer() const;
	Player *GetPlayer(const string &id) const;

	// tools
	bool AddUnitToMap(Unit *unit);
	void RemoveUnitFromMap(Unit *unit);

protected:
	virtual void ClearHeader();
	virtual void ClearBody();

	virtual bool LoadResources();
	virtual void UnloadResources();

	// serializing
	virtual bool ReadHeader(Serializer &serializer);
	virtual bool ReadBody(Serializer &serializer);
	virtual bool WriteHeader(Serializer &serializer);
	virtual bool WriteBody(Serializer &serializer);

private:
	bool RegisterPlayer(Player *player);
	bool DeregisterPlayer(Player *player);


//--- variables
protected:
	// level header
	string name;                    ///< Full level name.
	string author_name;
	string author_contact;

	Ref<Map> map;

	// level body
	string script_file;
	Ref<Root> players;              ///< Root node of all players.
	Ref<Player> active_player;      ///< Active player.
};


//=========================================================================
// Mathods
//=========================================================================

inline
Map *Level::GetMap() const
{
	return this->map;
}


inline
Player *Level::GetActivePlayer() const
{
	return this->active_player;
}


inline
Player *Level::GetPlayer(const string &id) const
{
	return (Player *)this->players->Find(id);
}


#endif // __level_h__

// vim:ts=4:sw=4:
