//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file boxselrenderer.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/boxselrenderer.h"
#include "engine/unit.h"
#include "engine/prototype.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"

PrepareClass(BoxSelRenderer, "SelectionRenderer", s_NewBoxSelRenderer, s_InitBoxSelRenderer);


//=========================================================================
// Settings
//=========================================================================

#define BAR_SIZE   7.0f             ///< Size of status bar.
#define BAR_SIZE_2 (BAR_SIZE / 2)   ///< Half size of status bar.


//=========================================================================
// BoxSelRenderer
//=========================================================================

BoxSelRenderer::BoxSelRenderer(const char *id) :
	SelectionRenderer(id)
{
	//
}


void BoxSelRenderer::RenderBackground(Unit *unit)
{
	Assert(unit);

	if (!unit->IsAnyFlag(Unit::FLAG_SELECTED | Unit::FLAG_UNDER_CURSOR))
		return;

	Prototype *prototype = unit->GetPrototype();
	Assert(prototype);

	static vector2 coords[6];
	static vector2 coords2[3];
	static MapSize last_size;
	static float last_selection_height = 0.0f;
	static bool last_selected = false;

	// update coodrinates
	if (last_size != prototype->GetSize() || last_selection_height != prototype->GetSelectionHeight())
	{
		last_size = prototype->GetSize();
		last_selection_height = prototype->GetSelectionHeight();

		static float x2, y2;
		x2 = last_size.width;
		y2 = last_size.height;

		coords[0].set((x2 - y2) * ISO_SCENE_H_COEF, (x2 + y2) * ISO_SCENE_V_COEF);
		coords[1].set(x2 * ISO_SCENE_H_COEF, x2 * ISO_SCENE_V_COEF);
		coords[2].set(0.0f, 0.0f);
		coords[3].set(-y2 * ISO_SCENE_H_COEF, y2 * ISO_SCENE_V_COEF);
	}

	if (!last_selected && unit->IsFlags(Unit::FLAG_SELECTED))
	{
		coords[4].set(coords[0]);
		coords[5].set(coords[4].x, coords[4].y + last_selection_height);

		coords2[0].set(coords[1].x, coords[1].y + last_selection_height);
		coords2[1].set(coords[5]);
		coords2[2].set(coords[3].x, coords[3].y + last_selection_height);
	}

	last_selected = unit->IsFlags(Unit::FLAG_SELECTED);

	// render hover quad
	if (unit->IsFlags(Unit::FLAG_UNDER_CURSOR))
	{
		this->gfx_server->SetColor(this->GetHoverColor(unit));
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
	}

	// render selection
	if (unit->IsFlags(Unit::FLAG_SELECTED))
	{
		this->gfx_server->SetColor(this->GetSelectionColor(unit));
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_LINE_STRIP, coords, 6);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_LINE_STRIP, coords2, 3);
	}
}


void BoxSelRenderer::RenderForeground(Unit *unit)
{
	Assert(unit);

	if (!unit->IsAnyFlag(Unit::FLAG_SELECTED | Unit::FLAG_UNDER_CURSOR))
		return;

	Prototype *prototype = unit->GetPrototype();
	Assert(prototype);

	static float x2, y2;
	static vector2 coords[5];
	static vector2 coords2[2];
	static MapSize last_size;
	static float last_selection_height = 0.0f;
	static bool last_selected = false;

	// update coodrinates
	if (last_size != prototype->GetSize() || last_selection_height != prototype->GetSelectionHeight())
	{
		last_size = prototype->GetSize();
		last_selection_height = prototype->GetSelectionHeight();

		x2 = last_size.width;
		y2 = last_size.height;

		coords[0].set(x2 * ISO_SCENE_H_COEF, x2 * ISO_SCENE_V_COEF);
	}

	if (!last_selected && unit->IsFlags(Unit::FLAG_SELECTED))
	{
		coords[4].set(-y2 * ISO_SCENE_H_COEF, y2 * ISO_SCENE_V_COEF);
		coords[1].set(coords[0].x, coords[0].y + last_selection_height);
		coords[2].set(0.0f, last_selection_height);
		coords[3].set(coords[4].x, coords[4].y + last_selection_height);

		coords2[0].set(0.0f, 0.0f);
		coords2[1].set(coords[2]);
	}

	last_selected = unit->IsFlags(Unit::FLAG_SELECTED);

	// render life and hiding bar
	this->RenderStatusBar(coords[0].x - BAR_SIZE, coords[0].y - BAR_SIZE_2, last_selection_height, unit->GetLifePercent(), this->GetLifeColor(unit));
	if (unit->IsMy() && unit->GetPrototype()->CanHide())
		this->RenderStatusBar(coords[0].x - 2 * BAR_SIZE, coords[0].y - 2 * BAR_SIZE_2, last_selection_height, unit->GetHidingPercent(), this->GetHidingColor(unit));

	// render selection
	if (unit->IsFlags(Unit::FLAG_SELECTED))
	{
		this->gfx_server->SetColor(this->GetSelectionColor(unit));
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_LINE_STRIP, coords, 5);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_LINE_STRIP, coords2, 2);
	}
}


void BoxSelRenderer::RenderStatusBar(float x, float y, float height, float percent, const vector3 &color)
{
	static vector2 coords[4];
	static float hp;
	static vector3 fg_color;
	static vector4 bg_color(1.0f, 1.0f, 1.0f, 0.4f);

	hp = height * percent;

	// status bar
	if (percent > 0)
	{
		coords[0].set(x, y + hp);
		coords[1].set(x - BAR_SIZE, y + BAR_SIZE_2 + hp);
		coords[2].set(x, y + BAR_SIZE + hp);
		coords[3].set(x + BAR_SIZE, y + BAR_SIZE_2 + hp);

		this->gfx_server->SetColor(color);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);

		coords[0].set(x, y);
		coords[1].set(x, y + hp);
		coords[2].set(x - BAR_SIZE, y + BAR_SIZE_2 + hp);
		coords[3].set(x - BAR_SIZE, y + BAR_SIZE_2);

		fg_color = color * 0.6f;
		this->gfx_server->SetColor(fg_color);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);

		coords[0].set(x, y);
		coords[1].set(x, y + hp);
		coords[2].set(x + BAR_SIZE, y + BAR_SIZE_2 + hp);
		coords[3].set(x + BAR_SIZE, y + BAR_SIZE_2);

		fg_color = color * 0.8f;
		this->gfx_server->SetColor(fg_color);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
	}

	// background
	if (percent < 1)
	{
		if (!percent)
		{
			coords[0].set(x, y + hp);
			coords[1].set(x - BAR_SIZE, y + BAR_SIZE_2 + hp);
			coords[2].set(x, y + BAR_SIZE + hp);
			coords[3].set(x + BAR_SIZE, y + BAR_SIZE_2 + hp);

			bg_color = color;
			bg_color.w = 0.4f;

			this->gfx_server->SetColor(bg_color);
			this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
		}

		coords[0].set(x, y + height);
		coords[1].set(x - BAR_SIZE, y + BAR_SIZE_2 + height);
		coords[2].set(x, y + BAR_SIZE + height);
		coords[3].set(x + BAR_SIZE, y + BAR_SIZE_2 + height);

		bg_color = color;
		bg_color.w = 0.4f;

		this->gfx_server->SetColor(bg_color);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);

		coords[0].set(x, y + hp);
		coords[1].set(x, y + height);
		coords[2].set(x - BAR_SIZE, y + BAR_SIZE_2 + height);
		coords[3].set(x - BAR_SIZE, y + BAR_SIZE_2 + hp);

		bg_color = color * 0.6f;
		bg_color.w = 0.4f;

		this->gfx_server->SetColor(bg_color);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);

		coords[0].set(x, y + hp);
		coords[1].set(x, y + height);
		coords[2].set(x + BAR_SIZE, y + BAR_SIZE_2 + height);
		coords[3].set(x + BAR_SIZE, y + BAR_SIZE_2 + hp);

		bg_color = color * 0.8f;
		bg_color.w = 0.4f;

		this->gfx_server->SetColor(bg_color);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
	}
}


// vim:ts=4:sw=4:
