#ifndef __preloaded_h__
#define __preloaded_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file preloaded.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "kernel/root.h"


//=========================================================================
// Preloaded
//=========================================================================

/**
	@class Preloaded
	@ingroup Engine_Module

	@todo Maybe move this class to Framework.
*/
    
class Preloaded : public Root
{
//--- variables
protected:
	string file_name;              ///< Full file name of preloaded file.
	bool loaded;                   ///< Whether this object is loaded.
	bool load_header;              ///< Whether only header will be loaded.
	bool active;                   ///< Whether this object is active.


//--- methods
public:
	Preloaded(const char *id);
	virtual ~Preloaded();

	bool LoadHeader();
	virtual bool Load();
	virtual bool Unload();
	virtual void SetActive(bool active);
	bool Save();

	void SetFileName(const string &file_name);
	const string &GetFileName();

	bool IsActive();
	bool IsLoaded();

protected:
	virtual void ClearHeader();
	virtual void ClearBody();

	virtual bool LoadResources();
	virtual void UnloadResources();

	// serializing
	virtual bool Serialize(Serializer &serializer);
	virtual bool Deserialize(Serializer &serializer, bool first);

	virtual bool ReadHeader(Serializer &serializer);
	virtual bool ReadBody(Serializer &serializer);
	virtual bool WriteHeader(Serializer &serializer);
	virtual bool WriteBody(Serializer &serializer);
};


//=========================================================================
// Methods
//=========================================================================

/**
	Sets path to the preloaded file.
	@param  file_name    Path to the resource file.
*/
inline
void Preloaded::SetFileName(const string &file_name)
{
	this->file_name = file_name;
}


/**
	Gets path to the preloaded file.

	@return     Path to the preloaded file.
*/
inline
const string &Preloaded::GetFileName()
{
	return this->file_name;
}


/**
	Returns whether preloaded is loaded.
	@return Load state.
*/
inline
bool Preloaded::IsLoaded()
{
	return this->loaded;
}


/**
	Returns whether preloaded is active.
	@return Active state.
*/
inline
bool Preloaded::IsActive()
{
	return this->active;
}


#endif // __preloaded_h__

// vim:ts=4:sw=4:
