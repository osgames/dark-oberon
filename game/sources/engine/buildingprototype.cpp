//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file buildingprototype.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/buildingprototype.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializer.h"

PrepareClass(BuildingPrototype, "Prototype", s_NewBuildingPrototype, s_InitBuildingPrototype);


//=========================================================================
// BuildingPrototype
//=========================================================================

/**
	Inicializes member variables.
*/
BuildingPrototype::BuildingPrototype(const char *id) :
	Prototype(id)
{
	//
}


/**
	Unloades resources and deletes created models.
*/
BuildingPrototype::~BuildingPrototype()
{
	//
}


//=========================================================================
// Deserializing
//=========================================================================

/**
*/
bool BuildingPrototype::Deserialize(Serializer &serializer, bool first)
{
	// call ancestor
	if (!Prototype::Deserialize(serializer, first))
		return false;

	return true;
}


//=========================================================================
// Serializing
//=========================================================================

/**
*/
bool BuildingPrototype::Serialize(Serializer &serializer)
{
	// call ancestor
	if (!Prototype::Serialize(serializer))
		return false;

	/// @todo Implement
	LogError("Serializing is not implemented");
	return true;
}


// vim:ts=4:sw=4:
