#ifndef __mouseinfo_h__
#define __mouseinfo_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file mouseinfo.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/mapstructures.h"
#include "kernel/vector2.h"

class Unit;
class Map;
class PlayerMap;
class Selection;


//=========================================================================
// MouseInfo
//=========================================================================

/**
	@class MouseInfo
	@ingroup Engine_Module
*/
class MouseInfo
{
	friend class Game;

//--- embeded
protected:
	enum CursorType
	{
		CURSOR_ARROW,
		CURSOR_SELECT,
		CURSOR_ADD_REMOVE,
		CURSOR_CAN_MOVE,
		CURSOR_CAN_ATTACK,
		CURSOR_CAN_MINE,
		CURSOR_CAN_REPAIR,
		CURSOR_CAN_BUILD,
		CURSOR_CANT_MOVE,
		CURSOR_CANT_ATTACK,
		CURSOR_CANT_MINE,
		CURSOR_CANT_REPAIR,
		CURSOR_CANT_BUILD,
		CURSOR_POINT,
		CURSOR_HIDE,
		CURSOR_EJECT,
		CURSOR_COUNT
	};


//--- methods
public:
	const MapPosition3D &GetMapPosition() const;
	static const string &GetCursorName(CursorType type);

protected:
	MouseInfo();

	// cursor
	void AddCursor(CursorType type, const string &model_file);
	void SetCursor(CursorType type);

	// per-frame updating
	void UpdatePickUnit(const Map *map);
	void UpdateCursor(const Map *map, const PlayerMap *player_map, const Selection *selection);
	void RenderSelectionRectangle(const Map *map);

	// key callbacks
	void OnLeftKeyDown(const Map *map);
	void OnLeftKeyUp(const Map *map, Selection *selection);
	void OnRightKeyDown(Selection *selection);
	void Reset();

	// properties
	bool IsRectActive() const;


//--- variables
private:
	CursorType cursor_type;
	Unit *pick_unit;                ///< Unit currently under mouse cursor.
	MapPosition3D map_position;

	bool rect_active;
	bool rect_select;
	vector2 rect_corner;
	
	byte_t cursor_id[CURSOR_COUNT];                 ///< Array of cursor identifiers that belong to each cursor type.
	static const string cursor_name[CURSOR_COUNT];  ///< Array of cursor names that belong to each cursor type.
};


//=========================================================================
// Methods
//=========================================================================

inline
bool MouseInfo::IsRectActive() const
{
	return this->rect_active;
}


inline
const MapPosition3D &MouseInfo::GetMapPosition() const
{
	return this->map_position;
}


inline
const string &MouseInfo::GetCursorName(CursorType type)
{
	Assert(type < CURSOR_COUNT);
	return cursor_name[type];
}


#endif // __mouseinfo_h__

// vim:ts=4:sw=4:
