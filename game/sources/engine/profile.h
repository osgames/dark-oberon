#ifndef __profile_h__
#define __profile_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file profile.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/preloaded.h"
#include "kernel/vector.h"
#include "framework/gfxserver.h"


//=========================================================================
// Constants
//=========================================================================

#define PROFILE_DEFAULT_ID string("default")


//=========================================================================
// Profile
//=========================================================================

/**
	@class Profile
	@ingroup Engine_Module

	Stores all profile settings, like resolution, sound volume or race color.
*/
    
class Profile : public Preloaded
{
//--- variables
public:
	// player settings
	struct a
	{
		string name;
		vector3 color;
	} player;

	// input settings
	struct b
	{
		float sensitivity;         ///< Mouse sensitivity in <0.0, 1.0> interval.
		hash_map<string, string> bind_map;   ///< Key binding map.
	} input;

	// video settings
	struct c
	{
		string gui;

		bool fullscreen;
		bool vert_sync;
		byte_t refresh_rate;

		DisplayMode::WidowPosition wnd_position;
		DisplayMode::WidowSize wnd_size;
		DisplayMode::WidowSize full_size;

		GfxServer::MinMagFilter minmag_filter;
		GfxServer::MinMagFilter mipmap_filter;
		ushort_t max_fps;
		string unit_selection;
		string building_selection;

		vector4 warfog_color;
		bool show_fps;
		float map_move_speed;
		float map_zoom_speed;
	} video;

	// audio settings
	struct d
	{
		float master_volume;
		bool menu_play_music;
		float menu_music_volume;
		float menu_sound_volume;
		bool game_play_music;
		bool game_play_speach;
		float game_music_volume;
		float game_sound_volume;
	} audio;

	// network settings
	struct e
	{
		string last_address;
		int server_port;
	} network;

	struct f
	{
		string font_path;
		vector3 font_color;
		byte_t font_size;
		bool log_to_ost;
		bool fps_show;
		float fps_interval;
	} system;

//--- methods
public:
	Profile(const char *id);
	virtual ~Profile();

	void LoadDefaultSettings();

protected:
	void SetDefaultSettings();

	virtual void ClearHeader();
	virtual void ClearBody();

	// serializing
	virtual bool ReadHeader(Serializer &serializer);
	virtual bool ReadBody(Serializer &serializer);
	virtual bool WriteHeader(Serializer &serializer);
	virtual bool WriteBody(Serializer &serializer);
};


#endif // __profile_h__

// vim:ts=4:sw=4:
