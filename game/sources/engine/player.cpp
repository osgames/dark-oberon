//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file player.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/player.h"
#include "engine/playermap.h"
#include "engine/unit.h"
#include "engine/unitmessages.h"
#include "engine/game.h"
#include "engine/level.h"
#include "engine/race.h"
#include "engine/prototype.h"
#include "framework/sceneobject.h"
#include "kernel/serializer.h"
#include "kernel/kernelserver.h"

PrepareScriptClass(Player, "PlayerRemoteClient", s_NewPlayer, s_InitPlayer, s_InitPlayer_cmds);


//=========================================================================
// Player
//=========================================================================

/**
	Sets color to default red.
*/
Player::Player(const char *id) :
	PlayerRemoteClient(id),
	name("Player"),
	color(1.0f, 0.2f, 0.1f),
	active(false),
	map(NULL)
{
	//
}


/**
	Unloads resources and destroys player map.
*/
Player::~Player()
{
	this->DeletePlayerMap();
	this->UnloadResources();
}


bool Player::LoadResources()
{
	this->race = Game::GetInstance()->GetRace(this->race_id);

	/// @todo Load missing race from server.
	if (!this->race.IsValid())
	{
		LogError1("Missing race: %s", this->race_id.c_str());
		return false;
	}

	if (!this->race->Load())
	{
		LogError1("Error loading race: %s", this->race_id.c_str());
		return false;
	}

	// units may be loaded automatically, we just need to save the reference
	this->units = this->Find(string("units"));

	// if there are no units defined, create storage node
	if (!this->units.IsValid())
	{
		this->kernel_server->PushCwd(this);
		this->units = this->kernel_server->New("Root", string("units"));
		this->kernel_server->PopCwd();
	}

	// register all loaded units
	Unit *unit, *next_unit;
	for (unit = (Unit *)this->units->GetFront(); unit; )
	{
		next_unit = (Unit *)unit->GetNext();

		if (!this->RegisterUnit(unit))
			unit->Release();

		unit = next_unit;
	}

	// finish loading of all units
	Prototype *prototype;
	for (unit = (Unit *)this->units->GetFront(); unit; unit = (Unit *)unit->GetNext())
	{
		Assert(!unit->GetPrototype());

		prototype = this->race->GetPrototype(unit->prototype_id);
		if (!prototype)
			return NULL;

		unit->SetPrototype(prototype);
	}

	return true;
}


void Player::UnloadResources()
{
	// unload race
	if (this->race.IsValid())
		this->race->Unload();
}


Unit *Player::NewUnit(const string &prototype_id)
{
	Assert(this->race->IsLoaded());

	Prototype *prototype = this->race->GetPrototype(prototype_id);
	if (!prototype)
		return NULL;

	return this->NewUnit(prototype);
}


Unit *Player::NewUnit(Prototype *prototype)
{
	Assert(prototype);

	// create new object
	this->units->Lock();
	this->kernel_server->PushCwd(this->units);

	// generate identifier
	char txt[10];
	static ushort_t num_id = 1;
	sprintf(txt, "unit_%d", num_id);
	string id = txt;

	// create unit appropriate to type of prototype
	Unit *unit;
	if (prototype->IsA("BuildingPrototype"))
		unit = (Unit *)this->kernel_server->New("BuildingUnit", id);
	else if (prototype->IsA("ForcePrototype"))
		unit = (Unit *)this->kernel_server->New("ForceUnit", id);
	else
		ErrorMsg("Unknow prototype type");
	Assert(unit);

	// register unit
	if (!this->RegisterUnit(unit))
	{
		unit->Release();
		unit = NULL;
	}

	this->kernel_server->PopCwd();
	this->units->Unlock();

	// set prototype
	if (unit)
		unit->SetPrototype(prototype);

	return unit;
}


bool Player::RegisterUnit(Unit *unit)
{
	Assert(unit && !unit->GetPlayer() && !unit->remote_id);

	unit->player = this;

	// generate remote identifier
	static ushort_t counter = 1;

	// add unit to hash map
	this->units_map[counter] = unit;
	unit->remote_id = counter++;

	return true;
}


bool Player::DeregisterUnit(Unit *unit)
{
	Assert(unit && unit->GetPlayer() == this && unit->remote_id);

	unit->player = NULL;

	// remove unit from hash map
	UnitsMap::iterator itUnit = this->units_map.find(unit->remote_id);
	if (itUnit == this->units_map.end())
		return false;

	this->units_map.erase(itUnit);
	return true;
}


void Player::CreatePlayerMap(const MapSize &map_size)
{
	Assert(map_size.width && map_size.height);
	Assert(!this->map);

	// create player local map
	this->map = NEW PlayerMap(map_size, this->active);
	Assert(this->map);
}


void Player::DeletePlayerMap()
{
	SafeDelete(this->map);
}


bool Player::UpdatePlayerMap(const Unit *unit, bool added)
{
	if (unit->GetPlayer() != this)
		return true;

	Assert(this->map);
	return this->map->UpdateWarfog(unit, added);
}


void Player::Update()
{
	Unit *unit;
	list<Unit *> ghosts, deleted;
	list<Unit *>::iterator it;
	bool create_ghost, to_delete;
	Level *active_level = Game::GetInstance()->GetActiveLevel();

	this->units->Lock();

	// update all units
	for (unit = (Unit *)this->units->GetFront(); unit; unit = (Unit *)unit->GetNext())
	{
		unit->Update(create_ghost, to_delete);
		if (create_ghost)
			ghosts.push_back(unit);
		if (to_delete)
			deleted.push_back(unit);
	}

	// create ghosts
	for (it = ghosts.begin(); it != ghosts.end(); it++)
	{
		unit = this->NewUnit((*it)->GetPrototype());
		unit->SetState(Unit::STATE_GHOST);
		unit->SetGhostOwner(*it);
		unit->SetMapPosition((*it)->GetMapPosition());
		unit->SetRealPosition((*it)->GetRealPosition().x, (*it)->GetRealPosition().y);
		unit->SetLife((*it)->GetLife());
		unit->GetSceneObject()->PauseAnimators();

		Verify(active_level->AddUnitToMap(unit));
	}

	// remove deleted units
	for (it = deleted.begin(); it != deleted.end(); it++)
	{
		Assert((*it)->TestState(Unit::STATE_DELETE) || (*it)->TestState(Unit::STATE_GHOST));

		active_level->RemoveUnitFromMap(*it);
		(*it)->Release();
	}

	this->units->Unlock();
}


//=========================================================================
// Messages
//=========================================================================

void Player::OnMessage(ActionEvent *pevent)
{
	Assert(pevent && pevent->destination_id);

	// find unit
	Unit *unit = this->units_map[pevent->destination_id];
	Assert(unit);

	// send event to the unit
	this->SendMessage(pevent, unit);
}


void Player::OnMessage(InputEvent *pevent)
{
	Assert(pevent && pevent->destination_id);

	// find unit
	Unit *unit = this->units_map[pevent->destination_id];
	Assert(unit);

	// send event to the unit
	this->SendMessage(pevent, unit);
}


//=========================================================================
// Deserializing
//=========================================================================

bool Player::Deserialize(Serializer &serializer, bool first)
{
	// read	name [optional]
	serializer.GetAttribute("name", this->name);

	// read	race [mandatory]
	if (!serializer.GetAttribute("race", this->race_id, false))
	{
		LogError1("Error reading race of player: %s", this->id.c_str());
		return false;
	}

	// read	acive [optional]
	serializer.GetAttribute("active", this->active);

	// read color [optional]
	serializer.GetAttributeColor("color", this->color);
	
	// call ancestor
	return Root::Deserialize(serializer, first);
}


//=========================================================================
// Serializing
//=========================================================================

/**
	@todo Implement.
*/
bool Player::Serialize(Serializer &serializer)
{
	// write name
	if (!serializer.SetAttribute("name", this->name)) return false;

	// call ancestor
	return Root::Serialize(serializer);
}


// vim:ts=4:sw=4:
