//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file playermap.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/playermap.h"
#include "engine/unit.h"
#include "engine/prototype.h"
#include "engine/warfog.h"
#include "framework/gfxserver.h"


//=========================================================================
// PlayerMap
//=========================================================================

/**
	Creates warfog.
*/
PlayerMap::PlayerMap(const MapSize &size, bool warfog_texture) :
	map_size(size)
{
	Assert(map_size.width && map_size.height);

	// set warfog size
	this->warfog_size = size;
	this->warfog_size.width += WARFOG_BOUNDARY + SEGMENTS_SHIFT + 2;
	this->warfog_size.height += WARFOG_BOUNDARY + SEGMENTS_SHIFT + 2;

	byte_t i;
	int x;

	for (i = 0; i < MapEnums::SEG_COUNT; i++)
	{
		// create warfog
		this->warfog_field[i] = Create2DArray<byte_t>(this->warfog_size.width, this->warfog_size.height);
		Assert(this->warfog_field);

		// init warfog to unknown
		for (x = 0; x < this->warfog_size.width; x++)
			memset(this->warfog_field[i][x], Warfog::WARFOG_UNKNOWN, sizeof(byte_t) * this->warfog_size.height);

		// create warfog
		if (warfog_texture)
		{
			this->warfog[i] = NEW Warfog(this->warfog_size.width, this->warfog_size.height);
			Assert(this->warfog);
		}
		else
			this->warfog[i] = NULL;
	}
}


/**
	Deletes warfog of each segment.
*/
PlayerMap::~PlayerMap()
{
	for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++)
	{
		Delete2DArray<byte_t>(this->warfog_field[i], this->warfog_size.width);
		SafeDelete(this->warfog[i]);
	}
}


bool PlayerMap::UpdateWarfog(const Unit *unit, bool added)
{
	MapPosition3D unit_position = unit->GetMapPosition();
	unit_position.x++;
	unit_position.y++;
	
	MapEnums::Segment segment = unit_position.segment == MapEnums::SEG_UNDERGROUND ? MapEnums::SEG_UNDERGROUND : MapEnums::SEG_GROUND;  // ground and sky segments are updated together

	int unit_width2 = unit->GetPrototype()->GetSize().width / 2;
	int unit_height2 = unit->GetPrototype()->GetSize().height / 2;
	int unit_view = unit->GetPrototype()->GetView();

	int x, y;
	byte_t *warfog_value;
	
	// prepare envelope of view circle
	int left, right, bottom, top;

	left = unit_position.x + unit_width2 - unit_view + SEGMENTS_SHIFT;
	if (left >= this->warfog_size.width)
		return false;
	else if (left < 0)
		left = 0;

	bottom = unit_position.y + unit_height2 - unit_view + SEGMENTS_SHIFT;
	if (bottom >= this->warfog_size.height)
		return false;
	else if (bottom < 0)
		bottom = 0;

	right = unit_position.x + unit_width2 + unit_view + SEGMENTS_SHIFT;
	if (right < 0)
		return false;
	else if (right >= this->warfog_size.width)
		right = this->warfog_size.width - 1;

	top = unit_position.y + unit_height2 + unit_view + SEGMENTS_SHIFT;
	if (top < 0)
		return false;
	else if (top >= this->warfog_size.height)
		top = this->warfog_size.height - 1;

	float distance;

	// update segment and multiple warfog 
	for (x = left; x <= right ; x++)
	{
		for (y = bottom; y <= top; y++)
		{
			distance = sqrt(float(n_sqr(x - unit_position.x - SEGMENTS_SHIFT - unit_width2) + n_sqr(y - unit_position.y - SEGMENTS_SHIFT - unit_height2)));
			if (distance > unit_view)
				continue;

			// update warfog of segment
			warfog_value = &this->warfog_field[segment][x][y];

			if (added)
			{
				if (*warfog_value == (byte_t)Warfog::WARFOG_UNKNOWN)
					*warfog_value = Warfog::WARFOG_HIDED + 1;
				else
					(*warfog_value)++;
			}
			else
			{
				Assert(*warfog_value > Warfog::WARFOG_HIDED);
				(*warfog_value)--;
			}

			// update warfog of multiple segments
			if (segment == MapEnums::SEG_GROUND)
				this->warfog_field[MapEnums::SEG_SKY][x][y] = *warfog_value;

			else if (x >= SEGMENTS_SHIFT && y >= SEGMENTS_SHIFT)
			{
				if (this->warfog_field[MapEnums::SEG_GROUND][x - SEGMENTS_SHIFT][y - SEGMENTS_SHIFT] == Warfog::WARFOG_UNKNOWN)
					this->warfog_field[MapEnums::SEG_SKY][x - SEGMENTS_SHIFT][y - SEGMENTS_SHIFT] = *warfog_value;
			}
		}
	}

	return true;
}


void PlayerMap::RenderWarfog(MapEnums::Segment segment, const MapArea &visible_area)
{
	Assert(segment < MapEnums::SEG_COUNT);
	Assert(this->warfog[segment]);
	Assert(visible_area.x + visible_area.width <= this->map_size.width);
	Assert(visible_area.y + visible_area.height <= this->map_size.height);

	MapArea area = visible_area;

	// if right edge of the map is visible, we need to add warfog boundary
	if (area.x + area.width == this->map_size.width)
		area.width += WARFOG_BOUNDARY;

	// if top edge of the map is visible, we need to add warfog boundary
	if (area.y + area.height == this->map_size.height)
		area.height += WARFOG_BOUNDARY;

	// if left edge of the map is visible, we need to add segments shift or translate visible area otherwise
	if (area.x)
		area.x += SEGMENTS_SHIFT;
	else
		area.width += SEGMENTS_SHIFT;

	// if bottom edge of the map is visible, we need to add segments shift or translate visible area otherwise
	if (area.y)
		area.y += SEGMENTS_SHIFT;
	else
		area.height += SEGMENTS_SHIFT;

	// undergroud segment is shifted
	if (segment == MapEnums::SEG_UNDERGROUND)
	{
		area.width += SEGMENTS_SHIFT;
		if (int(area.x) + area.width > this->warfog_size.width)
			area.width = this->warfog_size.width - area.x;

		area.height += SEGMENTS_SHIFT;
		if (int(area.y) + area.height > this->warfog_size.height)
			area.height = this->warfog_size.height - area.y;
	}

	// update warfog of given segment
	this->warfog[segment]->Render(warfog_field[segment], area);
}


#define IsHole(i, j) (warfog_field[segment][i][j] == Warfog::WARFOG_UNKNOWN && warfog_field[MapEnums::SEG_SKY][i][j] != Warfog::WARFOG_UNKNOWN)
#define IsUnknownH2(i, j) (warfog_field[segment][i - 1][j] == Warfog::WARFOG_UNKNOWN && warfog_field[segment][i + 1][j] == Warfog::WARFOG_UNKNOWN)
#define IsUnknownH3(i, j) (IsUnknownH2(i, j) && warfog_field[segment][i][j] == Warfog::WARFOG_UNKNOWN)
#define IsUnknownV2(i, j) (warfog_field[segment][i][j - 1] == Warfog::WARFOG_UNKNOWN && warfog_field[segment][i][j + 1] == Warfog::WARFOG_UNKNOWN)
#define IsUnknownV3(i, j) (IsUnknownV2(i, j) && warfog_field[segment][i][j] == Warfog::WARFOG_UNKNOWN)

void PlayerMap::RenderMask(MapEnums::Segment segment, const MapArea &visible_area)
{
	Assert(segment < MapEnums::SEG_COUNT);

	if (segment == MapEnums::SEG_UNDERGROUND)
		return;

	MapArea area = visible_area;
	area.x++;
	area.y++;

	GfxServer *gfx_server = GfxServer::GetInstance();
	Assert(gfx_server);

	mapel_t j, i;
	bool draw;
	float x1, x2, y1, y2;
	vector2 coords[4];

	gfx_server->ResetMatrix();
	gfx_server->Translate(vector3(0.0f, 0.0f, 0.1f));
	gfx_server->SetColorMask(false, false, false, false);

	for (j = area.y; j < area.y + area.height + SEGMENTS_SHIFT; j++)
	{
		for (i = area.x; i < area.x + area.width + SEGMENTS_SHIFT; i++)
		{
			draw = (warfog_field[segment][i][j] == Warfog::WARFOG_UNKNOWN) &&
				(
					(warfog_field[MapEnums::SEG_SKY][i][j] != Warfog::WARFOG_UNKNOWN)
					|| (IsHole(i + 1, j) && IsUnknownV2(i, j) && IsUnknownV3(i - 1, j))
					|| (IsHole(i - 1, j) && IsUnknownV2(i, j) && IsUnknownV3(i + 1, j))
					|| (IsHole(i, j + 1) && IsUnknownH2(i, j) && IsUnknownH3(i, j - 1))
					|| (IsHole(i, j - 1) && IsUnknownH2(i, j) && IsUnknownH3(i, j + 1))
				);

			if (draw)
			{
				/// @todo Optimise using call list for rendering a quad.
				x1 = float(i - SEGMENTS_SHIFT - 1);
				y1 = float(j - SEGMENTS_SHIFT - 1);
				x2 = x1 + 1.1f;
				y2 = y1 + 1.1f;

				coords[0].set((x1 - y1) * ISO_SCENE_H_COEF, (x1 + y1) * ISO_SCENE_V_COEF);
				coords[1].set((x1 - y2) * ISO_SCENE_H_COEF, (x1 + y2) * ISO_SCENE_V_COEF);
				coords[2].set((x2 - y2) * ISO_SCENE_H_COEF, (x2 + y2) * ISO_SCENE_V_COEF);
				coords[3].set((x2 - y1) * ISO_SCENE_H_COEF, (x2 + y1) * ISO_SCENE_V_COEF);

				gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
			}
		}
	}

	gfx_server->SetColorMask(true, true, true, true);
	gfx_server->Translate(vector3(0.0f, 0.0f, -0.1f));
}


// vim:ts=4:sw=4:
