//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file player_cmds.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/player.h"


//=========================================================================
// Commands
//=========================================================================

static void s_NewUnit(void *slf, Cmd *cmd)
{
	Player *self = (Player *)slf;
	string proto = cmd->In()->GetS();
	cmd->Out()->SetO(self->NewUnit(proto));
}


void s_InitPlayer_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("o_NewUnit_s", 'NUNI', s_NewUnit);
	
	cl->EndCmds();
}

// vim:ts=4:sw=4:
