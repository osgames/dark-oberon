//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file prototype.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/prototype.h"
#include "engine/scheme.h"
#include "engine/race.h"
#include "engine/map.h"
#include "kernel/kernelserver.h"
#include "kernel/serializer.h"
#include "kernel/logserver.h"

PrepareClass(Prototype, "Root", s_NewPrototype, s_InitPrototype);


//=========================================================================
// Prototype
//=========================================================================

Prototype::Prototype(const char *id) :
	Root(id),
	name("Unnamed Prototype"),
	materials(NULL),
	life(0),
	view(5),
	food(0),
	energy(0),
	min_energy(0),
	selection_height(50.0f),
	aggressivity(Unit::AGGRESSIVITY_GUARD),
	hiding_capacity(0),
	can_move(false),
	can_attack(false),
	can_mine(false),
	can_repair(false),
	can_build(false),
	can_hide(false)
{
	//
}


/**
	Releases materials and terrains.
*/
Prototype::~Prototype()
{
	delete[] this->materials;

	for (byte_t i = 0; i < MapEnums::SEG_COUNT; i++)
		delete[] this->segments[i].terrains;
}


//=========================================================================
// Tools
//=========================================================================

/**
	@todo Corners are checked twice if unit is moving in diagonal direction.
*/
void Prototype::CheckPosition(const Map *map,
							  const MapPosition3D &position, MapEnums::Direction direction, 
							  bool &is_moveable, bool &is_free)
{
	Assert(map);
	Assert(position.segment < MapEnums::SEG_COUNT);
	Assert(direction < MapEnums::DIRECTION_COUNT);

	const MapSize &unit_size = this->GetSize();
	MapPosition3D act_position;

	// initialize checking
	is_moveable = is_free = true;
	act_position.segment = position.segment;

	// top edge
	if (direction == MapEnums::DIRECTION_NORTH || direction == MapEnums::DIRECTION_NORTH_EAST || direction == MapEnums::DIRECTION_NORTH_WEST)
	{
		act_position.y = position.y + unit_size.height - 1;
		for (act_position.x = position.x; act_position.x < position.x + unit_size.width; act_position.x++)
		{
			if (!CheckPosition(map, act_position, is_moveable, is_free))
				return;
		}
	}

	// bottom edge
	else if (direction == MapEnums::DIRECTION_SOUTH || direction == MapEnums::DIRECTION_SOUTH_EAST || direction == MapEnums::DIRECTION_SOUTH_WEST)
	{
		act_position.y = position.y;
		for (act_position.x = position.x; act_position.x < position.x + unit_size.width; act_position.x++)
		{
			if (!CheckPosition(map, act_position, is_moveable, is_free))
				return;
		}
	}

	// right edge
	if (direction == MapEnums::DIRECTION_EAST || ((direction == MapEnums::DIRECTION_NORTH_EAST || direction == MapEnums::DIRECTION_SOUTH_EAST) && unit_size.height > 1))
	{
		act_position.x = position.x + unit_size.width - 1;
		for (act_position.y = position.y; act_position.y < position.y + unit_size.height; act_position.y++)
		{
			if (!CheckPosition(map, act_position, is_moveable, is_free))
				return;
		}
	}

	// left edge
	else if (direction == MapEnums::DIRECTION_WEST || ((direction == MapEnums::DIRECTION_NORTH_WEST || direction == MapEnums::DIRECTION_SOUTH_WEST) && unit_size.height > 1))
	{
		act_position.x = position.x;
		for (act_position.y = position.y; act_position.y < position.y + unit_size.height; act_position.y++)
		{
			if (!CheckPosition(map, act_position, is_moveable, is_free))
				return;
		}
	}

	// direction is up or down, check whole area
	if (direction == MapEnums::DIRECTION_UP || direction == MapEnums::DIRECTION_DOWN)
	{
		for (act_position.x = position.x; act_position.x < position.x + unit_size.width; act_position.x++)
		{
			for (act_position.y = position.y; act_position.y < position.y + unit_size.height; act_position.y++)
			{
				if (!CheckPosition(map, act_position, is_moveable, is_free))
					return;
			}
		}
	}
}


bool Prototype::CheckPosition(const Map *map,
							  const MapPosition3D &position,
							  bool &is_moveable, bool &is_free)
{
	Assert(map && map->IsInMap(position));

	const Prototype::Terrain *terrain;
	Map::Field *field;

	// get info
	terrain = this->GetTerrain(position.segment, map->GetSurface(position));
	field = map->GetField(position);

	// if there is some unit (moving or not), position is not free
	if (field->unit)
		is_free = false;

	// if there is some non-moveable terrain or not-moving unit, position is not moveable
	// we can break checking
	if (!terrain || !terrain->speed || !terrain->rotation || !terrain->can_move || (field->unit && !field->unit->IsMoving()))
		is_moveable = false;

	return is_moveable;
}


void Prototype::GetSpeeds(const Map *map,
						  const MapPosition3D &position, MapEnums::Segment next_segment,
						  float &speed, float &rotation)
{
	Assert(map && map->IsInMap(position));
	Assert(position.segment < MapEnums::SEG_COUNT);
	Assert(next_segment < MapEnums::SEG_COUNT);

	const Prototype::Terrain *terrain;
	MapPosition3D act_position;
	byte_t surface;

	speed = rotation = 0.0f;

	act_position.segment = position.segment;

	// get info for each field under unit. If speed is not defined for actual segment, try to use next segment.
	// This is important for moving shrough segments.
	for (act_position.x = position.x; act_position.x < position.x + this->size.width; act_position.x++)
	{
		for (act_position.y = position.y; act_position.y < position.y + this->size.height; act_position.y++)
		{
			surface = map->GetSurface(act_position);
			terrain = this->GetTerrain(act_position.segment, surface);
			Assert(terrain);

			// if speed is not defined for actual segment, try next one
			if ((!terrain->speed || !terrain->rotation) && next_segment != position.segment)
			{
				terrain = this->GetTerrain(next_segment, surface);
				Assert(terrain);
			}
			
			// speed is still not defined, we can finish
			if (!terrain->speed || !terrain->rotation)
			{
				speed = rotation = 0.0f;
				return;
			}

			// add values to the sum
			speed += terrain->speed;
			rotation += terrain->rotation;
		}
	}

	// compute average speed
	float fields = float(this->size.width * this->size.height);
	speed = speed / fields;
	rotation = rotation / fields;
}


bool Prototype::CanHide(Prototype *prototype) const
{
	Assert(prototype);
	Assert(this->can_hide);

	// try to find prototype in cache
	HidingCache::const_iterator it = this->hiding_cache.find(prototype);
	if (it != this->hiding_cache.end())
		return it->second;

	// check if prototype can be hided here
	bool found = false;
	for (size_t i = 0; i < this->hiding.size() && !found; i++)
		found = (this->hiding[i] == prototype->GetID());

	// save result to the cache
	this->hiding_cache[prototype] = found;

	return found;
}


//=========================================================================
// Deserializing
//=========================================================================

/**
*/
bool Prototype::Deserialize(Serializer &serializer, bool first)
{
	// read	name [optional]
	serializer.GetAttribute("name", this->name);

	// read	life [mandatory]
	if (!serializer.GetAttribute("life", this->life))
	{
		LogError1("Error reading life of prototype: %s", this->id.c_str());
		return false;
	}

	// read	view [optional]
	mapel_t mapel_value;
	serializer.GetAttribute("view", mapel_value);
	if (!mapel_value)
	{
		LogWarning1("Wrong value of view in prototype: %s", this->id.c_str());
		return false;
	}
	else
		this->view = mapel_value;

	// read	size [mandatory]
	if (!this->ReadSize(serializer))
	{
		LogError1("Error reading size of prototype: %s", this->id.c_str());
		return false;
	}

	// read materials [optional]
	this->ReadMaterials(serializer);

	// read resources [optional]
	this->ReadResources(serializer);

	// read	models [one mandatory]
	if (!this->ReadModels(serializer))
	{
		LogError1("Error reading models of prototype: %s", this->id.c_str());
		return false;
	}

	// read graphics [optional]
	this->ReadGraphics(serializer);

	// read	segments [mandatory]
	if (!this->ReadSegments(serializer))
	{
		LogError1("Error reading segments of prototype: %s", this->id.c_str());
		return false;
	}

	// read graphics [optional]
	this->ReadHiding(serializer);
	
	// call ancestor
	return Root::Deserialize(serializer, first);
}


bool Prototype::ReadSize(Serializer &serializer)
{
	// read resources [mandatory]
	if (!serializer.GetGroup("size"))
	{
		LogError1("Missing size of prototype: %s", this->id.c_str());
		return false;
	}
	
	// read dimensions or diameter [mandatory]
	if (serializer.GetAttribute("diameter", this->size.width))
		this->size.height = this->size.width;

	else if (!serializer.GetAttribute("width", this->size.width) || !serializer.GetAttribute("height", this->size.height))
	{
		LogError1("Missing size of prototype: %s", this->id.c_str());
		return false;
	}

	serializer.EndGroup();

	return true;
}


bool Prototype::ReadMaterials(Serializer &serializer)
{
	byte_t count;
	byte_t i = 0;

	Scheme *scheme = ((Race *)this->GetParent()->GetParent())->GetScheme();

	if (!scheme->GetMaterialsCount())
		return true;

	// read	materials [optional]
	if (!serializer.GetGroup("materials"))
		return true;

	count = (byte_t)serializer.GetGroupsCount();
	if (!count)
		return true;

	// create materials array
	this->materials = NEW Material[scheme->GetMaterialsCount()];
	Assert(this->materials);

	if (serializer.GetGroup() && serializer.CheckGroupName("material"))
	{
		string material_id;
		Scheme::Material *material_ref; 

		do {
			// read	id [mandatory]
			if (!serializer.GetAttribute("id", material_id, false))
			{
				LogError1("Missing material identifier of prototype: %s", this->id.c_str());
				return false;
			}

			// find scheme material [mandatory]
			material_ref = scheme->GetMaterial(material_id);
			if (!material_ref)
			{
				LogError1("Unknown material of prototype: %s", material_id.c_str());
				return false;
			}

			// read	price [optional]
			serializer.GetAttribute("price", this->materials[material_ref->num_id].price);

			// read	accept [optional]
			serializer.GetAttribute("accept", this->materials[material_ref->num_id].accept);

			i++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (i < count)
		LogWarning1("Materials mismatch in prototype: %s", this->id.c_str());

	serializer.EndGroup();

	return true;
}


bool Prototype::ReadResources(Serializer &serializer)
{
	// read resources [optional]
	if (!serializer.GetGroup("resources"))
		return true;
	
	serializer.GetAttribute("food", this->food);
	serializer.GetAttribute("energy", this->energy);
	serializer.GetAttribute("min-energy", this->min_energy);

	serializer.EndGroup();

	return true;
}


bool Prototype::ReadModels(Serializer &serializer)
{
	byte_t count;
	byte_t i = 0;

	// read models [one mandatory]
	if (!serializer.GetGroup("models") || !(count = (byte_t)serializer.GetGroupsCount()))
	{
		LogError1("Missing models in prototype: %s", this->id.c_str());
		return false;
	}

	// create models array
	this->models.reserve(count);
	Model model;

	if (serializer.GetGroup() && serializer.CheckGroupName("model"))
	{
		do {
			// read	id [mandatory]
			if (!serializer.GetAttribute("id", model.id, false))
			{
				LogError1("Missing model id of prototype: %s", this->id.c_str());
				return false;
			}

			// read	model [mandatory]
			if (!serializer.GetAttribute("src", model.file_name, false))
			{
				LogError1("Missing source of model in prototype: %s", this->id.c_str());
				return false;
			}

			// add new model to hash map
			this->models.push_back(model);

			i++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (i < count)
		LogWarning1("Models mismatch in prototype: %s", this->id.c_str());

	serializer.EndGroup();
	return true;
}


bool Prototype::ReadGraphics(Serializer &serializer)
{
	// read graphics [optional]
	if (!serializer.GetGroup("graphics"))
		return true;
	
	serializer.GetAttribute("selection-height", this->selection_height);
	this->selection_height = (this->selection_height + this->selection_height) * ISO_SCENE_V_COEF;

	serializer.EndGroup();

	return true;
}


bool Prototype::ReadSegments(Serializer &serializer)
{
	byte_t count;
	byte_t i = 0;

	// read segments [one mandatory]
	if (!serializer.GetGroup("segments") || !(count = (byte_t)serializer.GetGroupsCount()))
	{
		LogError1("Missing segments in prototype: %s", this->id.c_str());
		return false;
	}

	if (serializer.GetGroup() && serializer.CheckGroupName("segment"))
	{
		string segment_id;
		MapEnums::Segment segment;

		do {
			// read	id [mandatory]
			if (!serializer.GetAttribute("id", segment_id, false))
			{
				LogError1("Missing segment id in prototype: %s", this->id.c_str());
				return false;
			}

			// check segment id
			if (!MapEnums::StringToSegment(segment_id, segment))
			{
				LogError1("Wrong segment id in prototype: %s", this->id.c_str());
				return false;
			}

			// read properties [optional]
			serializer.GetAttribute("shootable", this->segments[segment].shootable);

			// read terrains [one mandatory]
			if (!this->ReadTerrains(serializer, this->segments[segment]))
			{
				LogError1("Error reading terrains of prototype: %s", this->id.c_str());
				return false;
			}
			
			i++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (i < count)
		LogWarning1("Segments mismatch in prototype: %s", this->id.c_str());

	serializer.EndGroup();
	return true;
}


bool Prototype::ReadTerrains(Serializer &serializer, Segment &segment)
{
	byte_t count;
	byte_t i = 0;
	bool has_stay = false;

	// read terrains [one mandatory]
	if (!serializer.GetGroup("terrains") || !(count = (byte_t)serializer.GetGroupsCount()))
	{
		LogError1("Missing terrains in prototype: %s", this->id.c_str());
		return false;
	}

	Scheme *scheme = ((Race *)this->GetParent()->GetParent())->GetScheme();
	Assert(scheme->IsLoaded());

	if (serializer.GetGroup() && serializer.CheckGroupName("terrain"))
	{
		string terrain_id;
		byte_t terrain;

		// create terrains array
		Assert(scheme->GetTerrainsCount());
		segment.terrains = NEW Terrain[scheme->GetTerrainsCount()];
		Assert(segment.terrains);

		do
		{
			// read	id [mandatory]
			if (!serializer.GetAttribute("id", terrain_id, false))
			{
				LogError1("Missing terrain id in prototype: %s", this->id.c_str());
				return false;
			}

			// check terrain id
			terrain = scheme->GetTerrain(terrain_id);
			if (!terrain)
			{
				LogError1("Undefined terrain id in prototype: %s", this->id.c_str());
				return false;
			}

			// terrains are indexed form 1 in the scheme, so we need to shift them
			terrain--;

			// read properties [optional]
			if (serializer.GetAttribute("stay", segment.terrains[terrain].can_stay))
				has_stay = true;
			serializer.GetAttribute("move", segment.terrains[terrain].can_move);
			serializer.GetAttribute("speed", segment.terrains[terrain].speed);
			serializer.GetAttribute("rotation", segment.terrains[terrain].rotation);

			if (segment.terrains[terrain].can_move && !(segment.terrains[terrain].speed && segment.terrains[terrain].rotation))
				LogWarning1("Terrain has move parameter set to true, but speed or rotation parameter is not specified in prototype: %s", this->id.c_str());

			i++;
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (i < count)
		LogWarning1("Terrains mismatch in prototype: %s", this->id.c_str());

	// check if there is at least one terrain with stay parameter
	if (!has_stay)
	{
		LogError1("Missing terrain for staying in prototype: %s", this->id.c_str());
		return false;
	}

	serializer.EndGroup();
	return true;
}


bool Prototype::ReadHiding(Serializer &serializer)
{
	byte_t count;

	// read	hiding [optional]
	if (!serializer.GetGroup("hiding"))
		return true;

	// read	capacity [mandatory]
	if (!serializer.GetAttribute("capacity", this->hiding_capacity))
	{
		LogError1("Missing hiding capacity in prototype: %s", this->id.c_str());
		return false;
	}

	count = (byte_t)serializer.GetGroupsCount();
	if (!count)
		return true;

	// prepare array
	this->hiding.reserve(count);

	if (serializer.GetGroup() && serializer.CheckGroupName("prototype"))
	{
		string prototype_id;

		do {
			// read	id [mandatory]
			if (!serializer.GetAttribute("id", prototype_id, false))
			{
				LogError1("Missing hiding prototype identifier in prototype: %s", this->id.c_str());
				return false;
			}

			this->hiding.push_back(prototype_id);
		} while (serializer.GetNextGroup());

		serializer.EndGroup();
	}

	if (this->hiding.size() < count)
		LogWarning1("Hiding prototypes mismatch in prototype: %s", this->id.c_str());

	serializer.EndGroup();

	this->can_hide = this->hiding.size() > 0;
	return true;
}


//=========================================================================
// Serializing
//=========================================================================

/**
	@todo Implement.
*/
bool Prototype::Serialize(Serializer &serializer)
{
	// write name
	if (!serializer.SetAttribute("name", this->name)) return false;

	// call ancestor
	return Root::Serialize(serializer);
}


// vim:ts=4:sw=4:
