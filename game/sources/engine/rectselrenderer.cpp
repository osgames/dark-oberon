//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file rectselrenderer.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/rectselrenderer.h"
#include "engine/unit.h"
#include "engine/prototype.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"

PrepareClass(RectSelRenderer, "SelectionRenderer", s_NewRectSelRenderer, s_InitRectSelRenderer);


//=========================================================================
// Settings
//=========================================================================

#define BAR_SIZE   4.0f  ///< Size of status bar.
#define BAR_INDENT 0.7f  ///< Coeficient to make bar shorter.


//=========================================================================
// RectSelRenderer
//=========================================================================

RectSelRenderer::RectSelRenderer(const char *id) :
	SelectionRenderer(id)
{
	//
}


void RectSelRenderer::RenderBackground(Unit *unit)
{
	Assert(unit);

	if (!unit->IsAnyFlag(Unit::FLAG_SELECTED | Unit::FLAG_UNDER_CURSOR))
		return;

	Prototype *prototype = unit->GetPrototype();
	Assert(prototype);

	static vector2 coords[4];
	static MapSize last_size;

	// update coodrinates
	if (last_size != prototype->GetSize())
	{
		last_size = prototype->GetSize();

		static float x2, y2;
		x2 = last_size.width;
		y2 = last_size.height;

		coords[0].set(0.0f, 0.0f);
		coords[1].set(-y2 * ISO_SCENE_H_COEF, y2 * ISO_SCENE_V_COEF);
		coords[2].set((x2 - y2) * ISO_SCENE_H_COEF, (x2 + y2) * ISO_SCENE_V_COEF);
		coords[3].set(x2 * ISO_SCENE_H_COEF, x2 * ISO_SCENE_V_COEF);
	}

	// render hover quad
	if (unit->IsFlags(Unit::FLAG_UNDER_CURSOR))
	{
		this->gfx_server->SetColor(this->GetHoverColor(unit));
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
	}

	// render selection rectangle
	if (unit->IsFlags(Unit::FLAG_SELECTED))
	{
		this->gfx_server->SetColor(this->GetSelectionColor(unit));
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_LINE_LOOP, coords, 4);
	}
}


void RectSelRenderer::RenderForeground(Unit *unit)
{
	Assert(unit);

	if (!unit->IsAnyFlag(Unit::FLAG_SELECTED | Unit::FLAG_UNDER_CURSOR))
		return;

	Prototype *prototype = unit->GetPrototype();
	Assert(prototype);

	static MapSize last_size;
	static float last_selection_height = 0.0f;
	static float x;
	static float w;
	static float sel_y;

	// update coordinates
	if (last_size != prototype->GetSize() || last_selection_height != prototype->GetSelectionHeight())
	{
		last_size = prototype->GetSize();
		last_selection_height = prototype->GetSelectionHeight();

		x = -(last_size.height * ISO_SCENE_H_COEF) * BAR_INDENT;
		w = (last_size.width + last_size.height) * ISO_SCENE_H_COEF * BAR_INDENT;

		sel_y = n_max(last_size.width, last_size.height) * ISO_SCENE_V_COEF + last_selection_height + BAR_SIZE;
	}

	// render life and hiding bar
	this->RenderStatusBar(x, sel_y, w, unit->GetLifePercent(), this->GetLifeColor(unit));
	if (unit->IsMy() && unit->GetPrototype()->CanHide())
		this->RenderStatusBar(x, sel_y - BAR_SIZE, w, unit->GetHidingPercent(), this->GetHidingColor(unit));
}


void RectSelRenderer::RenderStatusBar(float x, float y, float width, float percent, const vector3 &color)
{
	static vector2 coords[4];
	static float wp;
	static vector4 bg_color;

	wp = width * percent;

	// status bar
	if (percent > 0)
	{
		coords[0].set(x, y);
		coords[1].set(x, y + BAR_SIZE);
		coords[2].set(x + wp, y + BAR_SIZE);
		coords[3].set(x + wp, y);

		this->gfx_server->SetColor(color);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
	}

	// background
	if (percent < 1)
	{
		coords[0].set(x + wp, y);
		coords[1].set(x + wp, y + BAR_SIZE);
		coords[2].set(x + width, y + BAR_SIZE);
		coords[3].set(x + width, y);

		bg_color = color;
		bg_color.w = 0.4f;

		this->gfx_server->SetColor(bg_color);
		this->gfx_server->RenderPrimitive(GfxServer::PRIMITIVE_QUADS, coords, 4);
	}
}


// vim:ts=4:sw=4:
