#ifndef __buildingprototype_h__
#define __buildingprototype_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file buildingprototype.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/prototype.h"
#include "kernel/ref.h"


//=========================================================================
// BuildingPrototype
//=========================================================================

/**
	@class BuildingPrototype
	@ingroup Engine_Module
*/
    
class BuildingPrototype : public Prototype
{
//--- methods
public:
	BuildingPrototype(const char *id);
	virtual ~BuildingPrototype();

protected:
	// serializing
	virtual bool Serialize(Serializer &serializer);
	virtual bool Deserialize(Serializer &serializer, bool first);
};


#endif // __buildingprototype_h__

// vim:ts=4:sw=4:
