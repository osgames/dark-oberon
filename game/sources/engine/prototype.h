#ifndef __prototype_h__
#define __prototype_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file prototype.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/mapstructures.h"
#include "engine/unit.h"
#include "kernel/root.h"

class Map;


//=========================================================================
// Prototype
//=========================================================================

/**
	@class Prototype
	@ingroup Engine_Module
*/
    
class Prototype : public Root
{
//--- embeded
public:
	struct Model
	{
		string id;
		string file_name;
	};

	struct Terrain
	{
		bool can_stay;     ///< Whether unit can stay on this terrain.
		bool can_move;     ///< Whether unit can move on this terrain.
		float speed;       ///< Move speed. [mapel / second]
		float rotation;    ///< Rotation speed. [degrees / second]

		Terrain() : can_stay(false), can_move(false), speed(0), rotation(0) {}
	};

	typedef vector<Model> ModelsArray;

protected:
	struct Material
	{
		uint_t price;
		bool accept;

		Material() : price(0), accept(false) {}
	};

	struct Segment
	{
		bool shootable;    ///< Whether unit can shoot to this segment.

		Terrain *terrains; ///< Array of all terrains defined in scheme.

		Segment() : shootable(false), terrains(NULL) {}
	};

	typedef vector<string> HidingArray;
	typedef hash_map<Prototype *, bool> HidingCache;


//--- methods
public:
	Prototype(const char *id);
	virtual ~Prototype();

	const string &GetName() const;
	const ModelsArray &GetModels() const;
	ushort_t GetLife() const;
	mapel_t GetView() const;
	const MapSize &GetSize() const;
	short GetFood() const;
	short GetEnergy() const;
	byte_t GetMinEnergy() const;
	float GetSelectionHeight() const;
	Unit::Aggressivity GetAggressivity() const;
	byte_t GetHidingCapacity() const;

	bool CanMove() const;
	bool CanAttack() const;
	bool CanMine() const;
	bool CanRepair() const;
	bool CanBuild() const;
	bool CanHide() const;

	const Terrain *GetTerrain(MapEnums::Segment segment, byte_t terrain_id) const;

	// tools
	void CheckPosition(const Map *map,
		const MapPosition3D &position, MapEnums::Direction direction, 
		bool &is_moveable, bool &is_free);
	void GetSpeeds(const Map *map,
		const MapPosition3D &position, MapEnums::Segment next_segment, 
		float &speed, float &rotation);
	bool CanHide(Prototype *prototype) const;

protected:
	// serializing
	virtual bool Serialize(Serializer &serializer);
	virtual bool Deserialize(Serializer &serializer, bool first);

	bool ReadSize(Serializer &serializer);
	bool ReadMaterials(Serializer &serializer);
	bool ReadResources(Serializer &serializer);
	bool ReadModels(Serializer &serializer);
	bool ReadGraphics(Serializer &serializer);
	bool ReadSegments(Serializer &serializer);
	bool ReadTerrains(Serializer &serializer, Segment &segment);
	bool ReadHiding(Serializer &serializer);

private:
	// tools
	bool CheckPosition(const Map *map,
		const MapPosition3D &position,
		bool &is_moveable, bool &is_free);


//--- variables
protected:
	string name;                      ///< Full prototype name.

	Material *materials;              ///< Array of all materials defined in scheme.
	ModelsArray models;               ///< Array of all models defined in prototype.
	Segment segments[MapEnums::SEG_COUNT];    ///< Array of all available segments.

	ushort_t life;                    ///< Amount of maximal life. [virtual unit]
	mapel_t view;                     ///< Radius of unit view. [mapels]
	MapSize size;                     ///< Unit size. [mapels]
	short food;                       ///< Amount of food. Positive number means that unit will supply food, negative number means that unit will consume food. [virtual unit]
	short energy;                     ///< Amount of energy. Positive number means that unit will supply energy, negative number means that unit will consume energy. [virtual unit]
	byte_t min_energy;                ///< Minimal amout of energy the unit needs for work. [%]
	float selection_height;           ///< Height of selection box (for buildings) or height of life bar (for units). [mapels]
	Unit::Aggressivity aggressivity;  ///< Default aggressivity of the unit.
	HidingArray hiding;               ///< Array of prototype identifiers that can be hidden in this prototype.
	mutable HidingCache hiding_cache; ///< Cache of pointers to prototypes.
	byte_t hiding_capacity;           ///< Capacity for hided units.

	struct
	{
		bool can_move : 1;            ///< Unit can move.
		bool can_attack : 1;          ///< Unit can attack (has gun).
		bool can_mine : 1;            ///< Unit can mine some sources.
		bool can_repair : 1;          ///< Unit can repair some other units.
		bool can_build : 1;           ///< Unit can build some other units.
		bool can_hide : 1;            ///< Unit can hide some other units.
	};
};


//=========================================================================
// Methods
//=========================================================================

inline
const string &Prototype::GetName() const
{
	return this->name;
}


inline
const Prototype::ModelsArray &Prototype::GetModels() const
{
	return this->models;
}


inline
ushort_t Prototype::GetLife() const
{
	return this->life;
}


inline
mapel_t Prototype::GetView() const
{
	return this->view;
}


inline
const MapSize &Prototype::GetSize() const
{
	return this->size;
}


inline
short Prototype::GetFood() const
{
	return this->food;
}


inline
short Prototype::GetEnergy() const
{
	return this->energy;
}


inline
byte_t Prototype::GetMinEnergy() const
{
	return this->min_energy;
}


inline
float Prototype::GetSelectionHeight() const
{
	return this->selection_height;
}


inline
Unit::Aggressivity Prototype::GetAggressivity() const
{
	return this->aggressivity;
}


inline
byte_t Prototype::GetHidingCapacity() const
{
	return this->hiding_capacity;
}


inline
bool Prototype::CanMove() const
{
	return this->can_move;
}


inline
bool Prototype::CanAttack() const
{
	return this->can_attack;
}


inline
bool Prototype::CanMine() const
{
	return this->can_mine;
}


inline
bool Prototype::CanRepair() const
{
	return this->can_repair;
}


inline
bool Prototype::CanBuild() const
{
	return this->can_build;
}


inline
bool Prototype::CanHide() const
{
	return this->can_hide;
}


inline
const Prototype::Terrain *Prototype::GetTerrain(MapEnums::Segment segment, byte_t terrain_id) const
{
	Assert(terrain_id);
	Assert(segment < MapEnums::SEG_COUNT);
	return this->segments[segment].terrains ? this->segments[segment].terrains + terrain_id - 1 : NULL;
}


#endif // __prototype_h__

// vim:ts=4:sw=4:
