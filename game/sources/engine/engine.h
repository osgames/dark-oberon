#ifndef __engine_h__
#define __engine_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file engine.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "kernel/kernelserver.h"


//=========================================================================
// Engine
//=========================================================================

/**
	@class Engine
	@ingroup Engine_Module

	@brief Main class of engine.

	Registers all available modules, creates core servers and runs given game.
*/

class Engine
{
//--- variables
protected:
	bool opened;                     ///< Whether engine is opened.

	KernelServer *kernel_server;     ///< Shortcut to kernel server.

//--- methods
public:
	Engine();
	virtual ~Engine();

	// engine
	bool Open(int argc, char *argv[]);
	bool Run(const char *game_class, const string &title, void (*game_module) (void) = NULL);
	void Close();
};


#endif // __engine_h__

// vim:ts=4:sw=4:
