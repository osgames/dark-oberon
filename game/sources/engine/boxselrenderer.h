#ifndef __boxselrenderer_h__
#define __boxselrenderer_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file boxselrenderer.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/selectionrenderer.h"


//=========================================================================
// BoxSelRenderer
//=========================================================================

/**
	@class BoxSelRenderer
	@ingroup Engine_Module
*/
    
class BoxSelRenderer : public SelectionRenderer
{
//--- methods
public:
	BoxSelRenderer(const char *id);

	virtual void RenderBackground(Unit *unit);
	virtual void RenderForeground(Unit *unit);

protected:
	void RenderStatusBar(float x, float y, float height, float percent, const vector3 &color);
};


#endif // __boxselrenderer_h__

// vim:ts=4:sw=4:
