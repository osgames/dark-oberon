#ifndef __scheme_h__
#define __scheme_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scheme.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/preloaded.h"
#include "engine/mapstructures.h"
#include "kernel/mathlib.h"

class Model;


//=========================================================================
// Scheme
//=========================================================================

/**
	@class Scheme
	@ingroup Engine_Module
*/
    
class Scheme : public Preloaded
{
//--- embeded
public:
	struct Fragment
	{
		string id;
		string name;
		MapSize size;

		vector<string> models[MapEnums::SEG_COUNT];
		byte_t **surface;

		Fragment() : surface(NULL) {}
		~Fragment();
	};

	struct Material
	{
		byte_t num_id;
		string name;
		string image;
	};

private:
	typedef hash_map<string, Material *> MaterialsMap;
	typedef hash_map<string, Fragment *> FragmentsMap;
	typedef hash_map<string, byte_t> TerrainsMap;


//--- variables
protected:
	// scheme header
	string name;                  ///< Full scheme name.
	string author_name;
	string author_contact;

	// scheme body
	MaterialsMap materials;
	FragmentsMap fragments;
	TerrainsMap terrains;

	// resources
	vector<Model *> models;


//--- methods
public:
	Scheme(const char *id);
	virtual ~Scheme();

	Material *GetMaterial(const string &id) const;
	byte_t GetMaterialsCount() const;
	Fragment *GetFragment(const string &id) const;
	byte_t GetTerrain(const string &id) const;
	byte_t GetTerrainsCount() const;
	bool HasModels(const Fragment &fragment, MapEnums::Segment segment) const;
	const string &GetRandomModel(const Fragment &fragment, MapEnums::Segment segment) const;

protected:
	virtual void ClearHeader();
	virtual void ClearBody();

	virtual bool LoadResources();
	virtual void UnloadResources();

	// serializing
	virtual bool ReadHeader(Serializer &serializer);
	virtual bool ReadBody(Serializer &serializer);
	virtual bool WriteHeader(Serializer &serializer);
	virtual bool WriteBody(Serializer &serializer);

	bool ReadMaterials(Serializer &serializer);
	bool ReadTerrains(Serializer &serializer);
	bool ReadFragments(Serializer &serializer);
	bool ReadFragment(Serializer &serializer, Fragment &fragment);
	bool ReadModels(Serializer &serializer, Fragment &fragment);
	bool ReadSurface(Serializer &serializer, Fragment &fragment);
};


//=========================================================================
// Methods
//=========================================================================

inline
Scheme::Fragment::~Fragment()
{
	Delete2DArray<byte_t>(this->surface, size.width);
	this->surface = NULL;
}


inline
Scheme::Material *Scheme::GetMaterial(const string &id) const
{
	MaterialsMap::const_iterator it = this->materials.find(id);
	return it == this->materials.end() ? NULL : it->second;
}


inline
byte_t Scheme::GetMaterialsCount() const
{
	return (byte_t)this->materials.size();
}


inline
Scheme::Fragment *Scheme::GetFragment(const string &id) const
{
	FragmentsMap::const_iterator it = this->fragments.find(id);
	return it == this->fragments.end() ? NULL : it->second;
}


inline
byte_t Scheme::GetTerrain(const string &id) const
{
	TerrainsMap::const_iterator it = this->terrains.find(id);
	return it == this->terrains.end() ? 0 : it->second;
}


inline
byte_t Scheme::GetTerrainsCount() const
{
	return (byte_t)this->terrains.size();
}


inline
bool Scheme::HasModels(const Fragment &fragment, MapEnums::Segment segment) const
{
	Assert(segment < MapEnums::SEG_COUNT);
	return !fragment.models[segment].empty();
}


inline
const string &Scheme::GetRandomModel(const Fragment &fragment, MapEnums::Segment segment) const
{
	Assert(segment < MapEnums::SEG_COUNT);

	int size = (int)fragment.models[segment].size();
	Assert(size > 0);
	if (size == 1)
		return fragment.models[segment][0];
	else
		return fragment.models[segment][n_irand(size)];
}


#endif // __scheme_h__

// vim:ts=4:sw=4:
