#ifndef __unitmessages_h__
#define __unitmessages_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file unitmessages.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "engine/unit.h"
#include "engine/mapstructures.h"
#include "kernel/messages.h"


//=========================================================================
// StateEvent
//=========================================================================

/**
	@class StateEvent
	@ingroup Engine_Module
*/

class StateEvent : public Event<StateEvent>
{
//--- methods
public:
	StateEvent(uint_t sequence = 0);


//--- variables
public:
	Unit::State state;

	union
	{
		byte_t next_step_counter;        ///< Used with STATE_NEXT_STEP.
		MapEnums::Direction direction;   ///< Used with STATE_ROTATING_*.
		struct
		{
			MapPosition3D position;      ///< Used with STATE_MOVING.
		};
	};
};


//=========================================================================
// Methods
//=========================================================================

inline
StateEvent::StateEvent(uint_t sequence) :
	Event<StateEvent>(sequence),
	state(Unit::STATE_NONE)
{
	//
}


//=========================================================================
// ActionEvent
//=========================================================================

/**
	@class ActionEvent
	@ingroup Engine_Module
*/

class ActionEvent : public Event<ActionEvent>
{
//--- methods
public:
	ActionEvent();


//--- variables
public:
	ushort_t destination_id;   ///< Identifier of destination unit.
	Unit::Action action;       ///< Requested action.

	byte_t player_id;          ///< Identifier of interacted player.
	ushort_t unit_id;          ///< Identifier of interacted unit.
	MapPosition3D position;    ///< Interacted position.
};


//=========================================================================
// Methods
//=========================================================================

inline
ActionEvent::ActionEvent() :
	Event<ActionEvent>(),
	destination_id(0),
	action(Unit::ACTION_NONE),
	player_id(0),
	unit_id(0)
{
	//
}


//=========================================================================
// InputEvent
//=========================================================================

/**
	@class InputEvent
	@ingroup Engine_Module
*/

class InputEvent : public Event<InputEvent>
{
//--- methods
public:
	InputEvent();


//--- variables
public:
	ushort_t destination_id;   ///< Identifier of destination unit.
	Unit::Input input;         ///< Requested input.

	MapPosition3D position;    ///< Interacted position.
};


//=========================================================================
// Methods
//=========================================================================

inline
InputEvent::InputEvent() :
	Event<InputEvent>(),
	destination_id(0),
	input(Unit::INPUT_NONE)
{
	//
}


#endif // __unit_h__

// vim:ts=4:sw=4:
