#ifndef __selectionrenderer_h__
#define __selectionrenderer_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file selectionrenderer.h
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/system.h"
#include "kernel/root.h"
#include "kernel/ref.h"

class Unit;
class GfxServer;
class vector3;
class vector4;


//=========================================================================
// SelectionRenderer
//=========================================================================

/**
	@class SelectionRenderer
	@ingroup Engine_Module
*/
    
class SelectionRenderer : public Root
{
//--- variables
protected:
	Ref<GfxServer> gfx_server;

//--- methods
public:
	SelectionRenderer(const char *id);

	virtual void RenderBackground(Unit *unit);
	virtual void RenderForeground(Unit *unit);

	static bool StringToSelRenderer(const string &name, string &class_name);
	static bool SelRendererToString(const string &class_name, string &name);

	// tools
	static const vector4 &GetHoverColor(Unit *unit);
	static const vector3 &GetSelectionColor(Unit *unit);
	static const vector3 &GetLifeColor(Unit *unit);
	static const vector3 &GetHidingColor(Unit *unit);
};


//=========================================================================
// Methods
//=========================================================================

inline
bool SelectionRenderer::StringToSelRenderer(const string &name, string &class_name)
{
	if (name == "none")
		class_name.clear();
	else if (name == "rectangle")
		class_name = "RectSelRenderer";
	else if (name == "box")
		class_name = "BoxSelRenderer";
	else
		return false;

	return true;
}


inline
bool SelectionRenderer::SelRendererToString(const string &class_name, string &name)
{
	if (class_name.empty())
		name = "none";
	else if (class_name == "RectSelRenderer")
		name = "rectangle";
	else if (class_name == "BoxSelRenderer")
		name = "box";
	else
		return false;

	return true;
}


#endif // __selectionrenderer_h__

// vim:ts=4:sw=4:
