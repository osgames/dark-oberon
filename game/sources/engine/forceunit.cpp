//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file forceunit.cpp
	@ingroup Engine_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/forceunit.h"
#include "engine/prototype.h"
#include "engine/unitmessages.h"
#include "engine/game.h"
#include "engine/map.h"
#include "engine/selection.h"
#include "engine/level.h"
#include "engine/player.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "framework/remoteserver.h"
#include "framework/sceneobject.h"
#include "framework/gfxserver.h"

PrepareClass(ForceUnit, "Unit", s_NewForceUnit, s_InitForceUnit);


//=========================================================================
// Defines
//=========================================================================

#define UNIT_NEXT_EJECTING_WAITING  1.0     ///< How long will hided unit wait before next finding of free position around hider.


//=========================================================================
// ForceUnit
//=========================================================================

/**
	Constructor.
*/
ForceUnit::ForceUnit(const char *id) :
	Unit(id)
{
	this->type = TYPE_FORCE;

	for (byte_t i = 0; i < MODEL_COUNT; i++)
	{
		for (byte_t j = 0; j < MapEnums::DIRECTION_COUNT; j++)
			this->models[i][j] = 0;
	}
}


bool ForceUnit::SetModel(const string &model_id, byte_t num_id)
{
	// convert id
	ModelType model_type;
	MapEnums::Direction direction_type;
	string model_id2, direction_id;

	str_pos_t pos = model_id.find(':');
	if (pos == string::npos)
	{
		LogWarning2("Wrong format of model id '%s' in force unit prototype: %s", model_id.c_str(), this->prototype->GetID().c_str());
		return false;
	}

	model_id2.assign(model_id, 0, pos);
	direction_id.assign(model_id, pos + 1, model_id.size() - pos - 1);

	if (this->StringToModelType(model_id2, model_type) && MapEnums::StringToDirection(direction_id, direction_type))
		this->models[model_type][direction_type] = num_id;
	else
	{
		LogWarning2("Undefined model id '%s' in force unit prototype: %s", model_id.c_str(), this->prototype->GetID().c_str());
		return false;
	}

	return true;
}


bool ForceUnit::ActivateModel(ModelType model)
{
	Assert(model < MODEL_COUNT);
	Assert(this->direction < MapEnums::DIRECTION_COUNT);

	byte_t model_id = this->models[model][this->direction];
	return model_id ? this->scene_object->ActivateModel(model_id) : false;
}


//=========================================================================
// Actions
//=========================================================================

bool ForceUnit::StartHiding(Unit *unit)
{
	Assert(unit);
	
	// check if we can hide
	if (!unit->CanHide(this))
		return false;

	// find path to unit
	if (!this->StartMoving(unit))
		return false;

	// set new hider
	this->hider = unit;
	return true;
}


void ForceUnit::StopAction()
{
	// stop hiding
	this->hider = NULL;

	// stop other actions
	Unit::StopAction();
}


bool ForceUnit::CanHideToUnit(const Unit *unit, bool write_msg) const
{
	Assert(unit && unit != this);

	if (!unit->IsMy())
	{
		if (write_msg)
			this->Say(false, "Cannot hide into enemy unit.");
		return false;
	}

	if (unit->TestState(STATE_DYING) || unit->TestState(STATE_ZOMBIE) || unit->TestState(STATE_DELETE))
	{
		if (write_msg)
			this->Say(false, "Cannot hide into destroyed %s.", unit->GetPrototype()->GetName().c_str());
		return false;
	}

	if (unit->TestState(STATE_HIDING))
	{
		if (write_msg)
			this->Say(false, "Cannot hide into the hided %s.", unit->GetPrototype()->GetName().c_str());
		return false;
	}

	if (unit->TestState(STATE_IS_BEING_BUILT))
	{
		if (write_msg)
			this->Say(false, "Cannot hide into the %s while building.", unit->GetPrototype()->GetName().c_str());
		return false;
	}

	if (!unit->GetPrototype()->CanHide())
	{
		if (write_msg)
			this->Say(false, "%s does not accept any units.", unit->GetPrototype()->GetName().c_str());
		return false;
	}

	if (!unit->CanHide(this))
	{
		if (write_msg)
			this->Say(false, "Cannot hide into the %s.", unit->GetPrototype()->GetName().c_str());
		return false;
	}

	return true;
}


//=========================================================================
// Messages
//=========================================================================

void ForceUnit::OnMessage(ActionEvent *pevent)
{
	Assert(pevent && pevent->destination_id == this->remote_id);

	Player *player = pevent->player_id ? (Player *)RemoteServer::GetInstance()->GetClient(pevent->player_id) : NULL;
	Unit *unit = player && pevent->unit_id ? player->GetUnit(pevent->unit_id) : NULL;
	
	switch (pevent->action)
	{
	case ACTION_HIDE:
		Assert(unit);
		this->StartHiding(unit);
		break;

	default:
		Unit::OnMessage(pevent);
		break;
	}
}


void ForceUnit::ProcessEvent(StateEvent *pevent)
{
	Assert(pevent);

	// process event
	switch (pevent->state)
	{
	case STATE_STAYING:
		// unit has hider, it is trying to hide to him
		if (this->hider.IsValid())
		{
			// if unit can not hide into the hider, we can finish hiding directly
			if (!this->CanHideToUnit(this->hider, true))
			{
				this->hider = NULL;
				break;
			}

			// if unit stays next to hider, hide
			if (this->IsAroundUnit(this->hider))
			{
				vector2 unit_position;
				vector2 hider_position;
				bool rotating_left;

				this->GetCenterPosition(unit_position);
				this->hider->GetCenterPosition(hider_position);

				// ckeck if rotation is ok
				MapEnums::Direction new_direction = MapEnums::GetDirection(unit_position, hider_position);
				new_direction = MapEnums::GetNextDirection(this->direction, new_direction, rotating_left);

				// rotation is ok, unit can hide
				if (this->direction == new_direction)
				{
					pevent->state = STATE_NEXT_HIDING;
					this->SendMessage(pevent, this);
				}

				// unit must rotate to hider first
				else
				{
					pevent->state = rotating_left ? STATE_ROTATING_LEFT : STATE_ROTATING_RIGHT;
					pevent->direction = new_direction;
					this->SendMessage(pevent, this);
				}
			}

			// if unit does not wait for new path, start moving to hider. This can happen when
			// hider has moved while unit was rotating at the end position
			else if (!this->path_request_id)
			{
				this->StartMoving(this->hider, false);
				this->SendMessage(pevent, this);
			}
		}

		// just stop
		else
			Unit::ProcessEvent(pevent);
		break;

	case STATE_NEXT_HIDING:
		// if unit cannot hide into the hider, stop
		if (!this->hider.IsValid() || !this->CanHideToUnit(this->hider, true))
		{
			this->hider = NULL;
			pevent->state = STATE_STAYING;
			this->SendMessage(pevent, this);
			break;
		}

		// hide
		pevent->state = STATE_HIDING;
		this->SendMessage(pevent, this);
		break;

	case STATE_HIDING:
		// if hider is invalid, stop
		if (!this->hider.IsValid())
		{
			pevent->state = STATE_STAYING;
			this->SendMessage(pevent, this);
			break;
		}

		// unselect unit
		if (this->IsFlags(Unit::FLAG_SELECTED))
			Game::GetInstance()->GetSelection()->RemoveUnit(this);

		// hide into the hider
		Game::GetInstance()->GetActiveLevel()->RemoveUnitFromMap(this);
		this->hider->AddToHidingList(this);
		break;

	case STATE_NEXT_EJECTING:
		{
			Assert(this->hider.IsValid());

			// find free position around hider
			MapPosition3D free_position;
			MapEnums::Direction free_direction;
			if (!this->FindFreePositionAroundUnit(this->hider, free_position, free_direction))
			{
				// if not found, try it later
				this->SendMessage(pevent, this, UNIT_NEXT_EJECTING_WAITING);
				break;
			}

			// eject unit
			this->SetMapPosition(free_position);
			this->SetDirection(free_direction);
			Verify(Game::GetInstance()->GetActiveLevel()->AddUnitToMap(this));
			this->hider->RemoveFromHidingList(this);

			// if hider has meeting point, start moving to it
			if (this->hider->GetMeetingPoint(free_position))
				this->StartMoving(free_position);

			// stay
			this->hider = NULL;
			pevent->state = STATE_NEXT_STEP;
			this->SendMessage(pevent, this);
		}
		break;
	
	default:
		Unit::ProcessEvent(pevent);
		return;
	}
}


// vim:ts=4:sw=4:
