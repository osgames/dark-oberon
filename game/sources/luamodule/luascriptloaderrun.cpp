//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file luascriptloaderrun.cpp
	@ingroup Lua_Module

	@author Matthew T. Welker, Vadim Macagon, Peter Knut
	@date 2005 - 2006

	Command processing.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "luamodule/luascriptloader.h"


//=========================================================================
// LuaScriptLoader
//=========================================================================

/**
	@brief Executes the chunk at the top of the Lua stack.

	@param result   Will be filled with the result.
	@param err_func Stack index of error handler.
	@return @c True if successful.
*/
bool LuaScriptLoader::ExecuteLuaChunk(string &result, int err_func)
{
	AssertMsg(err_func > 0, "Error function stack index must be absolute");

	// call chunk main
	int status = lua_pcall(this->L, 0, LUA_MULTRET, err_func);
	if (status) {
		result = this->stack_trace;

		// clear stack
		lua_settop(this->L, 0);
		return false;
	}

	// clean stack
	result = "";
	this->StackToString(this->L, 0, result);

	return true;
}


/**
	@brief Empties the Lua stack and returns a string representation 
		   of the contents.

	Only items between the bottom index and the stack top
	(excluding the bottom index) will be crammed into the 
	string, passing 0 for the bottom will dump all the 
	items in the stack.
       
	@param L Lua context.
	@param bottom Absolute index of the bottom stack item.
	@param result String representation of the contents.
*/
void LuaScriptLoader::StackToString(lua_State *L, int bottom, string &result)
{
	Assert(L);

	// go through whole stack
	while (bottom < lua_gettop(L)) {
		switch (lua_type(L, -1)) {
			case LUA_TBOOLEAN:
			{
				if (lua_toboolean(L, -1)) 
					result.append("true");
				else 
					result.append("false");
				break;  
			}

			case LUA_TNUMBER:
			case LUA_TSTRING:
			{
				result.append(lua_tostring(L, -1));
				break;
			}

			case LUA_TUSERDATA:
			case LUA_TLIGHTUSERDATA:
			{
				result.append("<userdata>");
				break;
			}

			case LUA_TNIL:
			{
				result.append("<nil>");
				break;
			}

			case LUA_TTABLE:
			{
				// check if it's a thunk
				lua_pushstring(L, "_");
				lua_rawget(L, -2);

				// assume it's a thunk
				if (lua_isuserdata(L, -1))
					result.append("<thunk>");

				// it's a table
				else {
					result.append("{ ");
					lua_pushnil(L);
					lua_gettable(L, -2);
					bool first_item = true;

					while (lua_next(L, -2) != 0) {
						if (!first_item)
							result.append(", ");
						else
							first_item = false;

						LuaScriptLoader::StackToString(L, lua_gettop(L) - 1, result);
					}

					lua_pop(L, 1);
					result.append(" }");
				}
				break;
			}
			default:
				result.append("???");
				break;  
		}
		lua_pop(L, -1);
	}
}


/**
	@brief Executes a chunk of Lua code and provide a string representation of the result.

	@param lua_code A null-terminated string of Lua code.
	@param result   The result (if any) of the execution of the Lua code.

	@return         @c True if successful.
*/
bool LuaScriptLoader::RunCode(const char *lua_code, string &result)
{
	Assert(lua_code);

	// push the error handler on stack
	lua_pushstring(this->L, "_STACKDUMP");
	lua_gettable(this->L, LUA_GLOBALSINDEX);
	AssertMsg(lua_isfunction(this->L, -1), "Error handler not registered");
	int err_func = lua_gettop(this->L);
	
	// load chunk
	int status = luaL_loadbuffer(this->L, lua_code, strlen(lua_code), lua_code);
	if (status) {
		// pop error message from the stack
		result = "";
		this->StackToString(this->L, lua_gettop(this->L) - 1, result);
		return false;
	}

	// execute chunk
	return this->ExecuteLuaChunk(result, err_func);
}


/**
	Runs a LUA script function with the specified name without any args.

	param  func_name   Function name.
	param  result      Will be filled with the result.
	@return            @c True if successful.
*/
bool LuaScriptLoader::RunFunction(const char *func_name, string &result)
{
	Assert(func_name);

	// make a code from func name
	string lua_code = func_name;
	lua_code.append("()");

	// run code
	return this->RunCode(lua_code.c_str(), result);
}

// vim:ts=4:sw=4:
