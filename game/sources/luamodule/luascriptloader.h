#ifndef __luascriptloader_h__
#define __luascriptloader_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file luascriptloader.h
	@ingroup Lua_Module

	@author Matthew T. Welker, Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/kernelserver.h"
#include "kernel/scriptloader.h"
#include "kernel/server.h"

#include <lua/lua.hpp>


//=========================================================================
// LuaScriptLoader
//=========================================================================

/** 
	@class LuaScriptLoader
	@ingroup Lua_Module

	@brief Lua 5 wrapper.

	Implements an ScriptLoader that runs Lua 5, extended
	by default with a few specific commands.
*/

class LuaScriptLoader: public ScriptLoader, public Server<LuaScriptLoader> {
//--- variables
public:
    string class_cache_name;
    string thunk_store_name;

private:
	lua_State *L;              ///< Lua context.
    bool echo;
    bool selgrab;
    string stack_trace;        ///< Contains stack trace after GenerateStackTrace is called.

//--- methods
public:
    LuaScriptLoader(const char *id);
    virtual ~LuaScriptLoader();
 
    virtual bool RunCode(const char *lua_code, string &result);
    virtual bool RunFunction(const char *func_name, string &result);

    const string &GenerateStackTrace();
    virtual lua_State *GetContext();
    
    // manipulate _nebthunks table
    static void RemoveThunk(lua_State *, void *);
    static void AddThunk(lua_State *, void *);
    static void FindThunk(lua_State *, void *);
    
    static void AddClassToCache(lua_State *, Class *);
    static bool ThunkNebObject(lua_State *, Root *);
    static Root *UnpackThunkRoot(lua_State *, int);
    
	// amnipulate arguments
    static void InArgsToStack(lua_State *, Cmd *);
    static void OutArgsToStack(lua_State *, Cmd *, bool);
    static bool StackToInArgs(lua_State *, Cmd *);
    static bool StackToOutArgs(lua_State *, Cmd *);
    static void ListArgToTable(lua_State *, Arg *, bool);

private:
    static void ArgToStack(lua_State *, Arg *);
    static bool StackToArg(lua_State *, Arg *, int index);
    static void StackToString(lua_State *L, int bottom, string &result);

	bool ExecuteLuaChunk(string &result, int err_func);
    void reg_globalfunc(lua_CFunction func, const char *name);
};


#endif // __luaserver_h__

// vim:ts=4:sw=4:
