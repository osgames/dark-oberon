//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file luacmdproto.cpp
	@ingroup Lua_Module

	@author Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "luamodule/luacmdproto.h"
#include "luamodule/luascriptloader.h"


//=========================================================================
// LuaCmdProto
//=========================================================================

/**
	Constructor.

	@param protoDef [in] Blue print string.
*/
LuaCmdProto::LuaCmdProto(const char* protoDef) :
	CmdProto(protoDef, 0)
{
	//
}


/**
	Copy constructor.
*/
LuaCmdProto::LuaCmdProto(const LuaCmdProto& rhs) :
	CmdProto(rhs)
{
	//
}


/**
*/
bool LuaCmdProto::Dispatch(void* obj, Cmd* cmd)
{
	lua_State* L = LuaScriptLoader::GetInstance()->GetContext();

	int top = lua_gettop(L);
	    
	// find the thunk that corresponds to the object
	// use the object pointer as a key into thunks table
	lua_pushstring(L, LuaScriptLoader::GetInstance()->thunk_store_name.c_str());
	lua_rawget(L, LUA_GLOBALSINDEX);
	AssertMsg(0 == lua_isnil(L, -1), "_nebthunks table not found!");
	lua_pushlightuserdata(L, obj);
	lua_gettable(L, -2);

	if (0 == lua_istable(L, -1)) { // thunk not found?
		//n_message("No thunk found for %s\n", 
		//          ((Root*)obj)->GetFullName(buf, sizeof(buf)));
		// bail out - no script-side script commands were defined
		return false;
	}
	    
	// at this point the correct thunk is on top of the stack
	// find the function of interest
	lua_pushstring(L, cmd->GetProto()->GetName().c_str());
	lua_rawget(L, -2);

	if (0 == lua_isfunction(L, -1)) // function doesn't exist?
		return false;

	// shift thunk to be the first arg to the function
	lua_pushvalue(L, -2);
	lua_remove(L, -3);
	// put cmd in-args on the stack
	LuaScriptLoader::InArgsToStack(L, cmd);

	// call the function
	if (0 != lua_pcall(L, cmd->GetInArgsCount() + 1, cmd->GetOutArgsCount(), 0)) {
		// error occured, display it
		ErrorMsg3(
			"LUA encountered an error (%s) while trying to call %s on %s",
			lua_tostring(L, -1),
			cmd->GetProto()->GetName(),
			((Root*)obj)->GetFullPath().c_str()
		);
	}

	// put out-args into the cmd
	LuaScriptLoader::StackToOutArgs(L, cmd);
	lua_settop(L, top);

	return true;
}

// vim:ts=4:sw=4:
