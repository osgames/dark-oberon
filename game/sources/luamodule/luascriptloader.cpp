//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file luascriptloader.cpp
	@ingroup Lua_Module

	@author Matthew T. Welker, Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "kernel/ref.h"
#include "luamodule/luascriptloader.h"

PrepareClass(LuaScriptLoader, "ScriptLoader", s_NewLuaScriptLoader, s_InitLuaScriptLoader);


//=========================================================================
// External declaration of commands
//=========================================================================

extern int luacmd_StackDump(lua_State*);
extern int luacmd_Error(lua_State*);
extern int luacmd_Panic(lua_State*);
extern int luacmd_New(lua_State*);
extern int luacmd_NewThunk(lua_State*);
extern int luacmd_Delete(lua_State*);
extern int luacmd_PinThunk(lua_State*);
extern int luacmd_UnpinThunk(lua_State*);
extern int luacmd_Sel(lua_State*);
extern int luacmd_Psel(lua_State*);
extern int luacmd_Exit(lua_State*);
extern int luacmd_LogTree(lua_State*);
extern int luacmd_LogInfo(lua_State*);
extern int luacmd_LogWarning(lua_State*);
extern int luacmd_LogError(lua_State*);
extern int luacmd_LogCritical(lua_State*);
extern int luacmd_LogDebug(lua_State*);
extern int luacmd_Dir(lua_State*);
extern int luacmd_PushCwd(lua_State*);
extern int luacmd_PopCwd(lua_State*);
extern int luacmd_CmdDispatch(lua_State*);
extern int luacmd_Call(lua_State*);
extern int luacmd_ConCall(lua_State*);
extern int luacmd_Lookup(lua_State*);
extern int luacmd_Mangle(lua_State*);
extern int luacmd_Exists(lua_State*);
extern int luacmd_BeginCmds(lua_State*);
extern int luacmd_AddCmd(lua_State*);
extern int luacmd_EndCmds(lua_State*);
extern int luacmd_IsZombieThunk(lua_State*);
extern int luacmd_DeleteNRef(lua_State*);


//=========================================================================
// LuaScriptLoader
//=========================================================================

/**
	Constructor. Initializes Lua 5 interpreter.
*/
LuaScriptLoader::LuaScriptLoader(const char *id) :
	ScriptLoader(id),
	Server<LuaScriptLoader>(),
	echo(true),
	selgrab(false)
{
	Assert(FileServer::GetInstance());

	// get a default Lua interpreter up and running then
	this->L = luaL_newstate();

	if (!this->L) 
	  ErrorMsg("Could not create the Lua 5 interpreter");

	// set up a panic handler
	lua_atpanic(this->L, luacmd_Panic);

	// load all standard libraries
	luaL_openlibs(this->L);
	lua_settop(this->L, 0);  // discard any results

	// get a global table of Neb funcs up
	reg_globalfunc(luacmd_StackDump,        "_STACKDUMP");
	reg_globalfunc(luacmd_Error,            "error");
	reg_globalfunc(luacmd_New,              "new");
	reg_globalfunc(luacmd_NewThunk,         "newthunk");
	reg_globalfunc(luacmd_Delete,           "delete");
	reg_globalfunc(luacmd_PinThunk,         "pin");
	reg_globalfunc(luacmd_UnpinThunk,       "unpin");
	reg_globalfunc(luacmd_Sel,              "sel");
	reg_globalfunc(luacmd_Psel,             "psel");
	reg_globalfunc(luacmd_Exit,             "exit");
	reg_globalfunc(luacmd_LogTree,          "logtree");
	reg_globalfunc(luacmd_LogInfo,          "loginfo");
	reg_globalfunc(luacmd_LogWarning,       "logwarning");
	reg_globalfunc(luacmd_LogError,         "logerror");
	reg_globalfunc(luacmd_LogCritical,      "logcritical");
	reg_globalfunc(luacmd_LogDebug,         "logdebug");
	reg_globalfunc(luacmd_Dir,              "ls");
	reg_globalfunc(luacmd_PushCwd,          "pushcwd");
	reg_globalfunc(luacmd_PopCwd,           "popcwd");
	reg_globalfunc(luacmd_Call,             "call");
	reg_globalfunc(luacmd_ConCall,          "concall");
	reg_globalfunc(luacmd_Lookup,           "lookup");
	reg_globalfunc(luacmd_Mangle,           "mangle");
	reg_globalfunc(luacmd_Exists,           "exists");
	reg_globalfunc(luacmd_BeginCmds,        "begincmds");
	reg_globalfunc(luacmd_AddCmd,           "addcmd");
	reg_globalfunc(luacmd_EndCmds,          "endcmds");
	reg_globalfunc(luacmd_IsZombieThunk,    "iszombie");
	reg_globalfunc(luacmd_DeleteNRef,       "_delnref");

	// create the class cache
	this->class_cache_name = "nebclasses";
	lua_pushstring(this->L, this->class_cache_name.c_str());
	lua_newtable(this->L);
	lua_settable(this->L, LUA_GLOBALSINDEX);

	// create the thunk store (pinned thunks are stored here)
	this->thunk_store_name = "_nebthunks";
	lua_pushstring(this->L, this->thunk_store_name.c_str());
	lua_newtable(this->L);
	lua_settable(this->L, LUA_GLOBALSINDEX);

	// create the Ref metatable
	luaL_dostring(this->L, "_nrefmetatable = { __gc = function(nref) _delnref(nref) end }");

	// clear stack
	lua_settop(this->L, 0);
}


/**
	Destructor.
*/
LuaScriptLoader::~LuaScriptLoader()
{
    lua_close(this->L);
    this->L = 0;
}


/**
	@brief Generates a stack trace.
	@return A pointer to the string containing the stack trace.

	@warning The pointer points to an internal buffer that is likely to change,
			 therefore you should copy the string before any further Lua 
			 server methods are called.
*/
const string &LuaScriptLoader::GenerateStackTrace()
{
	Assert(this->L);
	AssertMsg(lua_gettop(this->L) == 1, "Only error message should be on stack");

	// add header to error text
	this->stack_trace = "LuaScriptLoader encountered a problem...\n";
	this->stack_trace.append(lua_tostring(this->L, -1));
	this->stack_trace.append("\n\n-- Stack Trace --\n");

	lua_Debug debug_info;
	int level = 0;
	char buffer[1024];
	buffer[0] = 0;
	const char *name_what = 0;
	const char *func_name = 0;

	// add whole stack to error text
	while (lua_getstack(this->L, level, &debug_info)) {

		if (lua_getinfo(this->L, "nSl", &debug_info)) {
			name_what = debug_info.namewhat[0] ? debug_info.namewhat : "???";			
			func_name = debug_info.name ? debug_info.name : "???";
                
			_snprintf(buffer, sizeof(buffer),
					 "%s - #%d: %s (%s/%s)\n",
					 debug_info.short_src,
					 debug_info.currentline,
					 func_name,
					 name_what,
					 debug_info.what);

			buffer[sizeof(buffer)-1] = 0; // null terminate in case snprintf doesn't
			this->stack_trace.append(buffer);
		}
		else
			this->stack_trace.append("Failed to generate stack trace.\n");

		level++;
	}

	// returns error text
	return this->stack_trace;
}


/**
	Simple accessor to allow users to get at the Lua context
	if needed. Care is needed here not to close the context.

	@return Lua context.
*/
lua_State *LuaScriptLoader::GetContext()
{
	return this->L;
}


/**
	Add a class to the class cache.

	@warning The global class cache table is expected to be on top of
	the stack when this method is called.

	@param L   Lua context.
	@param clazz Pointer to Class object.
*/
void LuaScriptLoader::AddClassToCache(lua_State *L, Class *clazz)
{
	Assert(L);
	Assert(clazz);

	HashList* protoList = 0;
	CmdProto* cmdProto;

	// Create the table that will store cmd protos for this class
	// each element will have a key that corresponds to the cmd
	// name and a value which corresponds to a luacmd_CmdDispatch
	// closure that contains the CmdProto*. In short when you
	// call that value the command is going to be called.
	lua_pushstring(L, clazz->GetName().c_str());
	lua_newtable(L);
  
	do {
		protoList = clazz->GetCmdList();
		if (protoList) {
			cmdProto = (CmdProto*)protoList->GetFront();

			for (; cmdProto; cmdProto = (CmdProto*)cmdProto->GetNext()) {
				lua_pushstring(L, cmdProto->GetName().c_str());
				lua_pushlightuserdata(L, cmdProto);
				lua_pushcclosure(L, luacmd_CmdDispatch, 1);
				lua_settable(L, -3);
			}

		}
	} while ((clazz = clazz->GetSuperClass()));
    
	// store table in the global class cache
	lua_settable(L, -3);
}


/**
	Builds a Lua table for a more natural syntax for getting 
	at the NOH and calling cmds.

	@param L   Lua context.
	@param root Pointer to Root object.
	@return @c True if successful.
*/
bool LuaScriptLoader::ThunkNebObject(lua_State *L, Root *root)
{
	Assert(L);
	Assert(root);
        
    // create the thunk table
    lua_newtable(L); // 1
    lua_pushstring(L, "_"); // 2

    // create and push heavy user data on stack
    Ref<Root> *ref_Root = NEW Ref<Root>(root);
	*(void **)(lua_newuserdata(L, sizeof(void *))) = ref_Root;

    // give the Ref userdata the proper metatable
    lua_pushstring(L, "_nrefmetatable"); // 4
    lua_gettable(L, LUA_GLOBALSINDEX); // 4
    lua_setmetatable(L, -2); // 4
    lua_settable(L, -3); // add ('_', Ref userdata) to 1
    lua_pushstring(L, "_class"); // 2

    // find the corresponding class table
    lua_pushstring(L, LuaScriptLoader::GetInstance()->class_cache_name.c_str()); // 3
    lua_gettable(L, LUA_GLOBALSINDEX); // 3 (nebula classes table)
    AssertMsg(lua_istable(L, -1), "nebula classes table not found");

    const char *class_name = root->GetClass()->GetName().c_str();
    lua_pushstring(L, class_name); // 4
    lua_gettable(L, -2); // 4 (class table)

    if (!lua_istable(L, -1)) {
        lua_pop(L, 1); // pop the nil off the stack

        // class table isnot created yet, do it now
        LuaScriptLoader::AddClassToCache(L, root->GetClass());
        lua_pushstring(L, class_name);
        lua_gettable(L, -2);
    }

    lua_remove(L, -2); // remove 3 (nebula classes table)
    lua_settable(L, -3); // add ('_class', class table ref) to thunk

    // give the thunk the proper metatable
    lua_pushstring(L, "_nebthunker"); // 2
    lua_gettable(L, LUA_GLOBALSINDEX); // 2
    lua_setmetatable(L, -2);

    // leave the thunk on the stack and return
    return true;
}


/**
	Returns a thunk (in the _nebthunks table) that is associated
	with the specified key. Or nil a thunk for the specified key
	doesn't exist yet.

	@param L   Lua context.
	@param key The key with which the thunk should be associated.
*/
void LuaScriptLoader::FindThunk(lua_State *L, void *key)
{
	Assert(L);
	Assert(key);

	// find thunk
	lua_pushstring(L, LuaScriptLoader::GetInstance()->thunk_store_name.c_str());
	lua_rawget(L, LUA_GLOBALSINDEX); // table
	lua_pushlightuserdata(L, key); // key
	lua_rawget(L, -2);
	lua_remove(L, -2); // remove table
}


/**
	Adds a thunk (assumed to be at the top of the lua stack) to
	the _nebthunks table. The thunk will remain on the stack.

	@param L   Lua context.
	@param key The key with which the thunk should be associated.
*/
void LuaScriptLoader::AddThunk(lua_State *L, void *key)
{
	Assert(L);
	Assert(key);

	// add thunk
	lua_pushstring(L, LuaScriptLoader::GetInstance()->thunk_store_name.c_str());
	lua_rawget(L, LUA_GLOBALSINDEX); // table
	lua_pushlightuserdata(L, key); // key
	lua_pushvalue(L, -3); // value
	lua_rawset(L, -3);
	lua_remove(L, -1); // remove table
}


/**
	Removes a thunk from the _nebthunks table.

	@param L   Lua context.
	@param key The key that is associated with the thunk.
*/
void LuaScriptLoader::RemoveThunk(lua_State *L, void *key)
{
	Assert(L);
	Assert(key);

	// remove thunk
	lua_pushstring(L, LuaScriptLoader::GetInstance()->thunk_store_name.c_str());
	lua_rawget(L, LUA_GLOBALSINDEX);
	lua_pushlightuserdata(L, key);
	lua_pushnil(L);
	lua_rawset(L, -3);
	lua_remove(L, -1); // remove table
}


/**
	@brief Simply unwraps a thunk for a passable Root* value.

	@param L        Lua context.
	@param table_idx The absolute stack index of the table.
	@return         Unpacked pointer to Root object.
*/
Root* LuaScriptLoader::UnpackThunkRoot(lua_State *L, int table_idx)
{
	Assert(L);
	Assert(table_idx > 0);

	// push the key on and see what we get back
	// make sure this doesn't chump with the metatables
	Root *root;

	lua_pushliteral(L, "_");
	lua_rawget(L, table_idx);

	if (lua_isuserdata(L, -1)) {
		Ref<Root>* ref = *((Ref<Root>**)lua_touserdata(L, -1));
		if (ref)
			root = ref->Get();
		else
			root = 0;
    }
	else
		root = 0;

	lua_pop(L, 1);
	return root;
}


/**
	@brief Pull a cmd's in-args from the stack and pack them into 
	the cmd.

	@param L Lua state object.
	@param cmd Pointer to command.
	@return @c True if successful.
*/
bool LuaScriptLoader::StackToInArgs(lua_State *L, Cmd *cmd)
{
	Assert(L);
	Assert(cmd);

	cmd->Rewind();
	Arg *arg;
	int args_count = cmd->GetInArgsCount();
	if (args_count < 1)
		return true;

	// compute the stack index of the first cmd arg
	int top = lua_gettop(L);
	int i = top - args_count + 1;

	// loop through and pack it all in
	for (; i <= top; i++) {
		// get the arg prepped
		arg = cmd->In();
		if (!LuaScriptLoader::StackToArg(L, arg, i))
			return false;
	}

	return true;
}


/**
	@brief Create a new table on the stack and populate it with the
	members of the list Arg.

	@param L Lua state object.
	@param arg Pointer to argument.
	@param print Print stack into log file.
*/
void LuaScriptLoader::ListArgToTable(lua_State *L, Arg *arg, bool print)
{
	Assert(L);
	Assert(arg);

	Arg* listArg;
	int listLen = arg->GetA(listArg);

	lua_newtable(L); // create a table

	if (print)
		LogInfo("{");

	for (int j = 0; j < listLen; j++)
	{
		// farq - these are built as a lua table
		lua_pushnumber(L, j);
		switch (listArg->GetType()) {
			case Arg::AT_INT:
			{
				lua_pushnumber(L, listArg->GetI());
				if (print) {
					if (j > 0)
						LogInfo1(", %d", listArg->GetI());
					else
						LogInfo1("%d", listArg->GetI());
				}
				break;
			}
			case Arg::AT_FLOAT:
			{
				lua_pushnumber(L, listArg->GetF());
				if (print) {
					if (j > 0)
						LogInfo1(", %f", listArg->GetF());
					else
						LogInfo1("%f", listArg->GetF());
				}
				break;
			}
			case Arg::AT_STRING:
			{
				lua_pushstring(L, listArg->GetS());
				if (print) {
					if (j > 0)
						LogInfo1(", %s", listArg->GetS());
					else
						LogInfo1("%s", listArg->GetS());
				}
				break;
			}
			case Arg::AT_BOOL:
			{
				lua_pushboolean(L, listArg->GetB());
				if (print) {
					if (j > 0)
						LogInfo1(", %s", listArg->GetB() ? "true" : "false");
					else
						LogInfo1("%s", listArg->GetB() ? "true" : "false");
				}
				break;
			}
			case Arg::AT_OBJECT:
			{
				Root* o = (Root*)listArg->GetO();
				if (o) {
					LuaScriptLoader::GetInstance()->ThunkNebObject(L, o);
					if (print) {
						if (j > 0)
							LogInfo1(", %s", o->GetFullPath().c_str());
						else
							LogInfo1("%s", o->GetFullPath().c_str());
					}
				}
				else
				{
					lua_pushnil(L);
					if (print) {
						if (j > 0)
							LogInfo(", nil");
						else
							LogInfo("nil");
					}
				}
				break;
			}
			case Arg::AT_ARRAY:
			{
				if (print && (j > 0))
					LogInfo(", ");
				LuaScriptLoader::ListArgToTable(L, listArg, print);
			}
			default:
				lua_pushnil(L);
		}
		lua_settable(L, -3);
		listArg++;
	}

	if (print)
		LogInfo("}");
}


/**
	@brief Pushes the output args of a cmd onto the LUA stack, and 
	optionally prints the output.

	@param L Lua state object.
	@param cmd Pointer to command.
	@param print Print stack into log file.
*/
void LuaScriptLoader::OutArgsToStack(lua_State *L, Cmd *cmd, bool print)
{
	Assert(L);
	Assert(cmd);

	cmd->Rewind();
	Arg *arg;
	int n = cmd->GetOutArgsCount();

	// loop through and pack it all in
	for (int i = 0; i < n; i++)
	{
		// get the arg prepped
		arg = cmd->Out();
		switch (arg->GetType()) {
			case Arg::AT_INT:
			{
				lua_pushnumber(L, arg->GetI());
				if (print) {
					if (i > 0)
						LogInfo1(" %d", arg->GetI());
					else
						LogInfo1("%d", arg->GetI());
				}
				break;
			}
			case Arg::AT_FLOAT:
			{
				lua_pushnumber(L, arg->GetF());
				if (print) {
					if (i > 0)
						LogInfo1(" %f", arg->GetF());
					else
						LogInfo1("%f", arg->GetF());
				}
				break;
			}
			case Arg::AT_STRING:
			{
				lua_pushstring(L, arg->GetS());
				if (print) {
					if (i > 0)
						LogInfo1(" %s", arg->GetS());
					else
						LogInfo1("%s", arg->GetS());
				}
				break;
			}
			case Arg::AT_BOOL:
			{
				lua_pushboolean(L, arg->GetB());
				if (print) {
					if (i > 0)
						LogInfo1(" %s", arg->GetB() ? "true" : "false");
					else
						LogInfo1("%s", arg->GetB() ? "true" : "false");
				}
				break;
			}
			case Arg::AT_OBJECT:
			{
				Root* o = (Root*)arg->GetO();
				if (o) {
					LuaScriptLoader::GetInstance()->ThunkNebObject(L, o);
					if (print) {
						if (i > 0)
							LogInfo1(" %s", o->GetFullPath().c_str());
						else
							LogInfo1("%s", o->GetFullPath().c_str());
					}
				}
				else {
					lua_pushnil(L);
					if (print) {
						if (i > 0)
							LogInfo(" nil");
						else
							LogInfo("nil");
					}
				}
				break;
			}
			case Arg::AT_VOID:
			{
				lua_pushnil(L);
				break;
			}
			case Arg::AT_ARRAY:
			{
				LuaScriptLoader::ListArgToTable(L, arg, print);
				break;
			}
		}
	}

	if (print && (n > 0))
		LogInfo("\n");
}


/**
	@brief Take a cmd's in-args and put them on the LUA stack.

	@param L Lua state object.
	@param cmd Pointer to command.
*/
void LuaScriptLoader::InArgsToStack(lua_State *L, Cmd *cmd)
{
	Assert(L);
	Assert(cmd);

	cmd->Rewind();
	for (int i = 0; i < cmd->GetInArgsCount(); i++)
		LuaScriptLoader::ArgToStack(L, cmd->In()); 
}


/**
	@brief Take a cmd's out-args from the stack and pack them into
	the cmd.

	@param L Lua state object.
	@param cmd Pointer to command.
*/
bool LuaScriptLoader::StackToOutArgs(lua_State *L, Cmd *cmd)
{
	Assert(L);
	Assert(cmd);

	cmd->Rewind();
	int args_count = cmd->GetOutArgsCount();
	if (args_count < 1)
		return true;

	// compute the stack index of the first output value
	int top = lua_gettop(L);
	int i = top - args_count + 1;

	// loop through and pack it all in
	for (; i <= top; i++) {
		if (!LuaScriptLoader::StackToArg(L, cmd->Out(), i))
			return false;
	}

	return true;
}


/**
	@brief Convert an Arg to a Lua compatible value and push it on the 
	LUA stack.

	@param L Lua state object.
	@param arg Pointer to argument.
*/
void LuaScriptLoader::ArgToStack(lua_State *L, Arg *arg)
{
	Assert(L);
	Assert(arg);

	switch (arg->GetType()) {
		case Arg::AT_VOID:
			lua_pushnil(L);
			break;

		case Arg::AT_INT:
			lua_pushnumber(L, arg->GetI());
			break;

		case Arg::AT_FLOAT:
			lua_pushnumber(L, arg->GetF());
			break;

		case Arg::AT_STRING:
			lua_pushstring(L, arg->GetS());
			break;
    
		case Arg::AT_BOOL:
			lua_pushboolean(L, arg->GetB());
			break;
  
		case Arg::AT_OBJECT:
			LuaScriptLoader::ThunkNebObject(L, (Root*)arg->GetO());
			break;
  
		case Arg::AT_ARRAY:
			LuaScriptLoader::ListArgToTable(L, arg, false);
			break;
	}
}


/**
	@brief Convert a value from the LUA stack to an Arg of the 
	specified type.

	@param L Lua state object.
	@param arg Pointer to argument.
	@param index The absolute stack index of the value to convert.

	@return True if arg was successfuly retrieved and converted.
*/
bool LuaScriptLoader::StackToArg(lua_State *L, Arg *arg, int index)
{
	Assert(L);
	Assert(arg);
	Assert(index > 0);

	switch (arg->GetType()) {
		case Arg::AT_INT:
		{
			if (!lua_isnumber(L, index)) 
				return false;
			arg->SetI(static_cast<int>(lua_tonumber(L, index)));
			break;
		}
		case Arg::AT_FLOAT:
		{
			if (!lua_isnumber(L, index)) 
				return false;
			arg->SetF(static_cast<float>(lua_tonumber(L, index)));
			break;
		}
		case Arg::AT_STRING:
		{
			if (!lua_isstring(L, index)) 
				return false;
			arg->SetS(lua_tostring(L, index));
			break;
		}
		case Arg::AT_BOOL:
		{
			if (!lua_isboolean(L, index)) 
				return false;
			arg->SetB(lua_toboolean(L, index) == 1);
			break;
		}
		case Arg::AT_OBJECT:
		{
			if (lua_isuserdata(L, index))
				arg->SetO(lua_touserdata(L, index));
			else if (lua_istable(L, index)) {
				Root *root = LuaScriptLoader::UnpackThunkRoot(L, index);
				if (!root) 
					return false;
				arg->SetO(root);
			}
			else
				return false;
			break;
		}
		case Arg::AT_ARRAY:
		{
			LogError("List Args can't be used as input args");
			return false;
			break;
		}
		default:
			return false;
	}
	return true;
}


/**
	Register global Lua function.

	@param func Lua function.
	@param name Name of the function.
*/
void LuaScriptLoader::reg_globalfunc(lua_CFunction func, const char *name)
{
	Assert(name);

	// register function
	lua_pushstring(this->L, name);
	lua_pushlightuserdata(L, this);
	lua_pushcclosure(L, func, 1);
	lua_settable(L, LUA_GLOBALSINDEX);
	lua_settop(L, 0);
}


// vim:ts=4:sw=4:
