#ifndef __LUACMDPROTO_H__
#define __LUACMDPROTO_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file luacmdproto.h
	@ingroup Lua_Module

	@author Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/cmdproto.h"


//=========================================================================
// LuaCmdProto
//=========================================================================

/**
	@class LuaCmdProto
	@ingroup Lua_Module

	@brief A factory for Cmd objects that correspond to Lua implemented
	script commands.
*/

class LuaCmdProto : public CmdProto	{
//-- methods
public:
	LuaCmdProto(const char* protoDef);
	LuaCmdProto(const LuaCmdProto& rhs);

	bool Dispatch(void *, Cmd *);
};


#endif  // __LUACMDPROTO_H__

// vim:ts=4:sw=4:
