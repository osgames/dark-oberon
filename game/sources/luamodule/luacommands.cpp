//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file luacommands.cpp
	@ingroup Lua_Module

 	@author Matthew T. Welker, Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/logserver.h"
#include "kernel/kernelserver.h"
#include "kernel/fileserver.h"
#include "kernel/ref.h"
#include "luamodule/luascriptloader.h"
#include "luamodule/luacmdproto.h"


//=========================================================================
// Lua commands
//=========================================================================

/**
	@brief Static function that will handle Lua to Cmd translations
	and dispatch to the provided Object *pointer.
*/
int _luaDispatch(lua_State *L, Object *obj, CmdProto *cmd_proto, bool print)
{
	Assert(cmd_proto); // -- unfriendly isn't it?
	Cmd *cmd = cmd_proto->NewCmd();
	Assert(cmd);

	// Need to get the proper args in...
	int numargs = cmd->GetInArgsCount();
	if ((lua_gettop(L) - 1) != numargs) {
		LogError1("Wrong number of arguments for command: %s", 
					cmd_proto->GetProtoDef());
		lua_settop(L, 0);
		lua_pushnil(L);

		return 1;
	}

	if (!LuaScriptLoader::StackToInArgs(L, cmd)) {
		LogError1("Incorrect arguments for: %s",
					cmd_proto->GetProtoDef());
		lua_settop(L, 0);
		lua_pushnil(L);

		return 1;
	}

	lua_settop(L, 0);

	if (!obj->Dispatch(cmd)) {
		LogError1("Could not dispatch the command: %s",
					cmd_proto->GetProtoDef());
		lua_pushnil(L);
		return 1;
	}

	LuaScriptLoader::OutArgsToStack(L, cmd, print);

	int retnum = cmd->GetOutArgsCount();
	cmd_proto->RelCmd(cmd);

	return retnum;
}


/**
*/
int luacmd_Error(lua_State *L)
{
	if (LuaScriptLoader::GetInstance()->IsFailOnError())
		ErrorMsg(lua_tostring(L, -1));
	else
		LogCritical1("%s", lua_tostring(L, -1));

	lua_settop(L, 0);

	return 0;
}


/**
*/
int luacmd_StackDump(lua_State *L)
{
	LuaScriptLoader::GetInstance()->GenerateStackTrace();
	lua_settop(L, 0);

	return 0;
}


/**
*/
int luacmd_Panic(lua_State *L)
{  
	ErrorMsg("Lua paniced");

	ScriptLoader *loader = (ScriptLoader *)lua_touserdata(L, lua_upvalueindex(1));
	loader->Exit();
	lua_settop(L, 0);

	return 0;
}


/**
*/
int luacmd_New(lua_State *L)
{
	const char* class_name;
	const char* object_name;

	// takes 2 strings as arguments
    // returns 1 thunk on success or nil on failure
	if ((2 != lua_gettop(L)) || !lua_isstring(L, -1) || !lua_isstring(L, -2)) {
		LogError("Usage is new('class', 'name')");
		lua_settop(L, 0);
		lua_pushnil(L);

		return 1;
	}

	class_name = lua_tostring(L, -2);
	object_name = lua_tostring(L, -1);
	lua_settop(L, 0);
	Root *root = KernelServer::GetInstance()->New(class_name, object_name, false);

	if (!root) {
		LogError2("Could not create object '%s' of class '%s'", object_name, class_name);
		lua_pushnil(L);
	}
	else
		lua_pushboolean(L, true);

	return 1;
}

/**
	Just like luacmd_New() except that it also creates and returns
	a thunk for the new object (the thunk is also stored in the
	_nebthunks table).
*/
int luacmd_NewThunk(lua_State *L)
{
	// takes 2 strings as arguments
	// returns 1 thunk on success or nil on failure
	if ((2 != lua_gettop(L)) || !lua_isstring(L, -1) || !lua_isstring(L, -2)) {
		LogError("Usage is newthunk('class', 'name')");
		lua_settop(L, 0);
		lua_pushnil(L);

		return 1;
	}

	const char* class_name = lua_tostring(L, -2);
	const char* object_name = lua_tostring(L, -1);

	lua_settop(L, 0);

	Root *root = KernelServer::GetInstance()->New(class_name, object_name, false);
	if (!root) {
		LogError2("Could not create object '%s' of class '%s'\n", object_name, class_name);
		lua_pushnil(L);
	}
	else
		// create thunk and leave it on stack
		LuaScriptLoader::GetInstance()->ThunkNebObject(L, root);

	return 1;
}


/**
	If a string is passed in it is assumed to be a NOH path, the
	corresponding Root instance will be deleted and if there's a
	thunk associated with that instance it will be removed from
	the _nebthunks table. If a thunk is passed in pretty much the
	same thing happens.
*/
int luacmd_Delete(lua_State *L)
{
	// takes 1 string or thunk as an argument
	// returns nil on failure and true on success
	Root *root;
	const char* object_name;

	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1) && !lua_istable(L, -1)) {
		LogError("Usage is delete('object name') or delete(thunk)");
		lua_settop(L, 0);
		lua_pushnil(L);

		return 1;
	}
	if (lua_isstring(L, -1)) {
		object_name = lua_tostring(L, -1);
		root = KernelServer::GetInstance()->Lookup(object_name);
	}
	else {
		object_name = "_thunk";
		root = LuaScriptLoader::UnpackThunkRoot(L, 1);
	}

	lua_settop(L, 0);
	if (root) {
		// remove the corresponding thunk (if there is one) from _nebthunks
		LuaScriptLoader::RemoveThunk(L, (void *)root);
		root->Release();
		lua_settop(L, 0);
		lua_pushboolean(L, true);
	}
	else {
		LogError1("Could not find %s", object_name);
		lua_pushnil(L);
	}

	return 1;
}


/**
	Removes the thunk passed in from the _nebthunks table, but
	does not delete the corresponding Root instance.

	If a string is passed in it is assumed to be a NOH path,
	otherwise it a thunk is expected.
*/
int luacmd_UnpinThunk(lua_State *L)
{
	// takes 1 string or thunk as an argument
	// returns nil on failure and true on success
	Root *root;
	const char* object_name;

	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1) && !lua_istable(L, -1)) {
		LogError("Usage is unpin('object name') or unpin(thunk)");
		lua_settop(L, 0);
		lua_pushnil(L);

		return 1;
	}
	if (lua_isstring(L, -1)) {
		object_name = lua_tostring(L, -1);
		root = KernelServer::GetInstance()->Lookup(object_name);
	}
	else {
		object_name = "_thunk";
		root = LuaScriptLoader::UnpackThunkRoot(L, 1);
	}

	lua_settop(L, 0);
	if (root) {
		// remove the corresponding thunk (if there is one) from _nebthunks
		LuaScriptLoader::RemoveThunk(L, (void *)root);
		lua_settop(L, 0);
		lua_pushboolean(L, true);
	}
	else {
		LogError1("Could not find %s", object_name);
		lua_pushnil(L);
	}

	return 1;
}


/**
	Pins a thunk, which means that a reference to it is stored in
	_nebthunks. The thunk will then be reused whenever possible
	instead of thunking an object all over again. If you want to
	define script side commands for an object you'll have
	to pin the corresponding thunk.

	If you have multiple thunks that correspond to the same object
	and you pin them all then only the last pin will actually "work".
*/
int luacmd_PinThunk(lua_State *L)
{
	// takes 1 thunk as an argument
	// returns nil on failure and true on success    
	if ((1 != lua_gettop(L)) || !lua_istable(L, -1)) {
		LogError("Usage is pin(thunk)");
		lua_settop(L, 0);
		lua_pushnil(L);
	}
	else {
		Root *root = LuaScriptLoader::UnpackThunkRoot(L, 1);
		LuaScriptLoader::AddThunk(L, (void *)root);
		lua_settop(L, 0);
		lua_pushboolean(L, true);
	}

	return 1;
}


/**
*/
int luacmd_Sel(lua_State *L)
{
	// takes 1 string as an argument
	// returns false on incorrect usage, nil if there is no such object, 
	// or true on success
	Root *root;

	if ((1 != lua_gettop(L)) || (!lua_isstring(L, -1) && !lua_istable(L, -1))) {
		LogError("Usage is sel('path') or sel(obj ref)");
		lua_settop(L, 0);
		lua_pushboolean(L, false);
		return 1;
	}

	if (lua_isstring(L, -1)) {
		const char* path = lua_tostring(L, -1);
		root = KernelServer::GetInstance()->Lookup(path);
	}
	else
		root = LuaScriptLoader::UnpackThunkRoot(L, 1);

	lua_settop(L, 0);
	if (!root) {
		LogError("Could not select object");
		lua_pushnil(L);
	}
	else {
		KernelServer::GetInstance()->SetCwd(root);
		lua_pushboolean(L, true);
	}

	return 1;
}


/**
	Returns the cwd of the NOH as a thunk.
*/
int luacmd_Psel(lua_State *L)
{
	// takes no arguments
	// returns 1 thunk
	Root *root = KernelServer::GetInstance()->GetCwd();
	if (root) {
		// check if a thunk for this object already exists in _nebthunks
		LuaScriptLoader::FindThunk(L, (void *)root);
		if (1 == lua_isnil(L, -1)) { // no thunk found? create it
			lua_settop(L, 0);
			LuaScriptLoader::GetInstance()->ThunkNebObject(L, root);
		}   
	}
	else {
		LogError("Could not find current working directory");
		lua_settop(L, 0);
		lua_pushnil(L);
	}

	return 1;
}


/**
*/
int luacmd_Exit(lua_State *L)
{
	//Takes no arguments
	//Returns nothing
	ScriptLoader *loader = (ScriptLoader *)lua_touserdata(L, lua_upvalueindex(1));
	loader->Exit();
	lua_settop(L, 0);

	return 0;
}


/**
*/
int luacmd_LogTree(lua_State *L)
{
	// returns nothing

	KernelServer::GetInstance()->LogTree();
	lua_settop(L, 0); // clear the stack
	return 0;
}


/**
*/
int luacmd_LogInfo(lua_State *L)
{
	// takes 1 string as an argument
	// returns nothing
	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1)) {
		LogError("Usage is loginfo('some text here')");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	LogInfo(lua_tostring(L, -1));
	lua_settop(L, 0); // clear the stack
	return 0;
}


/**
*/
int luacmd_LogWarning(lua_State *L)
{
	// takes 1 string as an argument
	// returns nothing
	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1)) {
		LogError("Usage is logwarning('some text here')");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	LogWarning(lua_tostring(L, -1));
	lua_settop(L, 0); // clear the stack
	return 0;
}


/**
*/
int luacmd_LogError(lua_State *L)
{
	// takes 1 string as an argument
	// returns nothing
	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1)) {
		LogError("Usage is logerror('some text here')");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	LogError(lua_tostring(L, -1));
	lua_settop(L, 0); // clear the stack
	return 0;
}


/**
*/
int luacmd_LogCritical(lua_State *L)
{
	// takes 1 string as an argument
	// returns nothing
	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1)) {
		LogError("Usage is logcritical('some text here')");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	LogCritical(lua_tostring(L, -1));
	lua_settop(L, 0); // clear the stack
	return 0;
}


/**
*/
int luacmd_LogDebug(lua_State *L)
{
	// takes 1 string as an argument
	// returns nothing
	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1)) {
		LogError("Usage is logdebug('some text here')");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	LogDebug(lua_tostring(L, -1));
	lua_settop(L, 0); // clear the stack
	return 0;
}


/**
*/
int luacmd_Dir(lua_State *L)
{
	// This should return a table - for output this will
	// have to be translated back to a string.
	lua_settop(L, 0);
	Root *cwd = KernelServer::GetInstance()->GetCwd();

	if (!cwd) {
		LogError("Could not acquire the current working directory");
		lua_pushnil(L);
		return 1;
	}

	// Construct a table to return
	lua_newtable(L);
	int i = 0;
	Root *root = 0;

	for (root=cwd->GetFront(); root; root = root->GetNext()) {
		lua_pushnumber(L, i++);
		lua_pushstring(L, root->GetID().c_str());
		lua_settable(L, -3);
	}

	return 1;
}


/**
*/
int luacmd_CmdDispatch(lua_State *L)
{
	// Thunks *only* come here
	// Get the Root *out of the table self ref first
	// The parent table is only guarenteed to be first
	// if the : syntax is used - otherwise this fails
	if (!lua_istable(L, 1)) {
		LogError("On calling member functions make sure to use the ':' operator to access methods");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	Root *root = LuaScriptLoader::UnpackThunkRoot(L, 1);
	// haul out the particular call - in the upvalue
	CmdProto *cmdproto = (CmdProto*)lua_touserdata(L, lua_upvalueindex(1));

	return _luaDispatch(L, root, cmdproto, false);
}


/**
	Executes a func on the current working directory.
*/
int luacmd_Call(lua_State *L)
{
	// Anonymous call version
	// This requires no thunk and has
	// to do the lookup from here.
	int num = lua_gettop(L);
	if (!num || !lua_isstring(L, 1)) {
		LogError("Usage is call('func', ...)");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	Object *obj = ScriptLoader::GetCurrentTarget();
	if (!obj)
		obj = KernelServer::GetInstance()->GetCwd();

	const char* cmdname = lua_tostring(L, 1);
	Class* cl = obj->GetClass();
	CmdProto *cmd_proto = (CmdProto*) cl->FindCmdByName(string(cmdname));

	if (!cmd_proto) {
		LogError1("Could not find the command '%s'", cmdname);
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	return _luaDispatch(L, obj, cmd_proto, false);
}


/**
	Executes a func on the current working directory and prints 
	the result to the console.
*/
int luacmd_ConCall(lua_State *L)
{
	//Anonymous call version
	//This requires no thunk and has
	//to do the look up from here.
	int num = lua_gettop(L);
	if (!num || !lua_isstring(L, 1)) {
		LogError("Usage is concall('func', ...)");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	Root *root = KernelServer::GetInstance()->GetCwd();
	const char* cmdname = lua_tostring(L, 1);
	Class* cl = root->GetClass();
	CmdProto *cmd_proto = (CmdProto*) cl->FindCmdByName(string(cmdname));

	if (!cmd_proto) {
		LogError1("Could not find the command '%s'", cmdname);
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	return _luaDispatch(L, root, cmd_proto, true);
}

/**
	Returns the thunk that corresponds to specified NOH path. If the
	thunk hasn't been created yet it will be created, otherwise the
	existing thunk is returned.
*/
int luacmd_Lookup(lua_State *L)
{
	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1))
	{
		LogError("Usage is lookup('fullpathname')");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	Root *root = (Root*)KernelServer::GetInstance()->Lookup(lua_tostring(L, -1));
	lua_settop(L, 0);

	if (!root) {
		LogError1("Could not find object: %s", lua_tostring(L, -1));
		lua_pushnil(L);
	}
	else {
		LuaScriptLoader::FindThunk(L, (void *)root);
		if (1 == lua_isnil(L, -1)) // no thunk? create it
			LuaScriptLoader::ThunkNebObject(L, root);
	}
	return 1;
}


/**
*/
int luacmd_Mangle(lua_State *L)
{
    if ((1 != lua_gettop(L)) || !lua_isstring(L, -1))
    {
        LogError("Usage is mangle('path')");
        lua_settop(L, 0);
        lua_pushnil(L);
        return 1;
    }
    
    string path;
	if (!FileServer::GetInstance()->ManglePath(string(lua_tostring(L, -1)), path))
	{
        LogError1("Failed to mangle %s", path.c_str());
        lua_settop(L, 0);
        lua_pushnil(L);
        return 1;
    }
    lua_settop(L, 0);
    lua_pushstring(L, path.c_str());

    return 1;
}


/**
*/
int luacmd_Exists(lua_State *L)
{
	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1)) {
		LogError("Usage is exists('name')");
		lua_settop(L, 0);
		lua_pushnil(L);
		return 1;
	}

	Root *root = (Root*)KernelServer::GetInstance()->Lookup(lua_tostring(L, -1));
	lua_settop(L, 0);

	if (root)
		lua_pushboolean(L, 1);
	else
		lua_pushboolean(L, 0);

	return 1;
}


/**
*/
int luacmd_BeginCmds(lua_State *L)
{
	// takes two arguments - the class name and the number of cmds.
	// returns nothing.
	if ((2 != lua_gettop(L)) || !(lua_isstring(L, -2) && lua_isnumber(L, -1))) {
		LogError("Usage is begincmds('classname', iNumCmds)");
		lua_settop(L, 0);
		return 0;
	}

	const char* class_name = lua_tostring(L, -2);
	Class* clazz = KernelServer::GetInstance()->FindClass(class_name);
	if (clazz)
		clazz->BeginScriptCmds(int(lua_tonumber(L, -1)));
	else
		ErrorMsg1("Failed to open class %s", class_name);

	lua_settop(L, 0);

	return 0;
}


/**
*/
int luacmd_AddCmd(lua_State *L)
{
	// takes in 2 strings, the name of the class and the name of the cmd.
	// returns nothing.
	if ((2 != lua_gettop(L)) || !(lua_isstring(L, -1) && lua_isstring(L, -2))) {
		LogError("Usage is addcmd('classname', 'cmd def')");
		lua_settop(L, 0);
		return 0;
	}

	LuaCmdProto* cmdProto = NEW LuaCmdProto(lua_tostring(L, -1));
	const char* class_name = lua_tostring(L, -2);
	Class* clazz = KernelServer::GetInstance()->FindClass(class_name);
	if (clazz)
		clazz->AddScriptCmd((CmdProto*)cmdProto);
	else
		ErrorMsg1("Failed to find class %s", class_name);
    
	lua_settop(L, 0);

	return 0;
}


/**
*/
int luacmd_EndCmds(lua_State *L)
{
	// Takes one argument - the class name.
	// Returns nothing.
	if ((1 != lua_gettop(L)) || !lua_isstring(L, -1)) {
		LogError("Usage is endcmds('classname')");
		lua_settop(L, 0);
		return 0;
	}

	const char* class_name = lua_tostring(L, -1);
	Class* clazz = KernelServer::GetInstance()->FindClass(class_name);
	if (clazz)
		clazz->EndScriptCmds();
	else
		ErrorMsg1("Failed to find class %s", class_name);
    
	lua_settop(L, 0);

	return 0;
}


/**
	Deletes an Ref previously created during thunking.
	This method will be automatically called when a thunk gets
	garbage collected.
*/
int luacmd_DeleteNRef(lua_State *L)
{
	// Takes in one argument - the nref userdata
	Assert((1 == lua_gettop(L)) && lua_isuserdata(L, -1));
	Ref<Root>* ref = *((Ref<Root>**)lua_touserdata(L, -1));
	if (ref)
		delete ref;

	return 0;
}


/**
	Checks if a thunk is a zombie (has an Ref that points to a
	non-existent Root).
*/
int luacmd_IsZombieThunk(lua_State *L)
{
	// Takes in one argument - the thunk
	// Returns true if thunk is a zombie, false if it isn't, nil on error
	if ((1 != lua_gettop(L)) || !lua_istable(L, -1)) {
	  LogError("Usage is iszombie(thunk)");
	  lua_settop(L, 0);
	  lua_pushnil(L);
	  return 1;
	}

	lua_pushstring(L, "_");
	lua_rawget(L, -2);
	AssertMsg(lua_isuserdata(L, -1), "Ref userdata not found in thunk");

	Ref<Root>* ref = *((Ref<Root>**)lua_touserdata(L, -1));
	lua_settop(L, 0);

	if (ref)
		lua_pushboolean(L, !ref->IsValid());
	else {
		ErrorMsg("Degenerate thunk detected");
		lua_pushnil(L);
	}

	return 1;
}


/**
	Just exposes KernelServer::PushCwd().
*/
int luacmd_PushCwd(lua_State *L)
{
	// Takes in one argument - either a path string or a thunk.
	// Returns true on success, false on failure.
	if ((1 != lua_gettop(L)) || !(lua_istable(L, -1) || lua_isstring(L, -1)))
	{
		LogError("Usage is pushcwd('path') or pushcwd(thunk)");
		lua_settop(L, 0);
		lua_pushboolean(L, false);
		return 1;
	}

	if (lua_istable(L, -1)) { // thunk case
		lua_pushstring(L, "_");
		lua_rawget(L, -2);
		AssertMsg(lua_isuserdata(L, -1), "Ref userdata not found in thunk");

		Ref<Root>* ref = *((Ref<Root>**)lua_touserdata(L, -1));

		if (ref) {
			KernelServer::GetInstance()->PushCwd(ref->Get());
			lua_settop(L, 0);
			lua_pushboolean(L, true);
		}
		else
			ErrorMsg("Degenerate thunk detected");
	}
	else // string path case
	{
		Root *node = KernelServer::GetInstance()->Lookup(lua_tostring(L, -1));
		if (node) {
			KernelServer::GetInstance()->PushCwd(node);
			lua_settop(L, 0);
			lua_pushboolean(L, true);
		}
		else
		{
			LogError1("pushcwd: %s not found", lua_tostring(L, -1));
			lua_settop(L, 0);
			lua_pushboolean(L, false);
		}
	}
    
	return 1;
}


/**
	Just exposes KernelServer::PopCwd()
*/
int luacmd_PopCwd(lua_State *L)
{
	// Takes in no arguments.
	// Returns nothing.
	if (0 != lua_gettop(L)) {
		LogError("Usage is popcwd()");
		return 0;
	}
	KernelServer::GetInstance()->PopCwd();

	return 0;
}

// vim:ts=4:sw=4:
