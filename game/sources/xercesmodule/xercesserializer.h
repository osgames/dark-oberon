#ifndef __xercesserializer_h__
#define __xercesserializer_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file xercesserializer.h
	@ingroup Xerces_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes 
//=========================================================================

#include "framework/system.h"
#include "kernel/serializer.h"

//class DOMElement;


//=========================================================================
// XercesSerializer
//=========================================================================

/**
	@class XercesSerializer
	@ingroup Formats_Module
*/

class XercesSerializer : public Serializer
{
//--- methods
public:
	XercesSerializer(const char *id);
	virtual ~XercesSerializer();

	// serializing
	virtual bool BeginSerialize(const char *root_name);
	virtual long EndSerialize(byte_t *&buffer);

	virtual bool AddGroup(const char *group_name);

	virtual bool SetAttribute(const char *attr_name, const char *value);
	virtual bool SetAttribute(const char *attr_name, const string &value);
	virtual bool SetAttribute(const char *attr_name, byte_t value);
	virtual bool SetAttribute(const char *attr_name, ushort_t value);
	virtual bool SetAttribute(const char *attr_name, uint_t value);
	virtual bool SetAttribute(const char *attr_name, short value);
	virtual bool SetAttribute(const char *attr_name, int value);
	virtual bool SetAttribute(const char *attr_name, long value);
	virtual bool SetAttribute(const char *attr_name, float value);
	virtual bool SetAttribute(const char *attr_name, double value);
	virtual bool SetAttribute(const char *attr_name, bool value);
	virtual bool SetAttribute(const char *attr_name, const vector2 &value);
	virtual bool SetAttribute(const char *attr_name, const vector3 &value);
	virtual bool SetAttribute(const char *attr_name, const vector4 &value);

	virtual bool SetAttributePercent(const char *attr_name, float value);
	virtual bool SetAttributeColor(const char *attr_name, const vector3 &value);
	virtual bool SetAttributeColor(const char *attr_name, const vector4 &value);

	virtual bool SetValue(const char *value);
	virtual bool SetValue(const string &value);
	virtual bool SetValue(byte_t value);
	virtual bool SetValue(ushort_t value);
	virtual bool SetValue(uint_t value);
	virtual bool SetValue(short value);
	virtual bool SetValue(int value);
	virtual bool SetValue(long value);
	virtual bool SetValue(float value);
	virtual bool SetValue(double value);
	virtual bool SetValue(bool value);
	virtual bool SetValue(const vector2 &value);
	virtual bool SetValue(const vector3 &value);
	virtual bool SetValue(const vector4 &value);

	// deserializing
	virtual bool BeginDeserialize(const byte_t *buffer, long buff_size);
	virtual void EndDeserialize();

	virtual bool GetGroup(const char *group_name = NULL);
	virtual bool GetNextGroup(bool mixed = false);

	virtual bool GetAttribute(const char *attr_name, string &value, bool empty = true);
	virtual bool GetAttribute(const char *attr_name, byte_t &value);
	virtual bool GetAttribute(const char *attr_name, ushort_t &value);
	virtual bool GetAttribute(const char *attr_name, uint_t &value);
	virtual bool GetAttribute(const char *attr_name, short &value);
	virtual bool GetAttribute(const char *attr_name, int &value);
	virtual bool GetAttribute(const char *attr_name, long &value);
	virtual bool GetAttribute(const char *attr_name, float &value);
	virtual bool GetAttribute(const char *attr_name, double &value);
	virtual bool GetAttribute(const char *attr_name, bool &value);
	virtual bool GetAttribute(const char *attr_name, vector2 &value);
	virtual bool GetAttribute(const char *attr_name, vector3 &value);
	virtual bool GetAttribute(const char *attr_name, vector4 &value);

	virtual bool GetAttributePercent(const char *attr_name, float &value);
	virtual bool GetAttributeColor(const char *attr_name, vector3 &value);
	virtual bool GetAttributeColor(const char *attr_name, vector4 &value);

	virtual bool GetValue(string &value, bool empty = true);
	virtual bool GetValue(byte_t &value);
	virtual bool GetValue(ushort_t &value);
	virtual bool GetValue(uint_t &value);
	virtual bool GetValue(short &value);
	virtual bool GetValue(int &value);
	virtual bool GetValue(long &value);
	virtual bool GetValue(float &value);
	virtual bool GetValue(double &value);
	virtual bool GetValue(bool &value);
	virtual bool GetValue(vector2 &value);
	virtual bool GetValue(vector3 &value);
	virtual bool GetValue(vector4 &value);

	// universal
	virtual bool CheckGroupName(const char *name);
	virtual ushort_t GetGroupsCount();
	virtual void EndGroup();

private:
	bool StringToBool(const string &value, bool &result);
	const char *BoolToString(bool value);

private:
	XercesDOMParser *parser;
	DOMWriter *writer;
	MemBufInputSource *source;
	MemBufFormatTarget *target;

	ErrorHandler *parse_err;
	DOMErrorHandler *write_err;

	DOMElement *act_element;
	DOMDocument *target_doc;

	uint_t group_level;
};


//=========================================================================
// Methods
//=========================================================================

inline
bool XercesSerializer::StringToBool(const string &value, bool &result)
{
	// convert to proper type
	if ((value == "true") || 
		(value == "yes") ||
		(value == "on") ||
		(value == "1"))
	{
		result = true;
		return true;
	}

	if ((value == "false") || 
		(value == "no") ||
		(value == "off") ||
		(value == "0"))
	{
		result = false;
		return true;
	}

	return false;
}


inline
const char *XercesSerializer::BoolToString(bool value)
{
	return value ? "true" : "false";
}


#endif // __xercesserializer_h__

// vim:ts=4:sw=4:
