#ifndef __xmlutils_h__
#define __xmlutils_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file xmlutils.h
	@ingroup Formats_Module

 	@author Peter Knut
	@date 2006

	Contains usefull classes and methods that make working with Xerces-C library easier.
	DO NOT include this file into any header file! After that whole project dies because of
	hell with XSerializeEngine::Assert method which is not compatible with our Assert macro.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes 
//=========================================================================

// XML_LIBRARY tells Xerces headers, that we use it as static library
#ifndef XML_LIBRARY
#	define XML_LIBRARY
#endif

// include Xerces-C before system.h
#include <xercesc/util/XMLString.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#include <xercesc/framework/MemBufFormatTarget.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include "framework/system.h"
#include "kernel/logserver.h"

XERCES_CPP_NAMESPACE_USE


//=========================================================================
// StrX
//=========================================================================

/**
	@class StrX
	@ingroup Formats_Module

	Simple class that lets us do easy (though not terribly efficient)
	transcoding of XMLCh data to local code page and back.
*/

class StrX
{
//--- methods
public :
	StrX(const XMLCh *text);
	StrX(const char *text);
	~StrX();

	const char *GetLocal() const;
	const XMLCh *GetUnicode() const;
	const XMLCh *GetUnicodeLower() const;

//--- variables
private :
	char *local_text;                        ///< The local code page form of the string.
	XMLCh *unicode_text;                     ///< The unicode code page form of the string.
};


/**
	Constructor. Transcodes given text into local form.

	@param text Unicode text.
*/
inline
StrX::StrX(const XMLCh *text) :
	unicode_text(NULL)
{
	Assert(text);
	local_text = XMLString::transcode(text);
}


/**
	Constructor. Transcodes given text into unicode form.

	@param text Text in local form.
*/
inline
StrX::StrX(const char *text) :
	local_text(NULL)
{
	Assert(text);
	unicode_text = XMLString::transcode(text);
}


/**
	Destructor. Deletes transcoded text.
*/
inline
StrX::~StrX()
{
	if (local_text)
		XMLString::release(&local_text);
	if (unicode_text)
		XMLString::release(&unicode_text);
}


/**
	Returns text in local code page form.
*/
inline
const char* StrX::GetLocal() const
{
	Assert(this->local_text);
	return this->local_text;
}


/**
	Returns text in unicode code page form.
*/
inline
const XMLCh* StrX::GetUnicode() const
{
	Assert(this->unicode_text);
	return this->unicode_text;
}


/**
	Transform text to lowercase and returns it in unicode code page form.
*/
inline
const XMLCh* StrX::GetUnicodeLower() const
{
	Assert(this->unicode_text);
	XMLString::lowerCase(this->unicode_text);
	return this->unicode_text;
}


//=========================================================================
// Error handlers
//=========================================================================

class ParseErrorHandler : public ErrorHandler
{
public:
	ParseErrorHandler() {}
	~ParseErrorHandler() {}

	// implementation of the error handler interface
	void warning(const SAXParseException &exc)
	{
		LogWarning(StrX(exc.getMessage()).GetLocal());
	}
	void error(const SAXParseException &exc)
	{
		LogError(StrX(exc.getMessage()).GetLocal());
	}
	void fatalError(const SAXParseException &exc)
	{
		LogError(StrX(exc.getMessage()).GetLocal());
	}

	void resetErrors() {}
};


class WriteErrorHandler : public DOMErrorHandler
{
public:
	WriteErrorHandler() {}
	~WriteErrorHandler() {}

	// implementation of the error handler interface
	bool handleError(const DOMError &error)
	{
		if (error.getSeverity() == DOMError::DOM_SEVERITY_WARNING)
			LogWarning(StrX(error.getMessage()).GetLocal());
		else
			LogError(StrX(error.getMessage()).GetLocal());

		// continue serialization if possible.
		return true;
	}

	void resetErrors() {}

private :
	// unimplemented
	WriteErrorHandler(const DOMErrorHandler &);
	void operator=(const DOMErrorHandler &);

};


//=========================================================================
// XMLUtils
//=========================================================================

/**
	@class XMLUtils
	@ingroup Formats_Module

	Covers usefull methods that make working with Xerces-C library easier.
	All methods are static so you don't need to create an instance.
*/

class XMLUtils
{
//--- methods
public:
	static bool Init();
	static void Done();

	static XercesDOMParser *CreateParser();
	static DOMWriter *CreateWriter();

	static ErrorHandler *CreateParseErrorHandler();
	static DOMErrorHandler *CreateWriteErrorHandler();

	static MemBufInputSource *CreateMemorySource(const byte_t *buffer, long size, const char *source_id);
	static MemBufFormatTarget *CreateMemoryTarget();

	static bool ParseSource(XercesDOMParser *parser, InputSource* source);
	static bool WriteTarget(DOMWriter *writer, XMLFormatTarget *target, DOMNode *node);

	static DOMDocument *CreateDocument(const char *root_name);
	static const XMLCh *GetAttributeValue(const DOMElement *element, const char *attr_name);
	static ushort_t GetSubElementsCount(const DOMElement *element);
};


/**
	Initializes Xerces-C library. Library can be initialized and terminated multiple times.
	@return @c True if successful.
*/
inline
bool XMLUtils::Init()
{
	try
	{
		XMLPlatformUtils::Initialize();
	}
	catch (const XMLException& exception)
	{
		LogError(StrX(exception.getMessage()).GetLocal());
		return false;
	}

	return true;
}


/**
	Terminates Xerces-C library. Library can be initialized and terminated multiple times.
*/
inline
void XMLUtils::Done()
{
	XMLPlatformUtils::Terminate();
}


/**
	Creates DOM parser and sets wanted properties.

	@return Parser object.
*/
inline
XercesDOMParser *XMLUtils::CreateParser()
{
	// create parser
	XercesDOMParser *parser = NEW XercesDOMParser();
	Assert(parser);

	// set parser properties
	parser->setValidationScheme(XercesDOMParser::Val_Auto);
	parser->setDoNamespaces(true);
	parser->setDoSchema(true);
    parser->setValidationSchemaFullChecking(true);

	return parser;
}


/**
	Creates DOM writer and sets wanted properties.

	@return Writer object.
*/
inline
DOMWriter *XMLUtils::CreateWriter()
{
	// create parser
	DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(StrX("LS").GetUnicode());
	Assert(impl);

	DOMWriter *writer = ((DOMImplementationLS*)impl)->createDOMWriter();
	Assert(writer);

	// set parser properties
	writer->setEncoding(StrX("utf-8").GetUnicode());

	if (writer->canSetFeature(XMLUni::fgDOMWRTDiscardDefaultContent, true))
		writer->setFeature(XMLUni::fgDOMWRTDiscardDefaultContent, true);

	if (writer->canSetFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true))
		writer->setFeature(XMLUni::fgDOMWRTFormatPrettyPrint, true);

	return writer;
}


/**
	Creates error handler for parsing.
	@return Error handler.
*/
inline
ErrorHandler *XMLUtils::CreateParseErrorHandler()
{
	// create handler
	ErrorHandler *err_handler = NEW ParseErrorHandler();
	Assert(err_handler);

	return err_handler;
}


/**
	Creates error handler for writing.
	@return Error handler.
*/
inline
DOMErrorHandler *XMLUtils::CreateWriteErrorHandler()
{
	// create handler
	DOMErrorHandler *err_handler = NEW WriteErrorHandler();

	return err_handler;
}


/**
	Creates memory source from given memory buffer.

	@param buffer    Memory buffer.
	@param size      Size of memory buffer.
	@param source_id Text identifier of new memory source.

	@return Memory source object.
*/
inline
MemBufInputSource *XMLUtils::CreateMemorySource(const byte_t *buffer, long size, const char *source_id)
{
	Assert(buffer);
	Assert(size);
	Assert(source_id);

	// create memory source
	MemBufInputSource *source = NEW MemBufInputSource((const XMLByte *)buffer, size, StrX(source_id).GetUnicode(), false);
	source->setCopyBufToStream(false);

	return source;
}


/**
	Creates memory source from given memory buffer.

	@return Memory source object.
*/
inline
MemBufFormatTarget *XMLUtils::CreateMemoryTarget()
{
	// create memory target
	MemBufFormatTarget *target = NEW MemBufFormatTarget();

	return target;
}


/**
	Parses data from source using given parser.

	@param parser DOM parser object.
	@param source Input source object.
	@return @c True if successful.
*/
inline
bool XMLUtils::ParseSource(XercesDOMParser *parser, InputSource *source)
{
	Assert(parser);
	Assert(source);

	// parse source
	try {
		parser->parse(*source);
	}

	// catch exceptions
	catch (const XMLException& exception)
	{
		LogError(StrX(exception.getMessage()).GetLocal());
		return false;
	}
	catch (const DOMException& exception)
	{
		LogError(StrX(exception.getMessage()).GetLocal());
		return false;
	}
	catch (...)
	{
		LogError("Unexpected exception during parsing XML data");
		return false;
	}

	return true;
}


/**
	Writes data to target using given writer.

	@param writer  DOM writer object.
	@param target  Input source object.
	@param node    Node to write.

	@return @c True if successful.
*/
inline
bool XMLUtils::WriteTarget(DOMWriter *writer, XMLFormatTarget *target, DOMNode *node)
{
	Assert(writer);
	Assert(target);
	Assert(node);

	// parse source
	try {
		writer->writeNode(target, *node);
	}

	// catch exceptions
	catch (const XMLException& exception)
	{
		LogError(StrX(exception.getMessage()).GetLocal());
		return false;
	}
	catch (const DOMException& exception)
	{
		LogError(StrX(exception.getMessage()).GetLocal());
		return false;
	}
	catch (...)
	{
		LogError("Unexpected exception during parsing XML data");
		return false;
	}

	return true;
}


DOMDocument *XMLUtils::CreateDocument(const char *root_name)
{
	assert(root_name && *root_name);

	DOMImplementation *impl = DOMImplementationRegistry::getDOMImplementation(StrX("Core").GetUnicode());
	Assert(impl);

	DOMDocument *doc = NULL;

	try
    {
		doc = impl->createDocument(NULL, StrX(root_name).GetUnicodeLower(), NULL);
		Assert(doc);
	}

	// catch exceptions
	catch (const XMLException& exception)
	{
		LogError(StrX(exception.getMessage()).GetLocal());
		return false;
	}
	catch (const DOMException& exception)
	{
		LogError(StrX(exception.getMessage()).GetLocal());
		return false;
	}
	catch (...)
	{
		LogError("Unexpected exception during creating XML document");
		return false;
	}

	return doc;
}


/**
	Returns text value of specified attribute.

	@param element   DOM element.
	@param attr_name Name of attribute.
	@return Value of attribute.
*/
inline
const XMLCh *XMLUtils::GetAttributeValue(const DOMElement *element, const char *attr_name)
{
	Assert(element);
	Assert(attr_name);

	// get attribute value
	const XMLCh *value;
	value = element->getAttribute(StrX(attr_name).GetUnicode());

	return value;
}


inline
ushort_t XMLUtils::GetSubElementsCount(const DOMElement *element)
{
	ushort_t count = 0;

	DOMNode *node;
	for (node = element->getFirstChild(); node; node = node->getNextSibling())
		if (node->getNodeType() == DOMNode::ELEMENT_NODE)
			count++;

	return count;
}


#endif // __xmlutils_h__

// vim:ts=4:sw=4:
