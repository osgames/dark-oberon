//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file xercesmodule.cpp
	@ingroup Lua_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"


//=========================================================================
// LuaModule
//=========================================================================

extern "C" void XercesModule();


extern bool s_InitXercesSerializer (Class *, KernelServer *);
extern Object *s_NewXercesSerializer (const char *name);


void XercesModule()
{
	KernelServer::GetInstance()->RegisterClass("XercesSerializer", s_InitXercesSerializer, s_NewXercesSerializer);
}

// vim:ts=4:sw=4:
