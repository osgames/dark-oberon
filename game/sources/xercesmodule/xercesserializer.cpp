//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file xercesserializer.cpp
	@ingroup Formats_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes 
//=========================================================================

#include "xercesmodule/xmlutils.h"
#include "xercesmodule/xercesserializer.h"
#include "kernel/kernelserver.h"

PrepareClass(XercesSerializer, "Serializer", s_NewXercesSerializer, s_InitXercesSerializer);


//=========================================================================
// XmlSerializer
//=========================================================================

/**
	Constructor. Sets file mode to binary.
*/
XercesSerializer::XercesSerializer(const char *id) :
	Serializer(id),
	parser(NULL),
	writer(NULL),	
	source(NULL),
	target(NULL),
	parse_err(NULL),
	write_err(NULL),
	target_doc(NULL),
	act_element(NULL),
	group_level(0)
{
	// initialize Xerces
	if (!XMLUtils::Init())
		LogError("Error initializing Xcerces-C library");

	// create parser objects
	this->parser = XMLUtils::CreateParser();
	this->writer = XMLUtils::CreateWriter();
	this->parse_err = XMLUtils::CreateParseErrorHandler();
	this->write_err = XMLUtils::CreateWriteErrorHandler();
	this->target = XMLUtils::CreateMemoryTarget();

	Assert(this->parser && this->writer && this->parse_err && this->write_err && this->target);

	parser->setErrorHandler(this->parse_err);
	writer->setErrorHandler(this->write_err);
}


/**
	Destructor. Closes opened file.
*/
XercesSerializer::~XercesSerializer()
{
	// delete parser objects
	delete this->parser;
	delete this->writer;
	delete this->parse_err;
	delete this->write_err;
	delete this->target;

	// terminate Xerces
	XMLUtils::Done();
}


//=========================================================================
// Serializing
//=========================================================================

bool XercesSerializer::BeginSerialize(const char *root_name)
{
	assert(root_name && *root_name);

	// call ancestor
	if (!Serializer::BeginSerialize(root_name))
		return false;

	// create DOM document
	this->target_doc = XMLUtils::CreateDocument(root_name);
	Assert(this->target_doc);

	// set root element as actual
	this->act_element = this->target_doc->getDocumentElement();
	Assert(this->act_element);

	this->group_level = 0;

	return true;
}


long XercesSerializer::EndSerialize(byte_t *&buffer)
{
	// write xml data
	bool ok = XMLUtils::WriteTarget(this->writer, this->target, this->target_doc);
	if (ok)
	{
		this->serialize_size = this->target->getLen();
		this->serialize_buffer = new byte_t[this->serialize_size];
		memcpy(this->serialize_buffer, this->target->getRawBuffer(), this->serialize_size);
	}

	// delete DOM tree
	if (this->target_doc)
		this->target_doc->release();
	this->act_element = NULL;

	// reset memory target
	this->target->reset();

	// some error occures
	if (!ok)
	{
		LogError("Error writing XML data");
		return 0;
	}

	// call ancestor
	return Serializer::EndSerialize(buffer);
}


bool XercesSerializer::AddGroup(const char *group_name)
{
	DOMElement *element = this->target_doc->createElement(StrX(group_name).GetUnicode());

	if (!element || !this->act_element->appendChild(element))
		return false;

	this->act_element = element;
	this->group_level++;
	return true;
}


//=========================================================================
// SetAttribute
//=========================================================================

bool XercesSerializer::SetAttribute(const char *attr_name, const char *value)
{
	this->act_element->setAttribute(StrX(attr_name).GetUnicode(), StrX(value).GetUnicode());
	return true;
}


bool XercesSerializer::SetAttribute(const char *attr_name, const string &value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	return this->SetAttribute(attr_name, value.c_str());
}


bool XercesSerializer::SetAttribute(const char *attr_name, byte_t value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%hu", ushort_t(value));

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, ushort_t value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%hu", value);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, uint_t value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%u", value);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, short value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%hd", value);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, int value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%d", value);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, long value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%ld", value);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, float value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f", value);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, double value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f", value);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, bool value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	return this->SetAttribute(attr_name, this->BoolToString(value));
}


bool XercesSerializer::SetAttribute(const char *attr_name, const vector2 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f;%.4", value.x, value.y);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, const vector3 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f;%.4;%.4f", value.x, value.y, value.z);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttribute(const char *attr_name, const vector4 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f;%.4f;%.4f;%.4f", value.x, value.y, value.z, value.w);

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttributePercent(const char *attr_name, float value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	return this->SetAttribute(attr_name, byte_t(value * 100.0f));
}


bool XercesSerializer::SetAttributeColor(const char *attr_name, const vector3 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%hu;%hu;%hu", byte_t(value.x * 255.0f), byte_t(value.y * 255.0f), byte_t(value.z * 255.0f));

	return this->SetAttribute(attr_name, buff);
}


bool XercesSerializer::SetAttributeColor(const char *attr_name, const vector4 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%hu;%hu;%hu;%hu", byte_t(value.x * 255.0f), byte_t(value.y * 255.0f), byte_t(value.z * 255.0f), byte_t(value.w * 255.0f));

	return this->SetAttribute(attr_name, buff);
}


//=========================================================================
// SetValue
//=========================================================================

bool XercesSerializer::SetValue(const char *value)
{
	DOMText *text = this->target_doc->createTextNode(StrX(value).GetUnicode());
	if (!text)
		return false;

	return this->act_element->appendChild(text) != NULL;
}


bool XercesSerializer::SetValue(const string &value)
{
	Assert(this->state == SS_SERIALIZING);

	return this->SetValue(value.c_str());
}


bool XercesSerializer::SetValue(byte_t value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%hu", ushort_t(value));

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(ushort_t value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%hu", value);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(uint_t value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%u", value);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(short value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%hd", value);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(int value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%d", value);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(long value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%ld", value);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(float value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f", value);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(double value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f", value);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(bool value)
{
	Assert(this->state == SS_SERIALIZING);

	return this->SetValue(this->BoolToString(value));
}


bool XercesSerializer::SetValue(const vector2 &value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f;%.4f", value.x, value.y);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(const vector3 &value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f;%.4f;%.4f", value.x, value.y, value.z);

	return this->SetValue(buff);
}


bool XercesSerializer::SetValue(const vector4 &value)
{
	Assert(this->state == SS_SERIALIZING);

	char buff[100];
	sprintf(buff, "%.4f;%.4f;%.4f;%.4f", value.x, value.y, value.z, value.w);

	return this->SetValue(buff);
}


//=========================================================================
// Deserializing
//=========================================================================

bool XercesSerializer::BeginDeserialize(const byte_t *buffer, long buff_size)
{
	Assert(buffer);
	Assert(buff_size > 0);

	// call ancestor
	if (!Serializer::BeginDeserialize(buffer, buff_size))
		return false;

	// create memory source
	this->source = XMLUtils::CreateMemorySource(this->deserialize_buffer, this->deserialize_size, "XercesSerializerSource");
	Assert(this->source);

	// parse xml data
	if (!XMLUtils::ParseSource(this->parser, this->source))
	{
		LogError("Error parsing XML data");
		SafeDelete(this->source);
		return false;
	}

	// set root element as actual
	this->act_element = parser->getDocument()->getDocumentElement();
	if (!this->act_element)
	{
		LogError("Missing root element");
		SafeDelete(this->source);
		return false;
	}

	this->group_level = 0;

	return true;
}


void XercesSerializer::EndDeserialize()
{
	// delete memory source
	SafeDelete(this->source);

	// call ancestor
	Serializer::EndDeserialize();
}


/**
	@param group_name Name of group. @c NULL means first group.
*/
bool XercesSerializer::GetGroup(const char *group_name)
{
	Assert(this->state == SS_DESERIALIZING);

	// find child
	DOMNode *node;
	const XMLCh *xml_name;

	for (node = this->act_element->getFirstChild(); node; node = node->getNextSibling())
	{
		if (node->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			if (group_name && *group_name)
			{
				xml_name = ((DOMElement *)node)->getTagName();
				Assert(xml_name);

				if (XMLString::equals(xml_name, StrX(group_name).GetUnicode()))
					break;
			}
			else
				break;
		}
	}

	// child was found
	if (node)
	{
		this->act_element = (DOMElement *)node;
		this->group_level++;
		return true;
	}

	return false;
}


bool XercesSerializer::GetNextGroup(bool mixed)
{
	Assert(this->state == SS_DESERIALIZING);

	// find next child
	DOMNode *node;
	const XMLCh *xml_name;

	for (node = this->act_element->getNextSibling(); node; node = node->getNextSibling())
	{
		if (node->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			if (mixed)
				break;

			else
			{
				xml_name = ((DOMElement *)node)->getTagName();
				Assert(xml_name);

				if (XMLString::equals(xml_name, this->act_element->getTagName()))
					break;
			}
		}
	}

	// child was found
	if (node)
	{
		this->act_element = (DOMElement *)node;
		return true;
	}

	return false;
}


//=========================================================================
// GetAttribute
//=========================================================================

bool XercesSerializer::GetAttribute(const char *attr_name, string &value, bool empty)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value
	const XMLCh *xml_value = XMLUtils::GetAttributeValue(this->act_element, attr_name);

	// check if value is empty
	if (!empty && (!xml_value || !*xml_value))
		return false;

	// return empty value
	if (!xml_value)
	{
		value.clear();
		return true;
	}

	// convert to proper type
	value = StrX(xml_value).GetLocal();
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, byte_t &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	value = (byte_t)atoi(str_value.c_str());
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, ushort_t &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	value = (ushort_t)atoi(str_value.c_str());
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, uint_t &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	value = (uint_t)atol(str_value.c_str());
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, short &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	value = (short)atoi(str_value.c_str());
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, int &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	value = atoi(str_value.c_str());
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, long &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	value = atol(str_value.c_str());
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, float &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	value = (float)atof(str_value.c_str());
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, double &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	value = atof(str_value.c_str());
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, bool &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	return this->StringToBool(str_value, value);
}


bool XercesSerializer::GetAttribute(const char *attr_name, vector2 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	vector2 vec;
	if (sscanf(str_value.c_str(), "%f;%f", &vec.x, &vec.y) != 2)
		return false;

	value = vec;
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, vector3 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	vector3 vec;
	if (sscanf(str_value.c_str(), "%f;%f;%f", &vec.x, &vec.y, &vec.z) != 3)
		return false;

	value = vec;
	return true;
}


bool XercesSerializer::GetAttribute(const char *attr_name, vector4 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	vector4 vec;
	if (sscanf(str_value.c_str(), "%f;%f;%f;%f", &vec.x, &vec.y, &vec.z, &vec.w) != 4)
		return false;

	value = vec;
	return true;
}


bool XercesSerializer::GetAttributePercent(const char *attr_name, float &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as byte
	byte_t btvalue;
	if (!this->GetAttribute(attr_name, btvalue))
		return false;

	// convert to proper type
	value = float(btvalue) / 100.0f;
	return true;
}


bool XercesSerializer::GetAttributeColor(const char *attr_name, vector3 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	ushort_t r, g, b;
	if (sscanf(str_value.c_str(), "%hu;%hu;%hu", &r, &g, &b) != 3)
		return false;

	value.set(float(r) / 255.0f, float(g) / 255.0f, float(b) / 255.0f);
	return true;
}


bool XercesSerializer::GetAttributeColor(const char *attr_name, vector4 &value)
{
	Assert(attr_name);
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetAttribute(attr_name, str_value, false))
		return false;

	// convert to proper type
	ushort_t r, g, b, a;
	if (sscanf(str_value.c_str(), "%hu;%hu;%hu;%hu", &r, &g, &b, &a) != 4)
		return false;

	value.set(float(r) / 255.0f, float(g) / 255.0f, float(b) / 255.0f, float(a) / 255.0f);
	return true;
}


//=========================================================================
// GetValue
//=========================================================================

bool XercesSerializer::GetValue(string &value, bool empty)
{
	Assert(this->state == SS_DESERIALIZING);

	// get element value
	const XMLCh *xml_value = this->act_element->getTextContent();

	// check if value is empty
	if (!empty && (!xml_value || !*xml_value))
		return false;

	// return empty value
	if (!xml_value)
	{
		value.clear();
		return true;
	}

	// convert to proper type
	value = StrX(xml_value).GetLocal();
	return true;
}


bool XercesSerializer::GetValue(byte_t &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	value = (byte_t)atoi(str_value.c_str());
	return true;
}


bool XercesSerializer::GetValue(ushort_t &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	value = (ushort_t)atoi(str_value.c_str());
	return true;
}


bool XercesSerializer::GetValue(uint_t &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	value = (uint_t)atol(str_value.c_str());
	return true;
}


bool XercesSerializer::GetValue(short &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	value = (short)atoi(str_value.c_str());
	return true;
}


bool XercesSerializer::GetValue(int &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	value = atoi(str_value.c_str());
	return true;
}


bool XercesSerializer::GetValue(long &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	value = atol(str_value.c_str());
	return true;
}


bool XercesSerializer::GetValue(float &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	value = (float)atof(str_value.c_str());
	return true;
}


bool XercesSerializer::GetValue(double &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	value = atof(str_value.c_str());
	return true;
}


bool XercesSerializer::GetValue(bool &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	return this->StringToBool(str_value, value);
}


bool XercesSerializer::GetValue(vector2 &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	vector2 vec;
	if (sscanf(str_value.c_str(), "%f;%f", &vec.x, &vec.y) != 2)
		return false;

	value = vec;
	return true;
}


bool XercesSerializer::GetValue(vector3 &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	vector3 vec;
	if (sscanf(str_value.c_str(), "%f;%f;%f", &vec.x, &vec.y, &vec.z) != 3)
		return false;

	value = vec;
	return true;
}


bool XercesSerializer::GetValue(vector4 &value)
{
	Assert(this->state == SS_DESERIALIZING);

	// get attribute value as string
	string str_value;
	if (!this->GetValue(str_value, false))
		return false;

	// convert to proper type
	vector4 vec;
	if (sscanf(str_value.c_str(), "%f;%f;%f;%f", &vec.x, &vec.y, &vec.z, &vec.w) != 4)
		return false;

	value = vec;
	return true;
}


//=========================================================================
// Universal methods
//=========================================================================

bool XercesSerializer::CheckGroupName(const char *name)
{
	return XMLString::compareString(this->act_element->getTagName(), StrX(name).GetUnicode()) == 0;
}


ushort_t XercesSerializer::GetGroupsCount()
{
	return XMLUtils::GetSubElementsCount(this->act_element);
}


/**
*/
void XercesSerializer::EndGroup()
{
	Assert(this->group_level);

	this->act_element = (DOMElement *)this->act_element->getParentNode();
	this->group_level--;
}


// vim:ts=4:sw=4:
