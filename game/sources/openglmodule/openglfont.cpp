//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openglfont.cpp
	@ingroup OpenGL_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added support for shadow.
*/


//=========================================================================
// Includes
//=========================================================================

#include "openglmodule/openglfont.h"
#include "openglmodule/openglgfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"

PrepareClass(OpenGLFont, "Font", s_NewOpenGLFont, s_InitOpenGLFont);


//=========================================================================
// OpenGLFont
//=========================================================================

/**
*/
OpenGLFont::OpenGLFont(const char *id) :
	Font(id),
	glid(NULL),
	list_base(0),
	data_size(0)
{
	this->gl_gfx_server = (OpenGLGfxServer *)GfxServer::GetInstance();
	Assert(this->gl_gfx_server.GetUnsafe());
}


/**
*/
OpenGLFont::~OpenGLFont()
{
	this->Unload();
	SafeDelete(this->data_file);
}


/**
	Load the texture resource.
*/
bool OpenGLFont::LoadResource()
{
	Assert(!this->loaded);

	if (!this->gl_gfx_server->IsDisplayOpened())
		return false;

	FontFile *font_file;

	// create and load font
	if (this->data_file)
		font_file = (FontFile *)this->data_file;
	else
	{
		Assert(!this->file_name.empty());
		font_file = this->gl_gfx_server->NewFontFile(this->file_name, true);
		if (!font_file)
			return false;

		font_file->SetCharSize(this->char_size);

		if (!font_file->LoadData())
		{
			delete font_file;
			return false;
		}
	}

	this->glyphs_count = font_file->GetGlyphsCount();
	this->height = 0;

	// generate call list and textures
	glEnable(GL_TEXTURE_2D);
	this->list_base = glGenLists(this->glyphs_count);
	this->glid = NEW GLuint[this->glyphs_count];
	glGenTextures(this->glyphs_count, this->glid);

	// load all glyphs
	for (ulong_t i = 0; i < this->glyphs_count; i++)
	{
		if (!this->LoadGlyph(font_file, i))
		{
			delete font_file;
			glDeleteLists(this->list_base, this->glyphs_count);
			glDeleteTextures(this->glyphs_count, this->glid);
			SafeDelete(this->glid);
			return false;
		}
	}

	// we dont need font file more
	if (!this->data_file)
		delete font_file;

	this->loaded = true;
	return true;
}


bool OpenGLFont::LoadGlyph(FontFile *font_file, ulong_t char_code)
{
	// read image data
	byte_t *data = font_file->GetData(char_code);
	if (!data)
		return false;

	// get extension glyph data
	const FontFile::GlyphData &glyph_data = font_file->GetGlyphData();

	int width = glyph_data.width + this->shadow_x;
	int height = glyph_data.height + this->shadow_y;
	GLubyte *expanded_data = data;

	// get maximal height
	if (height > this->height)
		this->height = height;

	// resample image size to nearest 2^x, 2^y size if non-POT textures are not supported
	if (!this->gl_gfx_server->IsExtensions(GfxServer::EXT_NPOT_TEXTURES))
	{
		width = n_next_p2(width);
		height = n_next_p2(height);
	}

	// create image buffer
	ulong_t data_size = 2 * width * height;
	expanded_data = NEW GLubyte[data_size];
	memset(expanded_data, 0, data_size);

	// render shadow
	int glyph_x, glyph_y;
	if (this->shadow_x || this->shadow_y)
	{
		int exp_x, exp_y;
		for (glyph_y = 0, exp_y = this->shadow_y; glyph_y < glyph_data.height; glyph_y++, exp_y++)
		{
			for (glyph_x = 0, exp_x = this->shadow_x; glyph_x < glyph_data.width; glyph_x++, exp_x++)
				expanded_data[2 * (exp_y * width + exp_x) + 1] = GLubyte(data[glyph_y * glyph_data.width + glyph_x] * this->shadow_alpha);
		}
	}

	// render character
	GLubyte glyph_pix;
	GLubyte *exp_pix;
	GLubyte *exp_pix_next;

	for (glyph_y = 0; glyph_y < glyph_data.height; glyph_y++)
	{
		for (glyph_x = 0; glyph_x < glyph_data.width; glyph_x++)
		{
			glyph_pix = data[glyph_y * glyph_data.width + glyph_x];
			if (glyph_pix)
			{
				exp_pix = expanded_data + 2 * (glyph_y * width + glyph_x);
				exp_pix_next = exp_pix + 1;

				*exp_pix = GLubyte(255 - (255 - glyph_pix) * (*exp_pix_next / 255.0f));
				*exp_pix_next = GLubyte(*exp_pix_next + (255 - *exp_pix_next) * (glyph_pix / 255.0f));
			}
		}
	}

	// create OpenGL texture
	glBindTexture(GL_TEXTURE_2D, this->glid[char_code]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glTexImage2D(
		GL_TEXTURE_2D, 
		0, GL_RGBA, width, height,
		0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, expanded_data
	);

	// delete data
	delete[] expanded_data;

	// create the display list
	glNewList(this->list_base + char_code, GL_COMPILE);

	glBindTexture(GL_TEXTURE_2D, this->glid[char_code]);

	glTranslated(glyph_data.left, 0.0, 0.0);
	glPushMatrix();
	glTranslatef(0.0f, GLfloat(glyph_data.top - glyph_data.height), 0.0f);

	float w = float(glyph_data.width + this->shadow_x);
	float h = float(glyph_data.height + this->shadow_y);
	float x = w / width;
	float y = h / height;

	glBegin(GL_QUADS);
		glTexCoord2d(0, 0);  glVertex2f(0, h);
		glTexCoord2d(0, y);  glVertex2f(0, 0);
		glTexCoord2d(x, y);  glVertex2f(w, 0);
		glTexCoord2d(x, 0);  glVertex2f(w, h);
	glEnd();

	glPopMatrix();
	glTranslated(glyph_data.advance_x, 0, 0);
	glEndList();

	// compute data size
	this->data_size += data_size;

	return true;
}


/**
	Unload everything.
*/
void OpenGLFont::UnloadResource()
{
	Assert(this->loaded);

	// delete call lists and textures
	glDeleteLists(this->list_base, this->glyphs_count);
	glDeleteTextures(this->glyphs_count, this->glid);
	SafeDelete(this->glid);
	this->glyphs_count = 0;
	this->data_size = 0;

	this->loaded = false;
}


/**
	Prints given text.
*/
void OpenGLFont::Print(const string &text) const
{
	// save current OpenGL settings
	this->gl_gfx_server->PushScreenCoordinates();
	glPushAttrib(GL_LIST_BIT | GL_CURRENT_BIT  | GL_ENABLE_BIT | GL_TRANSFORM_BIT);

	// set OpenGL settings we need
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glListBase(this->list_base);

	// draw text
	glCallLists(GLsizei(text.length()), GL_UNSIGNED_BYTE, text.c_str());

	// restore OpenGL settings
	glPopAttrib();
	this->gl_gfx_server->PopScreenCoordinates();
}


ulong_t OpenGLFont::GetDataSize()
{
	return this->data_size;
}


// vim:ts=4:sw=4:
