//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openglgfxserver.cpp
	@ingroup OpenGL_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Rendering quads, color mask.
*/


//=========================================================================
// Includes
//=========================================================================

#include "openglmodule/openglgfxserver.h"
#include "framework/resourceserver.h"
#include "kernel/logserver.h"
#include "kernel/kernelserver.h"

PrepareScriptClass(OpenGLGfxServer, "GfxServer", s_NewOpenGLGfxServer, s_InitOpenGLGfxServer, s_InitOpenGLGfxServer_cmds);


//=========================================================================
// OpenGLGfxServer
//=========================================================================

/**
	Constructor.
*/
OpenGLGfxServer::OpenGLGfxServer(const char *id) :
	GfxServer(id)
{
	// empty
}


/**
	Writes basic gfx information to log file.
*/
void OpenGLGfxServer::WriteInfo()
{
	LogInfo("--- OpenGL info ---");
	LogInfo1("Version:     %s", (char *)glGetString(GL_VERSION));
	LogInfo1("Vendor:      %s", (char *)glGetString(GL_VENDOR));
	LogInfo1("Renderer:    %s", (char *)glGetString(GL_RENDERER));
}


/**
	Creates a shared texture resource object.
*/
Texture *OpenGLGfxServer::NewTexture(const string &file_name)
{
    return (Texture *)this->resource_server->NewResource("OpenGLTexture", file_name, Resource::RES_TEXTURE);
}


/**
	Creates a shared texture resource object with overlay image.
*/
Texture *OpenGLGfxServer::NewTexture(const string &file_name, const string &overlay_name, const vector3 &overlay_color)
{
	if (overlay_name.empty())
		return NewTexture(file_name);

	// generate modifier
	char modifier[10];
	vector3 color = overlay_color * 255.0f;
	sprintf(modifier, "%03d%03d%03d", byte_t(color.x), byte_t(color.y), byte_t(color.z));

	Texture *texture = (Texture *)this->resource_server->NewResource("OpenGLTexture", file_name, Resource::RES_TEXTURE, modifier);

	if (texture && !overlay_name.empty())
	{
		texture->SetOverlayFile(overlay_name);
		texture->SetOverlayColor(overlay_color);
	}

    return texture;
}


/**
	Creates a shared texture resource object using given image file.
*/
Texture *OpenGLGfxServer::NewTexture(ImageFile *image_file)
{
	return (Texture *)this->resource_server->NewResource("OpenGLTexture", image_file, Resource::RES_TEXTURE);
}


/**
	Creates a shared font resource object.
*/
Font *OpenGLGfxServer::NewFont(const string &file_name, const FontFile::CharSize &char_size)
{
	Font *font = (Font *)this->resource_server->NewResource("OpenGLFont", file_name, Resource::RES_FONT);

	if (font)
		font->SetCharSize(char_size);

    return font;
}


void OpenGLGfxServer::SetProjection(const Projection& projection)
{
	Assert(this->display_opened);

	// set projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	switch (projection.type) {
	case Projection::PT_ORTHO:
		glOrtho(projection.left, projection.right, projection.bottom, projection.top, projection.front, projection.back);
		break;

	case Projection::PT_ORTHO_2D:
		gluOrtho2D(projection.left, projection.right, projection.bottom, projection.top);
		break;

	default:
		Error();
		break;
	}

	glTranslatef(projection.translation[0], projection.translation[1], projection.translation[2]);

	// reset model settings
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


void OpenGLGfxServer::SetViewport(const Viewport& view_port)
{
	Assert(this->display_opened);

	// set viewport
	glViewport(view_port.x, view_port.y, view_port.width, view_port.height);
}


/**
	Pushes a projection matrix that will make object world coordinates identical to window coordinates.
*/
void OpenGLGfxServer::PushScreenCoordinates()
{
	// save transform
	glPushAttrib(GL_TRANSFORM_BIT);

	// get viewport
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);

	// save old projection
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();

	// set new projection
	glLoadIdentity();
	gluOrtho2D(0, this->GetWindowSize().width, -this->GetWindowSize().height, 0);

	// restore transform
	glPopAttrib();
}


/**
	Pops the projection matrix without changing the current.
*/
void OpenGLGfxServer::PopScreenCoordinates()
{
	// save transform
	glPushAttrib(GL_TRANSFORM_BIT);

	// restore old projection
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	// restore transform
	glPopAttrib();
}


bool OpenGLGfxServer::SaveScreenshot(const string &file_name)
{
	Assert(this->display_opened);

	LogInfo1("Saving screenshot to '%s'", file_name.c_str());

	const DisplayMode::WidowSize &wnd_size = this->GetWindowSize();

	ulong_t size = wnd_size.width * wnd_size.height * ImageFile::GetBytesPerPixel(ImageFile::IF_RGB);
	byte_t *data = NEW byte_t[size];

	glPixelStorei(GL_PACK_ALIGNMENT, 1);
	glReadBuffer(GL_FRONT);
	glReadPixels(0, 0, wnd_size.width, wnd_size.height, GL_RGB, GL_UNSIGNED_BYTE, data);

	return this->SaveImage(file_name, data, ImageFile::IF_RGB, wnd_size.width, wnd_size.height);
}


void OpenGLGfxServer::ResetMatrix()
{
	Assert(this->display_opened);
	glLoadIdentity();
}


void OpenGLGfxServer::PushMatrix()
{
	Assert(this->display_opened);
	glPushMatrix();
}


void OpenGLGfxServer::PopMatrix()
{
	Assert(this->display_opened);
	glPopMatrix();
}


void OpenGLGfxServer::Translate(const vector2 &translation)
{
	Assert(this->display_opened);
	glTranslatef(translation.x, translation.y, 0.0f);
}


void OpenGLGfxServer::Translate(const vector3 &translation)
{
	Assert(this->display_opened);
	glTranslatef(translation.x, translation.y, translation.z);
}


void OpenGLGfxServer::Rotate(float rotation)
{
	Assert(this->display_opened);
	glRotatef(rotation, 0, 1, 0);
}


void OpenGLGfxServer::Rotate(const vector3 &rotation)
{
	Assert(this->display_opened);
	glRotatef(rotation.x, 1, 0, 0);
	glRotatef(rotation.y, 0, 1, 0);
	glRotatef(rotation.z, 0, 0, 1);
}


/**
	Sets values for clearing buffers.

	@param  red             The red value to write into the color buffer.
	@param  green           The green value to write into the color buffer.
	@param  blue            The blue value to write into the color buffer.
	@param  alpha           The alpha value to write into the color buffer.
	@param  depth           The z value to write into the depth buffer.
	@param  stencil         The stencil value to write into the stencil buffer.
*/
void OpenGLGfxServer::SetClearValues(float red, float green, float blue, float alpha, float depth, int stencil)
{
	GfxServer::SetClearValues(red, green, blue, alpha, depth, stencil);

	glClearColor(red, green, blue, alpha);
	glClearDepth(depth);
	glClearStencil(stencil);
}


inline
void OpenGLGfxServer::SetColorWithEffect(float r, float g, float b, float alpha)
{
	glColor4f(r * color_filter.x, g * color_filter.y, b * color_filter.z, alpha * color_filter.w);
}


/**
	Sets current color.

	@param color  Color to be set (R, G, B).
*/
void OpenGLGfxServer::SetColor(const vector3 &color)
{
	Assert(this->display_opened);
	
	this->SetColorWithEffect(color.x, color.y, color.z);
}


/**
	Sets current color.

	@param color  Color to be set (R, G, B, aplha).
*/
void OpenGLGfxServer::SetColor(const vector4 &color)
{
	Assert(this->display_opened);

	this->SetColorWithEffect(color.x, color.y, color.z, color.w);
}


/**
	Sets current color mask.
*/
void OpenGLGfxServer::SetColorMask(bool r, bool g, bool b, bool alpha)
{
	Assert(this->display_opened);

	glColorMask(r, g, b, alpha);
}


void OpenGLGfxServer::SetBlending(bool blending)
{
	GfxServer::SetBlending(blending);

	if (blending)
	{
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
	}
	else
		glDisable(GL_BLEND);
}


void OpenGLGfxServer::SetDepthTest(bool depth_test)
{
	GfxServer::SetDepthTest(depth_test);

	if (depth_test)
	{
		glDepthFunc(GL_LEQUAL);
		glEnable(GL_DEPTH_TEST);
	}
	else
		glDisable(GL_DEPTH_TEST);
}


/**
	Clears buffers.

	@param  buffers A combination of BufferType flags.
*/
void OpenGLGfxServer::ClearBuffers(byte_t buffers)
{
	Assert(this->display_opened);

	GLbitfield mask = 0;

	if (buffers & BUFFER_COLOR)
		mask |= GL_COLOR_BUFFER_BIT;
	if (buffers & BUFFER_DEPTH)
		mask |= GL_DEPTH_BUFFER_BIT;
	if (buffers & BUFFER_STENCIL)
		mask |= GL_STENCIL_BUFFER_BIT;

	glClear(mask);
}


void OpenGLGfxServer::RenderShape()
{
	Assert(this->display_opened);

	// apply texture
	if (this->texture)
	{
		glEnable(GL_TEXTURE_2D);
		texture->Apply();
	}
	else
		glDisable(GL_TEXTURE_2D);

	// reset color
	this->SetColorWithEffect(1.0f, 1.0f, 1.0f);
	
	// variables
	ushort_t i, j;
	vector2 *coords = this->coords_buff ? this->coords_buff : this->mesh->GetCoords();
	vector4 *colors = this->colors_buff ? this->colors_buff : this->mesh->GetColors();

	Primitive *primitives = this->mesh->GetPrimitives();
	Primitive *primitive;

	ushort_t *indices, index;

	// go through all primitives
	for (i = 0; i < this->mesh->GetPrimitivesCount(); i++)
	{
		primitive = primitives + i;

		// begin primitive
		switch (primitive->GetType())
		{
		case Primitive::PT_QUADS:       glBegin(GL_QUADS); break;
		case Primitive::PT_STRIP:       glBegin(GL_TRIANGLE_STRIP); break;
		case Primitive::PT_FAN:         glBegin(GL_TRIANGLE_FAN); break;
		case Primitive::PT_TRIANGLES: 
		default:                        glBegin(GL_TRIANGLES); break;
		}

		indices = primitive->GetIndices();

		// go through all indices
		for (j = 0; j < primitive->GetIndicesCount(); j++)
		{
			index = indices[j];

			// apply vertex color
			if (colors)
				this->SetColorWithEffect(colors[index].x, colors[index].y, colors[index].z, colors[index].w);

			// apply texture coords
			if (this->mapping && this->texture)
				glTexCoord2f(this->mapping[index].x, this->mapping[index].y);

			// apply vertex
		    glVertex2d(coords[index].x, coords[index].y);
		}

		// end primitive
		glEnd();
	}

	this->ResetRenderObjects();
}


void OpenGLGfxServer::RenderPrimitive(PrimitiveType primitive, const vector2 *vertices, uint_t count)
{
	Assert(this->display_opened);
	Assert(primitive < PRIMITIVE_COUNT);

	uint_t mapping_count = count;

	// apply texture
	if (this->texture && this->mapping)
	{
		glEnable(GL_TEXTURE_2D);
		texture->Apply();
	}
	else
		glDisable(GL_TEXTURE_2D);

	// set primitive
	switch (primitive)
	{
	case PRIMITIVE_POINTS:         glBegin(GL_POINTS); break;
	case PRIMITIVE_LINES:          glBegin(GL_LINES); break;
	case PRIMITIVE_LINE_STRIP:     glBegin(GL_LINE_STRIP); break;
	case PRIMITIVE_LINE_LOOP:      glBegin(GL_LINE_LOOP); break;
	case PRIMITIVE_TRIANGLE_STRIP: glBegin(GL_TRIANGLE_STRIP); break;
	case PRIMITIVE_TRIANGLE_FAN:   glBegin(GL_TRIANGLE_FAN); break;
	case PRIMITIVE_QUADS:          glBegin(GL_QUADS); break;
	case PRIMITIVE_QUAD_STRIP:     glBegin(GL_QUAD_STRIP); break;
	case PRIMITIVE_POLYGON:        glBegin(GL_POLYGON); break;
	case PRIMITIVE_TRIANGLES:
	default:                       glBegin(GL_TRIANGLES); break;
	}

	// draw primitive
	byte_t mapping_id;
	const vector2 *vertex = vertices;

	for (uint_t i = 0; i < count; i++, vertex++)
	{
		if (this->texture && this->mapping)
		{
			mapping_id = i % mapping_count;
			glTexCoord2f(this->mapping[mapping_id].x, this->mapping[mapping_id].y);
		}
		glVertex2d(vertex->x, vertex->y);
	}
	glEnd();

	this->ResetRenderObjects();
}


void OpenGLGfxServer::Print(const vector2 &position, const string &text)
{
	Assert(this->display_opened);

	// save settings
	glPushAttrib(GL_TRANSFORM_BIT);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	// set new position
	glLoadIdentity();
	glTranslatef(position.x, -position.y, 0.0f);

	// draw text
	GfxServer::Print(text);

	// restore settings
	glPopMatrix();
	glPopAttrib();
}


void OpenGLGfxServer::ReadPixels(ushort_t x, ushort_t y, ushort_t width, ushort_t height, byte_t *pixels)
{
	Assert(this->display_opened);

	glReadBuffer(GL_BACK);
	glReadPixels(x, y, width, height, GL_RGB, GL_UNSIGNED_BYTE, pixels);
}


// vim:ts=4:sw=4:
