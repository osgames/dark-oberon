//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file opengltexture.cpp
	@ingroup OpenGL_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "openglmodule/opengltexture.h"
#include "openglmodule/openglgfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"

PrepareClass(OpenGLTexture, "Texture", s_NewOpenGLTexture, s_InitOpenGLTexture);


//=========================================================================
// OpenGLTexture
//=========================================================================

/**
*/
OpenGLTexture::OpenGLTexture(const char *id) :
	Texture(id),
	glid(0),
	data_size(0)
{
	this->gl_gfx_server = (OpenGLGfxServer *)GfxServer::GetInstance();
	Assert(this->gl_gfx_server.GetUnsafe());
}


/**
*/
OpenGLTexture::~OpenGLTexture()
{
	this->Unload();
	SafeDelete(this->data_file);
}


/**
	Load the texture resource.
*/
bool OpenGLTexture::LoadResource()
{
	Assert(!this->loaded);

	if (!this->gl_gfx_server->IsDisplayOpened())
	{
		LogError("Display must be opened before loading OpenGL texture");
		return false;
	}

	ImageFile *image_file, *overlay_file;

	// create and load image
	if (this->data_file)
		image_file = (ImageFile *)this->data_file;
	else
	{
		Assert(!this->file_name.empty());
		image_file = this->gl_gfx_server->NewImageFile(this->file_name, true);
		if (!image_file)
			return false;

		if (!image_file->LoadData())
		{
			delete image_file;
			return false;
		}
	}

	// process overlay image
	if (!this->overlay_file_name.empty())
	{
		// create and load overlay image
		overlay_file = this->gl_gfx_server->NewImageFile(this->overlay_file_name, true);
		if (!overlay_file)
		{
			delete image_file;
			return false;
		}

		if (!image_file->AddOverlay(overlay_file, this->overlay_color))
		{
			LogError1("Error adding overlay image '%s'", this->overlay_file_name.c_str());
			delete overlay_file;
			delete image_file;
			return false;
		}

		delete overlay_file;
	}

	// save original dimensions dimensions
	this->original_width = image_file->GetWidth();
	this->original_height = image_file->GetHeight();

	// resample image to nearest 2^x, 2^y size if non-POT textures are not supported
	if (!this->gl_gfx_server->IsExtensions(GfxServer::EXT_NPOT_TEXTURES))
	{
		// compute POT size
		ushort_t width_p2 = n_next_p2(image_file->GetWidth());
		ushort_t height_p2 = n_next_p2(image_file->GetHeight());

		// resample if image is non-POT
		if (width_p2 != image_file->GetWidth() || height_p2 != image_file->GetHeight())
			image_file->Resample(width_p2, height_p2);
	}

	// save final dimensions
	this->width = image_file->GetWidth();
	this->height = image_file->GetHeight();
	
	// generate texture
	glEnable(GL_TEXTURE_2D);

	glGenTextures(1, &this->glid);
	glBindTexture(GL_TEXTURE_2D, this->glid);

	// set properties
	switch (image_file->GetFormat())
	{
	case ImageFile::IF_LUMINANCE: this->format = GL_LUMINANCE; break;
	case ImageFile::IF_RGBA:      this->format = GL_RGBA; break;
	case ImageFile::IF_RGB:
	default:                      this->format = GL_RGB; break;
	}

	// set texture filtering
	GLenum min_filter, mag_filter, mm_filter;

	mm_filter = GL_NONE;
	if (this->allow_mipmap)
	{
		switch (this->gl_gfx_server->GetMipMapFilter())
		{
		case GfxServer::FILTER_LINEAR:  mm_filter = GL_LINEAR; break;
		case GfxServer::FILTER_NEAREST: mm_filter = GL_NEAREST; break;
		case GfxServer::FILTER_NONE:
		default:                        mm_filter = GL_NONE; break;
		}
	}

	this->mipmap = (mm_filter != GL_NONE);

	switch (this->gl_gfx_server->GetMinMagFilter())
	{
	case GfxServer::FILTER_LINEAR:
		min_filter = this->mipmap ? (mm_filter == GL_NEAREST ? GL_LINEAR_MIPMAP_NEAREST : GL_LINEAR_MIPMAP_LINEAR) : GL_LINEAR;
		mag_filter = GL_LINEAR;
		break;

	case GfxServer::FILTER_NEAREST:
	default:
		min_filter = this->mipmap ? (mm_filter == GL_NEAREST ? GL_NEAREST_MIPMAP_NEAREST : GL_NEAREST_MIPMAP_LINEAR) : GL_NEAREST;
		mag_filter = GL_NEAREST;
		break;
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag_filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min_filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	// upload to memory
	if (this->mipmap)
		gluBuild2DMipmaps(GL_TEXTURE_2D, this->format, this->width, this->height, this->format, GL_UNSIGNED_BYTE, (void *)image_file->GetData());		
	else
		glTexImage2D(GL_TEXTURE_2D, 0, this->format, this->width, this->height, 0, this->format, GL_UNSIGNED_BYTE, (void *)image_file->GetData());
		

	// compute data size
	this->data_size = image_file->GetDataSize();
    if (mm_filter != GL_NONE)
	{
		this->data_size *= 5;
        this->data_size /= 4;
    }

	// we dont need image file more
	if (!this->data_file)
		delete image_file;

	this->loaded = true;
	return true;
}


/**
	Unload everything.
*/
void OpenGLTexture::UnloadResource()
{
	Assert(this->loaded);

	// delete texture
	glDeleteTextures(1, &this->glid);
	this->glid = 0;
	this->data_size = 0;

	this->loaded = false;
}


/**
	Activates texture for drawing.
*/
void OpenGLTexture::Apply()
{
	glBindTexture(GL_TEXTURE_2D, this->glid);
}


/**
	Replaces image data.
*/
void OpenGLTexture::ReplaceData(byte_t *data)
{
	Assert(data);

	glBindTexture(GL_TEXTURE_2D, this->glid);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, this->width, this->height, this->format, GL_UNSIGNED_BYTE, (void *)data);
}


ulong_t OpenGLTexture::GetDataSize()
{
	return this->data_size;
}


// vim:ts=4:sw=4:
