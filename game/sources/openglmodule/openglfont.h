#ifndef __openglfont_h__
#define __openglfont_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openglfont.h
	@ingroup OpenGL_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added support for shadow.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/ref.h"
#include "framework/font.h"
#include "openglmodule/includegl.h"

class OpenGLGfxServer;


//=========================================================================
// OpenGLFont
//=========================================================================

/**
    @class OpenGLFont
    @ingroup OpenGL_Module
*/

class OpenGLFont : public Font
{
//--- variables
protected:
	Ref<OpenGLGfxServer> gl_gfx_server;
	GLuint *glid;
	GLuint list_base;
	ulong_t data_size;

//--- methods
public:
	OpenGLFont(const char *id);
	virtual ~OpenGLFont();

	virtual ulong_t GetDataSize();
	virtual void Print(const string &text) const;

protected:
	virtual bool LoadResource();
	virtual void UnloadResource();

	bool LoadGlyph(FontFile *font_file, ulong_t char_code);
};


#endif // __openglfont_h__

// vim:ts=4:sw=4:
