//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openglmodule.cpp
	@ingroup OpenGL_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"


//=========================================================================
// OpenGLModule
//=========================================================================

extern "C" void OpenGLModule();


extern bool s_InitOpenGLGfxServer (Class *, KernelServer *);
extern Object *s_NewOpenGLGfxServer (const char *name);
extern bool s_InitOpenGLTexture (Class *, KernelServer *);
extern Object *s_NewOpenGLTexture (const char *name);
extern bool s_InitOpenGLFont (Class *, KernelServer *);
extern Object *s_NewOpenGLFont (const char *name);


void OpenGLModule()
{
	KernelServer::GetInstance()->RegisterClass("OpenGLGfxServer", s_InitOpenGLGfxServer, s_NewOpenGLGfxServer);
	KernelServer::GetInstance()->RegisterClass("OpenGLTexture", s_InitOpenGLTexture, s_NewOpenGLTexture);
	KernelServer::GetInstance()->RegisterClass("OpenGLFont", s_InitOpenGLFont, s_NewOpenGLFont);
}

// vim:ts=4:sw=4:
