#ifndef __includegl_h__
#define __includegl_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file includegl.h
	@ingroup OpenGL_Module

 	@author Marcus Geelnard, Peter Knut
	@date 2005

	Includes OpenGL header files with all system/compiler specifics.

	@version 1.0 - Initial.
*/


#include "framework/system.h"

#ifndef OPENGL_INCLUDED
#define OPENGL_INCLUDED 1

// Windows needs APIENTRY for <GL/gl.h>
#if !defined(APIENTRY) && defined(WINDOWS)
#	define APIENTRY __stdcall
#	define GL_APIENTRY_DEFINED
#endif // APIENTRY


// The following three defines are here solely to make some Windows-based
// <GL/gl.h> files happy. Theoretically we could include <windows.h>, but
// it has the major drawback of severely polluting our namespace.

// Under Windows, we need WINGDIAPI defined
#if !defined(WINGDIAPI) && defined(WINDOWS)
#	if defined(_MSC_VER) || defined(__BORLANDC__) || defined(__POCC__)
		// Microsoft Visual C++, Borland C++ Builder and Pelles C
#		define WINGDIAPI __declspec(dllimport)
#	elif defined(__LCC__)
		// LCC-Win32
#		define WINGDIAPI __stdcall
#	else
		// Others (e.g. MinGW, Cygwin)
#		define WINGDIAPI extern
#	endif
#	define GL_WINGDIAPI_DEFINED
#endif // WINGDIAPI


// Some <GL/glu.h> files also need CALLBACK defined
#if !defined(CALLBACK) && defined(WINDOWS)
	#if defined(_MSC_VER)
		// Microsoft Visual C++
		#if (defined(_M_MRX000) || defined(_M_IX86) || defined(_M_ALPHA) || defined(_M_PPC)) && !defined(MIDL_PASS)
			#define CALLBACK __stdcall
		#else
			#define CALLBACK
		#endif
	#else
		// Other Windows compilers
		#define CALLBACK __stdcall
	#endif
	#define GLU_CALLBACK_DEFINED
#endif // CALLBACK


// Microsoft Visual C++, Borland C++ and Pelles C <GL/glu.h> needs wchar_t
#if defined(WINDOWS) && (defined(_MSC_VER) || defined(__BORLANDC__) || defined(__POCC__)) && !defined(_WCHAR_T_DEFINED)
	typedef unsigned short wchar_t;
	#define _WCHAR_T_DEFINED
#endif // _WCHAR_T_DEFINED


// Include standard OpenGL headers
#if defined(__APPLE_CC__)
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#else
	#include <GL/gl.h>
	#include <GL/glu.h>
#endif

// undef all system macros
#undef APIENTRY
#ifdef WINDOWS
#	undef WINGDIAPI
#	undef CALLBACK
#endif

#endif // OPENGL_INCLUDED

// Include OpenGL extensions
#include "openglmodule/glext.h"

#endif  // __includegl_h__

// vim:ts=4:sw=4:
