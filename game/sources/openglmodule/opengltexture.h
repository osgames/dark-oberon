#ifndef __opengltexture_h__
#define __opengltexture_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file opengltexture.h
	@ingroup OpenGL_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/ref.h"
#include "framework/texture.h"
#include "openglmodule/includegl.h"

class OpenGLGfxServer;


//=========================================================================
// OpenGLTexture
//=========================================================================

/**
    @class OpenGLTexture
    @ingroup OpenGL_Module
*/

class OpenGLTexture : public Texture {
//--- variables
protected:
	Ref<OpenGLGfxServer> gl_gfx_server;
	GLuint glid;
	GLenum format;
	ulong_t data_size;

//--- methods
public:
	OpenGLTexture(const char *id);
	virtual ~OpenGLTexture();

	virtual ulong_t GetDataSize();
	virtual void Apply();
	virtual void ReplaceData(byte_t *data);

protected:
	virtual bool LoadResource();
	virtual void UnloadResource();
};


#endif // __opengltexture_h__

// vim:ts=4:sw=4:
