#ifndef __openglgfxserver_h__
#define __openglgfxserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openglgfxserver.h
	@ingroup OpenGL_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Rendering primitives, color mask.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/gfxserver.h"
#include "openglmodule/includegl.h"
#include "openglmodule/opengltexture.h"


//=========================================================================
// OpenGLGfxServer
//=========================================================================

/**
	@class OpenGLGfxServer
	@ingroup OpenGL_Module

	@brief OpenAL audio server.
*/

class OpenGLGfxServer : public GfxServer
{
//--- methods
public:
	OpenGLGfxServer(const char *id);

    virtual Texture *NewTexture(const string &file_name);
	virtual Texture *NewTexture(const string &file_name, const string &overlay_name, const vector3 &overlay_color);
	virtual Texture *NewTexture(ImageFile *image_file);
	virtual Font *NewFont(const string &file_name, const FontFile::CharSize &char_size);
	virtual void WriteInfo();

	virtual void SetProjection(const Projection& projection);
	virtual void SetViewport(const Viewport& view_port);
	virtual void PushScreenCoordinates();
	virtual void PopScreenCoordinates();

	void SetMinMagFilters(GLint min_filter, GLint mag_filter);
	void SetMipMapFilter(GLint mm_filter);

	virtual bool SaveScreenshot(const string &file_name);

	// drawing
	virtual void ResetMatrix();
	virtual void PushMatrix();
	virtual void PopMatrix();

	virtual void Translate(const vector2 &translation);
	virtual void Translate(const vector3 &translation);
	virtual void Rotate(float rotation);
	virtual void Rotate(const vector3 &rotation);

	virtual void SetColor(const vector3 &color);
	virtual void SetColor(const vector4 &color);
	virtual void SetColorMask(bool r, bool g, bool b, bool alpha);
	virtual void SetClearValues(float red, float green, float blue, float alpha = 0.0f, float depth = 0.0f, int stencil = 0);
	virtual void SetBlending(bool blending);
	virtual void SetDepthTest(bool depth_test);

	virtual void ClearBuffers(byte_t buffers);
	virtual void RenderShape();
	virtual void RenderPrimitive(PrimitiveType primitive, const vector2 *vertices, uint_t count);
	virtual void Print(const vector2 &position, const string &text);
	virtual void ReadPixels(ushort_t x, ushort_t y, ushort_t width, ushort_t height, byte_t *pixels);

private:
	void SetColorWithEffect(float r, float g, float b, float alpha = 1.0f);
};


#endif  // __openglgfxserver_h__

// vim:ts=4:sw=4:
