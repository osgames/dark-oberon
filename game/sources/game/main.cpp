//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file main.cpp
	@ingroup Game_Module

 	@author Peter Knut
	@date 2006

	Dark Oberon application.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "engine/engine.h"


//=========================================================================
// main
//=========================================================================

int main(int argc, char* argv[])
{
	Engine *engine = NEW Engine();

	if (!engine->Open(argc, argv))
		return EXIT_FAILURE;

	if (!engine->Run("Game", string("Dark Oberon 1.1.0")))
	{
		engine->Close();
		return EXIT_FAILURE;
	}

	engine->Close();
	return EXIT_SUCCESS;
}


// vim:ts=4:sw=4:
