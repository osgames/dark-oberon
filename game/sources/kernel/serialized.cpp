//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file serialized.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/serialized.h"


//=========================================================================
// Serialized
//=========================================================================

/**
	Constructor.
*/
Serialized::Serialized()
{
	// empty
}


/**
	Destructor.
*/
Serialized::~Serialized()
{
	// empty
}


const char *Serialized::GetGroupName()
{
	return "Serialized";
}


/**
*/
bool Serialized::Serialize(Serializer &serializer)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


/**
*/
bool Serialized::Deserialize(Serializer &serializer, bool first)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


// vim:ts=4:sw=4:
