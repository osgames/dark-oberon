#ifndef __threadserver_h__
#define __threadserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file threadserver.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/condition.h"
#include "kernel/mutex.h"
#include "kernel/thread.h"
#include "kernel/semaphore.h"
#include "kernel/server.h"


//=========================================================================
// ThreadServer
//=========================================================================

/**
	@class ThreadServer
	@ingroup Kernel_Module

	@brief Threads, mutexes, conditional variables.
*/

class ThreadServer : public Server<ThreadServer>
{
	friend class KernelServer;

//--- methods
public:
	virtual ~ThreadServer();

	virtual Mutex *CreateMutex();
	virtual RecMutex *CreateRecMutex();
	virtual Thread *CreateThread();
	virtual Condition *CreateCondition();
	virtual Semaphore *CreateSemaphore();

	virtual int GetThreadID();
	virtual void Sleep(double time);

protected:
	ThreadServer();
};


//=========================================================================
// Methods
//=========================================================================

inline
ThreadServer::ThreadServer() :
	Server<ThreadServer>()
{
	//
}


inline
ThreadServer::~ThreadServer()
{
	//
}


inline
Mutex *ThreadServer::CreateMutex()
{
	return NEW Mutex();
}


inline
RecMutex *ThreadServer::CreateRecMutex()
{
	return NEW RecMutex();
}


inline
Thread *ThreadServer::CreateThread()
{
	return NEW Thread();
}


inline
Condition *ThreadServer::CreateCondition()
{
	return NEW Condition();
}


inline
Semaphore *ThreadServer::CreateSemaphore()
{
	return NEW Semaphore();
}


inline
int ThreadServer::GetThreadID()
{
	return 0;
}


inline
void ThreadServer::Sleep(double time)
{
	//
}


#endif  // __threadserver_h__

// vim:ts=4:sw=4:
