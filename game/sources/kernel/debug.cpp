//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file debug.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/debug.h"
#include "kernel/logserver.h"


//=========================================================================
// Debug methods
//=========================================================================

#ifdef USE_ASSERT
void CallAssert(const char *exp, const char *file, int line, const char *message, ...)
{
	if (message)
	{
		char msg[LOG_MAX_MESSAGE_SIZE];

		va_list args;
		va_start(args, message);

		// process message text
		#ifdef WINDOWS
			_vsnprintf(msg, LOG_MAX_MESSAGE_SIZE, message, args);
		#else
			vsnprintf(msg, LOG_MAX_MESSAGE_SIZE, message, args);
		#endif

		if (LogServer::GetInstance())
			LogCritical4("*** ASSERTION ***\n\nexpression: %s\nfile: %s\nline: %d\nmesg: %s", exp, file, line, msg);

		va_end(args);
	}
	else
	{
		if (LogServer::GetInstance())
			LogCritical3("*** ASSERTION ***\n\nexpression: %s\nfile: %s\nline: %d", exp, file, line);
	}

	abort();
}
#endif

void CallAbort(const char *file, int line, const char *message, ...)
{
	if (message)
	{
		char msg[LOG_MAX_MESSAGE_SIZE];

		va_list args;
		va_start(args, message);

		// process message text
		#ifdef WINDOWS
			_vsnprintf(msg, LOG_MAX_MESSAGE_SIZE, message, args);
		#else
			vsnprintf(msg, LOG_MAX_MESSAGE_SIZE, message, args);
		#endif

		if (LogServer::GetInstance())
			LogCritical3("*** ABORT ***\n\nfile: %s\nline: %d\nmesg: %s", file, line, msg);

		va_end(args);
	}
	else
	{
		if (LogServer::GetInstance())
			LogCritical2("*** ABORT ***\n\nfile: %s\nline: %d", file, line);
	}

	abort();
}

// vim:ts=4:sw=4:
