//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file class.cpp
	@ingroup Kernel_Module

 	@author Andre Weissflog, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <stdlib.h>

#include "kernel/hashlist.h"
#include "kernel/keyarray.h"
#include "kernel/class.h"
#include "kernel/object.h"
#include "kernel/cmdprotonative.h"


//=========================================================================
// Class
//=========================================================================

/**
	Constructor.

	@param name               Name of the class.
	@param kserv              Pointer to kernel server.
	@param init_func           Pointer to s_init function in class package.
	@param new_func            Pointer to s_create function in class package.
*/
Class::Class(const string &name,
				KernelServer *kserv,
				bool (*init_func)(Class *, KernelServer *),
				Object *(*new_func)(const char *name)) :
	HashNode(name),
	kernel_server(kserv),
	super_class(0),
	cmd_list(0),
	cmd_table(0),
	script_cmd_list(0),
	ref_count(0),
	instance_size(0),
	s_init_ptr(init_func),
	s_new_ptr(new_func)
{
	// call the class module's init function
	this->s_init_ptr(this, this->kernel_server);
}


/**
	Destructor.
*/
Class::~Class(void)
{
	// if I'm still connected to a superclass, unlink from superclass
	if (this->super_class)
		this->super_class->RemSubClass(this);

	// make sure we're cleaned up ok
	Assert(!this->super_class);
	Assert(!this->ref_count);

	// release cmd protos
	if (this->cmd_list) {
		CmdProto *cmd_proto;

		while ((cmd_proto = (CmdProto*) this->cmd_list->PopFront()))
			delete cmd_proto;

		delete this->cmd_list;
	}

	if (this->cmd_table)
		delete this->cmd_table;

	if (this->script_cmd_list) {
		CmdProto *cmd_proto;

		while ((cmd_proto = (CmdProto *) this->script_cmd_list->PopFront()))
			delete cmd_proto;

		delete this->script_cmd_list;
	}
}


/**
	Creates a new instance of the class.
*/
Object *Class::NewObject(const char *name)
{
	Object *obj = (Object *) this->s_new_ptr(name);
	Assert(obj);
	obj->SetClass(this);

	return obj;
}


/**
	Start defining commands.
*/
void Class::BeginCmds(void)
{
	Assert(!this->cmd_table);
	Assert(!this->cmd_list);

	// compute an average number of commands, assume an average
	// of 16 commands per subclass
	int cmds_count = 0;
	Class *cl = this;

	do {
		cmds_count += 16;
	} while ((cl = cl->GetSuperClass()));

	this->cmd_list = NEW HashList(cmds_count);
}


/**
	Adds a command to the class.

	@param  cmd_proto    pointer to CmdProto object to be added
*/
void Class::AddCmd(CmdProto * cmd_proto)
{
	Assert(this->cmd_list && cmd_proto);

	this->cmd_list->PushBack(cmd_proto);
}


/**
	Adds a command to the class.

	@param  proto_def   the command's prototype definition
	@param  id          the command's unique fourcc code
	@param  cmd_proc    the command's stub function
*/
void Class::AddCmd(const char *proto_def, fourcc_t id, void (*cmd_proc)(void *, Cmd *))
{
	Assert(proto_def && id && cmd_proc && this->cmd_list);

	CmdProtoNative *cp = NEW CmdProtoNative(proto_def, id, cmd_proc);
	Assert(cp);

	this->AddCmd(cp);
}


/**
	Finishes defining commands.
	Builds sorted array of attached cmds for a bsearch() by ID.
*/
void
Class::EndCmds(void)
{
	Assert(!this->cmd_table && this->cmd_list);

	Class *cl;

	// count commands
	int cmds_count = 0;
	cl = this;

	do {
		if (cl->cmd_list) {
			HashNode *node;
			for (node = cl->cmd_list->GetFront(); node; node = node->GetNext())
				cmds_count++;
		}
	} while ((cl = cl->GetSuperClass()));

	// create and fill command table
	this->cmd_table = NEW KeyArray<CmdProto *>(cmds_count);
	cl = this;

	do {
		if (cl->cmd_list) {
			CmdProto *cp;
			for (cp = (CmdProto *) cl->cmd_list->GetFront(); cp; cp = (CmdProto *) cp->GetNext())
				this->cmd_table->Add(cp->GetId(),cp);
		}
	} while ((cl = cl->GetSuperClass()));

	// check for identical cmd ids
	int i;
	for (i = 0; i < cmds_count - 1; i++) {
		if (this->cmd_table->GetKeyAt(i) == this->cmd_table->GetKeyAt(i + 1)) {
			CmdProto *cp0 = (CmdProto *) this->cmd_table->GetElementAt(i);
			CmdProto *cp1 = (CmdProto *) this->cmd_table->GetElementAt(i + 1);

			ErrorMsg4(
				"Command id collision in class '%s'. Cmd '%s' and cmd '%s' both have id '0x%x'",
				this->GetName(),
				cp0->GetName(),
				cp1->GetName(),
				this->cmd_table->GetKeyAt(i)
			);
		}
	}
}


/**
	Starts defining script-side commands.

	@param cmds_count The number of script cmds that will be defined.
*/
void Class::BeginScriptCmds(int cmds_count)
{
	Assert(!this->script_cmd_list);

	this->script_cmd_list = NEW HashList(cmds_count);
}


/**
	Adds a script-side command for this class.
	Can only be called between Begin/EndScriptCmds().

	@note Script server cmd protos can't be looked up by their 4cc.

	@param cmd_proto    A cmd proto created by a script server.
*/
void Class::AddScriptCmd(CmdProto *cmd_proto)
{
	Assert(cmd_proto && this->script_cmd_list);

	this->script_cmd_list->PushBack(cmd_proto);
}


/**
	Finishes defining script-side commands.
*/
void Class::EndScriptCmds()
{
    // empty
}


/**
	Finds command by name (searches both native & script-side).

	@param  name    name of command to be found
	@return         pointer to CmdProto object, or 0
*/
CmdProto *Class::FindCmdByName(const string &name)
{
    Assert(!name.empty());

    CmdProto *cp = 0;
    // try the native cmd list first
    if (this->cmd_list)
        cp = (CmdProto *) this->cmd_list->Find(name);

    // if that fails try the script cmd list
    if (!cp && this->script_cmd_list)
        cp = (CmdProto *) this->script_cmd_list->Find(name);

    // if not found or no command list, recursively hand up to parent class
    if (!cp && this->super_class)
        cp = this->super_class->FindCmdByName(name);

    return cp;
}


/**
	Finds a native command by name.

	@param name The name of the command to be found
*/
CmdProtoNative *Class::FindNativeCmdByName(const string &name)
{
    Assert(!name.empty());
    CmdProtoNative *cp = 0;

    if (this->cmd_list)
        cp = (CmdProtoNative *) this->cmd_list->Find(name);

    // if not found, recursively hand up to parent class
    if (!cp && this->super_class)
        cp = this->super_class->FindNativeCmdByName(name);

    return cp;
}


/**
	Finds a script-side command by name.

	@param name The name of the command to be found
*/
CmdProto *Class::FindScriptCmdByName(const string &name)
{
	Assert(!name.empty());
	CmdProto *cp = 0;

	if (this->script_cmd_list)
		cp = (CmdProto *)this->script_cmd_list->Find(name);

	// if not found, recursively hand up to parent class
	if ((!cp) && (this->super_class))
		cp = this->super_class->FindScriptCmdByName(name);

	return cp;
}


/**
	Find a native command by fourcc code.

	@param  id      the fourcc code of the command to be found
	@return         pointer to CmdProto object, or 0
*/
CmdProto *Class::FindCmdById(fourcc_t id)
{
	// if the class has no commands, hand the request to the parent class.
	if (!this->cmd_table) {
		Class *cl = this->GetSuperClass();
		if (cl)
			return cl->FindCmdById(id);
	}
	else {
		CmdProto *cp = NULL;
		if (this->cmd_table->Find((int)id, cp))
			return cp;
	}
	return NULL;
}


/**
	Checks if the given class is an ancestor of this class.

	@param className Name (all lowercase) of a class.
	@return true if className is an ancestor of this class, or if
			ancestorName is the name of this class.
			false otherwise.
*/
bool Class::IsA(const string &className) const
{
	Assert(!className.empty());

	if (this->GetName() == className)
		return true;
	    
	Class *ancestor = this->super_class;
	while (ancestor) {
		if (ancestor->GetName() == className)
			return true;
		ancestor = ancestor->GetSuperClass();
	}

	return false;
}


// vim:ts=4:sw=4:
