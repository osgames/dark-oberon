//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file referenced.cpp
	@ingroup Kernel_Module

 	@author Vadim Macagon, Peter Knut
	@date 2005, 2008

	@version 1.0 - Initial.
	@version 1.1 - Referencing is hread-safe now.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/referenced.h"
#include "kernel/ref.h"
#include "kernel/threadserver.h"


//=========================================================================
// Referenced
//=========================================================================

uint_t Referenced::instances_count = 0;
Mutex *Referenced::mutex = NULL;


/**
	Constructor.
*/
Referenced::Referenced() :
	deleted(false)
{
	Assert(ThreadServer::GetInstance());

	// create mutex for counted pointers
	if (!this->mutex)
	{
		this->mutex = ThreadServer::GetInstance()->CreateMutex();
		Assert(this->mutex);
	}

	// increase number of instances
	this->mutex->Lock();
	this->instances_count++;
	this->mutex->Unlock();
}


/**
	Destructor.
*/
Referenced::~Referenced()
{
	this->mutex->Lock();

	// invalidate all refs to me
	this->deleted = true;
	Ref<Referenced> *ref;

	while ((ref = (Ref<Referenced> *) this->references.PopFront())) 
	{
		ref->target_object = NULL;
	}

	// decrease number of instances
	this->instances_count--;
	bool delete_mutex = this->instances_count == 0; 

	this->mutex->Unlock();

	// destroy mutex for counted pointers
	if (delete_mutex)
		SafeDelete(this->mutex);
}


// vim:ts=4:sw=4:
