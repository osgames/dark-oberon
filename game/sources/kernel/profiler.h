#ifndef __profiler_h__
#define __profiler_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file profiler.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - One active profiler per one thread.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/root.h"
#include "kernel/mutex.h"

#ifdef USE_PROFILERS

class Env;


//=========================================================================
// Profiler
//=========================================================================

/**
	@class Profiler
	@ingroup Kernel_Module

	@brief Profiler.
*/

class Profiler : public Root
{
//--- embeded
private:
	typedef vector<Profiler *> ProfilersList;
	typedef hash_map<int, ProfilersList *> ProfilersMap;

	class SharedData : public Counted
	{
	public:
		SharedData();
		~SharedData();

	public:
		ProfilersMap profilers;            ///< Started profiles for each thread.
		byte_t active_count;               ///< Number of active profilers.
		Mutex *mutex;
	};


//--- methods
public:
	Profiler(const char *id);
	virtual ~Profiler();

	void Start();
	void Stop();
	void Update();

	float Profiler::GetPercentage() const;

private:
	void Break(double real_time);
	void Resume(double real_time);

	void BreakAll(double real_time);
	void ResumeAll(double real_time);


//--- variables
private:
	double update_time;
	double start_time;
	double used_time;

	bool active;
	float percentage;
	Env *system_variable;

	static SharedData *shared_data;
};


//=========================================================================
// Methods
//=========================================================================

inline
float Profiler::GetPercentage() const
{
	return this->percentage;
}


#endif  // USE_PROFILERS
#endif  // __profiler_h__

// vim:ts=4:sw=4:
