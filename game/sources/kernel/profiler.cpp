//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file profiler.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - One active profiler per one thread.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/profiler.h"
#include "kernel/threadserver.h"
#include "kernel/timeserver.h"
#include "kernel/kernelserver.h"
#include "kernel/env.h"
#include "kernel/mathlib.h"

#ifdef USE_PROFILERS

PrepareClass(Profiler, "Root", s_NewProfiler, s_InitProfiler);


//=========================================================================
// Static members
//=========================================================================

Profiler::SharedData *Profiler::shared_data = NULL;


//=========================================================================
// SharedData
//=========================================================================

Profiler::SharedData::SharedData() :
	Counted(),
	active_count(0)
{
	Assert(ThreadServer::GetInstance());

	this->mutex = ThreadServer::GetInstance()->CreateRecMutex();
	Assert(this->mutex);
}


Profiler::SharedData::~SharedData()
{
	delete this->mutex;

	// break profilers in all threads
	ProfilersMap::iterator it;
	for (it = this->profilers.begin(); it != this->profilers.end(); it++)
		delete it->second;
}


//=========================================================================
// Profiler
//=========================================================================

Profiler::Profiler(const char *id) :
	Root(id),
	update_time(0),
	start_time(0),
	used_time(0),
	active(false),
	percentage(0)
{
	if (!TimeServer::GetInstance())
		ErrorMsg("TimeServer has to be created before Profiler");

	// create system variable for Profiler
	this->system_variable = (Env *)kernel_server->New("Env", NOH_VARIABLES + "/" + this->GetID());

	// create shared mutex
	if (!this->shared_data)
		this->shared_data = NEW SharedData();
	else
		this->shared_data->AcquirePointer();

	Assert(this->shared_data);
}


Profiler::~Profiler()
{
	this->shared_data->Release();
}


inline
void Profiler::Break(double real_time)
{
	Assert(this->active);
	
	// stop profiling
	this->used_time += (real_time - this->start_time) / this->shared_data->active_count;
	this->active = false;
}


inline
void Profiler::Resume(double real_time)
{
	Assert(!this->active);

	// start profiling
	this->start_time = real_time;
	this->active = true;
}


inline
void Profiler::BreakAll(double real_time)
{
	// break profilers in all threads
	ProfilersMap::iterator it;
	for (it = this->shared_data->profilers.begin(); it != this->shared_data->profilers.end(); it++)
	{
		if (!it->second->empty() && it->second->back()->active)
			it->second->back()->Break(real_time);
	}
}


inline
void Profiler::ResumeAll(double real_time)
{
	this->shared_data->active_count = 0;

	// resume profilers in all threads
	ProfilersMap::iterator it;
	for (it = this->shared_data->profilers.begin(); it != this->shared_data->profilers.end(); it++)
	{
		if (!it->second->empty() && !it->second->back()->active)
		{
			it->second->back()->Resume(real_time);
			this->shared_data->active_count++;
		}
	}
}


void Profiler::Start()
{
	Assert(TimeServer::GetInstance());

	this->shared_data->mutex->Lock();

	double real_time = TimeServer::GetInstance()->GetRealTime();

	// break profilers in all threads
	this->BreakAll(real_time);

	// find buffer related to current thread or add new buffer to the map if missing
	int thread_id = ThreadServer::GetInstance()->GetThreadID();

	ProfilersMap::iterator it = this->shared_data->profilers.find(thread_id);
	if (it == this->shared_data->profilers.end())
	{
		it = this->shared_data->profilers.insert(ProfilersMap::value_type(thread_id, NULL)).first;
		it->second = NEW ProfilersList();
	}

	// activate profiler
	it->second->reserve(3);
	it->second->push_back(this);

	// resume profilers in all threads
	this->ResumeAll(real_time);

	this->shared_data->mutex->Unlock();
}


void Profiler::Stop()
{
	Assert(TimeServer::GetInstance());

	this->shared_data->mutex->Lock();

	double real_time = TimeServer::GetInstance()->GetRealTime();

	// break profilers in all threads
	this->BreakAll(real_time);

	// find buffer related to current thread
	int thread_id = ThreadServer::GetInstance()->GetThreadID();
	ProfilersList *profilers = this->shared_data->profilers[thread_id];
	Assert(profilers->back() == this);

	// deactivate profiler
	profilers->pop_back();

	// resume profilers in all threads
	this->ResumeAll(real_time);

	this->shared_data->mutex->Unlock();
}


void Profiler::Update()
{
	this->shared_data->mutex->Lock();

	// profiler was never used
	if (!this->start_time)
	{
		this->shared_data->mutex->Unlock();
		return;
	}

	// get real time
	double real_time = TimeServer::GetInstance()->GetRealTime();
	bool was_active = this->active;

	// berak started profiler
	if (was_active)
		this->Break(real_time);

	// calculate percentage
	this->percentage = float((this->used_time * 100) / (real_time - this->update_time));
	n_clamp(this->percentage, 0, 1);
	this->system_variable->SetF(this->percentage);

	// reset counters
	this->update_time = real_time;
	this->used_time = 0;

	// resume profiler again
	if (was_active)
		this->Resume(real_time);

	this->shared_data->mutex->Unlock();
}


#endif  // USE_PROFILERS

// vim:ts=4:sw=4:
