//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file directory.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/directory.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"


//=========================================================================
// Directory
//=========================================================================

/**
	Constructor. It is private because only FileServer may create objects.
*/
Directory::Directory() : 
#ifdef WINDOWS
	entry_id(-1),
#else
	dir(NULL),
	entry(NULL),
#endif
	empty(true),
	opened(false)
{
	// empty
}


/**
	Destructor.
*/
Directory::~Directory()
{
	if (this->IsOpened())
	{
		LogWarning("Directory destroyed before closing");
		this->Close();
	}
}


/**
	Opens the specified directory.

	@param path   The name of the directory to open.
	@return       Success.
*/
bool Directory::Open(const string &path)
{
	Assert(!this->IsOpened());
	Assert(!path.empty());
	Assert(path.length() > 0);

	// mangle path name
	if (!FileServer::GetInstance()->ManglePath(path, this->path))
		return false;

#ifdef WINDOWS
	if (!FileServer::GetInstance()->DirectoryExists(this->path))
	{
#else
	if (!(this->dir = opendir(this->path.c_str())))
	{
#endif
		LogError1("Can not open directory '%s'", this->path.c_str());
		return false;
	}

	this->opened = true;
	this->SetFirstEntry();

	return true;
}


/**
	Closes the directory.
*/
void Directory::Close()
{
	Assert(this->IsOpened());

#ifdef WINDOWS
	if (!this->IsEmpty())
	{
		Assert(this->entry_id > -1);
		_findclose(this->entry_id);
	}
		
#else
	closedir(this->dir);
	this->entry = NULL;
#endif

	this->opened = false;
	this->path = "";
}


/**
	Checks if the directory is empty.

	@return          @c True if empty.
*/
bool Directory::IsEmpty()
{
	Assert(this->IsOpened());
	return this->empty;
}


/**
    Sets search index to first entry in directory.
  
    @return          Success.
*/
bool Directory::SetFirstEntry()
{
	Assert(this->IsOpened());

#ifdef WINDOWS
	this->entry_id = _findfirst((this->path + "/*").c_str(), &this->entry);
	this->empty = (this->entry_id == -1);

#else
	this->entry = readdir(this->dir);
	this->empty = (this->entry == NULL);
#endif

	return !this->empty;
}


/**
	Selects next directory entry.

	@return          Success.
*/
bool Directory::SetNextEntry()
{
	Assert(this->IsOpened());

#ifdef WINDOWS
	return !_findnext(this->entry_id, &this->entry);

#else
	this->entry = readdir(this->dir);
	return (this->entry != NULL);
#endif
}


/**
	Gets name of actual directory entry.

	@return          The name.
*/
const string &Directory::GetEntryName()
{
	Assert(this->IsOpened());

#ifdef WINDOWS
	Assert(this->entry_id > -1);
	this->entry_name = this->entry.name;

#else
	Assert(this->entry);
	this->entry_name = this->entry->d_name;
#endif

	return this->entry_name;
}


/**
	Gets type of actual directory entry.

	@return          ET_FILE or ET_DIRECTORY.
*/
Directory::EntryType Directory::GetEntryType()
{
	Assert(this->IsOpened());

#ifdef WINDOWS
	Assert(this->entry_id > -1);
	return this->entry.attrib & _A_SUBDIR ? ET_DIRECTORY : ET_FILE;

#else
	Assert(this->entry);
	switch (this->entry->d_type) {
	case DT_DIR:  return ET_DIRECTORY;
	case DT_FILE: return ET_FILE;
	else          return ET_INVALID;
	}
#endif
}

// vim:ts=4:sw=4:
