#ifndef __semaphore_h__
#define __semaphore_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file semaphore.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Semaphore
//=========================================================================

/**
	@class Semaphore
	@ingroup Kernel_Module

	@brief Semaphore.
*/

class Semaphore
{
	friend class ThreadServer;

//--- methods
public:
	virtual ~Semaphore() {};

	virtual void Wait(double timeout = -1.0) {};
	virtual void SendSignal() {};

protected:
	Semaphore() : singnals(0) {};

//--- variables
protected:
	int singnals;
};


#endif  // __semaphore_h__

// vim:ts=4:sw=4:
