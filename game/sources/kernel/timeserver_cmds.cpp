//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file timeserver_cmds.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/timeserver.h"


//=========================================================================
// Commands
//=========================================================================

static void s_StartTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;
    self->StartTime();
}


static void s_StopTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;
    self->StopTime();
}


static void s_GetRealTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;
    cmd->Out()->SetF((float)self->GetRealTime());
}


static void s_GetTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;
    cmd->Out()->SetF((float)self->GetTime());
}


static void s_GetFrameTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;
    cmd->Out()->SetF((float)self->GetFrameTime());
}


static void s_GetMinFrameTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;
    cmd->Out()->SetF((float)self->GetMinFrameTime());
}


static void s_GetFixFrameTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;
    cmd->Out()->SetF((float)self->GetFixFrameTime());
}


static void s_SetTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;
	self->SetTime((double)cmd->In()->GetF());
}


static void s_SetMinFrameTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;

	double time = (double)cmd->In()->GetF();
	bool wait = cmd->In()->GetB();

	self->SetMinFrameTime(time, wait);
}


static void s_SetFixFrameTime(void* o, Cmd* cmd)
{
    TimeServer *self = (TimeServer *)o;

	double time = (double)cmd->In()->GetF();
	bool wait = cmd->In()->GetB();

	self->SetFixFrameTime(time, wait);
}


void s_InitTimeServer_cmds(Class *cl)
{
    cl->BeginCmds();

    cl->AddCmd("v_StartTime_v",         'SATM', s_StartTime);
	cl->AddCmd("v_StopTime_v",          'SOTM', s_StopTime);
	cl->AddCmd("f_GetRealTime_v",       'GRTM', s_GetRealTime);
	cl->AddCmd("f_GetTime_v",           'GTM_', s_GetTime);
	cl->AddCmd("f_GetFrameTime_v",      'GFTM', s_GetFrameTime);
	cl->AddCmd("f_GetMinFrameTime_v",   'GMFT', s_GetMinFrameTime);
	cl->AddCmd("f_GetFixFrameTime_v",   'GFFT', s_GetFixFrameTime);
	cl->AddCmd("v_SetTime_f",           'STM_', s_SetTime);
	cl->AddCmd("v_SetMinFrameTime_fb",  'SMFT', s_SetMinFrameTime);
	cl->AddCmd("v_SetFixFrameTime_fb",  'SFFT', s_SetFixFrameTime);

	cl->EndCmds();
}

// vim:ts=4:sw=4:
