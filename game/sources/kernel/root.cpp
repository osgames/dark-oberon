//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file root.cpp
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Serializing of object.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/root.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializeserver.h"
#include "kernel/messageserver.h"


PrepareScriptClass(Root, "Object", s_NewRoot, s_InitRoot, s_InitRoot_cmds);


//=========================================================================
// Root
//=========================================================================

Root::Root(const char *id) :
	TreeNode<Root>(),
	Referenced(),
	Object(NULL),
	mutex(NULL)
{
	Assert(id);
	this->id = id;
}


Root::~Root()
{
	if (this->IsLinked())
		this->Remove();

	this->ReleaseChildren();

	delete this->mutex;
}


/**
	Releases all children nodes.
	@note Children with more counted pointers are preserved.

	@return @c True if all children were released.
*/
bool Root::ReleaseChildren()
{
	// release all children
	bool child_released;
	bool released_all = true;

	Root *child;
	while (child = this->PopFront())
	{
		child_released = child->Release();
		released_all = released_all && child_released;
	}

	return released_all;
}


/**
	Removes all children nodes.
	@note Children are not released.
*/
void Root::ClearChildren()
{
	// remove all children
	Root *child;
	while (child = this->PopFront());
}


/**
	Returns the full pathname of the object.
*/
string Root::GetFullPath()
{
	// path to root node is empty
	if (!this->GetParent())
		return string("");

	// build stack of pointers leading from me to root
	const int max_depth = 128;

	Root *stack[max_depth];
	Root *cur = (Root *)this;
	int i = 0;

	do {
		stack[i++] = cur;
	} while ((cur = cur->GetParent()) && (i < max_depth));

	// traverse stack in reverse order and build path
	string str;

	for (i--; i >= 0; i--)
	{
		const char *cur_id = stack[i]->GetID().c_str();
		str.append(cur_id);
	    
		// add slash if not hierarchy root, and not last element in path
		if ((cur_id[0] != '/') && (i > 0)) 
			str.append("/");
	}

	return str;
}


/**
	Compare-Hook for qsort() in Root::Sort()
*/
int CompareChilds(const void *e0, const void *e1)
{
	Root *r0 = *((Root **)e0);
	Root *r1 = *((Root **)e1);

	return r0->GetID().compare(r1->GetID());
}


/**
	Compare-Hook for qsort() in Root::Sort()
*/
int CompareChildsBack(const void *e0, const void *e1)
{
	Root *r0 = *((Root **)e0);
	Root *r1 = *((Root **)e1);

	return r1->GetID().compare(r0->GetID());
}


/**
	Sort child objects alphabetically. This is a slow operation.
*/
void Root::Sort(bool backward)
{
	int num,i;
	Root *c;

	// count child objects
	for (num = 0, c = this->GetFront(); c; c = c->GetNext(), num++);

	if (num > 0)
	{
		Root **c_array = NEW Root *[num];
		Assert(c_array);

		for (i = 0, c = this->GetFront(); c; c = c->GetNext(), i++)
			c_array[i] = c;

		if (backward)
			qsort(c_array, num, sizeof(Root *), CompareChildsBack);
		else
			qsort(c_array, num, sizeof(Root *), CompareChilds);

		for (i = 0; i < num; i++)
		{
			c_array[i]->Remove();
			this->PushBack(c_array[i]);
		}

		delete[] c_array;
	}
}


/**
	Recursive version of GetInstanceSize(). Returns the summed the size of 
	all children.
*/
int Root::GetTreeSize() const
{
	// get size of child objects
	int size = 0;
	Root *child;

	for (child = this->GetFront(); child; child = child->GetNext())
		size += child->GetTreeSize();

	// add own size
	size += this->GetInstanceSize();
	return size;
}


/**
	Writes hierarchy with this node in the root to log file.
*/
void Root::LogTree(const string &indent) const
{
	Root *child;

	// print header
	if (indent.empty())
		LogInfo("--- Named Object Hierarchy ---");

	// print this node
	string text = indent;
	if (text.empty())
		text += "*-";
	else
		text += "|-";
	text += this->id;
	LogInfo1("%s", text.c_str());

	// print children
	text = indent;
	text += this->IsLinked() && this->GetNext() ? "| " : "  ";
	for (child = this->GetFront(); child; child = child->GetNext())
		child->LogTree(text);
}


/**
	Recursively serialize all children.
*/
bool Root::Serialize(Serializer &serializer)
{
	if (!serializer.SetAttribute("class", this->GetClass()->GetName()))
		return false;
	if (!serializer.SetAttribute("id", this->GetID()))
		return false;

	// serialize all children
	Root *child;
	for (child = this->GetFront(); child; child = child->GetNext())
	{
		// create new group and set common attributes
		if (!serializer.AddGroup(child->GetGroupName()))
			return false;

		// serialize child
		if (!child->Serialize(serializer))
			return false;

		// end new group
		serializer.EndGroup();
	}

	return true;
}


/**
	Recursively deserialize all children.
*/
bool Root::Deserialize(Serializer &serializer, bool first)
{
	string class_name, object_id;
	Root *child;

	// check object class in first object
	if (first)
	{
		// get class and object ids
		if (!serializer.GetAttribute("class", class_name, false))
		{
			LogError("Missing 'class' or 'id' attribute");
			return false;
		}

		// check class
		if (class_name != this->GetClass()->GetName())
		{
			LogError2("Classes mismatch: %s, %s", this->GetClass()->GetName().c_str(), class_name.c_str());
			return false;
		}
	}

	// delete all children
	if (!this->ReleaseChildren())
	{
		LogError("Error deleting old children objects");
		return false;
	}

	// go to first subgroup [optional]
	if (!serializer.GetGroup())
		return true;

	// read root nodes
	do
	{
		// read root node
		if (serializer.CheckGroupName("object"))
		{
			// get class and object ids
			if (!serializer.GetAttribute("class", class_name, false) || !serializer.GetAttribute("id", object_id, false))
			{
				LogError("Missing 'class' or 'id' attribute");
				return false;
			}

			// create child object
			this->kernel_server->PushCwd(this);
			child = this->kernel_server->New(class_name, object_id);
			this->kernel_server->PopCwd();
			if (!child)
				return false;

			// deserialize child
			if (!child->Deserialize(serializer, false))
				return false;
		}

	} while (serializer.GetNextGroup(true));

	// finish group
	serializer.EndGroup();
	return true;
}


void Root::SendMessage(BaseMessage *message, Root *destination, double delay)
{
	Assert(message);
	Assert(destination);

	MessageServer::GetInstance()->SendMessage(message, this, destination, delay);
}


// vim:ts=4:sw=4:
