#ifndef __list_h__
#define __list_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file list.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Handling of to parent pointer in ListNode.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/listnode.h"


//=========================================================================
// List
//=========================================================================

/**
	@class List
	@ingroup Kernel_Module

	@brief Implements double linked list.

	Node has to be only ListNode derived class!
*/

template <class Node>
class List
{
//--- variables
private:
	ListNode<Node> *front;
	ListNode<Node> *back;

//--- methods
public:
	List();
	virtual ~List();

	bool IsEmpty() const;

	Node* GetFront() const;
	Node* GetBack() const;
	void PushFront(Node* node);
	void PushBack(Node* node);
	Node* PopFront();
	Node* PopBack();
};


//=========================================================================
// Methods
//=========================================================================

template <class Node>
List<Node>::List()
{
	// create boundaries
	this->front = NEW ListNode<Node>();
	this->back  = NEW ListNode<Node>();

	// link boundaries
	this->front->next = (Node *)back;
	this->back->prev = (Node *)front;

	// set parents to this list
	this->front->parent = this;
	this->back->parent = this;
}


template <class Node>
List<Node>::~List()
{
	// assert if not empty
	Assert(this->IsEmpty());

	// unlink boundaries
	this->front->next = NULL;
	this->back->prev = NULL;

	// delete boundaries
	delete this->front;
	delete this->back;
}


template <class Node>
bool List<Node>::IsEmpty() const
{
	return (this->front->GetNext() == NULL);
}


/**
	Returns first child object, or @c NULL if no child objects exist.
*/
template <class Node>
Node* List<Node>::GetFront() const
{
	return this->front->GetNext();
}


/**
	Returns last child object, or @c NULL if no child objects exist.
*/
template <class Node>
Node* List<Node>::GetBack() const
{
	return this->back->GetPrev();
}


template <class Node>
void List<Node>::PushFront(Node *node)
{
	Assert(node);
	node->InsertAfter((Node *)this->front);
}


template <class Node>
void List<Node>::PushBack(Node *node)
{
	Assert(node);
	node->InsertBefore((Node *)this->back);
}


template <class Node>
Node *List<Node>::PopFront()
{
	if (this->IsEmpty())
		return NULL;

	Node *node = this->front->GetNext();
	node->Remove();

	return node;
}


template <class Node>
Node *List<Node>::PopBack()
{
	if (this->IsEmpty())
		return NULL;

	Node *node = this->back->GetPrev();
	node->Remove();

	return node;
}


//=========================================================================
// SList
//=========================================================================

/**
	@class SList
	@ingroup Kernel_Module

	@brief Simple list (not template).
*/

class SList : public List<SListNode>
{
	//
};


#endif  // __list_h__

// vim:ts=4:sw=4:
