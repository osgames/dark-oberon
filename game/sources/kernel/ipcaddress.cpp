//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file ipcaddress.cpp
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2007 - 2008

	@version 1.0 - Copy from Nebula Device.
	@version 1.1 - Comparison operators. Fixed const methods.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/ipcaddress.h"


//=========================================================================
// Tools
//=========================================================================

static inline int hash(const char* str, int size)
{
    int i = 0;
    int j = 1;
    char c;

    while (c = *str++)
        i += ((uchar_t)c) * j++;

    return (i % size);
}


//=========================================================================
// IpcAddress
//=========================================================================

/**
*/
IpcAddress::IpcAddress() :
	host_name("localhost"),
	addr_struct_valid(false),
	ip_address_valid(false),
	port_valid(false),
	port(0)
{
	memset(&(this->addr_struct), 0, sizeof(this->addr_struct));
	memset(&(this->ip_address), 0, sizeof(this->ip_address));
}


/**
*/
IpcAddress::IpcAddress(const char *host_name, const char *port_name) :
	addr_struct_valid(false),
	ip_address_valid(false),
	port_valid(false),
	port(0)
{
	memset(&(this->addr_struct), 0, sizeof(this->addr_struct));
	memset(&(this->ip_address), 0, sizeof(this->ip_address));
	this->SetHostName(host_name);
	this->SetPortName(port_name);
}


/**
*/
IpcAddress::IpcAddress(const char *host_name, ushort_t port) :
	addr_struct_valid(false),
	ip_address_valid(false),
	port_valid(true)
{
	memset(&(this->addr_struct), 0, sizeof(this->addr_struct));
	memset(&(this->ip_address), 0, sizeof(this->ip_address));
	this->SetHostName(host_name);
	this->SetPortNum(port);
}


IpcAddress::IpcAddress(const IpcAddress &addr)
{
	this->host_name = addr.host_name;
	this->port_name = addr.port_name;
	this->addr_struct = addr.addr_struct;
	this->ip_address = addr.ip_address;
	this->ip_string = addr.ip_string;
	this->port = addr.port;
	this->addr_struct_valid = addr.addr_struct_valid;
	this->ip_address_valid = addr.ip_address_valid;
	this->port_valid = addr.port_valid;
}


/**
*/
IpcAddress::~IpcAddress()
{
	// empty
}


/**
	Set the host name, this includes the special host names "any",
	"localhost", "broadcast", "self" and "inetself". The corresponding
	ip address can be queried using the GetIpAddress() method.
*/
void IpcAddress::SetHostName(const char *name)
{
	Assert(name);
	this->host_name = name;
	this->addr_struct_valid = false;
	this->ip_address_valid = false;
}


/**
	Get the host name.
*/
const char* IpcAddress::GetHostName() const
{
	return this->host_name.c_str();
}


/**
	Set the port name. The corresponding port number can be queried
	using the method GetPortNum().
*/
void IpcAddress::SetPortName(const char *name)
{
	Assert(name);
	this->port_name = name;
	this->addr_struct_valid = false;
	this->port_valid = false;
}


/**
    Get the port name.
*/
const char* IpcAddress::GetPortName() const
{
	return this->port_name.c_str();
}


/**
	Return true if an ip address is an "internet" address (not a
	Class A, B or C network address). If a machine is both connected
	to a LAN and the Internet, this method can be used to find the ip
	address that is visible from outside the LAN.
*/
bool IpcAddress::IsInternetAddress(const in_addr &addr) const
{
	// generate address string from addr
	char *addrString = inet_ntoa(addr);

	// tokenize string into its members
	int b1, b2, b3, b4;
	Verify(sscanf(addrString, "%d.%d.%d.%d", &b1, &b2, &b3, &b4) == 4);

	if ((b1 == 10) && (b2 >= 0) && (b2 <= 254))
	{
		// Class A net
		return false;
	}
	if ((b1 == 172) && (b2 >= 16) && (b2 <= 31))
	{
		// Class B net
		return false;
	}
	if ((b1 == 192) && (b2 == 168) && (b3 >= 0) && (b3 <= 254))
	{
		// Class C net
		return false;
	}
	if (b1 < 224)
	{
		// unknown other local net type
		return false;
	}

	// an internet address
	return true;
}


/**
	This updates the ip address field by converting the host name to
	an ip address.
*/
bool IpcAddress::ValidateIpAddr() const
{
	Assert(!this->ip_address_valid);

	// first check for special case hostnames
	if (this->host_name == "any")
	{
		// the "ANY" address
		this->ip_address.s_addr = htonl(INADDR_ANY);
	}
	else if (this->host_name == "broadcast")
	{
		// the "BROADCAST" address
		this->ip_address.s_addr = htonl(INADDR_BROADCAST);
	}
	else if ((this->host_name == "self") || (this->host_name == "inetself"))
	{
		// the machine's ip address
		char localHostName[512];
		int err = gethostname(localHostName, sizeof(localHostName));
		if (0 != err)
		{
			// error getting host name
			return false;
		}

		// resolve host name
		struct hostent* he = gethostbyname(localHostName);
		if (0 == he)
		{
			// could not resolve own host name(!)
			return false;
		}

		// initialize with the default address
		this->ip_address = *((struct in_addr*)he->h_addr);
		if (this->host_name == "inetself")
		{
			// if internet address requested, scan list of ip addresses
			// for a non-Class A,B or C network address
			int i;
			for (i = 0; he->h_addr_list[i]; i++)
			{
				if (this->IsInternetAddress(*((struct in_addr*)he->h_addr_list[i])))
				{
					this->ip_address = *((struct in_addr*)he->h_addr_list[i]);
					break;
				}
			}
		}
	}
	else
	{
		struct hostent* he = gethostbyname(this->host_name.c_str());
		if (0 == he)
		{
			// could not resolve host name!
			return false;
		}
		this->ip_address = *((struct in_addr*)he->h_addr);
	}

	// also convert the ip address to a string
	char buf[64];
	sprintf(buf, "%s", inet_ntoa(this->ip_address));
	this->ip_string = buf;

	// all ok
	this->ip_address_valid = true;
	return true;
}


/**
	Validate the port number by converting the port name to a port number
	inside the allowed port range.
*/
bool IpcAddress::ValidatePortNum() const
{
	Assert(!this->port_valid);

	this->port = ((ushort_t)hash(this->port_name.c_str(), PortRange)) + MinPortNum;
	this->port_valid = true;

	return true;
}


/**
    Validate the complete ip address struct.
*/
bool IpcAddress::ValidateAddrStruct() const
{
	Assert(!this->addr_struct_valid);

	// validate ip address if necessary
	if (!this->ip_address_valid)
	{
		if (!this->ValidateIpAddr())
			return false;
	}

	// validate port num if necessary
	if (!this->port_valid)
	{
		if (!this->ValidatePortNum())
			return false;
	}

	// fill the address struct
	this->addr_struct.sin_family = AF_INET;
	this->addr_struct.sin_port   = htons(this->port);
	this->addr_struct.sin_addr   = this->ip_address;

	this->addr_struct_valid = true;
	return true;
}


/**
    Set the ip address structure. This will also initialize the host name,
    the port number and the port name.
*/
void IpcAddress::SetAddrStruct(const sockaddr_in &addr)
{
	this->addr_struct = addr;
	this->ip_address  = this->addr_struct.sin_addr;
	this->port = ntohs(this->addr_struct.sin_port);
	this->addr_struct_valid = true;
	this->ip_address_valid = true;
	this->port_valid = true;

	// initialize the host name, port number, and port name
	this->host_name = inet_ntoa(this->addr_struct.sin_addr);

	char buf[64];
	sprintf(buf, "%hu", this->port);
	this->port_name = buf;

	sprintf(buf, "%s", inet_ntoa(this->ip_address));
	this->ip_string = buf;
}


/**
    Get the ip address structure.
*/
const sockaddr_in &IpcAddress::GetAddrStruct() const
{
	if (!this->addr_struct_valid)
		this->ValidateAddrStruct();

	return this->addr_struct;
}


/**
    Get the ip address as string.
*/
const char *IpcAddress::GetIpAddrString() const
{
	if (!this->ip_address_valid)
		this->ValidateIpAddr();

	return this->ip_string.c_str();
}


/**
	Sets the port num in host byte order.
*/
void IpcAddress::SetPortNum(ushort_t portnum)
{
	this->port = portnum;
	this->port_valid = true;

	char buf[64];
	sprintf(buf, "%hu", this->port);
	this->port_name = buf;
}


/**
	Get the port num in host byte order.
*/
ushort_t IpcAddress::GetPortNum() const
{
	if (!this->port_valid)
		this->ValidatePortNum();

	return this->port;
}


// vim:ts=4:sw=4:
