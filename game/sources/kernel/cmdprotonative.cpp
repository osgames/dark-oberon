//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file cmdprotonative.cpp
	@ingroup Kernel_Module

 	@author Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/cmdprotonative.h"


//=========================================================================
// CmdProtoNative
//=========================================================================

/**
	@brief Constructor.

	@param _proto_def [in] Blue print string.
	@param _id        [in] 4cc code
	@param _cmd_proc  [in] Pointer to C style command handler.
*/
CmdProtoNative::CmdProtoNative(const char *_proto_def, fourcc_t _id, void (*_cmd_proc)(void *, Cmd *)) :
	CmdProto(_proto_def, _id)
{
	this->cmd_proc = _cmd_proc; // can be NULL if legacy cmd handling used
}


/**
	Copy constructor.
*/
CmdProtoNative::CmdProtoNative(const CmdProtoNative& rhs) :
	CmdProto(rhs)
{
	this->cmd_proc = rhs.cmd_proc;
}


/**
	Executes a command on the provided object.
*/
bool CmdProtoNative::Dispatch(void* obj, Cmd* cmd)
{
	Assert(this->cmd_proc);

	this->cmd_proc(obj, cmd);

	return true;
}


// vim:ts=4:sw=4:
