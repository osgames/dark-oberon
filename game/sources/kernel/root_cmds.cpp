//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file root_cmds.cpp
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/root.h"


//=========================================================================
// Commands
//=========================================================================

/**
	Returns the parent object. If the object doesn't have a parent object 
	(this is only valid for the root object '/'), @c NULL will be returned.
*/
static void s_GetParent(void *o, Cmd *cmd)
{
	Root *self = (Root *)o;
	cmd->Out()->SetO(self->GetParent());
}


/**
	Returns first child object, or @c NULL if no child objects exist.
*/
static void s_GetFront(void *o, Cmd *cmd)
{
	Root *self = (Root *)o;
	cmd->Out()->SetO(self->GetFront());
}


/**
	Returns last child object, or @c NULL if no child objects exist.
*/
static void s_GetBack(void *o, Cmd *cmd)
{
    Root *self = (Root *)o;
    cmd->Out()->SetO(self->GetBack());
}


/**
	Returns next object in the same hierarchy level, @c NULL if no next
	object exists.
*/
static void s_GetNext(void *o, Cmd *cmd)
{
	Root *self = (Root *)o;
	cmd->Out()->SetO(self->GetNext());
}


/**
	Returns previous object in the same hierarchy level, @c NULL if no
	previous object exists. 
*/
static void s_GetPrev(void *o, Cmd *cmd)
{
    Root *self = (Root *)o;
    cmd->Out()->SetO(self->GetPrev());
}


/**
	Returns a list of all children.
*/
static void s_GetChildren(void *o, Cmd *cmd)
{
	Root *self = (Root *)o;

	int num_children = 0;
	int i = 0;

	Root *child;
	for (child = (Root *)self->GetFront(); child; child = (Root *)child->GetNext())
		num_children++;

	Arg* children = NEW Arg[num_children];
	Assert(children);

	for (child = (Root *)self->GetFront(); child; child = (Root *)child->GetNext())
		children[i++].SetO(child);

	cmd->Out()->SetA(children, num_children);
}


/**
	Gives the object a new name. Name may not contain any path components!
*/
static void s_SetID(void *o, Cmd *cmd)
{
	Root *self = (Root *)o;
	self->SetID(string(cmd->In()->GetS()));
}


/**
	Returns the object's name without path.
*/
static void s_GetID(void *o, Cmd *cmd)
{
	Root *self = (Root *)o;
	cmd->Out()->SetS(self->GetID().c_str());
}


/**
	Returns the full pathname of the object.
*/
static void s_GetFullPath(void *o, Cmd *cmd)
{
	Root *self = (Root *)o;
	cmd->Out()->SetS(self->GetFullPath().c_str());
}


/**
	Finds a child object with a given name, @c NULL if not found.
*/
static void s_Find(void *o, Cmd *cmd)
{
	Root *self = (Root *)o;
	cmd->Out()->SetO(self->Find(string(cmd->In()->GetS())));
}


/**
	Sort all child objects alphabetically.
*/
static void s_Sort(void *o, Cmd *)
{
	Root *self = (Root *)o;
	self->Sort();
}


/**
	Recursive version of GetInstanceSize(). Returns the summed the size of 
	all children.
*/
static void s_GetTreeSize(void* o, Cmd* cmd)
{
	Root* self = (Root*)o;
	cmd->Out()->SetI(self->GetTreeSize());
}


void s_InitRoot_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("o_GetParent_v",         'GPRT', s_GetParent);
	cl->AddCmd("o_GetFront_v",          'GFRO', s_GetFront);
	cl->AddCmd("o_GetBack_v",           'GBCK', s_GetBack);
	cl->AddCmd("o_GetNext_v",           'GNXT', s_GetNext);
	cl->AddCmd("o_GetPrev_v",           'GPRV', s_GetPrev);
	cl->AddCmd("a_GetChildren_v",       'GCHD', s_GetChildren);
	cl->AddCmd("v_SetID_s",             'SEID', s_SetID);
	cl->AddCmd("s_GetID_v",             'GEID', s_GetID);
	cl->AddCmd("s_GetFullPath_v",       'GFPA', s_GetFullPath);
	cl->AddCmd("o_Find_s",              'FIND', s_Find);
	cl->AddCmd("v_Sort_v",              'SORT', s_Sort);
	cl->AddCmd("i_GetTreeSize_v",       'GTSZ', s_GetTreeSize);

	cl->EndCmds();
}


// vim:ts=4:sw=4:
