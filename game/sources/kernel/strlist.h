#ifndef __strlist_h__
#define __strlist_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file strlist.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/list.h"
#include "kernel/strnode.h"


//=========================================================================
// StrList
//=========================================================================

/**
	@class StrList
	@ingroup Kernel_Module

	@brief Implements double linked list of named nodes.

	Node has to be only _StrNode derived class!
*/

class StrList : public List<StrNode> {
//--- methods
public:
	StrList();
	
	StrNode* Find(const string &name);
};


//=========================================================================
// Methods
//=========================================================================

inline
StrList::StrList() :
	List<StrNode>()
{
	//
}


inline
StrNode *StrList::Find(const string &name)
{
	for (StrNode* node = this->GetFront(); node; node = node->GetNext()) {
		if (node->GetName() == name) 
			return node;
	}

	return NULL;
}


#endif  // __strlist_h__

// vim:ts=4:sw=4:
