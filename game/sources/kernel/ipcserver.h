#ifndef __ipcserver_h__
#define __ipcserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file ipcserver.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2007 - 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/root.h"
#include "kernel/ipcaddress.h"
#include "kernel/ipcbuffer.h"
#include "kernel/list.h"
#include "kernel/ipcchannel.h"

class Thread;
class Mutex;


//=========================================================================
// IpcServer
//=========================================================================

/**
    @class IpcServer
    @ingroup Kernel_Module

    @brief Sockets based server object for simple inter-process communication.

    An IpcServer object opens a named public message port, and
    waits for connection requests from IpcClient objects. One
    IpcServer can handle any number of IpcClients.
*/

class IpcServer : public Root
{
//--- embeded
private:
	typedef hash_map<uint_t, IpcChannel *> ChannelsList;
	

//--- methods
public:
    IpcServer(const char *id);
    virtual ~IpcServer();

	bool StartListening(ushort_t port, IpcChannel::Protocol protocol);
	void StopListening(IpcChannel::Protocol protocol);
	bool StartReceiving();
	void StopReceiving();

    virtual bool Trigger();

	uint_t Connect(const IpcAddress &addr, IpcChannel::Protocol protocol);
	void Disconnect(uint_t channel_id);
	void DisconnectAll();
    bool Send(uint_t channel_id, const IpcBuffer &msg);
    bool SendAll(const IpcBuffer &msg, IpcChannel::Protocol protocol);

	// broadcast
	void SetBroadcastPort(ushort_t port);
	bool SendBroadcast(const IpcBuffer &msg);

protected:
	virtual void ProcessData(uint_t channel_id, const IpcBuffer &buffer);
	bool ExistsChannel(uint_t channel_id);

private:
	void AddChannel(IpcChannel *channel);
	IpcChannel *FindChannel(uint_t channel_id);
	IpcChannel *FindChannel(const IpcAddress &addr, IpcChannel::Protocol protocol);

	bool CreateSharedChannel();
	bool DestroySharedChannel();
	IpcChannel *CreateSendingChannel();

	void PollChannels();

	static void ListenerThreadFunc(Thread *thread);
	static void ListenerWakeupFunc(Thread *thread);
	static void ReceiverThreadFunc(Thread *thread);


//--- variables
private:
    ushort_t tcp_listener_port;
	ushort_t udp_listener_port;
	ushort_t udp_broadcast_port;

    Thread *listener_thread;
	Thread *receiver_thread;
	fd_set *receive_set;

	bool udp_listening;
	IpcChannel *udb_shared_channel;
	IpcChannel *udb_broadcast_channel;

	Mutex *channels_mutex;
	ChannelsList channels;
	IpcBuffer buffer;
};


//=========================================================================
// Methods
//=========================================================================

inline
void IpcServer::SetBroadcastPort(ushort_t port)
{
	this->udp_broadcast_port = port;
}

inline
bool IpcServer::ExistsChannel(uint_t channel_id)
{
	return this->FindChannel(channel_id) != NULL;
}

#endif // __ipcserver_h__

// vim:ts=4:sw=4:
