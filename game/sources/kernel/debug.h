#ifndef __debug_h__
#define __debug_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file debug.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/config.h"


//=========================================================================
// Debug macros
//=========================================================================

#ifdef USE_ASSERT
#	define Assert(exp)                          do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, NULL); } while (0)
#	define AssertMsg(exp, msg)                  do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg);  } while (0)
#	define AssertMsg1(exp, msg, p1)             do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg, p1);  } while (0)
#	define AssertMsg2(exp, msg, p1, p2)         do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg, p1, p2);  } while (0)
#	define AssertMsg3(exp, msg, p1, p2, p3)     do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg, p1, p2, p3);  } while (0)
#	define AssertMsg4(exp, msg, p1, p2, p3, p4) do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg, p1, p2, p3, p4);  } while (0)

#	define Verify(exp)                          do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, NULL); } while (0)
#	define VerifyMsg(exp, msg)                  do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg);  } while (0)
#	define VerifyMsg1(exp, msg, p1)             do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg, p1);  } while (0)
#	define VerifyMsg2(exp, msg, p1, p2)         do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg, p1, p2);  } while (0)
#	define VerifyMsg3(exp, msg, p1, p2, p3)     do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg, p1, p2, p3);  } while (0)
#	define VerifyMsg4(exp, msg, p1, p2, p3, p4) do { if (!(exp)) CallAssert(#exp, __FILE__, __LINE__, msg, p1, p2, p3, p4);  } while (0)

#else
#	define Assert(exp)
#	define AssertMsg(exp, msg)
#	define AssertMsg1(exp, msg, p1)
#	define AssertMsg2(exp, msg, p1, p2)
#	define AssertMsg3(exp, msg, p1, p2, p3)
#	define AssertMsg4(exp, msg, p1, p2, p3, p4)

#	define Verify(exp)                          (exp)
#	define VerifyMsg(exp, msg)                  (exp)
#	define VerifyMsg1(exp, msg, p1)             (exp)
#	define VerifyMsg2(exp, msg, p1, p2)         (exp)
#	define VerifyMsg3(exp, msg, p1, p2, p3)     (exp)
#	define VerifyMsg4(exp, msg, p1, p2, p3, p4) (exp)
#endif

#define	Error()					        CallAbort(__FILE__, __LINE__, NULL)
#define ErrorMsg(msg)			        CallAbort(__FILE__, __LINE__, msg)
#define ErrorMsg1(msg, p1)              CallAbort(__FILE__, __LINE__, msg, p1)
#define ErrorMsg2(msg, p1, p2)          CallAbort(__FILE__, __LINE__, msg, p1, p2)
#define ErrorMsg3(msg, p1, p2, p3)      CallAbort(__FILE__, __LINE__, msg, p1, p2, p3)
#define ErrorMsg4(msg, p1, p2, p3, p4)  CallAbort(__FILE__, __LINE__, msg, p1, p2, p3, p4)


#ifdef USE_ASSERT
void CallAssert(const char *exp, const char *file, int line, const char *message, ...);
#endif
void CallAbort(const char *file, int line, const char *message, ...);


#endif  // __debug_h__

// vim:ts=4:sw=4:
