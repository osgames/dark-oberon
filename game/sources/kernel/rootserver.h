#ifndef __rootserver_h__
#define __rootserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file rootserver.h
	@ingroup Kernel_Module

 	@author Peter Knut, RadonLabs GmbH
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/server.h"
#include "kernel/root.h"


//=========================================================================
// RootServer
//=========================================================================

/**
	@class RootServer
	@ingroup Kernel_Module

	@brief RootServer.
*/

template <class Node>
class RootServer : public Root, public Server<Node>
{
	friend class Server<Node>;

//--- methods
public:
	RootServer(const char *name);
};


template <class Node>
inline
RootServer<Node>::RootServer(const char *id) :
	Root(id),
	Server<Node>()
{
	//
}


#endif  // __rootserver_h__

// vim:ts=4:sw=4:
