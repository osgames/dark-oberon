#ifndef __scriptloader_h__
#define __scriptloader_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scriptloader.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005 - 2006

	@version 1.0 - Initial.
	@version 1.1 - Separated SriptLoader from ScriptServer.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/root.h"


//=========================================================================
// ScriptLoader
//=========================================================================

/**
	@class ScriptLoader
	@ingroup Kernel_Module

	The script loader object is the communication point between
	a Root object's Dispatch() method and a specific scripting
	language. More specifically, the script server's main task is to
	translate Cmd objects into script statements and back, and to
	communicate with receiver Root objects by sending Cmd objects
	to them (which is done by invoking Dispatch() on the receiver
	with the Cmd object as the argument).

	Script loaders are created and managed by ScriptServer. There can be
	more than one script loader at the same time.
*/

class ScriptLoader : public Root
{
//--- varialbles
private:
	bool fail_on_error;    ///< Whether loader aborts the application when some error occures.

//--- methods
public:
	ScriptLoader(const char *id);
	virtual ~ScriptLoader();

	void Exit();

	// rinning script
	virtual bool RunCode(const char *code, string &result);
	virtual bool RunFunction(const char *func_name, string &result);
	bool RunScript(const string &file_path, string &result);

	// settings
	void SetFailOnError(bool b);
	bool IsFailOnError();
	static void SetCurrentTarget(Object *obj);
	static Object* GetCurrentTarget();

protected:
	static Object *current_target;   ///< Curent object for sending commands.
};


//=========================================================================
// Methods
//=========================================================================

/**
	Abort application on error?
	@param fail New abort status.
*/
inline
void ScriptLoader::SetFailOnError(bool fail)
{
	this->fail_on_error = fail;
}


/**
	Returns abort program status.
	@return Abort status.
*/
inline
bool ScriptLoader::IsFailOnError()
{
	return this->fail_on_error;
}


/**
	Sets an unnamed object that will receive commands instead of the current working directory.
*/
inline
void ScriptLoader::SetCurrentTarget(Object *obj)
{
	ScriptLoader::current_target = obj;
}


/**
	Returns the currently set target object.
	@return Pointer to unnamed target object.
*/
inline
Object *ScriptLoader::GetCurrentTarget()
{
	return ScriptLoader::current_target;
}


#endif // __scriptloader_h__

// vim:ts=4:sw=4:
