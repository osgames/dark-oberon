#ifndef __CMDPROTONATIVE_H__
#define __CMDPROTONATIVE_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file cmdprotonative.h
	@ingroup Kernel_Module

 	@author Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/cmdproto.h"


//=========================================================================
// CmdProtoNative
//=========================================================================

/**
	@class CmdProtoNative
	@ingroup Kernel_Module

	@brief A factory for Cmd objects that correspond to natively implemented
	script commands.
*/

class CmdProtoNative : public CmdProto {
//--- variables
private:
    void (*cmd_proc)(void *, Cmd *);  ///< Pointer to C style command handler.


//--- methods
public:
	CmdProtoNative(const char *_proto_def, fourcc_t _id, void (*)(void *, Cmd *));
	CmdProtoNative(const CmdProtoNative& rhs);

	bool Dispatch(void *, Cmd *);
};


#endif  // __CMDPROTONATIVE_H__

// vim:ts=4:sw=4:
