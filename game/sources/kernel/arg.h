#ifndef __ARG_H__
#define __ARG_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file arg.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <stdarg.h>
#include "kernel/system.h"


//=========================================================================
// Arg
//=========================================================================

/**
	@class Arg
	@ingroup Kernel_Module

	Encapsulates a typed argument (float, int, bool, string, 
	object pointer or array) into a uniform C++ class.
*/

class Arg
{
//--- embeded
public:
	enum Type
	{ 
		AT_VOID,
		AT_INT,
		AT_FLOAT,
		AT_STRING,
		AT_BOOL,
		AT_OBJECT,
		AT_ARRAY
	};


//--- methods
public:
	Arg();
	Arg(const Arg& rhs);
	~Arg();

	void Delete();
	void Copy(const Arg &rhs);
	void Copy(va_list *marker);
	void Reset(char c);
	Type GetType() const;
	bool IsDirty() const;

	bool operator==(const Arg& rhs) const;
	bool operator!=(const Arg& rhs) const;
	Arg& operator=(const Arg& rhs);
	
	void SetI(int i);
	void SetB(bool b);
	void SetF(float f);
	void SetS(const char *s);
	void SetO(void*);
	void SetA(Arg* _a, int len);

	int   GetI() const;
	bool  GetB() const;
	float GetF() const;
	const char *GetS() const;
	void *GetO() const;
	int   GetA(Arg*&) const;

	static bool IsValidArg(char c);
	static bool IsVoid(char c);


//--- variables
private:
	Type type;
	union
	{
		int i;
		bool b;
		float f;
		char *s;
		void *o;
		Arg *a;
	};

	int arr_len;       ///< Number of nArgs in array. Only used for AT_LIST type.
	mutable bool dirty;
};


//=========================================================================
// Methods
//=========================================================================

/**
	The default constructor will initialize the arg type to AT_VOID
*/
inline 
Arg::Arg() :
	type(AT_VOID),
	s(0),
	arr_len(0),
	dirty(false)
{
    //
}


/**
	The copy constructor simply calls Arg::Copy()

	@param rhs reference to Arg object to be copied
*/
inline 
Arg::Arg(const Arg& rhs) :
	type(AT_VOID),
	s(0),
	arr_len(0)
{
	this->Copy(rhs);
}
        

/**
	Destructor.
*/
inline Arg::~Arg()
{
	this->Delete();
}


/**
	Deletes content, sets type to AT_VOID.
*/
inline
void Arg::Delete()
{
	if (this->type == AT_STRING && this->s)
	{
		delete[] this->s;
		this->s = NULL;
	}
	else if (this->type == AT_ARRAY && this->a)
	{
		delete[] this->a;
		this->a = NULL;
		this->arr_len = 0;
	}

	this->dirty = true;
	this->type = AT_VOID;
}


/**
	Copies contents. Only calls delete if necessary to avoid memory allocation
	overhead.

	@param rhs reference to Arg object to be copied
*/
inline
void Arg::Copy(const Arg& rhs)
{
	if (this->type != rhs.type)
		this->Delete();

	this->type = rhs.type;
	this->dirty = true;

	switch (this->type)
	{
	case AT_VOID:
		break;

	case AT_INT: 
		this->i = rhs.i; 
		break;
	    
	case AT_FLOAT:    
		this->f = rhs.f; 
		break;

	case AT_STRING:
		this->Delete();
		if (rhs.s)
		{   
			this->s = NEW char[strlen(rhs.s) + 1];
			strcpy(this->s, rhs.s);
		} 
		break;

	case AT_BOOL:
		this->b = rhs.b;
		break;

	case AT_OBJECT:
		this->o = rhs.o;
		break;

	case AT_ARRAY:
		this->Delete();
		if (rhs.a)
		{
			this->a = NEW Arg[rhs.arr_len];

			for (int i = 0; i < rhs.arr_len; i++)
				this->a[i].Copy(rhs.a[i]);

			this->arr_len = rhs.arr_len;
		}
		else
		{
			this->a = 0;
			this->arr_len = 0;
		}
		break;
	}
}

 
/**
	Copies contents from variable argument list marker. The type of the variable
	is known by the this->type.

	@param marker va_list parameter.
*/
inline
void Arg::Copy(va_list *marker)
{
	switch (this->type) 
	{
	case AT_VOID:
		break;

	case AT_INT:
		this->i = va_arg(*marker, int); 
		break;
	    
	case AT_FLOAT:
		// Compiler passes floats as double to variable argument functions 
		this->f = static_cast<float> (va_arg(*marker, double)); 
		break;

	case AT_STRING:
		{
			this->Delete();

			const char *str = va_arg(*marker, char *);
			if (str)
			{   
				this->s = NEW char[strlen(str) + 1];
				strcpy(this->s, str);
			} 
		}
		break;

	case AT_BOOL:     
		// bool is promoted to int when passed through ...
		this->b = (va_arg(*marker, int) != 0);
		break;

	case AT_OBJECT:
		this->o = va_arg(*marker, void *);
		break;

	case AT_ARRAY:
		Error();
		/* not possible to implement from va_list */
		break;
	}

	this->dirty = true;
}


/**
	Copies content from another Arg variable.
*/
inline
Arg& Arg::operator=(const Arg& rhs)
{
	this->Copy(rhs);
	return *this;
}


/**
	Checks whether data type and contents of the object are identical.
*/
inline
bool Arg::operator==(const Arg& rhs) const
{
	if (this->type != rhs.type)
		return false;

	switch (this->type)
	{
	case AT_VOID:
		return true;

	case AT_INT: 
		return (this->i == rhs.i); 

	case AT_FLOAT:    
		return (this->f == rhs.f); 

	case AT_STRING:
		Assert(this->s && rhs.s);
		return (strcmp(this->s, rhs.s) == 0);

	case AT_BOOL:     
		return (this->b == rhs.b); 

	case AT_OBJECT:
		return (this->o == rhs.o);

	case AT_ARRAY:
		if (this->a && rhs.a && this->arr_len == rhs.arr_len)
		{
			for (int i = 0; i < this->arr_len; i++)
			{
				if (this->a[i] != rhs.a[i])
					return false;
			}
			return true;
		}
	}        

	return true;
}    


/**
	Checks whether data type and contents of the object are not identical.
*/
inline
bool Arg::operator!=(const Arg& rhs) const
{
	return !(*this == rhs);
}


/**
	Sets the contents to an integer, and sets the arg type to AT_INT.

	@param _i The integer.
*/
inline 
void Arg::SetI(int _i)
{
	Assert((AT_VOID == this->type) || (AT_INT == this->type));

	this->type = AT_INT;
	this->i = _i;
	this->dirty = true;
}


/**
	Sets the contents to a bool value, and set the arg type to AT_BOOL.

	@param _b The bool value.
*/
inline 
void Arg::SetB(bool _b)
{
	Assert((AT_VOID == this->type) || (AT_BOOL == this->type));

	this->type = AT_BOOL;
	this->b = _b;
	this->dirty = true;
}


/**
	Sets the contents to a float value, and sets the arg type to AT_FLOAT.

	@param _f The float value.
*/
inline 
void Arg::SetF(float _f)
{
	Assert((AT_VOID == this->type) || (AT_FLOAT == this->type));

	this->type = AT_FLOAT;
	this->f = _f;
	this->dirty = true;
}


/**
	Sets the contents to a string, and sets the arg type to AT_STRING.
	The string is duplicated internally.

	@param _s The string.
*/
inline 
void Arg::SetS(const char *_s)
{
	Assert((AT_VOID == this->type) || (AT_STRING == this->type));

	this->Delete();
	this->type = AT_STRING;
	this->dirty = true;

	if (_s)
	{
		this->s = NEW char[strlen(_s) + 1];
		strcpy(this->s, _s);
	}
}


/**
	Sets the contents to an object pointer, and sets the arg type to
	AT_OBJECT. The pointer is NOT safe (if the object is destroyed,
	the pointer points to Nirvana).

	@param _o The object pointer.
*/
inline 
void Arg::SetO(void *_o)
{
	Assert((AT_VOID == this->type) || (AT_OBJECT == this->type));

	this->type = AT_OBJECT;
	this->o = _o;
	this->dirty = true;
}


/**
	Sets the contents to an array of other nArgs, and sets the arg type 
	to AT_ARRAY. The array is NOT duplicated!

	@param _a pointer to array of Arg
	@param len length of array
*/
inline void Arg::SetA(Arg* _a, int len)
{
	Assert((AT_VOID == this->type) || (AT_ARRAY == this->type));

	this->Delete();
	this->type = AT_ARRAY;
	this->dirty = true;

	if (_a)
	{
		this->a = _a;
		this->arr_len = len;
	} 
	else
	{
		this->a = NULL;
		this->arr_len = 0;
	}
}


/**
	Returns the argument type of the object, which is one of
	AT_VOID        - unitialized
	AT_INT         - integer
	AT_BOOL        - boolean
	AT_FLOAT       - float
	AT_STRING      - string
	AT_OBJECT      - pointer to object
	AT_ARRAY       - array of Args

	@return Arg type.
*/
inline 
Arg::Type Arg::GetType() const
{
	return this->type;
}


/**
	Returns and clears the dirty flag.
*/
inline
bool Arg::IsDirty() const
{
	bool res = this->dirty;
	this->dirty = false;

	return res;
}


/**
	Returns the integer content of the object. Throws assertion if
	not set to an integer.

	@return The integer value.
*/
inline 
int Arg::GetI() const
{
	Assert(AT_INT == this->type);
	return this->i;
}


/**
	Returns the bool content of the object. Throws assertion if
	not set to a bool.

	@return The bool value.
*/
inline 
bool Arg::GetB() const
{
	Assert(AT_BOOL == this->type);
	return this->b;
}


/**
	Returns the float content of the object. Throws assertion if
	not set to a float.

	@return The float value.
*/
inline 
float Arg::GetF() const
{
	Assert(AT_FLOAT == this->type);
	return this->f;
}


/**
	Returns the string content of the object. Throws assertion if
	not set to a string.

	@return The string pointer.
*/
inline 
const char *Arg::GetS() const
{
	Assert(AT_STRING == this->type);
	return this->s;    
}


/**
	Returns the object pointer content of the object. Throws assertion if
	not set to an object pointer.

	@return The object pointer.
*/
inline 
void *Arg::GetO() const
{
	Assert(AT_OBJECT == this->type);
	return this->o;
}


/**
	Returns the Arg array content of the object. Throws assertion if
	not set to a list.

	@param _a Pointer to Arg array. Will be set by this function

	@return The length of the array.
*/
inline 
int Arg::GetA(Arg*& _a) const
{
	Assert(AT_ARRAY == this->type);
	_a = this->a;
	return this->arr_len;
}


/**
	Checks if the provided data type character is a valid argument.
*/
inline
bool Arg::IsValidArg(char c)
{
	switch (c)
	{
		case 'i':
		case 'f':
		case 's':
		case 'v':
		case 'b':
		case 'o':
		case 'a':
			return true;

		default:
			return false;
	}
}


/**
	Checks if the provided data type character is void.
*/
inline
bool Arg::IsVoid(char c)
{
	return ('v' == c);
}


/**
	Sets type corresponding to data type character and reset arg contents.
*/
inline
void Arg::Reset(char c)
{
	switch (c) {
		case 'i':   this->SetI(0);      break;
		case 'f':   this->SetF(0.0f);   break;
		case 'b':   this->SetB(false);  break;
		case 's':   this->SetS(0);      break;
		case 'o':   this->SetO(0);      break;
		case 'a':   this->SetA(0,0);    break;
		default:    break;
	} 
}


#endif // __ATG_H__

// vim:ts=4:sw=4:
