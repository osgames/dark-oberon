#ifndef __threadpool_h__
#define __threadpool_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file threadpool.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007 - 2008

	@version 1.0 - Initial.
	@version 1.1 - Fixed waiting for threads.
	@version 1.2 - Improved stopping and waiting for threads.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/condition.h"
#include "kernel/threadserver.h"


//=========================================================================
// ThreadPool
//=========================================================================

/**
	@class ThreadPool
	@ingroup Kernel_Module

	@brief ThreadPool.
*/

class ThreadPool
{
//--- embeded
public:
	typedef void (*CancelFun)(void *);

private:
	struct Request
	{
		Thread::ThreadFun fun;
		Thread::WakeupFun wfun;
		void *data;
		CancelFun cancel;

		Request(Thread::ThreadFun fun = NULL, Thread::WakeupFun wfun = NULL, void *data = NULL, CancelFun cancel = NULL);
	};

	typedef std::vector<Thread *>::size_type threads_pos_t;


//--- methods
public:
	ThreadPool(ushort_t count = 1);
	virtual ~ThreadPool();

	bool Run(Thread::ThreadFun fun, Thread::WakeupFun wfun = NULL, void *data = NULL, ThreadPool::CancelFun cancel = NULL, bool front = false);
	void Stop(bool all_requests = true);
	void Wait();
	void Kill();

private:
	static void StartThread(Thread *thread);
	Request GetRequest(Thread *thread);


//--- variables
private:
	vector<Thread *> threads;
	list<Request> requests;

	Mutex *mutex;
	Condition *request_cond;

	bool can_run;
};


//=========================================================================
// Methods
//=========================================================================

inline
ThreadPool::ThreadPool(ushort_t count) :
	can_run(true)
{
	Assert(count > 0);

	// create synchronisation primitives
	this->mutex = ThreadServer::GetInstance()->CreateMutex();
	this->request_cond = ThreadServer::GetInstance()->CreateCondition();
	Assert(this->mutex && this->request_cond);

	// create threads and run starters
	Thread *thread;
	for (ushort_t i = 0; i < count; i++)
	{
		thread = ThreadServer::GetInstance()->CreateThread();
		thread->Run(StartThread, NULL, this);
		this->threads.push_back(thread);
	}
}


inline
ThreadPool::~ThreadPool()
{
	// destroy all threads
	Kill();

	// destroy synchronisation primitives
	delete this->mutex;
	delete this->request_cond;
}


inline
bool ThreadPool::Run(Thread::ThreadFun fun, Thread::WakeupFun wfun, void *data, ThreadPool::CancelFun cancel, bool front)
{
	this->mutex->Lock();

	if (!this->can_run)
	{
		this->mutex->Unlock();
		return false;
	}

	Assert(fun && !this->threads.empty());

	if (front)
		this->requests.push_front(Request(fun, wfun, data, cancel));
	else
		this->requests.push_back(Request(fun, wfun, data, cancel));

	this->request_cond->SendSignal();

	this->mutex->Unlock();
	return true;
}


/**
	@param all_requests  Whether all requests will be processed before stopping all threads.
*/
inline
void ThreadPool::Stop(bool all_requests)
{
	this->mutex->Lock();

	// add one empty request for each thread. This will stop the thread
	for (threads_pos_t i = 0; i < this->threads.size(); i++)
	{
		if (all_requests)
			this->requests.push_back(Request());
		else
			this->requests.push_front(Request());
	}

	this->can_run = false;
	this->mutex->Unlock();

	// stop all threads and delete them
	for (threads_pos_t i = 0; i < this->threads.size(); i++)
	{
		this->threads[i]->Stop();

		// delete thread object
		delete this->threads[i];
	}
	this->threads.clear();

	Assert(!all_requests || (all_requests && this->requests.empty()));

	// cancel pending requests
	list<Request>::iterator it;
	for (it = this->requests.begin(); it != this->requests.end(); it++)
	{
		if (it->cancel)
			it->cancel(it->data);
	}
	this->requests.clear();
}


inline
void ThreadPool::Kill()
{
	this->mutex->Lock();

	// kill all threads
	for (threads_pos_t i = 0; i < this->threads.size(); i++)
	{
		this->threads[i]->Kill();
		delete this->threads[i];
	}
	this->threads.clear();
	this->can_run = false;
	this->mutex->Unlock();
}


inline
ThreadPool::Request ThreadPool::GetRequest(Thread *thread)
{
	Assert(thread);
	Request result;

	this->mutex->Lock();

	while (!thread->IsStopRequested())
	{
		// if there are no requests, wait for new one. But don't wait forever.
		// If thread was busy while sending signal, signal was lost.
		if (this->requests.empty())
			this->request_cond->Wait(this->mutex, 0.5);

		if (!this->requests.empty())
		{
			result = this->requests.front();
			this->requests.pop_front();
			break;
		}

		// call the sheduler to be multitask friendly
		ThreadServer::GetInstance()->Sleep(0);
	}

	this->mutex->Unlock();

	return result;
}


inline
void ThreadPool::StartThread(Thread *thread)
{
	ThreadPool *thread_pool = (ThreadPool *)thread->GetData();
	Request request;

	// main loop. We don't use thread->IsStopRequested() in this loop, this is reserved for
	// requested function (request.fun)
	while (true)
	{
		request = thread_pool->GetRequest(thread);

		// empty request means that thread will stop
		if (!request.fun)
			break;

		thread->Thread::RunSimple(request.fun, request.wfun, request.data);

		// call the sheduler to be multitask friendly
		ThreadServer::GetInstance()->Sleep(0);
	}
}


//=========================================================================

inline
ThreadPool::Request::Request(Thread::ThreadFun fun, Thread::WakeupFun wfun, void *data, CancelFun cancel)
{
	this->fun = fun;
	this->wfun = wfun;
	this->data = data;
	this->cancel = cancel;
}


#endif  // __threadpool_h__

// vim:ts=4:sw=4:
