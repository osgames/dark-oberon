#ifndef __HASHNODE_H__
#define __HASHNODE_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file hashnode.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/strnode.h"
#include "kernel/hashtable.h"


//=========================================================================
// HashNode
//=========================================================================

/**
	@class HashNode
	@ingroup Kernel_Module

	@brief A node element in a HashList.
*/

class HashNode : public ListNode<HashNode>
{
friend class HashList;

//--- variables
private:	
	StrNode str_node;
	HashTable *h_table;

//--- methods
public:
	HashNode();
	HashNode(const string &name);

	void SetName(const string &name);
	const string &GetName() const;

	virtual void Remove();

protected:
	void SetHashTable(HashTable *t);
};


//=========================================================================
// Methods
//=========================================================================

/**
	Default constructor.
*/
inline
HashNode::HashNode() :
	ListNode<HashNode>(),
    h_table(NULL)
{
	this->str_node.SetData((void*)this);
}


/**
	Constructor with given name.
*/
inline
HashNode::HashNode(const string &name) :
	ListNode<HashNode>(),
	str_node(name),
	h_table(NULL)
{
	this->str_node.SetData((void*)this);
}


/**
	Sets hash table for this node.
*/
inline
void HashNode::SetHashTable(HashTable *t)
{
	// t can be NULL
	this->h_table = t;
}


/**
	Removes this node from hash list.
*/
inline
void HashNode::Remove()
{
	this->str_node.Remove();
	ListNode<HashNode>::Remove();

	this->h_table = 0;
}


/**
	Get name of the node.
*/
inline
const string &HashNode::GetName() const
{
	return this->str_node.GetName();
}


/**
	Sets name of node.
*/
inline
void HashNode::SetName(const string &name)
{
	if (this->IsLinked()) {
		Assert(this->h_table);

		this->str_node.Remove();
		this->str_node.SetName(name);
		this->h_table->Add(&(this->str_node));
	} 
	else 
		this->str_node.SetName(name);
}


#endif // __HASHNODE_H__

// vim:ts=4:sw=4:
