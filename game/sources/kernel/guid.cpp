//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file guid.cpp
	@ingroup Kernel_Module

 	@author RadonLabs GmbH
	@date 2003

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/guid.h"

#if WINDOWS
#	include <Rpc.h>
#	include <Rpcdce.h>

#elif defined(UNIX)
#	include <uuid/uuid.h>

#elif defined(MACOSX)
#	include <CoreFoundation/CFUUID.h>

#else
#	error "Guid not implemented"
#endif


//=========================================================================
// Guid
//=========================================================================

void Guid::Generate()
{
#if defined(WINDOWS)
	GUID guid;

	UuidCreate(&guid);
	uchar_t *str;
	UuidToString(&guid, &str);

	this->guid = (const char*)str;

	RpcStringFree(&str);

#elif defined(UNIX)
	uuid_t guid;

	uuid_generate(guid);
	Assert(!uuid_is_null(guid));

	char str[37];
	uuid_unparse(guid, str);
	this->guid = str;

#elif defined(MACOSX)
	CFUUIDRef guid = CFUUIDCreate(NULL);

	CFStringRef str = CFUUIDCreateString(NULL, guid);
	CFRelease(guid);

	char guidcstr[37];
	CFStringGetCString(str, guidcstr,
					   sizeof(guidcstr), kCFStringEncodingASCII);
	CFRelease(str);

	this->guid = guidcstr;
#endif
}


// vim:ts=4:sw=4:
