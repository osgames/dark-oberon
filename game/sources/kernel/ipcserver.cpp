//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file ipcserver.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2007 - 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/includesockets.h"
#include "kernel/ipcserver.h"
#include "kernel/tcpchannel.h"
#include "kernel/udpchannel.h"
#include "kernel/kernelserver.h"
#include "kernel/threadserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"

PrepareClass(IpcServer, "Root", s_NewIpcServer, s_InitIpcServer);


//=========================================================================
// IpcServer
//=========================================================================

/**
	@note The host name of the ipc is set to "any",
	the port name must be initialized with a valid portname.
*/
IpcServer::IpcServer(const char *id) :
	Root(id),
	tcp_listener_port(0),
	udp_listener_port(0),
	udp_broadcast_port(0),
	listener_thread(NULL),
	receiver_thread(NULL),
	receive_set(NULL),
	udp_listening(false),
	udb_shared_channel(NULL),
	udb_broadcast_channel(NULL),
	channels_mutex(NULL),
	buffer(4096)
{
	Verify(SocketUtils::InitSockets());

	// create mutex
	this->channels_mutex = ThreadServer::GetInstance()->CreateRecMutex();
	Assert(this->channels_mutex);
}


/**
	Deletes the list of current mini servers.
*/
IpcServer::~IpcServer()
{
	if (this->receiver_thread)
		this->StopReceiving();

	if (this->listener_thread)
		this->StopListening(IpcChannel::PROTOCOL_TCP);

	// delete existing channels
	this->channels_mutex->Lock();

	if (this->udp_listening)
		this->StopListening(IpcChannel::PROTOCOL_UDP);

	if (this->udb_broadcast_channel)
		this->udb_broadcast_channel->Release();

	while (this->udb_shared_channel)
		this->DestroySharedChannel();
		
	this->DisconnectAll();

	this->channels_mutex->Unlock();

	// release mutexes
	SafeDelete(this->channels_mutex);

	SocketUtils::DoneSockets();
}


bool IpcServer::StartListening(ushort_t port, IpcChannel::Protocol protocol)
{
	Assert(port > 0);
	Assert(protocol != IpcChannel::PROTOCOL_NONE);

	switch (protocol)
	{
	case IpcChannel::PROTOCOL_TCP:
		Assert(!this->listener_thread);

		this->tcp_listener_port = port;

		// start the listener thread
		this->listener_thread = ThreadServer::GetInstance()->CreateThread();
		if (!this->listener_thread)
		{
			LogError("Error creating TCP listener thread");
			return false;
		}

		this->listener_thread->Run(ListenerThreadFunc, ListenerWakeupFunc, this);
		break;

	case IpcChannel::PROTOCOL_UDP:
		Assert(!this->udp_listening);

		this->udp_listener_port = port;

		this->channels_mutex->Lock();
		Verify(this->CreateSharedChannel());
		this->channels_mutex->Unlock();

		this->udp_listening = true;
		LogInfo1("UDP listener is running on port: %hu", this->udp_listener_port);
		break;

	default:
		return false;
	}

	return true;
}


void IpcServer::StopListening(IpcChannel::Protocol protocol)
{
	Assert(protocol != IpcChannel::PROTOCOL_NONE);

	switch (protocol)
	{
	case IpcChannel::PROTOCOL_TCP:
		Assert(this->listener_thread);

		// delete listener thread
		this->listener_thread->Stop();
		SafeDelete(this->listener_thread);
		break;

	case IpcChannel::PROTOCOL_UDP:
		Assert(this->udp_listening);
		this->udp_listening = false;

		this->channels_mutex->Lock();
		this->DestroySharedChannel();
		this->channels_mutex->Unlock();

		LogInfo("UDP listener has stopped");
		break;

	default:
		break;
	}
}


bool IpcServer::StartReceiving()
{
	Assert(!this->receiver_thread);

	// start the TCP listener thread
	this->receiver_thread = ThreadServer::GetInstance()->CreateThread();
	if (!this->receiver_thread)
	{
		LogError("Error creating receiver thread");
		return false;
	}

	this->receiver_thread->Run(ReceiverThreadFunc, NULL, this);
	return true;
}


void IpcServer::StopReceiving()
{
	Assert(this->receiver_thread);

	// delete listener thread
	this->receiver_thread->Stop();
	SafeDelete(this->receiver_thread);
}


bool IpcServer::CreateSharedChannel()
{
	if (this->udb_shared_channel)
		return this->udb_shared_channel->AcquirePointer() != NULL;

	SOCKET local_socket_id = INVALID_SOCKET;
	IpcAddress local_address;

	// set local address
	local_address.SetHostName("self");
	local_address.SetPortNum(this->udp_listener_port);

	// create socket
	local_socket_id = SocketUtils::CreateUDPSocket(true);
	if (local_socket_id == INVALID_SOCKET)
		return false;

	// put socket into nonblocking mode
	SocketUtils::SetBlocking(local_socket_id, false);

	// bind address to socket
	int res = bind(local_socket_id, (const sockaddr *) &(local_address.GetAddrStruct()), sizeof(local_address.GetAddrStruct()));
	if (res == SOCKET_ERROR)
	{
		LogError("Error binding UDP listener socket");
		closesocket(local_socket_id);
		return false;
	}

	this->udb_shared_channel = NEW UdpChannel(local_socket_id);
	return this->udb_shared_channel != NULL;
}


bool IpcServer::DestroySharedChannel()
{
	Assert(this->udb_shared_channel);

	SOCKET socket_id = this->udb_shared_channel->GetSocketId();
	if (this->udb_shared_channel->Release())
	{
		shutdown(socket_id, 2);
		closesocket(socket_id);
		this->udb_shared_channel = NULL;

		return true;
	}

	return false;
}


IpcChannel *IpcServer::CreateSendingChannel()
{
	// create shared UDP channel for receiving data (one counted pointer for each sending channel)
	if (!this->CreateSharedChannel())
		return NULL;

	IpcChannel *channel;

	// create UDP channel for sending data. This channel will use the same socket as shared channel
	channel = NEW UdpChannel(this->udb_shared_channel->GetSocketId());
	if (!channel)
	{
		LogError("Error creating UDP channel");
		this->DestroySharedChannel();
	}
	channel->SetCanReceive(false);

	return channel;
}


/**
	Poll the channels for new messages.
	Has to be called frequently.
*/
inline
void IpcServer::PollChannels()
{
	IpcChannel *channel;
	SOCKET socket_id;

	// poll all channels
	this->channels_mutex->Lock();

	ChannelsList::iterator it;
	for (it = this->channels.begin(); it != this->channels.end(); )
	{
		channel = it->second;
		socket_id = channel->GetSocketId();

		// check if event occures for the socket
		if (this->receive_set && !FD_ISSET(socket_id, this->receive_set))
		{
			it++;
			continue;
		}

		// try to receive from connected channels data
		if (!channel->CanReceive())
		{
			it++;
			continue;
		}

		this->buffer = "";
		if (!channel->Receive(this->buffer))
		{
			// this ipc channel has a closed connection, delete it
			if (channel->GetProtocol() == IpcChannel::PROTOCOL_TCP)
				LogInfo1("TCP client has disconnected: %d", channel->GetChannelId());
			channel->Release();
			it = this->channels.erase(it);
		}

		else
		{
			// process received data
			if (this->buffer.GetSize())
				this->ProcessData(it->first, this->buffer);
			it++;
		}
	}

	// try to receive data from shared UDP channel
	if (this->udb_shared_channel && (!this->receive_set || FD_ISSET(this->udb_shared_channel->GetSocketId(), this->receive_set)))
	{
		this->buffer = "";
		if (this->udb_shared_channel->Receive(this->buffer) && this->buffer.GetSize())
		{
			// try to find existing UDP channel
			channel = this->FindChannel(this->udb_shared_channel->GetAddress(), IpcChannel::PROTOCOL_UDP);

			// if channel doesn't exists yet and server is listening, create new one
			if (!channel && this->udp_listening && (channel = this->CreateSendingChannel()))
			{
				const IpcAddress &addr = this->udb_shared_channel->GetAddress();
				channel->SetAddress(addr);
				this->AddChannel(channel);
				LogInfo3("UDP client has connected - IP: %s:%s, channel: %d", addr.GetHostName(), addr.GetPortName(), channel->GetChannelId());
			}

			// process received data
			if (channel && this->buffer.GetSize())
				this->ProcessData(channel->GetChannelId(), this->buffer);
		}
	}

	this->channels_mutex->Unlock();
}


bool IpcServer::Trigger()
{
	if (!this->receiver_thread)
		this->PollChannels();

	return true;
}


void IpcServer::ProcessData(uint_t channel_id, const IpcBuffer &buffer)
{
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Establish a connection to the ipc server identified by the IpcAddress
	object.
*/
uint_t IpcServer::Connect(const IpcAddress &addr, IpcChannel::Protocol protocol)
{
	Assert(protocol != IpcChannel::PROTOCOL_NONE);

	string protocol_str;
	IpcChannel::ProtocolToString(protocol, protocol_str);

	LogInfo3("Opening %s connection - IP: %s:%s", protocol_str.c_str(), addr.GetHostName(), addr.GetPortName());

	// try to find existing channel
	this->channels_mutex->Lock();

	IpcChannel *channel = this->FindChannel(addr, protocol);
	if (channel)
	{
		LogInfo4("%s connection already exists - IP: %s:%s, channel: %d", protocol_str.c_str(), addr.GetHostName(), addr.GetPortName(), channel->GetChannelId());

		channel->AcquirePointer();

		this->channels_mutex->Unlock();
		return channel->GetChannelId();
	}

	this->channels_mutex->Unlock();

	switch (protocol)
	{
	case IpcChannel::PROTOCOL_TCP:
		{
			// create socket
			SOCKET remote_socket_id = SocketUtils::CreateTCPSocket();
			if (remote_socket_id == INVALID_SOCKET)
				return 0;

			// try connection
			int res = connect(remote_socket_id, (const sockaddr*) &(addr.GetAddrStruct()), sizeof(addr.GetAddrStruct()));
			if (res == SOCKET_ERROR)
			{
				// connection failed
				LogError2("Opening TCP connection failed - IP: %s:%s", addr.GetHostName(), addr.GetPortName());
				closesocket(remote_socket_id);
				return 0;
			}

			// put socket into nonblocking mode
			SocketUtils::SetBlocking(remote_socket_id, false);

			// create IPC channel
			channel = NEW TcpChannel(remote_socket_id);
			if (!channel)
			{
				LogError("Error creating TCP channel");
				closesocket(remote_socket_id);
			}
		}
		break;

	case IpcChannel::PROTOCOL_UDP:
		channel = this->CreateSendingChannel();
		if (!channel)
			return 0;
		break;
	}

	// all ok
	channel->SetAddress(addr);
	this->AddChannel(channel);
	LogInfo4("%s connected - IP: %s:%s, channel: %d", protocol_str.c_str(), addr.GetHostName(), addr.GetPortName(), channel->GetChannelId());

	return channel->GetChannelId();
}

void IpcServer::DisconnectAll()
{
	while (!this->channels.empty())
		this->Disconnect(this->channels.begin()->second->GetChannelId());
}

void IpcServer::Disconnect(uint_t channel_id)
{
	this->channels_mutex->Lock();

	// check if channel exists
	ChannelsList::iterator it = this->channels.find(channel_id);
	if (it == this->channels.end())
	{
		this->channels_mutex->Unlock();
		return;
	}

	string protocol_str;
	IpcChannel::ProtocolToString(it->second->GetProtocol(), protocol_str);

	// delete channel
	LogInfo2("Closing %s connection: %d", protocol_str.c_str(), channel_id);
	if (it->second->Release())
	{
		if (it->second->GetProtocol() == IpcChannel::PROTOCOL_UDP)
			this->DestroySharedChannel();

		this->channels.erase(it);
		LogInfo2("%s disconnected: %d", protocol_str.c_str(), channel_id);
	}
	else
		LogInfo2("%s connection still in use: %d", protocol_str.c_str(), channel_id);

	this->channels_mutex->Unlock();
}


inline
void IpcServer::AddChannel(IpcChannel *channel)
{
	Assert(channel);

	this->channels_mutex->Lock();
	Assert(!this->channels[channel->GetChannelId()]);
	this->channels[channel->GetChannelId()] = channel;
	this->channels_mutex->Unlock();
}


inline
IpcChannel *IpcServer::FindChannel(uint_t channel_id)
{
	// chaeck if channel exists
	ChannelsList::iterator it = this->channels.find(channel_id);
	return it != this->channels.end() ? it->second : NULL;
}


inline
IpcChannel *IpcServer::FindChannel(const IpcAddress &addr, IpcChannel::Protocol protocol)
{
	Assert(protocol != IpcChannel::PROTOCOL_NONE);

	ChannelsList::iterator it;
	for (it = this->channels.begin(); it != this->channels.end(); it++)
	{
		if (it->second->GetAddress() == addr && it->second->GetProtocol() == protocol)
			return it->second;
	}

	return NULL;
}


/**
	Sends a message to a specific client.
	@return true on success
*/
bool IpcServer::Send(uint_t channel_id, const IpcBuffer &msg)
{
	this->channels_mutex->Lock();

	IpcChannel *channel = this->FindChannel(channel_id);
	if (!channel) {
		this->channels_mutex->Unlock();
		return false;
	}

	bool ok = channel->Send(msg);

	this->channels_mutex->Unlock();

	return ok;
}


/**
	Send a message to all clients.
*/
bool IpcServer::SendAll(const IpcBuffer &msg, IpcChannel::Protocol protocol)
{
	this->channels_mutex->Lock();

	ChannelsList::iterator it;
	for (it = this->channels.begin(); it != this->channels.end(); it++)
	{
		if (it->second->GetProtocol() == protocol)
			it->second->Send(msg);
	}

	this->channels_mutex->Unlock();

	return true;
}


bool IpcServer::SendBroadcast(const IpcBuffer &msg)
{
	Assert(this->udp_broadcast_port);

	// create sending channel
	if (!this->udb_broadcast_channel) {
		if (!this->udp_listener_port)
			this->udp_listener_port = this->udp_broadcast_port;

		this->udb_broadcast_channel = this->CreateSendingChannel();
	}

	IpcAddress address("broadcast", this->udp_broadcast_port);

	this->udb_broadcast_channel->SetAddress(address);

	return this->udb_broadcast_channel->Send(msg);
}


/**
	The listener thread. Opens one listening socket and accepts all incomming connections.
	An IpcChannel is created for each connection.
*/
void IpcServer::ListenerThreadFunc(Thread *thread)
{
	SOCKET local_socket_id = INVALID_SOCKET;
	SOCKET remote_socket_id = INVALID_SOCKET;
	sockaddr_in remote_addr;
	socklen_t remote_addr_size = sizeof(remote_addr);
	IpcAddress remote_address;
	IpcAddress local_address;

	// get pointer to ipc server object
	IpcServer *ipc_server = (IpcServer *)thread->GetData();

	// set local address
	local_address.SetHostName("any");
	local_address.SetPortNum(ipc_server->tcp_listener_port);

	// create socket
	local_socket_id = SocketUtils::CreateTCPSocket();
	if (local_socket_id == INVALID_SOCKET)
		return;

	// bind address to socket
	int res = bind(local_socket_id, (const sockaddr *) &(local_address.GetAddrStruct()), sizeof(local_address.GetAddrStruct()));
	if (res == SOCKET_ERROR)
	{
		LogError("Error binding listener socket");
		closesocket(local_socket_id);
		return;
	}

	// this loop waits for a new client to connect, for each
	// new client, an IpcChannel object is created
	LogInfo1("TCP listener is running on port: %s", local_address.GetPortName());

	while (!thread->IsStopRequested())
	{
		// wait for a client
		if (listen(local_socket_id, 5) == SOCKET_ERROR)
		{
			LogError("TCP listening failed");
			break;
		}

		if (thread->IsStopRequested())
		{
			closesocket(local_socket_id);
			break;
		}

		remote_socket_id = accept(local_socket_id, (sockaddr *) &remote_addr, &remote_addr_size);
		if (remote_socket_id == INVALID_SOCKET)
		{
			LogError("TCP accepting failed");
			continue;
		}

		// put remote socket into nonblocking mode
		SocketUtils::SetBlocking(remote_socket_id, false);
		remote_address.SetAddrStruct(remote_addr);

		IpcChannel *channel = NEW TcpChannel(remote_socket_id);
		channel->SetAddress(remote_address);
		LogInfo3(
			"TCP client has connected - IP: %s:%s, channel: %d",
			remote_address.GetIpAddrString(),
			remote_address.GetPortName(),
			channel->GetChannelId()
		);
		ipc_server->AddChannel(channel);

		// call the sheduler to be multitask friendly
		ThreadServer::GetInstance()->Sleep(0);
	}

	// shutdown the listener socket
	shutdown(local_socket_id, 0);
	closesocket(local_socket_id);

	LogInfo("TCP listener has stopped");
}


/**
	The wakeup func for the thread object.
	This creates a dummy connection to the current channel object
	in the listener thread in order to wakeup the thread because it is
	going to shut down.
*/
void IpcServer::ListenerWakeupFunc(Thread *thread)
{
	// get pointer to ipc server object
	IpcServer *ipc_server = (IpcServer *)thread->GetData();

	// prepare address to localhost
	sockaddr_in myAddr;
	memset(&myAddr, 0, sizeof(myAddr));
	myAddr.sin_family = AF_INET;
	myAddr.sin_port   = ipc_server->tcp_listener_port;
	myAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	// open socket_id
	SOCKET socket_id = socket(AF_INET, SOCK_STREAM, 0);
	Assert(INVALID_SOCKET != socket_id);

	// connect to listener
	connect(socket_id, (struct sockaddr *) &myAddr, sizeof(myAddr));

	// close socket_id
	shutdown(socket_id, 2);
	closesocket(socket_id);
}


void IpcServer::ReceiverThreadFunc(Thread *thread)
{
	fd_set receive_set;
	TIMEVAL timeout;
	IpcChannel *channel;
	SOCKET socket_id;

	// set timeout to 0.2 sec
	timeout.tv_sec = 0;
	timeout.tv_usec = 200;

	// get pointer to ipc server object
	IpcServer *ipc_server = (IpcServer *)thread->GetData();
	ipc_server->receive_set = &receive_set;

	// this loop waits for a new client to connect, for each
	// new client, an IpcChannel object is created
	LogInfo("IPC receiver is runnig");

	while (!thread->IsStopRequested())
	{
		// add all channel to reading and error set
		ipc_server->channels_mutex->Lock();

		FD_ZERO(&receive_set);
		ChannelsList::iterator it;
		for (it = ipc_server->channels.begin(); it != ipc_server->channels.end(); it++)
		{
			channel = it->second;
			socket_id = channel->GetSocketId();

			FD_SET(socket_id, &receive_set);
		}

		if (ipc_server->udb_shared_channel)
			FD_SET(ipc_server->udb_shared_channel->GetSocketId(), &receive_set);

		ipc_server->channels_mutex->Unlock();

		// wait for an event
		select(0, &receive_set, NULL, NULL, &timeout);

		// process events if there are any
		if (receive_set.fd_count)
		{
			ipc_server->PollChannels();

			// call the sheduler to be multitask friendly
			ThreadServer::GetInstance()->Sleep(0);
		}

		// wait for a while there is nothing to do
		else
			ThreadServer::GetInstance()->Sleep(0.1);
	}

	ipc_server->receive_set = NULL;

	LogInfo("IPC receiver has stopped");
}


// vim:ts=4:sw=4:
