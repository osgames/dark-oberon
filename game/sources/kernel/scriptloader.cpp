//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scriptloader.cpp
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005 - 2006

	@version 1.0 - Initial.
	@version 1.1 - Separated SriptLoader from ScriptServer.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/scriptloader.h"
#include "kernel/scriptserver.h"
#include "kernel/kernelserver.h"
#include "kernel/fileserver.h"
#include "kernel/logserver.h"

PrepareClass(ScriptLoader, "Root", s_NewScriptLoader, s_InitScriptLoader);


//=========================================================================
// Global variables
//=========================================================================

Object *ScriptLoader::current_target = NULL;


//=========================================================================
// ScriptLoader
//=========================================================================

/**
	Constructor.
*/
ScriptLoader::ScriptLoader(const char *id) :
	Root(id),
	fail_on_error(true)
{
	Assert(ScriptServer::GetInstance());
}


/**
	Destructor.
*/
ScriptLoader::~ScriptLoader()
{
	// empty
}


/**
	Runs a script statement.

	param  code        The statement to execute.
	param  result      Will be filled with the result.
	@return            @c True if successful.
*/
bool ScriptLoader::RunCode(const char * /*code*/, string & /*result*/)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


/**
	Runs a script function with the specified name without any args.

	param func_name    The function to invoke.
	param result       Will be filled with the result.
	@return            @c True if successful.
*/
bool ScriptLoader::RunFunction(const char * /*func_name*/, string & /*result*/)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


/**
	Runs a script file.

	param  file_path    The script file name with path.
	param  result       Will be filled with the result.
	@return             @c True if successful.
*/
bool ScriptLoader::RunScript(const string &file_path, string &result)
{
	Assert(!file_path.empty());

	// create file
	File *file = FileServer::GetInstance()->NewFile();
	Assert(file);

	// open file
	if (!file->Open(file_path, "rb"))
	{
		LogError1("Error opening file: %s", file_path.c_str());
		delete file;
		return false;
	}

	// read whole file into memory buffer
	char *buffer = file->ReadAllToString();
	if (!buffer)
	{
		LogError1("Error reading data from: %s", file_path.c_str());
		delete file;
		return false;
	}

	// close file
	file->Close();
	delete file;

	// run script command
	bool ok = this->RunCode(buffer, result);

	// deletes data
	delete buffer;

	// if some error occures, write message
	if (!ok)
	{
		if (!result.empty())
			LogError2("Error running script\nfile: %s\nmess:\n%s", file_path.c_str(), result.c_str());
		else
			LogError1("Error running script\nfile: %s", file_path.c_str());
	}

	return ok;
}


/**
	Exits interractive mode.
*/
void ScriptLoader::Exit()
{
	ScriptServer::GetInstance()->Exit();
}


// vim:ts=4:sw=4:
