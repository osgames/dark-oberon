#ifndef __server_h__
#define __server_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file server.h
	@ingroup Kernel_Module

 	@author Peter Knut, RadonLabs GmbH
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Server
//=========================================================================

/**
	@class Server
	@ingroup Kernel_Module

	@brief Server.
*/

template <class Node>
class Server
{
//--- methods
public:
	Server();
	virtual ~Server();

	virtual bool Trigger();

	static Node *GetInstance();

//--- variables
private:
	static Node *instance;
};


//=========================================================================
// Methods
//=========================================================================

template <class Node>
Node *Server<Node>::instance = NULL;


template <class Node>
inline
Server<Node>::Server()
{
	AssertMsg(instance == NULL, "Double instancing of server");

	this->instance = (Node *)this;
}


template <class Node>
inline
Server<Node>::~Server()
{
	Assert(this->instance != NULL);
	this->instance = NULL;
}


template <class Node>
inline
bool Server<Node>::Trigger()
{
	return true;
}


template <class Node>
inline
Node *Server<Node>::GetInstance()
{
	return instance;
}


#endif  // __server_h__

// vim:ts=4:sw=4:
