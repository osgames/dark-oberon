#ifndef __strnode_h__
#define __strnode_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file strnode.h
	@ingroup Kernel_Module

	@author Peter Knut, RadonLabs GmbH
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/listnode.h"


//=========================================================================
// StrNode
//=========================================================================

/**
	@class StrNode
	@ingroup Kernel_Module

	@brief Implements a named node used with StrList template.

	Node have to be only StrNode derived class!
*/

class StrNode : public ListNode<StrNode> {
//--- variables
private:
	string name;

//--- methods
public:
	StrNode(void *pdata = NULL);
	StrNode(const string &name, void *pdata = NULL);

	void SetName(const string &name);
	const string &GetName(void) const;
};


//=========================================================================
// Methods
//=========================================================================

inline
StrNode::StrNode(void *pdata) :
	ListNode<StrNode>(pdata)
{
}


inline
StrNode::StrNode(const string &name, void *pdata) :
	ListNode<StrNode>(pdata)
{
	this->name = name;
}


inline
void StrNode::SetName(const string &name)
{
	this->name = name;
};


inline
const string &StrNode::GetName(void) const
{
	return this->name;
};


#endif  // __strnode_h__

// vim:ts=4:sw=4:
