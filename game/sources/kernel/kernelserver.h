#ifndef __kernelserver_h__
#define __kernelserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file kernelserver.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/server.h"
#include "kernel/root.h"
#include "kernel/mutex.h"
#include "kernel/hashlist.h"
#include "kernel/ref.h"
#include "kernel/env.h"


//=========================================================================
// KernelServer
//=========================================================================

/**
	@class KernelServer
	@ingroup Kernel_Module

	@brief Central kernel server class.

	Manipulating with current working directory is not recomanded in multi
	threaded applications. Use full paths instead.

	Kernel server object must persists throughout the lifetime
	of the application.
*/

class KernelServer : public Server<KernelServer>
{
//--- methods
public:
	KernelServer();
	virtual ~KernelServer();

	virtual bool Trigger();

	// arguments
	void ProcessArgs(int argc, char* argv[]);
	string &GetApplicationPath();
	string &GetApplicationName();

	// classes
	void RegisterModule(void (*module)());
	void RegisterClass(const string &name, bool (*init_func)(Class *, KernelServer *), Object* (*new_func)(const char *name));

	void AddClass(const char *super_class_name, Class* cl);
	void RemClass(Class* cl);
	Class *FindClass(const char *class_name);
	const HashList *GetClassList() const;
	
	// objects creating
	Root *New(const char *class_name, const string &path, bool fail = true);
	Root *New(const string &class_name, const string &path, bool fail = true);

	Object *NewObject(const char *class_name, const char *name, bool fail = true);
	Object *NewObject(const string &class_name, const char *name, bool fail = true);

	// current directory
	void PopCwd();
	void PushCwd(Root *node);
	void PushCwd(const string &path);
	void SetCwd(Root *node);
	void SetCwd(const string &path);
	Root *GetCwd();

	// objects hierarchy
	Root *Lookup(const string &path);
	void LogTree();

protected:
	KernelServer(bool init);

	void Init();
	void Done();
	void Lock() const;
	void Unlock() const;

	// objects hierarchy
	void AddNode(Root *node, const string &path = "");
	Root *CheckPath(const string &path, bool create);
	bool IsAbsolutePath(const string &path);


//--- variables
protected:
	Root *root;
	Mutex *mutex;
	vector<Root *> cwd;
	string app_path;
	string app_name;
	bool inited;

	HashList class_list;            // list of Class objects

#if defined(USE_PROFILERS) && defined(DEBUG_MEMORY)
	Ref<Env> memory_variable;
	Ref<Env> blocks_variable;
#endif
};


//=========================================================================
// Methods
//=========================================================================

inline
string &KernelServer::GetApplicationPath()
{
	return this->app_path;
}


inline
string &KernelServer::GetApplicationName()
{
	return this->app_name;
}


inline
Root *KernelServer::New(const string &class_name, const string &path, bool fail)
{
	return this->New(class_name.c_str(), path, fail);
}


inline
Object *KernelServer::NewObject(const string &class_name, const char *name, bool fail)
{
	return this->NewObject(class_name.c_str(), name, fail);
}


inline
void KernelServer::PopCwd()
{
	Assert(cwd.back() != this->root);
	cwd.pop_back();
}


inline
void KernelServer::PushCwd(Root *node)
{
	this->Lock();

	cwd.push_back(node ? node : root);

	this->Unlock();
}


inline
void KernelServer::PushCwd(const string &path)
{
	Assert(!path.empty());

	this->Lock();

	Root *node = this->Lookup(path);
	Assert(node);

	this->PushCwd(node);

	this->Unlock();
}


inline
void KernelServer::SetCwd(Root *node)
{
	this->Lock();

	this->cwd.clear();
	this->PushCwd(node);

	this->Unlock();
}


inline
void KernelServer::SetCwd(const string &path)
{
	this->Lock();

	this->cwd.clear();
	this->PushCwd(path);

	this->Unlock();
}


inline
Root *KernelServer::GetCwd()
{
	this->Lock();
	Root *obj = this->cwd.back();
	this->Unlock();

	return obj;
}


inline
Root *KernelServer::Lookup(const string &path)
{
	Assert(!path.empty());

	this->Lock();
	Root *obj = this->CheckPath(path, false);
	this->Unlock();

	return obj;
}


inline
void KernelServer::LogTree()
{
	this->Lock();
	this->root->LogTree(string());
	this->Unlock();
}


inline
bool KernelServer::IsAbsolutePath(const string &path)
{
    Assert(!path.empty());
    return (path[0] == '/');
}


inline
void KernelServer::Lock() const
{
	this->mutex->Lock();
}


inline
void KernelServer::Unlock() const
{
	this->mutex->Unlock();
}


#endif  // __kernelserver_h__

// vim:ts=4:sw=4:
