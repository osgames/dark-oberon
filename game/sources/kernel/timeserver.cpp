//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file timeserver.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - System variables.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/timeserver.h"
#include "kernel/kernelserver.h"
#include "kernel/profiler.h"


PrepareScriptClass(TimeServer, "Root", s_NewTimeServer, s_InitTimeServer, s_InitTimeServer_cmds);


//=========================================================================
// TimeServer
//=========================================================================

TimeServer::TimeServer(const char *id) :
	RootServer<TimeServer>(id),
	started(false),
	start_time(0),
	time(0),
	frame_time(0),
	min_frame_time(0),
	fix_frame_time(0),
	wait_frame_time(false),
	frame(0),
	fps_last_frame(0),
	fps_last_time(0),
	fps_interval(0.3),
	fps_updated(false),
	fps(0),
	fps_show(false),
	profilers(NULL)
{
	// create profilers list
	this->kernel_server->PushCwd(this);
	profilers = kernel_server->New("Root", string("profilers"));
	this->kernel_server->PopCwd();
}


TimeServer::~TimeServer()
{
	//
}


void TimeServer::ShowFPS(bool show)
{
	this->fps_show = show;

	// create system variable for FPS
	if (show)
	{
		if (!this->fps_variable.IsValid())
			this->fps_variable = (Env *)this->kernel_server->New("Env", NOH_VARIABLES + "/FPS");
	}

	// remove FPS variable
	else if (this->fps_variable.IsValid())
		this->fps_variable->Release();
}


bool TimeServer::Trigger()
{
	if (!this->started)
		return false;

	double real_time = this->GetRealTime();
	this->frame_time = real_time - this->time - this->start_time;

	if (this->fix_frame_time)
	{
		if (this->frame_time < this->fix_frame_time && this->wait_frame_time)
		{
			ThreadServer::GetInstance()->Sleep(this->fix_frame_time - this->frame_time);
			real_time = this->GetRealTime();
			this->frame_time = real_time - this->time - this->start_time;
		}

		this->start_time += this->frame_time - this->fix_frame_time;
		this->frame_time = this->fix_frame_time;
	}

	else if (this->frame_time < this->min_frame_time)
	{
		// wait for min frame time
		if (this->wait_frame_time)
		{
			static double sleep_time = 0.0;
			static double real_time_after;

			sleep_time += this->min_frame_time - this->frame_time;

			if (sleep_time >= 0.001)
				ThreadServer::GetInstance()->Sleep(sleep_time);

			real_time_after = this->GetRealTime();
			sleep_time -= real_time_after - real_time;

			real_time = real_time_after;
			this->frame_time = real_time - this->time - this->start_time;
		}

		// increase time and frame time
		else
		{
			this->start_time -= this->min_frame_time - this->frame_time;
			this->frame_time = this->min_frame_time;
		}
	}

	this->time = real_time - this->start_time;

	// update fps and profilers
	this->frame++;
	if (this->time - this->fps_last_time >= this->fps_interval)
	{
		// update fps
		this->fps = float((this->frame - this->fps_last_frame) / (this->time - this->fps_last_time));

		this->fps_last_frame = this->frame;
		this->fps_last_time = this->time;
		this->fps_updated = true;

		if (this->fps_variable.IsValid())
			this->fps_variable->SetF(this->fps);

#ifdef USE_PROFILERS
		// update profilers
		Profiler *profiler;
		float percentage = 0;
		for (profiler = (Profiler *)this->profilers->GetFront(); profiler; profiler = (Profiler *)profiler->GetNext())
		{
			profiler->Update();
			percentage += profiler->GetPercentage();
		}

		if (profiler_variable.IsValid())
			profiler_variable->SetF(percentage < 100 ? 100 - percentage : 0);
#endif
	}	

	return true;
}


#ifdef USE_PROFILERS
Profiler *TimeServer::NewProfiler(const string &name)
{
	Profiler *profiler;

	if (!profiler_variable.IsValid())
		profiler_variable = (Env *)this->kernel_server->New("Env", NOH_VARIABLES + "/% other");

	this->kernel_server->PushCwd(this->profilers);
	profiler = (Profiler *)this->kernel_server->New("Profiler", name);
	this->kernel_server->PopCwd();

	return profiler;
}
#endif


// vim:ts=4:sw=4:
