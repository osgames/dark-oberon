#ifndef __thread_h__
#define __thread_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file thread.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007 - 2008

	@version 1.0 - Initial.
	@version 1.1 - Fixed IsRunning() method.
	@version 1.2 - Improved stopping and waiting for thread.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Thread
//=========================================================================

/**
	@class Thread
	@ingroup Kernel_Module

	@brief Thread.
*/

class Thread
{
	friend class ThreadServer;
	friend class ThreadPool;

//--- embeded
public:
	typedef void (*ThreadFun)(Thread *thread);
	typedef void (*WakeupFun)(Thread *thread);


//--- methods
public:
	virtual ~Thread();

	virtual void Run(ThreadFun fun, ThreadFun wfun = NULL, void *data = NULL);
	virtual void Stop(bool wait = true);
	virtual void Kill() {};
	virtual void Wait() {};

	bool IsRunning() const;
	bool IsStopRequested() const;
	void *GetData() const;

protected:
	Thread();
	void RunSimple(ThreadFun fun, ThreadFun wfun = NULL, void *data = NULL);


//--- variables
protected:
	ThreadFun wakeup_fun;
	bool stop_requested;
	void *data;
	bool running;
};


//=========================================================================
// Methods
//=========================================================================

inline
Thread::Thread() :
	wakeup_fun(NULL),
	stop_requested(false),
	data(NULL),
	running(false)
{
	//
}


inline
Thread::~Thread()
{
	Assert(!this->running);
}


inline
bool Thread::IsRunning() const
{
	return this->running;
}


inline
void Thread::Run(ThreadFun fun, ThreadFun wfun, void *data)
{
	Assert(fun);

	this->running = true;
	this->RunSimple(fun, wfun, data);
	this->running = false;
}


inline
void Thread::RunSimple(ThreadFun fun, ThreadFun wfun, void *data)
{
	Assert(fun);

	this->wakeup_fun = wfun;
	this->data = data;
	this->stop_requested = false;

	fun(this);
}


inline
void Thread::Stop(bool wait)
{
	this->stop_requested = true;

	if (this->wakeup_fun)
		this->wakeup_fun(this);

	if (wait)
		this->Wait();
}


inline
bool Thread::IsStopRequested() const
{
	return this->stop_requested;
}


inline
void *Thread::GetData() const
{
	return this->data;
}


#endif  // __thread_h__

// vim:ts=4:sw=4:
