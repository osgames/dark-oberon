#ifndef __socketutils_h__
#define __socketutils_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file socketutils.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2009

	@version 1.0 - Initial.
*/

//=========================================================================
// Includes
//=========================================================================

#include "kernel/includesockets.h"


//=========================================================================
// SocketUtils
//=========================================================================

/**
*/

class SocketUtils
{
//--- methods
public:
	static bool InitSockets();
	static void DoneSockets();

	static SOCKET CreateTCPSocket();
	static SOCKET CreateUDPSocket(bool can_broadcast);
	static void SetBlocking(SOCKET socket_id, bool block);

//--- variables
protected:
	static int init_count;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Initializes sockets, which are used for networking.

	@return @c true when no error, @c false otherwise.

	@note On Windows it calls WSAStartup() and must be called for every thread
	using sockets. On Unix it ignores the SIGPIPE signal; it should be called
	once but may be called repeatedly.
*/
inline
bool SocketUtils::InitSockets()
{
	if (SocketUtils::init_count) {
		SocketUtils::init_count++;
		return true;
	}

	LogInfo("Initializing sockets");

#ifdef WINDOWS
	WSADATA data;

	// init Windows sockets
	if (WSAStartup (MAKEWORD (1,1), &data) != 0)
	{
		LogCritical("Can not initialize sockets");
		return false;
	}

	// write status about the Windows sockets
	LogInfo4("%s (version %d.%d) %s",
		data.szDescription,
		LOBYTE (data.wVersion),
		HIBYTE (data.wVersion),
		data.szSystemStatus
	);
#endif

#ifdef UNIX
	// avoid "Broken pipe"
	signal (SIGPIPE, SIG_IGN);
#endif

	SocketUtils::init_count++;
	return true;
}


/**
	Ends the work with network sockets.

	@note This function is actualy needed only for #WINDOWS.
*/
inline
void SocketUtils::DoneSockets()
{
	if (--SocketUtils::init_count)
		return;

	LogInfo("Closing sockets");

#ifdef WINDOWS
	WSACleanup();
#endif
}


inline
SOCKET SocketUtils::CreateTCPSocket()
{
	// create a socket
	SOCKET socket_id = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_id == INVALID_SOCKET)
	{
		LogError("Error creating socket");
		return INVALID_SOCKET;
	}

	// configure the socket
	#ifdef WINDOWS
		char yes = 1;
	#else
		int yes = 1;
	#endif
	
	int res = setsockopt(socket_id, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
	if (res == SOCKET_ERROR)
	{
		LogError("Error setting options for socket");
		closesocket(socket_id);
		return INVALID_SOCKET;
	}

	return socket_id;
}


inline
SOCKET SocketUtils::CreateUDPSocket(bool can_broadcast = false)
{
	// create a socket
	SOCKET socket_id = socket(AF_INET, SOCK_DGRAM, 0);
	if (socket_id == INVALID_SOCKET)
	{
		LogError("Error creating socket");
		return INVALID_SOCKET;
	}

	// configure the socket
	#ifdef WINDOWS
		char yes = 1;
	#else
		int yes = 1;
	#endif
	
	int res = setsockopt(socket_id, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes));
	if (res == SOCKET_ERROR)
	{
		LogError("Error setting options for socket");
		closesocket(socket_id);
		return INVALID_SOCKET;
	}

	if (can_broadcast) {
		int res = setsockopt(socket_id, SOL_SOCKET, SO_BROADCAST, &yes, sizeof(yes));
		if (res == SOCKET_ERROR)
		{
			LogError("Error setting options for socket");
			closesocket(socket_id);
			return INVALID_SOCKET;
		}
	}

	return socket_id;
}


inline
void SocketUtils::SetBlocking(SOCKET socket_id, bool block)
{
	// disable blocking
	if (block)
	{
		#ifdef WINDOWS
			u_long bool_as_ulong = 0;
			int res = ioctlsocket(socket_id, FIONBIO, &bool_as_ulong);
		#else
			int flags;
			flags = fcntl(socket_id, F_GETFL);
			flags &= ~O_NONBLOCK;
			int res = fcntl(socket_id, F_SETFL, flags);
		#endif
		Assert(!res);
	}

	// enable blocking
	else
	{
		#ifdef WINDOWS
			u_long bool_as_ulong = 1;
			int res = ioctlsocket(socket_id, FIONBIO, &bool_as_ulong);
		#else
			int flags;
			flags = fcntl(socket_id, F_GETFL);
			flags |= O_NONBLOCK;
			int res = fcntl(socket_id, F_SETFL, flags);
		#endif
		Assert(!res);
	}
}


#endif // __socketutils_h__

// vim:ts=4:sw=4:
