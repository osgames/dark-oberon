#ifndef __memory_h__
#define __memory_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file memory.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/config.h"
#include "kernel/types.h"


//========================================================================
// Operators
//========================================================================

#ifdef DEBUG_MEMORY

#	define NEW new(__FILE__,__LINE__)

void* operator new(size_t size);
void* operator new(size_t size, const char *file, int line);
void* operator new[](size_t size);
void* operator new[](size_t size, const char *file, int line);

void operator delete(void* p);
void operator delete(void* p, const char *file, int line);
void operator delete[](void* p);
void operator delete[](void* p, const char *file, int line);


//========================================================================
// Methods
//========================================================================

void OpenMemorySystem();
void CloseMemorySystem();
void EnableMemorySystem();
void DisableMemorySystem();

size_t GetActMemorySize();
uint_t GetActMemoryBlocks();


#else // DEBUG_MEMORY

#	define NEW new


//========================================================================
// Methods
//========================================================================

inline
void OpenMemorySystem() {}

inline
void CloseMemorySystem() {}

inline
void EnableMemorySystem() {}

inline
void DisableMemorySystem() {}

inline
size_t GetActMemorySize() { return 0; }

inline
uint_t GetActMemoryBlocks() { return 0; }


#endif // DEBUG_MEMORY


#endif  // __memory_h__

// vim:ts=4:sw=4:
