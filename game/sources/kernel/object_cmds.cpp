//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file object_cmds.cpp
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/object.h"
#include "kernel/kernelserver.h"


//=========================================================================
// Commands
//=========================================================================


/**
	Returns name of class which the object is an instance of.
*/
static void s_GetClassName(void *o, Cmd *cmd)
{
	Object *self = (Object *)o;
	cmd->Out()->SetS(self->GetClass()->GetName().c_str());
}


/**
	Returns the list of classes which the object is an instance of.
*/
static void s_GetClasses(void *o, Cmd *cmd)
{
	Object *self = (Object *)o;
	Class* cls;

	// count classes
	int cls_count = 0;
	for (cls = self->GetClass(); cls; cls = cls->GetSuperClass())
		cls_count++;

	// allocate
	Arg* args = NEW Arg[cls_count];

	// and fill
	int i = 0;
	cls = self->GetClass();
	do {
		args[i++].SetS(cls->GetName().c_str());
	}
	while ((cls = cls->GetSuperClass()));

	cmd->Out()->SetA(args, cls_count);
}


/**
	Checkes whether the object is instantiated or derived from the
	class given by its name.
*/
static void s_IsA(void *o, Cmd *cmd)
{
	Object *self = (Object *)o;
	const char *arg0 = cmd->In()->GetS();
	Class *cl = KernelServer::GetInstance()->FindClass(arg0);

	if (cl)
		cmd->Out()->SetB(self->IsA(cl));
	else
		cmd->Out()->SetB(false);
}


/**
	Checkes whether the object is an instance of the class given
	by its name.
*/
static void s_IsInstanceOf(void *o, Cmd *cmd)
{
	Object *self = (Object *)o;
	const char *arg0 = cmd->In()->GetS();
	Class *cl = KernelServer::GetInstance()->FindClass(arg0);

	if (cl)
		cmd->Out()->SetB(self->IsInstanceOf(cl));
	else
		cmd->Out()->SetB(false);
}


/**
    Returns a list of all script command prototypes the object accepts.
*/
static void s_GetCommands(void *o, Cmd *cmd)
{
	Object *self = (Object *)o;
	HashList cmds_list;
	HashNode* node;
	int cmds_count = 0;

	// count commands
	self->GetCmdProtos(&cmds_list);
	for (node = cmds_list.GetFront(); node; node = node->GetNext())
		cmds_count++;

	// allocate
	Arg* args = NEW Arg[cmds_count];
	int i = 0;

	// fill list
	while ((node = cmds_list.PopFront())) {
		args[i++].SetS(((CmdProto*) node->GetData())->GetProtoDef());
		delete node;
	}

	cmd->Out()->SetA(args, cmds_count);
}


/**
	Gets byte size of this object. This may or may not accurate,
	depending on whether the object uses external allocated memory,
	and if the object's class takes this into account.
*/
static void s_GetInstanceSize(void* o, Cmd* cmd)
{
	Object* self = (Object*)o;
	cmd->Out()->SetI(self->GetInstanceSize());
}


void s_InitObject_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("s_GetClassName_v",      'GCLS', s_GetClassName);
	cl->AddCmd("a_GetClasses_v",        'GCLL', s_GetClasses);
	cl->AddCmd("b_IsA_s",               'ISA_', s_IsA);
	cl->AddCmd("b_IsInstanceOf_s",      'ISIO', s_IsInstanceOf);
	cl->AddCmd("a_GetCommands_v",       'GMCD', s_GetCommands);
	cl->AddCmd("a_GetInstanceSize_v",   'GISZ', s_GetInstanceSize);

	cl->EndCmds();
}


// vim:ts=4:sw=4:
