//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file tcpchannel.cpp
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/tcpchannel.h"
#include "kernel/ipcbuffer.h"
#include "kernel/logserver.h"


//=========================================================================
// TcpChannel
//=========================================================================

/**
*/
TcpChannel::TcpChannel(SOCKET sock) :
	IpcChannel(sock)
{
	this->protocol = PROTOCOL_TCP;
}


/**
*/
TcpChannel::~TcpChannel()
{
	this->DestroySocket();
}


/**
	Checks if an incoming message is available, if yes, reads the message
	and adds a new MsgNode to the parent IpcServer's message list.

	If no message is pending, just do nothing.

	Handles handshakes and close requests internally.

	@return  @c false if socket_id has been closed.
*/
bool TcpChannel::Receive(IpcBuffer &buffer)
{
	Assert(this->can_receive);

	if (this->socket_id == INVALID_SOCKET)
		return true;
	
	// do a non-blocking recv
	int len = recv(this->socket_id, buffer.GetPointer(), int(buffer.GetMaxSize()), 0);

	// the connection has been gracefully closed
	if (!len)
	{
		LogInfo1("TCP connection closed gracefully: %d", this->channel_id);
		this->DestroySocket();
		return false;
	}

	if (len == SOCKET_ERROR)
	{		
		switch (N_SOCKET_LAST_ERROR)
		{
		case N_EWOULDBLOCK:
			buffer.SetSize(0);
			return true;

		case N_ECONNRESET:
			LogInfo1("TCP connection closed hard: %d", this->channel_id);
			this->DestroySocket();
			return false;

		default:
			LogInfo1("Receiving TCP data failed: %d", this->channel_id);
			this->DestroySocket();
			return false;
		}
	}

	// a message was received
	buffer.SetSize(len);
	return true;
}


/**
*/
bool TcpChannel::Send(const IpcBuffer &buffer)
{
	if (this->socket_id == INVALID_SOCKET)
		return false;

	int result;
	int pos = 0;

	do
	{
		result = send(this->socket_id, buffer.GetPointer() + pos, int(buffer.GetSize()) - pos, 0);

		if ((result == SOCKET_ERROR) || (result != buffer.GetSize()))
		{
			LogError("Sending TCP data failed");
			return false;
		}

		pos += result;
	} while (pos < int(buffer.GetSize()));

	return true;
}


// vim:ts=4:sw=4:
