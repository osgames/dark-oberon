#ifndef __file_h__
#define __file_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file file.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added PrintF and Flush methods.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"

#include <stdio.h>


//=========================================================================
// File
//=========================================================================

/**
	@class File
	@ingroup Kernel_Module

	@brief Wrapper class for accessing file system files.
	Provides functions for reading and writing files.
*/

class File
{
	friend class FileServer;

//--- embeded
public:
	/// Start point for seeking in file.
	enum SeekType
	{
		ST_CURRENT,
		ST_START,
		ST_END
	};

//--- variables
protected:
	ushort_t act_line;        ///< Actual line number.
	bool opened;              ///< Whether file is opened.
	bool binary;              ///< Whether file was opend as binary;

	FILE *file;               ///< Ansi C file pointer.
	string file_path;         ///< Path to the file.

//--- methods
public:
	virtual ~File();

	bool Open(const string &fileName, const char *accessMode);
	void Close();

	// info
	bool Eof() const;
	long GetSize() const;
	bool IsOpened() const;
	bool IsBinary() const;
	int GetLine() const;
	const string &GetFilePath() const;

	long Tell() const;
	bool Seek(long offset, SeekType origin);
	bool Flush();
	
	// read/write
	size_t Write(const void *buffer, size_t size);
	size_t Read(void *buffer, size_t size);
	byte_t *ReadAll();
	char *ReadAllToString();
	bool WriteLine(const string &line);
	bool PrintF(const char *format, ...);
	bool ReadLine(string &line);

	size_t WriteInt(int val);
	size_t WriteShort(short val);
	size_t WriteLong(long val);
	size_t WriteChar(char val);
	size_t WriteUInt(unsigned int val);
	size_t WriteUShort(unsigned short val);
	size_t WriteULong(unsigned long val);
	size_t WriteUChar(unsigned char val);
	size_t WriteFloat(float val);
	size_t WriteDouble(double val);

	int ReadInt();
	short ReadShort();
	long ReadLong();
	char ReadChar();
	unsigned int ReadUInt();
	unsigned short ReadUShort();
	unsigned long ReadULong();
	unsigned char ReadUChar();
	float ReadFloat();
	double ReadDouble();

	// callbacks used in external libraries
	static size_t OnRead(void *ptr, size_t size, size_t nmemb, void *source);
	static size_t OnWrite(const void *ptr, size_t size, size_t count, void *source);
	static int OnSeek(void *source, long offset, int whence);
	static int OnSeek64(void *source, __int64 offset, int whence);
	static long OnTell(void *source);
	static int OnClose(void *source);
	static int OnFlush(void *source);

protected:
	File();
	File(FILE *file, const char *file_path, bool binary);
};



//=========================================================================
// Methods
//=========================================================================

/**
	Determines whether the file is opened.
*/
inline
bool File::IsOpened() const
{
	return this->opened;
}


/**
	Determines whether the file was opened with binary flag.
*/
inline
bool File::IsBinary() const
{
	return this->binary;
}


/**
	Gets current line number (incremented by WriteLine() and ReadLine())
	@return     The current line number.
*/
inline
int File::GetLine() const
{
	return this->act_line;
}


/**
	Gets full file name and path.
	@return     File path.
*/
inline
const string &File::GetFilePath() const
{
	return this->file_path;
}


/**
	Writes a 32bit integer to the file.

	@param   val    A 32 bit int value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteInt(int val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a 16bit integer to the file.

	@param  val     A 16 bit int value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteShort(short val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a 64bit integer to the file.

	@param  val     A 64 bit int value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteLong(long val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a 8bit integer to the file.

	@param  val     A 8 bit int value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteChar(char val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a unsigned 32bit integer to the file.

	@param   val    A 32 unsigned bit int value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteUInt(unsigned int val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a unsigned 16bit integer to the file.

	@param  val     A unsigned 16 bit int value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteUShort(unsigned short val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a unsigned 64bit integer to the file.

	@param  val     A unsigned 64 bit int value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteULong(unsigned long val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a unsigned 8bit integer to the file.

	@param  val     A unsigned 8 bit int value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteUChar(unsigned char val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a float to the file.

	@param  val     A 32 bit float value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteFloat(float val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Writes a double to the file.

	@param  val     A 64 bit double value.
	@return         Number of bytes written.
*/
inline
size_t File::WriteDouble(double val)
{
	return this->Write(&val, sizeof(val));
}


/**
	Reads a 32 bit integer from the file.

	@return     The value.
*/
inline
int File::ReadInt()
{
	int val = 0;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a 16 bit integer from the file.

	@return     The value.
*/
inline
short File::ReadShort()
{
	short val = 0;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a 64 bit integer from the file.

	@return     The value.
*/
inline
long File::ReadLong()
{
	long val = 0;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a 8 bit integer from the file.

	@return     The value.
*/
inline
char File::ReadChar()
{
	char val;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a unsigned 32 bit integer from the file.

	@return     The value.
*/
inline
unsigned int File::ReadUInt()
{
	unsigned int val = 0;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a unsigned 16 bit integer from the file.

	@return     The value.
*/
inline
unsigned short File::ReadUShort()
{
	unsigned short val = 0;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a unsigned 64 bit integer from the file.

	@return     The value.
*/
inline
unsigned long File::ReadULong()
{
	unsigned long val = 0;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a 8 unsigned bit integer from the file.

	@return     The value.
*/
inline
unsigned char File::ReadUChar()
{
	unsigned char val;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a float from the file.

	@return     The value.
*/
inline
float File::ReadFloat()
{
	float val;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Reads a double from the file.

	@return     The value.
*/
inline
double File::ReadDouble()
{
	double val;
	this->Read(&val, sizeof(val));
	return val;
}


/**
	Flushes a stream.
*/
inline
bool File::Flush()
{
	Assert(this->IsOpened());
	return fflush(this->file) == 0;
}


//=========================================================================
// Callback methods
//=========================================================================

/**
	Wraps our file access into callback function used in external libraries.

	@param ptr     Pointer to memory buffer.
	@param size    Size of block.
	@param count   Number of blocks we want to read.
	@param source  Pointer to File object.

	@return Size of read data.
*/
inline
size_t File::OnRead(void *ptr, size_t size, size_t count, void *source)
{
	// call method from File object
	File* fp = reinterpret_cast<File *>(source);
	return fp->Read(ptr, size * count);
}


/**
	Wraps our file access into callback function used in external libraries.

	@param ptr     Pointer to memory buffer.
	@param size    Size of block.
	@param count   Number of blocks we want to write.
	@param source  Pointer to File object.

	@return Size of written data.
*/
inline
size_t File::OnWrite(const void *ptr, size_t size, size_t count, void *source)
{
	// call method from File object
	File* fp = reinterpret_cast<File *>(source);
	return fp->Write(ptr, size * count);
}


/**
	Wraps our file access into callback function used in external libraries.

	@param source  Pointer to File object.
	@param offset  Offset for seeking.
	@param whence  Start point for seeking.

	@return 0 if successful.
*/
inline
int File::OnSeek(void *source, long offset, int whence)
{
	// convert start point
	File::SeekType dir;

	switch (whence)
	{
	case SEEK_SET:  dir = File::ST_START; break;
	case SEEK_CUR:  dir = File::ST_CURRENT; break;
	case SEEK_END:  dir = File::ST_END; break;
	default:        return -1;
	}

	// call method from File object
	File* fp = reinterpret_cast<File *>(source);
	return fp->Seek(offset, dir) ? 0 : -1;
}


/**
	Wraps our file access into callback function used in external libraries.

	@param source  Pointer to File object.
	@param offset  Offset for seeking.
	@param whence  Start point for seeking.

	@return 0 if successful.
*/
inline
int File::OnSeek64(void *source, __int64 offset, int whence)
{
	return File::OnSeek(source, (long)offset, whence);
}


/**
	Wraps our file access into callback function used in external libraries.

	@param source  Pointer to File object.

	@return Actual position in the file.
*/
inline
long File::OnTell(void *source)
{
	// call method from File object
	File* fp = reinterpret_cast<File *>(source);
	return fp->Tell();
}


/**
	Wraps our file access into callback function used in external libraries.

	@param source  Pointer to File object.

	@return 0 if successful.
*/
inline
int File::OnClose(void *source)
{
	// call method from File object
	File* fp = reinterpret_cast<File*>(source);
	fp->Close();

	return 0;
}


/**
	Wraps our file access into callback function used in external libraries.

	@param source  Pointer to File object.

	@return 0 if successful.
*/
inline
int File::OnFlush(void *source)
{
	// call method from File object
	File *fp = reinterpret_cast<File*>(source);
	return fp->Flush();
}


#endif // __file_h__

// vim:ts=4:sw=4:
