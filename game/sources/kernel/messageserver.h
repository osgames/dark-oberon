#ifndef __messageserver_h__
#define __messageserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file messageserver.h
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/rootserver.h"
#include "kernel/messages.h"
#include "kernel/ref.h"


class Profiler;
class TimeServer;


//=========================================================================
// MessageServer
//=========================================================================

/**
	@class MessageServer
	@ingroup Kernel_Module

	Delivers messages to receivers.
*/

class MessageServer : public RootServer<MessageServer>
{
//--- methods
public:
	MessageServer(const char *id);
	virtual ~MessageServer();

	bool Run();
	void Stop();

	void SendMessage(BaseMessage *message, Root *sender, Root *destination, double delay = 0.0);
	void CancelMessage(BaseMessage *message);

private:
	static void OnProcess(Thread *thread);

//--- varialbles
private:
	bool running;
	List<BaseMessage> queue;             ///< Queue of messages to deliver immediately.
	Ref<TimeServer> time_server;
	Thread *thread;
	Semaphore *semaphore;

protected:
#ifdef USE_PROFILERS
	Ref<Profiler> messages_profiler;  ///< Profiler for messages processing.
#endif
};


#endif // __messageserver_h__

// vim:ts=4:sw=4:
