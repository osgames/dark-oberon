#ifndef __CLASS_H__
#define __CLASS_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file class.h
	@ingroup Kernel_Module

 	@author Andre Weissflog, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/cmdproto.h"
#include "kernel/keyarray.h"


class KernelServer;
class Object;
class CmdProtoNative;
class HashList;


//=========================================================================
// Class
//=========================================================================

/**
	@class Class
	@ingroup Kernel_Module

	Nebula metaclass. Root derived objects are not created directly
	in C++, but by Class objects. Class objects wrap dynamic demand-loading
	of classes, and do other householding stuff.
*/

class Class : public HashNode {
//--- variables
private:
	KernelServer *kernel_server;
	Class *super_class;
	HashList *cmd_list;
	KeyArray<CmdProto*> *cmd_table;
	HashList *script_cmd_list;                          ///< The hashed script commandlist of this class.
	int ref_count;
	int instance_size;

	bool (*s_init_ptr)(Class *, KernelServer *);        ///< Pointer to class init function.
	Object *(*s_new_ptr)(const char *name);                   ///< Pointer to object construction function.

//--- methods
public:
	Class(const string &name, KernelServer *ks, bool (*init_func)(Class *, KernelServer *), Object* (*new_func)(const char *name));
	~Class();

	Object* NewObject(const char *name);

	void BeginCmds();
	void AddCmd(CmdProto * cmd_proto);
	void AddCmd(const char *proto_def, fourcc_t id, void (*)(void *, Cmd *));
	void EndCmds();

	void BeginScriptCmds(int cmds_count);
	void AddScriptCmd(CmdProto*);
	void EndScriptCmds();

	CmdProto *FindCmdByName(const string &name);
	CmdProtoNative *FindNativeCmdByName(const string &name);
	CmdProto *FindScriptCmdByName(const string &name);
	CmdProto *FindCmdById(fourcc_t id);

	HashList *GetCmdList() const;
	Class *GetSuperClass() const;
	void AddSubClass(Class* cl);
	void RemSubClass(Class* cl);

	int AddRef();
	int RemRef();
	int GetRefCount() const;

	void SetInstanceSize(int size);
	int GetInstanceSize() const;

	bool IsA(const string &className) const;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Sets instance size.
*/
inline
void Class::SetInstanceSize(int size)
{
	this->instance_size = size;
}


/**
	Gets instance size.
*/
inline
int Class::GetInstanceSize() const
{
	return this->instance_size;
}


/**
	Gets super class of this class.
*/
inline
Class *Class::GetSuperClass() const
{
	return this->super_class;
}


/**
	Adds a sub class to this class.
*/
inline
void Class::AddSubClass(Class *cl)
{
	this->AddRef();
	cl->super_class = this;
}


/**
	Removes a subclass from this class.
*/
inline
void Class::RemSubClass(Class *cl)
{
	this->RemRef();
	cl->super_class = NULL;
}


/**
	Increments ref count of class object.
*/
inline
int Class::AddRef()
{
	return ++this->ref_count;
}


/**
	Decrements ref count of class object.
*/
inline
int Class::RemRef()
{
	Assert(this->ref_count > 0);
	return --this->ref_count;
}


/**
	Gets current refcount of class object.
*/
inline
int Class::GetRefCount() const
{
	return this->ref_count;
}


/**
	Gets pointer to command list.
*/
inline
HashList *Class::GetCmdList() const
{
	return this->cmd_list;
}


#endif // __CLASS_H__

// vim:ts=4:sw=4:
