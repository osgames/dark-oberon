#ifndef __flagged_h__
#define __flagged_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file flagged.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Flagged
//=========================================================================

/**
	@class Flagged
	@ingroup Kernel_Module

	@brief Helps with setting flags.
	
	Can be used together with basic unsigned types like byte_t, uint_t...
*/

template <typename Type>
class Flagged
{
//--- variables
private:
	Type flags;
	static Type all_flags;

//--- methods
public:
	Flagged();

	void ResetFlags();
	void SetFlags(Type flags);
	void ClearFlags(Type flags);
	void ToggleFlags(Type flags);
	bool IsFlags(Type flags) const;
	bool IsAnyFlag(Type flags) const;
};


//=========================================================================
// Methods
//=========================================================================

template <typename Type>
Type Flagged<Type>::all_flags = 0;

template <class Type>
Flagged<Type>::Flagged() :
	flags(0)
{
	memset(&this->all_flags, 0xFF, sizeof(Type));
}


template <class Type>
void Flagged<Type>::ResetFlags()
{
	this->flags = 0;
}


template <class Type>
void Flagged<Type>::SetFlags(Type flags)
{
	this->flags |= flags;
}


template <class Type>
void Flagged<Type>::ClearFlags(Type flags)
{
	this->flags &= flags ^ this->all_flags;
}


template <class Type>
void Flagged<Type>::ToggleFlags(Type flags)
{
	this->flags ^= flags;
}


template <class Type>
bool Flagged<Type>::IsFlags(Type flags) const
{
	return (this->flags & flags) == flags;
}


template <class Type>
bool Flagged<Type>::IsAnyFlag(Type flags) const
{
	return (this->flags & flags) != 0;
}


//=========================================================================
// Helper
//=========================================================================

class Flagged8 : public Flagged<byte_t>
{
	//
};


class Flagged16 : public Flagged<ushort_t>
{
	//
};


class Flagged32 : public Flagged<uint32_t>
{
	//
};


class Flagged64 : public Flagged<uint64_t>
{
	//
};


#endif  // __flagged_h__

// vim:ts=4:sw=4:
