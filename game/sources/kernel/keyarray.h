#ifndef __KEYARRAY_H__
#define __KEYARRAY_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file keyarray.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// KeyArray
//=========================================================================

/**
	@class KeyArray
	@ingroup Kernel_Module

	@brief Implements growable array of key-data pairs. The array
	is kept sorted for fast Find() by key.
*/


template<class Type>
class KeyArray {
//--- embeded
private:
	struct KeyElement {
		int key;
		Type elm;
	};

//--- variables
private:
	int elements_count;
	int max_elements;
	int grow_elements;
	KeyElement *cur_element;
	KeyElement *elements;

//--- methods
public:
	KeyArray(int num);
	KeyArray(int num, int Grow);
	~KeyArray();

	void Add(int key, const Type &e);
	bool Find(int key, Type &e);
	bool FindPtr(int key, Type* &e);
	void Rem(int key);
	void RemByIndex(int index);
	int Size() const;
	Type& GetElementAt(int index) const;
	int GetKeyAt(int index) const;
	void Clear();
	
private:
	void Alloc(int num);
	void Grow();
	KeyElement* Find(int key);
};


//=========================================================================
// Methods
//=========================================================================

/**
	Constructor for non-growable array.
*/
template<class Type>
KeyArray<Type>::KeyArray(int num) :
	elements_count(0),
	max_elements(0),
	grow_elements(0),
	cur_element(0),
	elements(0)
{
	this->Alloc(num);
}


/**
	Constructor for growable array.
*/
template<class Type>
KeyArray<Type>::KeyArray(int num, int Grow) :
	elements_count(0),
	max_elements(0),
	grow_elements(Grow),
	cur_element(0),
	elements(0)
{
	this->Alloc(num);
}


/**
	Destructor.
*/
template<class Type>
KeyArray<Type>::~KeyArray() 
{
	if (this->elements) 
		delete[] this->elements;
}


/**
	Allocates array.
*/
template<class Type>
void KeyArray<Type>::Alloc(int num)
{
	Assert(!this->elements && !this->cur_element);

	this->elements = NEW KeyElement[num];
	this->max_elements = num;
}


/**
	Grows array.
*/
template<class Type>
void KeyArray<Type>::Grow()
{
	Assert(this->elements && this->grow_elements > 0);

	int newNum = this->max_elements + this->grow_elements;
	KeyElement* newArray = NEW KeyElement[newNum];

	memcpy(newArray, this->elements, this->elements_count * sizeof(KeyElement));

	delete[] this->elements;
	this->elements = newArray;
	this->max_elements  = newNum;
	this->cur_element   = 0;
}


/**
	Binary search.
*/
template<class Type>
typename KeyArray<Type>::KeyElement *KeyArray<Type>::Find(int key)
{
	Assert(this->elements_count > 0);

	int num = this->elements_count;
	int half;

	KeyElement* lo = &(this->elements[0]);
	KeyElement* hi = &(this->elements[num-1]);
	KeyElement* mid;

	while (lo <= hi) {
		if ((half = num/2)) {
			mid = lo + ((num & 1) ? half : (half - 1));

			if (key < mid->key) {
				hi = mid - 1;
				num = num & 1 ? half : half-1;
			} 
			else if (key > mid->key) {
				lo = mid + 1;
				num = half;
			} 
			else
				return mid;
		} 

		else if (num) {
			int diff = key - lo->key;
			return diff ? 0 : lo;
		}

		else 
			break;
	}

	return NULL;
}


/**
	Adds key/element pair to array.
*/
template<class Type>
void KeyArray<Type>::Add(int key, const Type &e) 
{
	// need to Grow array?
	if (this->elements_count == this->max_elements) {
		Assert(this->grow_elements > 0);
		this->Grow();
	}

	// insert key into array, keep array sorted by key
	for (int i = 0; i < this->elements_count; i++) {
		KeyElement* kae = &(this->elements[i]);

		if (key < kae->key) {
			// insert in front of 'e'
			KeyElement* kaeSucc = kae + 1;

			int move_count = this->elements_count - i;
			if (move_count > 0) 
				memmove(kaeSucc, kae, move_count * sizeof(KeyElement));

			kae->key = key;
			kae->elm = e;
			this->elements_count++;
			this->cur_element = 0;

			return;
		}
	}

	// fallthrough: add element to end of array
	this->elements[this->elements_count].key = key;
	this->elements[this->elements_count].elm = e;
	this->elements_count++;
}


/**
	Finds element associated with given key.
*/
template<class Type>
bool KeyArray<Type>::Find(int key, Type &e) 
{
	if (!this->elements_count)
		return false;

	if (this->cur_element && (this->cur_element->key == key)) {
		e = this->cur_element->elm;
		return true;
	}

	else {
		KeyElement *p = this->Find(key);
		if (p) {
			this->cur_element = p;
			e = this->cur_element->elm;
			return true;
		} 
		else 
			return false;
	}
}


/**
	Finds pointer to element associated with key.
*/
template<class Type>
bool KeyArray<Type>::FindPtr(int key, Type *&e)
{
	if (!this->elements_count) 
		return false;

	if (this->cur_element && (this->cur_element->key == key)) {
		e = &(this->cur_element->elm);
		return true;
	}
	
	else {
		KeyElement *p = this->Find(key);
		if (p) {
			this->cur_element = p;
			e = &(this->cur_element->elm);
			return true;
		}
		else
			return false;
	}
}


/**
	Removes element defined by key.
*/
template<class Type>
void KeyArray<Type>::Rem(int key) 
{
	KeyElement* e = this->Find(key);
	if (e) {
		this->cur_element = NULL;
		this->elements_count--;

		KeyElement *next_e = e + 1;
		int i = e - this->elements;
		int move_count = this->elements_count - i;

		if (move_count > 0) 
			memmove(e, next_e, move_count * sizeof(KeyElement));
	}
}


/**
	Removes element defined by key index.
*/
template<class Type>
void KeyArray<Type>::RemByIndex(int index) 
{
	Assert((index >= 0) && (index < this->elements_count));

	KeyElement* e = &(this->elements[index]);
	KeyElement* next_e = e + 1;

	this->cur_element = 0;
	this->elements_count--;
	int move_count = this->elements_count - index;

	if (move_count > 0) 
		memmove(e, next_e, move_count * sizeof(KeyElement));
}


/**
	Returns number of elements.
*/
template<class Type>
int KeyArray<Type>::Size() const
{
	return this->elements_count;
}


/**
	Gets element at index.
*/
template<class Type>
Type &KeyArray<Type>::GetElementAt(int index) const
{
	Assert((index >= 0) && (index < this->elements_count));
	return this->elements[index].elm;
}


/**
	Gets key at index.
*/
template<class Type>
int KeyArray<Type>::GetKeyAt(int index) const
{
	Assert((index >= 0) && (index < this->elements_count));
	return this->elements[index].key;
}


/**
	Clear the array without deallocating memory.
*/
template<class Type>
void KeyArray<Type>::Clear()
{
	this->elements_count = 0;
	this->cur_element = 0;
}


#endif // __KEYARRAY_H__

// vim:ts=4:sw=4:
