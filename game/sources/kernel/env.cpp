//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file env.cpp
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/env.h"
#include "kernel/kernelserver.h"


PrepareScriptClass(Env, "Root", s_NewEnv, s_InitEnv, s_InitEnv_cmds);


//=========================================================================
// Env
//=========================================================================

const char *Env::GetAsString() const
{
	if (!this->arg.IsDirty())
		return this->as_string.c_str();

	switch (this->arg.GetType())
	{
	case Arg::AT_VOID:
		this->as_string = "(void)";
		break;

	case Arg::AT_ARRAY:
		this->as_string = "(array)";
		break;

	case Arg::AT_OBJECT:
		this->as_string = "(pointer)";
		break;

	case Arg::AT_BOOL:
		this->as_string = this->arg.GetB() ? "true" : "false";
		break;

	case Arg::AT_STRING:
		this->as_string = this->arg.GetS();
		break;

	case Arg::AT_INT:
		{
			char txt[100];
			sprintf(txt, "%d", this->arg.GetI());
			this->as_string = txt;
		}
		break;

	case Arg::AT_FLOAT:
		{
			char txt[100];
			sprintf(txt, "%.2f", this->arg.GetF());
			this->as_string = txt;
		}
		break;

	default:
		this->as_string = "(unknown)";
		break;
	}

	return this->as_string.c_str();
}


// vim:ts=4:sw=4:
