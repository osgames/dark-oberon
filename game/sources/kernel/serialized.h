#ifndef __serialized_h__
#define __serialized_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file serialized.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"

class Serializer;


//=========================================================================
// Serialized
//=========================================================================

/**
	@class Serialized
	@ingroup Kernel_Module
*/

class Serialized
{
	friend class SerializeServer;

//--- methods
public:
	Serialized();
	virtual ~Serialized();

protected:
	// serializing
	virtual bool Serialize(Serializer &serializer);
	virtual bool Deserialize(Serializer &serializer, bool first);

	virtual const char *GetGroupName();
};


#endif // __serialized_h__

// vim:ts=4:sw=4:
