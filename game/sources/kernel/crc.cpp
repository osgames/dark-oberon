//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file crc.cpp
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/crc.h"


//=========================================================================
// Variables
//=========================================================================

const uint_t CRC::key = 0x04C11DB7;
uint_t CRC::table[BYTE_VALUES_COUNT] = { 0 };
bool CRC::initialized = false;


//=========================================================================
// CRC
//=========================================================================

/**
	Default constructor.
*/
CRC::CRC()
{
	// initialize byte table
	if (!initialized) {
		for (uint_t i = 0; i < BYTE_VALUES_COUNT; ++i) {
			uint_t reg = i << 24;
			int j;

			for (j = 0; j < 8; ++j) {
				bool top_bit = (reg & 0x80000000) != 0;
				reg <<= 1;
				if (top_bit)
					reg ^= key;
			}

			table[i] = reg;
		}

		initialized = true;
	}
}


/**
	Compute the checksum for a chunk of memory.
*/
uint_t CRC::Checksum(uchar_t* ptr, uint_t bytes_count)
{
	uint_t reg = 0;
	uint_t i;

	for (i = 0; i < bytes_count; i++) {
		uint_t top = reg >> 24;
		top ^= ptr[i];
		reg = (reg << 8) ^ table[top];
	}

	return reg;
}


// vim:ts=4:sw=4:
