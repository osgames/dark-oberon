#ifndef __defclass_h__
#define __defclass_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file defclass.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Helper macros
//=========================================================================

/**
	@ingroup Kernel_Module

	Wraps the module initialization function.

	Use like this:

	@code
	UseModule(ModuleName);

	void main()
	{
		NEW KernelServer();
		KernelServer::GetInstance()->AddPackage(ModuleName);
	}
	@endcode
*/
#define UseModule(module) extern "C" void module()


/**
	@ingroup Kernel_Module

	Special macro for the root class.

	@code
	PrepareClass(RootClass, s_NewRootClass, s_InitRootClass);
	@endcode
*/
#define PrepareRootClass(CLASS, NEWFUNC, INITFUNC, INITCMDSFUNC) \
	extern Object* NEWFUNC(const char *name); \
	extern bool INITFUNC(Class* clazz, KernelServer* kernel_server); \
	extern void INITCMDSFUNC(Class *); \
	\
	Object* NEWFUNC(const char *name) { return NEW CLASS(name); }; \
	\
	bool INITFUNC(Class* clazz, KernelServer* kernel_server) {\
		clazz->SetInstanceSize(sizeof(CLASS)); \
		INITCMDSFUNC(clazz); \
		return true; \
	};


/**
	@ingroup Kernel_Module

	Prepare a simple class without script interface. It takes the C name 
	of the class, a string defining the superclass name, function that 
	creates new instances and initialisation function.

	@code
	PrepareClass(TestClass, "ParentClass", s_NewTestClass, s_InitTestClass);
	@endcode
*/
#define PrepareClass(CLASS, SUPERCLASSNAME, NEWFUNC, INITFUNC) \
	extern Object* NEWFUNC(const char *name); \
	extern bool INITFUNC(Class* clazz, KernelServer* kernel_server); \
	\
	Object* NEWFUNC(const char *name) { return NEW CLASS(name); }; \
	\
	bool INITFUNC(Class* clazz, KernelServer* kernel_server) {\
		clazz->SetInstanceSize(sizeof(CLASS)); \
		kernel_server->AddClass(SUPERCLASSNAME, clazz); \
		return true; \
	};


/**
	@ingroup Kernel_Module

	Prepares a class with script interface. You'll have to provide a function 
	void s_Init*_cmds(Class*). It takes the C name of the class, a string defining 
	the superclass name, function that creates new instances, initialisation
	function and function that initialise script commands.

	@code
	PrepareScriptClass(TestClass, "ParentClass", s_NewTestClass, s_InitTestClass, s_InitTestClass_cmds);
	@endcode
*/
#define PrepareScriptClass(CLASS, SUPERCLASSNAME, NEWFUNC, INITFUNC, INITCMDSFUNC) \
	extern Object* NEWFUNC(const char *name); \
	extern bool INITFUNC(Class* clazz, KernelServer* kernel_server); \
	extern void INITCMDSFUNC(Class *); \
	\
	Object* NEWFUNC(const char *name) { return NEW CLASS(name); }; \
	\
	bool INITFUNC(Class* clazz, KernelServer* kernel_server) {\
		clazz->SetInstanceSize(sizeof(CLASS)); \
		kernel_server->AddClass(SUPERCLASSNAME, clazz); \
		INITCMDSFUNC(clazz); \
		return true; \
	};


#endif // __defclass_h__


// vim:ts=4:sw=4:
