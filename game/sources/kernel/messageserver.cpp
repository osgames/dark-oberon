//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file messageserver.cpp
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/messageserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/timeserver.h"
#include "kernel/threadserver.h"
#include "kernel/env.h"

PrepareClass(MessageServer, "Root", s_NewMessageServer, s_InitMessageServer);


//=========================================================================
// Defines
//=========================================================================

#define MESSAGE_WAIT_TIME 0.02


//=========================================================================
// MessageServer
//=========================================================================

MessageServer::MessageServer(const char *id) :
	RootServer<MessageServer>(id),
	running(false)
{
	// get time server and create profiler
	this->time_server = TimeServer::GetInstance();
	AssertMsg(this->time_server, "TimeServer has to be created before MessageServer");

#ifdef USE_PROFILERS
	this->messages_profiler = this->time_server->NewProfiler(string("% messages"));
	Assert(this->messages_profiler);
#endif

	// create thread object
	this->semaphore = ThreadServer::GetInstance()->CreateSemaphore();
	this->thread = ThreadServer::GetInstance()->CreateThread();
	Assert(this->semaphore && this->thread);
}


MessageServer::~MessageServer()
{
	Assert(!this->running);

	// delete thread object
	delete this->thread;
	delete this->semaphore;
}


bool MessageServer::Run()
{
	Assert(!this->running);

	// start message thread
	this->running = true;
	this->thread->Run(this->OnProcess);

	return true;
}


void MessageServer::Stop()
{
	Assert(this->running);

	// stop message thread and wait until it finishes processing
	this->running = false;
	this->semaphore->SendSignal();
	this->thread->Wait();
}


void MessageServer::SendMessage(BaseMessage *message, Root *sender, Root *destination, double delay)
{
	Assert(message);
	Assert(!message->IsLinked());
	Assert(sender && destination);
	Assert(delay >= 0.0);

	this->Lock();

	// set properties
	message->sender = sender;
	message->destination = destination;

	// if message was resent, do not update timestamp to actual time
	if (message->timestamp)
		message->timestamp += delay;
	else
		message->timestamp = this->time_server->GetRealTime() + delay;

	// insert message into the queue according to its timestamp
	BaseMessage *act_message;
	for (act_message = this->queue.GetBack(); act_message; act_message = act_message->GetPrev())
	{
		if (act_message->timestamp <= message->timestamp)
			break;
	}

	if (act_message)
		message->InsertAfter(act_message);
	else
		this->queue.PushFront(message);

	this->Unlock();

	// unblock the thread if message should be processed immediately
	if (!delay)
		this->semaphore->SendSignal();
}


void MessageServer::CancelMessage(BaseMessage *message)
{
	Assert(message);

	this->Lock();

	// remove message from the queue and reset properties
	if (message->IsLinked())
	{
		message->Remove();
		message->destination = NULL;
		message->timestamp = 0;
	}

	this->Unlock();
}


void MessageServer::OnProcess(Thread *thread)
{
	MessageServer *message_server = MessageServer::GetInstance();
	BaseMessage *message;
	double time;

	LogInfo("Message queue is running");

	while (message_server->running)
	{
		// wait to next message or to timeout
		message_server->semaphore->Wait(MESSAGE_WAIT_TIME);

#ifdef USE_PROFILERS
		message_server->messages_profiler->Start();
#endif

		// get actual time
		time = message_server->time_server->GetRealTime();

		do
		{
			// pop message from queue
			message_server->Lock();

			message = message_server->queue.GetFront();
			if (message && message->timestamp <= time)
				message_server->queue.PopFront();
			else
				message = NULL;

			message_server->Unlock();

			// deliver message
			if (message)
			{
				message->DeliverMessage();

				// if message hasn't been sent again, delete it
				if (!message->IsLinked())
					message->Release();
			}
		} while (message);

#ifdef USE_PROFILERS
		message_server->messages_profiler->Stop();
#endif
	}

	// discard unprocessed messages in the queue
	while ((message = message_server->queue.PopFront()))
		message->Release();

	LogInfo("Message queue has stopped");
}


// vim:ts=4:sw=4:
