//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file env_cmds.cpp
	@ingroup Kernel_Module

 	@author A.Weissflog/A.Flemming, Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/env.h"
#include "kernel/kernelserver.h"
#include "kernel/cmdproto.h"


//=========================================================================
// Commands
//=========================================================================

/**
	Returns the datatype the variable is set to. If void 
	is returned the variable is empty. 
*/
static void s_GetType(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	char *st;
	Arg::Type t = self->GetType();

	switch (t) {
		case Arg::AT_INT:            st = "int"; break;
		case Arg::AT_FLOAT:          st = "float"; break;
		case Arg::AT_STRING:         st = "string"; break;
		case Arg::AT_BOOL:           st = "bool"; break;
		case Arg::AT_OBJECT:         st = "object"; break;
		default:                     st = "void"; break;
	}

	cmd->Out()->SetS(st);
}


/**
	Returns the content of the variable if it is an int 
	variable. If it's not an int variable, 0 is returned and 
	an error message is printed.
*/
static void s_GetI(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	cmd->Out()->SetI(self->GetI());
}


/**
	Returns the content of the variable if it is a float 
	variable. If it's not a float variable, 0.0 is returned
	and an error message is printed.
*/
static void s_GetF(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	cmd->Out()->SetF(self->GetF());
}


/**
	Returns the content of the variable if it is a bool 
	variable. If it's not a bool, false is returned and an
	error message is printed.
*/
static void s_GetB(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	cmd->Out()->SetB(self->GetB());
}


/**
	Returns the content of the variable if it is a string 
	variable. If it's not a string variable, "" is returned
	and an error message is printed.
*/
static void s_GetS(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	cmd->Out()->SetS(self->GetS());
}


/**
	Returns the content of the variable as an object handle. If it's
	not an object handle, NULL is returned and an error message is printed.
*/
static void s_GetO(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	cmd->Out()->SetO(self->GetO());
}


/**
	Sets the content of the variable to the passed integer value. 
*/
static void s_SetI(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	self->SetI(cmd->In()->GetI());
}


/**
	Sets the content of the variable to the passed float value. 
*/
static void s_SetF(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	self->SetF(cmd->In()->GetF());
}


/**
	Sets the content of the variable to the passed boolean value. 
*/
static void s_SetB(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	self->SetB(cmd->In()->GetB());
}


/**
	Sets the content of the variable to the passed string. 
*/
static void s_SetS(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	self->SetS(cmd->In()->GetS());
}


/**
	Sets the content of the variable to the passed object handle. 
*/
static void s_SetO(void *o, Cmd *cmd)
{
	Env *self = (Env *)o;
	self->SetO((Root *)cmd->In()->GetO());
}


void s_InitEnv_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("s_GetType_v",   'GTYP', s_GetType);
	cl->AddCmd("i_GetI_v",      'GETI', s_GetI);
	cl->AddCmd("f_GetF_v",      'GETF', s_GetF);
	cl->AddCmd("b_GetB_v",      'GETB', s_GetB);
	cl->AddCmd("s_GetS_v",      'GETS', s_GetS);
	cl->AddCmd("o_GetO_v",      'GETO', s_GetO);
	cl->AddCmd("v_SetI_i",      'SETI', s_SetI);
	cl->AddCmd("v_SetF_f",      'SETF', s_SetF);
	cl->AddCmd("v_SetB_b",      'SETB', s_SetB);
	cl->AddCmd("v_SetS_s",      'SETS', s_SetS);
	cl->AddCmd("v_SetO_o",      'SETO', s_SetO);

	cl->EndCmds();
}


// vim:ts=4:sw=4:
