#ifndef __ipcaddress_h__
#define __ipcaddress_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file ipcaddress.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2007 - 2008

	@version 1.0 - Copy from Nebula Device.
	@version 1.1 - Comparison operators. Fixed const methods.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/includesockets.h"


//=========================================================================
// IpcAddress
//=========================================================================

/**
    @class IpcAddress
    @ingroup Kernel_Module

    @brief Encapsulates a target address for the Ipc* class family.

    A target address is a string made of 2 parts, host name and port id.
    A host name is either a valid tcp/ip address in string form,
    a name which can be resolved by <tt>gethostbyname()</tt> (including
    <tt>localhost</tt>), or the special strings <tt>self</tt>,
    <tt>inetself</tt>, <tt>any</tt> and <tt>broadcast</tt>. The port
    id string is either a normal name, which will be hashed into a port number,
    or if the string is valid integer number, this number will be used directly
    as port number.

    Examples:

     - <tt>localhost</tt>
     - <tt>192.168.0.90</tt>
     - <tt>flohbox</tt>
     - <tt>broadcast</tt>

    <b>Special host names and IP addresses:</b>

    <dl>
      <dt><tt>localhost</tt></dt>
        <dd>this will translate to <tt>127.0.0.1</tt>.</dd>
      <dt><tt>any</tt></dt>
        <dd>this will translate to <tt>INADDR_ANY</tt>, which is
          <tt>0.0.0.0</tt>.</dd>
      <dt><tt>broadcast</tt></dt>
        <dd>this will translate to <tt>INADDR_BROADCAST</tt>, which is
          <tt>255.255.255.255</tt>.</dd>
      <dt><tt>self</tt></dt>
        <dd>this will translate to the first valid tcp/ip address
          for this host (there may be more then one tcp/ip addresses
          bound to a hostname)</dd>
      <dt><tt>inetself</tt></dt>
        <dd>This will translate to the first valid tcp/ip address
          for this host which is not a LAN address (which is not
          a Class A, B or C network address). If no such exists,
          the address will fallback to <tt>self</tt>.</dd>
    </dl>
*/

class IpcAddress
{
//--- embeded
private:
	enum Enums
    {
        MinPortNum = N_SOCKET_MIN_PORTNUM,
        MaxPortNum = N_SOCKET_MAX_PORTNUM,
        PortRange = N_SOCKET_PORTRANGE,
    };


//--- methods
public:
    IpcAddress();
    IpcAddress(const char *host_name, const char *port_name);
    IpcAddress(const char *host_name, ushort_t port);
	IpcAddress(const IpcAddress &addr);
    ~IpcAddress();

    void SetHostName(const char *name);
    const char *GetHostName() const;

    void SetPortName(const char *name);
    const char *GetPortName() const;

    void SetAddrStruct(const sockaddr_in &addr);
    const sockaddr_in &GetAddrStruct() const;

    const char *GetIpAddrString() const;

    void SetPortNum(ushort_t portnum);
    ushort_t GetPortNum() const;

	bool operator==(const IpcAddress &ref) const;
	bool operator!=(const IpcAddress &ref) const;

private:
	bool ValidateAddrStruct() const;
    bool ValidateIpAddr() const;
    bool ValidatePortNum() const;

    /// check if an ip address is a valid internet address (vs. LAN address)
    bool IsInternetAddress(const in_addr &addr) const;


//--- variables
private:
    string host_name;
    string port_name;

    mutable sockaddr_in addr_struct;
    mutable in_addr ip_address;
    mutable string ip_string;
    mutable ushort_t port;

    mutable bool addr_struct_valid;
    mutable bool ip_address_valid;
    mutable bool port_valid;
};


//=========================================================================
// Methods
//=========================================================================

inline
bool IpcAddress::operator==(const IpcAddress &ref) const
{
	const sockaddr_in &s1 = this->GetAddrStruct();
	const sockaddr_in &s2 = ref.GetAddrStruct();
	return (s1.sin_addr.S_un.S_addr == s2.sin_addr.S_un.S_addr && s1.sin_port == s2.sin_port);
}


inline
bool IpcAddress::operator!=(const IpcAddress &ref) const
{
    return !(*this == ref);
}


#endif // __ipcaddress_h__

// vim:ts=4:sw=4:
