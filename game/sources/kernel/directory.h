#ifndef __directory_h__
#define __directory_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file directory.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/counted.h"

#ifdef WINDOWS
#	include <io.h>
#	if _MSC_VER == 1200
typedef long intptr_t;         ///< Defines intptr_t for MSVC 6.0
#	endif
#else
#	include <sys/types.h>
#	include <dirent.h>
#endif


//=========================================================================
// Directory
//=========================================================================

/**
	@class Directory
	@ingroup Kernel_Module

	@brief Wrapper for directory functions.

	Provides functions for searching directories.
*/

class Directory : public Counted
{
	friend class FileServer;

//--- embeded
public:
	enum EntryType {
		ET_INVALID,
		ET_FILE,
		ET_DIRECTORY
	};

//--- variables
protected:
	bool empty;
	bool opened;
	string path;
	string entry_name;

#ifdef WINDOWS
	struct _finddata_t entry;
	intptr_t entry_id;
#else
	DIR *dir;
	struct dirent *entry;
#endif


//--- methods
public:
	// manipulation
	bool Open(const string &path);
	void Close();
	bool IsOpened() const;

	// properties
	const string &GetPathName() const;
	bool IsEmpty();

	// entries
	bool SetNextEntry();
	const string &GetEntryName();
	EntryType GetEntryType();
	bool TestEntryType(EntryType type);

protected:
	Directory();
	virtual ~Directory();

	bool SetFirstEntry();
};


/**
	Determines whether the directory is open.
*/
inline
bool Directory::IsOpened() const
{
	return this->opened;
}


/**
	Gets the full path name of the directory itself.
*/
inline
const string &Directory::GetPathName() const
{
	Assert(this->IsOpened());
	return this->path;
}


/**
	Returns whether given type is the same as current entry type.

	@param type Entry type to test.
	@return @c True if given type is same as entry type.
*/
inline
bool Directory::TestEntryType(EntryType type)
{
	return (type == this->GetEntryType());
}


#endif // __directory_h__

// vim:ts=4:sw=4:
