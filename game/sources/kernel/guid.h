#ifndef __guid_h__
#define __guid_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file guid.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH
	@date 2003

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Guid
//=========================================================================

/**
	@class Guid
	@ingroup Kernel_Module

	@brief Wraps a Globally Unique Identifier.  The actual
	GUID data is hidden inside the object and only accessible as string.

	Under Win32, this is a normal GUID.
	Under GNU/Linux, libuuid from the e2fsprogs package is used.
	Other platforms may implement something different.
*/

class Guid
{
//--- methods
public:
	Guid();
	Guid(const char *guid);

	bool operator==(const Guid &rhs) const;

	void Set(const char *guid);
	const char *Get() const;
	void Generate();


//--- variables
private:
	string guid;
};


//=========================================================================
// Methods
//=========================================================================

inline
Guid::Guid()
{
	this->guid = "00000000-0000-0000-0000-000000000000";
}


inline
Guid::Guid(const char *guid)
{
	Assert(guid);
	this->guid = guid;
}


inline
bool Guid::operator==(const Guid &rhs) const
{
	return (this->guid == rhs.guid);
}


inline
void Guid::Set(const char *guid)
{
	Assert(guid);
	this->guid = guid;
}


/**
*/
inline
const char *Guid::Get() const
{
	return this->guid.c_str();
}


#endif  // __guid_h__

// vim:ts=4:sw=4:
