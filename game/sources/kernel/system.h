#ifndef __kernel_system_h__
#define __kernel_system_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file kernel/system.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@brief Includes all common kernel headers.

	@version 1.0 - Initial.
	@version 1.1 - Removed math. functions for random numbers.
*/


//=========================================================================
// Common header files
//=========================================================================

#include "kernel/config.h"
#include "kernel/debug.h"
#include "kernel/types.h"
#include "kernel/memory.h"
#include "kernel/defclass.h"


//=========================================================================
// Common definitions
//=========================================================================

// NOH paths
#define NOH_ASSIGNS   string("/sys/assigns")
#define NOH_SERVERS   string("/sys/servers")
#define NOH_RESOURCES string("/sys/resources")
#define NOH_VARIABLES string("/sys/variables")


//=========================================================================
// Useful macros and functions
//=========================================================================

#define SafeDelete(a) do { delete a; a = NULL; } while(0)
#define SafeDeleteArray(a) do { delete[] a; a = NULL; } while (0)
#define SafeRelease(a) do { if (a) { a->Release(); a = NULL; } } while(0)


template<class T>
void Delete2DArray(T **array2D, int width)
{
	Assert(width);

	if (!array2D)
		return;

	for (int i = 0; i < width; i++)
	{
		if (array2D[i])
			delete[] array2D[i];
	}

	delete[] array2D;
}


template<class T>
T **Create2DArray(int width, int height)
{
	Assert(width && height);

	// create first dimension
	T **array2D = NEW T *[width];
	if (!array2D)
		return NULL;
	
	// initialize array
	memset(array2D, 0, sizeof(T *) * width);

	// create second dimension
	for (int i = 0; i < width; i++)
	{
		array2D[i] = NEW T[height];
		if (!array2D[i])
		{
			Delete2DArray<T>(array2D, width);
			return NULL;
		}
		memset(array2D[i], 0, sizeof(T) * height);
	}

	return array2D;
}


#endif // __system_h__

// vim:ts=2:sw=2:et:
