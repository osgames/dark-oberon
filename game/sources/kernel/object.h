#ifndef __object_h__
#define __object_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file object.h
	@ingroup Kernel_Module

 	@author Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/class.h"
#include "kernel/cmd.h"
#include "kernel/counted.h"
#include "kernel/serialized.h"

class KernelServer;


//=========================================================================
// Object
//=========================================================================

/**
	@class Object
	@ingroup Kernel_Module

	Provides:
	- RTTI, a class is identified by a string name

	Rules for subclasses:
	- only the default constructor is allowed
	- never use new/delete (or variants like NEW) with Object objects
	- use KernelServer::New() to create an object and
		the object's Release() method to destroy it
*/
class Object : public Counted, public Serialized
{
	friend class Class;

//--- variables
protected:
	KernelServer* kernel_server;    ///< Pointer to kernel server.
	Class* my_class;                ///< Pointer to my class.

//--- methods
public:
	Object(const char *name);

	virtual int GetInstanceSize() const;

	Class *GetClass() const;
	bool IsA(const Class *) const;
	bool IsA(const char *) const;
	bool IsInstanceOf(const Class *) const; 

	bool Dispatch(Cmd *);
	void GetCmdProtos(HashList *);

protected:
	virtual ~Object();
	void SetClass(Class *);

	// serializing
	virtual const char *GetGroupName();
};


//=========================================================================
// Methods
//=========================================================================

/**
	Sets pointer to my class object.
*/
inline
void Object::SetClass(Class* cl)
{
	if (this->my_class)
		this->my_class->RemRef();

	this->my_class = cl;
	this->my_class->AddRef();
}


/**
	Returns pointer to my class object.
*/
inline
Class *Object::GetClass() const
{
	return this->my_class;
}


/**
	Returns @c true if object is part of class hierarchy.
*/
inline
bool Object::IsA(const Class *cl) const
{
	Class *act_class = this->my_class;

	do {
		if (act_class == cl) 
			return true;
	} while ((act_class = act_class->GetSuperClass()));

	return false;
}


/**
	Returns @c true if object is instance of class.
*/
inline
bool Object::IsInstanceOf(const Class *cl) const
{
	return (cl == this->my_class);
}


#endif // __object_h__

// vim:ts=4:sw=4:
