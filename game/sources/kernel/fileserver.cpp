//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file fileserver.cpp

	@author Peter Knut
	@ingroup Kernel_Module
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added wrappers for standart streams.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/env.h"
#include "kernel/fileserver.h"
#include "kernel/crc.h"

#ifdef WINDOWS
#	include <direct.h>
#endif

PrepareScriptClass(FileServer, "Root", s_NewFileServer, s_InitFileServer, s_InitFileserver_cmds);


File FileServer::std_in(stdin, "Standart input", false);
File FileServer::std_out(stdout, "Standart output", false);
File FileServer::std_err(stderr, "Standart error output", false);


//=========================================================================
// FileServer
//=========================================================================

/**
	Constructor.
*/
FileServer::FileServer(const char *id) :
	RootServer<FileServer>(id),
	assigns(NULL)
{
	// initialize assign repository if not already exists
	Root *assignRoot = kernel_server->Lookup(NOH_ASSIGNS);
	if (0 == assignRoot)
		this->InitAssigns();
	else
		this->assigns = assignRoot;
}


/**
	Destructor.
*/
FileServer::~FileServer()
{
	//
}


/**
	Creates new or modifies existing assign under /sys/assigns.

	@param assign      The name of the assign.
	@param path        The path to which the assign links.
*/
bool FileServer::SetAssign(const string &assign, const string &path)
{
	Assert(assign.length() > 1);

	string path_str;
	if (!this->ManglePath(path, path_str))
		return false;

	// create and set assign
	kernel_server->PushCwd(this->assigns);

	Env *env = (Env *)this->assigns->Find(assign);
	if (!env)
	{
		env = (Env *)kernel_server->New("Env", assign);
		Assert(env);
	}
	env->SetS(path_str.c_str());

	kernel_server->PopCwd();

	return true;
}


/**
	Queries existing assign under /sys/assigns.

	@param assign      The name of the assign.
	@return            The path to which the assign links, or NULL if assign is undefined.
*/
const char *FileServer::GetAssign(const string &assign)
{
	Env *env = (Env *)this->assigns->Find(assign);
	if (env)
		return env->GetS();

	else
	{
		LogError1("Assign '%s' is not defined", assign.c_str());
		return NULL;
	}
}


/**
	Initialize home directory assign ("home:").
*/
void FileServer::InitAppAssign()
{
	Verify(this->SetAssign(string("app"), KernelServer::GetInstance()->GetApplicationPath()));
}


/**
	Initialize the user assign. This is where the application should
	save any type of data, like save games or config options, since
	applications may not have write access to the home: directory (which is by
	tradition the application directory).
*/
void FileServer::InitUserAssign()
{
#ifdef WINDOWS
	Verify(this->SetAssign(string("user"), string(this->GetAssign("app"))));

#else
	string path(getenv("HOME"));
	path.Append("/");

	Verify(this->SetAssign("user", path.c_str()));
#endif
}


/**
	Initializes all default assigns.
*/
void FileServer::InitAssigns()
{
	Assert(!this->assigns);
	this->assigns = kernel_server->New("Root", NOH_ASSIGNS);

	this->InitAppAssign();
	this->InitUserAssign();
}


/**
	Remove all existing assigns and setup base assigns.
*/
void FileServer::ResetAssigns()
{
	if (this->assigns)
		this->assigns->Release();

	this->InitAssigns();
}


/**
	Returns a cleaned up path name (replaces backslashes with slashes,
	and removes trailing slash if exists.
*/
void FileServer::CleanupPath(string &path)
{
	Assert(!path.empty());

	char buff[4096];
	char *ptr;
	char c;

	strcpy(buff, path.c_str());
	ptr = buff;

	// replace backslashes with slashes
	while ((c = *ptr))
	{
		if (c == '\\')
			*ptr = '/';

		ptr++;
	}

	// remove trailing slashes
	while ((ptr > buff) && (*(--ptr) == '/'))
		*ptr = 0;

	path = buff;
}


/**
	Expands assign in path to full absolute path, replaces any backslashes
	by slashes, removes any trailing slash, and makes the path absolute.

	@param path       The path to expand.
	@param full_path  Expanded path.
	@return           @c True if successful.
*/
bool FileServer::ManglePath(const string &path, string &full_path)
{
	str_pos_t pos;
	string path_str = path;
	
	this->CleanupPath(path_str);

	// check for assigns
	// ignore one character "assigns" because they are really DOS drive letters
	if ((pos = path_str.find(':', 0)) < 2 || pos == string::npos)
	{
		full_path = path;
		return true;
	}

	// find assign
	const char *replace = this->GetAssign(path_str.substr(0, pos));
	if (!replace)
		return false;

	full_path = replace;
	full_path.append("/");
	full_path.append(path_str, pos + 1, path_str.npos);

	return true;
}


void FileServer::ExtractDirectory(const string &path, string &dir)
{
	str_pos_t pos;
	dir = path;
	
	CleanupPath(dir);

	if ((pos = dir.rfind('/')) != string::npos)
		dir = dir.substr(0, pos);

	else if ((pos = dir.rfind(':')) != string::npos)
		dir = dir.substr(0, pos + 1);

	else
		dir.clear();
}


void FileServer::ExtractFile(const string &path, string &file)
{
	str_pos_t pos;
	file = path;
	
	CleanupPath(file);

	if ((pos = file.rfind('/')) != string::npos)
		file = file.substr(pos + 1);

	else if ((pos = file.rfind(':')) != string::npos)
		file = file.substr(pos + 1);
}


void FileServer::ExtractFileName(const string &path, string &file_name)
{
	str_pos_t pos;
	
	ExtractFile(path, file_name);

	if ((pos = file_name.rfind('.')) != string::npos)
		file_name = file_name.substr(0, pos);
}


void FileServer::ExtractExtension(const string &path, string &extension)
{
	str_pos_t pos;

	extension = path;
	CleanupPath(extension);

	if ((pos = extension.rfind('.')) != string::npos)
		extension = extension.substr(pos + 1);

	else
		extension.clear();
}


/**
	Check if file exists.
*/
bool FileServer::FileExists(const string &path) const
{
	Assert(!path.empty());

	string full_path;
	if (!FileServer::GetInstance()->ManglePath(path, full_path))
		return false;

	// try to open file
	FILE *fp = fopen(full_path.c_str(), "r");

    if (!fp)
        return false;

	fclose(fp);
	return true;
}


/**
	Check if directory exists.
*/
bool FileServer::DirectoryExists(const string &path) const
{
	Assert(!path.empty());

	string full_path;
	if (!FileServer::GetInstance()->ManglePath(path, full_path))
		return false;
	
#ifdef WINDOWS
	// try to search in directory
	struct _finddata_t entry;
	intptr_t entry_id = _findfirst((full_path + "/*").c_str(), &entry);

	if (entry_id == -1)
		return false;

	_findclose(entry_id);
	
#else
	// try to open directory
	DIR *dir;
	if (!(dir = opendir(full_path.c_str())))
		return false;

	closedir(dir);
#endif

	return true;
}


/**
	Copy a file.
*/
bool FileServer::CopyFileEx(const string &from, const string &to)
{
	// open source file
	File *from_file = this->NewFile();
	if (!from_file->Open(from, "rb"))
	{
		LogError1("Can not open source file '%s'", from.c_str());
		delete from_file;
		return false;
	}

	// open dest file
	File *to_file = this->NewFile();
	if (!to_file->Open(to, "wb"))
	{
		LogError1("Can not open destination file '%s'", to.c_str());
		from_file->Close();
		delete from_file;
		delete to_file;
		return false;
	}

	size_t read_count, numWritten;

	bool ok = true;
	const int buff_size = 4096;
	void *buffer = NEW byte_t[buff_size];
	Assert(buffer);

	// copy file
	read_count = from_file->Read(buffer, buff_size);
	while (read_count > 0)
	{
		numWritten = to_file->Write(buffer, read_count);
		if (numWritten != read_count)
		{
			ok = false;
			break;
		}
		read_count = from_file->Read(buffer, buff_size);
	}

	delete[] buffer;

	// close files
	from_file->Close();
	delete from_file;
	to_file->Close();
	delete to_file;

	return ok;
}


/**
	Compute the CRC checksum for a file.
	FIXME: the current implementation loads the entire file into memory!!!

	@param  file_name    [in]    File name.
	@param  crc         [out]   The computed CRC checksum.
	@return             @c True if all ok, @c false if file could not be opened.
*/
bool FileServer::Checksum(const string &file_name, uint_t &crc)
{
    crc = 0;
    bool ok = false;
    File* file = this->NewFile();
    Assert(file);

    if (file->Open(file_name, "rb"))
	{
        int size = file->GetSize();
        byte_t* buf = (byte_t*) NEW byte_t[size];
        Assert(buf);

		// read file into RAM buffer
        size_t read_count = file->Read(buf, size);
        if (read_count != size)
			ok = false;
		
		// compute CRC
		else
		{
			CRC crc_sum;
			crc = crc_sum.Checksum(buf, size);
			ok = true;
		}

		// free and close everything
		delete[] buf;
		file->Close();
    }

    delete file;
    return ok;
}


/**
	Create all missing directories in path. Path can not contain a file name.

	@param path Path without file name.
	@return @c True if successful.
*/
bool FileServer::CreatePath(const string &path)
{
	// build stack of non-existing dir components
	string full_path;
	if (!this->ManglePath(path, full_path))
		return false;

	vector<string> path_stack;

	while ((!full_path.empty()) && (!this->DirectoryExists(full_path)))
	{
		path_stack.push_back(full_path);
		ExtractDirectory(full_path, full_path);
	}

	// error
	if (full_path.empty())
		return false;

	// create missing directory components
	long count = (long)path_stack.size();

	for (long i = count - 1; i >= 0 ; i--)
	{
		const string &act_path = path_stack[i];

		#ifdef WINDOWS
			int err = _mkdir(act_path.c_str());
		#else
			int err = mkdir(act_path.c_str(), S_IRWXU|S_IRWXG);
		#endif

		if (err == -1)
			return false;
	}

	return true;
}


/**
	Delete a file.
*/
bool FileServer::DeleteFileEx(const string &file_name)
{
	string full_path;
	if (!this->ManglePath(file_name, full_path))
		return false;

#ifdef WINDOWS
	return (_unlink(full_path.c_str()) == 0);
#else
	return (unlink(full_path.c_str()) == 0);
#endif
}


/**
    Delete an empty directory.

	@param path     Directory path.
	@return @c True if successful.
*/
bool FileServer::DeleteDirectory(const string &path)
{
	string full_path;
	if (!this->ManglePath(path, full_path))
		return false;

#ifdef WINDOWS
	return (_rmdir(full_path.c_str()) == 0);
#else
	return (rmdir(full_path.c_str()) == 0);
#endif
}


/**
	List all files in a directory, ignores subdirecories.

	@param dir_list This list will be filled with all file names.
	@param path     Directory path.

	@return @c True if successful.
*/
bool FileServer::ListFiles(vector<string> &file_list, const string &path)
{
	Assert(!path.empty());

	// test existency
	if (!this->DirectoryExists(path))
	{
		LogError1("Directory does not exists: %s", path);
		return false;
	}

	// create directory object
	Directory *dir = this->NewDirectory();
	if (!dir)
	{
		LogError1("Error opening directory: %s", path);
		return false;
	}

	// open directory
	if (!dir->Open(path))
	{
		LogError1("Error opening directory: %s", path);
		dir->Release();
		return false;
	}

	// get all files
	if (!dir->IsEmpty())
	{
		file_list.clear();

		do {
			if (dir->GetEntryType() == Directory::ET_FILE)
				file_list.push_back(dir->GetEntryName());
		} while (dir->SetNextEntry());
	}

	// close directory
	dir->Close();
	dir->Release();

	return true;
}


/**
	List all subdirectories in a directory, ignores files.

	@param dir_list This list will be filled with all subfolder names.
	@param path     Directory path.
	@param special  Whether special folders ('.', '..') will be included in result.

	@return @c True if successful.
*/
bool FileServer::ListDirectories(vector<string> &dir_list, const string &path, bool special)
{
	Assert(!path.empty());

	// test existency
	if (!this->DirectoryExists(path))
	{
		LogError1("Directory does not exists: %s", path);
		return false;
	}

	// create directory object
	Directory *dir = this->NewDirectory();
	if (!dir)
	{
		LogError1("Error opening directory: %s", path);
		return false;
	}

	// open directory
	if (!dir->Open(path))
	{
		LogError1("Error opening directory: %s", path);
		dir->Release();
		return false;
	}

	// get all subfolders
	if (!dir->IsEmpty())
	{
		dir_list.clear();

		do {
			if (dir->GetEntryType() == Directory::ET_DIRECTORY)
			{
				if (special || (dir->GetEntryName() != "." && dir->GetEntryName() != ".."))
					dir_list.push_back(dir->GetEntryName());
			}
		} while (dir->SetNextEntry());
	}

	// close directory
	dir->Close();
	dir->Release();

	return true;
}

// vim:ts=4:sw=4:
