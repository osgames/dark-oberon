#ifndef __ipcchannel_h__
#define __ipcchannel_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file ipcchannel.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2007 - 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/ipcaddress.h"
#include "kernel/counted.h"


class IpcBuffer;


//=========================================================================
// IpcChannel
//=========================================================================

/**
    @class IpcChannel
    @ingroup Ipc

    A IpcServer creates one IpcChannel for each client.
*/

class IpcChannel : public Counted
{
//--- embeded
public:
	enum Protocol
	{
		PROTOCOL_NONE,
		PROTOCOL_UDP,
		PROTOCOL_TCP
	};


//--- methods
public:
	IpcChannel(SOCKET sock);
	virtual ~IpcChannel();

	virtual bool Send(const IpcBuffer &buffer) = 0;
	virtual bool Receive(IpcBuffer &buffer) = 0;

	void SetAddress(const IpcAddress &address);
	const IpcAddress &GetAddress() const;
	uint_t GetChannelId() const;
	Protocol GetProtocol() const;
	bool IsConnected() const;
	void SetCanReceive(bool can_receive);
	bool CanReceive() const;
	const SOCKET GetSocketId() const;

	static bool ProtocolToString(Protocol protocol, string &name);
	static bool StringToProtocol(const string &name, Protocol &protocol);

protected:
	void DestroySocket();


//--- variables
protected:
	Protocol protocol;
	IpcAddress address;
	uint_t channel_id;
	SOCKET socket_id;
	bool can_receive;
};


//=========================================================================
// Methods
//=========================================================================

inline
IpcChannel::Protocol IpcChannel::GetProtocol() const
{
	return this->protocol;
}


/**
*/
inline
uint_t IpcChannel::GetChannelId() const
{
	return this->channel_id;
}


/**
*/
inline
bool IpcChannel::IsConnected() const
{
	return this->socket_id != INVALID_SOCKET;
}


inline
void IpcChannel::SetCanReceive(bool can_receive)
{
	this->can_receive = can_receive;
}


inline
bool IpcChannel::CanReceive() const
{
	return this->can_receive;
}


inline
void IpcChannel::SetAddress(const IpcAddress &address)
{
	this->address = address;
}


inline
const IpcAddress &IpcChannel::GetAddress() const
{
	return this->address;
}


inline
const SOCKET IpcChannel::GetSocketId() const
{
	return this->socket_id;
}


#endif

// vim:ts=4:sw=4:
