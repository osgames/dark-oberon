#ifndef __scriptserver_h__
#define __scriptserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scriptserver.h
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/ref.h"
#include "kernel/rootserver.h"

class ScriptLoader;


//=========================================================================
// ScriptServer
//=========================================================================

/**
	@class ScriptServer
	@ingroup Kernel_Module

	Stores all script loaders in one place. Script loaders has to be
	created here.
*/

class ScriptServer : public RootServer<ScriptServer>
{
//--- varialbles
private:
	bool exit_requested;                   ///< Whether Exit function was called.
	string default_language;               ///< Name of default language.
	Ref<ScriptLoader> default_loader;      ///< Default loader used when language is not specified.

//--- methods
public:
	ScriptServer(const char *id);
	virtual ~ScriptServer();

	// settings
	bool SetDefaultLanguage(const string &language);
	const string &GetDefaultLanguage();

	// creating loaders
	ScriptLoader *NewScriptLoader(const char *class_name, const string &language);

	// running scripts
	bool RunCode(const char *code, string &result, const char *language = NULL);
	bool RunFunction(const char *func_name, string &result, const char *language = NULL);
	bool RunScript(const string &file_path, string &result);

	virtual string Prompt(); 
	virtual bool Trigger(void);
	
	void Exit();

protected:
	ScriptLoader *GetLoader(const char *language);
};


//=========================================================================
// Methods
//=========================================================================

/**
	Returns defult language.
	@return Default language.
*/
inline
const string &ScriptServer::GetDefaultLanguage()
{
	this->default_language;
}


/**
	Exits interractive mode.
*/
inline
void ScriptServer::Exit()
{
	this->exit_requested = true;
}


#endif // __scriptserver_h__

// vim:ts=4:sw=4:
