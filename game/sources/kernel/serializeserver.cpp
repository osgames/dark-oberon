//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file serializeserver.cpp
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/serializeserver.h"
#include "kernel/kernelserver.h"
#include "kernel/fileserver.h"
#include "kernel/logserver.h"

PrepareScriptClass(SerializeServer, "Root", s_NewSerializeServer, s_InitSerializeServer, s_InitSerializeServer_cmds);


//=========================================================================
// SerializeServer
//=========================================================================

/**
	Constructor.
*/
SerializeServer::SerializeServer(const char *id) :
	RootServer<SerializeServer>(id)
{
	// empty
}


/**
	Destructor.
*/
SerializeServer::~SerializeServer()
{
	// empty
}


/**
	Sets default format. The serializer of this format will be used	when format is not set explicitly.

	@param format Identifier of serialize format.
	@return @c True if serializer of specified format is found.
*/
bool SerializeServer::SetDefaultFormat(const string &format)
{
	Assert(!format.empty());

	// find serializer
	hash_map<string, string>::iterator it = this->serializers_map.find(format);
	if (it == this->serializers_map.end())
	{
		LogWarning1("No serializer was registered for format: %s", format.c_str());
		return false;
	}

	// set new serializer
	this->default_format = format;
	this->default_serializer = it->second;

	return true;
}


/**
	Registers serializer of given class. This serializer will be associated with specified format.

	@param class_name Class name of serializer.
	@param format Identifier of data format.
	@return @c True if successful.
*/
bool SerializeServer::RegisterSerializer(const string &class_name, const string &format)
{
	Assert(!class_name.empty() && !format.empty());

	// check if there is no serializer for given format
	hash_map<string, string>::iterator it = this->serializers_map.find(format);
	if (it != this->serializers_map.end())
	{
		LogWarning1("There was another serializer regitered for format: %s", format.c_str());
		return false;
	}

	// add serializer to hash map
	this->serializers_map[format] = class_name;

	// set default serializer if first serializer is registered
	if (this->default_format.empty())
	{
		this->default_format = format;
		this->default_serializer = class_name;
	}

	return true;
}


/**
	Cerates new serializer of given class name.
	Each serializer is a NOH child of serialize server.

	@param format Identifier of data format.
	@return Pointer to new object.
*/
Serializer *SerializeServer::NewSerializer(const char *format)
{
	// check if we have any serializer
	if (this->serializers_map.empty())
	{
		LogWarning("No serializers were registered");
		return NULL;
	}

	const char *class_name;

	// find serializer
	if (format && *format)
	{
		hash_map<string, string>::iterator it = this->serializers_map.find(string(format));
		if (it == this->serializers_map.end())
		{
			LogWarning1("No serializer was registered for format: %s", format);
			return NULL;
		}
		class_name = it->second.c_str();
	}
	else
	{
		Assert(!this->default_serializer.empty());
		class_name = this->default_serializer.c_str();
	}

	// create serializer
	this->kernel_server->PushCwd(this);
	Serializer *serializer = (Serializer *)this->kernel_server->New(class_name, format);
	Assert(serializer);
	this->kernel_server->PopCwd();

	return serializer;
}


bool SerializeServer::IsSerializer(const char *format)
{
	// check if we have any serializer
	if (this->serializers_map.empty())
		return false;
	
	// find serializer
	if (format && *format)
	{
		hash_map<string, string>::iterator it = this->serializers_map.find(string(format));
		return it != this->serializers_map.end();
	}
	else
		return !this->default_serializer.empty();
}


long SerializeServer::Serialize(Serialized *obj, byte_t *&buffer, const char *format)
{
	Assert(obj);

	// find serializer
	Serializer *serializer = this->NewSerializer(format);
	if (!serializer)
	{
		LogError1("Error creating serializer for format: %s", format);
		return 0;
	}

	// serialize object
	serializer->BeginSerialize(obj->GetGroupName());

	bool ok = obj->Serialize(*serializer);
	if (!ok)
	{
		LogError1("Error serializing object with serializer: %s", serializer->GetID().c_str());
		ok = false;
	}

	long buff_size = serializer->EndSerialize(buffer);

	serializer->Release();

	return ok ? buff_size : 0;
}


bool SerializeServer::Serialize(Serialized *obj, const string &file_path)
{
	Assert(obj);
	Assert(!file_path.empty());

	// get data format
	string format;
	FileServer::ExtractExtension(file_path, format);

	// check if serializer exists
	if (!this->IsSerializer(format.c_str()))
	{
		LogError1("No serializer was registered for format: %s", format.c_str());
		return false;
	}

	// create file
	File *file = FileServer::GetInstance()->NewFile();
	Assert(file);

	// open file
	bool exists = FileServer::GetInstance()->FileExists(file_path);
	if (!file->Open(file_path, "wb"))
	{
		LogError1("Error opening file: %s", file_path.c_str());
		delete file;
		return false;
	}

	// serialize
	byte_t *buffer = NULL;
	long buff_size = this->Serialize(obj, buffer, format.c_str());

	if (!buff_size)
		LogError1("Error serializing object to file: %s", file_path.c_str());
	else
	{
		// write buffer into the file
		if (file->Write(buffer, buff_size) != buff_size)
		{
			LogError1("Error writing serialized data to file: %s", file_path.c_str());
			buff_size = 0;
		}
	}

	// deletes data
	SafeDeleteArray(buffer);

	// close file
	file->Close();
	delete file;

	// remove empty file if it does not exists before
	if (!buff_size && !exists)
		FileServer::GetInstance()->DeleteFileEx(file_path);

	return buff_size > 0;
}


Serialized *SerializeServer::Deserialize(const byte_t *buffer, long buff_size, Serialized *obj, const char *format)
{
	Assert(buffer);
	Assert(buff_size);

	bool ok = true;
	bool first = true;

	// find serializer
	Serializer *serializer = this->NewSerializer(format);
	if (!serializer)
	{	
		LogError1("Error creating serializer for format: %s", format);
		return NULL;
	}

	// start deserializing
	serializer->BeginDeserialize(buffer, buff_size);

	// create root object if does not exists
	if (!obj)
	{
		string class_name, object_id;

		// check if buffer contains root object
		if (!serializer->CheckGroupName("object"))
		{
			LogError("Source does not contain root object");
			ok = false;
		}

		// create root node
		else
		{
			first = false;

			// get class and object names
			if (!serializer->GetAttribute("class", class_name, false) || !serializer->GetAttribute("name", object_id, false))
			{
				LogError("Missing 'class' or 'name' attribute");
				ok = false;
			}

			// create root node
			else
			{
				obj = this->kernel_server->New(class_name, object_id);
				if (!obj)
					ok = false;
			}
		}
	}

	// deserialize whole object
	if (ok)
	{
		if (!obj->Deserialize(*serializer, first))
		{
			LogError1("Error deserializing object with serializer: %s", serializer->GetID().c_str());
			ok = false;
		}
	}

	// finish deserializing
	serializer->EndDeserialize();
	serializer->Release();

	return ok ? obj : NULL;
}


Serialized *SerializeServer::Deserialize(const string &file_path, Serialized *obj)
{
	Assert(!file_path.empty());

	// get data format
	string format;
	FileServer::ExtractExtension(file_path, format);

	// check if serializer exists
	if (!this->IsSerializer(format.c_str()))
	{
		LogError1("No serializer was registered for format: %s", format.c_str());
		return false;
	}

	// create file
	File *file = FileServer::GetInstance()->NewFile();
	Assert(file);

	// open file
	if (!file->Open(file_path, "rb"))
	{
		LogError1("Error opening file: %s", file_path.c_str());
		delete file;
		return false;
	}

	// read whole file into memory buffer
	byte_t *buffer = file->ReadAll();
	if (!buffer)
	{
		LogError1("Error reading data from: %s", file_path.c_str());
		delete file;
		return false;
	}
	long file_size = file->GetSize();

	// close file
	file->Close();
	delete file;

	// deserialize
	obj = this->Deserialize(buffer, file_size, obj, format.c_str());

	// deletes data
	delete buffer;

	// if some error occures, write message
	if (!obj)
		LogError1("Error deserializing object from file: %s", file_path.c_str());

	return obj;
}


// vim:ts=4:sw=4:
