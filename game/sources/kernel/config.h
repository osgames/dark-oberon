#ifndef __kernel_config_h__
#define __kernel_config_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file kernel/config.h
	@ingroup Kernel_Module

 	@author Marian Cerny, Peter Knut
	@date 2003 - 2007

	@brief Configuration options for kernel pre-processing.

	@version 1.0 - Initial.
	@version 1.1 - Added USE_ASSERT definition.
	@version 1.2 - All config macros are defined in project properties.
*/


//=========================================================================
// Configuration
//=========================================================================

/**
	Defines target platform. WINDOWS is defined by default.
*/
#ifdef WINDOWS
#	undef UNIX
#	undef MACOS
#endif
#ifdef UNIX
#	undef MACOSX
#endif

#if !defined(WINDOWS) && !defined(UNIX) && !defined(MACOS)
#	define WINDOWS
#endif


/**
	Specifies the debugging mode. In this mode some debugging information is
	also printed to log files, and the lines in the logs will have a header
	with source file and line number to easily find the position in the source
	files, where the message was printed.
*/
#ifndef DEBUG
//#define DEBUG
#endif


/**
	Specifies the debugging mode for memory system. Works only together with WINDOWS platform.
	Not defined by default.
*/
#ifndef DEBUG_MEMORY
//#	define DEBUG_MEMORY
#endif


/**
	Specifies usage of Assert and Verify macros. Not defined by default.
*/
#ifndef USE_ASSERT
//#	define USE_ASSERT
#endif


#endif // __kernel_config_h__

// vim:ts=2:sw=2:et:
