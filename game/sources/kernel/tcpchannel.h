#ifndef __tcpchannel_h__
#define __tcpchannel_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file tcpchannel.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/ipcchannel.h"


//=========================================================================
// TcpChannel
//=========================================================================

/**
    @class TcpChannel
    @ingroup Ipc

    @brief The per-connection state for the network server.

    A IpcServer creates one TcpChannel for each connecting client.
*/

class TcpChannel : public IpcChannel
{
//--- methods
public:
	TcpChannel(SOCKET sock);
	virtual ~TcpChannel();

	virtual bool Send(const IpcBuffer &buffer);
	virtual bool Receive(IpcBuffer &buffer);
};


#endif

// vim:ts=4:sw=4:
