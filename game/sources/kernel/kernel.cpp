//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file kernel.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"


//=========================================================================
// Kernel module
//=========================================================================

extern "C" void Kernel();


extern bool s_InitObject (Class *, KernelServer *);
extern Object *s_NewObject (const char *name);
extern bool s_InitRoot (Class *, KernelServer *);
extern Object *s_NewRoot (const char *name);
extern bool s_InitTimeServer (Class *, KernelServer *);
extern Object *s_NewTimeServer (const char *name);
extern bool s_InitScriptServer (Class *, KernelServer *);
extern Object *s_NewScriptServer (const char *name);
extern bool s_InitScriptLoader (Class *, KernelServer *);
extern Object *s_NewScriptLoader (const char *name);
extern bool s_InitSerializeServer (Class *, KernelServer *);
extern Object *s_NewSerializeServer (const char *name);
extern bool s_InitSerializer (Class *, KernelServer *);
extern Object *s_NewSerializer (const char *name);
extern bool s_InitEnv (Class *, KernelServer *);
extern Object *s_NewEnv (const char *name);
extern bool s_InitFileServer (Class *, KernelServer *);
extern Object *s_NewFileServer (const char *name);
extern bool s_InitMessageServer (Class *, KernelServer *);
extern Object *s_NewMessageServer (const char *name);
extern bool s_InitIpcServer (Class *, KernelServer *);
extern Object *s_NewIpcServer (const char *name);

#ifdef USE_PROFILERS
extern bool s_InitProfiler (Class *, KernelServer *);
extern Object *s_NewProfiler (const char *name);
#endif

void Kernel()
{
	KernelServer::GetInstance()->RegisterClass("Object", s_InitObject, s_NewObject);
	KernelServer::GetInstance()->RegisterClass("Root", s_InitRoot, s_NewRoot);
	KernelServer::GetInstance()->RegisterClass("TimeServer", s_InitTimeServer, s_NewTimeServer);
	KernelServer::GetInstance()->RegisterClass("ScriptServer", s_InitScriptServer, s_NewScriptServer);
	KernelServer::GetInstance()->RegisterClass("ScriptLoader", s_InitScriptLoader, s_NewScriptLoader);
	KernelServer::GetInstance()->RegisterClass("SerializeServer", s_InitSerializeServer, s_NewSerializeServer);
	KernelServer::GetInstance()->RegisterClass("Serializer", s_InitSerializer, s_NewSerializer);
	KernelServer::GetInstance()->RegisterClass("Env", s_InitEnv, s_NewEnv);
	KernelServer::GetInstance()->RegisterClass("FileServer", s_InitFileServer, s_NewFileServer);
	KernelServer::GetInstance()->RegisterClass("MessageServer", s_InitMessageServer, s_NewMessageServer);
	KernelServer::GetInstance()->RegisterClass("IpcServer", s_InitIpcServer, s_NewIpcServer);

#ifdef USE_PROFILERS
	KernelServer::GetInstance()->RegisterClass("Profiler", s_InitProfiler, s_NewProfiler);
#endif
}


// vim:ts=4:sw=4:
