#ifndef _matrix33_h__
#define _matrix33_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file matrix33.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/vector2.h"
#include "kernel/vector3.h"

#include <memory.h>


static float matrix33_ident[9] = {
	1.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 1.0f,
};


//=========================================================================
// matrix33
//=========================================================================

/**
	@class matrix33
	@ingroup NebulaMathDataTypes

	A generic matrix33 class.
*/

class matrix33 {
//--- variables
public:
	float m[3][3];

//--- methods
public:
	matrix33();
	matrix33(const matrix33& mx);
	matrix33(const vector3& v0, const vector3& v1, const vector3& v2);
	matrix33(float m11, float m12, float m13, float m21, float m22, float m23, float m31, float m32, float m33);

	void set(const matrix33& m1);
	void set(const vector3& v0, const vector3& v1, const vector3& v2);
	void set(float m11, float m12, float m13, float m21, float m22, float m23, float m31, float m32, float m33);

	void operator *= (const matrix33& m1);

	void ident();
	void lookat(const vector3& from, const vector3& to, const vector3& up);
	void billboard(const vector3& from, const vector3& to, const vector3& up);
	void transpose();
	bool orthonorm(float limit);
	void scale(const vector3& s);

	void rotate_x(const float a);
	void rotate_y(const float a);
	void rotate_z(const float a);

	void rotate_local_x(const float a);
	void rotate_local_y(const float a);
	void rotate_local_z(const float a);
	void rotate(const vector3& vec, float a);

	vector3 x_component(void) const;
	vector3 y_component(void) const;
	vector3 z_component(void) const;

	void mult(const vector3& src, vector3& dst) const;
	void translate(const vector2& t);
};


//=========================================================================
// Methods
//=========================================================================


/**
	Contructor.
*/
inline
matrix33::matrix33() 
{
	memcpy(&(m[0][0]), matrix33_ident, sizeof(matrix33_ident));
}


/**
	Contructor.
*/
inline
matrix33::matrix33(const matrix33& m1) 
{
	this->set(m1);
}


/**
	Contructor.
*/
inline
matrix33::matrix33(const vector3& v0, const vector3& v1, const vector3& v2) 
{
	this->set(v0, v1, v2);
}


/**
	Contructor.
*/
inline
matrix33::matrix33(float m11, float m12, float m13,
					float m21, float m22, float m23,
					float m31, float m32, float m33)
{
	this->set(m11, m12, m13, m21, m22, m23, m31, m32, m33);
}


/**
	Sets new values.
*/
inline
void matrix33::set(const matrix33& m1) 
{
	memcpy(m, &(m1.m), 9 * sizeof(float));
}


/**
	Sets new values.
*/
inline
void  matrix33::set(const vector3& v0, const vector3& v1, const vector3& v2) 
{
	M11 = v0.x; M12 = v0.y; M13 = v0.z;
	M21 = v1.x; M22 = v1.y; M23 = v1.z;
	M31 = v2.x; M32 = v2.y; M33 = v2.z;
}


/**
	Sets new values.
*/
inline
void matrix33::set(float m11, float m12, float m13,
					float m21, float m22, float m23,
					float m31, float m32, float m33) 
{
	M11 = m11; M12 = m12; M13 = m13;
	M21 = m21; M22 = m22; M23 = m23;
	M31 = m31; M32 = m32; M33 = m33;
}


/**
	Inplace matrix multiply.
*/
static 
inline 
matrix33 operator * (const matrix33& m0, const matrix33& m1) 
{
	matrix33 m2(
		m0.m[0][0]*m1.m[0][0] + m0.m[0][1]*m1.m[1][0] + m0.m[0][2]*m1.m[2][0],
		m0.m[0][0]*m1.m[0][1] + m0.m[0][1]*m1.m[1][1] + m0.m[0][2]*m1.m[2][1],
		m0.m[0][0]*m1.m[0][2] + m0.m[0][1]*m1.m[1][2] + m0.m[0][2]*m1.m[2][2],

		m0.m[1][0]*m1.m[0][0] + m0.m[1][1]*m1.m[1][0] + m0.m[1][2]*m1.m[2][0],
		m0.m[1][0]*m1.m[0][1] + m0.m[1][1]*m1.m[1][1] + m0.m[1][2]*m1.m[2][1],
		m0.m[1][0]*m1.m[0][2] + m0.m[1][1]*m1.m[1][2] + m0.m[1][2]*m1.m[2][2],

		m0.m[2][0]*m1.m[0][0] + m0.m[2][1]*m1.m[1][0] + m0.m[2][2]*m1.m[2][0],
		m0.m[2][0]*m1.m[0][1] + m0.m[2][1]*m1.m[1][1] + m0.m[2][2]*m1.m[2][1],
		m0.m[2][0]*m1.m[0][2] + m0.m[2][1]*m1.m[1][2] + m0.m[2][2]*m1.m[2][2]
	);

	return m2;
}


/**
	Rightplace vector multiply.
*/
static 
inline vector3 operator * (const matrix33& m, const vector3& v)
{
	return vector3(
		m.M11*v.x + m.M21*v.y + m.M31*v.z,
		m.M12*v.x + m.M22*v.y + m.M32*v.z,
		m.M13*v.x + m.M23*v.y + m.M33*v.z
	);
};


/**
*/
inline
void matrix33::operator *= (const matrix33& m1) 
{
	for (int i=0; i<3; i++) {
		float mi0 = m[i][0];
		float mi1 = m[i][1];
		float mi2 = m[i][2];
		m[i][0] = mi0*m1.m[0][0] + mi1*m1.m[1][0] + mi2*m1.m[2][0];
		m[i][1] = mi0*m1.m[0][1] + mi1*m1.m[1][1] + mi2*m1.m[2][1];
		m[i][2] = mi0*m1.m[0][2] + mi1*m1.m[1][2] + mi2*m1.m[2][2];
	};
}


/**
*/
inline
void matrix33::ident() 
{
	memcpy(&(m[0][0]), matrix33_ident, sizeof(matrix33_ident));
}


/**
	Unrestricted lookat.
*/
inline
void  matrix33::lookat(const vector3& from, const vector3& to, const vector3& up) 
{
	vector3 z(from - to);
	z.norm();
	vector3 x(up * z);   // x = y cross z
	x.norm();
	vector3 y = z * x;   // y = z cross x

	M11=x.x;  M12=x.y;  M13=x.z;
	M21=y.x;  M22=y.y;  M23=y.z;
	M31=z.x;  M32=z.y;  M33=z.z;
}


/**
	Restricted lookat (billboard).
*/
inline
void matrix33::billboard(const vector3& from, const vector3& to, const vector3& up)
{
	vector3 z(from - to);
	z.norm();
	vector3 y(up);
	y.norm();
	vector3 x(y * z);
	z = x * y;

	M11=x.x;  M12=x.y;  M13=x.z;
	M21=y.x;  M22=y.y;  M23=y.z;
	M31=z.x;  M32=z.y;  M33=z.z;
}


/**
	Sets to transpose.
*/
inline
void matrix33::transpose()
{
	#undef n_swap
	#define n_swap(x, y) { float t=x; x=y; y=t; }

	n_swap(m[0][1],m[1][0]);
	n_swap(m[0][2],m[2][0]);
	n_swap(m[1][2],m[2][1]);
}


/**
	Is orthonormal?
*/
inline
bool matrix33::orthonorm(float limit) 
{
	return (
		((M11*M21+M12*M22+M13*M23)<limit) &&
		((M11*M31+M12*M32+M13*M33)<limit) &&
		((M31*M21+M32*M22+M33*M23)<limit) &&
		((M11*M11+M12*M12+M13*M13)>(1.0-limit)) &&
		((M11*M11+M12*M12+M13*M13)<(1.0+limit)) &&
		((M21*M21+M22*M22+M23*M23)>(1.0-limit)) &&
		((M21*M21+M22*M22+M23*M23)<(1.0+limit)) &&
		((M31*M31+M32*M32+M33*M33)>(1.0-limit)) &&
		((M31*M31+M32*M32+M33*M33)<(1.0+limit))
	);
}


/**
	Scales.
*/
inline
void
matrix33::scale(const vector3& s)
{
	for (int i =0 ; i < 3; i++) {
		m[i][0] *= s.x;
		m[i][1] *= s.y;
		m[i][2] *= s.z;
	}
}


/**
	Rotates about global X.
*/
inline
void matrix33::rotate_x(const float a)
{
	float c = cos(a);
	float s = sin(a);

	for (int i = 0; i < 3; i++) {
		float mi1 = m[i][1];
		float mi2 = m[i][2];
		m[i][1] = mi1 * c + mi2 * -s;
		m[i][2] = mi1 * s + mi2 * c;
	}
}


/**
	Rotates about global Y.
*/
inline
void matrix33::rotate_y(const float a)
{
	float c = cos(a);
	float s = sin(a);

	for (int i = 0; i < 3; i++) {
		float mi0 = m[i][0];
		float mi2 = m[i][2];
		m[i][0] = mi0 * c + mi2 * s;
		m[i][2] = mi0 * -s + mi2 * c;
	}
}


/**
	Rotates about global Z.
*/
inline
void matrix33::rotate_z(const float a)
{
	float c = cos(a);
	float s = sin(a);

	for (int i = 0; i < 3; i++) {
		float mi0 = m[i][0];
		float mi1 = m[i][1];
		m[i][0] = mi0 * c + mi1 * -s;
		m[i][1] = mi0 * s + mi1 * c;
	}
}


/**
	Rotates about local X (not very fast).
*/
inline
void matrix33::rotate_local_x(const float a)
{
	matrix33 rotM;  // initialized as identity matrix
	rotM.M22 = (float) cos(a); rotM.M23 = -(float) sin(a);
	rotM.M32 = (float) sin(a); rotM.M33 =  (float) cos(a);

	(*this) = rotM * (*this);
}


/**
	Rotates about local Y (not very fast).
*/
inline
void 
matrix33::rotate_local_y(const float a)
{
	matrix33 rotM;  // initialized as identity matrix
	rotM.M11 = (float) cos(a);  rotM.M13 = (float) sin(a);
	rotM.M31 = -(float) sin(a); rotM.M33 = (float) cos(a);

	(*this) = rotM * (*this); 
}


/**
	Rotates about local Z (not very fast).
*/
inline
void 
matrix33::rotate_local_z(const float a)
{
	matrix33 rotM;  // initialized as identity matrix
	rotM.M11 = (float) cos(a); rotM.M12 = -(float) sin(a);
	rotM.M21 = (float) sin(a); rotM.M22 =  (float) cos(a);

	(*this) = rotM * (*this); 
}


/**
	Rotates about any axis.
*/
inline
void matrix33::rotate(const vector3& vec, float a)
{
	vector3 v(vec);
	v.norm();
	float sa = (float) sin(a);
	float ca = (float) cos(a);

	matrix33 rotM;
	rotM.M11 = ca + (1.0f - ca) * v.x * v.x;
	rotM.M12 = (1.0f - ca) * v.x * v.y - sa * v.z;
	rotM.M13 = (1.0f - ca) * v.z * v.x + sa * v.y;
	rotM.M21 = (1.0f - ca) * v.x * v.y + sa * v.z;
	rotM.M22 = ca + (1.0f - ca) * v.y * v.y;
	rotM.M23 = (1.0f - ca) * v.y * v.z - sa * v.x;
	rotM.M31 = (1.0f - ca) * v.z * v.x - sa * v.y;
	rotM.M32 = (1.0f - ca) * v.y * v.z + sa * v.x;
	rotM.M33 = ca + (1.0f - ca) * v.z * v.z;

	(*this) = (*this) * rotM;
}


/**
	Gets X component.
*/
inline
vector3 matrix33::x_component() const
{
	return vector3(M11,M12,M13);
}


/**
	Gets Y component.
*/
inline
vector3 
matrix33::y_component(void) const
{
	return vector3(M21,M22,M23);
}


/**
	Gets Z component.
*/
inline
vector3 
matrix33::z_component(void) const 
{
	return vector3(M31,M32,M33);
};


/**
	Multiplies source vector with matrix and store in destination vector
	this eliminates the construction of a temp vector3 object
*/
inline
void matrix33::mult(const vector3& src, vector3& dst) const
{
	dst.x = M11*src.x + M21*src.y + M31*src.z;
	dst.y = M12*src.x + M22*src.y + M32*src.z;
	dst.z = M13*src.x + M23*src.y + M33*src.z;
}


/**
	Translate, this treats the matrix as a 2x2 rotation + translate matrix.
*/
inline
void matrix33::translate(const vector2& t)
{
	M31 += t.x;
	M32 += t.y;
}


#endif // __metrix33_h__

// vim:ts=4:sw=4:
