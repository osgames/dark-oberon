//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file udpchannel.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/udpchannel.h"
#include "kernel/ipcbuffer.h"
#include "kernel/logserver.h"


//=========================================================================
// UdpChannel
//=========================================================================

/**
*/
UdpChannel::UdpChannel(SOCKET sock) :
	IpcChannel(sock)
{
	this->protocol = PROTOCOL_UDP;
}


bool UdpChannel::Receive(IpcBuffer &buffer)
{
	Assert(this->can_receive);

	if (this->socket_id == INVALID_SOCKET)
		return true;

	sockaddr_in remote_addr;
	socklen_t remote_addr_size = sizeof(remote_addr);
	int len;

	// do a non-blocking recv
	len = recvfrom(
		this->socket_id,
		buffer.GetPointer(),
		int(buffer.GetMaxSize()),
		0,
		(sockaddr *) &remote_addr,
		&remote_addr_size
	);

	if (len == SOCKET_ERROR)
	{		
		switch (N_SOCKET_LAST_ERROR)
		{
		case N_EWOULDBLOCK:
			buffer.SetSize(0);
			return true;

		case N_ECONNRESET:
			LogInfo("UDP port unreachable for last sending");
			return true;

		default:
			LogInfo1("Receiving UDP data failed: %d", this->channel_id);
			this->DestroySocket();
			return false;
		}
	}


	// something has been received, fill out the fromAddr
	this->address.SetAddrStruct(remote_addr);
	buffer.SetSize(len);

	return true;
}


bool UdpChannel::Send(const IpcBuffer &buffer)
{
	if (this->socket_id == INVALID_SOCKET)
		return false;

	int result = sendto(
		this->socket_id,
		(const char *)buffer.GetPointer(),
		int(buffer.GetSize()),
		0,
		(const sockaddr *) &(this->address.GetAddrStruct()),
		sizeof(this->address.GetAddrStruct())
	);

	if (result == SOCKET_ERROR || result < int(buffer.GetSize()))
	{
		LogInfo2("Sending UDP data failed by channel %d: %d", this->channel_id, N_SOCKET_LAST_ERROR);
		return false;
	}

	return true;
}


// vim:ts=4:sw=4:
