#ifndef __fileserver_h__
#define __fileserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file fileserver.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added wrappers for standart streams.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/rootserver.h"
#include "kernel/file.h"
#include "kernel/directory.h"


//=========================================================================
// FileServer
//=========================================================================

/**
	@class FileServer
	@ingroup Kernel_Module

	@brief Central server object of file system.
	Provides functions for creating file and directory objects and assigns.
*/

class FileServer : public RootServer<FileServer>
{
//--- variables
protected:
	Root *assigns;
	static File std_in;
	static File std_out;
	static File std_err;

//--- methods
public:
	FileServer(const char *id);
	virtual ~FileServer();

	// assigns
	bool SetAssign(const string &assign, const string &path);
	const char *GetAssign(const string &assign);
	void ResetAssigns();

	// paths
	bool ManglePath(const string &path, string &full_path);
	static void CleanupPath(string &path);
	static void ExtractDirectory(const string &path, string &dir);
	static void ExtractFile(const string &path, string &file);
	static void ExtractFileName(const string &path, string &file_name);
	static void ExtractExtension(const string &path, string &extension);

	// objects
	Directory *NewDirectory() const;
	File *NewFile() const;

	// standart streams
	static File *GetStdIn();
	static File *GetStdOut();
	static File *GetStdErr();
	bool IsStandartStream(const File *file) const;

	// file system
	bool FileExists(const string &path) const;
	bool DirectoryExists(const string &path) const;
	bool CopyFileEx(const string &from, const string &to);
	bool Checksum(const string &file_name, uint_t &crc);
	bool CreatePath(const string &path);
	bool DeleteFileEx(const string &file_name);
	bool DeleteDirectory(const string &dir_name);

	bool ListFiles(vector<string> &file_list, const string &dir_name);
	bool ListDirectories(vector<string> &file_list, const string &dir_name, bool special = false);

protected:
	void InitAssigns();
	void InitAppAssign();
	void InitUserAssign();
};


//=========================================================================
// Methods
//=========================================================================

/**
	Creates a new Directory object.

	@return    The Directory object.
*/
inline
Directory *FileServer::NewDirectory() const
{
	Directory *result = NEW Directory;
	Assert(result);

	return result;
}


/**
	Creates a new File object.

	@return    The File object.
*/
inline
File *FileServer::NewFile() const
{
	File *result = NEW File;
	Assert(result);

	return result;
}


/**
	Returns wrapper of standart input.

	@return    Wrapper of standart input.
*/
inline
File *FileServer::GetStdIn()
{
	return &std_in;
}


/**
	Returns wrapper of standart output.

	@return    Wrapper of standart input.
*/
inline
File *FileServer::GetStdOut()
{
	return &std_out;
}


/**
	Returns wrapper of standart error output.

	@return    Wrapper of standart input.
*/
inline
File *FileServer::GetStdErr()
{
	return &std_err;
}


/**
	Check if given file is a wrapper of standart stream.

	@return    @c True if file is a wrapper of standart stream.
*/
inline
bool FileServer::IsStandartStream(const File *file) const
{
	return file == &this->std_in || file == &this->std_out || file == &this->std_err;
}


#endif // __fileserver_h__

// vim:ts=4:sw=4:
