#ifndef __serializer_h__
#define __serializer_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file serializer.h
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/root.h"
#include "kernel/vector.h"


//=========================================================================
// Serializer
//=========================================================================

/**
	@class Serializer
	@ingroup Kernel_Module

	Serializers are created and managed by SerializeServer. There can be
	more than one serializer at the same time.
*/

class Serializer : public Root
{
//--- embeded
protected:
	enum SerializerState
	{
		SS_NONE,
		SS_SERIALIZING,
		SS_DESERIALIZING
	};

//--- varialblees
protected:
	SerializerState state;
	ushort_t act_goups_count;

	byte_t *serialize_buffer;
	const byte_t *deserialize_buffer;
	long serialize_size;
	long deserialize_size;

//--- methods
public:
	Serializer(const char *id);
	virtual ~Serializer();

	// serializing
	virtual bool BeginSerialize(const char *root_name);
	virtual long EndSerialize(byte_t *&buffer);

	virtual bool AddGroup(const char *group_name);

	virtual bool SetAttribute(const char *attr_name, const char *value);
	virtual bool SetAttribute(const char *attr_name, const string &value);
	virtual bool SetAttribute(const char *attr_name, byte_t value);
	virtual bool SetAttribute(const char *attr_name, ushort_t value);
	virtual bool SetAttribute(const char *attr_name, uint_t value);
	virtual bool SetAttribute(const char *attr_name, short value);
	virtual bool SetAttribute(const char *attr_name, int value);
	virtual bool SetAttribute(const char *attr_name, long value);
	virtual bool SetAttribute(const char *attr_name, float value);
	virtual bool SetAttribute(const char *attr_name, double value);
	virtual bool SetAttribute(const char *attr_name, bool value);
	virtual bool SetAttribute(const char *attr_name, const vector2 &value);
	virtual bool SetAttribute(const char *attr_name, const vector3 &value);
	virtual bool SetAttribute(const char *attr_name, const vector4 &value);

	virtual bool SetAttributePercent(const char *attr_name, float value);
	virtual bool SetAttributeColor(const char *attr_name, const vector3 &value);
	virtual bool SetAttributeColor(const char *attr_name, const vector4 &value);

	virtual bool SetValue(const char *value);
	virtual bool SetValue(const string &value);
	virtual bool SetValue(byte_t value);
	virtual bool SetValue(ushort_t value);
	virtual bool SetValue(uint_t value);
	virtual bool SetValue(short value);
	virtual bool SetValue(int value);
	virtual bool SetValue(long value);
	virtual bool SetValue(float value);
	virtual bool SetValue(double value);
	virtual bool SetValue(bool value);
	virtual bool SetValue(const vector2 &value);
	virtual bool SetValue(const vector3 &value);
	virtual bool SetValue(const vector4 &value);

	// deserializing
	virtual bool BeginDeserialize(const byte_t *buffer, long buff_size);
	virtual void EndDeserialize();

	virtual bool GetGroup(const char *group_name = NULL);
	virtual bool GetNextGroup(bool mixed = false);

	virtual bool GetAttribute(const char *attr_name, string &value, bool empty = true);
	virtual bool GetAttribute(const char *attr_name, byte_t &value);
	virtual bool GetAttribute(const char *attr_name, ushort_t &value);
	virtual bool GetAttribute(const char *attr_name, uint_t &value);
	virtual bool GetAttribute(const char *attr_name, short &value);
	virtual bool GetAttribute(const char *attr_name, int &value);
	virtual bool GetAttribute(const char *attr_name, long &value);
	virtual bool GetAttribute(const char *attr_name, float &value);
	virtual bool GetAttribute(const char *attr_name, double &value);
	virtual bool GetAttribute(const char *attr_name, bool &value);
	virtual bool GetAttribute(const char *attr_name, vector2 &value);
	virtual bool GetAttribute(const char *attr_name, vector3 &value);
	virtual bool GetAttribute(const char *attr_name, vector4 &value);

	virtual bool GetAttributePercent(const char *attr_name, float &value);
	virtual bool GetAttributeColor(const char *attr_name, vector3 &value);
	virtual bool GetAttributeColor(const char *attr_name, vector4 &value);

	virtual bool GetValue(string &value, bool empty = true);
	virtual bool GetValue(byte_t &value);
	virtual bool GetValue(ushort_t &value);
	virtual bool GetValue(uint_t &value);
	virtual bool GetValue(short &value);
	virtual bool GetValue(int &value);
	virtual bool GetValue(long &value);
	virtual bool GetValue(float &value);
	virtual bool GetValue(double &value);
	virtual bool GetValue(bool &value);
	virtual bool GetValue(vector2 &value);
	virtual bool GetValue(vector3 &value);
	virtual bool GetValue(vector4 &value);

	// universal
	virtual bool CheckGroupName(const char *name);
	virtual ushort_t GetGroupsCount();
	virtual void EndGroup();
};


#endif // __serializer_h__

// vim:ts=4:sw=4:
