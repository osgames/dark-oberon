#ifndef __matrix_h__
#define __matrix_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file matrix.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005

	Implement 3x3 and 4x4 matrix classes.

	@version 1.0 - Initial.
*/


//=========================================================================
// Defines
//=========================================================================

#define M11 m[0][0]
#define M12 m[0][1]
#define M13 m[0][2]
#define M14 m[0][3]
#define M21 m[1][0]
#define M22 m[1][1]
#define M23 m[1][2]
#define M24 m[1][3]
#define M31 m[2][0]
#define M32 m[2][1]
#define M33 m[2][2]
#define M34 m[2][3]
#define M41 m[3][0]
#define M42 m[3][1]
#define M43 m[3][2]
#define M44 m[3][3]


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/matrix33.h"
#include "kernel/matrix44.h"


#endif // __matrix_h__

// vim:ts=4:sw=4:
