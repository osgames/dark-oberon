#ifndef _matrix44_h__
#define _matrix44_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file matrix44.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005

	Implement 2, 3 and 4-dimensional vector classes.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/vector4.h"
#include "kernel/vector3.h"

static float matrix44_ident[16] = 
{
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f,
};


//=========================================================================
// matrix33
//=========================================================================

/**
	@class matrix44
	@ingroup NebulaMathDataTypes

	Generic matrix44 class.
*/

class matrix44 {
//--- variables
public:
	float m[4][4];

//--- methods
public:
	matrix44();
	matrix44(const matrix44& m1);
	matrix44(const vector4& v0, const vector4& v1, const vector4& v2, const vector4& v3);
	matrix44(float m11, float m12, float m13, float m14,
				float m21, float m22, float m23, float m24,
				float m31, float m32, float m33, float m34,
				float m41, float m42, float m43, float m44);

	void set(const vector4& v0, const vector4& v1, const vector4& v2, const vector4& v3);
	void set(const matrix44& m1);
	void set(float m11, float m12, float m13, float m14,
				float m21, float m22, float m23, float m24,
				float m31, float m32, float m33, float m34,
				float m41, float m42, float m43, float m44);

	void operator *= (const matrix44& m1);

	void ident();
	void transpose();

	float det();
	void invert(void);
	void invert_simple(void);
	void mult_simple(const matrix44& m1);

	vector3 transform_coord(const vector3& v) const;
	vector3& x_component() const;
	vector3& y_component() const;
	vector3& z_component() const;
	vector3& pos_component() const;

	void rotate_x(const float a);
	void rotate_y(const float a);
	void rotate_z(const float a);
	void rotate(const vector3& vec, float a);

	void translate(const vector3& t);
	void set_translation(const vector3& t);
	void scale(const vector3& s);

	void lookatLh(const vector3& to, const vector3& up);
	void lookatRh(const vector3& to, const vector3& up);
	void perspFovLh(float fovY, float aspect, float zn, float zf);
	void perspFovRh(float fovY, float aspect, float zn, float zf);
	void perspOffCenterLh(float minX, float maxX, float minY, float maxY, float zn, float zf);
	void perspOffCenterRh(float minX, float maxX, float minY, float maxY, float zn, float zf);
	void orthoLh(float w, float h, float zn, float zf);
	void orthoRh(float w, float h, float zn, float zf);

	void billboard(const vector3& to, const vector3& up);
	void mult(const vector4& src, vector4& dst) const;
	void mult(const vector3& src, vector3& dst) const;

	vector3 mult_divw(const vector3& v) const;
	void weightedmadd(const vector3& src, vector3& dst, float weight) const;
};


/**
	Constructor.
*/
inline
matrix44::matrix44()
{
	memcpy(&(m[0][0]), matrix44_ident, sizeof(matrix44_ident));
}


/**
	Constructor.
*/
inline
matrix44::matrix44(const vector4& v0, const vector4& v1, const vector4& v2, const vector4& v3)
{
	M11 = v0.x; M12 = v0.y; M13 = v0.z; M14 = v0.w;
	M21 = v1.x; M22 = v1.y; M23 = v1.z; M24 = v1.w;
	M31 = v2.x; M32 = v2.y; M33 = v2.z; M34 = v2.w;
	M41 = v3.x; M42 = v3.y; M43 = v3.z; M44 = v3.w;
}


/**
	Constructor.
*/
inline
matrix44::matrix44(const matrix44& m1) 
{
	memcpy(m, &(m1.m[0][0]), 16 * sizeof(float));
}


/**
	Constructor.
*/
inline
matrix44::matrix44(float m11, float m12, float m13, float m14,
					float m21, float m22, float m23, float m24,
					float m31, float m32, float m33, float m34,
					float m41, float m42, float m43, float m44)
{
	M11 = m11; M12 = m12; M13 = m13; M14 = m14;
	M21 = m21; M22 = m22; M23 = m23; M24 = m24;
	M31 = m31; M32 = m32; M33 = m33; M34 = m34;
	M41 = m41; M42 = m42; M43 = m43; M44 = m44;
}


/**
	Sets new values.
*/
inline
void 
matrix44::set(const vector4& v0, const vector4& v1, const vector4& v2, const vector4& v3) 
{
	M11 = v0.x; M12 = v0.y; M13 = v0.z; M14 = v0.w;
	M21 = v1.x; M22 = v1.y; M23 = v1.z; M24 = v1.w;
	M31 = v2.x; M32 = v2.y; M33 = v2.z; M34 = v2.w;
	M41 = v3.x; M42 = v3.y; M43 = v3.z; M44 = v3.w;
}


/**
	Sets new values.
*/
inline
void 
matrix44::set(const matrix44& m1) 
{
	memcpy(m, &(m1.m[0][0]), 16*sizeof(float));
}


/**
	Sets new values.
*/
inline
void
matrix44::set(float m11, float m12, float m13, float m14,
				float m21, float m22, float m23, float m24,
				float m31, float m32, float m33, float m34,
				float m41, float m42, float m43, float m44)
{
	M11 = m11; M12 = m12; M13 = m13; M14 = m14;
	M21 = m21; M22 = m22; M23 = m23; M24 = m24;
	M31 = m31; M32 = m32; M33 = m33; M34 = m34;
	M41 = m41; M42 = m42; M43 = m43; M44 = m44;
}


/**
	Sets to identity.
*/
inline
void 
matrix44::ident() 
{
	memcpy(&(m[0][0]), matrix44_ident, sizeof(matrix44_ident));
}


/**
	Transposes.
*/
inline
void 
matrix44::transpose() 
{
	#undef n_swap
	#define n_swap(x,y) { float t=x; x=y; y=t; }
	n_swap(M12, M21);
	n_swap(M13, M31);
	n_swap(M14, M41);
	n_swap(M23, M32);
	n_swap(M24, M42);
	n_swap(M34, M43);
}


/**
	Returns determinant.
*/
inline
float 
matrix44::det() 
{
	return
		(M11 * M22 - M12 * M21) * (M33 * M44 - M34 * M43)
		-(M11 * M23 - M13 * M21) * (M32 * M44 - M34 * M42)
		+(M11 * M24 - M14 * M21) * (M32 * M43 - M33 * M42)
		+(M12 * M23 - M13 * M22) * (M31 * M44 - M34 * M41)
		-(M12 * M24 - M14 * M22) * (M31 * M43 - M33 * M41)
		+(M13 * M24 - M14 * M23) * (M31 * M42 - M32 * M41);
}


/**
	Full invert.
*/
inline
void
matrix44::invert() 
{
	float s = det();
	if (s == 0.0) return;
	s = 1/s;
	this->set(
		s*(M22*(M33*M44 - M34*M43) + M23*(M34*M42 - M32*M44) + M24*(M32*M43 - M33*M42)),
		s*(M32*(M13*M44 - M14*M43) + M33*(M14*M42 - M12*M44) + M34*(M12*M43 - M13*M42)),
		s*(M42*(M13*M24 - M14*M23) + M43*(M14*M22 - M12*M24) + M44*(M12*M23 - M13*M22)),
		s*(M12*(M24*M33 - M23*M34) + M13*(M22*M34 - M24*M32) + M14*(M23*M32 - M22*M33)),
		s*(M23*(M31*M44 - M34*M41) + M24*(M33*M41 - M31*M43) + M21*(M34*M43 - M33*M44)),
		s*(M33*(M11*M44 - M14*M41) + M34*(M13*M41 - M11*M43) + M31*(M14*M43 - M13*M44)),
		s*(M43*(M11*M24 - M14*M21) + M44*(M13*M21 - M11*M23) + M41*(M14*M23 - M13*M24)),
		s*(M13*(M24*M31 - M21*M34) + M14*(M21*M33 - M23*M31) + M11*(M23*M34 - M24*M33)),
		s*(M24*(M31*M42 - M32*M41) + M21*(M32*M44 - M34*M42) + M22*(M34*M41 - M31*M44)),
		s*(M34*(M11*M42 - M12*M41) + M31*(M12*M44 - M14*M42) + M32*(M14*M41 - M11*M44)),
		s*(M44*(M11*M22 - M12*M21) + M41*(M12*M24 - M14*M22) + M42*(M14*M21 - M11*M24)),
		s*(M14*(M22*M31 - M21*M32) + M11*(M24*M32 - M22*M34) + M12*(M21*M34 - M24*M31)),
		s*(M21*(M33*M42 - M32*M43) + M22*(M31*M43 - M33*M41) + M23*(M32*M41 - M31*M42)),
		s*(M31*(M13*M42 - M12*M43) + M32*(M11*M43 - M13*M41) + M33*(M12*M41 - M11*M42)),
		s*(M41*(M13*M22 - M12*M23) + M42*(M11*M23 - M13*M21) + M43*(M12*M21 - M11*M22)),
		s*(M11*(M22*M33 - M23*M32) + M12*(M23*M31 - M21*M33) + M13*(M21*M32 - M22*M31)));
}


/**
	Quick invert (if 3x3 rotation and translation).

	Inverts a 4x4 matrix consisting of a 3x3 rotation matrix and
	a translation (eg. everything that has [0,0,0,1] as
	the rightmost column) MUCH cheaper then a real 4x4 inversion
*/
inline
void 
matrix44::invert_simple() 
{
	float s = det();
	if (s == 0.0f) return;
	s = 1.0f/s;
	this->set(
		s * ((M22 * M33) - (M23 * M32)),
		s * ((M32 * M13) - (M33 * M12)),
		s * ((M12 * M23) - (M13 * M22)),
		0.0f,
		s * ((M23 * M31) - (M21 * M33)),
		s * ((M33 * M11) - (M31 * M13)),
		s * ((M13 * M21) - (M11 * M23)),
		0.0f,
		s * ((M21 * M32) - (M22 * M31)),
		s * ((M31 * M12) - (M32 * M11)),
		s * ((M11 * M22) - (M12 * M21)),
		0.0f,
		s * (M21*(M33*M42 - M32*M43) + M22*(M31*M43 - M33*M41) + M23*(M32*M41 - M31*M42)),
		s * (M31*(M13*M42 - M12*M43) + M32*(M11*M43 - M13*M41) + M33*(M12*M41 - M11*M42)),
		s * (M41*(M13*M22 - M12*M23) + M42*(M11*M23 - M13*M21) + M43*(M12*M21 - M11*M22)),
		1.0f);
}


/**
	Optimized multiplication, assumes that M14==M24==M34==0 AND M44==1.
*/
inline
void
matrix44::mult_simple(const matrix44& m1) 
{
	int i;
	for (i=0; i<4; i++) 
	{
		float mi0 = m[i][0];
		float mi1 = m[i][1];
		float mi2 = m[i][2];
		m[i][0] = mi0*m1.m[0][0] + mi1*m1.m[1][0] + mi2*m1.m[2][0];
		m[i][1] = mi0*m1.m[0][1] + mi1*m1.m[1][1] + mi2*m1.m[2][1];
		m[i][2] = mi0*m1.m[0][2] + mi1*m1.m[1][2] + mi2*m1.m[2][2];
	}
	m[3][0] += m1.m[3][0];
	m[3][1] += m1.m[3][1];
	m[3][2] += m1.m[3][2];
	m[0][3] = 0.0f;
	m[1][3] = 0.0f;
	m[2][3] = 0.0f;
	m[3][3] = 1.0f;
}


/**
	Transforms a vector by the matrix, projecting the result back into w=1.
*/
inline
vector3
matrix44::transform_coord(const vector3& v) const
{
	float d = 1.0f / (M14*v.x + M24*v.y + M34*v.z + M44);
	return vector3(
		(M11*v.x + M21*v.y + M31*v.z + M41) * d,
		(M12*v.x + M22*v.y + M32*v.z + M42) * d,
		(M13*v.x + M23*v.y + M33*v.z + M43) * d);
}


/**
	Returns X component.
	@return the first row of the matrix. (M11, M12 and M13)
*/
inline
vector3&
matrix44::x_component() const
{  
	return *(vector3*)&M11;
}


/**
	Returns Y component.
	@return the second row of the matrix. (M21, M22 and M23)
*/
inline
vector3&
matrix44::y_component() const
{
	return *(vector3*)&M21;
}


/**
	Returns Z component.
	@return the third row of the matrix. (M31, M32 and M33)
*/
inline
vector3&
matrix44::z_component() const
{
	return *(vector3*)&M31;
}


/**
	Returns translate component.
    @return the 4th row of the matrix. (M41, M42 and M43)
*/
inline
vector3& 
matrix44::pos_component() const
{
	return *(vector3*)&M41;
}


/**
	Rotates around global X.
*/
inline
void
matrix44::rotate_x(const float a) 
{
	float c = cos(a);
	float s = sin(a);
	int i;
	for (i=0; i<4; i++) {
		float mi1 = m[i][1];
		float mi2 = m[i][2];
		m[i][1] = mi1*c + mi2*-s;
		m[i][2] = mi1*s + mi2*c;
	}
}


/**
	Rotates around global Y.
*/
inline
void 
matrix44::rotate_y(const float a) 
{
	float c = cos(a);
	float s = sin(a);
	int i;
	for (i=0; i<4; i++) {
		float mi0 = m[i][0];
		float mi2 = m[i][2];
		m[i][0] = mi0*c + mi2*s;
		m[i][2] = mi0*-s + mi2*c;
	}
}


/**
	Rotates around global Z.
*/
inline
void 
matrix44::rotate_z(const float a) 
{
	float c = cos(a);
	float s = sin(a);
	int i;
	for (i=0; i<4; i++) {
		float mi0 = m[i][0];
		float mi1 = m[i][1];
		m[i][0] = mi0*c + mi1*-s;
		m[i][1] = mi0*s + mi1*c;
	}
}


/**
	Translates.
*/
inline
void 
matrix44::translate(const vector3& t) 
{
	M41 += t.x;
	M42 += t.y;
	M43 += t.z;
}


/**
	Sets absolute translation.
*/
inline
void
matrix44::set_translation(const vector3& t) 
{
	M41 = t.x;
	M42 = t.y;
	M43 = t.z;
};


/**
	Scales.
*/
inline
void
matrix44::scale(const vector3& s) 
{
	int i;
	for (i=0; i<4; i++) 
	{
		m[i][0] *= s.x;
		m[i][1] *= s.y;
		m[i][2] *= s.z;
	}
}


/**
	Lookat in a right-handed coordinate system.
*/
inline
void 
matrix44::lookatRh(const vector3& at, const vector3& up) 
{
	vector3 eye(M41, M42, M43);
	vector3 zaxis = eye - at;
	zaxis.norm();
	vector3 xaxis = up * zaxis;
	xaxis.norm();
	vector3 yaxis = zaxis * xaxis;
	M11 = xaxis.x;  M12 = xaxis.y;  M13 = xaxis.z;  M14 = 0.0f;
	M21 = yaxis.x;  M22 = yaxis.y;  M23 = yaxis.z;  M24 = 0.0f;
	M31 = zaxis.x;  M32 = zaxis.y;  M33 = zaxis.z;  M34 = 0.0f;
}


/**
	Lookat in a left-handed coordinate system.
*/
inline
void 
matrix44::lookatLh(const vector3& at, const vector3& up) 
{
	vector3 eye(M41, M42, M43);
	vector3 zaxis = at - eye;
	zaxis.norm();
	vector3 xaxis = up * zaxis;
	xaxis.norm();
	vector3 yaxis = zaxis * xaxis;
	M11 = xaxis.x;  M12 = yaxis.x;  M13 = zaxis.x;  M14 = 0.0f;
	M21 = xaxis.y;  M22 = yaxis.y;  M23 = zaxis.y;  M24 = 0.0f;
	M31 = xaxis.z;  M32 = yaxis.z;  M33 = zaxis.z;  M34 = 0.0f;
}


/**
	Creates left-handed field-of-view perspective projection matrix.
*/
inline
void
matrix44::perspFovLh(float fovY, float aspect, float zn, float zf)
{
	float h = float(1.0 / tan(fovY * 0.5f));
	float w = h / aspect;
	M11 = w;    M12 = 0.0f; M13 = 0.0f;                   M14 = 0.0f;
	M21 = 0.0f; M22 = h;    M23 = 0.0f;                   M24 = 0.0f;
	M31 = 0.0f; M32 = 0.0f; M33 = zf / (zf - zn);         M34 = 1.0f;
	M41 = 0.0f; M42 = 0.0f; M43 = -zn * (zf / (zf - zn)); M44 = 0.0f;
}


/**
	Creates right-handed field-of-view perspective projection matrix.
*/
inline
void
matrix44::perspFovRh(float fovY, float aspect, float zn, float zf)
{
	float h = float(1.0 / tan(fovY * 0.5f));
	float w = h / aspect;
	M11 = w;    M12 = 0.0f; M13 = 0.0f;                  M14 = 0.0f;
	M21 = 0.0f; M22 = h;    M23 = 0.0f;                  M24 = 0.0f;
	M31 = 0.0f; M32 = 0.0f; M33 = zf / (zn - zf);        M34 = -1.0f;
	M41 = 0.0f; M42 = 0.0f; M43 = zn * (zf / (zn - zf)); M44 = 0.0f;
}


/**
	Creates off-center left-handed perspective projection matrix.
*/
inline
void
matrix44::perspOffCenterLh(float minX, float maxX, float minY, float maxY, float zn, float zf)
{
	M11 = 2.0f * zn / (maxX - minX); M12 = 0.0f, M13 = 0.0f; M14 = 0.0f;
	M21 = 0.0f; M22 = 2.0f * zn / (maxY - minY); M23 = 0.0f; M24 = 0.0f;
	M31 = (minX + maxX) / (minX - maxX); M32 = (maxY + minY) / (minY - maxY); M33 = zf / (zf - zn); M34 = 1.0f;
	M41 = 0.0f; M42 = 0.0f; M43 = zn * zf / (zn - zf); M44 = 0.0f;
}


/**
	Creates off-center right-handed perspective projection matrix.
*/
inline
void
matrix44::perspOffCenterRh(float minX, float maxX, float minY, float maxY, float zn, float zf)
{
	M11 = 2.0f * zn / (maxX - minX); M12 = 0.0f, M13 = 0.0f; M14 = 0.0f;
	M21 = 0.0f; M22 = 2.0f * zn / (maxY - minY); M23 = 0.0f; M24 = 0.0f;
	M31 = (minX + maxX) / (maxX - minX); M32 = (maxY + minY) / (maxY - minY); M33 = zf / (zn - zf); M34 = -1.0f;
	M41 = 0.0f; M42 = 0.0f; M43 = zn * zf / (zn - zf); M44 = 0.0f;
}


/**
	Creates left-handed orthogonal projection matrix.
*/
inline
void
matrix44::orthoLh(float w, float h, float zn, float zf)
{
	M11 = 2.0f / w; M12 = 0.0f;     M13 = 0.0f;             M14 = 0.0f;
	M21 = 0.0f;     M22 = 2.0f / h; M23 = 0.0f;             M24 = 0.0f;
	M31 = 0.0f;     M32 = 0.0f;     M33 = 1.0f / (zf - zn); M34 = 0.0f;
	M41 = 0.0f;     M42 = 0.0f;     M43 = zn / (zn - zf);   M44 = 1.0f;
}


/**
	Creates right-handed orthogonal projection matrix.
*/
inline
void
matrix44::orthoRh(float w, float h, float zn, float zf)
{
	M11 = 2.0f / w; M12 = 0.0f;     M13 = 0.0f;             M14 = 0.0f;
	M21 = 0.0f;     M22 = 2.0f / h; M23 = 0.0f;             M24 = 0.0f;
	M31 = 0.0f;     M32 = 0.0f;     M33 = 1.0f / (zn - zf); M34 = 0.0f;
	M41 = 0.0f;     M42 = 0.0f;     M43 = zn / (zn - zf);   M44 = 1.0f;
}


/**
	Restricted lookat.
*/
inline
void 
matrix44::billboard(const vector3& to, const vector3& up)
{
	vector3 from(M41, M42, M43);
	vector3 z(from - to);
	z.norm();
	vector3 y(up);
	y.norm();
	vector3 x(y * z);
	z = x * y;       

	M11=x.x;  M12=x.y;  M13=x.z;  M14=0.0f;
	M21=y.x;  M22=y.y;  M23=y.z;  M24=0.0f;
	M31=z.x;  M32=z.y;  M33=z.z;  M34=0.0f;
}


/**
	Inplace matrix mulitply.
*/
inline
void
matrix44::operator *= (const matrix44& m1) 
{
	int i;
	for (i=0; i<4; i++) 
	{
		float mi0 = m[i][0];
		float mi1 = m[i][1];
		float mi2 = m[i][2];
		float mi3 = m[i][3];
		m[i][0] = mi0*m1.m[0][0] + mi1*m1.m[1][0] + mi2*m1.m[2][0] + mi3*m1.m[3][0];
		m[i][1] = mi0*m1.m[0][1] + mi1*m1.m[1][1] + mi2*m1.m[2][1] + mi3*m1.m[3][1];
		m[i][2] = mi0*m1.m[0][2] + mi1*m1.m[1][2] + mi2*m1.m[2][2] + mi3*m1.m[3][2];
		m[i][3] = mi0*m1.m[0][3] + mi1*m1.m[1][3] + mi2*m1.m[2][3] + mi3*m1.m[3][3];
	}
}


/**
	Rotates around any axis.
*/
inline
void 
matrix44::rotate(const vector3& vec, float a)
{
	vector3 v(vec);
	v.norm();
	float sa = (float) sin(a);
	float ca = (float) cos(a);

	matrix44 rotM;
	rotM.M11 = ca + (1.0f - ca) * v.x * v.x;
	rotM.M12 = (1.0f - ca) * v.x * v.y - sa * v.z;
	rotM.M13 = (1.0f - ca) * v.z * v.x + sa * v.y;
	rotM.M21 = (1.0f - ca) * v.x * v.y + sa * v.z;
	rotM.M22 = ca + (1.0f - ca) * v.y * v.y;
	rotM.M23 = (1.0f - ca) * v.y * v.z - sa * v.x;
	rotM.M31 = (1.0f - ca) * v.z * v.x - sa * v.y;
	rotM.M32 = (1.0f - ca) * v.y * v.z + sa * v.x;
	rotM.M33 = ca + (1.0f - ca) * v.z * v.z;

	(*this) *= rotM;
}


/**
	Multiply source vector into target vector, eliminates tmp vector.
*/
inline
void
matrix44::mult(const vector4& src, vector4& dst) const
{
	dst.x = M11*src.x + M21*src.y + M31*src.z + M41*src.w;
	dst.y = M12*src.x + M22*src.y + M32*src.z + M42*src.w;
	dst.z = M13*src.x + M23*src.y + M33*src.z + M43*src.w;
	dst.w = M14*src.x + M24*src.y + M34*src.z + M44*src.w;
}


/**
	Multiply source vector into target vector, eliminates tmp vector.
*/
inline
void
matrix44::mult(const vector3& src, vector3& dst) const
{
	dst.x = M11*src.x + M21*src.y + M31*src.z + M41;
	dst.y = M12*src.x + M22*src.y + M32*src.z + M42;
	dst.z = M13*src.x + M23*src.y + M33*src.z + M43;
}


/**
	Fast multiply-add with weighting.

    Perform a multiply-add with weighting (this is quite specialized for
    CPU-skinning)
*/
inline
void
matrix44::weightedmadd(const vector3& src, vector3& dst, float weight) const
{
	dst.x += (M11*src.x + M21*src.y + M31*src.z + M41) * weight;
	dst.y += (M12*src.x + M22*src.y + M32*src.z + M42) * weight;
	dst.z += (M13*src.x + M23*src.y + M33*src.z + M43) * weight;
}


/**
*/
static 
inline 
matrix44 operator * (const matrix44& m0, const matrix44& m1) 
{
	return matrix44 (
		m0.m[0][0]*m1.m[0][0] + m0.m[0][1]*m1.m[1][0] + m0.m[0][2]*m1.m[2][0] + m0.m[0][3]*m1.m[3][0],
		m0.m[0][0]*m1.m[0][1] + m0.m[0][1]*m1.m[1][1] + m0.m[0][2]*m1.m[2][1] + m0.m[0][3]*m1.m[3][1],
		m0.m[0][0]*m1.m[0][2] + m0.m[0][1]*m1.m[1][2] + m0.m[0][2]*m1.m[2][2] + m0.m[0][3]*m1.m[3][2],
		m0.m[0][0]*m1.m[0][3] + m0.m[0][1]*m1.m[1][3] + m0.m[0][2]*m1.m[2][3] + m0.m[0][3]*m1.m[3][3],

		m0.m[1][0]*m1.m[0][0] + m0.m[1][1]*m1.m[1][0] + m0.m[1][2]*m1.m[2][0] + m0.m[1][3]*m1.m[3][0],
		m0.m[1][0]*m1.m[0][1] + m0.m[1][1]*m1.m[1][1] + m0.m[1][2]*m1.m[2][1] + m0.m[1][3]*m1.m[3][1],
		m0.m[1][0]*m1.m[0][2] + m0.m[1][1]*m1.m[1][2] + m0.m[1][2]*m1.m[2][2] + m0.m[1][3]*m1.m[3][2],
		m0.m[1][0]*m1.m[0][3] + m0.m[1][1]*m1.m[1][3] + m0.m[1][2]*m1.m[2][3] + m0.m[1][3]*m1.m[3][3],

		m0.m[2][0]*m1.m[0][0] + m0.m[2][1]*m1.m[1][0] + m0.m[2][2]*m1.m[2][0] + m0.m[2][3]*m1.m[3][0],
		m0.m[2][0]*m1.m[0][1] + m0.m[2][1]*m1.m[1][1] + m0.m[2][2]*m1.m[2][1] + m0.m[2][3]*m1.m[3][1],
		m0.m[2][0]*m1.m[0][2] + m0.m[2][1]*m1.m[1][2] + m0.m[2][2]*m1.m[2][2] + m0.m[2][3]*m1.m[3][2],
		m0.m[2][0]*m1.m[0][3] + m0.m[2][1]*m1.m[1][3] + m0.m[2][2]*m1.m[2][3] + m0.m[2][3]*m1.m[3][3],

		m0.m[3][0]*m1.m[0][0] + m0.m[3][1]*m1.m[1][0] + m0.m[3][2]*m1.m[2][0] + m0.m[3][3]*m1.m[3][0],
		m0.m[3][0]*m1.m[0][1] + m0.m[3][1]*m1.m[1][1] + m0.m[3][2]*m1.m[2][1] + m0.m[3][3]*m1.m[3][1],
		m0.m[3][0]*m1.m[0][2] + m0.m[3][1]*m1.m[1][2] + m0.m[3][2]*m1.m[2][2] + m0.m[3][3]*m1.m[3][2],
		m0.m[3][0]*m1.m[0][3] + m0.m[3][1]*m1.m[1][3] + m0.m[3][2]*m1.m[2][3] + m0.m[3][3]*m1.m[3][3]
	);
}


/**
*/
static 
inline 
vector3 operator * (const matrix44& m, const vector3& v)
{
	return vector3(
		m.M11*v.x + m.M21*v.y + m.M31*v.z + m.M41,
		m.M12*v.x + m.M22*v.y + m.M32*v.z + m.M42,
		m.M13*v.x + m.M23*v.y + m.M33*v.z + m.M43);
}


/**
*/
static 
inline 
vector4 operator * (const matrix44& m, const vector4& v)
{
	return vector4(
		m.M11*v.x + m.M21*v.y + m.M31*v.z + m.M41*v.w,
		m.M12*v.x + m.M22*v.y + m.M32*v.z + m.M42*v.w,
		m.M13*v.x + m.M23*v.y + m.M33*v.z + m.M43*v.w,
		m.M14*v.x + m.M24*v.y + m.M34*v.z + m.M44*v.w);
};


/**
	Multiply and divide by w.
*/
inline
vector3
matrix44::mult_divw(const vector3& v) const
{
	vector4 v4(v.x, v.y, v.z, 1.0f);
	v4 = *this * v4;
	return vector3(v4.x / v4.w, v4.y / v4.w, v4.z / v4.w);
}


#endif // _matrix44_h__

// vim:ts=4:sw=4:
