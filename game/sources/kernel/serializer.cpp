//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file serializer.cpp
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/serializer.h"
#include "kernel/serializeserver.h"
#include "kernel/kernelserver.h"
#include "kernel/fileserver.h"
#include "kernel/logserver.h"

PrepareClass(Serializer, "Root", s_NewSerializer, s_InitSerializer);


//=========================================================================
// Serializer
//=========================================================================

/**
	Constructor.
*/
Serializer::Serializer(const char *id) :
	Root(id),
	serialize_buffer(NULL),
	deserialize_buffer(NULL),
	state(SS_NONE),
	act_goups_count(0),
	serialize_size(0),
	deserialize_size(0)
{
	Assert(SerializeServer::GetInstance());
}


/**
	Destructor.
*/
Serializer::~Serializer()
{
	// empty
}


//=========================================================================
// Serializing
//=========================================================================

bool Serializer::BeginSerialize(const char *root_name)
{
	Assert(this->state == SS_NONE);

	// lock whole process
	this->Lock();

	// prepare serializing
	this->serialize_size = 0;
	this->state = SS_SERIALIZING;

	return true;
}


long Serializer::EndSerialize(byte_t *&buffer)
{
	Assert(this->state == SS_SERIALIZING);

	// set result
	buffer = this->serialize_buffer;
	long result = this->serialize_size;

	this->serialize_buffer = NULL;
	this->serialize_size = 0;

	// finish serializing
	this->state = SS_NONE;
	this->Unlock();

	return result;
}


bool Serializer::AddGroup(const char *group_name)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


//=========================================================================
// SetAttribute
//=========================================================================

bool Serializer::SetAttribute(const char *attr_name, const char *value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}

bool Serializer::SetAttribute(const char *attr_name, const string &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, byte_t value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, ushort_t value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, uint_t value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, short value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, int value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, long value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, float value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, double value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, bool value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, const vector2 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, const vector3 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttribute(const char *attr_name, const vector4 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttributePercent(const char *attr_name, float value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttributeColor(const char *attr_name, const vector3 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetAttributeColor(const char *attr_name, const vector4 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


//=========================================================================
// SetValue
//=========================================================================

bool Serializer::SetValue(const char *value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(const string &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(byte_t value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(ushort_t value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(uint_t value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(short value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(int value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(long value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(float value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(double value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(bool value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(const vector2 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(const vector3 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::SetValue(const vector4 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


//=========================================================================
// Deserializing
//=========================================================================

bool Serializer::BeginDeserialize(const byte_t *buffer, long buff_size)
{
	Assert(this->state == SS_NONE);
	Assert(buffer);
	Assert(buff_size > 0);

	// lock whole process
	this->Lock();

	// prepare deserializing
	this->deserialize_buffer = buffer;
	this->deserialize_size = buff_size;
	this->state = SS_DESERIALIZING;

	return true;
}


void Serializer::EndDeserialize()
{
	Assert(this->state == SS_DESERIALIZING);

	// finish deserializing
	this->state = SS_NONE;
	this->Unlock();
}


bool Serializer::GetGroup(const char *group_name)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetNextGroup(bool mixed)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


//=========================================================================
// GetAttribute
//=========================================================================

bool Serializer::GetAttribute(const char *attr_name, string &value, bool empty)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, byte_t &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, ushort_t &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, uint_t &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, short &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, int &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, long &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, float &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, double &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, bool &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, vector2 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, vector3 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttribute(const char *attr_name, vector4 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttributePercent(const char *attr_name, float &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttributeColor(const char *attr_name, vector3 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetAttributeColor(const char *attr_name, vector4 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


//=========================================================================
// GetValue
//=========================================================================

bool Serializer::GetValue(string &value, bool empty)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(byte_t &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(ushort_t &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(uint_t &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(short &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(int &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(long &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(float &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(double &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(bool &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(vector2 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(vector3 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool Serializer::GetValue(vector4 &value)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


//=========================================================================
// Universal methods
//=========================================================================

bool Serializer::CheckGroupName(const char *name)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


ushort_t Serializer::GetGroupsCount()
{
	ErrorMsg("Overwrite this method in subclass");
	return 0;
}


void Serializer::EndGroup()
{
	ErrorMsg("Overwrite this method in subclass");
}


// vim:ts=4:sw=4:
