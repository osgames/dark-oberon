#ifndef __serializeserver_h__
#define __serializeserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file serializeserver.h
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/ref.h"
#include "kernel/rootserver.h"
#include "kernel/serializer.h"


//=========================================================================
// SerializeServer
//=========================================================================

/**
	@class SerializeServer
	@ingroup Kernel_Module

	Stores all serializers in one place. Serializers has to be created here.
*/

class SerializeServer : public RootServer<SerializeServer>
{
//--- varialbles
private:
	string default_format;                     ///< Name of default format.
	string default_serializer;                 ///< Class name of default serializer used when format is not specified.
	hash_map<string, string> serializers_map;  ///< Hash map of all registered serializers.

//--- methods
public:
	SerializeServer(const char *id);
	virtual ~SerializeServer();

	// settings
	bool SetDefaultFormat(const string &format);
	const string &GetDefaultFormat();

	// serializers
	bool RegisterSerializer(const string &class_name, const string &format);

	// serializing
	bool Serialize(Serialized *obj, const string &file_path);
	Serialized *Deserialize(const string &file_path, Serialized *obj = NULL);

private:
	long Serialize(Serialized *obj, byte_t *&buffer, const char *format = NULL);
	Serialized *Deserialize(const byte_t *buffer, long buff_size, Serialized *obj = NULL, const char *format = NULL);

	Serializer *NewSerializer(const char *format = NULL);
	bool IsSerializer(const char *format = NULL);
};


//=========================================================================
// Methods
//=========================================================================

/**
	Returns defult format.
	@return Default format.
*/
inline
const string &SerializeServer::GetDefaultFormat()
{
	this->default_format;
}


#endif // __serializeserver_h__

// vim:ts=4:sw=4:
