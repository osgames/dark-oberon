#ifndef __pool_h__
#define __pool_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file pool.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/list.h"
#include "kernel/condition.h"
#include "kernel/threadserver.h"


//=========================================================================
// Pool
//=========================================================================

/**
	@class Pool
	@ingroup Kernel_Module

	@brief Pool.

	Node have to be only PoolNode derived class!
*/

template <class Node>
class Pool : protected List<Node> {
//--- variables
private:
	uint_t increment;
	uint_t max_count;
	uint_t created_count;
	uint_t pooled_count;

	Mutex *mutex;
	Condition *empty_cond;

//--- methods
public:
	Pool(uint_t init_count = 1, uint_t increment = 1, uint_t max_count = 0);
	virtual ~Pool();

	void Put(Node *prev);
	Node *Get();

	uint_t GetPooledCount();
	uint_t GetCreatedCount();

private:
	void AllocateNodes(uint_t count);
};


//=========================================================================
// Methods
//=========================================================================

template <class Node>
Pool<Node>::Pool(uint_t init_count, uint_t increment, uint_t max_count) :
	List<Node>(),
	created_count(0),
	pooled_count(0)
{
	Assert(increment > 0);

	this->increment = increment;
	this->max_count = max_count;

	// create synchronisation primitives
	this->mutex = ThreadServer::GetInstance()->CreateMutex();
	this->empty_cond = ThreadServer::GetInstance()->CreateCondition();
	Assert(this->mutex && this->empty_cond);

	AllocateNodes(init_count);
}


template <class Node>
Pool<Node>::~Pool()
{
	// test if all nodes created in this pool are returned back
	Assert(this->pooled_count == this->created_count);

	Node *node;
	while (node = this->PopFront())
		delete node;

	// destroy synchronisation primitives
	delete this->empty_cond;
	delete this->mutex;
}


template <class Node>
void Pool<Node>::Put(Node *node)
{
	this->mutex->Lock();

	Assert(this->pooled_count < this->created_count);
	PushBack(node);
	this->pooled_count++;

	this->empty_cond->SendSignal();

	this->mutex->Unlock();
}


template <class Node>
Node *Pool<Node>::Get()
{
	this->mutex->Lock();

	// create new nodes if missing
	if (!this->pooled_count)
		this->AllocateNodes(this->increment);

	// wait while we have some node
	while (!this->pooled_count) {
		empty_cond->Wait(this->mutex);
	}

	// remove node from list
	Node *node = PopFront();
	this->pooled_count--;

	this->mutex->Unlock();

	return node;
}


template <class Node>
void Pool<Node>::AllocateNodes(uint_t count)
{
	Assert(count > 0);

	Node *node;
	if (this->max_count && this->created_count + count > this->max_count)
		count = this->max_count - this->created_count;

	if (!count)
		return;

	for(uint_t i = 0; i < count; i++) {
		node = NEW Node();
		this->PushBack(node);
	}

	this->created_count += count;
	this->pooled_count += count;
}


template <class Node>
uint_t Pool<Node>::GetPooledCount()
{
	return this->pooled_count;
}


template <class Node>
uint_t Pool<Node>::GetCreatedCount()
{
	return this->created_count;
}


#endif  // __pool_h__

// vim:ts=4:sw=4:
