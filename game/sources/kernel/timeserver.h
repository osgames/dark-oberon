#ifndef __timeserver_h__
#define __timeserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file timeserver.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - System variables.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/rootserver.h"
#include "kernel/profiler.h"
#include "kernel/env.h"
#include "kernel/ref.h"


//=========================================================================
// TimeServer
//=========================================================================

/**
	@class TimeServer
	@ingroup Kernel_Module

	@brief Time server interface.
*/

class TimeServer : public RootServer<TimeServer>
{
//--- variables
private:
	bool started;
	double start_time;
	double time;
	double frame_time;
	double min_frame_time;
	double fix_frame_time;
	bool wait_frame_time;
	ulong_t frame;

	// fps
	float    fps;
	double   fps_last_time;
	ulong_t  fps_last_frame;
	double   fps_interval;
	bool     fps_updated;
	bool     fps_show;

	// profiling
	Root *profilers;
	Ref<Env> fps_variable;

#ifdef USE_PROFILERS
	Ref<Env> profiler_variable;
#endif


//--- methods
public:
	TimeServer(const char *id);
	virtual ~TimeServer();

	virtual bool Trigger();

	void StartTime();
	void StopTime();

	virtual double GetRealTime();

	double GetTime();
	double GetFrameTime();
	double GetMinFrameTime();
	double GetFixFrameTime();
	ulong_t GetFrame();

	void SetTime(double time);
	void SetMinFrameTime(double min_time, bool wait = true);
	void SetFixFrameTime(double fix_time, bool wait = false);

	// fps
	float GetFPS();
	bool IsFPSRefreshed();
	void ShowFPS(bool show);
	void SetFPSInterval(double interval);

	// profiling
#ifdef USE_PROFILERS
	Profiler *NewProfiler(const string &name);
	Profiler *GetProfiler(const string &name);
#endif
};


//=========================================================================
// Methods
//=========================================================================

inline
void TimeServer::StartTime()
{
	Assert(!this->started);

	this->started = true;
	this->start_time = this->GetRealTime();
	this->time = 0;
	this->frame_time = 0;

	this->frame = 0;
	this->fps = 0;
	this->fps_last_frame = 0;
	this->fps_last_time = 0;
}


inline
void TimeServer::StopTime()
{
	Assert(this->started);
	this->started = false;
}


inline
double TimeServer::GetRealTime()
{
	ErrorMsg("Implemented in descendant");
	return 0;
}


inline
double TimeServer::GetTime()
{
	return this->time;
}


inline
double TimeServer::GetFrameTime()
{
	return this->frame_time;
}


inline
double TimeServer::GetMinFrameTime()
{
	return this->min_frame_time;
}


inline
double TimeServer::GetFixFrameTime()
{
	return this->fix_frame_time;
}


inline
ulong_t TimeServer::GetFrame()
{
	return this->frame;
}


inline
void TimeServer::SetTime(double time)
{
	Assert(time >= 0);

	double delta = time - this->time;

	this->start_time -= delta;
	this->frame_time += delta;
	this->time = time;

	Assert(this->frame_time >= 0);
}


inline
void TimeServer::SetMinFrameTime(double min_time, bool wait)
{
	Assert(min_time >= 0);

	this->min_frame_time = min_time;
	this->fix_frame_time = 0;
	this->wait_frame_time = wait;
}


inline
void TimeServer::SetFixFrameTime(double fix_time, bool wait)
{
	Assert(fix_time >= 0);

	this->min_frame_time = 0;
	this->fix_frame_time = fix_time;
	this->wait_frame_time = wait;
}


inline
float TimeServer::GetFPS()
{
	return this->fps;
}


inline
bool TimeServer::IsFPSRefreshed()
{
	bool result = this->fps_updated;
	this->fps_updated = false;
	return result;
}


inline
void TimeServer::SetFPSInterval(double interval)
{
	this->fps_interval = interval;
}

#ifdef USE_PROFILERS
inline
Profiler *TimeServer::GetProfiler(const string &name)
{
	return (Profiler *)this->profilers->Find(name);
}
#endif

#endif  // __timeserver_h__

// vim:ts=4:sw=4:
