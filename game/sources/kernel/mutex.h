#ifndef __mutex_h__
#define __mutex_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file mutex.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Mutex
//=========================================================================

/**
	@class Mutex
	@ingroup Kernel_Module

	@brief Mutex.
*/

class Mutex
{
	friend class ThreadServer;

public:
	virtual ~Mutex() {};

	virtual void Lock() const {};
	virtual void Unlock() const {};

protected:
	Mutex() {};
};


//=========================================================================
// RecMutex
//=========================================================================

/**
	@class RecMutex
	@ingroup Kernel_Module

	@brief Recursive mutex.
*/

class RecMutex : public Mutex
{
	friend class ThreadServer;

public:
	virtual ~RecMutex() {};

	virtual void Lock() const {};
	virtual void Unlock() const {};

protected:
	RecMutex() : Mutex() {};
};


#endif  // __mutex_h__

// vim:ts=4:sw=4:
