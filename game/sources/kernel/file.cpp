//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file file.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added PrintF and Flush methods.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/file.h"
#include "kernel/fileserver.h"
#include "kernel/logserver.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>


//=========================================================================
// File
//=========================================================================

/**
	Constructor. It is private because only FileServer may create objects.
*/
File::File() :
    act_line(0),
    opened(false),
	binary(false),
	file(NULL)
{
    //
}


/**
    Given file stream will be set directly. Used in FileServer to wraps standart streams.

    @param file         Opened file stream.
    @param file_path    Name and full path of the stream.
	@param binary       Whether the stream is binary.
    @return             @c True if success.
*/
File::File(FILE *file, const char *file_path, bool binary)
{
	Assert(file);
	Assert(file_path);

	// set file properties
	this->act_line = 0;
	this->file = file;
	this->file_path = file_path;
	this->binary = binary;
	this->opened = true;
}


/**
	Destructor.
*/
File::~File()
{
    if (this->IsOpened())
        this->Close();
}


/**
    Opens the specified file.

    @param file_path    The full name of the file to open.
    @param access_mode  The access mode ("(r|w|a)[+]").
    @return             @c True if success.
*/
bool File::Open(const string &file_path, const char *access_mode)
{
    Assert(!this->IsOpened());
    Assert(!file_path.empty());
    Assert(access_mode);

	// get file server
	FileServer *file_server = FileServer::GetInstance();
	Assert(file_server);

	// mangle path
	string full_path;
	if (!file_server->ManglePath(file_path, full_path))
		return false;

	// create missing folders in write mode
	if (strchr(access_mode, 'w') != NULL)
	{
		string path;
		file_server->ExtractDirectory(file_path, path);

		if (!path.empty() && !file_server->DirectoryExists(path) && !file_server->CreatePath(path))
		{
			LogError1("Error creating path to file: %s", file_path.c_str());
			return false;
		}
	}

	// open file
    this->file = fopen(full_path.c_str(), access_mode);

	if (!this->file)
	{
		LogError2("Error opening file in '%s' mode: %s", access_mode, file_path.c_str());
        return false;
	}

	// set file properties
	this->act_line = 0;
	this->binary = (!strchr(access_mode, 't'));
	this->file_path = file_path;
	this->opened = true;

    return true;
}


/**
	Closes the file.
*/
void File::Close()
{
	Assert(this->IsOpened());

	if (this->file)
	{
		fclose(this->file);
		this->file = NULL;
	}

	this->opened = false;
}


/**
	Writes a number of bytes to the file.

	@param buffer      Buffer with data.
	@param size        Number of bytes to write.
	@return            Number of bytes written.
*/
size_t File::Write(const void *buffer, size_t size)
{
	Assert(this->IsOpened());
	if (!this->binary)
		LogWarning1("Function Write called on text file: %s", this->file_path.c_str());

	return fwrite(buffer, 1, size, this->file);
}


/**
	Reads a number of bytes from the file.

	@param buffer     Buffer for data.
	@param size       Number of bytes to read.
	@return           Number of bytes read.
*/
size_t File::Read(void *buffer, size_t size)
{
	Assert(this->IsOpened());
	if (!this->binary)
		LogWarning1("Function Read called on text file: %s", this->file_path.c_str());

	size_t read_size = fread(buffer, 1, size, this->file);

	if (ferror(this->file))
	{
		LogWarning1("Error reading from file '%s'", this->file_path.c_str());
		clearerr(this->file);
	}

	return read_size;
}


/**
	Reads whole file into memory buffer.

	This function does not depend on current position in the file. This position will not be changed.
	Buffer is creted using NEW operator so user has to delete it hisself.

	@return           Buffer with file data. If error occures or file is empty, @c NULL is returned.
*/
byte_t *File::ReadAll()
{
	Assert(this->IsOpened());
	if (!this->binary)
		LogWarning1("Function ReadAll called on text file: %s", this->file_path.c_str());

	long file_size = this->GetSize();
	if (!file_size)
		return NULL;

	byte_t *buffer = NEW byte_t[file_size];
	long position = this->Tell();

	this->Seek(0, ST_START);
	size_t read_size = this->Read(buffer, file_size);
	this->Seek(position, ST_START);

	if (!read_size)
		SafeDeleteArray(buffer);

	return buffer;
}


char *File::ReadAllToString()
{
	Assert(this->IsOpened());
	if (!this->binary)
		LogWarning1("Function ReadAllToString called on text file: %s", this->file_path.c_str());

	long file_size = this->GetSize();
	if (!file_size)
		return NULL;

	char *buffer = NEW char[file_size + 1];
	long position = this->Tell();

	this->Seek(0, ST_START);
	size_t read_size = this->Read(buffer, file_size);
	buffer[file_size] = 0;
	this->Seek(position, ST_START);

	if (!read_size)
		SafeDeleteArray(buffer);

	return buffer;
}


/**
	Writes a string to the file.

	@param line        The string to write.
	@return            Success.
*/
bool File::WriteLine(const string &line)
{
	Assert(this->IsOpened());
	if (this->binary)
		LogWarning1("Function WriteLine called on binary file: %s", this->file_path.c_str());

	return this->PrintF("%s\n", line.c_str());
}


/**
	Writes a formatted string to the file.

	@param format      Standart format string like in printf.
	@return            Success.
*/
bool File::PrintF(const char *format, ...)
{
	Assert(this->IsOpened());
	if (this->binary)
		LogWarning1("Function PrintF called on binary file: %s", this->file_path.c_str());

	va_list arg;
	va_start(arg, format);

	bool ok = (vfprintf(this->file, format, arg) > 0);

	va_end(arg);

	if (ok && format[strlen(format) - 1] == '\n')
		this->act_line++;

	return ok;
}


/**
	Reads one line from the file up to the first newline character
	or up to the end of the buffer.

	@param line           Buffer for line.
	@return               @c False if eof is reached.
*/
bool File::ReadLine(string &line)
{
	Assert(this->IsOpened());
	if (this->binary)
		LogWarning1("Function ReadLine called on binary file: %s", this->file_path.c_str());

	line = "";

	// eof is reached
	if (this->Eof())
		return false;

	// read line
	const int buff_size = 4096;
	char buffer[buff_size];
	buffer[0] = 0;
	if (!fgets(buffer, buff_size, this->file))
	{
		if (!feof(this->file))
			return false;
	}

	// remove \n
	size_t size = strlen(buffer);
	if (size > 0 && buffer[size - 1] == '\n')
		buffer[size - 1] = 0;

	line = buffer;

	this->act_line++;
	return true;
}


/**
	Gets current position of file pointer.

	@return          Position of pointer.
*/
long File::Tell() const
{
	Assert(this->IsOpened());
	if (!this->binary)
		LogWarning1("Function Tell called on text file: %s", this->file_path.c_str());

	return ftell(this->file);
}


/**
    Sets the file pointer to given absolute or relative position.

    @param offset        The offset.
    @param origin        Position from which to count.
    @return              Success.
*/
bool
File::Seek(long offset, SeekType origin)
{
    Assert(this->IsOpened());
	if (!this->binary)
		LogWarning1("Function Seek called on text file: %s", this->file_path.c_str());

    int whence = SEEK_SET;
    switch (origin)
    {
        case ST_CURRENT:
            whence = SEEK_CUR;
            break;
        case ST_START:
            whence = SEEK_SET;
            break;
        case ST_END:
            whence = SEEK_END;
            break;
    }

    return (fseek(this->file, offset, whence) == 0);
}


/**
	Is the file at the end.
*/
bool File::Eof() const
{
	Assert(this->IsOpened());

	return (!feof(this->file)) ? false : true;
}


/**
	Returns size of file in bytes.

	@return     Byte-size of file.
*/
long File::GetSize() const
{
	Assert(this->IsOpened());

	struct stat s;
	fstat(fileno(this->file), &s);

	return s.st_size;
}


// vim:ts=4:sw=4:
