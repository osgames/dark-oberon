#ifndef __logmacros_h__
#define __logmacros_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file logmacros.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Definitions
//=========================================================================

/** @brief Maximum size of the log message. */
#define LOG_MAX_MESSAGE_SIZE 1024

/** @def LOG_HEADER_INFO
	@brief Header of the log in macro Info().
/** @def LOG_HEADER_WARNING
	@brief Header of the log in macro Warning().
/** @def LOG_HEADER_ERROR
	@brief Header of the log in macro Error().
/** @def LOG_HEADER_CRITICAL
	@brief Header of the log in macro Critical().
/** @def LOG_HEADER_DEBUG
	@brief Header of the log in macro Debug().
*/

#define LOG_HEADER_INFO       "[Info]    "
#define LOG_HEADER_WARNING    "[Warning] "
#define LOG_HEADER_ERROR      "[Error]   "
#define LOG_HEADER_CRITICAL   "[Critical]"
#define LOG_HEADER_DEBUG      "[Debug]   "


//=========================================================================
// Macros
//=========================================================================

/** @def LogInfo
	@def LogInfo1
	@def LogInfo2
	@def LogInfo3
	@def LogInfo4
	@brief Log info message. */
/** @def LogWarning
	@def LogWarning1
	@def LogWarning2
	@def LogWarning3
	@def LogWarning4
	@brief Log warning message. */
/** @def LogError
	@def LogError1
	@def LogError2
	@def LogError3
	@def LogError4
	@brief Log error message. */
/** @def LogCritical
	@def LogCritical1
	@def LogCritical2
	@def LogCritical3
	@def LogCritical4
	@brief Log critical error message. */
/** @def LogDebug
	@def LogDebug1
	@def LogDebug2
	@def LogDebug3
	@def LogDebug4
	@brief Log debug message.
	@note If DEBUG is not defined, LogDebug() is defined empty. */

#define LogInfo(msg)                      do { LogServer::GetInstance()->WriteInfo(__FILE__, __LINE__, msg); } while (0)
#define LogInfo1(msg, p1)                 do { LogServer::GetInstance()->WriteInfo(__FILE__, __LINE__, msg, p1); } while (0)
#define LogInfo2(msg, p1, p2)             do { LogServer::GetInstance()->WriteInfo(__FILE__, __LINE__, msg, p1, p2); } while (0)
#define LogInfo3(msg, p1, p2, p3)         do { LogServer::GetInstance()->WriteInfo(__FILE__, __LINE__, msg, p1, p2, p3); } while (0)
#define LogInfo4(msg, p1, p2, p3, p4)     do { LogServer::GetInstance()->WriteInfo(__FILE__, __LINE__, msg, p1, p2, p3, p4); } while (0)


#define LogWarning(msg)                   do { LogServer::GetInstance()->WriteWarning(__FILE__, __LINE__, msg); } while (0)
#define LogWarning1(msg, p1)              do { LogServer::GetInstance()->WriteWarning(__FILE__, __LINE__, msg, p1); } while (0)
#define LogWarning2(msg, p1, p2)          do { LogServer::GetInstance()->WriteWarning(__FILE__, __LINE__, msg, p1, p2); } while (0)
#define LogWarning3(msg, p1, p2, p3)      do { LogServer::GetInstance()->WriteWarning(__FILE__, __LINE__, msg, p1, p2, p3); } while (0)
#define LogWarning4(msg, p1, p2, p3, p4)  do { LogServer::GetInstance()->WriteWarning(__FILE__, __LINE__, msg, p1, p2, p3, p4); } while (0)


#define LogError(msg)                     do { LogServer::GetInstance()->WriteError(__FILE__, __LINE__, msg); } while (0)
#define LogError1(msg, p1)                do { LogServer::GetInstance()->WriteError(__FILE__, __LINE__, msg, p1); } while (0)
#define LogError2(msg, p1, p2)            do { LogServer::GetInstance()->WriteError(__FILE__, __LINE__, msg, p1, p2); } while (0)
#define LogError3(msg, p1, p2, p3)        do { LogServer::GetInstance()->WriteError(__FILE__, __LINE__, msg, p1, p2, p3); } while (0)
#define LogError4(msg, p1, p2, p3, p4)    do { LogServer::GetInstance()->WriteError(__FILE__, __LINE__, msg, p1, p2, p3, p4); } while (0)

#define LogCritical(msg)                  do { LogServer::GetInstance()->WriteCritical(__FILE__, __LINE__, msg); } while (0)
#define LogCritical1(msg, p1)             do { LogServer::GetInstance()->WriteCritical(__FILE__, __LINE__, msg, p1); } while (0)
#define LogCritical2(msg, p1, p2)         do { LogServer::GetInstance()->WriteCritical(__FILE__, __LINE__, msg, p1, p2); } while (0)
#define LogCritical3(msg, p1, p2, p3)     do { LogServer::GetInstance()->WriteCritical(__FILE__, __LINE__, msg, p1, p2, p3); } while (0)
#define LogCritical4(msg, p1, p2, p3, p4) do { LogServer::GetInstance()->WriteCritical(__FILE__, __LINE__, msg, p1, p2, p3, p4); } while (0)

#ifdef DEBUG
#	define LogDebug(msg)                  do { LogServer::GetInstance()->WriteDebug(__FILE__, __LINE__, msg); } while (0)
#	define LogDebug1(msg, p1)             do { LogServer::GetInstance()->WriteDebug(__FILE__, __LINE__, msg, p1); } while (0)
#	define LogDebug2(msg, p1, p2)         do { LogServer::GetInstance()->WriteDebug(__FILE__, __LINE__, msg, p1, p2); } while (0)
#	define LogDebug3(msg, p1, p2, p3)     do { LogServer::GetInstance()->WriteDebug(__FILE__, __LINE__, msg, p1, p2, p3); } while (0)
#	define LogDebug4(msg, p1, p2, p3, p4) do { LogServer::GetInstance()->WriteDebug(__FILE__, __LINE__, msg, p1, p2, p3, p4); } while (0)
#else
#	define LogDebug(msg)
#	define LogDebug1(msg, p1)
#	define LogDebug2(msg, p1, p2)
#	define LogDebug3(msg, p1, p2, p3)
#	define LogDebug4(msg, p1, p2, p3, p4)
#endif


#endif  // __logmacros_h__

// vim:ts=4:sw=4:
