//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file fileserver_cmds.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/fileserver.h"


//=========================================================================
// Commands
//=========================================================================

/**
	Defines an assign with the specified name and links it to the specified
	path.
*/
static void s_SetAssign(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	const char* s0 = cmd->In()->GetS();
	const char* s1 = cmd->In()->GetS();
	self->SetAssign(s0, s1);
}


/**
	Gets a path associated with an assign.
*/
static void s_GetAssign(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	cmd->Out()->SetS(self->GetAssign(cmd->In()->GetS()));
}


static void s_ResetAssigns(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	self->ResetAssigns();
}


static void s_CleanupPath(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;

	string path = cmd->In()->GetS();
	self->CleanupPath(path);
	cmd->Out()->SetS(path.c_str());
}


/**
	Converts a path with assigns into a native absolute path.
*/
static void s_ManglePath(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;

	string path = cmd->In()->GetS();
	if (self->ManglePath(path, path))
		cmd->Out()->SetS(path.c_str());
	else
		cmd->Out()->SetS("");
}


static void s_ExtractDirectory(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;

	string path = cmd->In()->GetS();
	self->ExtractDirectory(path, path);

	cmd->Out()->SetS(path.c_str());
}


static void s_ExtractFile(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;

	string path = cmd->In()->GetS();
	self->ExtractFile(path, path);

	cmd->Out()->SetS(path.c_str());
}


/**
	Returns true if file exists.
*/
static void s_FileExists(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	cmd->Out()->SetB(self->FileExists(cmd->In()->GetS()));
}


/**
	Returns true if directory exists.
*/
static void s_DirectoryExists(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	cmd->Out()->SetB(self->DirectoryExists(cmd->In()->GetS()));
}


static void s_CopyFile(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	string from = cmd->In()->GetS();
	string to = cmd->In()->GetS();
	cmd->Out()->SetB(self->CopyFileEx(from, to));
}


static void s_CreatePath(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	cmd->Out()->SetB(self->CreatePath(cmd->In()->GetS()));
}


static void s_DeleteFile(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	cmd->Out()->SetB(self->DeleteFileEx(cmd->In()->GetS()));
}


static void s_DeleteDirectory(void* slf, Cmd* cmd)
{
	FileServer *self = (FileServer*) slf;
	cmd->Out()->SetB(self->DeleteDirectory(cmd->In()->GetS()));
}


void s_InitFileserver_cmds(Class* cl)
{
	cl->BeginCmds();
	cl->AddCmd("v_SetAssign_ss",        'SASS', s_SetAssign);
	cl->AddCmd("s_GetAssign_s",         'GASS', s_GetAssign);
	cl->AddCmd("v_ResetAssign_v",       'RASS', s_ResetAssigns);

	cl->AddCmd("s_CleanupPath_s",       'CLUP', s_CleanupPath);
	cl->AddCmd("s_ManglePath_s",        'MNGP', s_ManglePath);
	cl->AddCmd("s_ExtractDirectory_s",  'EXDR', s_ExtractDirectory);
	cl->AddCmd("s_ExtractFile_s",       'EXFL', s_ExtractFile);

	cl->AddCmd("b_FileExists_s",        'FLEX', s_FileExists);
	cl->AddCmd("b_DirectoryExists_s",   'DREX', s_DirectoryExists);
	cl->AddCmd("b_CopyFile_ss",         'CPFL', s_CopyFile);
	cl->AddCmd("b_CreatePath_s",        'CREP', s_CreatePath);
	cl->AddCmd("b_DeleteFile_s",        'DLFL', s_DeleteFile);
	cl->AddCmd("b_DeleteDirectory_s",   'DLDR', s_DeleteDirectory);
	cl->EndCmds();
}

// vim:ts=4:sw=4:
