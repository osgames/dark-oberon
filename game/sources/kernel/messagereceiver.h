#ifndef __messagereceiver_h__
#define __messagereceiver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file messagereceiver.h
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"

//=========================================================================
// MessageReceiver
//=========================================================================

/**
	@class MessageReceiver
	@ingroup Kernel_Module

	Class is able to receive messages of given type.
*/

template <class Type>
class MessageReceiver
{
//--- methods
public:
	virtual void OnMessage(Type *message) {}
};


#endif // __messagereceiver_h__

// vim:ts=4:sw=4:
