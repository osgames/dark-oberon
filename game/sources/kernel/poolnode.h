#ifndef __poolnode_h__
#define __poolnode_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file poolnode.h
	@ingroup Kernel_Module

 	@author Peter Knut, RadonLabs GmbH
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Forward declarations
//=========================================================================

//template <class Node> class List;


//=========================================================================
// PoolNode
//=========================================================================

/**
	@class PoolNode
	@ingroup Kernel_Module

	@brief Implements a node used with Pool template.

	Node have to be only PoolNode derived class!
*/

template <class Node>
class PoolNode : protected ListNode<Node> {
	friend class List<Node>;
	friend class ListNode<Node>;

//--- methods
public:
	PoolNode();
	virtual ~PoolNode();

	bool IsPooled() const;
};


//=========================================================================
// Methods
//=========================================================================

template <class Node>
PoolNode<Node>::PoolNode() :
	ListNode<Node>()
{
}


template <class Node>
PoolNode<Node>::~PoolNode()
{
	// assert if still in list
}


template <class Node>
bool PoolNode<Node>::IsPooled(void) const
{
	return this->IsLinked();
};


#endif  // __poolnode_h__

// vim:ts=4:sw=4:
