#ifndef __vector2_h__
#define __vector2_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file vector2.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005

	Implement 2, 3 and 4-dimensional vector classes.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/mathlib.h"
#include <float.h>


//=========================================================================
// vector2
//=========================================================================

/**
	@class vector2
	@ingroup NebulaMathDataTypes

	Generic vector2 class.
*/

class vector2
{
public:
	vector2();
	vector2(const float _x, const float _y);
	vector2(const vector2& vec);
	vector2(const float* p);

	void set(const float _x, const float _y);
	void set(const vector2& vec);
	void set(const float* p);

	float len() const;
	void norm();

	void operator+=(const vector2& v0);
	void operator-=(const vector2& v0);
	void operator*=(const float s);
	void operator/=(const float s);

	bool isequal(const vector2& v, const float tol = 0.0f) const;
	int compare(const vector2& v, float tol = 0.0f) const;
	void rotate(float angle);
	void lerp(const vector2& v0, float lerpVal);
	void lerp(const vector2& v0, const vector2& v1, float lerpVal);

	float x, y;
};


/**
*/
inline
vector2::vector2() :
	x(0.0f),
	y(0.0f)
{
	// empty
}


/**
*/
inline
vector2::vector2(const float _x, const float _y) :
	x(_x),
	y(_y)
{
	// empty
}


/**
*/
inline
vector2::vector2(const vector2& vec) :
	x(vec.x),
	y(vec.y)
{
	// empty
}


/**
*/
inline
vector2::vector2(const float* p) :
	x(p[0]),
	y(p[1])
{
	// empty
}


/**
*/
inline
void vector2::set(const float _x, const float _y)
{
	x = _x;
	y = _y;
}


/**
*/
inline
void vector2::set(const vector2& v)
{
	x = v.x;
	y = v.y;
}


/**
*/
inline
void vector2::set(const float* p)
{
	x = p[0];
	y = p[1];
}


/**
*/
inline
float vector2::len() const
{
	return (float) sqrt(x * x + y * y);
}


/**
*/
inline
void vector2::norm()
{
	float l = len();
	if (l > TINY) {
		x /= l;
		y /= l;
	}
}


/**
*/
inline
void vector2::operator +=(const vector2& v0)
{
	x += v0.x;
	y += v0.y;
}


/**
*/
inline
void vector2::operator -=(const vector2& v0)
{
	x -= v0.x;
	y -= v0.y;
}


/**
*/
inline
void vector2::operator *=(const float s)
{
	x *= s;
	y *= s;
}


/**
*/
inline
void vector2::operator /=(const float s)
{
	x /= s;
	y /= s;
}


/**
*/
inline
bool vector2::isequal(const vector2& v, const float tol) const
{
	if (fabs(v.x - x) > tol)      return false;
	else if (fabs(v.y - y) > tol) return false;
	return true;
}


/**
*/
inline
int vector2::compare(const vector2& v, float tol) const
{
	if (fabs(v.x - x) > tol)      return (v.x > x) ? +1 : -1;
	else if (fabs(v.y - y) > tol) return (v.y > y) ? +1 : -1;
	else                          return 0;
}


/**
*/
inline
void vector2::rotate(float angle)
{
	// rotates this one around P(0,0).
	float sa, ca;

	sa = (float) sin(angle);
	ca = (float) cos(angle);

	// "handmade" multiplication
	vector2 help(ca * this->x - sa * this->y,
				sa * this->x + ca * this->y);

	*this = help;
}


/**
*/
static inline
vector2 operator +(const vector2& v0, const vector2& v1)
{
	return vector2(v0.x + v1.x, v0.y + v1.y);
}


/**
*/
static
inline vector2 operator -(const vector2& v0, const vector2& v1)
{
	return vector2(v0.x - v1.x, v0.y - v1.y);
}


/**
*/
static
inline vector2 operator *(const vector2& v0, const float s)
{
	return vector2(v0.x * s, v0.y * s);
}


/**
*/
static inline
vector2 operator -(const vector2& v)
{
	return vector2(-v.x, -v.y);
}


/**
*/
inline
void vector2::lerp(const vector2& v0, float lerpVal)
{
	x = v0.x + ((x - v0.x) * lerpVal);
	y = v0.y + ((y - v0.y) * lerpVal);
}


/**
*/
inline
void vector2::lerp(const vector2& v0, const vector2& v1, float lerpVal)
{
	x = v0.x + ((v1.x - v0.x) * lerpVal);
	y = v0.y + ((v1.y - v0.y) * lerpVal);
}


#endif // __vector2_h__

// vim:ts=4:sw=4:
