#ifndef __root_h__
#define __root_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file root.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Serializing of object.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/object.h"
#include "kernel/referenced.h"
#include "kernel/treenode.h"

class BaseMessage;


//=========================================================================
// Root
//=========================================================================

/**
	@class Root
	@ingroup Kernel_Module

	@brief Defines the basic functionality and interface for object hierarchy.
*/

class Root : public Object, public TreeNode<Root>, public Referenced
{
//--- methods
public:
	Root(const char *id);

	void SetID(const string &id);
	const string &GetID();

	string GetFullPath();
	Root *Find(const string &path);
	void Sort(bool backward = false);
	int GetTreeSize() const;
	void LogTree(const string &indent) const;

	void Lock() const;
	void Unlock() const;

	bool ReleaseChildren();
	void ClearChildren();

protected:
	virtual ~Root();

	void SendMessage(BaseMessage *message, Root *destination, double delay = 0.0);

	// serializing
	virtual bool Serialize(Serializer &serializer);
	virtual bool Deserialize(Serializer &serializer, bool first);


//--- variables
protected:
	string id;
	mutable Mutex *mutex;
};


//=========================================================================
// Methods
//=========================================================================

inline
void Root::SetID(const string &id)
{
	Assert(!id.empty() && id != "." && id != ".." && id.find('/') == id.npos);
	this->id = id;
}


/**
	Returns the object's id without path.
*/
inline
const string &Root::GetID()
{
	return this->id;
}


/**
	Finds a child object with a given id, @c NULL if not found.
*/
inline
Root *Root::Find(const string &id)
{
	Assert(!id.empty());

	// handle special cases '.' and '..'
	if (id[0] == '.')
	{
		if (id[1] == 0) 
			return this;

		if ((id[1]=='.') && (id[2]==0))
			return this->GetParent();
	}

	// find child
	for (Root *node = this->GetFront(); node; node = node->GetNext())
	{
		if (node->GetID() == id)
			return node;
	}

	return NULL;
}


inline
void Root::Lock() const
{
	if (!this->mutex)
	{
		this->mutex = ThreadServer::GetInstance()->CreateRecMutex();
		Assert(this->mutex);
	}

	this->mutex->Lock();
}


inline
void Root::Unlock() const
{
	Assert(this->mutex);
	this->mutex->Unlock();
}



#endif  // __root_h__

// vim:ts=4:sw=4:
