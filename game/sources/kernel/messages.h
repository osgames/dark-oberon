#ifndef __messages_h__
#define __messages_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file messages.h
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/ref.h"
#include "kernel/counted.h"
#include "kernel/messagereceiver.h"

class Root;


//=========================================================================
// BaseMessage
//=========================================================================

/**
	@class BaseMessage
	@ingroup Kernel_Module

	Used as common non-tempalted ancestor. Usefull in lists of messages.
*/
class BaseMessage : public ListNode<BaseMessage>, public Counted
{
	friend class MessageServer;

//--- methods
public:
	BaseMessage();
	double GetTimestamp() const;
	Root *GetSender() const;

protected:
	virtual bool DeliverMessage() = 0;

//--- variables
protected:
	double timestamp;
	Ref<Root> sender;
	Ref<Root> destination;
};


//=========================================================================

inline
BaseMessage::BaseMessage() :
	ListNode<BaseMessage>(),
	timestamp(0.0)
{
	//
}


inline
double BaseMessage::GetTimestamp() const
{
	return this->timestamp;
}


inline
Root *BaseMessage::GetSender() const
{
	return this->sender.GetUnsafe();
}


//=========================================================================
// Message
//=========================================================================

/**
	@class Message
	@ingroup Kernel_Module

	This class is able to deliver message to specific recever.
*/
template<class Type>
class Message : public BaseMessage
{
//--- methods
public:
	Message();

protected:
	virtual bool DeliverMessage();
};


//=========================================================================

template<class Type>
Message<Type>::Message() :
	BaseMessage()
{
	//
}


template<class Type>
bool Message<Type>::DeliverMessage()
{
	// get counted pointer to destination
	Counted::LockCounted();
	Root *dest = this->destination.IsValid() ? (Root *)this->destination->AcquirePointer() : NULL;
	this->destination = NULL;
	Counted::UnlockCounted();

	MessageReceiver<Type> *receiver = NULL;

	// destination is still alive
	if (dest)
	{
		// check if destination can receive this type of message
		receiver = dynamic_cast<MessageReceiver<Type> *>(dest);

		// deliver message
		if (receiver)
			receiver->OnMessage((Type *)this);

		// free counted pointer
		dest->Release();
	}

	return (receiver != NULL);
}


//=========================================================================
// Request
//=========================================================================

/**
	@class Request
	@ingroup Kernel_Module
*/
template <class Type>
class Request : public Message<Type>
{
public:
	Request();
	uint_t GetRequestId();

private:
	uint_t request_id;
	static uint_t counter;
};


//=========================================================================

template <class Type>
uint_t Request<Type>::counter = 0;


template <class Type>
Request<Type>::Request() :
	Message<Type>::Message()
{
	this->request_id = ++this->counter;
}


template <class Type>
uint_t Request<Type>::GetRequestId()
{
	return this->request_id;
}


//=========================================================================
// Response
//=========================================================================

/**
	@class Response
	@ingroup Kernel_Module
*/
template <class Type>
class Response : public Message<Type>
{
public:
	Response(uint_t request_id);
	uint_t GetRequestId();

private:
	uint_t request_id;
};


//=========================================================================

template <class Type>
Response<Type>::Response(uint_t request_id) :
	Message<Type>::Message()
{
	this->request_id = request_id;
}


template <class Type>
uint_t Response<Type>::GetRequestId()
{
	return this->request_id;
}


//=========================================================================
// Event
//=========================================================================

/**
	@class Event
	@ingroup Kernel_Module
*/
template <class Type>
class Event : public Message<Type>
{
public:
	Event(uint_t sequence = 0);
	uint_t GetSequence();

private:
	uint_t sequence;
	static uint_t counter;
};


//=========================================================================

template <class Type>
uint_t Event<Type>::counter = 0;


template <class Type>
Event<Type>::Event(uint_t sequence) :
	Message<Type>::Message()
{
	this->sequence = sequence ? sequence : ++this->counter;
}


template <class Type>
uint_t Event<Type>::GetSequence()
{
	return this->sequence;
}


//=========================================================================
// EmptyMessage
//=========================================================================

/**
	@class EmptyMessage
	@ingroup Kernel_Module

	Empty message used for testing.
*/
class EmptyMessage : public Message<EmptyMessage>
{
public:
	EmptyMessage() : Message<EmptyMessage>::Message() {}
};


#endif // __message_h__

// vim:ts=4:sw=4:
