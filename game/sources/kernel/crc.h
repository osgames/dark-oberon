#ifndef __CRC_H__
#define __CRC_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file crc.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// CRC
//=========================================================================

/**
	@class CRC
	@ingroup Kernel_Module

	A CRC checker class. 
*/

class CRC
{
//--- variables
private:
	enum 
	{
		BYTE_VALUES_COUNT = 256,      // MUST BE 256 (for each possible byte value 1 entry)
	};

	static const uint_t key;
	static uint_t table[BYTE_VALUES_COUNT];
	static bool initialized;


//--- methods
public:
	CRC();
	uint_t Checksum(uchar_t* ptr, uint_t bytes_count);
};


#endif  // __CRC_H__

// vim:ts=4:sw=4:
