#ifndef __ipcbuffer_h__
#define __ipcbuffer_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file ipcbuffer.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2007

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// IpcBuffer
//=========================================================================

/**
	@class IpcBuffer
	@ingroup Kernel_Module

	@brief A message buffer for the Ipc* class family.

	Note that a buffer is just a raw byte pool, especially buffers used
	for receiving messages may contain more then one message!
*/

class IpcBuffer
{
//--- methods
public:
	IpcBuffer(size_t max_size);
	IpcBuffer(const char *buf, size_t size);
	IpcBuffer(const char *str);
	~IpcBuffer();

	IpcBuffer& operator=(const IpcBuffer& rhs);

	size_t GetMaxSize() const;
	size_t Set(const char *buf, size_t size);
	char *GetPointer();
	const char *GetPointer() const;

	void SetSize(size_t s);
	size_t GetSize() const;
	size_t SetString(const char *str);
	const char *GetString() const;
	const char *GetFirstString() const;
	const char *GetNextString() const;
	bool IsValidString() const;

//--- variables
private:
	size_t buffer_size;
	size_t content_size;
	char *buffer;
	mutable const char *act_pointer;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Sets buffer contents. Returns number of bytes copied (this may be less
	then intended if a buffer overflow would occur.
*/
inline
size_t IpcBuffer::Set(const char *buf, size_t size)
{
	Assert(buf);

	if (size > this->buffer_size)
		size = this->buffer_size;

	memcpy(this->buffer, buf, size);
	this->content_size = size;

	return size;
}


/**
	Set the buffer content as string. The string may be truncated if it
	is too long. Returns the number of bytes copied (includes the
	terminating 0).
*/
inline
size_t IpcBuffer::SetString(const char *str)
{
	Assert(str);

	strncpy(this->buffer, str, this->buffer_size);
	this->content_size = strlen(this->buffer) + 1;

	return this->content_size;
}


/**
*/
inline
IpcBuffer &IpcBuffer::operator=(const IpcBuffer& rhs)
{
	this->Set(rhs.buffer, rhs.content_size);
	return *this;
}


/**
*/
inline
IpcBuffer::IpcBuffer(size_t max_size) :
	buffer_size(max_size),
	content_size(0),
	act_pointer(0)
{
	this->buffer = NEW char[max_size];
}


/**
*/
inline
IpcBuffer::IpcBuffer(const char *buf, size_t size) :
	buffer_size(size),
	act_pointer(0)
{
	this->buffer = NEW char[size];
	this->Set(buf, size);
}


/**
*/
inline
IpcBuffer::IpcBuffer(const char *buf) :
	act_pointer(0)
{
	Assert(buf);

	this->buffer_size = strlen(buf) + 1;
	this->buffer = NEW char[this->buffer_size];
	this->SetString(buf);
}


/**
*/
inline
IpcBuffer::~IpcBuffer()
{
	Assert(this->buffer);
	delete this->buffer;
	this->buffer = 0;
}


/**
*/
inline
size_t IpcBuffer::GetMaxSize() const
{
	return this->buffer_size;
}


/**
*/
inline
char *IpcBuffer::GetPointer()
{
	return this->buffer;
}


/**
*/
inline
const char *IpcBuffer::GetPointer() const
{
	return this->buffer;
}


/**
*/
inline
void IpcBuffer::SetSize(size_t s)
{
	Assert((s >= 0) && (s <= this->buffer_size));
	this->content_size = s;
}


/**
*/
inline
size_t IpcBuffer::GetSize() const
{
	return this->content_size;
}


/**
	Check if the buffer content is a valid string. This is the case
	if the last content byte is a 0.
*/
inline
bool IpcBuffer::IsValidString() const
{
	return ((this->content_size > 0) && (0 == this->buffer[this->content_size - 1]));
}


/**
	Get the buffer content as string. Should only be used if the
	buffer content has been set with the SetString() method before.
	If in doubt, use the IsValidString() method to check this out.
*/
inline
const char *IpcBuffer::GetString() const
{
	Assert(this->IsValidString());
	return this->buffer;
}


/**
	Returns the first string in a buffer containing multiple strings. Returns
	0 if buffer content is not a valid string.
*/
inline
const char *IpcBuffer::GetFirstString() const
{
	Assert(this->buffer);

	if (this->IsValidString())
	{
		this->act_pointer = this->buffer;
		return this->act_pointer;
	}

	else
	{
		this->act_pointer = 0;
		return 0;
	}
}


/**
	Returns the next string in a buffer containing multiple strings, or 0
	if no more strings in buffer.
*/
inline
const char *IpcBuffer::GetNextString() const
{
	Assert(this->act_pointer);
	Assert(this->buffer);

	const char *nextString = this->act_pointer + strlen(this->act_pointer) + 1;

	if (nextString >= (this->buffer + this->content_size))
		return NULL;

	else
	{
		this->act_pointer = nextString;
		return this->act_pointer;
	}
}

#endif

// vim:ts=4:sw=4:
