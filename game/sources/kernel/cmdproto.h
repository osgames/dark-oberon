#ifndef __CMDPROTO_H__
#define __CMDPROTO_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file cmdproto.h
	@ingroup Kernel_Module

 	@author Andre Weissflog, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/hashnode.h"

class Cmd;


//=========================================================================
// ProtoDefInfo
//=========================================================================

struct ProtoDefInfo {
    char out_args[64];
    char in_args[64];
    char name[128];

    byte_t out_args_count;
    byte_t in_args_count;

    bool valid;

	ProtoDefInfo(const char *proto_def);
};


//=========================================================================
// CmdProto
//=========================================================================

/**
	@class CmdProto
	@ingroup Kernel_Module

	An CmdProto object holds the prototype description for an Cmd
	object and can construct Cmd objects based on the prototype
	description "blue print".

	The prototype description is given to the CmdProto constructor
	as a string of the format

	"outargs_name_inargs"

	"outargs" is a list of characters describing number and datatypes
	of output arguments, "inargs" describes the input args in the
	same way. "name" is the name of the command.

	The following datatypes are defined:

		- 'v'     - void
		- 'i'     - int
		- 'f'     - float
		- 's'     - string
		- 'b'     - bool
		- 'a'     - array

	Examples of prototype descriptions:

	@verbatim
		v_rotate_fff    - name is 'rotate', no output args, 3 float input args,
		v_set_si        - name is 'set', no output args, one string and one int input arg
		fff_getrotate_v - 3 float output args, no input arg, name is 'getrotate'
	@endverbatim
*/

class CmdProto : public HashNode {
//--- variables
private:
    fourcc_t fourcc;
    byte_t in_args_count;
    byte_t out_args_count;
    bool cmd_locked;
    Cmd* cmd_template;

    string proto_def;


//--- methods
public:
	CmdProto(const char *proto_def, fourcc_t id);
	CmdProto(const CmdProto& rhs);
	virtual ~CmdProto();

	virtual bool Dispatch(void *, Cmd *) = 0;    ///< Executes a command on the provided object.

	fourcc_t GetId() const;
	int GetInArgsCount() const;
	int GetOutArgsCount() const;
	int GetNumArgs() const;
	const char *GetProtoDef() const;

	Cmd *NewCmd();
	void RelCmd(Cmd* cmd);
};


/**
	Gets fourcc code.
*/
inline
fourcc_t CmdProto::GetId() const
{
	return this->fourcc;
}


/**
	Gets number of input args.
*/
inline
int CmdProto::GetInArgsCount() const
{
	return this->in_args_count;
}


/**
	Gets number of output args.
*/
inline
int CmdProto::GetOutArgsCount() const
{
	return this->out_args_count;
}


/**
	Gets overall number of args.
*/
inline
int CmdProto::GetNumArgs() const
{
	return (this->in_args_count + this->out_args_count);
}


/**
	Gets the prototype string.
*/
inline
const char *CmdProto::GetProtoDef() const
{
	return this->proto_def.empty() ? 0 : this->proto_def.c_str();
}


#endif  // __CMDPROTO_H__

// vim:ts=4:sw=4:
