#ifndef __referenced_h__
#define __referenced_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file referenced.h
	@ingroup Kernel_Module

 	@author Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/list.h"
#include "kernel/mutex.h"

template<class TYPE> class Ref;


//=========================================================================
// Referenced
//=========================================================================

/**
	@class Referenced
	@ingroup Kernel_Module

	Provides simple reference counting as well as tracking references to self.
	Never destroy Referenced objects through delete.
*/

class Referenced
{
//--- methods
public:
	Referenced();
	virtual ~Referenced();

	template<class TYPE>
	static bool SetReference(Ref<TYPE> *, TYPE *obj);


//--- variables
private:
	SList references;                  ///< List of references.
	bool deleted;

	static uint_t instances_count;     ///< Number of all instances of Referenced object.
	static Mutex *mutex;               ///< Mutex for reference list.
};


//=========================================================================
// Methods
//=========================================================================

template<class TYPE>
inline
bool Referenced::SetReference(Ref<TYPE> *ref, TYPE *obj)
{
	// if no Referenced instance exists, all pointers should be NULL
	if (!Referenced::instances_count)
	{
		Assert(!ref->target_object && !obj);
		return true;
	}

	Referenced::mutex->Lock();

	// target object is the same
	if (ref->target_object == obj)
	{
		Referenced::mutex->Unlock();
		return true;
	}

	// unset old reference
	if (ref->target_object)
	{
		ref->Remove();
		ref->target_object = NULL;
	}

	// target object was deleted or NULL
	if (!obj || ((Referenced *)obj)->deleted)
	{
		Referenced::mutex->Unlock();
		return obj == NULL;
	}

	// set new reference
	((Referenced *)obj)->references.PushBack((SListNode *)ref);
	ref->target_object = obj;

	Referenced::mutex->Unlock();
	return true;
}


#endif // __referenced_h__

// vim:ts=4:sw=4:
