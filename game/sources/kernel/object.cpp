//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file object.cpp
	@ingroup Kernel_Module

 	@author Vadim Macagon, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/hashlist.h"
#include "kernel/kernelserver.h"
#include "kernel/object.h"


PrepareRootClass(Object, s_NewObject, s_InitObject, s_InitObject_cmds);


//=========================================================================
// Object
//=========================================================================

/**
	Constructor. DONT CALL DIRECTLY, USE KernelServer::New() INSTEAD!
*/
Object::Object(const char *id) :
	Counted(),
	Serialized(),
	my_class(NULL)
{
	// get kernel server
	this->kernel_server = KernelServer::GetInstance();
	Assert(this->kernel_server);
}


/**
	Destructor. DONT CALL DIRECTLY, USE Release() INSTEAD.
*/
Object::~Object()
{
	Assert(this->my_class);
	this->my_class->RemRef();
}


/**
	Invokes Cmd on object.
*/
bool Object::Dispatch(Cmd *cmd)
{
	Assert(cmd && cmd->GetProto());

	cmd->Rewind();
	cmd->GetProto()->Dispatch((void*)this, cmd);
	cmd->Rewind();

	return true;
}


/**
	Returns cmd proto list from object.
*/
void Object::GetCmdProtos(HashList *cmd_list)
{
	// for each superclass attach it's command proto names to the list
	Class *cl = this->my_class;

	// for each superclass
	do {
		HashList *cl_cmdprotos = cl->GetCmdList();

		if (cl_cmdprotos) {
			CmdProto *cmd_proto;

			for (cmd_proto = (CmdProto *)cl_cmdprotos->GetFront(); 
					cmd_proto; 
					cmd_proto = (CmdProto *)cmd_proto->GetNext()) 
			{
				HashNode* node = NEW HashNode(cmd_proto->GetName());

				node->SetData((void*)cmd_proto);
				cmd_list->PushBack(node);
			}
		}

	} while ((cl = cl->GetSuperClass()));
}


/**
	Get byte size of this instance. For more accuracy, subclasses should
	add the size of allocated memory.
*/
int Object::GetInstanceSize() const
{
	Assert(this->my_class);
	return this->my_class->GetInstanceSize();
}


/**
	Checks if a class of the given name is part of the class hierarchy
	for this object.
*/
bool Object::IsA(const char *class_name) const
{
	return this->IsA(this->kernel_server->FindClass(class_name));
}


const char *Object::GetGroupName()
{
	return "object";
}


// vim:ts=4:sw=4:
