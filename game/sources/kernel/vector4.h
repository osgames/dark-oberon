#ifndef __vector4_h__
#define __vector4_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file vector4.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/mathlib.h"
#include "kernel/vector3.h"
#include <float.h>


//=========================================================================
// vector4
//=========================================================================

/**
	@class vector4
	@ingroup NebulaMathDataTypes

	A generic vector4 class.
*/

class vector4 {
//--- embeded
public:
	enum component {
		X = (1<<0),
		Y = (1<<1),
		Z = (1<<2),
		W = (1<<3),
	};

//--- variables
public:
	float x, y, z, w;

//--- methods
public:
	vector4();
	vector4(const float _x, const float _y, const float _z, const float _w);
	vector4(const vector4& vec);
	vector4(const vector3& vec3);

	void set(const float _x, const float _y, const float _z, const float _w);
	void set(const vector4& v);
	void set(const vector3& v);

	float len() const;
	void norm();

	void operator +=(const vector4& v);
	void operator -=(const vector4& v);
	void operator *=(const float s);
	vector4& operator=(const vector3& v);

	bool isequal(const vector4& v, float tol = 0.0f) const;
	int compare(const vector4& v, float tol = 0.0f) const;
	void minimum(const vector4& v);
	void maximum(const vector4& v);
	void setcomp(float val, int mask);
	float getcomp(int mask);
	int mincompmask() const;
	void lerp(const vector4& v0, float lerpVal);
	void lerp(const vector4& v0, const vector4& v1, float lerpVal);
	void saturate();
	float dot(const vector4& v0) const;
};


/**
*/
inline
vector4::vector4() :
	x(0.0f),
	y(0.0f),
	z(0.0f),
	w(0.0f)
{
	// empty
}


/**
*/
inline
vector4::vector4(const float _x, const float _y, const float _z, const float _w) :
	x(_x),
	y(_y),
	z(_z),
	w(_w)
{
	// empty
}


/**
*/
inline
vector4::vector4(const vector4& v) :
	x(v.x),
	y(v.y),
	z(v.z),
	w(v.w)
{
	// empty
}


/**
*/
inline
vector4::vector4(const vector3& v) :
	x(v.x),
	y(v.y),
	z(v.z),
	w(1.0f)
{
	// empty
}


/**
*/
inline
void vector4::set(const float _x, const float _y, const float _z, const float _w)
{
	x = _x;
	y = _y;
	z = _z;
	w = _w;
}


/**
*/
inline
void vector4::set(const vector4& v)
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;
}


/**
*/
inline
void vector4::set(const vector3& v)
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = 1.0f;
}


/**
*/
inline
float vector4::len() const
{
	return (float) sqrt(x * x + y * y + z * z + w * w);
}


/**
*/
inline
void vector4::norm()
{
	float l = len();
	if (l > TINY) {
		float oneDivL = 1.0f / l;
		x *= oneDivL;
		y *= oneDivL;
		z *= oneDivL;
		w *= oneDivL;
	}
}


/**
*/
inline
void vector4::operator +=(const vector4& v)
{
	x += v.x; 
	y += v.y; 
	z += v.z; 
	w += v.w;
}


/**
*/
inline
void vector4::operator -=(const vector4& v)
{
	x -= v.x; 
	y -= v.y; 
	z -= v.z; 
	w -= v.w;
}


/**
*/
inline
void vector4::operator *=(const float s)
{
	x *= s; 
	y *= s; 
	z *= s; 
	w *= s;
}


/**
*/
inline
vector4 &vector4::operator=(const vector3& v)
{
	this->set(v);
	return *this;
}


/**
*/
inline
bool vector4::isequal(const vector4& v, float tol) const
{
	if (fabs(v.x - x) > tol)      return false;
	else if (fabs(v.y - y) > tol) return false;
	else if (fabs(v.z - z) > tol) return false;
	else if (fabs(v.w - w) > tol) return false;

	return true;
}


/**
*/
inline
int vector4::compare(const vector4& v, float tol) const
{
	if (fabs(v.x - x) > tol)      return (v.x > x) ? +1 : -1; 
	else if (fabs(v.y - y) > tol) return (v.y > y) ? +1 : -1;
	else if (fabs(v.z - z) > tol) return (v.z > z) ? +1 : -1;
	else if (fabs(v.w - w) > tol) return (v.w > w) ? +1 : -1;
	else                          return 0;
}


/**
*/
inline
void vector4::minimum(const vector4& v)
{
	if (v.x < x) x = v.x;
	if (v.y < y) y = v.y;
	if (v.z < z) z = v.z;
	if (v.w < w) w = v.w;
}


/**
*/
inline
void vector4::maximum(const vector4& v)
{
	if (v.x > x) x = v.x;
	if (v.y > y) y = v.y;
	if (v.z > z) z = v.z;
	if (v.w > w) w = v.w;
}


/**
*/
static inline 
vector4 operator +(const vector4& v0, const vector4& v1) 
{
	return vector4(v0.x + v1.x, v0.y + v1.y, v0.z + v1.z, v0.w + v1.w);
}


/**
*/
static inline
vector4 operator -(const vector4& v0, const vector4& v1) 
{
	return vector4(v0.x - v1.x, v0.y - v1.y, v0.z - v1.z, v0.w - v1.w);
}


/**
*/
static inline
vector4 operator *(const vector4& v0, const float& s) 
{
	return vector4(v0.x * s, v0.y * s, v0.z * s, v0.w * s);
}


/**
*/
static inline
vector4 operator -(const vector4& v)
{
	return vector4(-v.x, -v.y, -v.z, -v.w);
}


/**
*/
inline
void vector4::setcomp(float val, int mask)
{
	if (mask & X) x = val;
	if (mask & Y) y = val;
	if (mask & Z) z = val;
	if (mask & W) w = val;
}


/**
*/
inline
float vector4::getcomp(int mask)
{
	switch (mask) {
		case X:  return x;
		case Y:  return y;
		case Z:  return z;
		default: return w;
	}
}


/**
*/
inline
int vector4::mincompmask() const
{
	float minVal = x;
	int minComp = X;

	if (y < minVal) {
		minComp = Y;
		minVal  = y;
	}
	if (z < minVal) {
		minComp = Z;
		minVal  = z;
	}
	if (w < minVal) {
		minComp = W;
		minVal  = w;
	}

	return minComp;
}


/**
*/
inline
void vector4::lerp(const vector4& v0, float lerpVal)
{
	x = v0.x + ((x - v0.x) * lerpVal);
	y = v0.y + ((y - v0.y) * lerpVal);
	z = v0.z + ((z - v0.z) * lerpVal);
	w = v0.w + ((w - v0.w) * lerpVal);
}


/**
*/
inline
void vector4::lerp(const vector4& v0, const vector4& v1, float lerpVal)
{
	x = v0.x + ((v1.x - v0.x) * lerpVal);
	y = v0.y + ((v1.y - v0.y) * lerpVal);
	z = v0.z + ((v1.z - v0.z) * lerpVal);
	w = v0.w + ((v1.w - v0.w) * lerpVal);
}



/**
*/
inline
void vector4::saturate()
{
	x = n_saturate(x);
	y = n_saturate(y);
	z = n_saturate(z);
	w = n_saturate(w);
}


/**
	Dot product for vector4
*/
inline
float vector4::dot(const vector4& v0) const
{
	return ( x * v0.x + y * v0.y + z * v0.z + w * v0.w );
}


#endif // __vector4_h__

// vim:ts=4:sw=4:
