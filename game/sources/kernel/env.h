#ifndef __ENV_H__
#define __ENV_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file env.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/arg.h"
#include "kernel/root.h"


//=========================================================================
// Env
//=========================================================================

/**
	@class Env

	A typed value in a named object. Technically, a Arg object
	wrapped into a Root.
*/

class Env : public Root
{
//--- methods
public:
	Env(const char *id);
	Arg::Type GetType() const;

	void SetI(int i);
	void SetB(bool b);
	void SetF(float f);
	void SetS(const char *s);
	void SetO(Root *o);

	int GetI(void) const;
	bool GetB(void) const;
	float GetF(void) const;
	const char *GetS(void) const;
	Root *GetO(void) const;

	const char *GetAsString() const;


//--- variables
private:
	Arg arg;
	mutable string as_string;
};


//=========================================================================
// Methods
//=========================================================================

inline
Env::Env(const char *id) :
	Root(id)
{
	//
}


/**
	Returns the type of this Env instance.
*/
inline
Arg::Type Env::GetType(void) const
{
	return this->arg.GetType();
}


/**
	Sets the value of this object to the passed integer.
*/
inline
void Env::SetI(int i)
{
	this->arg.SetI(i);
}


/**
	Sets the value of this object to the passes boolean value.
*/
inline
void Env::SetB(bool b)
{
	this->arg.SetB(b);
}


/**
	Sets the value of this object to the passed float value.
*/
inline
void Env::SetF(float f)
{
	this->arg.SetF(f);
}


/**
	Sets the value of this object to the passed string.
*/
inline
void Env::SetS(const char *s)
{
	this->arg.SetS(s);
}


/**
	Sets the value of this objet to the passed object.
*/
inline
void Env::SetO(Root *o)
{
	this->arg.SetO(o);
}


/**
	Returns the integer value.
*/
inline
int Env::GetI() const
{
	return this->arg.GetI();
}


/**
	Returns the boolean value.
*/
inline
bool Env::GetB() const
{
	return this->arg.GetB();
}


/**
	Returns the float value.
*/
inline
float Env::GetF() const
{
	return this->arg.GetF();
}


/**
	Returns the string.
*/
inline
const char *Env::GetS() const
{
	return this->arg.GetS();
}


/**
	Returns the object.
*/
inline
Root *Env::GetO() const
{
	return (Root *) this->arg.GetO();
}


#endif  // __ENV_H__

// vim:ts=4:sw=4:
