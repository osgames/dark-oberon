#ifndef __condition_h__
#define __condition_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file condition.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Foreward declarations
//=========================================================================

class Mutex;


//=========================================================================
// Condition
//=========================================================================

/**
	@class Condition
	@ingroup Kernel_Module

	@brief Condition.
*/

class Condition
{
	friend class ThreadServer;

//--- methods
public:
	virtual ~Condition() {};

	virtual void Wait(Mutex *mutex, double timeout = -1.0) {};
	virtual void SendSignal() {};
	virtual void SendBroadcast() {};

protected:
	Condition() {};
};


#endif  // __condition_h__

// vim:ts=4:sw=4:
