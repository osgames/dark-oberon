#ifndef __CMD_H__
#define __CMD_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file cmd.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/types.h"
#include "kernel/arg.h"
#include "kernel/cmdproto.h"


//=========================================================================
// Cmd
//=========================================================================

/**
	@class Cmd
	@ingroup Kernel_Module

	Encapsulates a function call into a C++ object.
*/


class Cmd {
//--- variables
private:
	enum {
		MAX_AGRS_COUNT = 16
	};

	CmdProto *cmd_proto;
	int out_arg_index;
	int in_arg_index;
	Arg args[MAX_AGRS_COUNT];


//--- methods
public:
	Cmd(CmdProto *proto);
	Cmd(const Cmd& rhs);

	CmdProto* GetProto() const;
	fourcc_t GetId() const;

	int GetInArgsCount() const;
	int GetOutArgsCount() const;

	Arg* In();
	Arg* Out();
	void Rewind();
	
	void CopyInArgsFrom(va_list marker);
	void CopyInArgsFrom(Cmd *cmd);
};


//=========================================================================
// Methods
//=========================================================================

/**
	Construcor.
*/
inline 
Cmd::Cmd(CmdProto *proto)
{
	Assert(proto);

	this->cmd_proto = proto;
	this->in_arg_index = this->cmd_proto->GetOutArgsCount();
	this->out_arg_index = 0;
}


/**
	Copy constructor.
*/
inline 
Cmd::Cmd(const Cmd &rhs)
{
	this->cmd_proto = rhs.cmd_proto;
	this->in_arg_index = this->cmd_proto->GetOutArgsCount();
	this->out_arg_index = 0;
	int i;
	int numArgs = this->cmd_proto->GetNumArgs();

	for (i = 0; i < numArgs; i++)
		this->args[i] = rhs.args[i];
}


/**
	Get number of input args.
*/
inline 
int Cmd::GetInArgsCount() const
{
	return this->cmd_proto->GetInArgsCount();
}


/**
	Get number of output args.
*/
inline 
int Cmd::GetOutArgsCount() const
{
	return this->cmd_proto->GetOutArgsCount();
}


/**
	Gets the cmd's prototype object.
*/
inline 
CmdProto *Cmd::GetProto() const
{
	return this->cmd_proto;
}


/**
	Gets the cmd's fourcc code.
*/
inline 
fourcc_t Cmd::GetId() const
{ 
	return this->cmd_proto->GetId();
}


/**
	Rewind internal next args.
*/
inline 
void Cmd::Rewind() 
{
	this->in_arg_index = this->cmd_proto->GetOutArgsCount();
	this->out_arg_index = 0;
}


/**
	Gets next input argument.
*/
inline 
Arg *Cmd::In()
{
	Assert(this->in_arg_index < this->cmd_proto->GetNumArgs());
	return &(this->args[this->in_arg_index++]);
}


/**
	Gets next output argument.
*/
inline 
Arg *Cmd::Out()
{
	Assert(this->out_arg_index < this->GetOutArgsCount());
	return &(this->args[this->out_arg_index++]);
}


/**
	Set input arguments in Cmd from a C variable argument list.
*/
inline
void Cmd::CopyInArgsFrom(va_list marker)
{
	va_list markerCopy;
#ifdef WINDOWS
	markerCopy = marker;
#else
	va_copy(markerCopy, marker);
#endif
	for(int i = 0;i < this->GetInArgsCount();i++) {
		Arg * arg = this->In();
		arg->Copy(&markerCopy);
	}
}


/**
	Copy input arguments from another Cmd.
*/
inline
void Cmd::CopyInArgsFrom(Cmd *cmd)
{
	Assert(cmd);
	Assert(cmd->GetInArgsCount() == this->cmd_proto->GetInArgsCount());

	// rewind both commands
	cmd->Rewind();
	this->Rewind();

	// copy the arguments from the cmd provided 
	for(int i = 0; i < cmd->GetInArgsCount(); i++) {
		Arg * argSrc = cmd->In();
		Arg * argDst = this->In();
		Assert(argSrc->GetType() == argDst->GetType());
		argDst->Copy(*argSrc);
	}
}


#endif // __CMD_H_

// vim:ts=4:sw=4:
