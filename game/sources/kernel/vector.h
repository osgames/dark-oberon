#ifndef __vector_h__
#define __vector_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file vector.h
	@ingroup Kernel_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005

	Implement 2, 3 and 4-dimensional vector classes.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/vector2.h"
#include "kernel/vector3.h"
#include "kernel/vector4.h"

#endif // __vector_h__

// vim:ts=4:sw=4:
