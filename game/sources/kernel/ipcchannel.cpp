//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file ipcchannel.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2007 - 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/ipcchannel.h"


//=========================================================================
// IpcChannel
//=========================================================================

/**
*/
IpcChannel::IpcChannel(SOCKET sock) :
	protocol(PROTOCOL_NONE),
	socket_id(sock),
	can_receive(true)
{
	Assert(sock != INVALID_SOCKET);

	static uint_t channel_id_counter = 1;

	this->channel_id = channel_id_counter++;
}


/**
*/
IpcChannel::~IpcChannel()
{
	//
}


/**
	Close the receiver socket_id.
*/
void IpcChannel::DestroySocket()
{
	if (this->socket_id == INVALID_SOCKET)
		return;

	shutdown(this->socket_id, 2);
	closesocket(this->socket_id);

	this->socket_id = INVALID_SOCKET;
}


bool IpcChannel::ProtocolToString(Protocol protocol, string &name)
{
	switch (protocol)
	{
	case PROTOCOL_UDP: name = "UDP";  return true;
	case PROTOCOL_TCP: name = "TCP";  return true;
	default:
		LogWarning1("Invalid protocol: %d", protocol);
		return false;
	}
}


bool IpcChannel::StringToProtocol(const string &name, Protocol &protocol)
{
	if (name == "UCP")       protocol = PROTOCOL_UDP;
	else if (name == "TCP")  protocol = PROTOCOL_TCP;
	else
	{
		LogWarning1("Invalid protocol: %s", name.c_str());
		return false;
	}

	return true;
}


// vim:ts=4:sw=4:
