#ifndef __mathlib_h__
#define __mathlib_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file mathlib.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <math.h>
#include <stdlib.h>


//=========================================================================
// Defines
//=========================================================================

#ifdef _MSC_VER
#	define isnan _isnan
#	define isinf _isinf
#endif

#ifndef PI
#	define PI (3.1415926535897932384626433832795028841971693993751f)
#endif
#ifndef TINY
#	define TINY (0.0000001f)
#endif
#ifndef SQRT2
#	define SQRT2 (1.4142135623730950488016887242097f)
#endif


#define n_clamp(x, min, max) (((x) < (min)) ? (min) : (((x) > (max)) ? (max) : (x)))
#define n_max(a, b)          (((a) > (b)) ? (a) : (b))
#define n_min(a, b)          (((a) < (b)) ? (a) : (b))
#define n_abs(a)             (((a) < 0.0f) ? (-(a)) : (a))
#define n_sqr(a)             ((a) * (a))
#define n_sgn(a)             (((a) < 0.0f) ? (-1) : (1))
#define n_deg2rad(d)         (((d) * PI) / 180.0f)
#define n_rad2deg(r)         (((r) * 180.0f) / PI)


//=========================================================================
// Methods
//=========================================================================

/**
    log2() function.
*/

inline
float n_log2(float f)
{
	return logf(f) / 0.693147180559945f;
}


/**
	acos with value clamping.
*/
inline
float n_acos(float x)
{
	if(x >  1.0f) x =  1.0f;
	if(x < -1.0f) x = -1.0f;
	return (float)acos(x);
}


/**
	asin with value clamping.
*/
inline
float n_asin(float x)
{
	if(x >  1.0f) x =  1.0f;
	if(x < -1.0f) x = -1.0f;
	return (float)asin(x);
}


/**
	Safe sqrt.
*/
inline
float n_sqrt(float x)
{
	if (x < 0.0f) x = (float) 0.0f;
	return (float) sqrt(x);
}


/**
	A fuzzy floating point equality check.
*/
inline
bool n_fequal(float f0, float f1, float tol)
{
	float f = f0 - f1;
	return (f > (-tol)) && (f < tol);
}


/**
	A fuzzy floating point less-then check.
*/
inline
bool n_fless(float f0, float f1, float tol)
{
	return (f0-f1) < tol;
}


/**
	A fuzzy floating point greater-then check.
*/
inline
bool n_fgreater(float f0, float f1, float tol)
{
	return (f0-f1) > tol;
}


/**
	Fast float to int conversion (always truncates).
	see http://www.stereopsis.com/FPU.html for a discussion.
	NOTE: This works only on x86 endian machines.
*/
inline
long n_ftol(float val)
{
	double v = double(val) + (68719476736.0 * 1.5);
	return ((long*)&v)[0] >> 16;
}


/**
	Smooth a new value towards an old value using a max change value.
*/
inline
float n_smooth(float new_val, float cur_val, float max_change)
{
	float diff = new_val - cur_val;

	if (fabs(diff) > max_change) {
		if (diff > 0.0f) {
			cur_val += max_change;
			if (cur_val > new_val)
				cur_val = new_val;
		}
		else if (diff < 0.0f) {
			cur_val -= max_change;
			if (cur_val < new_val)
				cur_val = new_val;
		}
	}
	else
		cur_val = new_val;

	return cur_val;
}


/**
	Saturates a value (clamps between 0.0f and 1.0f)
*/
inline
float n_saturate(float val)
{
	return n_clamp(val, 0.0f, 1.0f);
}


/**
	Returns a pseudo random number from interval <0, max).
	@param max Maximal boundary.
 */
inline
int n_irand(int max)
{
	Assert(max < RAND_MAX);
	if (!max) return 0;
	return rand() % max;
}


/**
	Returns a pseudo random number between 0.0f and 1.0f.
*/
inline
float n_frand()
{
	return float(rand()) / float(RAND_MAX);
}


/**
	Returns a pseudo random float number between 0.0f and max.
	@param max Maximal boundary.
 */
inline
float n_frand(float max)
{
	return (float(rand()) / RAND_MAX) * max;
}


/**
	Returns a pseudo random number between 0.0 and 1.0.
 */
inline
double n_drand()
{
	return (double(rand()) / RAND_MAX);
}


/**
	Returns a pseudo random number between 0.0 and max.
	@param max Maximal boundary.
 */
inline
double n_drand(double max)
{
	return (double(rand()) / RAND_MAX) * max;
}


/**
	Convert float to integer.
*/
inline
int n_fchop(float f)
{
	return int(f);
}


/**
	Rounds float to integer.
*/
inline
int n_fround(float f)
{
	return n_fchop(floorf(f + 0.5f));
}


/**
	Linearly interpolates between 2 values: ret = x + l * (y - x).
*/
inline
float n_lerp(float x, float y, float l)
{
	return x + l * (y - x);
}


/**
	Normalizes an angular value into the range rad(0) to rad(360).
*/
inline
float n_normangle(float a)
{
	while (a < 0.0f)
		a += n_deg2rad(360.0f);

	if (a >= n_deg2rad(360.0f))
		a = fmodf(a, n_deg2rad(360.0f));

	return a;
}


/**
	Gets the first power of 2 >= given value
*/
inline int n_next_p2(int a)
{
	int result = 1;

	while (result < a)
		result <<= 1;

	return result;
}


#endif // __mathlib_h__

// vim:ts=4:sw=4:
