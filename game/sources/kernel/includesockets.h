#ifndef __socketdefs_h__
#define __socketdefs_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file includesockets.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2002, 2007

	Some platform specific wrapper defs for sockets.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/logserver.h"

#ifdef WINDOWS
#	undef APIENTRY
#	undef WINGDIAPI
#	include <winsock.h>
#	undef RegisterClass
#	undef CreateMutex
#else
#	include <arpa/inet.h>
#	include <netdb.h>
#	include <netinet/in.h>
#	include <sys/socket.h>
#	include <sys/types.h>
#	include <unistd.h>
#	include <fcntl.h>
#	include <errno.h>
#endif


//=========================================================================
// Types and constants
//=========================================================================

#ifdef WINDOWS
	typedef int socklen_t;

#	define N_SOCKET_LAST_ERROR    WSAGetLastError()
#	define N_EWOULDBLOCK          WSAEWOULDBLOCK
#	define N_ECONNRESET           WSAECONNRESET

#else
	typedef int SOCKET;
#	define closesocket            close
#	define INVALID_SOCKET         (-1)
#	define SOCKET_ERROR           (-1)

#	define N_SOCKET_LAST_ERROR    errno
#	define N_EWOULDBLOCK          EWOULDBLOCK
#	define N_ECONNRESET           ECONNRESET
#endif


#define N_SOCKET_MIN_PORTNUM   (12000)
#define N_SOCKET_MAX_PORTNUM   (12999)
#define N_SOCKET_PORTRANGE     (N_SOCKET_MAX_PORTNUM - N_SOCKET_MIN_PORTNUM)


//=========================================================================
// SocketUtils
//=========================================================================

#include "kernel/socketutils.h"

#endif // __socketdefs_h__

// vim:ts=4:sw=4:
