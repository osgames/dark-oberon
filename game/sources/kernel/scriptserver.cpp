//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scriptserver.cpp
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/scriptserver.h"
#include "kernel/scriptloader.h"
#include "kernel/kernelserver.h"
#include "kernel/fileserver.h"
#include "kernel/logserver.h"

PrepareClass(ScriptServer, "Root", s_NewScriptServer, s_InitScriptServer);


//=========================================================================
// ScriptServer
//=========================================================================

/**
	Constructor.
*/
ScriptServer::ScriptServer(const char *id) :
	RootServer<ScriptServer>(id),
	exit_requested(false)
{
	// empty
}


/**
	Destructor.
*/
ScriptServer::~ScriptServer()
{
	// empty
}


/**
	Sets default language. The loader of this language will be used to run the scripts
	when language is not set explicitly.

	@param language Identifier of script language.
	@return @c True if loader of specified language is found.
*/
bool ScriptServer::SetDefaultLanguage(const string &language)
{
	Assert(!language.empty());

	// find script loader
	ScriptLoader *loader = (ScriptLoader *)this->Find(language);
	if (!loader) {
		LogError1("Error finding script loader for language: %s", language.c_str());
		return false;
	}

	// set new loader
	this->default_language = language;
	this->default_loader = loader;

	return true;
}


/**
	Cerates new script loader of given class. This loader will be associated with specified language.
	Each loader is a NOH child of script server.

	@param class_name Class name of script loader.
	@param language Identifier of script language.
	@return Pointer to new object.
*/
ScriptLoader *ScriptServer::NewScriptLoader(const char *class_name, const string &language)
{
	// create script loader
	this->kernel_server->PushCwd(this);
	ScriptLoader *loader = (ScriptLoader *)this->kernel_server->New(class_name, language);
	Assert(loader);
	this->kernel_server->PopCwd();

	// set default loader if first script loader is created
	if (!this->default_loader.IsValid()) {
		this->default_loader = loader;
		this->default_language = language;
	}

	return loader;
}


/**
	Returns loader of given language. If language parameter is NULL, the default loader is returned.

	@param language Identifier of script language.
	@return Pointer to script loader or NULL if some error occures.
*/
ScriptLoader *ScriptServer::GetLoader(const char *language)
{
	ScriptLoader *loader;

	// find script loader according to language
	if (language && *language) {
		loader = (ScriptLoader *)this->Find(string(language));
		if (!loader) {
			LogError1("Error finding script loader for language: %s", language);
			return NULL;
		}
	}

	// get default loader
	else {
		loader = this->default_loader.GetUnsafe();
		if (!loader) {
			LogError("Missing default script loader");
			return NULL;
		}
	}

	return loader;
}


/**
	Runs a script statement.

	@param  code        The statement to execute.
	@param  result      Will be filled with the result.
	@param language    Identifier of script language, NULL means defult language.
	@return            @c True if successful.
*/
bool ScriptServer::RunCode(const char *code, string &result, const char *language)
{
	Assert(code);

	// find script loader
	ScriptLoader *loader = this->GetLoader(language);
	if (!loader)
		return false;

	// run function
	return loader->RunCode(code, result);
}


/**
	Runs a script function with the specified name without any args.

	@param  func_name   Function name.
	@param  result      Will be filled with the result.
	@param language    Identifier of script language, NULL means defult language.
	@return            @c True if successful.
*/
bool ScriptServer::RunFunction(const char *func_name, string &result, const char *language)
{
	Assert(func_name);

	// find script loader
	ScriptLoader *loader = this->GetLoader(language);
	if (!loader)
		return false;

	// run function
	return loader->RunFunction(func_name, result);
}


/**
	Runs a script file.

	Language is recognised from file extension.

	@todo More powerful language recognising (one language can registered more file extensions).

	@param  file_path    The script file name with path.
	@param  result       Will be filled with the result.

	@return             @c True if successful.
*/
bool ScriptServer::RunScript(const string &file_path, string &result)
{
	Assert(!file_path.empty());

	// extract file extension
	string language;
	FileServer::ExtractExtension(file_path, language);

	// find script loader
	ScriptLoader *loader = this->GetLoader(language.c_str());
	if (!loader)
		return false;

	// run function
	return loader->RunScript(file_path, result);
}


/**
	Generates a prompt string for interactive mode.

	@return Prompt string.
*/
string ScriptServer::Prompt()
{
	string prompt = kernel_server->GetCwd()->GetFullPath();
	prompt.append("> ");
	return prompt;
}


/**
    This method should be called frequently (normally once per frame) when
    the script server is not run in interactive mode.

	@return Whether there was request to exit interractive mode.
*/
bool ScriptServer::Trigger()
{
	bool retval = !(this->exit_requested);

	this->exit_requested = false;

	return retval;
}

// vim:ts=4:sw=4:
