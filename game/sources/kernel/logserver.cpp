//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file logserver.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - File* insted of FILE* is used together with FileServer.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/logserver.h"
#include "kernel/threadserver.h"
#include "kernel/fileserver.h"


//=========================================================================
// LogServer
//=========================================================================

LogServer::LogServer() :
	Server<LogServer>()
{
	// create internal mutex
	this->mutex = ThreadServer::GetInstance()->CreateRecMutex();
	Assert(this->mutex);
}


LogServer::~LogServer()
{
	this->UnregisterAll();

	// delete mutex
	delete this->mutex;
}


bool LogServer::RegisterFile(File *file, int levels, bool close)
{
	Assert(file);
	Assert(FileServer::GetInstance());

	// register file
	this->Lock();
	this->entries.push_back(LogEntry(file, close, FileServer::GetInstance()->IsStandartStream(file), levels));
	this->Unlock();

	LogInfo1("Registered logging to file: %s", file->GetFilePath().c_str());

	return true;
}


bool LogServer::RegisterFile(const string &file_path, int levels)
{
	Assert(!file_path.empty());

	// get file server
	FileServer *file_server = FileServer::GetInstance();
	if (!file_server)
	{
		LogError1("File server does not exists while registering file: %s", file_path.c_str());
		return false;
	}

	// create file
	DisableMemorySystem();
	File *file = file_server->NewFile();
	file->Open(file_path, "wt");
	EnableMemorySystem();

	if (!file)
	{
		LogError1("Can not open log file: %s", file_path.c_str());
		return false;
	}

	return this->RegisterFile(file, levels, true);
}


bool LogServer::RegisterFunction(LogFunction *func, int levels)
{
	Assert(func);

	this->Lock();
	this->entries.push_back(LogEntry(func, levels));
	this->Unlock();

	LogInfo("Registered callback function for logging");

	return true;
}


void LogServer::UnregisterAll()
{
	LogInfo("Unregistering all logs");

	this->Lock();

	EntryArrayIter it;
	for (it = this->entries.begin(); it != this->entries.end(); it++)
	{
		if (it->type == LogEntry::LT_FILE && it->close)
		{
			it->file->Close();
			delete it->file;
		}
	}

	this->entries.clear();
	this->Unlock();
}


void LogServer::WriteMessage(LogLevel level, const char *file, int line, const char *message, va_list args)
{
	Assert(file);
	Assert(message);

	char msg[LOG_MAX_MESSAGE_SIZE];
	char *header;
	string file_name = file;
	
	int count;

	// process message text
	count = vsnprintf(msg, LOG_MAX_MESSAGE_SIZE, message, args);

	if (count == -1)
	{
		msg[LOG_MAX_MESSAGE_SIZE - 1] = 0;
		msg[LOG_MAX_MESSAGE_SIZE - 2] = '.';
		msg[LOG_MAX_MESSAGE_SIZE - 3] = '.';
		msg[LOG_MAX_MESSAGE_SIZE - 4] = '.';
	}

#ifdef DEBUG

	str_pos_t pos, pos2;

	// extract file name (only in debug)
#ifdef WINDOWS
	pos = file_name.rfind("/");
	pos2 = file_name.rfind("\\");
	if (pos2 != string::npos)
	{
		if (pos != string::npos && pos2 > pos) pos = pos2;
		else if (pos == string::npos) pos = pos2;
	}

	if (pos == string::npos)
		pos = 0;
	else
		pos++;

	file_name = file_name.substr(pos);
#endif

	// limit length
	if (file_name.length() > 17)
		file_name = file_name.substr(file_name.length() - 17);

#endif

	// get header
	switch (level)
	{
	case LL_INFO:     header = LOG_HEADER_INFO;     break;
	case LL_WARNING:  header = LOG_HEADER_WARNING;  break;
	case LL_ERROR:    header = LOG_HEADER_ERROR;    break;
	case LL_CRITICAL: header = LOG_HEADER_CRITICAL; break;
	case LL_DEBUG:    header = LOG_HEADER_DEBUG;    break;
	}

	this->Lock();

	// log message to every entry with defined level
	for (EntryArrayIter iter = this->entries.begin(); iter != this->entries.end(); iter++)
	{
		if (iter->levels & level)
			switch (iter->type)
			{
			case LogEntry::LT_FILE:
				if (iter->standart)
					iter->file->PrintF("%s %s\n", header, msg);
				else
				{
					#ifdef DEBUG
					iter->file->PrintF("[%17s:%4d] %s %s\n", file_name.c_str(), line, header, msg);
					#else
					iter->file->PrintF("%s %s\n", header, msg);
					#endif

					iter->file->Flush();
				}
				break;

			case LogEntry::LT_FUNC:
				iter->func(level, file_name, line, string(msg));
				break;

			default:
				break;
			}
	}

	this->Unlock();
}


// vim:ts=4:sw=4:
