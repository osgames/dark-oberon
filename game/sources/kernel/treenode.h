#ifndef __treenode_h__
#define __treenode_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file treenode.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Parent handling moved to List and ListNode.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/list.h"


//=========================================================================
// TreeNode
//=========================================================================

/**
	@class TreeNode
	@ingroup Kernel_Module

	@brief Implements a tree node with childrens stored in list.

	Node have to be only TreeNode derived class!
*/

template <class Node>
class TreeNode : public List<Node>, public ListNode<Node>
{
	friend class TreeNode<Node>;

//--- methods
public:
	TreeNode();

	Node *GetParent() const;
};


//=========================================================================
// Methods
//=========================================================================

template <class Node>
TreeNode<Node>::TreeNode() :
	List<Node>(),
	ListNode<Node>()
{
	//
}


/**
	Returns the parent object. If the object doesn't have a parent object 
	(this is only valid for the root object of tree), @c NULL will be returned.
*/
template <class Node>
Node *TreeNode<Node>::GetParent() const
{
	return (Node *)ListNode<Node>::GetParent();
}


#endif  // __treenode_h__

// vim:ts=4:sw=4:
