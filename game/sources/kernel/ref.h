#ifndef __ref_h__
#define __ref_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file ref.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005 - 2006, 2008

	@version 1.0 - Initial.
	@version 1.1 - Referencing is hread-safe now. Comparison operators are const.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/listnode.h"
#include "kernel/referenced.h"


//=========================================================================
// Ref
//=========================================================================

/**
	@class Ref
	@ingroup Kernel_Module

	Ref implements safe pointers to Referenced derived objects which will 
	invalidate themselves when the target object goes away.
	Usage of Ref helps you to avoid dangling pointers and 
	also protects against dereferencing a null pointer.

	Operations:

	Assigning ptr to ref:
	ref = ptr OR ref.Set(ptr)

	Invalidating:
	ref = NULL OR ref.Invalidate() OR ref.Set(NULL)

	Checking if pointer is valid (non-null):
	ref.IsValid()
*/

template<class TYPE>
class Ref : public SListNode
{
	friend Referenced;

//--- methods
public:
	Ref();
	Ref(TYPE *o);
	Ref(const Ref<TYPE> &rhs);
	~Ref();

	Ref& operator=(TYPE *obj);
	Ref& operator=(const Ref<TYPE> &rhs);
	bool operator==(const Ref<TYPE> &rhs) const;
	bool operator!=(const Ref<TYPE> &rhs) const;
	bool operator==(TYPE *obj) const;
	bool operator!=(TYPE *obj) const;

	TYPE *operator->() const;
	TYPE &operator*() const;
	operator TYPE*() const;

	void Set(TYPE *obj);
	TYPE *Get() const;
	TYPE *GetUnsafe() const;
	bool IsValid() const;


//--- variables
protected:
	TYPE *target_object;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Default constructor.
*/
template<class TYPE> 
inline
Ref<TYPE>::Ref() :
	target_object(NULL)
{
	// empty
}


/**
	Constructor with target object.

	@param obj Pointer to target object.
*/
template<class TYPE>
inline
Ref<TYPE>::Ref(TYPE *obj) :
	target_object(NULL)
{
	if (obj)
		this->Set(obj);
}


/**
	Copy constructor.

	@param ref Reference to object.
*/
template<class TYPE>
inline
Ref<TYPE>::Ref( const Ref<TYPE> &ref) :
	target_object(NULL)
{
	if (ref.GetUnsafe())
		this->Set(ref.GetUnsafe());
}


/**
	Destructor.
*/
template<class TYPE>
inline
Ref<TYPE>::~Ref()
{
	this->Set(NULL);
}


/**
	Set target object.

	@param obj Pointer to target object.
*/
template<class TYPE>
inline
void Ref<TYPE>::Set(TYPE *obj)
{
	Referenced::SetReference(this, obj);
}


/**
	Gets target object (safe).

	@return Pointer to target object.
*/
template<class TYPE>
inline
TYPE *Ref<TYPE>::Get() const
{    
	AssertMsg(this->target_object, "Null pointer access through Ref");
	return this->target_object;
}


/**
	Gets target object (unsafe, may return NULL).

	@return Pointer to target object.
*/
template<class TYPE>
inline
TYPE *Ref<TYPE>::GetUnsafe() const
{
	return this->target_object;
}


/**
	Checks if target object exists.

	@return @c True if pointer is still valid.
*/
template<class TYPE>
inline
bool Ref<TYPE>::IsValid(void) const
{
	return this->target_object ? true : false;
}


/**
	Overrides -> operator.
*/
template<class TYPE>
inline
TYPE *Ref<TYPE>::operator->() const
{
	AssertMsg(this->target_object, "Null pointer access through Ref");
	return this->target_object;
}


/**
	Dereferences operator.
*/
template<class TYPE>
inline
TYPE &Ref<TYPE>::operator*() const
{
	AssertMsg(this->target_object, "Null pointer access through Ref");
	return *this->target_object;
}


/**
	Copy operator.
*/
template<class TYPE>
inline
Ref<TYPE> &Ref<TYPE>::operator=(const Ref<TYPE> &rhs)
{
	this->Set(rhs.target_object);
	return *this;
}


/**
	Assigns TYPE pointer.
*/
template<class TYPE>
inline
Ref<TYPE> &Ref<TYPE>::operator=(TYPE *obj)
{
	this->Set(obj);
	return *this;
}


/**
	Cast operator.
*/
template<class TYPE>
inline
Ref<TYPE>::operator TYPE *() const
{
	AssertMsg(this->target_object, "Null pointer access through Ref");
	return this->target_object;
}


/**
    Equality operator.
*/
template<class TYPE>
inline
bool Ref<TYPE>::operator==(const Ref<TYPE> &rhs) const
{
    return (this->target_object == rhs.target_object);
}


/**
	Inequality operator.
*/
template<class TYPE>
inline
bool Ref<TYPE>::operator!=(const Ref<TYPE> &rhs) const
{
	return (this->target_object != rhs.target_object);
}


/**
	Equality operator.
*/
template<class TYPE>
inline
bool Ref<TYPE>::operator==(TYPE *obj) const
{
	return (obj == this->target_object);
}


/**
	Inequality operator.
*/
template<class TYPE>
inline
bool Ref<TYPE>::operator!=(TYPE *obj) const
{
	return (obj != this->target_object);
}


#endif  // __ref_h__

// vim:ts=4:sw=4:
