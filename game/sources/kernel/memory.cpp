//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file memory.cpp
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/memory.h"

#ifdef DEBUG_MEMORY

#include "kernel/debug.h"
#include "kernel/logserver.h"
#include "kernel/threadserver.h"


//========================================================================
// Structures
//========================================================================

class MemorySystem
{
//--- embeded
private:
	/**
		Stores information about one allocated block
	*/
	struct BlockHeader
	{
		size_t size;            ///< Size of allocated block.
		const char *file;       ///< File from which was block allocated.
		int line;               ///< Line from which was block allocated.
		BlockHeader *prev;      ///< Previous allocated block.
		BlockHeader *next;      ///< Next allocated block.
	};


//--- variables
private:
	bool opened;                ///< Whether system is initialized.
	bool enabled;               ///< Whether system is currently enabled.

	uint_t total_count;         ///< Total count of allocated blocks.
	size_t total_size;          ///< Total size of allocated blocks.
	uint_t act_count;		    ///< Actual count of allocated blocks.
	size_t act_size;		    ///< Actuam size of allocated blocks.
	uint_t max_count;		    ///< Maximal count of allocated blocks at once.
	size_t max_size;		    ///< Maximal size of allocated blocks at once.

	Mutex *mutex;               ///< Mutex for thread-safe managing list of allocated blocks.

	BlockHeader *first_header;  ///< First block of double linked list of allocated blocks.


//--- methods
public:
	MemorySystem();
	~MemorySystem();

	void Open();
	void Close();
	void SetEnabled(bool enabled);

	size_t GetActSize();
	uint_t GetActCount();

	void *NewBlock(size_t size, const char *file = NULL, int line = 0);
	void DeleteBlock(void *address, const char *file = NULL, int line = 0);

	void CheckBlocks();
};


//========================================================================
// Variables
//========================================================================

MemorySystem memory_system;


//========================================================================
// Methods
//========================================================================

void OpenMemorySystem()
{
	memory_system.Open();
}


void CloseMemorySystem()
{
	memory_system.Close();
}


void EnableMemorySystem()
{
	memory_system.SetEnabled(true);
}


void DisableMemorySystem()
{
	memory_system.SetEnabled(false);
}


size_t GetActMemorySize()
{
	return memory_system.GetActSize();
}


uint_t GetActMemoryBlocks()
{
	return memory_system.GetActCount();
}


//========================================================================
// MemorySystem
//========================================================================

MemorySystem::MemorySystem() :
	opened(false),
	enabled(false),
	total_count(0),
	total_size(0),
	act_count(0),
	act_size(0),
	max_count(0),
	max_size(0),
	first_header(NULL)
{
	//
}


MemorySystem::~MemorySystem()
{
	Assert(!this->opened);
}


/**
	Initialize memory system.
*/
void MemorySystem::Open()
{
	Assert(!this->opened);

	this->mutex = ThreadServer::GetInstance()->CreateRecMutex();
	Assert(this->mutex);

	this->opened = this->enabled = true;
}


/**
	Finishes memory system and checks undeleted blocks.
*/
void MemorySystem::Close()
{
	Assert(this->opened);

	this->opened = this->enabled = false;
	SafeDelete(this->mutex);

	this->CheckBlocks();
}


/**
	Enables or disables registering of allocated blocks.
*/
void MemorySystem::SetEnabled(bool enabled)
{
	Assert(this->opened);
	this->enabled = enabled;
}


size_t MemorySystem::GetActSize()
{
	return this->act_size;
}


uint_t MemorySystem::GetActCount()
{
	return this->act_count;
}


/**
	Check undeleted blocks and write info to log files.
*/
void MemorySystem::CheckBlocks()
{
	Assert(!this->opened);

	// log undeleted blocks
	if (this->first_header)
	{
		LogDebug("--- MEMORY BEGIN ---");

		// log all memory blocks from list
		BlockHeader *act_header = this->first_header;

		do
		{
			// write log
			if (act_header->file)
				LogDebug3("Undeleted block: size = %d, in %s:%d", act_header->size, act_header->file, act_header->line);
			else
				LogDebug1("Undeleted block: size = %d", act_header->size);

			act_header = act_header->next;
		} while (act_header != this->first_header);
	}

	// clear list of undeleted blocks (but keep blocks in memory)
	this->first_header = NULL;

	// log final statistics
	LogDebug("--- MEMORY STAT ---");
	LogDebug1("Total blocks:     %d", this->total_count);
	LogDebug1("Maximal blocks:   %d", this->max_count);
	LogDebug2("Undeleted blocks: %d (%.2lf%%)", this->act_count, ((double)this->act_count / this->total_count) * 100);
	LogDebug1("Total size:       %d", this->total_size);
	LogDebug1("Maximal size:     %d", this->max_size);
	LogDebug2("Undeleted size:   %d (%.2lf%%)", this->act_size, ((double)this->act_size / this->total_size) * 100);
	LogDebug("--- MEMORY END ---");
}


/**
	Allocates new memory block.
*/
inline
void *MemorySystem::NewBlock(size_t size, const char *file, int line)
{
	size_t header_size = sizeof(BlockHeader);

	// allocate zeroized memory
	void *block = malloc(size + header_size);
	if (!block)
		return NULL;

	// initialize memory block
	BlockHeader *header = (BlockHeader *)block;
	block = header + 1;
	memset(header, 0, header_size);
	memset(block, 0xFF, size);

	// memory system hasn't been opened
	if (!this->opened || !this->enabled)
		return block;

	this->mutex->Lock();

	// fill header
	header->size = size;
	header->file = file;
	header->line = line;

	// add header to the end of double linked list
	if (this->first_header)
	{
		header->prev = this->first_header->prev;
		header->next = this->first_header;
		header->prev->next = header;
		header->next->prev = header;
	}

	// add first header to the list
	else
		this->first_header = header->prev = header->next = header;

	// update statistics
	this->total_count++;
	this->total_size += size;
	this->act_count++;
	this->act_size += size;
	if (this->act_count > this->max_count)
		this->max_count = this->act_count;
	if (this->act_size > this->max_size)
		this->max_size = this->act_size;

	this->mutex->Unlock();

	return block;
}


/**
	Deletes memory block.
*/
inline
void MemorySystem::DeleteBlock(void *address, const char *file, int line)
{
	if (!address)
		return;

	BlockHeader *header = (BlockHeader *)address - 1;

	// memory system hasn't been opened
	if (this->opened && this->enabled)
	{
		this->mutex->Lock();

		// check if header is valid
		if (header->prev && header->next)
		{
			// remove record from the list of allocated blocks
			header->prev->next = header->next;
			header->next->prev = header->prev;
			if (header == this->first_header)
				this->first_header = header->next;

			// update statistics
			this->act_count--;
			this->act_size -= header->size;
		}

		// block is not in the list
		else
		{
			if (file)
				LogWarning3("Missing block: 0x00%8lx in %s:%d", (size_t)address, file, line);
			else
				LogWarning1("Missing block: 0x00%8lx", (size_t)address);
		}

		this->mutex->Unlock();
	}

	// delete memory block
	free(header);
}


//========================================================================
// Replacements of global new & delete
//========================================================================

/**
	Replacement global new operator without location reporting.
	This catches calls which don't use NEW for some reason.
*/
void* operator new(size_t size)
{
	return memory_system.NewBlock(size);
}


/**
	Replacement global new[] operator with location reporting.
*/
void* operator new(size_t size, const char *file, int line)
{
	return memory_system.NewBlock(size, file, line);
}


/**
	Replacement global new[] operator without location reporting.
*/
void* operator new[](size_t size)
{
	return memory_system.NewBlock(size);
}


/**
	Replacement global new[] operator with location reporting.
*/
void* operator new[](size_t size, const char *file, int line)
{
	return memory_system.NewBlock(size, file, line);
}


/**
	Replacement global delete operator.
*/
void operator delete(void* p)
{
	memory_system.DeleteBlock(p);
}


/**
	Replacement global delete operator to match the new with location reporting.
*/
void operator delete(void* p, const char *file, int line)
{
	memory_system.DeleteBlock(p, file, line);
}


/**
	Replacement global delete[] operator.
*/
void operator delete[](void* p)
{
	memory_system.DeleteBlock(p);
}


/**
	Replacement global delete[] operator to match the new with location reporting.
*/
void operator delete[](void* p, const char *file, int line)
{
	memory_system.DeleteBlock(p, file, line);
}


#endif

//=========================================================================
// END
//=========================================================================
// vim:ts=4:sw=4:
