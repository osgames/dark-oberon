#ifndef __listnode_h__
#define __listnode_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file listnode.h
	@ingroup Kernel_Module

 	@author Peter Knut, RadonLabs GmbH
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added pointer to parent list.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"


//=========================================================================
// Forward declarations
//=========================================================================

template <class Node> class List;


//=========================================================================
// ListNode
//=========================================================================

/**
	@class ListNode
	@ingroup Kernel_Module

	@brief Implements a node used with List template.

	Node have to be only ListNode derived class!
*/

template <class Node>
class ListNode
{
	friend class List<Node>;
	friend class ListNode<Node>;

//--- methods
public:
	ListNode(void *pdata = NULL);
	virtual ~ListNode();

	Node *GetNext() const;
	Node *GetPrev() const;
	List<Node> *GetParent() const;

	void SetData(void *data);
	void *GetData() const;

	bool IsLinked() const;

	void InsertBefore(Node *next);
	void InsertAfter(Node *prev);
	void Remove();


//--- variables
private:
	List<Node> *parent;
	Node *next;
	Node *prev;

	void *data;
};


//=========================================================================
// Methods
//=========================================================================

template <class Node>
ListNode<Node>::ListNode(void *pdata) :
	next(NULL),
	prev(NULL),
	data(pdata),
	parent(NULL)
{
	//
}


template <class Node>
ListNode<Node>::~ListNode()
{
	// assert if still in list
	Assert(!this->IsLinked());
}


template <class Node>
void ListNode<Node>::InsertBefore(Node *next)
{
	Assert(next && next->prev);
	Assert(!this->IsLinked());

	// link to list
	Node *prev = next->prev;

	this->next = next;
	this->prev = prev;
	prev->next = (Node *)this;
	next->prev = (Node *)this;

	// copy parent
	this->parent = next->parent;
}


template <class Node>
void ListNode<Node>::InsertAfter(Node *prev)
{
	Assert(prev && prev->next);
	Assert(!this->IsLinked());

	// link to list
	Node *next = prev->next;

	this->prev = prev;
	this->next = next;
	prev->next = (Node *)this;
	next->prev = (Node *)this;

	// copy parent
	this->parent = prev->parent;
}


template <class Node>
void ListNode<Node>::Remove(void)
{
	Assert(this->IsLinked());

	// unlink from list
	Node *next = this->next;
	Node *prev = this->prev;

	next->prev = prev;
	prev->next = next;
	this->next = NULL;
	this->prev = NULL;

	// reset parent
	this->parent = NULL;
}


/**
	Returns next object in the same hierarchy level, @c NULL if no next
	object exists.
*/
template <class Node>
Node *ListNode<Node>::GetNext() const
{
	Assert(this->IsLinked());
	return this->next->next ? this->next : NULL;
}


/**
	Returns previous object in the same hierarchy level, @c NULL if no
	previous object exists. 
*/
template <class Node>
Node *ListNode<Node>::GetPrev(void) const
{
	Assert(this->IsLinked());
	return this->prev->prev ? this->prev : NULL;
}


/**
	Returns the parent List object.
*/
template <class Node>
List<Node> *ListNode<Node>::GetParent() const
{
	return this->parent;
}


template <class Node>
bool ListNode<Node>::IsLinked(void) const
{
	return this->next != NULL || this->prev != NULL;
};


template <class Node>
void ListNode<Node>::SetData(void *data)
{
	this->data = data;
};


template <class Node>
void *ListNode<Node>::GetData(void) const
{
	return this->data;
};


//=========================================================================
// SListNode
//=========================================================================

/**
	@class SListNode
	@ingroup Kernel_Module

	@brief Simple list node (not template).
*/

class SListNode : public ListNode<SListNode>
{
	//
};


#endif  // __listnode_h__

// vim:ts=4:sw=4:
