//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file kernelserver.cpp
	@ingroup Kernel_Module

	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/threadserver.h"
#include "kernel/fileserver.h"
#include "kernel/serializeserver.h"

#include <time.h>

UseModule(Kernel);


//=========================================================================
// KernelServer
//=========================================================================

KernelServer::KernelServer() :
	Server<KernelServer>(),
	root(NULL),
	mutex(NULL),
	inited(false)
{
	// create dummy thread server
	NEW ThreadServer();
	Assert(ThreadServer::GetInstance());

	this->Init();
}


KernelServer::KernelServer(bool init) :
	Server<KernelServer>(),
	root(NULL),
	mutex(NULL),
	inited(false)
{
	// do not init kernel server now
	if (!init)
		return;

	// create dummy thread server
	NEW ThreadServer();
	Assert(ThreadServer::GetInstance());

	this->Init();
}


KernelServer::~KernelServer()
{
	if (this->inited)
		this->Done();
}


void KernelServer::Init()
{
	Assert(!this->inited);

	// create internal mutex
	this->mutex = ThreadServer::GetInstance()->CreateRecMutex();
	Assert(this->mutex);

	// create log server
	NEW LogServer();
	Assert(LogServer::GetInstance());

	OpenMemorySystem();

	// register kernel module
	this->RegisterModule(Kernel);

	// create root node
	this->root = (Root *)this->NewObject("Root", "Root");
	Assert(this->root);

	// set current directory
	this->SetCwd(NULL);

	// set app
#ifdef WINDOWS
	this->app_path = "c:";
#else
	this->app_path = "/";
#endif

	// create node for system variables
	this->New("Root", NOH_VARIABLES);

#if defined(USE_PROFILERS) && defined(DEBUG_MEMORY)
	// create system variables for memory system
	this->memory_variable = (Env *)this->New("Env", NOH_VARIABLES + "/# memory size");
	this->memory_variable->SetI((int)GetActMemorySize());

	this->blocks_variable = (Env *)this->New("Env", NOH_VARIABLES + "/# memory blocks");
	this->blocks_variable->SetI((int)GetActMemoryBlocks());
#endif

	// initializing of random generator
	srand((unsigned)time(NULL));

	this->inited = true;
}


void KernelServer::Done()
{
	Assert(this->inited);

	this->Lock();

	// delete root node
	root->Release();

	// delete class list
	// -----------------
	// Class objects must be released in inheritance order,
	// subclasses first, then parent class. Do multiple
	// runs over the class list and release classes
	// with a ref count of 0, until list is empty or
	// no classes with a ref count of 0 zero exists
	// (that's a fatal error, there are still objects
	// around of that class).
	bool is_empty;
	long zero_refs_count = 1;

	while ((!(is_empty = this->class_list.IsEmpty())) && (zero_refs_count > 0))
	{
		zero_refs_count = 0;
		Class *act_class = (Class*) this->class_list.GetFront();
		Class *next_class;

		do
		{
			next_class = (Class*) act_class->GetNext();

			if (!act_class->GetRefCount())
			{
				zero_refs_count++;
				act_class->Remove();
				delete act_class;
			}

			act_class = next_class;
		} while (act_class);
	}

	if (!is_empty)
	{
		LogError("Error cleaning up class list");
		LogInfo("Offending classes:");
		Class* act_class;

		for (act_class = (Class*) this->class_list.GetFront(); act_class; act_class = (Class*) act_class->GetNext())
			LogInfo2("%s: refcount = %d", act_class->GetName().c_str(), act_class->GetRefCount());

		ErrorMsg("Refcount errors occured during cleanup");
	}

	CloseMemorySystem();

	// delete servers
	delete LogServer::GetInstance();
	delete ThreadServer::GetInstance();

	this->Unlock();

	// delete mutex
	delete this->mutex;
	this->inited = false;
}


bool KernelServer::Trigger()
{
#if defined(USE_PROFILERS) && defined(DEBUG_MEMORY)
	this->memory_variable->SetI((int)GetActMemorySize());
	this->blocks_variable->SetI((int)GetActMemoryBlocks());
#endif

	return true;
}


void KernelServer::ProcessArgs(int argc, char* argv[])
{
	Assert(argc > 0);
	Assert(argv);

	DisableMemorySystem();
	FileServer::ExtractDirectory(string(argv[0]), this->app_path);
	if (this->app_path.empty())
		this->app_path = ".";

	FileServer::ExtractFile(string(argv[0]), this->app_name);
	EnableMemorySystem();
}


/**
	Add an extension class module to the kernel. The provided function
	should call KernelServer::RegisterClass() for each class in the module.
*/
void KernelServer::RegisterModule(void(*module)())
{
	module();
}


/**
    Add a new class to the class list. Normally called
    from the s_init_*() function of a class module.
*/
void KernelServer::RegisterClass(const string &name,
	bool (*init_func)(Class *, KernelServer *),
	Object *(*new_func)(const char *name))
{
	this->Lock();

	Class *cl = (Class *) this->class_list.Find(name);
	if (!cl)
	{
		cl = NEW Class(name, this, init_func, new_func);
		this->class_list.PushBack(cl);
	}

	this->Unlock();
}


/**
	Add a class object to the kernel.

	@param  super_class_name   Name of super class.
	@param  cl                 Class object to add.
*/
void KernelServer::AddClass(const char *super_class_name, Class *cl)
{
	Assert(super_class_name);
	Assert(cl);

	this->Lock();

	Class *super_class = this->FindClass(super_class_name);
	if (super_class)
		super_class->AddSubClass(cl);
	else
		ErrorMsg1("Could not open super class: %s", super_class_name);

	this->Unlock();
}


/**
	Remove class object from kernel.

	@param  cl  Pointer to class to be removed.
*/
void KernelServer::RemClass(Class *cl)
{
	Assert(cl);

	this->Lock();

	Class *super_class = cl->GetSuperClass();
	Assert(super_class);

	super_class->RemSubClass(cl);

	this->Unlock();
}


/**
	Return pointer to class object defined by class name. If the class
	is not loaded, 0 is returned.

	@param  class_name   Name of the class.
	@return              Pointer to class object or @c NULL.
*/
Class *KernelServer::FindClass(const char *class_name)
{
    Assert(class_name);

    this->Lock();
    Class *cl = (Class *)this->class_list.Find(class_name);
    this->Unlock();

    return cl;
}


/**
	Create a object given a class name and a path in the
	object hierarchy. This method will abort the app with
	a fatal error if the object couldn't be created.

	@param  class_name   Name of the object
	@param  path         Path where to create the new object in the hierarchy
	@param  fail         Whether application will be interrupted on error.
	@return              pointer to class object
*/
Root *KernelServer::New(const char *class_name, const string &path, bool fail)
{
	Assert(class_name && !path.empty());

	// extract name and path
	string parent_path;
	string name;
	str_pos_t pos;

	if ((pos = path.rfind('/')) != path.npos)
	{
		parent_path.assign(path, 0, pos + 1);
		name.assign(path, pos + 1, name.npos);
	}
	else
		name = path;

	// create object
	Root *obj = (Root*)this->NewObject(class_name, name.c_str(), fail);

	// add object to hierarchy
	this->AddNode(obj, parent_path);

	return obj;
}


/**
	Create a named object given a class name. This method will abort the 
	application with a fatal error if the object couldn't be created and
	fail parameter is set to @c true.

	@param  class_name   Class name of the object.
	@param  name         Name of the object.
	@param  fail         Whether application will be interrupted on error.
	@return              Pointer to new object.
*/
Object *KernelServer::NewObject(const char *class_name, const char *name, bool fail)
{
	Assert(class_name);

	Class *cl = this->FindClass(class_name);
	Object *obj = cl ? cl->NewObject(name) : NULL;

	if (!obj)
	{
		if (fail)
			ErrorMsg1("Can not create object of class: %s", class_name);
		else
			LogWarning1("Can not create object of class: %s", class_name);
	}

	return obj;
}


void KernelServer::AddNode(Root *node, const string &path)
{
	Assert(node);

	Root *last;

	this->Lock();

	// node will be added to current directory	
	if (path.empty())
		last = this->cwd.back();

	// check if whole path exists
	else
		last = this->CheckPath(path, true);

	// add node to the end
	last->PushBack(node);

	this->Unlock();
}


Root *KernelServer::CheckPath(const string &path, bool create)
{
	Assert(!path.empty());

	str_pos_t pos1, pos2;
	Root *node, *parent;
	string name;

	this->Lock();

	if (this->IsAbsolutePath(path))
	{
		parent = node = this->root;
		pos1 = 1;
	}
	else
	{
		parent = node = this->GetCwd();
		pos1 = 0;
	}

	do {
		if ((pos2 = path.find('/', pos1)) != path.npos)
		{
			Assert(pos2 > pos1);  // do not allow double '/'
			name.assign(path, pos1, pos2 - pos1);
		}
		else
			name.assign(path, pos1, name.npos);

		if (name.empty())
			break;

		node = parent->Find(name);

		if (!node)
		{
			if (create)
			{
				node = (Root *)this->NewObject("Root", name.c_str());
				parent->PushBack(node);
			}
			else
			{
				this->Unlock();
				return NULL;
			}
		}

		parent = node;
		pos1 = pos2 + 1;
	} while (pos2 != path.npos);

	this->Unlock();
	return node;
}

// vim:ts=4:sw=4:
