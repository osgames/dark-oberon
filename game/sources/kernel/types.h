#ifndef __types_h__
#define __types_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file types.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Linux compatibility for hash_map.
*/


//=========================================================================
// Includes
//=========================================================================

#ifdef WINDOWS
#	pragma warning(disable: 4786)   // disable warning: identifier was truncated to '255' characters in the debug information
#	pragma warning(disable: 4996)
#endif

#include <string>
#include <vector>
#include <list>

using std::string;
using std::vector;
using std::list;

#ifdef WINDOWS
#	include <hash_map>
using stdext::hash_map;
#else
#	include <ext/hash_map>
using __gnu_cxx::hash_map;
#endif


//=========================================================================
// Simple types
//=========================================================================

// NULL pointer
#ifndef NULL
#	define NULL (0L)
#endif


// signed shortcuts
typedef __int32 int32_t;
typedef __int64 int64_t;

// unsigned shortcuts
typedef unsigned char    byte_t;
typedef unsigned char    uchar_t;
typedef unsigned short   ushort_t;
typedef unsigned int     uint_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
typedef unsigned long    ulong_t;
typedef unsigned __int32 fourcc_t;


// STL shortcuts
typedef std::string::size_type str_size_t;
typedef std::string::size_type str_pos_t;


// platform depend methods
#ifdef WINDOWS
#	define snprintf _snprintf
#	define vsnprintf _vsnprintf
#endif

#endif  // __types_h__

// vim:ts=4:sw=4:
