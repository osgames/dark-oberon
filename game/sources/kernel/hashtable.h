#ifndef __HASHTABLE_H__
#define __HASHTABLE_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file hashtable.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/strlist.h"


//=========================================================================
// HashTable
//=========================================================================

/**
	@class HashTable
	@ingroup Kernel_Module

	@brief Implements a simple string Hash table.
*/

class HashTable 
{
public:
    HashTable(int size);
    ~HashTable();

    void Add(StrNode* n);
    StrNode *Find(const string &str) const;

private:
    int htable_size;
    StrList *htable;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Constructor.
*/
inline 
HashTable::HashTable(int size)
{
	this->htable_size = size;
	this->htable = NEW StrList[size];
}


/**
	Destructor.
*/
inline HashTable::~HashTable()
{
	delete[] this->htable;
}


/**
*/
static inline
int Hash(const char *str, int htable_size)
{
	int i = 0;
	int j = 1;
	char c;
	while ((c = *str++)) i += ((byte_t)c) * j++; 
	return (i % htable_size);
}


/**
	Adds an entry to the hashtable.
*/
inline 
void HashTable::Add(StrNode* n)
{
	int h_index = Hash(n->GetName().c_str(), this->htable_size);
	this->htable[h_index].PushFront(n);
}


/**
	Searches Hash table for entry.
*/
inline 
StrNode *HashTable::Find(const string &str) const
{
	int h_index = Hash(str.c_str(), this->htable_size);
	return this->htable[h_index].Find(str);
}


#endif  // __HASHTABLE_H__

// vim:ts=4:sw=4:
