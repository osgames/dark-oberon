#ifndef __counted_h__
#define __counted_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file counted.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Release method doesn't prevent to call AcquirePointer.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/mutex.h"
#include "kernel/threadserver.h"


//=========================================================================
// Counted
//=========================================================================

/**
	@class Counted
	@ingroup Kernel_Module

	@brief Implements counted pointers.

	Call AcquirePointer method to get counted pointer to this object. Deleting by delete operator is not
	allowed, use Release method instead. AcquirePointer can return @c NULL if object has been deleted from
	another thread in the same time.
	@n
	Object is not deleted from memory until number of pointers is zero. This is usefull if you want to
	be sure that your object still exists.
	@n
	This class is thread-safe execpt one case - the absotelly last deleted object. It must be deleted safetely
	from one thread and no other thread can acquire pointer simultaneously. This is provided by root node in
	KernelServer in our engine.

	Usage example:
	@code
Counted *p = new Counted();           // create object (first conted pointer is set)

Counted *cp1 = p->AcquirePointer();   // get next counted pointer
Counted *cp2 = p->AcquirePointer();   // get another counted opinter

p->Release();                         // object is not deleted from memory, only number of counted pointers is decreased

cp1->Release();                       // object still exists
cp2->Release();                       // now the object is deleted from memory
	@endcode
*/

class Counted
{
//--- methods
public:
	Counted();

	Counted *AcquirePointer();
	virtual bool Release();

protected:
	virtual ~Counted();

	static void LockCounted();
	static void UnlockCounted();

//--- variables
private:
	uint_t pointers_count;             ///< Number of pointers to this object.

	static uint_t instances_count;     ///< Number of all instances of Counted object.
	static Mutex *counted_mutex;       ///< Mutex for changing pointers count.
};


//=========================================================================
// Methods
//=========================================================================

inline
Counted::Counted() :
	pointers_count(1)
{
	Assert(ThreadServer::GetInstance());

	// create mutex for counted pointers
	if (!this->counted_mutex)
	{
		this->counted_mutex = ThreadServer::GetInstance()->CreateMutex();
		Assert(this->counted_mutex);
	}

	// increase number of instances
	this->LockCounted();
	this->instances_count++;
	this->UnlockCounted();
}


inline
Counted::~Counted()
{
	AssertMsg(!this->pointers_count, "delete operator was probably used instead of Release()");

	// decrease number of instances
	this->LockCounted();
	this->instances_count--;
	bool to_delete = this->instances_count == 0;
	this->UnlockCounted();

	// destroy mutex for counted pointers
	if (to_delete)
		SafeDelete(this->counted_mutex);
}


inline
bool Counted::Release()
{
	this->LockCounted();

	Assert(this->pointers_count > 0);
	this->pointers_count--;

	bool to_delete = this->pointers_count == 0;

	this->UnlockCounted();

	if (to_delete)
		delete this;

	return to_delete;
}


inline
Counted *Counted::AcquirePointer()
{
	this->LockCounted();

	if (!this->pointers_count)
	{
		this->UnlockCounted();
		return NULL;
	}

	this->pointers_count++;
	this->UnlockCounted();

	return this;
}


inline
void Counted::LockCounted()
{
	counted_mutex->Lock();
}


inline
void Counted::UnlockCounted()
{
	counted_mutex->Unlock();
}


#endif  // __counted_h__

// vim:ts=4:sw=4:
