#ifndef __HASHLIST_H__
#define __HASHLIST_H__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file hashlist.h
	@ingroup Kernel_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/hashtable.h"
#include "kernel/hashnode.h"


//=========================================================================
// HashList
//=========================================================================

/**
	@class HashList
	@ingroup Kernel_Module

	@brief A doubly linked list of named nodes with fast hashtable based search.
*/

class HashList : public List<HashNode> {
//--- variables
private:
	enum {
		DEFAULT_HASHSIZE = 16,
	};
	HashTable hash_table;

//--- methods
public:
	HashList();
	HashList(int hashsize);

	virtual void PushFront(HashNode* n);
	virtual void PushBack(HashNode* n);
	HashNode* PopFront();
	HashNode* PopBack();

	HashNode* Find(const string &name) const;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Default constructor.
*/
inline
HashList::HashList() :
	List<HashNode>(),
	hash_table(DEFAULT_HASHSIZE)
{
	// empty
}


/**
	Constructor with given hashtable size.
*/
inline
HashList::HashList(int hashsize) :
	List<HashNode>(),
	hash_table(hashsize)
{
	// empty
}


/**
	Adds node to beginning of list.
*/
inline
void HashList::PushFront(HashNode* n)
{
	Assert(n);

	n->SetHashTable(&(this->hash_table));

	this->hash_table.Add(&(n->str_node));
	List<HashNode>::PushFront(n);
}


/**
	Adds node to end of list.
*/
inline
void HashList::PushBack(HashNode *n)
{
	Assert(n);

	n->SetHashTable(&(this->hash_table));

	this->hash_table.Add(&(n->str_node));
	List<HashNode>::PushBack(n);
}


/**
	Removes first node.
*/
inline
HashNode *HashList::PopFront()
{
	return (HashNode *) List<HashNode>::PopFront();
}


/**
	Removes last node.
*/
inline
HashNode *HashList::PopBack()
{
	return (HashNode *) List<HashNode>::PopBack();
};


/**
	Searches node by name.
*/
inline
HashNode* HashList::Find(const string &name) const
{
	StrNode *sn = this->hash_table.Find(name);

	return sn ? (HashNode *) sn->GetData() : NULL;
}


#endif  // __HASHLIST_H__

// vim:ts=4:sw=4:
