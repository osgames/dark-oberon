//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file cmdproto.cpp
	@ingroup Kernel_Module

 	@author Andre Weissflog, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/cmdproto.h"
#include "kernel/cmd.h"


//=========================================================================
// ProtoDefInfo
//=========================================================================

/**
	ProtoDefInfo constructor which parses and checks validity of the prototype 
	definition. Initializes info resulting information from the parsing process.

	@param proto_def char array containing prototype definition to check & parse
*/
ProtoDefInfo::ProtoDefInfo(const char *proto_def)
{
	Assert(proto_def);

	const char * ptr;
	char c;

	// initialize all output strings & argument counters
	memset(this, 0, sizeof(ProtoDefInfo));

	// check out args
	ptr = proto_def;
	while ((0 != (c = *ptr++)) && ('_' != c)) {
		if (!Arg::IsVoid(c)) {
			if (Arg::IsValidArg(c))
				this->out_args[this->out_args_count++] = c;

			else
				return;
		}
	}
	this->out_args[this->out_args_count] = 0;

	// check if found separator '_'
	if ('_' != c) 
		return;

	// copy name
	int i = 0;
	while ((0 != (c = *ptr++)) && ('_' != c))
		this->name[i++] = c;

	this->name[i++] = 0;

	// check if found separator '_'
	if ('_' != c) 
		return;

	// copy out args if buffer provided
	while ((0 != (c = *ptr++)) && ('_' != c)) {
		if (!Arg::IsVoid(c)) {
			if (Arg::IsValidArg(c))
				this->in_args[this->in_args_count++] = c;

			else
				return;
		}
	}
	this->in_args[this->in_args_count] = 0;

	// check if found end of string
	if (0 != c) 
		return;

	valid = true;
}


//=========================================================================
// CmdProto
//=========================================================================

/**
	Constructor.
*/
CmdProto::CmdProto(const char *proto_def, fourcc_t id) :
	HashNode()
{
	// check prototype definition
	ProtoDefInfo info(proto_def);

	// parse proto definition and divided in out_args, name and in_args. @returns true if OK
	AssertMsg1(info.valid, "Invalid prototype definition for '%s'", proto_def);

	// copy prototype definition
	this->proto_def = proto_def;
	this->fourcc = id;
	this->in_args_count = info.in_args_count;
	this->out_args_count = info.out_args_count;

	// set cmd proto name in own hash node
	this->SetName(info.name);

	// create template cmd object
	this->cmd_template = NEW Cmd(this);

	// initialize template object in args
	int i;
	for (i = 0; i < this->in_args_count; i++)
		this->cmd_template->In()->Reset(info.in_args[i]);

	// initialize template object out args
	for (i = 0; i < this->out_args_count; i++)
		this->cmd_template->Out()->Reset(info.out_args[i]);

	this->cmd_template->Rewind();
	this->cmd_locked = false;
}


/**
	Copy constructor.
*/
CmdProto::CmdProto(const CmdProto& rhs) :
	HashNode(rhs.GetName())
{
	this->fourcc = rhs.fourcc;
	this->in_args_count = rhs.in_args_count;
	this->out_args_count = rhs.out_args_count;
	this->cmd_template = NEW Cmd(*(rhs.cmd_template));
	this->cmd_locked = false;
	this->proto_def = rhs.proto_def;
}


/**
	Destructor.
*/
CmdProto::~CmdProto()
{
	delete this->cmd_template;
}


/**
	Create a new cmd object from the internal template object. Optimization:
	normally just returns the template object (if nobody else uses it), instead
	of creating a new object.

	IMPORTANT: the CmdProto::RelCmd() method MUST be used to release the
	Cmd object!!!
*/
Cmd *CmdProto::NewCmd()
{
	if (this->cmd_locked) {
		// template object is locked, create a new object
		return NEW Cmd(*(this->cmd_template));
	}
	else {
		this->cmd_locked = true;
		this->cmd_template->Rewind();

		return this->cmd_template;
	}
}


/**
	Release a Cmd object created with CmdProto::NewCmd()
*/
void
CmdProto::RelCmd(Cmd* cmd)
{
	Assert(cmd);

	if (cmd != this->cmd_template) {
		// not the template object, release with s_delete
		delete cmd;
	}
	else {
		// this was the cached command template, just unlock it
		Assert(cmd_locked);
		cmd_locked = false;
	}
}


// vim:ts=4:sw=4:
