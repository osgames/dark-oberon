#ifndef __logserver_h__
#define __logserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file logserver.h
	@ingroup Kernel_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/system.h"
#include "kernel/server.h"
#include "kernel/mutex.h"
#include "kernel/logmacros.h"

#include <stdarg.h>

class File;


//=========================================================================
// LogServer
//=========================================================================

/**
	@class LogServer
	@ingroup Kernel_Module

	@brief This server provides logging messages.

*/

class LogServer : public Server<LogServer>
{
//--- embeded
public:
	enum LogLevel
	{
		LL_DEBUG    = (1 << 0),
		LL_INFO     = (1 << 1),
		LL_WARNING  = (1 << 2),
		LL_ERROR    = (1 << 3),
		LL_CRITICAL = (1 << 4),
		LL_ALL_STD  = LL_DEBUG | LL_INFO,
		LL_ALL_ERR  = LL_WARNING | LL_ERROR | LL_CRITICAL,
		LL_ALL		= LL_DEBUG | LL_INFO | LL_WARNING | LL_ERROR | LL_CRITICAL
	};

	typedef void LogFunction(LogLevel, const string &, int, const string &);

private:
	struct LogEntry
	{
		enum Type
		{
			LT_FILE,
			LT_FUNC
		};

		union
		{
			struct
			{
				File *file;
				bool close;
				bool standart;
			};
			LogFunction *func;
		};
		Type type;
		int levels;

		LogEntry(File *file, bool close, bool standart, int levels);
		LogEntry(LogFunction *func, int levels);
	};

	typedef vector<LogEntry> EntryArray;
	typedef EntryArray::iterator EntryArrayIter;


//--- variables
private:
	EntryArray entries;
	Mutex *mutex;

//--- methods
public:
	LogServer();
	virtual ~LogServer();

	bool RegisterFile(File *file, int levels, bool close = false);
	bool RegisterFile(const string &file_path, int levels);
	bool RegisterFunction(LogFunction *func, int levels);
	void UnregisterAll();

	void WriteInfo(const char *file, int line, const char *message, ...);
	void WriteWarning(const char *file, int line, const char *message, ...);
	void WriteError(const char *file, int line, const char *message, ...);
	void WriteCritical(const char *file, int line, const char *message, ...);
	void WriteDebug(const char *file, int line, const char *message, ...);

private:
	void Lock() const;
	void Unlock() const;

	void WriteMessage(LogLevel level, const char *file, int line, const char *message, va_list args);
};


//=========================================================================
// Methods
//=========================================================================

inline
void LogServer::Lock() const
{
	this->mutex->Lock();
}


inline
void LogServer::Unlock() const
{
	this->mutex->Unlock();
}


inline
void LogServer::WriteInfo(const char *file, int line, const char *message, ...)
{
	va_list arg;
	va_start(arg, message);

	this->WriteMessage(LL_INFO, file, line, message, arg);

	va_end(arg);
}


inline
void LogServer::WriteWarning(const char *file, int line, const char *message, ...)
{
	va_list arg;
	va_start(arg, message);

	this->WriteMessage(LL_WARNING, file, line, message, arg);

	va_end(arg);
}


inline
void LogServer::WriteError(const char *file, int line, const char *message, ...)
{
	va_list arg;
	va_start(arg, message);

	this->WriteMessage(LL_ERROR, file, line, message, arg);

	va_end(arg);
}


inline
void LogServer::WriteCritical(const char *file, int line, const char *message, ...)
{
	va_list arg;
	va_start(arg, message);

	this->WriteMessage(LL_CRITICAL, file, line, message, arg);

	va_end(arg);
}


inline
void LogServer::WriteDebug(const char *file, int line, const char *message, ...)
{
	va_list arg;
	va_start(arg, message);

	this->WriteMessage(LL_DEBUG, file, line, message, arg);

	va_end(arg);
}


inline
LogServer::LogEntry::LogEntry(File *file, bool close, bool standart, int levels)
{
	this->type = LT_FILE;
	this->levels = levels;
	this->file = file;
	this->close = close;
	this->standart = standart;
}


inline
LogServer::LogEntry::LogEntry(LogFunction *func, int levels)
{
	this->type = LT_FUNC;
	this->levels = levels;
	this->func = func;
}


#endif  // __logserver_h__

// vim:ts=4:sw=4:
