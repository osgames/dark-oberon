//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openalmodule.cpp
    @ingroup OpenAL_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"


//=========================================================================
// OpenALModule
//=========================================================================

extern "C" void OpenALModule();


extern Object *s_NewOpenALAudioServer (const char *name);
extern bool s_InitOpenALAudioServer (Class *, KernelServer *);
extern Object *s_NewOpenALSound (const char *name);
extern bool s_InitOpenALSound (Class *, KernelServer *);
extern Object *s_NewOpenALSoundResource (const char *name);
extern bool s_InitOpenALSoundResource (Class *, KernelServer *);


void OpenALModule()
{
	KernelServer::GetInstance()->RegisterClass("OpenALAudioServer", s_InitOpenALAudioServer, s_NewOpenALAudioServer);
	KernelServer::GetInstance()->RegisterClass("OpenALSound", s_InitOpenALSound, s_NewOpenALSound);
	KernelServer::GetInstance()->RegisterClass("OpenALSoundResource", s_InitOpenALSoundResource, s_NewOpenALSoundResource);
}

// vim:ts=4:sw=4:
