//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openallistener.cpp
    @ingroup OpenAL_Module

 	@author Bang, Chang Kyu, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "openalmodule/openallistener.h"


//=========================================================================
// OpenALListener
//=========================================================================

/**
	Constructor.
*/
inline
OpenALListener::OpenALListener()
{
	// empty
}


/**
	Destructor.
*/
inline
OpenALListener::~OpenALListener()
{
	// empty
}


void OpenALListener::Update()
{
	if (this->move_dirty) {
		const vector3 &position = this->transform.pos_component();
		ALfloat orientation[6];
		orientation[0] = -this->transform.M31; // "at" vector
		orientation[1] = -this->transform.M32;
		orientation[2] = -this->transform.M33;
		orientation[3] = this->transform.M21;  // "up" vector
		orientation[4] = this->transform.M22;
		orientation[5] = this->transform.M23;

		alListener3f(AL_POSITION, position.x, position.y, position.z);
		alListener3f(AL_VELOCITY, this->velocity.x, this->velocity.y, this->velocity.z);
		alListenerfv(AL_ORIENTATION, orientation);

		this->move_dirty = false;
	}
}


// vim:ts=4:sw=4:
