#ifndef __openalsound_h__
#define __openalsound_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openalsound.h
    @ingroup OpenAL_Module

 	@author Bang, Chang Kyu, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/ref.h"
#include "framework/sound.h"

#include <al/al.h>

class OpenALAudioServer;
class OpenALSoundResource;


//=========================================================================
// OpenALSound
//=========================================================================

/**
    @class OpenALSound
    @ingroup OpenAL_Module
*/
class OpenALSound: public Sound {
//--- variables
protected:
	Ref<OpenALAudioServer> al_audio_server;
	Ref<OpenALSoundResource> al_sound_resource;

	ALuint source;

//--- methods
public:
	OpenALSound(const char *id);
	virtual ~OpenALSound();

	virtual bool IsPlaying();
	virtual ulong_t GetDataSize();

protected:
	virtual void Play();
	virtual void Stop();
	virtual void Pause();
	virtual void Update();
	virtual void UpdateStream();

	virtual bool LoadResource();
	virtual void UnloadResource();
};


#endif // __openalsound_h__

// vim:ts=4:sw=4:
