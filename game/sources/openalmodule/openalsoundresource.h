#ifndef __openalsoundresource_h__
#define __openalsoundresource_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openalsoundresource.h
    @ingroup OpenAL_Module

 	@author Bang, Chang Kyu, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/ref.h"
#include "framework/soundresource.h"

#include <al/al.h>

class OpenALAudioServer;
class OpenALSound;


//=========================================================================
// OpenALSoundResource
//=========================================================================

/**
    @class OpenALSoundResource
    @ingroup OpenAL_Module
*/

class OpenALSoundResource : public SoundResource {
//--- variables
protected:
	Ref<OpenALAudioServer> al_audio_server;
	ALuint buffers[2];
	byte_t front_buffer;
	byte_t back_buffer;
	ALenum format;
	ALsizei bitrate;

//--- methods
public:
	OpenALSoundResource(const char *id);
	virtual ~OpenALSoundResource();

	virtual ulong_t GetDataSize();
	virtual bool UpdateStream();
	virtual void RewindStream();

	const ALuint *GetFrontBuffer();
	const ALuint *GetBackBuffer();
	const ALuint *GetBuffers();

protected:
	virtual bool LoadResource();
	virtual void UnloadResource();
};


//=========================================================================
// Methods
//=========================================================================

inline
const ALuint *OpenALSoundResource::GetFrontBuffer()
{
	return this->buffers + this->front_buffer;
}


inline
const ALuint *OpenALSoundResource::GetBackBuffer()
{
	return this->buffers + this->back_buffer;
}


inline
const ALuint *OpenALSoundResource::GetBuffers()
{
	return this->buffers;
}


#endif // __openalsoundresource_h__

// vim:ts=4:sw=4:
