//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openalaudioserver.cpp
    @ingroup OpenAL_Module

 	@author Bang, Chang Kyu, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "openalmodule/openalaudioserver.h"
#include "framework/resourceserver.h"
#include "kernel/logserver.h"
#include "kernel/kernelserver.h"

PrepareScriptClass(OpenALAudioServer, "AudioServer", s_NewOpenALAudioServer, s_InitOpenALAudioServer, s_InitOpenALAudioServer_cmds);


//=========================================================================
// MSVC 6.0 hack
//=========================================================================

#ifdef WINDOWS
#	if (_MSC_VER < 1300)
extern "C" long _ftol( double ); //defined by VC6 C libs
extern "C" long _ftol2( double dblSource ) { return _ftol( dblSource ); }
#	endif
#endif


//=========================================================================
// OpenALAudioServer
//=========================================================================

/**
    Constructor.
*/
OpenALAudioServer::OpenALAudioServer(const char *id) :
	AudioServer(id),
	device(0),
	context(0)
{
	// get all available devices
	int i;
	char *devices = (char *)alcGetString(NULL, ALC_DEVICE_SPECIFIER);
	char *dev = devices;

	// we have some devices
	if (dev && *dev) {
		for (i = 0; ; i++) {
			if (!devices[i]) {
				this->device_list.push_back(string(dev));
				if (devices[i + 1])
					dev = devices + i + 1;
				else
					break;
			}
		}
	}
}


/**
	Destructor.  
*/
OpenALAudioServer::~OpenALAudioServer()
{
	//
}


/**
    Open the audio server.
*/
bool OpenALAudioServer::OpenDevice()
{
	if (!AudioServer::OpenDevice())
		return false;

	this->device_opened = false;

	// open defined device
	if (!this->device_name.empty()) {
		this->device = alcOpenDevice(this->device_name.c_str());
		if (!this->device) {
			LogWarning1("Error opening sound device: %s", this->device_name.c_str());
			this->check_alc_error();
		}
	}

	// open default device
	// This function can cause crashes on old HW. :(
	// Using device_name = "Generic Hardware" can avoid this.
	if (!this->device)
		this->device = alcOpenDevice(NULL);

	if (!this->device) {
		LogError("Error opening default sound device");
		this->check_alc_error();
		return false;
	}

	// save device name
	this->device_name = (char *)alcGetString(this->device, ALC_DEVICE_SPECIFIER);

	// create context
	this->context = alcCreateContext(this->device, NULL);
	if (!this->context) {
		this->check_alc_error();
		return false;
	}

	alcMakeContextCurrent(this->context);
	if (this->check_al_error())
		return false;

	// set default listener properties
	ALfloat listener_pos[] = {0.0, 0.0, 0.0};
	ALfloat listener_vel[] = {0.0, 0.0, 0.0};
	ALfloat listener_ori[] = {0.0, 0.0, -1.0, 0.0, 1.0, 0.0};  // listener facing into the screen

	alListenerfv(AL_POSITION, listener_pos);
	alListenerfv(AL_VELOCITY, listener_vel);
	alListenerfv(AL_ORIENTATION, listener_ori);

	this->volume_dirty = this->props_dirty = true;
	this->device_opened = true;
	this->WriteInfo();

	return true;
}


/**
	Close the audio server.
*/
void OpenALAudioServer::CloseDevice()
{
	AudioServer::CloseDevice();

	// destroy context
	alcMakeContextCurrent(NULL);
	alcDestroyContext(this->context);

	// close device
	alcCloseDevice(this->device);
}


/**
	Writes basic audio information to log file.
*/
void OpenALAudioServer::WriteInfo()
{
	LogInfo("--- OpenAL info ---");
	LogInfo1("Version:     %s", (char *)alGetString(AL_VERSION));
	LogInfo1("Vendor:      %s", (char *)alGetString(AL_VENDOR));
	LogInfo1("Renderer:    %s", (char *)alGetString(AL_RENDERER));
	//LogInfo1("Extensions:  %s", (char *)alGetString(AL_EXTENSIONS));

	LogInfo("--- ALC info ---");
	LogInfo1("Device:      %s", (char *)alcGetString(this->device, ALC_DEVICE_SPECIFIER));
	//LogInfo1("Def. device: %s", (char *)alcGetString(this->device, ALC_DEFAULT_DEVICE_SPECIFIER));
	//LogInfo1("Extensions:  %s", (char *)alcGetString(this->device, ALC_EXTENSIONS));
}


/**
	Creates a shared sound resource object.
*/
SoundResource *OpenALAudioServer::NewSoundResource(const string &name)
{
	return (SoundResource*)this->resource_server->NewResource("OpenALSoundResource", name, Resource::RES_SOUND_RESOURCE);
}


/**
	Creates a non-shared sound object.
*/
Sound *OpenALAudioServer::NewSound(const string &name)
{
    // create non-shared resource (name is empty)
	Sound *sound = (Sound *)this->resource_server->NewResource("OpenALSound", string(), Resource::RES_SOUND);
	sound->SetFileName(name);

	return sound;
}


/**
*/
bool OpenALAudioServer::Trigger()
{
	if (!this->device_opened)
		return false;

	// set master volume
	if (this->volume_dirty) {
		if (this->muted)
			alListenerf(AL_GAIN, 0.0f);
		else
			alListenerf(AL_GAIN, this->master_volume);
	}

	// set static audio properties
	if (this->props_dirty) {
		switch (this->distance_model) {
		case DM_NONE:             alDistanceModel(AL_NONE); break;
		case DM_INVERSE:          alDistanceModel(AL_INVERSE_DISTANCE); break;
		case DM_INVERSE_CLAMPED:  alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED); break;
		case DM_LINEAR:           alDistanceModel(AL_LINEAR_DISTANCE); break;
		case DM_LINEAR_CLAMPED:   alDistanceModel(AL_LINEAR_DISTANCE_CLAMPED); break;
		case DM_EXPONENT:         alDistanceModel(AL_EXPONENT_DISTANCE); break;
		case DM_EXPONENT_CLAMPED: alDistanceModel(AL_EXPONENT_DISTANCE_CLAMPED); break;
		default:                  break;
		}

		alDopplerFactor(this->doppler_factor);
		alSpeedOfSound(this->speed_of_sound);
		this->props_dirty = false;
	}

	return AudioServer::Trigger();
}


//=========================================================================
// Error checking
//=========================================================================

bool OpenALAudioServer::check_al_error()
{
	ALenum ErrCode;
	string err_msg = "OpenAL error: ";

	if ((ErrCode = alGetError()) != AL_NO_ERROR) {
		err_msg += (char*)alGetString(ErrCode);
		LogError(err_msg.c_str());

		return true;
	}

	return false;
}


bool OpenALAudioServer::check_alc_error()
{
	ALenum ErrCode;
	string err_msg = "ALC error: ";

	if ((ErrCode = alcGetError(device)) != ALC_NO_ERROR) {
		err_msg += (char*)alcGetString(device, ErrCode);
		LogError(err_msg.c_str());

		return true;
	}

	return false;
}


// vim:ts=4:sw=4:
