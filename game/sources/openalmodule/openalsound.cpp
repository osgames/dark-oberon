//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openalsound.cpp
    @ingroup OpenAL_Module

 	@author Bang, Chang Kyu, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"
#include "kernel/logserver.h"

#include "openalmodule/openalsound.h"
#include "openalmodule/openalsoundresource.h"
#include "openalmodule/openalaudioserver.h"

PrepareClass(OpenALSound, "Sound", s_NewOpenALSound, s_InitOpenALSound);


//=========================================================================
// OpenAlSound
//=========================================================================

/**
	Constructor.
*/
OpenALSound::OpenALSound(const char *id) :
	Sound(id),
    source(-1)
{
	this->al_audio_server = (OpenALAudioServer *)AudioServer::GetInstance();
}


/**
	Destructor.
*/
OpenALSound::~OpenALSound()
{
	// empty
}


/**
	Creates a shared sound resource, and initializes it.
*/
bool OpenALSound::LoadResource()
{
	Assert(!this->loaded);
	Assert(!this->al_sound_resource.IsValid());

	// compute resource name
	string res_name = this->GetFileName();
	static int stream_counter = 0;

	if (this->streaming)
	{
		char buff[30];
		sprintf(buff, "_stream%d", stream_counter++);
		res_name += buff;
	}


	// create a sound resource object
	OpenALSoundResource *snd_res = (OpenALSoundResource *)this->al_audio_server->NewSoundResource(res_name);
	Assert(snd_res);

	// if sound resource not opened yet, do it
	if (!snd_res->IsLoaded())
	{
		snd_res->SetFileName(this->GetFileName());
		snd_res->SetStreaming(this->IsStreaming());
		snd_res->SetLooping(this->IsLooping());

		if (!snd_res->Load())
		{
			snd_res->Release();
			return false;
		}
	}

	// create source
	alGetError();
	alGenSources(1, &this->source);
	if (this->al_audio_server->check_al_error())
	{
		snd_res->Release();
		return false;
	}

	// set source properties
	if (this->streaming)
		alSourceQueueBuffers(this->source, 2, snd_res->GetBuffers());
	else
		alSourcei(this->source, AL_BUFFER, *snd_res->GetFrontBuffer());

	this->volume_dirty = this->move_dirty = this->props_dirty = true;
	this->al_sound_resource = snd_res;

	SoundResource *a = snd_res;
	

	this->loaded = true;
	this->Update();

	return true;
}


/**
	Unloads the sound resource object.
*/
void OpenALSound::UnloadResource()
{
	Assert(this->loaded);
	Assert(this->al_sound_resource.IsValid());

	// delete source-buffer(s) link(s)
	alSourcei(this->source, AL_BUFFER, 0);

	// delete source
	alDeleteSources(1, &this->source);
	this->source = 0;

	// delete sound resource
	this->al_sound_resource->Release();
	this->al_sound_resource = NULL;

	this->loaded = false;
}


/**
*/
void OpenALSound::Play()
{
	if (!this->loaded)
		return;

	if (this->streaming && this->IsPlaying())
		this->Stop();

	alSourcePlay(this->source);
}


/**
*/
void OpenALSound::Stop()
{
	if (!this->loaded)
		return;

	alSourceStop(this->source);

	if (this->streaming)
	{
		alSourcei(this->source, AL_BUFFER, 0);
		this->al_sound_resource->RewindStream();
		alSourceQueueBuffers(this->source, 2, this->al_sound_resource->GetBuffers());
	}
}


/**
	Pause the sound.
*/
void OpenALSound::Pause()
{
	if (!this->loaded)
		return;

	alSourcePause(this->source);
}


/**
*/
void OpenALSound::Update()
{
	if (!this->loaded)
		return;

	// update volume
	if (this->volume_dirty || this->al_audio_server->IsVolumeDirty())
	{
		alSourcef(this->source, AL_GAIN, this->volume * this->al_audio_server->GetCategoryVolume(this->category));
		this->volume_dirty = false;
	}

	// update movement
	if (this->move_dirty)
	{
		const vector3 &position = this->transform.pos_component();
		const vector3 &direction = this->transform.z_component();

		alSource3f(this->source, AL_POSITION, position.x, position.y, position.z);
		alSource3f(this->source, AL_DIRECTION, -direction.x, -direction.y, -direction.z);
		alSource3f(this->source, AL_VELOCITY, this->velocity.x, this->velocity.y, this->velocity.z);
		this->move_dirty = false;
	}

	// update static properties
	if (this->props_dirty)
	{
		if (this->streaming)
			this->al_sound_resource->SetLooping(this->looping);
		else
			alSourcei(this->source, AL_LOOPING, this->looping);

		alSourcef(this->source, AL_REFERENCE_DISTANCE, this->ref_distance);
		alSourcef(this->source, AL_MAX_DISTANCE, this->max_distance);
		alSourcef(this->source, AL_MIN_GAIN, this->min_volume);
		alSourcef(this->source, AL_MAX_GAIN, this->max_volume);
		alSourcef(this->source, AL_PITCH, this->pitch);
		alSourcef(this->source, AL_ROLLOFF_FACTOR, this->rolloff_factor);
		alSourcef(this->source, AL_CONE_INNER_ANGLE, this->inner_cone_angle);
		alSourcef(this->source, AL_CONE_OUTER_ANGLE, this->outer_cone_angle);
		alSourcef(this->source, AL_CONE_OUTER_GAIN, this->outer_cone_volume);
		this->props_dirty = false;
	}
}


void OpenALSound::UpdateStream()
{
	Assert(this->streaming && this->loaded);

	ALint processed;
	alGetSourcei(this->source, AL_BUFFERS_PROCESSED, &processed);

	if (processed)
	{
		ALuint buff[1];
		alSourceUnqueueBuffers(this->source, 1, buff);

		if (this->al_sound_resource->UpdateStream())
			alSourceQueueBuffers(this->source, 1, al_sound_resource->GetBackBuffer());
	}
}


/**
	Returns @c true if sound is playing.
*/
bool OpenALSound::IsPlaying()
{
	if (!this->loaded)
		return false;

	ALint i;
	alGetSourcei(this->source, AL_SOURCE_STATE, &i);

	return i == AL_PLAYING;
}


ulong_t OpenALSound::GetDataSize()
{
	// OpenAL sound does not contain any data
	return 0;
}


// vim:ts=4:sw=4:
