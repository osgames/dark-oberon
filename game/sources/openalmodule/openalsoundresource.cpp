//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openalsoundresource.cpp
    @ingroup OpenAL_Module

 	@author Bang, Chang Kyu, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "openalmodule/openalsoundresource.h"
#include "openalmodule/openalaudioserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"

PrepareClass(OpenALSoundResource, "SoundResource", s_NewOpenALSoundResource, s_InitOpenALSoundResource);


//=========================================================================
// OpenALSoundResource
//=========================================================================

/**
*/
OpenALSoundResource::OpenALSoundResource(const char *id) :
	SoundResource(id),
	front_buffer(0),
	back_buffer(1)
{
	buffers[front_buffer] = 0;
	buffers[back_buffer] = 0;

	this->al_audio_server = (OpenALAudioServer *)AudioServer::GetInstance();
}


/**
*/
OpenALSoundResource::~OpenALSoundResource()
{
	this->Unload();
}


/**
	Load the sound resource.
*/
bool OpenALSoundResource::LoadResource()
{
	Assert(!this->loaded);

	if (!this->al_audio_server->IsDeviceOpened())
		return false;

	if (this->data_file)
		this->audio_file = (AudioFile *)this->data_file;
	else
	{
		Assert(!this->file_name.empty());
		this->audio_file = this->al_audio_server->NewAudioFile(this->file_name, true);
		if (!this->audio_file)
			return false;
	}

	// check audio format
	AudioFile::Format &af_format = this->audio_file->GetFormat();

	if (af_format.channels == 1)
		this->format = af_format.bits == 8 ? this->format = AL_FORMAT_MONO8 : this->format = AL_FORMAT_MONO16;

	else if (af_format.channels == 2)
		this->format = af_format.bits == 8 ? this->format = AL_FORMAT_STEREO8 : this->format = AL_FORMAT_STEREO16;

	else
	{
		LogError1("Unsupported number of audio channels: %u", af_format.channels);
		SafeDelete(this->audio_file);
		return false;
	}
	this->bitrate = af_format.bitrate;

	// load audio data
	if (this->streaming)
	{
		// create buffers
		alGetError();
		alGenBuffers(2, this->buffers);

		// load front block
		if (this->audio_file->LoadTimeBlock(AUDIO_STREAM_TIME_BLOCK, this->looping) < 0)
		{
			LogError1("Error loading audio file: %s", this->file_name.c_str());
			SafeDelete(this->audio_file);
			return false;
		}

		alBufferData(
			this->buffers[this->front_buffer],
			this->format,
			(void *)this->audio_file->GetData(),
			(ALsizei)this->audio_file->GetDataSize(),
			(ALsizei)af_format.bitrate
		);

		if (this->al_audio_server->check_al_error())
		{
			SafeDelete(this->audio_file);
			return false;
		}

		// load back block
		if (this->audio_file->LoadTimeBlock(AUDIO_STREAM_TIME_BLOCK, this->looping) < 0)
		{
			LogError1("Error loading audio file: %s", this->file_name.c_str());
			SafeDelete(this->audio_file);
			return false;
		}

		alBufferData(
			this->buffers[this->back_buffer],
			this->format,
			(void *)this->audio_file->GetData(),
			(ALsizei)this->audio_file->GetDataSize(),
			(ALsizei)af_format.bitrate
		);

		if (this->al_audio_server->check_al_error())
		{
			SafeDelete(this->audio_file);
			return false;
		}
	}

	else
	{
		// create one buffer
		alGetError();
		alGenBuffers(1, this->buffers);

		if (!this->data_file && this->audio_file->LoadData() < 0)
		{
			LogError1("Error loading audio file: %s", this->file_name.c_str());
			SafeDelete(this->audio_file);
			return false;
		}		

		alBufferData(
			this->buffers[this->front_buffer],
			this->format,
			(void *)this->audio_file->GetData(),
			(ALsizei)this->audio_file->GetDataSize(),
			(ALsizei)af_format.bitrate
		);

		if (this->al_audio_server->check_al_error())
		{
			SafeDelete(this->audio_file);
			return false;
		}

		// we dont need audio file if not streaming
		SafeDelete(this->audio_file);
	}

	this->data_file = NULL;
	this->loaded = true;

	return true;
}


/**
	Unload everything.
*/
void OpenALSoundResource::UnloadResource()
{
	Assert(this->loaded);

	if (this->streaming)
	{
		// delete buffers
		alDeleteBuffers(2, this->buffers);

		// delete audio file if streaming
		delete this->audio_file;
		this->audio_file = NULL;
	}
	else
		// delete one buffer
		alDeleteBuffers(1, this->buffers);

	this->buffers[0] = 0;
	this->buffers[1] = 0;

	this->loaded = false;
}


ulong_t OpenALSoundResource::GetDataSize()
{
	ALint total_size;
	alGetBufferi(this->buffers[this->front_buffer], AL_SIZE, &total_size);

	if (this->buffers[this->back_buffer])
	{
		ALint size;
		alGetBufferi(this->buffers[this->back_buffer], AL_SIZE, &size);
		total_size += size;
	}

	return (ulong_t)total_size;
}


bool OpenALSoundResource::UpdateStream()
{
	Assert(this->streaming);

	if (this->audio_file->TestState(AudioFile::LS_LOADED))
		return false;

	if (this->audio_file->LoadTimeBlock(AUDIO_STREAM_TIME_BLOCK, this->looping) < 0)
	{
		LogError("Error reading audio stream");
		return false;
	}

	this->front_buffer = !this->front_buffer;
	this->back_buffer = !this->back_buffer;

	alBufferData(
		this->buffers[this->back_buffer],
		this->format,
		(void *)this->audio_file->GetData(),
		(ALsizei)this->audio_file->GetDataSize(),
		this->bitrate
	);
	
	return true;
}


void OpenALSoundResource::RewindStream()
{
	Assert(this->streaming);

	this->audio_file->Rewind();

	this->front_buffer = 0;
	this->back_buffer = 1;

	// load front block
	if (this->audio_file->LoadTimeBlock(AUDIO_STREAM_TIME_BLOCK, this->looping) < 0)
	{
		LogError("Error loading audio file");
		delete this->audio_file;
		this->audio_file = NULL;
		return;
	}

	alBufferData(
		this->buffers[this->front_buffer],
		this->format,
		(void *)this->audio_file->GetData(),
		(ALsizei)this->audio_file->GetDataSize(),
		this->bitrate
	);

	if (this->al_audio_server->check_al_error())
	{
		delete this->audio_file;
		this->audio_file = NULL;
		return;
	}

	// load back block
	if (this->audio_file->LoadTimeBlock(AUDIO_STREAM_TIME_BLOCK, this->looping) < 0)
	{
		LogError("Error loading audio file");
		delete this->audio_file;
		this->audio_file = NULL;
		return;
	}

	alBufferData(
		this->buffers[this->back_buffer],
		this->format,
		(void *)this->audio_file->GetData(),
		(ALsizei)this->audio_file->GetDataSize(),
		this->bitrate
	);

	if (this->al_audio_server->check_al_error())
	{
		delete this->audio_file;
		this->audio_file = NULL;
		return;
	}
}


// vim:ts=4:sw=4:
