#ifndef __openalaudioserver_h__
#define __openalaudioserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openalaudioserver.h
    @ingroup OpenAL_Module

 	@author Bang, Chang Kyu, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/audioserver.h"
#include "openalmodule/openalsound.h"

#include <al/al.h>
#include <al/alc.h>


//=========================================================================
// OpenALAudioServer
//=========================================================================

/**
    @class OpenALAudioServer
    @ingroup OpenAL_Module

    @brief A sound server based on OpenAL.
*/
class OpenALAudioServer : public AudioServer {
//--- variables
protected:
	ALCdevice  *device;
	ALCcontext *context;

//--- methods
public:
	OpenALAudioServer(const char *id);
	virtual ~OpenALAudioServer();

	virtual Sound* NewSound(const string &name);
	virtual SoundResource* NewSoundResource(const string &name);

	virtual bool OpenDevice();
	virtual void CloseDevice();
	virtual void WriteInfo();
	virtual bool Trigger();

	// error handling
	bool check_alc_error();
	bool check_al_error();
};


#endif // __openalaudioserver_h__

// vim:ts=4:sw=4:
