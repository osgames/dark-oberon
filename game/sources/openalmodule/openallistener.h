#ifndef __openallistener_h__
#define __openallistener_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file openallistener.h
    @ingroup OpenAL_Module

 	@author Bang, Chang Kyu, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/listener.h"

#include <al/al.h>


//=========================================================================
// OpenALListener
//=========================================================================

/**
	@class OpenALListener
	@ingroup Audio3

	Define listener properties for audio subsystem.
*/

class OpenALListener : public Listener {
//--- methods
public:
	OpenALListener();
	virtual ~OpenALListener();

protected:
	virtual void Update();
};


#endif // __openallistener_h__

// vim:ts=4:sw=4:
