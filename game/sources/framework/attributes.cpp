//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file attributes.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/attributes.h"
#include "kernel/kernelserver.h"
#include "kernel/env.h"

PrepareScriptClass(Attributes, "Root", s_NewAttributes, s_InitAttributes, s_InitAttributes_cmds);


//=========================================================================
// SessionServer
//=========================================================================


Attributes::Attributes(const char *id) :
	Root(id),
	dirty(false)
{
	//
}


Attributes::~Attributes()
{
	//
}


void Attributes::SetAttribute(const char *name, const char *value)
{
	Env *var = (Env *)this->Find(string(name));
	if (var)
	{
		var->SetS(value);
		this->dirty = true;
		return;
	}

	this->kernel_server->PushCwd(this);
	var = (Env *)this->kernel_server->New("Env", string(name));
	this->kernel_server->PopCwd();

	var->SetS(value);
	this->dirty = true;
}


/**
	Gets an attribute value by name. Returns NULL if attribute doesn't exist.

	@param  name    An attribute name.
	@return         String value, or NULL;
*/
const char *Attributes::GetAttribute(const char *name)
{
	Env *var = (Env *)this->Find(string(name));
	return var ? var->GetS() : NULL;
}


// vim:ts=4:sw=4:
