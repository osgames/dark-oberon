//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file imagefile.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/imagefile.h"
#include "kernel/logserver.h"
#include "kernel/vector.h"


//=========================================================================
// ImageFile
//=========================================================================

/**
	Constructor. Sets file mode to binary.
*/
ImageFile::ImageFile() :
	DataFile(),
	format(IF_UNKNOWN),
	data(NULL),
	width(0),
	height(0),
	use_free(false)
{
	this->binary = true;
}


/**
	Deletes data. This data can be set only by Set method in this class.
*/
ImageFile::~ImageFile()
{
	this->DeleteData();
}


ulong_t ImageFile::LoadData()
{
	ErrorMsg("Overwrite this method in subclass");
	return 0;
}

/**
	Deletes loaded data.
*/
void ImageFile::DeleteData()
{
	// delete data
	if (this->data)
	{
		if (this->use_free)
			free(this->data);
		else
			delete[] this->data;

		this->data = NULL;
	}
	
	this->data_size = 0;
	this->use_free = false;
}


/**
	Sets new image data. Old data will be deleted.

	@param data   Memory buffer with image data.
	@param size   Size of memory buffer.
	@param format Format of image data stored in buffer.
	@param width  Image width.
	@param height Image height.
*/
void ImageFile::SetData(byte_t *data, ulong_t size, Format format, ushort_t width, ushort_t height)
{
	Assert(data);
	Assert(format != IF_UNKNOWN);

	// delete old data
	this->DeleteData();

	// set new data
	this->data = data;
	this->data_size = size;
	this->use_free = false;
	
	this->format = format;
	this->width = width;
	this->height = height;

	// set state to loaded
	this->state = LS_LOADED;
}


/**
	Copys new image data from given memory buffer. Old data will be deleted.

	@param data   Memory buffer with image data.
	@param size   Size of memory buffer.
	@param format Format of image data stored in buffer.
	@param width  Image width.
	@param height Image height.
*/
void ImageFile::CopyData(const byte_t *data, ulong_t size, Format format, ushort_t width, ushort_t height)
{
	Assert(data);
	Assert(format != IF_UNKNOWN);

	// delete old data
	this->DeleteData();

	// create buffer
	this->data = NEW byte_t[size];
	Assert(this->data);

	// copy new data
	memcpy(this->data, data, size);
	this->data_size = size;
	this->use_free = false;
	
	this->format = format;
	this->width = width;
	this->height = height;

	// set state to loaded
	this->state = LS_LOADED;
}


/**
	Copys new image data from given ImageFile. Old data will be deleted.

	@param image   ImageFile object.
*/
void ImageFile::CopyData(ImageFile *image)
{
	Assert(image);

	// load overlay data
	if (!image->TestState(LS_LOADED))
		image->LoadData();

	// delete old data
	this->DeleteData();

	// create buffer
	this->data = NEW byte_t[image->GetDataSize()];
	Assert(this->data);

	// copy new data
	memcpy(this->data, image->GetData(), image->GetDataSize());
	this->use_free = false;

	this->data_size = image->GetDataSize();
	this->format = image->GetFormat();
	this->width = image->GetWidth();
	this->height = image->GetHeight();

	// set state to loaded
	this->state = LS_LOADED;
}


/**
	Adds overlay image to current image data.

	@param overlay_image   ImageFile object with overlay image.
	@param overlay_color  Color used for coloring overlay image.

	@return @c True if successful.
*/
bool ImageFile::AddOverlay(ImageFile *overlay_image, const vector3 &overlay_color)
{
	Assert(overlay_image);

	// check format of base image
	if (!(this->TestFormat(IF_RGB) || this->TestFormat(IF_RGBA))) {
		LogError("Wrong format of base image");
		return false;
	}

	// load overlay data
	if (!overlay_image->TestState(LS_LOADED))
		overlay_image->LoadData();

	// check size and format of overlay image
	if (overlay_image->GetWidth() != this->width ||
		overlay_image->GetHeight() != this->height ||
		!overlay_image->TestFormat(IF_RGB)
	) {
		LogError("Wrong size or format of overlay image");
		return false;
	}

	byte_t *overlay_data = overlay_image->GetData();
	Assert(overlay_data);
	int bpp = this->GetBytesPerPixel(this->format);

	ulong_t i, j;
	float gray, coef, coef_2, alpha, alpha_1;

	// prepare colors
	vector3 overlay_color256 = overlay_color;
	overlay_color256.saturate();
	overlay_color256 *= 255.0f;
	vector3 inverse_color = vector3(255.0f, 255.0f, 255.0f) - overlay_color256;
	vector3 masked_color;

	// add overlay image
	for (i = 0, j = 0; i < this->data_size; i += bpp, j += 3) {
		// get only pixel with alpha values > 0
		if (overlay_data[j]) {

			// trasform original color to grayscale
			gray = this->data[i] * 0.299f + this->data[i + 1] * 0.587f + this->data[i + 2] * 0.114f;
			coef = gray / 255.0f;
			coef_2 = 2 * coef;

			// get alpha from overlay mask
			alpha = overlay_data[j] / 255.0f;
			alpha_1 = 1.0f - alpha;

			// interpolate new color
			masked_color = coef <= 0.5f ? overlay_color256 * coef_2 : overlay_color256 + inverse_color * (coef_2 - 1.0f);

			// combine grayscale color with new one
			this->data[i]     = byte_t(gray * alpha_1 + masked_color.x * alpha);
			this->data[i + 1] = byte_t(gray * alpha_1 + masked_color.y * alpha);
			this->data[i + 2] = byte_t(gray * alpha_1 + masked_color.z * alpha);
		}
	}

	return true;
}


/**
	Resample current image data to new size.

	@param new_width   New width of image.
	@param new_height  New height of image.
*/
void ImageFile::Resample(ushort_t new_width, ushort_t new_height)
{
	Assert(new_width && new_height);

	// allocate new data
	byte_t bpp = this->GetBytesPerPixel(this->format);
	ulong_t new_data_size = new_width * new_height * bpp;
	byte_t *new_data = NEW byte_t[new_data_size];

	// resample image
	float x_coef = float(this->width) / new_width;
	float y_coef = float(this->height) / new_height;
	ulong_t pos, new_pos;
	byte_t b;

	for (ushort_t y = 0; y < new_height; y++)
	{
		for (ushort_t x = 0; x < new_width; x++)
		{
			pos = (ushort_t(y * y_coef) * this->width + ushort_t(x * x_coef)) * bpp;
			new_pos = (y * new_width + x) * bpp;

			for (b = 0; b < bpp; b++)
				new_data[new_pos++] = this->data[pos++];
		}
	}

	// delete old data
	if (this->use_free)
		free(this->data);
	else
		delete[] this->data;

	// set new data
	this->data = new_data;
	this->width = new_width;
	this->height = new_height;
	this->data_size = new_data_size;
	this->use_free = false;
}


// vim:ts=4:sw=4:
