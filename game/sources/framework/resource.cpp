//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file resource.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Added SerializedResource class.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/resource.h"
#include "framework/resourceserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/serializeserver.h"

PrepareClass(Resource, "Root", s_NewResource, s_InitResource);


//=========================================================================
// Resource
//=========================================================================

/**
	Constructor.
*/
Resource::Resource(const char *id) :
	Root(id),
	type(RES_INVALID),
	loaded(false),
	data_file(NULL)
{
	static uint_t id_counter = 0;

	Assert(ResourceServer::GetInstance());
	res_server = ResourceServer::GetInstance();
	this->unique_id = id_counter++;
}


/**
	Destructor.
*/
Resource::~Resource()
{
	Assert(!this->loaded);
}


/**
	Requests to load a resource. Invoke the LoadResource() method
	which should be overriden by subclasses to implement the actual loading.

	Pozn. Load() je oddelene od LoadResource() aby bolo mozne urobit nacitavanie
	na pozadi. V takomto pripade Load() iba prida job a nic viac. Ked sa job dostane
	na rad, zavola sa LoadResource().

	@return     @c True if all ok.
*/
bool Resource::Load()
{
	// if we are already loaded, do nothing (we still may be
	// in in-operational state, either Lost, or Restored)
	if (this->loaded)
		return true;

	// the synchronous case is simply
	bool ok = this->LoadResource();

	if (!ok)
	{
		if (this->file_name.empty())
			LogError("Error loading resource memory buffer");
		else
			LogError1("Error loading resource from file: %s", this->file_name.c_str());
	}

	return ok;
}


/**
	Unload the resource data, freeing runtime resources. This method will call
	the protected virtual UnloadResources() method which should be overriden
	by subclasses.
*/
void Resource::Unload()
{
	if (this->loaded)
		this->UnloadResource();
}


/**
	Subclasses must override this method and implement the actual
	resource loading code here. The method may get called from a thread
	(if the class returns true in the CanLoadAsync() method and the
	resource object works in async mode).
*/
bool Resource::LoadResource()
{
	Assert(!this->loaded);

	ErrorMsg("Overwrite this method in subclass");

	this->loaded = true;
	return true;
}


/**
	Subclasses must override this method and implement the resource unloading.
	This method will always run in the main thread.
*/
void Resource::UnloadResource()
{
	Assert(!this->loaded);

	ErrorMsg("Overwrite this method in subclass");

	this->loaded = false;
}


/**
	Return an estimated byte size of the resource data. This is only
	used for statistics.
*/
ulong_t Resource::GetDataSize()
{
	ErrorMsg("Overwrite this method in subclass");
	return 0;
}


//=========================================================================
// SerializedResource
//=========================================================================

/**
	Load the resource using SerializeServer.
*/
bool SerializedResource::LoadResource()
{
	Assert(!this->loaded);
	Assert(SerializeServer::GetInstance());

	if (!SerializeServer::GetInstance()->Deserialize(this->file_name, this))
	{
		LogError1("Error loading resource from file: %s", this->file_name.c_str());
		return false;
	}

	this->loaded = true;
	return true;
}


/**
	Clears data and sets state to unloaded.
*/
void SerializedResource::UnloadResource()
{
	Assert(this->loaded);

	this->Clear();
	this->loaded = false;
}


/**
	Clears all resource data.
*/
void SerializedResource::Clear()
{
	ErrorMsg("Overwrite this method in subclass");
}


// vim:ts=4:sw=4:
