//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionservertext_cmds.cpp
	@ingroup Framework_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2009

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sessionservercontext.h"
#include "framework/attributes.h"
#include "kernel/kernelserver.h"

PrepareScriptClass(SessionServerContext, "Root", s_NewSessionServerContext, s_InitSessionServerContext, s_InitSessionServerContext_cmds);


SessionServerContext::SessionServerContext(const char *id) :
	Root(id),
	channel_id(0),
	client_id(0),
	keep_alive_time(0.0)
{
	// create attributes node
	this->kernel_server->PushCwd(this);
	this->attributes = (Attributes *)this->kernel_server->New("Attributes", "attributes");
	this->kernel_server->PopCwd();

	Assert(this->attributes);
}


/**
*/
SessionServerContext::~SessionServerContext()
{
	//
}


// vim:ts=4:sw=4:
