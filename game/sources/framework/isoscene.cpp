//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file isoscene.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/isoscene.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/profiler.h"

PrepareClass(IsoScene, "Scene", s_NewIsoScene, s_InitIsoScene);


//=========================================================================
// IsoScene
//=========================================================================

/**
	Constructor.
*/
IsoScene::IsoScene(const char *id) :
	Scene(id),
	h_coef(20.0f),
	v_coef(10.0f)
{
	//
}


/**
	Destructor.
*/
IsoScene::~IsoScene()
{
	//
}


bool IsoScene::IsCloserThen(SceneObject *object1, SceneObject *object2)
{
	Assert(object1);
	Assert(object2);

	const vector3 &pos1 = object1->GetPosition();
	const vector3 &pos2 = object2->GetPosition();
	const vector2 &size1 = object1->GetSize();
	const vector2 &size2 = object2->GetSize();

	if (pos1.z < pos2.z)
		return false;

	else if (pos1.z > pos2.z)
		return true;

	else if ((pos1.x < pos2.x + size2.x) && (pos1.y < pos2.y + size2.y))
		return (pos1.x + size1.x <= pos2.x) || (pos1.y + size1.y <= pos2.y);

	else if ((pos2.x < pos1.x + size1.x) && (pos2.y < pos1.y + size1.y))
		return false;

	else if (pos2.x > pos1.x)
		return (pos1.x + pos1.y + size1.x) < (pos2.x + pos2.y + size2.y);

	else
		return (pos1.x + pos1.y + size1.y) < (pos2.x + pos2.y + size2.x);
}


void IsoScene::SortObjects()
{
	#ifdef DEBUG_SORTING
	int counter_test = 0;
	int counter = 0;
	#endif

	if (this->objects_count < 2)
		return;   // nothing to sort

	#ifdef USE_PROFILERS
	this->sorting_profiler->Start();
	#endif

	Root *p;
	Root *next_unit;

	Root *left_unit = this->objects->GetFront();
	Root *next_left_unit;

	uint_t left = 0;
	uint_t next_left;

	uint_t right = this->objects_count - 1;
	uint_t next_right;

	uint_t i, j;

	for (i = 0; i < this->objects_count; i++)
	{
		next_right = 0;
		next_left = this->objects_count - 1;
		next_left_unit = this->objects->GetBack();

		for (j = left, p = left_unit; j < right; j++)
		{
			#ifdef DEBUG_SORTING
			counter_test++;
			#endif

			if (this->IsCloserThen((SceneObject *)p->GetNext(), (SceneObject *)p))
			{
				#ifdef DEBUG_SORTING
				counter++;
				#endif

				next_unit = p->GetNext();

				// move unit
				p->Remove();
				p->InsertAfter(next_unit);

				next_right = j;
				if (j - 1 < next_left)
				{
					if (!j)
					{
						next_left = j;
						next_left_unit = next_unit;
					}
					else
					{
						next_left = j - 1;
						next_left_unit = next_unit->GetPrev();
					}
				}
			} // if IsCloserThen()
			else
				p = p->GetNext();
		} // for j

		if (next_right <= next_left) break;
		right = next_right;
		left = next_left;
		left_unit = next_left_unit;

	} // for i

	#ifdef USE_PROFILERS
	this->sorting_profiler->Stop();
	#endif

	#ifdef DEBUG_SORTING
	if (counter)
		LogDebug3("object: %d, tests: %d, sort: %d", this->objects_count, counter_test, counter);
	#endif
}


void IsoScene::CalculatePosition(const vector3 &object_position, vector3 &proj_position)
{
	proj_position.x = (object_position.x - object_position.y) * this->h_coef;
	proj_position.y = (object_position.x + object_position.y) * this->v_coef;
	proj_position.z = object_position.z;
}


// vim:ts=4:sw=4:
