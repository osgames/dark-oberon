#ifndef __isoscene_h__
#define __isoscene_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file isoscene.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/scene.h"


//=========================================================================
// IsoScene
//=========================================================================

/**
	@class IsoScene
	@ingroup Framework_Module
*/

class IsoScene : public Scene
{
//--- variables
private:
	float h_coef;
	float v_coef;

//--- methods
public:
	IsoScene(const char *id);
	virtual ~IsoScene();

	void SetCoefs(float h_coef, float v_coef);

protected:
	virtual void SortObjects();
	virtual void CalculatePosition(const vector3 &object_position, vector3 &proj_position);

	bool IsCloserThen(SceneObject *object1, SceneObject *object2);
};


//=========================================================================
// Methods
//=========================================================================

inline
void IsoScene::SetCoefs(float h_coef, float v_coef)
{
	this->h_coef = h_coef;
	this->v_coef = v_coef;
}


#endif  // __isoscene_h__

// vim:ts=4:sw=4:
