//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionserver.cpp
	@ingroup Framework_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2008

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sessionserver.h"
#include "framework/attributes.h"
#include "kernel/kernelserver.h"
#include "kernel/timeserver.h"
#include "kernel/ipcserver.h"
//#include "framework/netserver.h"
//#include "framework/netclient.h"

PrepareScriptClass(SessionServer, "IpcServer", s_NewSessionServer, s_InitSessionServer, s_InitSessionServer_cmds);


//=========================================================================
// SessionServer
//=========================================================================

SessionServer::SessionServer(const char *id) :
	IpcServer(id),
    max_clients_count(0),
	clients_count(0),
    opened(false),
    broadcast_time(0.0)
{
	AssertMsg(TimeServer::GetInstance(), "Create TimeServer before SessionServer");

	// create storage nodes
	this->kernel_server->PushCwd(this);
	this->attributes = (Attributes *)this->kernel_server->New("Attributes", "attributes");
	this->client_contexts = this->kernel_server->New("Root", "clients");
	this->kernel_server->PopCwd();

	Assert(this->attributes && this->client_contexts);
}


SessionServer::~SessionServer()
{
	if (this->opened)
		this->Close();
}


/**
	Open the session server. This makes the session visible in the network.
	After opening the session, the server must be triggered frequently
	to process messages (i.e. once per frame). An opened server will
	generally answer any requests from clients and potential clients.
*/
bool SessionServer::Open()
{
	Assert(!this->opened);
	Assert(this->GetAppName());
	Assert(this->GetAppVersion());
	Assert(this->GetMaxClientsCount() > 0);

	string port_name;
	IpcAddress address;

	LogInfo("Starting session server");

	// initialize the various addresses
	port_name = this->GetAppName();
	port_name += "Session";
	this->session_address.SetHostName("self");
	this->session_address.SetPortName(port_name.c_str());

	// initialie broadcast port
	port_name = this->GetAppName();
	port_name += "Broadcast";
	address.SetPortName(port_name.c_str());
	this->SetBroadcastPort(address.GetPortNum());
	LogInfo1("Broadcasting session messages on port: %u", address.GetPortNum());

	// create an unique identifier for this session
	this->session_guid.Generate();

	// initialize server attributes
	this->UpdateNumPlayersAttr();
	this->UpdateServerClientAttrs();

	// start listening on TCP
	if (!this->StartListening(this->session_address.GetPortNum(), IpcChannel::PROTOCOL_TCP))
		return false;

	return this->opened = true;
}


/**
	Close the session server. This removes the session from the network.
*/
void SessionServer::Close()
{
	Assert(this->opened);

	LogInfo("Closing session server");

	// stop listening
	this->StopListening(IpcChannel::PROTOCOL_TCP);

	// release client context objects
	this->client_contexts->ReleaseChildren();
	Assert(this->client_contexts->IsEmpty());

	// send a proper close message to all connected clients before we disappear
	this->SendAll(IpcBuffer("~closesession"), IpcChannel::PROTOCOL_TCP);

	this->opened = false;
}


void SessionServer::ProcessData(uint_t channel_id, const IpcBuffer &buffer)
{
	Assert(this->opened);

	static char command[4096];
	const char *command_name;

	// one received message could contain several strings
	const char *message;
	for (message = buffer.GetFirstString(); message; message = buffer.GetNextString())
	{
		LogDebug1("Session message received: %s", message);

		strcpy(command, message);
		command_name = strtok(command, " ");
		if (!command_name)
			continue;

		if (!strcmp(command_name, "~queryserverattrs"))
			this->SendServerAttrs(channel_id);

		else if (!strcmp(command_name, "~joinsession"))
			this->ProcessJoinSessionRequest(channel_id);

		else if (!strcmp(command_name, "~leavesession"))
			this->ProcessLeaveSessionRequest(channel_id);

		else if (!strcmp(command_name, "~clientattr"))
		{
			const char *name = strtok(NULL, " ");
			const char *value = strtok(NULL, "[]");
			this->ProcessClientAttribute(channel_id, name, value);
		}
	}
}


bool SessionServer::Trigger()
{
	Assert(this->opened);

	// periodically send a broadcast with server data
	this->BroadcastIdentity();

	// kick clients above MaxNumClients
	if (this->clients_count > this->max_clients_count)
	{
		list<SessionClientContext *> over_clients;
		list<SessionClientContext *>::iterator it;

		ushort_t over_count = this->max_clients_count - this->clients_count;
		SessionClientContext *client_context = (SessionClientContext *)this->client_contexts->GetBack();
		for (; client_context && over_count > 0; client_context = (SessionClientContext *)client_context->GetPrev(), over_count--)
			over_clients.push_back(client_context);

		for (it = over_clients.begin(); it != over_clients.end(); it++)
			this->KickClient(*it);
	}

	// remove session which have broken IPC connection
	this->CleanupDeadSessions();

	// if server attributes dirty, send them to
	// all connected clients
	if (this->attributes->IsDirty())
		this->SendServerAttrs(ALL_CLIENTS);

	return IpcServer::Trigger();
}


/**
	Checks session contexts for dead sessions due to timeout. Normally,
	session servers will correctly close the connection as part of the
	network protocol, if this fails however, a timeout will make sure
	that dead session contexts don't pile up.
*/
void SessionServer::CleanupDeadSessions()
{
	SessionClientContext *cur  = (SessionClientContext *)this->client_contexts->GetFront();
	SessionClientContext *next = 0;

	while (cur) {
		next = (SessionClientContext *)cur->GetNext();

		if (!this->ExistsChannel(cur->GetIpcChannelId()))
		{
			LogInfo1("Session dead from client: %u", cur->GetClientId());
			cur->Release();
			this->clients_count--;

			this->UpdateNumPlayersAttr();
			this->UpdateServerClientAttrs();
		}

		cur = next;
	};
}



/**
	Kick a joined client from the session.
*/
bool SessionServer::KickClient(const string &client_id)
{
	SessionClientContext *client_context = (SessionClientContext *)this->client_contexts->Find(client_id);
	return client_context ? this->KickClient(client_context) : false;
}


/**
	Kick a joined client from the session.
*/
bool SessionServer::KickClient(SessionClientContext *client_context)
{
	Assert(this->clients_count > 0);

	this->Send(client_context->GetIpcChannelId(), IpcBuffer("~kick"));

	client_context->Release();
	this->clients_count--;

	this->UpdateNumPlayersAttr();
	this->UpdateServerClientAttrs();

	return true;
}


/**
	Start the session. This will send the "~start" message
	to all joined clients, configure and opened the
	game network server object.
*/
bool SessionServer::Start()
{/*
	// build the port name for the game server
	char gamePortName[1024];
	sprintf(gamePortName, "%sGame", this->GetAppName());

	// build the start message string
	char messageBuf[1024];
	sprintf(messageBuf, "~start %s %s", this->session_address.GetIpAddrString(), gamePortName);
	IpcBuffer startmessage(messageBuf);

	// send start signal to all joined clients
	int numClients = this->GetNumClients();
	int index;
	for (index = 0; index < numClients; index++)
	{
		SessionClientContext *client_context = this->GetClientAt(index);
		this->Send(client_context->GetIpcChannelId(), startmessage);
	}

	// configure and opened the game net server
	NetServer *netServer = this->net_server.get();
	NetClient *netClient = this->net_client.get();

	// configure the local client
	Guid localGuid;
	localGuid.Generate();
	netClient->SetGuid(localGuid.Get());
	netClient->SetServerHostName("localhost");
	netClient->SetServerPortName(gamePortName);

	// configure the local server
	netServer->SetPortName(gamePortName);
	netServer->BeginClients(numClients + 1);

	// note: the first game client is always the local client
	netServer->SetGuid(0, localGuid.Get());
	netServer->SetClientPlayerName(0, this->GetServerAttr("PlayerName"));

	// configure the remote clients
	for (index = 0; index < numClients; index++)
	{
		SessionClientContext *client_context = this->GetClientAt(index);
		Assert(client_context);
		netServer->SetGuid(index + 1, client_context->GetGuid());
		netServer->SetClientPlayerName(index + 1, client_context->GetClientAttr("PlayerName"));
	}
	netServer->EndClients();

	// opened the net server and the local client
	netServer->Open();
	netClient->Open();
*/
	return true;
}


/**
	This broadcasts a server info message into the LAN (once per second
	or so). Interested clients use this info to connect to potential hosts
	in order to receive more detailed infos about the session in the
	form of server attributes.
*/
void SessionServer::BroadcastIdentity()
{
	double time_shift = TimeServer::GetInstance()->GetRealTime() - this->broadcast_time;
	if (time_shift > 1.0)
	{
		this->broadcast_time += time_shift;

		// build the broadcast message
		char message[512];
		sprintf(message, "~session %s %s %s %s %u",
				this->session_guid.Get(),
				this->GetAppName(),
				this->GetAppVersion(),
				this->session_address.GetIpAddrString(),
				this->session_address.GetPortNum());

		// send the broadcast message
		IpcBuffer buffer(message, strlen(message) + 1);

		this->SendBroadcast(buffer);
	}
}


/**
	Find a client context by the client guid.
*/
SessionClientContext *SessionServer::FindClientContext(uint_t channel_id)
{
	Assert(channel_id);

	SessionClientContext *client_context = (SessionClientContext *)this->client_contexts->GetFront();
	for (; client_context; client_context = (SessionClientContext *)client_context->GetNext())
	{
		if (client_context->GetIpcChannelId() == channel_id)
			return client_context;
	}

	return NULL;
}


/**
	Send server attributes to one or all connected clients. Either provide
	the client id, or the "ALL_CLIENTS" value as argument.
*/
void SessionServer::SendServerAttrs(uint_t channel_id)
{
	char buffer[1024];

	Env *attr;
	for (attr = (Env *)this->attributes->GetFront(); attr; attr = (Env *)attr->GetNext())
	{
		snprintf(buffer, sizeof(buffer), "~serverattr %s [%s]", attr->GetID().c_str(), attr->GetS());

		if (channel_id == ALL_CLIENTS)
		{
			if (!this->client_contexts->IsEmpty())
			{
				LogDebug2("Sending session attribute to all clients: %s=%s", attr->GetID().c_str(), attr->GetS());
				this->SendAll(buffer, IpcChannel::PROTOCOL_TCP);
			}
		}
		else
		{
			LogDebug3("Sending session attribute to channel %u: %s=%s", channel_id, attr->GetID().c_str(), attr->GetS());
			this->Send(channel_id, buffer);
		}
	}
}


/**
	This updates the MaxNumPlayers server attribute.
*/
void SessionServer::UpdateMaxNumPlayersAttr()
{
	char value[64];
	sprintf(value, "%u", this->max_clients_count + 1);
	this->attributes->SetAttribute("MaxPlayersCount", value);
}


/**
	This updates the NumPlayers server attribute. Must be called when
	the number of joined clients changes.
*/
void SessionServer::UpdateNumPlayersAttr()
{
	char value[64];
	sprintf(value, "%u", this->clients_count + 1);
	this->attributes->SetAttribute("PlayersCount", value);
}


/**
	Handle a join session request. This checks first if the join can be
	accepted (the only reason for not accepting is a full session). If the
	join is accepted, a new session client context will be created.
*/
void SessionServer::ProcessJoinSessionRequest(uint_t channel_id)
{
	LogInfo1("Joining session by client from channel: %u", channel_id);

	if (this->clients_count >= this->max_clients_count)
	{
		// deny the request
		this->Send(channel_id, IpcBuffer("~joindenied"));
		LogInfo("Joining session denied");
	}
	else
	{
		static uint_t counter = 1;

		// create a session client context
		char text[1024];
		sprintf(text, "Client_%u", counter);

		this->kernel_server->PushCwd(this->client_contexts);
		SessionClientContext *client_context = (SessionClientContext *)this->kernel_server->New("SessionClientContext", string(text));
		this->kernel_server->PopCwd();
		this->clients_count++;

		// initialize it
		client_context->SetClientId(counter);
		client_context->SetIpcChannelId(channel_id);

		// accept the request
		sprintf(text, "~joinaccepted %u", counter);
		this->Send(channel_id, IpcBuffer(text));
		LogInfo1("Joining session accepted, client id: %u", counter);

		// update the num players server attribute
		this->UpdateNumPlayersAttr();
		this->UpdateServerClientAttrs();

		counter++;
	}
}


/**
	Handle a leave session request. This removes the session identified by
	the provided client guid.
*/
void SessionServer::ProcessLeaveSessionRequest(uint_t channel_id)
{
	Assert(channel_id);

	SessionClientContext *client_context = this->FindClientContext(channel_id);
	if (!client_context)
		return;

	LogInfo1("Leaving session by client: %u", client_context->GetClientId());
	client_context->Release();

	this->UpdateNumPlayersAttr();
	this->UpdateServerClientAttrs();
}


/**
	Handle a client attribute update.
*/
void SessionServer::ProcessClientAttribute(uint_t channel_id, const char *name, const char *value)
{
	Assert(channel_id && name && value);

	SessionClientContext *client_context = this->FindClientContext(channel_id);
	if (!client_context)
		return;

	client_context->GetAttributes()->SetAttribute(name, value);

	// convert the client attributes into a server attribute
	this->UpdateServerClientAttrs();
}


/**
	Convert client attributes into server attributes. Client attributes
	are redistributed to all clients as server attributes with a ClientX_
	prefix.
*/
void SessionServer::UpdateServerClientAttrs()
{
	string name;

	// delete all client attributes
	Env *attr, *next;
	string attr_name;

	attr = (Env *)this->attributes->GetFront();
	while (attr)
	{
		next = (Env *)attr->GetNext();

		attr_name = attr->GetID().substr(0, 7);
		if (attr_name == "Client_")
			attr->Release();

		attr = next;
	}

	// for each joined client...
	SessionClientContext *client_context = (SessionClientContext *)this->client_contexts->GetFront();
	for (; client_context; client_context = (SessionClientContext *)client_context->GetNext())
	{
		// set the Status attribute
		name = client_context->GetID() + "_Status";
		this->attributes->SetAttribute(name.c_str(), "joined");

		// update the attributes for this client
		Env *attribute = (Env *)client_context->GetAttributes()->GetFront();
		for (; attribute; attribute = (Env *)attribute->GetNext())
		{
			name = client_context->GetID() + "_" + attribute->GetID();
			this->attributes->SetAttribute(name.c_str(), attribute->GetS());
		}
	}
}


// vim:ts=4:sw=4:
