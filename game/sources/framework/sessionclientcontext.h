#ifndef __sessionclientcontext_h__
#define __sessionclientcontext_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================


/**
	@file sessionclientcontext.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2008

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/root.h"
#include "kernel/ref.h"
#include "kernel/guid.h"

class Attributes;


//=========================================================================
// SessionClientContext
//=========================================================================

/**
	@class SessionClientContext
	@ingroup Network

	@brief Represents a session client on the server side.

	The session server object creates one SessionClientContext object
	for each connected client. The session client context contains
	all information about the client for the server to know.
*/

class SessionClientContext : public Root
{
//--- methods
public:
	SessionClientContext(const char *id);
	virtual ~SessionClientContext();

	void SetIpcChannelId(uint_t channel_id);
	uint_t GetIpcChannelId() const;

	void SetClientId(uint_t client_id);
	uint_t GetClientId() const;

	Attributes *GetAttributes() const;


//--- variables
private:
	uint_t channel_id;
	uint_t client_id;

	Ref<Attributes> attributes;
};


//=========================================================================
// Methods
//=========================================================================

inline
void SessionClientContext::SetIpcChannelId(uint_t channel_id)
{
	Assert(channel_id);
	this->channel_id = channel_id;
}


inline
uint_t SessionClientContext::GetIpcChannelId() const
{
	return this->channel_id;
}


inline
void SessionClientContext::SetClientId(uint_t client_id)
{
	Assert(client_id);
	this->client_id = client_id;
}


inline
uint_t SessionClientContext::GetClientId() const
{
	return this->client_id;
}


inline
Attributes *SessionClientContext::GetAttributes() const
{
	return this->attributes;
}


#endif

// vim:ts=4:sw=4:
