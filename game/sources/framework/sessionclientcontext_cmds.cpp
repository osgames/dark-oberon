//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionclientcontext_cmds.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sessionclientcontext.h"


//=========================================================================
// Commands
//=========================================================================

static void s_GetAttributes(void *slf, Cmd *cmd)
{
	SessionClientContext *self = (SessionClientContext *)slf;
	cmd->Out()->SetO(self->GetAttributes());
}


void s_InitSessionClientContext_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("o_GetAttributes_v",    'GATS', s_GetAttributes);

	cl->EndCmds();
}


// vim:ts=4:sw=4:
