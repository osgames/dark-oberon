//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file shapenode.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/shapenode.h"
#include "framework/texture.h"
#include "framework/mesh.h"
#include "framework/animation.h"
#include "framework/gfxserver.h"
#include "framework/sceneserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/timeserver.h"

PrepareClass(ShapeNode, "SceneNode", s_NewShapeNode, s_InitShapeNode);

#ifdef USE_PROFILERS
Ref<Env> ShapeNode::shapes_variable;
Ref<Env> ShapeNode::triangles_variable;
ulong_t ShapeNode::last_frame = 0;
#endif


//=========================================================================
// ShapeNode
//=========================================================================

/**
	Constructor.
*/
ShapeNode::ShapeNode(const char *id) :
	SceneNode(id),
	mapping(NULL),
	mapping_buff(NULL),
	coords_buff(NULL),
	colors_buff(NULL)
{
#ifdef USE_PROFILERS
	// create system variables
	if (!this->shapes_variable.IsValid())
		this->shapes_variable = (Env *)kernel_server->New("Env", NOH_VARIABLES + "/# shapes");
	if (!this->triangles_variable.IsValid())
		this->triangles_variable = (Env *)kernel_server->New("Env", NOH_VARIABLES + "/# triangles");

	this->shapes_variable->SetI(0);
	this->triangles_variable->SetI(0);
#endif
}


/**
	Destructor.
*/
ShapeNode::~ShapeNode()
{
	this->UnloadResources();

	SafeDeleteArray(this->mapping_buff);
	SafeDeleteArray(this->coords_buff);
	SafeDeleteArray(this->colors_buff);
}


void ShapeNode::SetTextureFile(const string &texture_file)
{
	Assert(!texture_file.empty());

	this->UnloadTexture();
	this->texture_file = texture_file;
}


void ShapeNode::SetOverlayFile(const string &overlay_file)
{
	this->UnloadTexture();
	this->overlay_file = overlay_file;
}


void ShapeNode::SetMeshFile(const string &mesh_file, vector2 *mapping, bool absolute)
{
	Assert(!mesh_file.empty());

	this->UnloadMesh();
	this->mesh_file = mesh_file;
	this->mapping = mapping;
	this->absolute = absolute;
}


void ShapeNode::SetMappingAnimation(const string &mapping_file)
{
	this->mapping_file = mapping_file;
}


void ShapeNode::SetCoordsAnimation(const string &coords_file)
{
	this->coords_file = coords_file;
}


void ShapeNode::SetColorsAnimation(const string &colors_file)
{
	this->colors_file = colors_file;
}


bool ShapeNode::LoadTexture()
{
	if (this->texture_file.empty())
		return true;

	if (this->texture.IsValid())
		return true;

	Texture *texture = GfxServer::GetInstance()->NewTexture(this->texture_file, this->overlay_file, this->overlay_color);
	if (!texture)
		return false;

	if (!texture->Load())
	{
		texture->Release();
		return false;
	}

	this->texture = texture;
	return true;
}


bool ShapeNode::LoadMesh()
{
	Assert(!this->mesh_file.empty());

	if (this->mesh.IsValid())
		return true;

	Mesh *mesh = GfxServer::GetInstance()->NewMesh(this->mesh_file);
	if (!mesh)
		return false;

	if (!mesh->Load())
	{
		mesh->Release();
		return false;
	}

	this->mesh = mesh;
	return true;
}


void ShapeNode::UnloadTexture()
{
	if (this->texture.IsValid())
	{
		this->texture->Release();
		this->texture = NULL;
	}

	this->resources_valid = false;
}


void ShapeNode::UnloadMesh()
{
	if (this->mesh.IsValid())
	{
		this->mesh->Release();
		this->mesh = NULL;
	}

	SafeDeleteArray(this->mapping);

	this->resources_valid = false;
}


/**
	This method makes sure that all resources needed by this object
	are loaded. The method does NOT recurse into its children.

	@return     @c True, if resource loading was successful.
*/
bool ShapeNode::LoadResources()
{
	// load resources
	this->resources_valid = 
		this->LoadMesh() && 
		this->LoadTexture();

	// update mapping if absolute coordinates are set
	if (this->resources_valid && this->absolute)
	{
		ushort_t width = this->texture->GetOriginalWidth();
		ushort_t height = this->texture->GetOriginalHeight();

		for (ushort_t id = 0; id < this->mesh->GetVerticesCount(); id++)
		{
			this->mapping[id].x = this->mapping[id].x / width;
			this->mapping[id].y = this->mapping[id].y / height;
		}
	}

	return this->resources_valid;
}


/**
	This method makes sure that all resources used by this object are
	unloaded. The method does NOT recurse into its children. 

	@return     @c True, if resources have actually been unloaded.
*/
void ShapeNode::UnloadResources()
{
	this->UnloadMesh();
	this->UnloadTexture();

	this->resources_valid = false;
}


/**
	Perform per-instance rendering of geometry. This method will be
	called once for each instance of the node.
*/
void ShapeNode::Render()
{
	GfxServer *gs = GfxServer::GetInstance();
	Assert(gs);

	#ifdef USE_PROFILERS
	// reset shapes counter with new frame
	if (TimeServer::GetInstance()->GetFrame() > this->last_frame)
	{
		this->shapes_variable->SetI(0);
		this->triangles_variable->SetI(0);
		this->last_frame = TimeServer::GetInstance()->GetFrame();
	}
	#endif

	if (this->mesh.IsValid())
	{
		// static model
		gs->SetMesh(this->mesh.Get());
		gs->SetTexture(this->texture.GetUnsafe());
		gs->SetMapping(this->mapping_buff ? this->mapping_buff : this->mapping);

		// animated buffers
		if (this->coords_buff)
			gs->SetCoordsBuffer(this->coords_buff);
		if (this->colors_buff)
			gs->SetColorsBuffer(this->colors_buff);

		// render shape
		gs->RenderShape();

		#ifdef USE_PROFILERS
		// update counter
		this->shapes_variable->SetI(this->shapes_variable->GetI() + 1);
		this->triangles_variable->SetI(this->triangles_variable->GetI() + this->mesh->GetTrianglesCount());
		#endif
	}

	SceneNode::Render();
}


/**
	Creates all assigned animators and animation buffers for this shape node and all children.
	Animators are created only if mesh is valid and it has nonzero amount of vertices.
*/
void ShapeNode::CreateAnimators()
{
	SceneServer *ss = SceneServer::GetInstance();
	Assert(ss);

	// animators are created only if mesh is valid
	if (this->mesh.IsValid() && this->mesh->GetVerticesCount())
	{
		if (!this->mapping_file.empty())
		{
			if (!this->mapping_buff)
				this->mapping_buff = NEW vector2[this->mesh->GetVerticesCount()];
			ss->NewAnimator(this->mapping_file, this->mapping_buff);
		}

		if (!this->coords_file.empty())
		{
			if (!this->coords_buff)
				this->coords_buff = NEW vector2[this->mesh->GetVerticesCount()];
			ss->NewAnimator(this->coords_file, this->coords_buff);
		}

		if (!this->colors_file.empty())
		{
			if (!this->colors_buff)
				this->colors_buff = NEW vector4[this->mesh->GetVerticesCount()];
			ss->NewAnimator(this->colors_file, this->colors_buff);
		}
	}

	// creates animators in all childs
	SceneNode::CreateAnimators();
}


// vim:ts=4:sw=4:
