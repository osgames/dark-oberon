#ifndef __transformnode_h__
#define __transformnode_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file transformnode.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/root.h"
#include "kernel/ref.h"
#include "kernel/vector.h"
#include "framework/scenenode.h"


//=========================================================================
// TransformNode
//=========================================================================

/**
	@class TransformNode
	@ingroup Framework_Module

	@brief A transform node groups its child nodes and defines position,
	orientation and scale of a scene node. Transformation hierarchies
	can be created using the object name space hierarchy.
*/

class TransformNode : public SceneNode
{
//--- embeded
protected:
	enum TransformType {
		TT_TRANSLATION = (1 << 0),
		TT_ROTATION = (1 << 1)
	};

//--- variables
protected:
	vector2 translation;
	float rotation;
	byte_t flags;


//--- methods
public:
	TransformNode(const char *id);
	virtual ~TransformNode();

	virtual void Render();

	void SetTranslation(const vector2 &translation);
	void SetRotation(float rotation);

	const vector2 &GetTranslation();
	float GetRotation();
};


//=========================================================================
// Methods
//=========================================================================


inline
const vector2 &TransformNode::GetTranslation()
{
	return this->translation;
}


inline
float TransformNode::GetRotation()
{
	return this->rotation;
}


#endif  // __transformnode_h__

// vim:ts=4:sw=4:
