//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sceneobject.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2006, 2007

	@version 1.0 - Initial.
	@version 1.1 - Visible flag.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sceneobject.h"
#include "framework/animator.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"

PrepareClass(SceneObject, "Root", s_NewSceneObject, s_InitSceneObject);


//=========================================================================
// SceneObject
//=========================================================================

/**
	Constructor.
*/
SceneObject::SceneObject(const char *id) :
	Root(id),
	resources_valid(false),
	active_model(NULL),
	visible(true),
	callback_render_begin(NULL),
	callback_render_end(NULL)
{
	this->size.set(1.0f, 1.0f);
}


/**
	Destructor.
*/
SceneObject::~SceneObject()
{
	if (this->resources_valid)
		this->UnloadResources();
}


bool SceneObject::LoadResources()
{
	ModelItem *item;
	bool ok = true;

	for (byte_t i = 0; i < this->models.size() && ok; i++)
	{
		item = this->models[i];
		Assert(!item->model_file.empty());

		if (item->model.IsValid())
			return true;

		// create model
		Model *model = GfxServer::GetInstance()->NewModel(item->model_file, this->overlay_color);
		if (!model)
		{
			ok = false;
			break;
		}

		// set new model
		item->model = model;

		// load model
		if (!model->Load())
		{
			ok = false;
			break;
		}

		// create parent of animators
		this->kernel_server->PushCwd(this);
		item->animators = this->kernel_server->New("Root", string("anim_") + model->GetID());
		Assert(item->animators.IsValid());
		this->kernel_server->PopCwd();

		// create animators
		this->kernel_server->PushCwd(item->animators.Get());
		model->CreateAnimators();
		this->kernel_server->PopCwd();

		Animator *animator = (Animator *)item->animators->GetFront();

		for (; animator; animator = (Animator *)animator->GetNext())
		{
			if (!animator->LoadResources())
			{
				ok = false;
				break;
			}
		}
	}

	if (!ok)
		this->UnloadResources();
	else
		this->ActivateModel(0);

	return this->resources_valid = ok;
}


void SceneObject::UnloadResources()
{
	for (byte_t i = 0; i < this->models.size(); i++)
	{
		// delete model
		if (this->models[i]->model.IsValid())
			this->models[i]->model->Release();

		// delete animators
		if (this->models[i]->animators.IsValid())
			this->models[i]->animators->Release();

		// delete array item
		delete this->models[i];
	}

	this->resources_valid = false;
}


byte_t SceneObject::AddModel(const string &model_file)
{
	Assert(this->models.size() < 255);

	// create new item
	ModelItem *item = NEW ModelItem;
	Assert(item);

	// set properties
	item->model_file = model_file;

	// add new item to the models array
	this->models.push_back(item);

	return byte_t(this->models.size() - 1);
}


bool SceneObject::ActivateModel(byte_t id)
{
	if (id >= this->models.size())
		return false;

	Animator *animator;

	// go through old animators and stop them
	if (this->active_model)
	{
		for (animator = (Animator *)this->active_model->animators->GetFront(); animator; animator = (Animator *)animator->GetNext())
			animator->Stop();
	}

	// set new active model
	this->active_model = this->models[id];

	// go through new animators and start them
	for (animator = (Animator *)this->active_model->animators->GetFront(); animator; animator = (Animator *)animator->GetNext())
		animator->Play();

	return true;
}


/**
*/
void SceneObject::UpdateAnimators()
{
	Assert(this->resources_valid);
	Assert(this->active_model);

	// go through all animators and update them
	Animator *animator = (Animator *)this->active_model->animators->GetFront();

	for (; animator; animator = (Animator *)animator->GetNext())
		animator->Update();
}


void SceneObject::PauseAnimators()
{
	Assert(this->resources_valid);
	Assert(this->active_model);

	// go through all animators and pause them
	Animator *animator = (Animator *)this->active_model->animators->GetFront();

	for (; animator; animator = (Animator *)animator->GetNext())
		animator->Pause();
}


// vim:ts=4:sw=4:
