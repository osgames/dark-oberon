#ifndef __gfxserver_h__
#define __gfxserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file gfxserver.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Converting MinMagFilter to string and back.
	@version 1.2 - Rendering quads, color mask.
	@version 1.3 - Cursor models.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/ref.h"
#include "kernel/rootserver.h"
#include "framework/displaymode.h"
#include "framework/gfxstructures.h"
#include "framework/texture.h"
#include "framework/mesh.h"
#include "framework/model.h"
#include "framework/imagefile.h"
#include "framework/animation.h"
#include "framework/font.h"

class Model;
class ResourceServer;
class vector2;
class MeshFile;
class ModelFile;
class AnimationFile;
class FontFile;


//=========================================================================
// GfxServer
//=========================================================================

/**
	@class GfxServer
	@ingroup Framework_Module

	@brief GFX server interface.
*/

class GfxServer : public RootServer<GfxServer>
{
//--- embeded
public:
	/// The visible mouse cursor type.
	enum CursorVisibility
	{
		CURSOR_HIDDEN,             ///< No mouse cursor is visible.
		CURSOR_SYSTEM,             ///< Use Window's system mouse cursor.
		CURSOR_CUSTOM              ///< Use the custom mouse cursor.
	};

	/// Minification/Magnification filter.
	enum MinMagFilter
	{
		FILTER_NONE,               ///< No filter (used only as mip-map filter).
		FILTER_NEAREST,            ///< Nearest filter.
		FILTER_LINEAR              ///< Linear filter.
	};

	/// Buffer Types.
	enum BufferType
	{
		BUFFER_COLOR   = (1 << 0),
		BUFFER_DEPTH   = (1 << 1),
		BUFFER_STENCIL = (1 << 2),
		BUFFER_ALL     = (BUFFER_COLOR | BUFFER_DEPTH | BUFFER_STENCIL)
	};

	/// Graphics extensions.
	enum Extension
	{
		EXT_NONE          = 0,
		EXT_NPOT_TEXTURES = (1 << 0)       ///< Textures with non power of two dimensions.
	};

	enum PrimitiveType
	{
		PRIMITIVE_POINTS,
		PRIMITIVE_LINES,
		PRIMITIVE_LINE_STRIP,
		PRIMITIVE_LINE_LOOP,
		PRIMITIVE_TRIANGLES,
		PRIMITIVE_TRIANGLE_STRIP,
		PRIMITIVE_TRIANGLE_FAN,
		PRIMITIVE_QUADS,
		PRIMITIVE_QUAD_STRIP,
		PRIMITIVE_POLYGON,
		PRIMITIVE_COUNT
	};

	typedef void (* SizeCallbackFun)(ushort_t, ushort_t, bool);
	typedef void (* RefreshCallbackFun)(void);
	typedef bool (* CloseCallbackFun)(void);

protected:
	struct ClearValues
	{
		float red;
		float green;
		float blue;
		float alpha;
		float depth;
		int stencil;
	};


//--- variables
protected:
	// properies
	string window_title;
	DisplayMode display_mode;
	MinMagFilter minmag_filter;
	MinMagFilter mipmap_filter;
	ClearValues clear_values;
	byte_t extensions;
	bool blending;
	bool depth_test;
	vector4 color_filter;
	
	bool display_opened;

    Ref<ResourceServer> resource_server;

	// mouse cursors
	CursorVisibility cursor_visibility;
	vector<Model *> cursors;
	Model *cursor_model;

	// callbacks
	SizeCallbackFun callback_size;
	RefreshCallbackFun callback_refresh;
	CloseCallbackFun callback_close;

	// drawing
	Mesh *mesh;
	Texture *texture;
	Texture *overlay;
	vector2 *mapping;
	vector2 *coords_buff;              ///< Pointer to active buffer with animated coords.
	vector4 *colors_buff;              ///< Pointer to active buffer with animated colors.
	Ref<Font> font;                    ///< Active font used for printing texts.


//--- methods
public:
	GfxServer(const char *id);
	virtual ~GfxServer();

	// creating objects
    virtual Texture *NewTexture(const string &file_name);
	virtual Texture *NewTexture(const string &file_name, const string &overlay_name, const vector3 &overlay_color);
	virtual Texture *NewTexture(ImageFile *image_file);
	virtual Font *NewFont(const string &file_name, const FontFile::CharSize &char_size);
	Mesh *NewMesh(const string &file_name);
	Model *NewModel(const string &file_name);
	Model *NewModel(const string &file_name, const vector3 &overlay_color);
	Animation *NewAnimation(const string &file_name);

	// creating data files
	ImageFile *NewImageFile(const string &file_name, bool open = false, bool write = false);
	FontFile *NewFontFile(const string &file_name, bool open = false, bool write = false);

	// settings
	void SetDisplayMode(const DisplayMode& mode);
	const DisplayMode &GetDisplayMode() const;
	
	virtual void SetProjection(const Projection& projection);
	virtual void SetViewport(const Viewport& display_viewport);
	virtual void PushScreenCoordinates();
	virtual void PopScreenCoordinates();
	virtual ushort_t GetVideoModes(VideoMode *modes, int max_count, VideoMode::Bpp bpp = VideoMode::BPP_UNDEFINED);
	byte_t GetExtensions();
	bool IsExtensions(byte_t extensions);

	// texture filtering
	void SetMinMagFilter(MinMagFilter minmag_filter);
	void SetMipMapFilter(MinMagFilter mipmap_filter);
	MinMagFilter GetMinMagFilter() const;
	MinMagFilter GetMipMapFilter() const;

	// window
	virtual void SetWindowTitle(const string &window_title);
	virtual void SetWindowPosition(const DisplayMode::WidowPosition &position);
	virtual const DisplayMode::WidowPosition &GetWindowPosition() const;
	virtual void SetWindowSize(const DisplayMode::WidowSize &size);
	virtual const DisplayMode::WidowSize &GetWindowSize() const;
	virtual void SetWindowVerticalSync(bool vsync);
	virtual void MinimizeWindow();
	virtual void RestoreWindow();
	virtual void ToggleFullscreen();
	virtual bool IsWindowMinimized();
	virtual bool IsWindowActive();
	virtual bool IsWindowAccelerated();

	// callbacks
	void SetWindowSizeCallback(SizeCallbackFun cbfun);
	void SetWindowRefreshCallback(RefreshCallbackFun cbfun);
	void SetWindowCloseCallback(CloseCallbackFun cbfun);

	// display
	virtual bool OpenDisplay();
    virtual void CloseDisplay();
	virtual void PresentScene();
	virtual void WriteInfo();

	virtual void RegisterCallbacks();
	virtual bool Trigger();
	
	bool IsDisplayOpened();

	// mouse
	virtual void SetCursorVisibility(CursorVisibility type);
	CursorVisibility GetCursorVisibility() const;
	bool TestCursorVisibility(CursorVisibility type) const;
	byte_t AddCursor(const string &model_file);
	void SetCursor(byte_t cursor_id);

	// images
	virtual bool SaveScreenshot(const string &file_name);
	bool SaveImage(const string &file_name, byte_t *data, ImageFile::Format format, ushort_t width, ushort_t height);

	// drawing
	void SetMesh(Mesh *mesh);
	void SetTexture(Texture *texture);
	void SetMapping(vector2 *mapping);
	void SetCoordsBuffer(vector2 *coords_buff);
	void SetColorsBuffer(vector4 *colors_buff);
	void SetFont(Font *font);
	const Font *GetFont();

	virtual void ResetMatrix();
	virtual void PushMatrix();
	virtual void PopMatrix();

	virtual void Translate(const vector2 &translation);
	virtual void Translate(const vector3 &translation);
	virtual void Rotate(float rotation);
	virtual void Rotate(const vector3 &rotation);

	virtual void SetColor(const vector3 &color);
	virtual void SetColor(const vector4 &color);
	virtual void SetColorMask(bool r, bool g, bool b, bool alpha = true);	
	virtual void SetClearValues(float red, float green, float blue, float alpha = 0.0f, float depth = 0.0f, int stencil = 0);
	virtual void SetBlending(bool blending);
	virtual void SetDepthTest(bool depth_test);
	void SetColorFilter(float r, float g, float b, float alpha = 1.0f);

	virtual void ClearBuffers(byte_t buffers);
	virtual void RenderShape();
	virtual void RenderPrimitive(PrimitiveType primitive, const vector2 *vertices, uint_t count);
	virtual void Print(const vector2 &position, const string &text);
	void Print(const string &text);
	virtual void ReadPixels(ushort_t x, ushort_t y, ushort_t width, ushort_t height, byte_t *pixels);

	// conversions
	static bool FilterToString(MinMagFilter filter, string &name);
	static bool StringToFilter(const string &name, MinMagFilter &filter);

protected:
	virtual void CheckExtensions();
	void SetClearValues(GfxServer::ClearValues &clear_values);
	void ResetRenderObjects();

	bool LoadResources();
	void UnloadResources();
};


//=========================================================================
// Methods
//=========================================================================

inline
const DisplayMode& GfxServer::GetDisplayMode() const
{
	return this->display_mode;
}


inline
bool GfxServer::IsDisplayOpened()
{
	return this->display_opened;
}


inline
GfxServer::CursorVisibility GfxServer::GetCursorVisibility() const
{
	return this->cursor_visibility;
}


inline
bool GfxServer::TestCursorVisibility(CursorVisibility type) const
{
	return this->cursor_visibility == type;
}


inline
void GfxServer::SetCursor(byte_t cursor_id)
{
	if (cursor_id < this->cursors.size())
		this->cursor_model = this->cursors[cursor_id];
	else if (this->cursors.size())
		this->cursor_model = this->cursors[cursor_id = 0];
	else
		this->cursor_model = NULL;

	Assert (!this->cursor_model || this->cursor_model->IsLoaded());
}


inline
void GfxServer::SetMinMagFilter(MinMagFilter minmag_filter)
{
	this->minmag_filter = minmag_filter;
}


inline
GfxServer::MinMagFilter GfxServer::GetMinMagFilter() const
{
	return this->minmag_filter;
}


inline
void GfxServer::SetMipMapFilter(MinMagFilter mipmap_filter)
{
	this->mipmap_filter = mipmap_filter;
}


inline
GfxServer::MinMagFilter GfxServer::GetMipMapFilter() const
{
	return this->mipmap_filter;
}


inline
void GfxServer::SetMesh(Mesh *mesh)
{
	this->mesh = mesh;
}


inline
void GfxServer::SetTexture(Texture *texture)
{
	this->texture = texture;
}


inline
void GfxServer::SetMapping(vector2 *mapping)
{
	/// @todo Add parameter with size of array and check the size while rendering. See RenderQuads for using this kind of size.
	this->mapping = mapping;
}


inline
void GfxServer::SetCoordsBuffer(vector2 *coords_buff)
{
	this->coords_buff = coords_buff;
}


inline
void GfxServer::SetColorsBuffer(vector4 *colors_buff)
{
	this->colors_buff = colors_buff;
}


inline
void GfxServer::SetColorFilter(float r, float g, float b, float alpha)
{
	this->color_filter.set(r, g, b, alpha);
}


inline
const Font *GfxServer::GetFont()
{
	return this->font.GetUnsafe();
}


inline
byte_t GfxServer::GetExtensions()
{
	return this->extensions;
}


inline
bool GfxServer::IsExtensions(byte_t extensions)
{
	return (this->extensions & extensions) == extensions;
}


inline
void GfxServer::SetClearValues(GfxServer::ClearValues &clear_values)
{
	this->SetClearValues(
		clear_values.red,
		clear_values.green,
		clear_values.blue,
		clear_values.alpha,
		clear_values.depth,
		clear_values.stencil
	);
}


inline
void GfxServer::SetWindowSizeCallback(SizeCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_size = cbfun;
}


inline
void GfxServer::SetWindowRefreshCallback(RefreshCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_refresh = cbfun;
}


inline
void GfxServer::SetWindowCloseCallback(CloseCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_close = cbfun;
}

inline
void GfxServer::ResetRenderObjects()
{
	this->mesh = NULL;
	this->texture = NULL;
	this->overlay = NULL;
	this->mapping = NULL;
	this->coords_buff = NULL;
	this->colors_buff = NULL;
}


#endif  // __gfxserver_h__

// vim:ts=4:sw=4:
