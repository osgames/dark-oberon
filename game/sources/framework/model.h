#ifndef __model_h__
#define __model_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file model.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/vector.h"
#include "framework/resource.h"
#include "framework/scenenode.h"


//=========================================================================
// Model
//=========================================================================

/**
	@class Model
	@ingroup Framework_Module
*/
    
class Model : public SerializedResource
{
//--- variables
protected:
	Ref<SceneNode> root_node;
	ulong_t data_size;

	vector3 overlay_color;   ///< Color used for overlay image.

//--- methods
public:
	Model(const char *id);
	virtual ~Model();

	SceneNode *GetRootNode();
	void SetOverlayColor(const vector3 &color);

	void Render();
	void CreateAnimators();

	virtual ulong_t GetDataSize();

protected:
	virtual bool LoadResource();
	virtual void Clear();

	// serializing
	virtual bool Deserialize(Serializer &serializer, bool first);
	bool ReadSceneNodes(Serializer &serializer, bool root);
	bool ReadShapeNode(Serializer &serializer, bool root);
	bool ReadTransformNode(Serializer &serializer, bool root);
};


//=========================================================================
// Methods
//=========================================================================

inline
SceneNode *Model::GetRootNode()
{
	return this->root_node.GetUnsafe();
}


inline
void Model::SetOverlayColor(const vector3 &color)
{
	this->overlay_color = color;
}


inline
void Model::Render()
{
	Assert(this->loaded);
	this->root_node->Render();
}


inline
void Model::CreateAnimators()
{
	Assert(this->loaded);
	this->root_node->CreateAnimators();
}


#endif // __model_h__

// vim:ts=4:sw=4:
