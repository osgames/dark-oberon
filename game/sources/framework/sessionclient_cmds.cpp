//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionclientcontext_cmds.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2009

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sessionclient.h"

//=========================================================================
// Commands
//=========================================================================

/**
	Set the application name. Will be used together with the AppVersion
	to identify sessions.
*/
static void s_SetAppName(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	self->SetAppName(cmd->In()->GetS());
}


/**
	Get the application name.
*/
static void s_GetAppName(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetS(self->GetAppName());
}


/**
	Set the version string. Will be used together with the AppName
	to identify sessions.
*/
static void s_SetAppVersion(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	self->SetAppVersion(cmd->In()->GetS());
}


/**
	Get the application version string.
*/
static void s_GetAppVersion(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetS(self->GetAppVersion());
}


/**
	Set a client attribute.
*/
static void s_SetClientAttr(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;

	const char* s0 = cmd->In()->GetS();
	const char* s1 = cmd->In()->GetS();

	self->SetClientAttr(s0, s1);
}


/**
	Get a client attribute.
*/
static void s_GetClientAttr(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetS(self->GetClientAttr(cmd->In()->GetS()));
}


/**
	Get number of discovered servers.
*/
static void s_GetServersCount(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetI(self->GetServersCount());
}


/**
	Get session server object at given index.
*/
static void s_GetServerAt(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetO(self->GetServerAt(cmd->In()->GetI()));
}


/**
	Get reference to joined server.
*/
static void s_GetJoinedServer(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetO(self->GetJoinedServer());
}


/**
	Open the session client. This will start discovering Open sessions
	on the network.
*/
static void s_Open(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetB(self->Open());
}


/**
	Close the session client.
*/
static void s_Close(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	self->Close();
}


/**
	Check if session client is currently Open.
*/
static void s_IsOpened(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetB(self->IsOpened());
}


/**
	Send a join request to the session server identified by the session's
	guid. The success code only indicates, whether a session of that
	guid could be found, it does NOT indicate whether the join was
	successful. The session server will reply a join accepted or denied
	message at some later time.
*/
static void s_JoinSession(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetB(self->JoinSession(cmd->In()->GetS()));
}


/**
	Leave the currently joined session.
*/
static void s_LeaveSession(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetB(self->LeaveSession());
}


/**
	Return true if currently joining to a session.
*/
static void s_IsJoining(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetB(self->IsJoining());
}


/**
	Return true if joining has been accepted.
*/
static void s_IsJoined(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetB(self->IsJoined());
}


/**
	Return true if the server had denied the join session request.
*/
static void s_IsJoinDenied(void *slf, Cmd *cmd)
{
	SessionClient *self = (SessionClient *)slf;
	cmd->Out()->SetB(self->IsJoinDenied());
}


/**
	The network session client class.
*/
void s_InitSessionClient_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("v_SetAppName_s",          'SAPN', s_SetAppName);
	cl->AddCmd("s_GetAppName_v",          'GAPN', s_GetAppName);
	cl->AddCmd("v_SetAppVersion_s",       'SAPV', s_SetAppVersion);
	cl->AddCmd("s_GetAppVersion_v",       'GAPV', s_GetAppVersion);
	cl->AddCmd("v_SetClientAttr_ss",      'SCLA', s_SetClientAttr);
	cl->AddCmd("s_GetClientAttr_s",       'GCLA', s_GetClientAttr);
	cl->AddCmd("i_GetServersCount_v",     'GNMS', s_GetServersCount);
	cl->AddCmd("o_GetServerAt_i",         'GSAT', s_GetServerAt);
	cl->AddCmd("o_GetJoinedServer_v",     'GJSR', s_GetJoinedServer);
	cl->AddCmd("b_opes_v",                'OPEN', s_Open);
	cl->AddCmd("v_Close_v",               'CLOS', s_Close);
	cl->AddCmd("b_IsOpened_v",            'ISOP', s_IsOpened);
	cl->AddCmd("b_JoinSession_s",         'JNSS', s_JoinSession);
	cl->AddCmd("b_LeaveSession_v",        'LVSS', s_LeaveSession);
	cl->AddCmd("b_IsJoining_v",           'ISJA', s_IsJoining);
	cl->AddCmd("b_IsJoined_v",            'ISJN', s_IsJoined);
	cl->AddCmd("b_IsJoinDenied_v",        'ISJD', s_IsJoinDenied);

	cl->EndCmds();
}

// vim:ts=4:sw=4:
