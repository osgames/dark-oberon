#ifndef __imagefile_h__
#define __imagefile_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file imagefile.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/datafile.h"

class vector3;


//=========================================================================
// ImageFile
//=========================================================================

/**
	@class ImageFile
	@ingroup Framework_Module

	Provides read / write access to iamge file.

	This class is the base for all image files.

*/

class ImageFile : public DataFile
{
//-- embeded
public:
	/**
		Format of image data.
	*/
	enum Format
	{
		IF_UNKNOWN,         ///< Unknown format.
		IF_LUMINANCE,       ///< Grayscale 8bit.
		IF_RGB,             ///< RGB 24bit.
		IF_RGBA             ///< RGB+Alpha 32bit.
	};


//-- variables
protected:
	Format format;           ///< Format of the image.
	ushort_t width;          ///< Image width.
	ushort_t height;         ///< Image height.

	byte_t *data;            ///< Loaded image data (simple buffer).
	bool use_free;           ///< Use free() instead of delete for releasing image data.


//-- methods
public:
	ImageFile();
	virtual ~ImageFile();

	virtual ulong_t LoadData();

	Format    GetFormat();
	bool      TestFormat(ImageFile::Format format);
	byte_t   *GetData(bool close = false);
	ushort_t  GetWidth();
	ushort_t  GetHeight();

	void SetData(byte_t *data, ulong_t size, Format format, ushort_t width, ushort_t height);
	void CopyData(const byte_t *data, ulong_t size, Format format, ushort_t width, ushort_t height);
	void CopyData(ImageFile *image);

	bool AddOverlay(ImageFile *overlay_image, const vector3 &overlay_color);
	void Resample(ushort_t new_width, ushort_t new_height);

	static byte_t GetBytesPerPixel(ImageFile::Format format);

private:
	virtual void DeleteData();
};


//=========================================================================
// Methods
//=========================================================================

/**
	Returns image format.

	@return Image format.
*/
inline
ImageFile::Format ImageFile::GetFormat()
{
	return this->format;
}


/**
	Tests image format.

	@param format Image format to be tested.
	@return @c True if given format is as same as image format.
*/
inline
bool ImageFile::TestFormat(ImageFile::Format format)
{
	return this->format == format;
}


/**
	Returns loaded image data.

	@param close  Whether file will be closed after returning the data.
	@return Loaded image data.
*/
inline
byte_t *ImageFile::GetData(bool close)
{
	byte_t *result = this->data;

	if (close)
	{
		this->data = NULL;
		this->Close();
	}

	return result;
}


/**
	Returns image width.

	@return Image width.
*/
inline
ushort_t ImageFile::GetWidth()
{
	return this->width;
}


/**
	Returns image height.

	@return Image height.
*/
inline
ushort_t ImageFile::GetHeight()
{
	return this->height;
}


/**
	Returns number of bytes per pixel of given image format.

	@param format Image format.
	@return Bytes per pixel.
*/
inline
byte_t ImageFile::GetBytesPerPixel(ImageFile::Format format)
{
	switch (format)
	{
	case IF_LUMINANCE: return 1;
	case IF_RGB:       return 3;
	case IF_RGBA:      return 4;
	case IF_UNKNOWN:
	default:           return 0;
	}
}


#endif // __imagefile_h__

// vim:ts=4:sw=4:
