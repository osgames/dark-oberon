#ifndef __audiofile_h__
#define __audiofile_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file audiofile.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/datafile.h"


//=========================================================================
// AudioFile
//=========================================================================

/**
	@class AudioFile
	@ingroup Framework_Module

	Provides read / write access to audio file.

	This class is the base for all audio files.
*/

class AudioFile : public DataFile {
//-- embeded
public:
	/**
		Format of audio data.
	*/
	struct Format {
		byte_t bits;        ///< Number of bits per one sample.
		ulong_t bitrate;    ///< Audio bitrate (bits per seconds).
		byte_t channels;    ///< Number of channels
	};

//-- variables
protected:
	Format format;          ///< Format of audio data.

	char *data;             ///< Memory buffer with data.
	ulong_t buff_size;      ///< Size of loaded buffer. This size can be smaller than data_size when one block is loaded.


//-- methods
public:
	AudioFile();
	virtual ~AudioFile();

	virtual ulong_t LoadBlock(ulong_t size, bool looping) = 0;
	virtual ulong_t LoadTimeBlock(double time, bool looping) = 0;
	virtual void Rewind() = 0;

	Format &GetFormat();
	char   *GetData(bool close = false);

	void SetFormat(Format &format);
	void SetData(char *data, long size);

protected:
	virtual void DeleteData();
};


//=========================================================================
// Methods
//=========================================================================

/**
	Returns format of audio data.

	@return Format of audio data.
*/
inline
AudioFile::Format &AudioFile::GetFormat()
{
	return this->format;
}


/**
	Sets format of audio data.

	@param format Format of audio data.
*/
inline
void AudioFile::SetFormat(Format &format)
{
	this->format = format;
}


#endif // __audiofile_h__

// vim:ts=4:sw=4:
