//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file fontfile.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/fontfile.h"


//=========================================================================
// FontFile
//=========================================================================

/**
	Constructor.
	Sets file mode to binary. Default glyph size is set to 12, resolution to 96.
*/
FontFile::FontFile() :
	DataFile(),
	data(NULL),
	glyphs_count(0)
{
	this->glyph_data.left = 0;
	this->glyph_data.top = 0;
	this->glyph_data.width = 0;
	this->glyph_data.height = 0;
	this->glyph_data.advance_x = 0;
	this->glyph_data.advance_y = 0;

	this->char_size.width = 12;
	this->char_size.height = 12;
	this->char_size.resolution_x = 96;
	this->char_size.resolution_y = 96;

	this->binary = true;
}


/**
	Destructor.
*/
FontFile::~FontFile()
{
	// empty
}


/**
	Returns bitmap of one character.

	@param char_code Code of wanted character.
	@param close  Whether file will be closed after returning the data.

	@return Character bitmap.
*/
byte_t *FontFile::GetData(ulong_t char_code, bool close)
{
	// return data
	uchar_t *result = this->data;

	// close file, but don't delete loaded data
	if (close) {
		this->data = NULL;
		this->Close();
	}

	return result;
}


/**
	Deletes loaded data.
*/
void FontFile::DeleteData()
{
	// delete data
	SafeDeleteArray(this->data);

	this->data_size = 0;
}


// vim:ts=4:sw=4:
