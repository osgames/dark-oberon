#ifndef __framework_system_h__
#define __framework_system_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file framework/system.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2007

	@brief Includes all common kernel headers.

	@version 1.0 - Initial.
*/


//=========================================================================
// Common header files
//=========================================================================

#include "kernel/system.h"
#include "framework/config.h"


#endif // __system_h__

// vim:ts=2:sw=2:et:
