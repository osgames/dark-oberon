//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file resourceserver.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Creating resource using given DataFile.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/fileserver.h"
#include "kernel/kernelserver.h"
#include "framework/resourceserver.h"
#include "framework/resource.h"
#include "framework/datafile.h"

PrepareClass(ResourceServer, "Root", s_NewResourceServer, s_InitResourceServer);

#define MAX_ID_LENGTH 1024


//=========================================================================
// ResourceServer
//=========================================================================

/**
	Constructor.
*/
ResourceServer::ResourceServer(const char *id) :
	RootServer<ResourceServer>(id)
{
	if (!FileServer::GetInstance())
		ErrorMsg("FileServer has to be created before ResourceServer");

	this->resources[Resource::RES_SOUND]          = this->kernel_server->New("Root", NOH_RESOURCES + "/sounds");
	this->resources[Resource::RES_SOUND_RESOURCE] = this->kernel_server->New("Root", NOH_RESOURCES + "/soundres");
	this->resources[Resource::RES_TEXTURE]        = this->kernel_server->New("Root", NOH_RESOURCES + "/textures");
	this->resources[Resource::RES_MESH]           = this->kernel_server->New("Root", NOH_RESOURCES + "/meshes");
	this->resources[Resource::RES_MODEL]          = this->kernel_server->New("Root", NOH_RESOURCES + "/models");
	this->resources[Resource::RES_ANIMATION]      = this->kernel_server->New("Root", NOH_RESOURCES + "/animations");
	this->resources[Resource::RES_FONT]           = this->kernel_server->New("Root", NOH_RESOURCES + "/fonts");
	this->resources[Resource::RES_OTHER]          = this->kernel_server->New("Root", NOH_RESOURCES + "/other");
}


/**
	Destructor.
*/
ResourceServer::~ResourceServer()
{
	this->UnloadAllResources();
}


/**
	Creates a resource id from a resource name. The resource name is usually
	just the filename of the resource file. The method strips off the last
	32 characters from the resource name, and replaces any invalid characters
	with underscores. It is valid to provide a 0-path for unshared resources.
	A unique res identifier string will then be created.

	@param  path         Pointer to a resource name (usually a file path), or 0.
	@return              Text identifier.
*/
string ResourceServer::GetResourceId(const string &path)
{
	static uint_t id_counter = 0;
	char buff[MAX_ID_LENGTH + 1];

	if (path.empty())
		sprintf(buff, "resource%u", id_counter++);

	else
	{
		str_size_t len = path.length() + 1;
		str_pos_t offset = len > MAX_ID_LENGTH ? len - MAX_ID_LENGTH : 0;

		// copy string and replace illegal characters, this also copies the terminating 0
		char c;
		const char *from = &(path.c_str()[offset]);
		char *to = buff;

		while ((c = *from++)) {
			*to++ = (c == '.') || (c == '/') || (c == ':') || (c == '\\') ? '_' : c;
		}
		*to = 0;
	}

	return string(buff);
}


/**
	Returns the right resource root object for a given resource type.

	@param  type    The resource type.
	@return         The root object.
*/
Root* ResourceServer::GetResourceList(Resource::Type type)
{
	Assert(type < Resource::RES_COUNT);
	return this->resources[type].Get();
}


/**
	Finds a resource object by resource type and path.

	@param  path    the res name
	@param  type    resource type
	@return             pointer to resource object, or 0 if not found
*/
Resource* ResourceServer::FindResource(const string &path, Resource::Type type)
{
	Assert(!path.empty());
	Assert(type < Resource::RES_COUNT);

	string id = this->GetResourceId(path);
	Root *pool = this->GetResourceList(type);
	Assert(pool);

	return (Resource *)pool->Find(id);
}


/**
	Creates a new possible shared resource object. Bumps pointers count on an 
	existing resource object. Pass a zero path if a (non-shared) resource 
	should be created.

	@param  class_name   Class name.
	@param  file_name    The resource file_name (for resource sharing), can be empty.
	@param  type         Resource type.
	@param  modifier     Modifier is added at the end of resource identifier.

	@return              Pointer to new resource object.
*/
Resource* ResourceServer::NewResource(const char* class_name, const string &file_name, Resource::Type type, const char *modifier)
{
	Assert(class_name);
	Assert(type < Resource::RES_COUNT);

	// compute resource identifier
	string id = this->GetResourceId(file_name);
	if (modifier)
	{
		id += "_";
		id += modifier;
	}

	// get loaded resources
	Root* pool = this->GetResourceList(type);
	Assert(pool);

	// see if resource exist
	Resource *obj = (Resource *) pool->Find(id);
	if (obj)
	{
		// exists, bump refcount and return
		obj->AcquirePointer();
	}

	else
	{
		// create new resource object
		this->kernel_server->PushCwd(pool);
		obj = (Resource*) this->kernel_server->New(class_name, id);
		Assert(obj);
		this->kernel_server->PopCwd();

		// set properties
		obj->SetFileName(file_name);
	}

	return obj;
}


Resource *ResourceServer::NewResource(const char *class_name, DataFile *data_file, Resource::Type type, const char *modifier)
{
	Resource *result = this->NewResource(class_name, data_file->GetFileName(), type, modifier);

	if (result)
		result->SetDataFile(data_file);

	return result;
}


/**
	Unload sall resources matching the given resource type.

	@param  type    Resource type.
*/
void ResourceServer::UnloadResources(Resource::Type type)
{
	if (type == Resource::RES_ALL)
	{
		this->UnloadAllResources();
		return;
	}

	Assert(type < Resource::RES_COUNT);

	Root *pool = this->GetResourceList(type);
	Assert(pool);
	Resource *res;

	for (res = (Resource *)pool->GetFront(); res; res = (Resource *)res->GetNext())
		res->Unload();
}


/**
	Loads all resources matching the given resource type. Returns false
	if any of the resources didn't load correctly.

	@param  type    Resource type.
	@return         @c True if all resources loaded correctly.
*/
bool ResourceServer::LoadResources(Resource::Type type)
{
	if (type == Resource::RES_ALL)
		return this->LoadAllResources();

	Assert(type < Resource::RES_COUNT);

	Root *pool = this->GetResourceList(type);
	Assert(pool);

	bool ok = true;
	Resource *res;

	for (res = (Resource *)pool->GetFront(); res; res = (Resource *)res->GetNext())
	{
		if (!res->IsLoaded())
			ok &= res->Load();
	}

	return ok;
}


/**
    Counts the number of resources of a given type.

	@param  type    Resource type.
*/
ushort_t ResourceServer::GetResourcesCount(Resource::Type type)
{
	if (type == Resource::RES_ALL)
		return this->GetAllResourcesCount();

	Assert(type < Resource::RES_COUNT);

	Root *pool = this->GetResourceList(type);
	Assert(pool);

	ushort_t count = 0;
	Resource *res;

	for (res = (Resource *)pool->GetFront(); res; res = (Resource *)res->GetNext())
		count++;

	return count;
}


/**
	Returns the number of bytes a resource type occupies in RAM.

	@param  type    Resource type.
*/
uint_t ResourceServer::GetResourcesSize(Resource::Type type)
{
	if (type == Resource::RES_ALL)
		return this->GetAllResourcesSize();

	Assert(type < Resource::RES_COUNT);

	Root *pool = this->GetResourceList(type);
	Assert(pool);

	uint_t size = 0;
	Resource *res;

	for (res = (Resource *)pool->GetFront(); res; res = (Resource *)res->GetNext())
		size += res->GetDataSize();

	return size;
}


// vim:ts=4:sw=4:
