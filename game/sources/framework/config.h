#ifndef __framework_config_h__
#define __framework_config_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file framework/config.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2003 - 2007

	@brief Configuration options for framework pre-processing.

	@version 1.0 - Initial.
*/


//=========================================================================
// Configuration
//=========================================================================

/**
	Specifies debugging of sorting methods (objects in scenes). Not defined by default.
*/
#ifndef DEBUG_SORTING
//#	define DEBUG_SORTING
#endif


/**
	Specifies usage of profilers. Not defined by default.
*/
#ifndef USE_PROFILERS
//#	define USE_PROFILERS
#endif


#endif // __framework_config_h__

// vim:ts=2:sw=2:et:
