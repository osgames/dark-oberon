//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionservercontext_cmds.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2009

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sessionservercontext.h"


//=========================================================================
// Commands
//=========================================================================

/**
	Get the session guid.
*/
static void s_GetSessionGuid(void* slf, Cmd* cmd)
{
	SessionServerContext *self = (SessionServerContext*) slf;
	cmd->Out()->SetS(self->GetSessionGuid());
}


/**
	Get the port name string of the session server.
*/
static void s_GetHostName(void* slf, Cmd* cmd)
{
	SessionServerContext *self = (SessionServerContext*) slf;
	cmd->Out()->SetS(self->GetHostName());
}


/**
	Get the port name string of the session server.
*/
static void s_GetPortNum(void* slf, Cmd* cmd)
{
	SessionServerContext *self = (SessionServerContext*) slf;
	cmd->Out()->SetI((int)self->GetPortNum());
}


static void s_GetAttributes(void *slf, Cmd *cmd)
{
	SessionServerContext *self = (SessionServerContext *)slf;
	cmd->Out()->SetO(self->GetAttributes());
}


void s_InitSessionServerContext_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("s_GetSessionGuid_v",        'GSGD', s_GetSessionGuid);
	cl->AddCmd("s_GetHostName_v",           'GHON', s_GetHostName);
	cl->AddCmd("s_GetPortNum_v",            'GPTN', s_GetPortNum);
	cl->AddCmd("o_GetAttributes_v",         'GATS', s_GetAttributes);

	cl->EndCmds();
}


// vim:ts=4:sw=4:
