//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scenenode.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/scenenode.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"

PrepareClass(SceneNode, "Root", s_NewSceneNode, s_InitSceneNode);


//=========================================================================
// SceneNode
//=========================================================================

/**
	Constructor.
*/
SceneNode::SceneNode(const char *id) :
	Root(id),
	resources_valid(false)
{
	//
}


/**
	Destructor.
*/
SceneNode::~SceneNode()
{
	//
}


/**
	This method makes sure that all resources needed by this object
	are loaded. The method does NOT recurse into its children.

	Subclasses should expect that the LoadResources() method can be 
	called on them although some or all of their resources are valid.
	Thus, a check should exist, whether the resource really needs to
	be reloaded.

	@return     @c True, if resource loading was successful.
*/
bool SceneNode::LoadResources()
{
	return this->resources_valid = true;
}


/**
	This method makes sure that all resources used by this object are
	unloaded. The method does NOT recurse into its children. 

	If you ovverride this method, be sure to call the overridden version
	in your destructor.
*/
void SceneNode::UnloadResources()
{
	this->resources_valid = false;
}


/**
	Recursively preloads required resources. Call this method after loading
	or creation and before the first rendering. It will load all required
	resources (textures, meshes, animations, ...) from disk and thus
	prevent stuttering during rendering.

	@return     @c True, if all resources have been loaded.
*/
bool SceneNode::PreloadResources()
{
	if (!this->resources_valid)
		this->LoadResources();

	// recurse
	SceneNode *node;
	bool all_valid = true;

	for (node = (SceneNode *)this->GetFront(); node; node = (SceneNode *)node->GetNext()) {
		if (!node->PreloadResources())
			all_valid = false;
	}

	return all_valid;
}


/**
	Performs per-instance rendering of geometry. This method will be
	called once for each instance of the node.
*/
void SceneNode::Render()
{
	SceneNode *node;

	for (node = (SceneNode *)this->GetFront(); node; node = (SceneNode *)node->GetNext())
		node->Render();
}


/**
	Creates per-instance animators.
*/
void SceneNode::CreateAnimators()
{
	SceneNode *node;

	for (node = (SceneNode *)this->GetFront(); node; node = (SceneNode *)node->GetNext())
		node->CreateAnimators();
}


// vim:ts=4:sw=4:
