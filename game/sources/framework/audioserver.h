#ifndef __audioserver_h__
#define __audioserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file audioserver.h
	@ingroup Framework_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/ref.h"
#include "kernel/rootserver.h"
#include "framework/sound.h"
#include "framework/listener.h"
#include "framework/audiofile.h"

class SoundResource;
class ResourceServer;


//=========================================================================
// Constants
//=========================================================================

const double AUDIO_STREAM_TIME_CHECK = 0.3;
const double AUDIO_STREAM_TIME_BLOCK = 1;


//=========================================================================
// AudioServer
//=========================================================================

/**
	@class AudioServer
	@ingroup Framework_Module

	Audio subsystem 3 server.
*/

class AudioServer : public RootServer<AudioServer>
{
//--- embeded
public:
	enum DistanceModel
	{
		DM_NONE,
		DM_INVERSE,
		DM_INVERSE_CLAMPED,
		DM_LINEAR,
		DM_LINEAR_CLAMPED,
		DM_EXPONENT,
		DM_EXPONENT_CLAMPED,
		DM_COUNT
	};

//--- variables
protected:
	bool muted;
	bool device_opened;
	string device_name;

	float master_volume;
	float category_volume[Sound::SND_COUNT];
	bool  category_allowed[Sound::SND_COUNT];

	DistanceModel distance_model;
	float doppler_factor;
	float speed_of_sound;

	bool volume_dirty;
	bool props_dirty;
	
	Listener *listener;
	Ref<ResourceServer> resource_server;
	Ref<Root> sounds_list;
	vector<string> device_list;

//--- methods
public:
	AudioServer(const char *id);
	virtual ~AudioServer();

	virtual Sound *NewSound(const string &file_name);
	virtual SoundResource* NewSoundResource(const string &file_name);

	AudioFile *NewAudioFile(const string &file_name, bool open = false, bool write = false);

	void SetDeviceName(const string &name);
	const string &GetDeviceName();
	const vector<string> &GetDeviceList();

	virtual bool OpenDevice();
	virtual void CloseDevice();
	virtual void WriteInfo();
	virtual bool Trigger();

	void PlaySound(Sound *snd);
	void StopSound(Sound *snd);
	void PauseSound(Sound *snd);
	void PlayPauseSound(Sound *snd);
	void StopAllSounds();
	void UpdateSound(Sound *snd);
	void UpdateStream(Sound *snd);
	void UpdateAllSounds();
	void UpdateAllStreams();
	void UpdateListener();

	void SetListener(Listener *l);
	void SetDistanceModel(DistanceModel model);
	void SetMasterVolume(float volume);
	void SetCategoryVolume(Sound::Category category, float volume);
	void SetCategoryAllowed(Sound::Category category, bool allowed);
	void SetDopplerFactor(float v);
	void SetSpeedOfSound(float v);

	AudioServer::DistanceModel GetDistanceModel();
	float GetMasterVolume();
	float GetCategoryVolume(Sound::Category category);
	bool  GetCategoryAllowed(Sound::Category category);
	float GetDopplerFactor() const;
	float GetSpeedOfSound() const;

	void Mute();
	void Unmute();
	bool IsMuted();

	bool IsDeviceOpened();
	bool IsVolumeDirty();

	static bool DistanceModelToString(DistanceModel model, string &name);
	static bool StringToDistanceModel(const string &name, DistanceModel &model);
};


//=========================================================================
// Methods
//=========================================================================

inline
void AudioServer::SetDeviceName(const string &name)
{
	Assert(!this->device_opened);
	this->device_name = name;
}


inline
const string &AudioServer::GetDeviceName()
{
	return this->device_name;
}


inline
const vector<string> &AudioServer::GetDeviceList()
{
	return this->device_list;
}


/**
	Updates listener attributes.
*/
inline
void AudioServer::UpdateListener()
{
	if (!this->device_opened)
		return;

	if (this->listener)
		this->listener->Update();
}


/**
	Starts a sound.
*/
inline
void AudioServer::PlaySound(Sound *snd)
{
	Assert(snd);
	if (!this->device_opened)
		return;

	if (this->category_allowed[snd->GetCategory()])
		snd->Play();
}


/**
	Updates a sound.
*/
inline
void AudioServer::UpdateSound(Sound *snd)
{
	Assert(snd);
	if (!this->device_opened)
		return;

	if (snd->IsPlaying())
		snd->Update();
}


inline
void AudioServer::UpdateStream(Sound *snd)
{
	Assert(snd && snd->IsStreaming());
	if (!this->device_opened)
		return;

	snd->UpdateStream();
}


/**
	Stops a sound.
*/
inline
void AudioServer::StopSound(Sound *snd)
{
	Assert(snd);
	if (!this->device_opened)
		return;

	if (snd->IsPlaying())
		snd->Stop();
}


/**
	Pause a sound.
*/
inline
void AudioServer::PauseSound(Sound *snd)
{
	Assert(snd);
	if (!this->device_opened)
		return;

	if (snd->IsPlaying())
		snd->Pause();
}


inline
void AudioServer::PlayPauseSound(Sound *snd)
{
	Assert(snd);
	if (!this->device_opened)
		return;

	snd->PlayPause();
}


/**
	Stops all sounds.
*/
inline
void AudioServer::StopAllSounds()
{
	if (!this->device_opened)
		return;

	for (Sound *snd = (Sound *)this->sounds_list->GetFront(); snd; snd = (Sound *)snd->GetNext())
		this->StopSound(snd);
}


/**
	Calls Update() on all playing sounds. This can be used to immediately
	commit changed without having to wait until Trigger().
*/
inline
void AudioServer::UpdateAllSounds()
{
	if (!this->device_opened)
		return;

	for (Sound *snd = (Sound *)this->sounds_list->GetFront(); snd; snd = (Sound *)snd->GetNext())
		this->UpdateSound(snd);
}


inline
void AudioServer::UpdateAllStreams()
{
	if (!this->device_opened)
		return;

	for (Sound *snd = (Sound *)this->sounds_list->GetFront(); snd; snd = (Sound *)snd->GetNext())
		if (snd->IsStreaming())
			this->UpdateStream(snd);
}


inline
void AudioServer::SetDistanceModel(DistanceModel model)
{
	this->distance_model = model;
	this->props_dirty = true;
}


inline
AudioServer::DistanceModel AudioServer::GetDistanceModel()
{
	return this->distance_model;
}


/**
	Sets the master volume.
*/
inline
void AudioServer::SetMasterVolume(float volume)
{
	this->master_volume = volume;
	this->volume_dirty = true;
}


/**
	Gets the master volume.
*/
inline
float AudioServer::GetMasterVolume()
{
	return this->master_volume;
}


/**
	Sets the category volume (0.0 .. 1.0).
*/
inline
void AudioServer::SetCategoryVolume(Sound::Category category, float volume)
{
	Assert(category >= 0 && category < Sound::SND_COUNT);
	this->category_volume[category] = volume;
	this->volume_dirty = true;
}


/**
	Gets the category volume.
*/
inline
float AudioServer::GetCategoryVolume(Sound::Category category)
{
	Assert(category >= 0 && category < Sound::SND_COUNT);
	return this->category_volume[category];
}


/**
	Gets whether sounds from category can be played.
*/
inline
bool AudioServer::GetCategoryAllowed(Sound::Category category)
{
	Assert(category >= 0 && category < Sound::SND_COUNT);
	return this->category_allowed[category];
}


inline
void AudioServer::SetDopplerFactor(float v)
{
	this->doppler_factor = v;
	this->props_dirty = true;
}


inline
float AudioServer::GetDopplerFactor() const
{
	return this->doppler_factor;
}


inline
void AudioServer::SetSpeedOfSound(float v)
{
	this->speed_of_sound = v;
	this->props_dirty = true;
}


inline
float AudioServer::GetSpeedOfSound() const
{
	return this->speed_of_sound;
}


/**
	Sets currnet listener.
*/
inline
void AudioServer::SetListener(Listener *l)
{
	this->listener = l;
	this->listener->SetDirty(true);
}


/**
	Returns @c true if currently device_opened.
*/
inline
bool AudioServer::IsDeviceOpened()
{
	return this->device_opened;
}


/**
	Is currently muted?
*/
inline
bool AudioServer::IsMuted()
{
	return this->muted;
}


inline
bool AudioServer::IsVolumeDirty()
{
	return this->volume_dirty;
}


#endif // __audioserver_h__

// vim:ts=4:sw=4:
