#ifndef __sceneserver_h__
#define __sceneserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sceneserver.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/rootserver.h"
#include "kernel/ref.h"
#include "framework/scene.h"

class Animator;


//=========================================================================
// SceneServer
//=========================================================================

/**
	@class SceneServer
	@ingroup Framework_Module
*/

class SceneServer : public RootServer<SceneServer>
{
//--- variables
protected:
	Ref<Root> scenes;


//--- methods
public:
	SceneServer(const char *id);
	virtual ~SceneServer();

	Scene *NewScene(const char* class_name);
	SceneObject *NewSceneObject(const char* class_name, const string *model_file = NULL);
	Animator *NewAnimator(const string &anim_file, void *buffer);

	void RenderScene(Scene *scene);
};


//=========================================================================
// Methods
//=========================================================================

inline
void SceneServer::RenderScene(Scene *scene)
{
	if (!scene)
		return;

	scene->Render();
}


#endif  // __sceneserver_h__

// vim:ts=4:sw=4:
