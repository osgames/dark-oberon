//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file mesh.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Added Deserialize method.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/mesh.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "kernel/serializer.h"

PrepareClass(Mesh, "Resource", s_NewMesh, s_InitMesh);


//=========================================================================
// Mesh
//=========================================================================

/**
	Constructor.
*/
Mesh::Mesh(const char *id) :
	SerializedResource(id),
	components(VC_NONE),
	coords(NULL),
	colors(NULL),
	primitives(NULL),
	vertices_count(0),
	primitives_count(0),
	triangles_count(0),
	data_size(0)
{
	Assert(GfxServer::GetInstance());

	this->type = RES_MESH;
}


/**
	Destructor.
*/
Mesh::~Mesh()
{
	this->Unload();
}


/**
*/
bool Mesh::Deserialize(Serializer &serializer, bool first)
{
	// call ancestor
	if (!Root::Deserialize(serializer, first))
		return false;

	this->Clear();

	if (!this->ReadVertices(serializer))
	{
		LogError1("Error reading vertices from: %s", this->file_name.c_str());

		// delete partially loaded data
		this->Clear();
		return false;
	}

	if (!this->ReadPrimitives(serializer))
	{
		LogError1("Error reading primitives from: %s", this->file_name.c_str());

		// delete partially loaded data
		this->Clear();
		return false;
	}
	
	return true;
}


bool Mesh::ReadVertices(Serializer &serializer)
{
	if (!serializer.GetGroup("vertices"))
		return false;

	// read components [one mandatory]
	this->components = 0;
	bool b_value;
	
	if (serializer.GetAttribute("coords", b_value) && b_value)
		this->components |= Mesh::VC_COORDS;
	if (serializer.GetAttribute("color", b_value) && b_value)
		this->components |= Mesh::VC_COLOR;

	if (!this->components)
	{
		LogError("Missing at least one vertex component");
		return false;
	}

	// read vertices count [one mandatory]
	this->vertices_count = serializer.GetGroupsCount();

	if (!this->vertices_count || !serializer.GetGroup("vertex"))
	{
		LogError("Missing at least one vertex");
		return false;
	}

	// create arrays
	if (this->components & Mesh::VC_COORDS) {
		this->coords = NEW vector2[this->vertices_count];
		Assert(this->coords);
		this->data_size += sizeof(this->coords[0]) * this->vertices_count;
	}
	if (this->components & Mesh::VC_COLOR) {
		this->colors = NEW vector4[this->vertices_count];
		Assert(this->colors);
		this->data_size += sizeof(this->colors[0]) * this->vertices_count;
	}

	// read vertices [one mandatory]
	for (ushort_t i = 0; i < this->vertices_count; i++)
	{
		// coords [mandatory]
		if (this->coords)
		{
			if (!serializer.GetAttribute("x", this->coords[i].x) ||
				!serializer.GetAttribute("y", this->coords[i].y))
			{
				LogError("Missing vertex coodinates");
				return false;
			}
		}

		// colors [mandatory]
		if (this->colors)
		{
			if (!serializer.GetAttribute("r", this->colors[i].x) ||
				!serializer.GetAttribute("g", this->colors[i].y) ||
				!serializer.GetAttribute("b", this->colors[i].z) ||
				!serializer.GetAttribute("a", this->colors[i].w))
			{
				LogError("Missing vertex color");
				return false;
			}
		}

		// go to next group
		if ((i < this->vertices_count - 1) && !serializer.GetNextGroup())
		{
			LogError("Vertices mismatch");
			return false;
		}
	}

	serializer.EndGroup();
	serializer.EndGroup();

	return true;
}


bool Mesh::ReadPrimitives(Serializer &serializer)
{
	if (!serializer.GetGroup("primitives"))
		return false;

	// read primitives count [one mandatory]
	this->primitives_count = serializer.GetGroupsCount();

	if (!this->primitives_count || !serializer.GetGroup("primitive"))
	{
		LogError("Missing at least one primitive");
		return false;
	}

	// create primitives
	this->primitives = NEW Primitive[this->primitives_count];
	Assert(this->primitives);

	this->data_size += sizeof(this->primitives[0]) * this->primitives_count;

	// read primitives [one mandatory]
	string type_name;
	Primitive::PrimitiveType primitive_type;
	ushort_t indices_count;
	ushort_t *indices;
	ushort_t i, j;

	for (i = 0; i < this->primitives_count; i++)
	{
		// read type of primitive [mandatory]
		if (!serializer.GetAttribute("type", type_name))
		{
			LogError("Missing primitive type");
			return false;
		}

		primitive_type = Primitive::StringToType(type_name);
		if (primitive_type == Primitive::PT_INVALID)
		{
			LogError1("Invalid primitive type: %s", type_name.c_str());
			return false;
		}

		// indices count [one mandatory]
		indices_count = serializer.GetGroupsCount();
		if (!indices_count || !serializer.GetGroup("index"))
		{
			LogError("Missing at least one vertex index in primitive");
			return false;
		}

		// create indices
		indices = NEW ushort_t[indices_count];
		this->data_size += sizeof(indices[0]) * indices_count;

		// read indices [one mandatory]
		for (j = 0; j < indices_count; j++)
		{
			// read value [mandatory]
			if (!serializer.GetValue(indices[j]))
			{
				LogError("Missing index value");
				return false;
			}

			// go to next index
			if ((j < indices_count - 1) && !serializer.GetNextGroup())
			{
				LogError("Indices mismatch");
				return false;
			}
		}

		serializer.EndGroup();

		// set primitive
		this->primitives[i].Set(primitive_type, indices, indices_count);

		// calculate triangles count
		switch (primitive_type)
		{
		case Primitive::PT_QUADS:       this->triangles_count = indices_count / 2; break;
		case Primitive::PT_TRIANGLES:   this->triangles_count = indices_count / 3; break;
		case Primitive::PT_STRIP:
		case Primitive::PT_FAN:         this->triangles_count = indices_count - 2; break;
		default:                        this->triangles_count = 0; break;
		}

		// go to next primitive
		if ((i < this->primitives_count - 1) && !serializer.GetNextGroup())
		{
			LogError("Primitives mismatch");
			return false;
		}
	}

	serializer.EndGroup();
	serializer.EndGroup();

	return true;
}


void Mesh::Clear()
{
	// delete mesh
	SafeDeleteArray(this->coords);
	SafeDeleteArray(this->colors);
	SafeDeleteArray(this->primitives);

	this->vertices_count = this->primitives_count = 0;
	this->data_size = 0;
	this->components = VC_NONE;
}


void Mesh::Set(vector2 *coords, vector4 *colors, Primitive *primitives, ushort_t vertices_count, ushort_t primitives_count, ulong_t data_size)
{
	this->Clear();

	if (coords)
		this->components |= VC_COORDS;
	if (colors)
		this->components |= VC_COLOR;
	if (this->components == 0)
		this->components = VC_NONE;

	this->coords = coords;
	this->colors = colors;
	this->primitives = primitives;
	
	this->vertices_count = vertices_count;
	this->primitives_count = primitives_count;
	this->data_size = data_size;
}


ulong_t Mesh::GetDataSize()
{
	return this->data_size;
}


// vim:ts=4:sw=4:
