#ifndef __listener_h__
#define __listener_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file listener.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/matrix.h"
#include "kernel/vector.h"


//=========================================================================
// Listener
//=========================================================================

/**
	@class Listener
	@ingroup Framework_Module

	Define listener properties for audio subsystem.
*/

class Listener {
	friend class AudioServer;

//--- variables
protected:
	matrix44 transform;
	vector3 velocity;

	bool move_dirty;

//--- methods
public:
	Listener();
	virtual ~Listener();

	void SetTransform(const matrix44& m);
	void SetVelocity(const vector3& v);

	const matrix44 &GetTransform() const;
	const vector3 &GetVelocity() const;

protected:
	void SetDirty(bool dirty);
	virtual void Update();
};


//=========================================================================
// Methods
//=========================================================================

/**
	Constructor.
*/
inline
Listener::Listener() :
	move_dirty(true)
{
	// empty
}


/**
	Destructor.
*/
inline
Listener::~Listener()
{
	// empty
}


/**
	Sets world space transform.
*/
inline
void Listener::SetTransform(const matrix44& m)
{
	this->transform = m;
	this->move_dirty = true;
}


/**
	Gets world space transform.
*/
inline
const matrix44& Listener::GetTransform() const
{
	return this->transform;
}


/**
	Set world space velocity.
*/
inline
void Listener::SetVelocity(const vector3& v)
{
	this->velocity = v;
	this->move_dirty = true;
}


/**
	Gets world space velocity.
*/
inline
const vector3& Listener::GetVelocity() const
{
	return this->velocity;
}


/**
	Sets all dirty flags. Used in AudioServer when listener is changed.
*/
inline
void Listener::SetDirty(bool dirty)
{
	this->move_dirty = dirty;
}


/**
	Updates listener.
*/
inline
void Listener::Update()
{
	ErrorMsg("Overwrite this method in subclass");
}


#endif // __openallistener_h__

// vim:ts=4:sw=4:
