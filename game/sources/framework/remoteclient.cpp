//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file remoteclient.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/remoteclient.h"
#include "framework/remoteserver.h"
#include "kernel/kernelserver.h"

PrepareClass(RemoteClient, "Root", s_NewRemoteClient, s_InitRemoteClient);


//=========================================================================
// RemoteClient
//=========================================================================

RemoteClient::RemoteClient(const char *id) :
	Root(id),
	remote_id(0),
	local(false)
{
	//
}


void RemoteClient::SendRemoteMessage(BaseMessage *message, RemoteClient *destination, double delay)
{
	RemoteServer::GetInstance()->SendRemoteMessage(message, this, destination, delay);
}


// vim:ts=4:sw=4:
