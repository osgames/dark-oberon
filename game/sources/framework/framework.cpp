//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file framework.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"


//=========================================================================
// Kernel module
//=========================================================================

extern "C" void Framework();


extern bool s_InitInputServer (Class *, KernelServer *);
extern Object *s_NewInputServer (const char *name);

extern bool s_InitResourceServer (Class *, KernelServer *);
extern Object *s_NewResourceServer (const char *name);
extern bool s_InitResource (Class *, KernelServer *);
extern Object *s_NewResource (const char *name);

extern bool s_InitGfxServer (Class *, KernelServer *);
extern Object *s_NewGfxServer (const char *name);
extern bool s_InitTexture (Class *, KernelServer *);
extern Object *s_NewTexture (const char *name);
extern bool s_InitMesh (Class *, KernelServer *);
extern Object *s_NewMesh (const char *name);
extern bool s_InitModel (Class *, KernelServer *);
extern Object *s_NewModel (const char *name);
extern bool s_InitAnimation (Class *, KernelServer *);
extern Object *s_NewAnimation (const char *name);
extern bool s_InitFont (Class *, KernelServer *);
extern Object *s_NewFont (const char *name);

extern bool s_InitAudioServer (Class *, KernelServer *);
extern Object *s_NewAudioServer (const char *name);
extern bool s_InitSound (Class *, KernelServer *);
extern Object *s_NewSound (const char *name);
extern bool s_InitSoundResource (Class *, KernelServer *);
extern Object *s_NewSoundResource (const char *name);

extern bool s_InitSceneServer (Class *, KernelServer *);
extern Object *s_NewSceneServer (const char *name);
extern bool s_InitSceneNode (Class *, KernelServer *);
extern Object *s_NewSceneNode (const char *name);
extern bool s_InitShapeNode (Class *, KernelServer *);
extern Object *s_NewShapeNode (const char *name);
extern bool s_InitTransformNode (Class *, KernelServer *);
extern Object *s_NewTransformNode (const char *name);
extern bool s_InitSceneObject (Class *, KernelServer *);
extern Object *s_NewSceneObject (const char *name);
extern bool s_InitScene (Class *, KernelServer *);
extern Object *s_NewScene (const char *name);
extern bool s_InitIsoScene (Class *, KernelServer *);
extern Object *s_NewIsoScene (const char *name);
extern bool s_InitAnimator (Class *, KernelServer *);
extern Object *s_NewAnimator (const char *name);

extern bool s_InitAttributes (Class *, KernelServer *);
extern Object *s_NewAttributes (const char *name);

extern bool s_InitSessionServer (Class *, KernelServer *);
extern Object *s_NewSessionServer (const char *name);
extern bool s_InitSessionClientContext (Class *, KernelServer *);
extern Object *s_NewSessionClientContext (const char *name);
extern bool s_InitSessionClient (Class *, KernelServer *);
extern Object *s_NewSessionClient (const char *name);
extern bool s_InitSessionServerContext (Class *, KernelServer *);
extern Object *s_NewSessionServerContext (const char *name);


extern bool s_InitRemoteServer (Class *, KernelServer *);
extern Object *s_NewRemoteServer (const char *name);
extern bool s_InitRemoteClient (Class *, KernelServer *);
extern Object *s_NewRemoteClient (const char *name);


void Framework()
{
	KernelServer::GetInstance()->RegisterClass("InputServer", s_InitInputServer, s_NewInputServer);

	KernelServer::GetInstance()->RegisterClass("ResourceServer", s_InitResourceServer, s_NewResourceServer);
	KernelServer::GetInstance()->RegisterClass("Resource", s_InitResource, s_NewResource);

	KernelServer::GetInstance()->RegisterClass("GfxServer", s_InitGfxServer, s_NewGfxServer);
	KernelServer::GetInstance()->RegisterClass("Texture", s_InitTexture, s_NewTexture);
	KernelServer::GetInstance()->RegisterClass("Mesh", s_InitMesh, s_NewMesh);
	KernelServer::GetInstance()->RegisterClass("Model", s_InitModel, s_NewModel);
	KernelServer::GetInstance()->RegisterClass("Animation", s_InitAnimation, s_NewAnimation);
	KernelServer::GetInstance()->RegisterClass("Font", s_InitFont, s_NewFont);

	KernelServer::GetInstance()->RegisterClass("AudioServer", s_InitAudioServer, s_NewAudioServer);
	KernelServer::GetInstance()->RegisterClass("Sound", s_InitSound, s_NewSound);
	KernelServer::GetInstance()->RegisterClass("SoundResource", s_InitSoundResource, s_NewSoundResource);	

	KernelServer::GetInstance()->RegisterClass("SceneServer", s_InitSceneServer, s_NewSceneServer);
	KernelServer::GetInstance()->RegisterClass("SceneNode", s_InitSceneNode, s_NewSceneNode);
	KernelServer::GetInstance()->RegisterClass("ShapeNode", s_InitShapeNode, s_NewShapeNode);
	KernelServer::GetInstance()->RegisterClass("TransformNode", s_InitTransformNode, s_NewTransformNode);
	KernelServer::GetInstance()->RegisterClass("SceneObject", s_InitSceneObject, s_NewSceneObject);
	KernelServer::GetInstance()->RegisterClass("Scene", s_InitScene, s_NewScene);
	KernelServer::GetInstance()->RegisterClass("IsoScene", s_InitIsoScene, s_NewIsoScene);
	KernelServer::GetInstance()->RegisterClass("Animator", s_InitAnimator, s_NewAnimator);

	KernelServer::GetInstance()->RegisterClass("Attributes", s_InitAttributes, s_NewAttributes);

	KernelServer::GetInstance()->RegisterClass("SessionServer", s_InitSessionServer, s_NewSessionServer);
	KernelServer::GetInstance()->RegisterClass("SessionClientContext", s_InitSessionClientContext, s_NewSessionClientContext);
	KernelServer::GetInstance()->RegisterClass("SessionClient", s_InitSessionClient, s_NewSessionClient);
	KernelServer::GetInstance()->RegisterClass("SessionServerContext", s_InitSessionServerContext, s_NewSessionServerContext);

	KernelServer::GetInstance()->RegisterClass("RemoteServer", s_InitRemoteServer, s_NewRemoteServer);
	KernelServer::GetInstance()->RegisterClass("RemoteClient", s_InitRemoteClient, s_NewRemoteClient);
}

// vim:ts=4:sw=4:
