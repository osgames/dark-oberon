#ifndef __gfxstructures_h__
#define __gfxstructures_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file gfxstructures.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"


//=========================================================================
// VideoMode
//=========================================================================

class VideoMode
{
//--- embeded
public:
	/// Bit depths.
	enum Bpp
	{
		BPP_UNDEFINED = 0,  ///< Undefined value.
		BPP_16 = 16,        ///< 16 bit colors.
		BPP_24 = 24         ///< 24 bit colors.
	};

//--- variables
public:
	ushort_t width;
	ushort_t height;
	Bpp bpp;

//--- methods
public:
	VideoMode();
	static Bpp IntToBpp(int i_bpp);
	static int BppToInt(Bpp e_bpp);
};


inline
VideoMode::VideoMode() :
	width(640),
	height(480),
	bpp(BPP_16)
{
}


inline
VideoMode::Bpp VideoMode::IntToBpp(int i_bpp)
{
	return VideoMode::Bpp(i_bpp);
}


inline
int VideoMode::BppToInt(Bpp e_bpp)
{
	return (int)e_bpp;
}


//=========================================================================
// Viewport
//=========================================================================

/**
	@class Viewport
	@ingroup Framework_Module

	@brief Viewport.
*/

class Viewport
{
//--- variables
public:
    int x;
    int y;
    ushort_t width;
    ushort_t height;

//--- methods
public:
	Viewport();
	void Set(int x, int y, ushort_t width, ushort_t height);
};


inline
Viewport::Viewport() :
	x(0),
	y(0),
	width(640),
	height(480)
{
	//
}


inline
void Viewport::Set(int x, int y, ushort_t width, ushort_t height)
{
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
}


//=========================================================================
// Projection
//=========================================================================

/**
	@class Projection
	@ingroup Framework_Module

	@brief Projection.
*/

class Projection
{
//--- embeded
public:
	enum Type
	{
		PT_ORTHO,
		PT_ORTHO_2D
	};

//--- variables
public:
	Type type;

    double left;
	double right;
	double bottom;
	double top;
	double front;
	double back;

	float translation[3];

//--- methods
public:
	Projection();

	void SetOrtho(double left, double right, double bottom, double top, double front, double back);
	void SetOrtho2D(double left, double right, double bottom, double top);
	void SetTranslation(float x, float y, float z);

	double GetWidth();
	double GetHeight();
	double GetLength();
};


inline
Projection::Projection() :
	type(PT_ORTHO_2D),
	left(0),
	right(1),
	bottom(0),
	top(1),
	front(0),
	back(1)
{
	this->translation[0] = 0;
	this->translation[1] = 0;
	this->translation[2] = 0;
}


inline
void Projection::SetOrtho(double left, double right, double bottom, double top, double front, double back)
{
	this->type = PT_ORTHO;
	this->left = left;
	this->right = right;
	this->bottom = bottom;
	this->top = top;
	this->front = front;
	this->back = back;
}


inline
void Projection::SetOrtho2D(double left, double right, double bottom, double top)
{
	this->type = PT_ORTHO_2D;
	this->left = left;
	this->right = right;
	this->bottom = bottom;
	this->top = top;
}


inline
void Projection::SetTranslation(float x, float y, float z)
{
	this->translation[0] = x;
	this->translation[1] = y;
	this->translation[2] = z;
}


inline
double Projection::GetWidth()
{
	return this->right - this->left;
}


inline
double Projection::GetHeight()
{
	return this->top - this->bottom;
}


inline
double Projection::GetLength()
{
	return this->front - this->back;
}


#endif  // __gfxstructures_h__

// vim:ts=4:sw=4:
