#ifndef __meshstructures_h__
#define __meshstructures_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file meshstructures.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/logserver.h"


//=========================================================================
// Primitive
//=========================================================================

class Primitive {
//--- embeded
public:
	/// Bit depths.
	enum PrimitiveType {
		PT_TRIANGLES,  ///< Triangles.
		PT_QUADS,      ///< Quads.
		PT_STRIP,      ///< Triangle strip.
		PT_FAN,        ///< Triangle fan.
		PT_INVALID     ///< Invalid primitive type.
	};

//--- variables
private:
	PrimitiveType type;

	ushort_t *indices;
	ushort_t indices_count;

//--- methods
public:
	Primitive();
	~Primitive();

	void Set(PrimitiveType type, ushort_t *indices, ushort_t indices_count);
	PrimitiveType GetType();

	ushort_t GetIndex(ushort_t id);
	ushort_t *GetIndices();
	ushort_t GetIndicesCount();

	static bool TypeToString(PrimitiveType type, string &name);
	static PrimitiveType StringToType(const string &name);
};


inline
Primitive::Primitive() :
	type(PT_TRIANGLES),
	indices(NULL),
	indices_count(0)
{
	//
}


inline
Primitive::~Primitive()
{
	if (this->indices)
		delete[] this->indices;
}


inline
void Primitive::Set(PrimitiveType type, ushort_t *indices, ushort_t indices_count)
{
	if (this->indices)
		delete this->indices;

	this->type = type;
	this->indices = indices;
	this->indices_count = indices_count;
}


inline
Primitive::PrimitiveType Primitive::GetType()
{
	return this->type;
}


inline
ushort_t Primitive::GetIndex(ushort_t id)
{
	Assert(id < this->indices_count);
	return this->indices[id];
}


inline
ushort_t *Primitive::GetIndices()
{
	return this->indices;
}


inline
ushort_t Primitive::GetIndicesCount()
{
	return this->indices_count;
}


inline
bool Primitive::TypeToString(Primitive::PrimitiveType type, string &name)
{
	switch (type)
	{
	case PT_TRIANGLES: name = "triangles"; return true;
	case PT_QUADS:     name = "quads";     return true;
	case PT_STRIP:     name = "strip";     return true;
	case PT_FAN:       name = "fan";       return true;
	default:
		LogWarning1("Invalid primitive type: %d", type);
		return false;
	}
}


inline
Primitive::PrimitiveType Primitive::StringToType(const string &name)
{
	if (name == "triangles")   return PT_TRIANGLES;
	else if (name == "quads")  return PT_QUADS;
	else if (name == "strip")  return PT_STRIP;
	else if (name == "fan")    return PT_FAN;
	else
	{
		LogWarning1("Invalid primitive type '%s'", name.c_str());
		return PT_INVALID;
	}
}


#endif  // __meshstructures_h__

// vim:ts=4:sw=4:
