#ifndef __sceneobject_h__
#define __sceneobject_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sceneobject.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Visible flag.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/root.h"
#include "kernel/vector.h"
#include "framework/model.h"


//=========================================================================
// SceneObject
//=========================================================================

/**
	@class SceneObject
	@ingroup Framework_Module
*/

class SceneObject : public Root
{
//--- embeded
public:
	typedef void (* RenderCallbackFun)(void *);

protected:
	struct ModelItem
	{
		string model_file;
		Ref<Model> model;
		Ref<Root> animators;
	};

	typedef vector<ModelItem *> ModelsArray;


//--- methods
public:
	SceneObject(const char *id);
	virtual ~SceneObject();

	byte_t AddModel(const string &model_file);
	bool ActivateModel(byte_t id);
	Model *GetActiveModel() const;
	void SetOverlayColor(const vector3 &color);

	void SetPosition(const vector3 &position);
	void SetSize(const vector2 &size);
	const vector3 &GetPosition() const;
	const vector2 &GetSize() const;
	void SetVisible(bool visible);
	bool IsVisible() const;
	void SetRenderCallbacks(RenderCallbackFun render_begin, RenderCallbackFun render_end);

	bool LoadResources();
	bool IsResourcesValid() const;

	void UpdateAnimators();
	void PauseAnimators();
	void Render(bool callbacks = true) const;

protected:
	void UnloadResources();


//--- variables
protected:
	vector3 position;            ///< Position of the object.
	vector2 size;                ///< Width and height of the object.
	vector3 overlay_color;       ///< Color used for overlay image.
	bool visible;                ///< Whether scene object will be drawn.

	ModelsArray models;          ///< Array of models.
	ModelItem *active_model;     ///< Points to active model item.

	bool resources_valid;
	RenderCallbackFun callback_render_begin;
	RenderCallbackFun callback_render_end;

};


//=========================================================================
// Methods
//=========================================================================

/**
*/
inline
void SceneObject::Render(bool callbacks) const
{
	Assert(this->resources_valid);
	Assert(this->visible);

	if (callbacks && this->callback_render_begin)
		this->callback_render_begin(this->GetData());

	this->active_model->model->Render();

	if (callbacks && this->callback_render_end)
		this->callback_render_end(this->GetData());
}


inline
void SceneObject::SetOverlayColor(const vector3 &color)
{
	this->overlay_color = color;
}


inline
Model *SceneObject::GetActiveModel() const
{
	return this->active_model->model.GetUnsafe();
}


inline
void SceneObject::SetPosition(const vector3 &position)
{
	this->position = position;
}


inline
const vector3 &SceneObject::GetPosition() const
{
	return this->position;
}


inline
void SceneObject::SetSize(const vector2 &size)
{
	this->size = size;
}


inline
const vector2 &SceneObject::GetSize() const
{
	return this->size;
}


inline
void SceneObject::SetVisible(bool visible)
{
	this->visible = visible;
}


inline
bool SceneObject::IsVisible() const
{
	return this->visible;
}


inline
bool SceneObject::IsResourcesValid() const
{
	return this->resources_valid;
}


inline
void SceneObject::SetRenderCallbacks(RenderCallbackFun render_begin, RenderCallbackFun render_end)
{
	this->callback_render_begin = render_begin;
	this->callback_render_end = render_end;
}


#endif  // __sceneobject_h__

// vim:ts=4:sw=4:
