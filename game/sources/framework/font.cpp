//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file font.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added support for shadow.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"
#include "framework/font.h"
#include "framework/gfxserver.h"

PrepareClass(Font, "Resource", s_NewFont, s_InitFont);


//=========================================================================
// Font
//=========================================================================

/**
	Constructor.
*/
Font::Font(const char *id) :
	Resource(id),
	shadow_x(0),
	shadow_y(0),
	shadow_alpha(255),
	height(0),
	glyphs_count(0)
{
	Assert(GfxServer::GetInstance());

	this->char_size.width = 12;
	this->char_size.height = 12;
	this->char_size.resolution_x = 96;
	this->char_size.resolution_y = 96;

	this->type = RES_FONT;
}


/**
	Destructor.
*/
Font::~Font()
{
	this->Unload();
}


/**
	Prints given text.
*/
void Font::Print(const string &/*text*/) const
{
	ErrorMsg("Overwrite this method in subclass");
}

// vim:ts=4:sw=4:
