#ifndef __sessionclient_h__
#define __sessionclient_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionclient.h
	@ingroup Framework_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2009

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/sessionservercontext.h"
#include "kernel/ipcserver.h"
#include "kernel/ref.h"
#include "kernel/ipcaddress.h"
#include "kernel/guid.h"

class Attributes;


/**
	@class SessionClientContext

	@brief A session client object can discover open sessions in a LAN,
	join an open session and get information about the session from
	the session server.

	When the session is started by the session server, the session client
	will configure and open the local game client object.
*/
class SessionClient : public IpcServer
{
//--- methods
public:
	SessionClient(const char *id);
	virtual ~SessionClient();

	void SetAppName(const char *name);
	const char *GetAppName() const;

	void SetAppVersion(const char *version);
	const char *GetAppVersion() const;

	bool Open();
	void Close();
	bool IsOpened() const;

	bool Trigger();
	virtual void ProcessData(uint_t channel_id, const IpcBuffer &buffer);

	void SetClientAttr(const char *name, const char *val);
	const char *GetClientAttr(const char *name);

	int GetServersCount() const;
	SessionServerContext *GetServerAt(int index) const;
	SessionServerContext *GetJoinedServer() const;

	bool JoinSession(const char *session_guid);
	bool LeaveSession();
	bool IsJoining() const;
	bool IsJoined() const;
	bool IsJoinDenied() const;

private:
	bool RegisterSession(const char *guid_string, const char *host, ushort_t port);
	SessionServerContext *FindServerContextByGuid(const char *guid);
	SessionServerContext *FindServerContext(uint_t channel_id);
	void CleanupExpiredSessions();
	void SendClientAttrs();
	void StartGame(const char *server_host, const char *server_port);


//--- variables
private:
	string app_name;
	string app_version;
	double time;
	bool opened;
	bool joined;
	bool join_denied;

	Ref<Root> server_contexts;
	Ref<SessionServerContext> join_server;
	Ref<Attributes> attributes;
	//Ref<NetServer> net_server;

	uint_t unique_number;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Sets application name (must match with session server).
*/
inline
void SessionClient::SetAppName(const char *name)
{
	Assert(name);
	this->app_name = name;
}


/**
	Gets application name.
*/
inline
const char *SessionClient::GetAppName() const
{
	return this->app_name.empty() ? NULL : this->app_name.c_str();
}


/**
	Set application version string (must match with session server).
*/
inline
void SessionClient::SetAppVersion(const char *version)
{
	Assert(version);
	this->app_version = version;
}


/**
	Gets application version string.
*/
inline
const char *SessionClient::GetAppVersion() const
{
	return this->app_version.empty() ? NULL : this->app_version.c_str();
}


/**
	Returns @c true if currently opened.
*/
inline
bool SessionClient::IsOpened() const
{
	return this->opened;
}


inline
bool SessionClient::IsJoining() const
{
	return this->join_server.IsValid() && !this->joined && !this->join_denied;
}


/**
	Return true if currently joined to a session.
*/
inline
bool SessionClient::IsJoined() const
{
	return this->joined;
}


/**
	Return status of join denied flag.
*/
inline
bool SessionClient::IsJoinDenied() const
{
	return this->join_denied;
}


#endif

// vim:ts=4:sw=4:
