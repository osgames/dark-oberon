#ifndef __soundresource_h__
#define __soundresource_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file soundresource.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/resource.h"

class AudioFile;


//=========================================================================
// SoundResource
//=========================================================================

/**
	@class SoundResource
	@ingroup Framework_Module

	A sound resource is a container for sound data which can be played back
	by the AudioServer subsystem. 

	Sound resources are generally shared and are referenced by 
	Sound objects (there should be one Sound object per "sound instance",
	but several Sound objects should reference the same SoundResource object,
	if the Sound objects sound the same).
*/
    
class SoundResource : public Resource
{
//--- variables
protected:
	bool streaming;
	bool looping;

	AudioFile *audio_file;

//--- methods
public:
	SoundResource(const char *id);
	virtual ~SoundResource();

	virtual bool UpdateStream();
	virtual void RewindStream();

	void SetStreaming(bool b);
	void SetLooping(bool b);

	bool  IsStreaming() const;
	bool  IsLooping() const;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Sets static/streaming type.
*/
inline
void SoundResource::SetStreaming(bool b)
{
	Assert(!this->loaded);
	this->streaming = b;
}


/**
	Gets static/streaming type.
*/
inline
bool SoundResource::IsStreaming() const
{
	return this->streaming;
}


/**
	Sets the looping behaviour.
*/
inline
void SoundResource::SetLooping(bool b)
{
	this->looping = b;
}


/**
	Gets the looping behaviour.
*/
inline
bool SoundResource::IsLooping() const
{
	return this->looping;
}


#endif // __soundresource_h__

// vim:ts=4:sw=4:
