//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file inputserver.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Registering script callback functions for actions.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/inputserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/scriptserver.h"

PrepareScriptClass(InputServer, "Root", s_NewInputServer, s_InitInputServer, s_InitInputServer_cmds);


//=========================================================================
// Key names
//=========================================================================

char *key_names[KEY_COUNT] = {
	"",
	"F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", 
	"F14", "F15", "F16", "F17", "F18", "F19", "F20", "F21", "F22", "F23", "F24", "F25",
	"Tab", "Backspace", "Enter", "Escape", "Space",
	"Up", "Down", "Left", "Right",
	"Insert", "Delete", "Home", "End", "Page Up", "Page Down",
	"Left Shift", "Right Shift", "Left Ctrl", "Right Ctrl", "Left Alt", "Right Alt",
	"",
	"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
	"Numpad /", "Numpad *", "Numpad -", "Numpad +", "Numpad .", "Numpad =", "Numpad Enter",
	"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
	"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
	"`", "'", "-", "=", "[", "]", "/", "\\", ",", ".", ";",
	"Numpad 0", "Numpad 1", "Numpad 2", "Numpad 3", "Numpad 4",
	"Numpad 5", "Numpad 6", "Numpad 7", "Numpad 8", "Numpad 9", 
	"Print Screen", "Scroll Lock", "Pause",
	"Left Win", "Right Win", "Menu",
	"Left Button", "Right Button", "Middle Button", "Button 4",
	"Button 5", "Button 6", "Button 7", "Button 8"
};


char *key_identifiers[KEY_COUNT] = {
	"",
	"f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12", "f13", 
	"f14", "f15", "f16", "f17", "f18", "f19", "f20", "f21", "f22", "f23", "f24", "f25",
	"tab", "backspace", "enter", "escape", "space",
	"up", "down", "left", "right",
	"insert", "delete", "home", "end", "pageup", "pagedn",
	"lshift", "rshift", "lctrl", "rctrl", "lalt", "ralt",
	"",
	"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
	"num/", "num*", "num-", "num+", "num.", "num=", "numenter",
	"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", 
	"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
	"`", "'", "-", "=", "[", "]", "/", "\\", ",", ".", ";",
	"num0", "num1", "num2", "num3", "num4",
	"num5", "num6", "num7", "num8", "num9", 
	"prtsc", "scrlock", "pause",
	"lwin", "rwin", "menu",
	"lbutton", "rbutton", "mbutton", "button4",
	"button5", "button6", "button7", "button8"
};


char *key_state_identifiers[KS_COUNT] = {
	"up", "down", "pressed"
};


char *slider_identifiers[SLIDER_COUNT] = {
	"x", "y", "wheel",
	"relx", "rely", "relwheel"
};


//=========================================================================
// InputServer
//=========================================================================

InputServer::InputServer(const char *id) :
	RootServer<InputServer>(id),
	callback_key(NULL),
	callback_char(NULL),
	callback_position(NULL),
	callback_relposition(NULL),
	callback_wheel(NULL),
	callback_relwheel(NULL),
	gui_callback_key(NULL),
	gui_callback_char(NULL),
	gui_callback_position(NULL),
	gui_callback_relposition(NULL),
	gui_callback_wheel(NULL),
	gui_callback_relwheel(NULL),
	mouse_x(0),
	mouse_y(0),
	mouse_absx(0),
	mouse_absy(0),
	mouse_reldx(0),
	mouse_reldy(0),
	mouse_wheel(0),
	mouse_fixed(false)
{
	//
}


InputServer::~InputServer()
{
	this->UnbindAll();
}


bool InputServer::Trigger()
{
	BindInfo *bind_info;

	// emit pressed events
	for (size_t i = 0; i < this->pressed_list.size(); i++)
	{
		if (
			this->keys[this->pressed_list[i]].pressed &&
			(bind_info = this->keys[this->pressed_list[i]].bind_info[KS_PRESSED])
		)
			this->EmitAction(bind_info->action, 1);
	}

	// reset relative positions
	this->mouse_reldx = this->mouse_reldy = 0;

	return true;
}


void InputServer::RegisterCallbacks()
{
	ErrorMsg("Overwrite this method in subclass");
}


void InputServer::WaitForEvent()
{
	ErrorMsg("Overwrite this method in subclass");
}


void InputServer::SetMousePosition(ushort_t x, ushort_t y)
{
	ErrorMsg("Overwrite this method in subclass");
}


const char *InputServer::GetKeyName(KeyCode key)
{
	return key_names[key];
}


KeyCode InputServer::GetKeyCode(const string &key_id)
{
	for (int i = 0; i < KEY_COUNT; i++)
	{
		if (key_id == key_identifiers[i])
			return KeyCode(i);
	}

	return KEY_INVALID;
}


KeyState InputServer::GetKeyState(const string &state_id)
{
	for (int i = 0; i < KS_COUNT; i++)
	{
		if (state_id == key_state_identifiers[i])
			return KeyState(i);
	}

	return KS_INVALID;
}


SliderCode InputServer::GetSliderCode(const string &slider_id)
{
	for (int i = 0; i < SLIDER_COUNT; i++)
	{
		if (slider_id == slider_identifiers[i])
			return SliderCode(i);
	}

	return SLIDER_INVALID;
}


bool InputServer::Bind(const char *definition, const char *action)
{
	// input values cannot be empty
	if (!(definition && *definition))
		return false;

	string def = definition;

	string key_id;
	string state_id;
	string slider_id;

	KeyCode key = KEY_INVALID;
	KeyState state = KS_INVALID;
	SliderCode slider = SLIDER_INVALID;

	// find separator
	str_pos_t pos = def.find(':');

	// get key/state
	if (pos != string::npos)
	{
		key_id.assign(def, 0, pos);
		state_id.assign(def, pos + 1, def.npos);

		if (key_id.empty())
		{
			LogWarning1("Missing key in bind definition: %s", definition);
			return false;
		}
		if (state_id.empty())
		{
			LogWarning1("Missing key state in bind definition: %s", definition);
			return false;
		}

		// check key
		key = this->GetKeyCode(key_id);
		if (key == KEY_INVALID)
		{
			LogWarning1("Undefined key identifier in bind definition: %s", definition);
			return false;
		}

		// check state
		state = this->GetKeyState(state_id);
		if (state == KS_INVALID)
		{
			LogWarning1("Undefined key state in bind definition: %s", definition);
			return false;
		}
	}

	// get slider
	else
	{
		slider_id = def;

		// check slider
		slider = this->GetSliderCode(slider_id);
		if (slider == SLIDER_INVALID)
		{
			LogWarning1("Undefined slider identifier in bind definition: %s", definition);
			return false;
		}
	}

	// remove previous binding
	RemoveBind(key, state, slider);

	// add new binding
	if (action && *action)
	{
		// add bind definition
		BindInfo *bind_info = NEW BindInfo(definition, action);
		this->bind_list.push_back(bind_info);

		// add definition to pressed list
		if (state == KS_PRESSED)
			this->pressed_list.push_back(key);

		// set bind links
		if (key != KEY_INVALID)
			this->keys[key].bind_info[state] = bind_info;
		else
			this->sliders[slider].bind_info = bind_info;
	}

	return true;
}


bool InputServer::RemoveBind(KeyCode key, KeyState state, SliderCode slider)
{
	BindInfo *bind_info = key != KEY_INVALID ? this->keys[key].bind_info[state] : this->sliders[slider].bind_info;

	if (!bind_info)
		return false;

	Assert(this->bind_list.size() > 0);

	// find and remove bind definition
	size_t i;
	if (state == KS_PRESSED)
	{
		Assert(this->pressed_list.size() > 0);

		for (i = 0; i < this->pressed_list.size(); i++)
			if (this->pressed_list[i] == key)
			{
				this->pressed_list.erase(this->pressed_list.begin() + i);
				break;
			}
	}

	for (i = 0; i < this->bind_list.size(); i++)
		if (this->bind_list[i] == bind_info)
		{
			this->bind_list.erase(this->bind_list.begin() + i);
			break;
		}

	// release info
	delete bind_info;

	// remove bind links
	if (key != KEY_INVALID)
		this->keys[key].bind_info[state] = NULL;
	else
		this->sliders[slider].bind_info = NULL;

	return true;
}


void InputServer::UnbindAll()
{
	// release all infos
	size_t k;
	for (k = 0; k < this->bind_list.size(); k++)
		delete this->bind_list[k];
	
	// clear lists
	this->bind_list.clear();
	this->pressed_list.clear();

	// clear links
	int i, j;
	for (i = 0; i < KEY_COUNT; i++)
	{
		for (j = 0; j < KS_COUNT; j++)
			this->keys[i].bind_info[j] = NULL;
	}

	for (i = 0; i < SLIDER_COUNT; i++)
		this->sliders[i].bind_info = NULL;
}


void InputServer::EmitAction(const string &action, int pos)
{
	/// @todo Akcie volat cez hashovaciu tabulku. (Kluc akcia, hodnoty CallbaciInfo).
	ActionIterator it;
	for (it = this->callback_action.begin(); it != this->callback_action.end(); it++)
	{
		if (it->function)
		{
			it->function(action, pos, it->data);
		}
		else
		{
			Assert(!it->script_function.empty());
			Assert(ScriptServer::GetInstance());

			string code = it->script_function + "(\"" + action + "\")";
			string result;

			ScriptServer::GetInstance()->RunCode(code.c_str(), result);
		}
	}
}


// vim:ts=4:sw=4:
