#ifndef __resourceserver_h__
#define __resourceserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file resourceserver.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Creating resource using given DataFile.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/rootserver.h"
#include "framework/resource.h"


//=========================================================================
// ResourceServer
//=========================================================================

/**
	@class ResourceServer
	@ingroup Framework_Module

	@brief Central resource server. Creates and manages resource objects.

	Resources are objects which provide several types of data (or data
	streams) to the application, and can unload and reload themselves
	on request.
*/

class ResourceServer : public RootServer<ResourceServer>
{
	friend class Resource;

//--- variables
protected:
	Ref<Root> resources[Resource::RES_COUNT];

//--- methods
public:
	ResourceServer(const char *id);
	virtual ~ResourceServer();

	Resource *FindResource(const string &path, Resource::Type type);
	Resource *NewResource(const char *class_name, const string &path, Resource::Type type, const char *modifier = NULL);
	Resource *NewResource(const char *class_name, DataFile *data_file, Resource::Type type, const char *modifier = NULL);
	bool LoadResources(Resource::Type type);
	bool LoadAllResources();
	void UnloadResources(Resource::Type type);
	void UnloadAllResources();

	Root* GetResourceList(Resource::Type type);
	string GetResourceId(const string &path);

	ushort_t GetResourcesCount(Resource::Type type);
	uint_t   GetResourcesSize(Resource::Type type);
	ushort_t GetAllResourcesCount();
	uint_t   GetAllResourcesSize();
};


//=========================================================================
// Methods
//=========================================================================

inline
bool ResourceServer::LoadAllResources()
{
	bool ok = true;

	for (int i = 0; i < Resource::RES_COUNT; i++)
		ok &= this->LoadResources(Resource::Type(i));

	return ok;
}


inline
void ResourceServer::UnloadAllResources()
{
	for (int i = 0; i < Resource::RES_COUNT; i++)
		this->UnloadResources(Resource::Type(i));
}


inline
ushort_t ResourceServer::GetAllResourcesCount()
{
	ushort_t count = 0;

	for (int i = 0; i < Resource::RES_COUNT; i++)
		count += this->GetResourcesCount(Resource::Type(i));

	return count;
}


inline
uint_t ResourceServer::GetAllResourcesSize()
{
	uint_t size = 0;

	for (int i = 0; i < Resource::RES_COUNT; i++)
		size += this->GetResourcesSize(Resource::Type(i));

	return size;
}


#endif  // __resourceserver_h__

// vim:ts=4:sw=4:
