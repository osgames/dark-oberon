#ifndef __mesh_h__
#define __mesh_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file mesh.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Added Deserialize method.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/vector.h"
#include "framework/resource.h"
#include "framework/meshstructures.h"


//=========================================================================
// Mesh
//=========================================================================

/**
	@class Mesh
	@ingroup Framework_Module
*/
    
class Mesh : public SerializedResource
{
//--- embeded
public:
	enum VertexComponent
	{
		VC_NONE   = 0,
		VC_COORDS = (1 << 0),
		VC_COLOR  = (1 << 1),
		VC_ALL    = ((1 << 2) - 1),
	};

//--- variables
protected:
	byte_t components;

	vector2 *coords;
	vector4 *colors;
	Primitive *primitives;

	ushort_t vertices_count;
	ushort_t primitives_count;
	ushort_t triangles_count;
	ulong_t data_size;

//--- methods
public:
	Mesh(const char *id);
	virtual ~Mesh();

	void Set(vector2 *coords, vector4 *colors, Primitive *primitives, ushort_t vertices_count, ushort_t primitives_count, ulong_t data_size);

	vector2 *GetCoords();
	vector2 &GetCoord(ushort_t id);
	vector4 *GetColors();
	vector4 &GetColor(ushort_t id);
	Primitive *GetPrimitives();
	Primitive &GetPrimitive(ushort_t id);

	ushort_t GetVerticesCount();
	ushort_t GetPrimitivesCount();
	ushort_t GetTrianglesCount();

	byte_t GetComponents();
	bool TestComponents(byte_t mask);

	virtual ulong_t GetDataSize();

protected:
	virtual void Clear();

	// serializing
	virtual bool Deserialize(Serializer &serializer, bool first);
	bool ReadVertices(Serializer &serializer);
	bool ReadPrimitives(Serializer &serializer);
};


//=========================================================================
// Methods
//=========================================================================

inline
vector2 *Mesh::GetCoords()
{
	return this->coords;
}


inline
vector2 &Mesh::GetCoord(ushort_t id)
{
	Assert(id < this->vertices_count);
	return this->coords[id];
}


inline
vector4 *Mesh::GetColors()
{
	return this->colors;
}


inline
vector4 &Mesh::GetColor(ushort_t id)
{
	Assert(id < this->vertices_count);
	return this->colors[id];
}


inline
Primitive *Mesh::GetPrimitives()
{
	return this->primitives;
}


inline
Primitive &Mesh::GetPrimitive(ushort_t id)
{
	Assert(id < this->primitives_count);
	return this->primitives[id];
}


inline
byte_t Mesh::GetComponents()
{
	return this->components;
}


inline
bool Mesh::TestComponents(byte_t mask)
{
	return ((this->components & mask) == mask);
}


inline
ushort_t Mesh::GetVerticesCount()
{
	return this->vertices_count;
}


inline
ushort_t Mesh::GetPrimitivesCount()
{
	return this->primitives_count;
}


inline
ushort_t Mesh::GetTrianglesCount()
{
	return this->triangles_count;
}


#endif // __mesh_h__

// vim:ts=4:sw=4:
