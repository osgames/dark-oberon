//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file animator.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/animator.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/timeserver.h"
#include "kernel/vector.h"
#include "kernel/mathlib.h"

PrepareClass(Animator, "Root", s_NewAnimator, s_InitAnimator);


//=========================================================================
// Animator
//=========================================================================

/**
	Constructor.
*/
Animator::Animator(const char *id) :
	Root(id),
	buffer(NULL),
	state(AS_STOP),
	start_time(0),
	pause_time(0),
	play_time(0),
	anim_time(0),
	resources_valid(false),
	zig(true)
{
	Assert(TimeServer::GetInstance());
}


/**
	Destructor.
*/
Animator::~Animator()
{
	if (this->resources_valid)
		this->UnloadResources();
}


bool Animator::LoadResources()
{
	Assert(!this->anim_file.empty());

	if (this->animation.IsValid())
		return true;

	// create and load animation
	Animation *anim = GfxServer::GetInstance()->NewAnimation(this->anim_file);
	if (!anim)
		return false;

	if (!anim->Load())
	{
		anim->Release();
		return false;
	}

	this->animation = anim;
	this->anim_time = anim->GetAnimTime();
	this->resources_valid = true;

	return true;
}


void Animator::UnloadResources()
{
	// delete model
	if (this->animation.IsValid())
		this->animation->Release();

	this->resources_valid = false;
}


void Animator::Update()
{
	Assert(this->resources_valid);

	// get animation info
	Animation::AnimationType type = this->animation->GetType();
	Animation::LoopType loop_type = this->animation->GetLoopType();
	Animation::AnimationSample *samples = this->animation->GetSamples();
	Assert(samples);

	if (this->state == AS_PLAY)
	{
		double act_time = TimeServer::GetInstance()->GetTime();

		// move start time closer to actual time
		if (loop_type == Animation::LT_NONE)
		{
			this->play_time = act_time - this->start_time;
			if (this->play_time > this->anim_time)
				this->play_time = this->anim_time;
		}
		else
		{
			while (this->start_time < act_time - this->anim_time)
			{
				this->start_time += this->anim_time;
				this->zig = !this->zig;
			}

			// actual play_time <= anim_time
			this->play_time = act_time - this->start_time;
		}
	}

	vector2 *buffer_v2 = (vector2 *)this->buffer;
	vector4 *buffer_v4 = (vector4 *)this->buffer;
	vector2 *data1_v2, *data2_v2;
	vector4 *data1_v4, *data2_v4;

	// compute zig-zag time
	double zigzag_time = (loop_type == Animation::LT_ZIGZAG) && !this->zig ? this->anim_time - this->play_time : this->play_time;

	// compute sample ids
	ushort_t sample_id1, sample_id2;
	for (sample_id1 = 0; samples[sample_id1].sample_time < zigzag_time; sample_id1++);

	sample_id2 = sample_id1 + 1;
	if (sample_id2 == this->animation->GetSamplesCount())
		sample_id2 = 0;

	// compute shift coef
	double prev_sample_time = sample_id1 == 0 ? 0 : samples[sample_id1 - 1].sample_time;
	float sample_coef = float((zigzag_time - prev_sample_time) / (samples[sample_id1].sample_time - prev_sample_time));
	
	// compute actual values
	for (ushort_t i = 0; i < this->animation->GetIndicesCount(); i++)
	{
		switch (type) {
		case Animation::AT_VECTROR2:
			data1_v2 = (vector2 *)samples[sample_id1].data;
			switch (samples[sample_id1].filter)
			{
				case Animation::AF_NONE:
					buffer_v2[i] = data1_v2[i];
					break;

				case Animation::AF_LINEAR:
					data2_v2 = (vector2 *)samples[sample_id2].data;
					buffer_v2[i].lerp(data1_v2[i], data2_v2[i], sample_coef);
					break;
			}
			break;

		case Animation::AT_VECTROR4:
			data1_v4 = (vector4 *)samples[sample_id1].data;
			switch (samples[sample_id1].filter)
			{
				case Animation::AF_NONE:
					buffer_v4[i] = data1_v4[i];
					break;

				case Animation::AF_LINEAR:
					data2_v4 = (vector4 *)samples[sample_id2].data;
					buffer_v4[i].lerp(data1_v4[i], data2_v4[i], sample_coef);
					break;
			}
			break;

		default:
			break;
		}
	}
}


void Animator::Play(AnimationBegin anim_begin)
{
	

	if (this->state == AS_PAUSE)
		this->start_time += TimeServer::GetInstance()->GetTime() - this->pause_time;
	else
	{
		this->start_time = TimeServer::GetInstance()->GetTime();

		if (
			(anim_begin == AB_RANDOM) || 
			((anim_begin == AB_DEFAULT) && this->animation->IsRandomBegin())
		)
			this->start_time -= n_drand(this->anim_time);
	}

	this->state = AS_PLAY;
}


void Animator::Pause()
{
	this->pause_time = TimeServer::GetInstance()->GetTime();
	this->state = AS_PAUSE;
}


void Animator::PlayPause()
{
	if (this->state == AS_PLAY)
		this->Pause();
	else
		this->Play();
}


void Animator::Stop()
{
	this->state = AS_STOP;
	this->play_time = 0;
}


// vim:ts=4:sw=4:
