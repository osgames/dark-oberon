#ifndef __font_h__
#define __font_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file font.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Added support for shadow.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/vector.h"
#include "framework/resource.h"
#include "framework/fontfile.h"


//=========================================================================
// Font
//=========================================================================

/**
	@class Font
	@ingroup Framework_Module
*/
    
class Font : public Resource
{
//--- variables
protected:
	// properties
	FontFile::CharSize char_size;
	ushort_t shadow_x;
	ushort_t shadow_y;
	float shadow_alpha;

	ushort_t height;
	ulong_t glyphs_count;

//--- methods
public:
	Font(const char *id);
	virtual ~Font();

	void SetCharSize(const FontFile::CharSize &char_size);
	void SetShadow(ushort_t x, ushort_t y, byte_t alpha = 255);

	const FontFile::CharSize &GetCharsize() const;
	ushort_t GetHeight() const;
	ulong_t GetGlyphsCount() const;

	virtual void Print(const string &text) const;
};


//=========================================================================
// Methods
//=========================================================================

inline
void Font::SetCharSize(const FontFile::CharSize &char_size)
{
	Assert(!this->loaded);
	this->char_size = char_size;
}


inline
void Font::SetShadow(ushort_t x, ushort_t y, byte_t alpha)
{
	this->shadow_x = x;
	this->shadow_y = y;
	this->shadow_alpha = float(alpha) / 255.0f;
}


inline
const FontFile::CharSize &Font::GetCharsize() const
{
	return this->char_size;
}


inline
ushort_t Font::GetHeight() const
{
	return this->height;
}


inline
ulong_t Font::GetGlyphsCount() const
{
	return this->glyphs_count;
}


#endif // __font_h__

// vim:ts=4:sw=4:
