#ifndef __isoscene_h__
#define __isoscene_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file animator.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/root.h"
#include "kernel/ref.h"
#include "framework/animation.h"


//=========================================================================
// Animator
//=========================================================================

/**
	@class Animator
	@ingroup Framework_Module
*/

class Animator : public Root
{
//--- embeded
public:
	enum AnimationBegin
	{
		AB_DEFAULT,
		AB_BEGIN,
		AB_RANDOM
	};

	enum AnimatorState
	{
		AS_STOP,
		AS_PLAY,
		AS_PAUSE
	};


	//--- methods
public:
	Animator(const char *id);
	virtual ~Animator();

	void Play(AnimationBegin anim_begin = AB_DEFAULT);
	void Pause();
	void PlayPause();
	void Stop();

	void SetAnimation(const string &anim_file);
	void SetBuffer(void *buffer);
	void Update();

	bool LoadResources();
	bool IsResourcesValid();

protected:
	void UnloadResources();


//--- variables
private:
	string anim_file;
	AnimatorState state;
	double start_time;
	double pause_time;
	double play_time;
	double anim_time;

	Ref<Animation> animation;
	void *buffer;

	bool resources_valid;
	bool zig;
};


//=========================================================================
// Methods
//=========================================================================

inline
void Animator::SetBuffer(void *buffer)
{
	this->buffer = buffer;
}


inline
void Animator::SetAnimation(const string &anim_file)
{
	this->UnloadResources();
	this->anim_file = anim_file;
}


#endif  // __isoscene_h__

// vim:ts=4:sw=4:
