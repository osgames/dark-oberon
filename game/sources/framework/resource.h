#ifndef __resource_h__
#define __resource_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file resource.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Added SerializedResource class.
	@version 1.2 - Added DataFile.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/root.h"
#include "kernel/ref.h"

class FileServer;
class File;
class DataFile;


//=========================================================================
// Resource
//=========================================================================

/**
	@class Resource
	@ingroup Framework_Module

	@brief A superclass of all resource related subclasses such as Sound, 
	Texture and so on.
*/

class Resource : public Root
{
	friend class ResourceServer;

//--- embeded
public:
	/// Resource type.
	enum Type
	{
		RES_SOUND,                  ///< A non-shared sound (Sound).
		RES_SOUND_RESOURCE,         ///< A shared sound resource (SoundResource).
		RES_TEXTURE,                ///< A texture object.
		RES_MESH,                   ///< A mesh object.
		RES_MODEL,                  ///< A model object.
		RES_ANIMATION,              ///< An animation.
		RES_FONT,                   ///< Font data.
		RES_OTHER,                  ///< Something else.

		RES_COUNT,                  ///< Count of types.
		RES_ALL,                    ///< Means all types.
		RES_INVALID,                ///< Invalid type.
	};


//--- variables
protected:
	Type type;
	bool loaded;

	string file_name;               ///< File name to load.
	DataFile *data_file;            ///< Data file will be used insted of file name.
	uint_t unique_id;

	Ref<ResourceServer> res_server;


//--- methods
public:
	Resource(const char *id);
	virtual ~Resource();

	bool Load();
	void Unload();

	// properties
	Type GetType() const;
	bool TestType(Type t) const;

	bool IsLoaded() const;

	void SetFileName(const string& file_name);
	const string &GetFileName() const;
	void SetDataFile(DataFile *data_file);
	DataFile *GetDataFile() const;

	virtual ulong_t GetDataSize();
	uint_t GetUniqueId() const;

protected:
	virtual bool LoadResource();
	virtual void UnloadResource();
};


//=========================================================================
// Methods
//=========================================================================

/**
	Gets the resource type.
	@return     Resource type.
*/
inline
Resource::Type Resource::GetType() const
{
	return this->type;
}


/**
	Tests the resource type.
	@return     @c True if tested type is same as current type.
*/
inline
bool Resource::TestType(Type t) const
{
	Assert(t < RES_COUNT);
	return (this->type == t);
}


/**
	Sets path to the resource file.
	@param  file_name    Path to the resource file.
*/
inline
void Resource::SetFileName(const string& file_name)
{
	this->file_name = file_name;
}


/**
	Gets path to the resource file.

	@return     Path to the resource file.
*/
inline
const string &Resource::GetFileName() const
{
	return this->file_name;
}


inline
void Resource::SetDataFile(DataFile *data_file)
{
	this->data_file = data_file;
}


inline
DataFile *Resource::GetDataFile() const
{
	return this->data_file;
}


/**
*/
inline
bool Resource::IsLoaded() const
{
	return this->loaded;
}


/**
	Returns the unique id of this resource object. You should use the unique
	id to check whether 2 resources are identical instead of comparing their
	pointers.
*/
inline
uint_t Resource::GetUniqueId() const
{
	return this->unique_id;
}


//=========================================================================
// SerializedResource
//=========================================================================

class SerializedResource : public Resource
{
//--- methods
protected:
	SerializedResource(const char *name);

	virtual bool LoadResource();
	virtual void UnloadResource();

	virtual void Clear();
};


//=========================================================================
// Methods
//=========================================================================

inline
SerializedResource::SerializedResource(const char *id) :
	Resource(id)
{
	//
}


#endif  // __resource_h__

// vim:ts=4:sw=4:
