//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file transformnode.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/transformnode.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"

PrepareClass(TransformNode, "SceneNode", s_NewTransformNode, s_InitTransformNode);


//=========================================================================
// TransformNode
//=========================================================================

/**
	Constructor.
*/
TransformNode::TransformNode(const char *id) :
	SceneNode(id),
	rotation(0)
{
	//
}


/**
	Destructor.
*/
TransformNode::~TransformNode()
{
	//
}


void TransformNode::SetTranslation(const vector2 &translation)
{
	this->translation = translation;

	if (translation.x || translation.y)
		this->flags |= TT_TRANSLATION;
	else
		this->flags &= (TT_TRANSLATION ^ 0xFF);
}


void TransformNode::SetRotation(float rotation)
{
	this->rotation = rotation;

	if (rotation)
		this->flags |= TT_ROTATION;
	else
		this->flags &= (TT_ROTATION ^ 0xFF);
}


/**
	Apply transformation.
*/
void TransformNode::Render()
{
	GfxServer *gs = GfxServer::GetInstance();
	Assert(gs);

	gs->PushMatrix();

	if (this->flags & TT_TRANSLATION)
		gs->Translate(this->translation);

	if (this->flags & TT_ROTATION)
		gs->Rotate(this->rotation);

	SceneNode::Render();

	gs->PopMatrix();
}


// vim:ts=4:sw=4:
