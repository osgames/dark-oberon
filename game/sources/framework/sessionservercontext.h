#ifndef __sessionservercontext_h__
#define __sessionservercontext_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================


/**
	@file sessionservercontext.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2009

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/ref.h"
#include "kernel/guid.h"
#include "kernel/ipcaddress.h"
#include "kernel/ipcserver.h"

class Attributes;

/**
	@class SessionServerContext
	@ingroup Network

	@brief Represents a session server on the client side.
*/

class SessionServerContext : public Root
{
//--- methods
public:
	SessionServerContext(const char *id);
	virtual ~SessionServerContext();

	void SetSessionGuid(const char *guid);
	const char *GetSessionGuid() const;

	void SetIpcChannelId(uint_t channel_id);
	uint_t GetIpcChannelId() const;

	void SetClientId(uint_t client_id);
	uint_t GetClientId() const;

	const IpcAddress &GetAddress() const;
	void SetHostName(const char *name);
	const char *GetHostName() const;
	void SetPortNum(ushort_t port_num);
	ushort_t GetPortNum() const;

	Attributes *GetAttributes() const;

	void SetKeepAliveTime(double t);
	double GetKeepAliveTime() const;


//--- variables
private:
	Guid session_guid;
	uint_t channel_id;
	uint_t client_id;

	double keep_alive_time;

	IpcAddress server_address;
	Ref<Attributes> attributes;
};


//=========================================================================
// Methods
//=========================================================================

inline
void SessionServerContext::SetSessionGuid(const char *guid)
{
	this->session_guid.Set(guid);
}

inline
const char *SessionServerContext::GetSessionGuid() const
{
	return this->session_guid.Get();
}

inline
void SessionServerContext::SetIpcChannelId(uint_t channel_id)
{
	Assert(channel_id);
	this->channel_id = channel_id;
}


inline
uint_t SessionServerContext::GetIpcChannelId() const
{
	return this->channel_id;
}


inline
void SessionServerContext::SetClientId(uint_t client_id)
{
	this->client_id = client_id;
}

inline
uint_t SessionServerContext::GetClientId() const
{
	return this->client_id;
}

inline
const IpcAddress &SessionServerContext::GetAddress() const
{
	return this->server_address;
}

inline
void SessionServerContext::SetHostName(const char *name)
{
	this->server_address.SetHostName(name);
}


inline
const char *SessionServerContext::GetHostName() const
{
	return this->server_address.GetHostName();
}


inline
void SessionServerContext::SetPortNum(ushort_t port_num)
{
	this->server_address.SetPortNum(port_num);
}


inline
ushort_t SessionServerContext::GetPortNum() const
{
	return this->server_address.GetPortNum();
}


inline
void SessionServerContext::SetKeepAliveTime(double t)
{
	this->keep_alive_time = t;
}


inline
double SessionServerContext::GetKeepAliveTime() const
{
	return this->keep_alive_time;
}

inline
Attributes *SessionServerContext::GetAttributes() const
{
	return this->attributes;
}


#endif

// vim:ts=4:sw=4:
