#ifndef __inputserver_h__
#define __inputserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file inputserver.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Registering script callback functions for actions.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/rootserver.h"
#include "framework/inputtypes.h"


//=========================================================================
// InputServer
//=========================================================================

/**
	@class InputServer
	@ingroup Framework_Module

	@brief GFX server interface.
*/

class InputServer : public RootServer<InputServer>
{
	friend class GuiServer;

//--- embeded
public:
	typedef bool (* ActionCallbackFun)(const string&, int, void *data);

	typedef bool (* KeyCallbackFun)(KeyCode, KeyState);
	typedef bool (* CharCallbackFun)(int, KeyState);
	typedef bool (* MousePosCallbackFun)(ushort_t, ushort_t);
	typedef bool (* MouseRelPosCallbackFun)(int, int);
	typedef bool (* MouseWheelCallbackFun)(int);
	typedef bool (* MouseRelWheelCallbackFun)(int);

protected:
	struct BindInfo
	{
		string definition;
		string action;

		BindInfo(const char *def, const char *act);
	};

	struct KeyInfo
	{
		bool pressed;
		
		BindInfo *bind_info[KS_COUNT];

		KeyInfo();
	};

	struct SliderInfo
	{
		BindInfo *bind_info;

		SliderInfo();
	};

	struct ActionInfo
	{
		ActionCallbackFun function;
		string script_function;
		void *data;

		ActionInfo(ActionCallbackFun cbfunc, void *dat);
		ActionInfo(string script_cbfunc);
	};

	typedef list<ActionInfo> ActionList;
	typedef list<ActionInfo>::iterator ActionIterator;


//--- variables
protected:
	// callbacks
	ActionList callback_action;

	KeyCallbackFun callback_key;
	CharCallbackFun callback_char;
	MousePosCallbackFun callback_position;
	MouseRelPosCallbackFun callback_relposition;
	MouseWheelCallbackFun callback_wheel;
	MouseRelWheelCallbackFun callback_relwheel;

	KeyCallbackFun gui_callback_key;
	CharCallbackFun gui_callback_char;
	MousePosCallbackFun gui_callback_position;
	MouseRelPosCallbackFun gui_callback_relposition;
	MouseWheelCallbackFun gui_callback_wheel;
	MouseRelWheelCallbackFun gui_callback_relwheel;

	// mouse
	ushort_t mouse_x;
	ushort_t mouse_y;
	int mouse_absx;
	int mouse_absy;
	int mouse_reldx;
	int mouse_reldy;
	int mouse_wheel;
	bool mouse_fixed;

	// binding
	KeyInfo keys[KEY_COUNT];
	SliderInfo sliders[SLIDER_COUNT];

	vector<BindInfo *> bind_list;
	vector<KeyCode> pressed_list;


//--- methods
public:
	InputServer(const char *id);
	virtual ~InputServer();

	virtual void RegisterCallbacks();
	virtual bool Trigger();
	virtual void WaitForEvent();

	// identifiers
	const char *GetKeyName(KeyCode key);
	KeyCode GetKeyCode(const string &key_id);
	KeyState GetKeyState(const string &state_id);
	SliderCode GetSliderCode(const string &slider_id);

	// events state
	bool IsKeyPressed(KeyCode key);
	void GetMousePosition(ushort_t &x, ushort_t &y);
	void GetAbsMousePosition(int &x, int &y);
	void GetRelMousePosition(int &dx, int &dy);
	int  GetMouseWheel();
	void FixMouse(bool fix);

	virtual void SetMousePosition(ushort_t x, ushort_t y);
	void ResetInput();

	// callbacks
	void AddActionCallback(ActionCallbackFun cbfun, void *data = NULL);
	void AddActionCallback(const char *script_cbfun);
	bool RemoveActionCallback(ActionCallbackFun cbfun);
	bool RemoveActionCallback(const char *script_cbfun);

	void SetKeyCallback(KeyCallbackFun cbfun);
	void SetCharCallback(CharCallbackFun cbfun);
	void SetMousePosCallback(MousePosCallbackFun cbfun);
	void SetMouseRelPosCallback(MouseRelPosCallbackFun cbfun);
	void SetMouseWheelCallback(MouseWheelCallbackFun cbfun);
	void SetMouseRelWheelCallback(MouseRelWheelCallbackFun cbfun);

	// binding
	bool Bind(const char *definition, const char *action);
	void UnbindAll();

protected:
	bool RemoveBind(KeyCode key, KeyState state, SliderCode slider);
	void EmitAction(const string &action, int pos);
};


//=========================================================================
// Methods
//=========================================================================


inline
void InputServer::AddActionCallback(ActionCallbackFun cbfun, void *data)
{
	Assert(cbfun);

	// new action callbacks are added to the front in order to optimize their emitting
	this->callback_action.push_front(InputServer::ActionInfo(cbfun, data));
}


inline
void InputServer::AddActionCallback(const char *script_cbfun)
{
	Assert(script_cbfun && *script_cbfun);

	// new action callbacks are added to the front in order to optimize their emitting
	this->callback_action.push_front(InputServer::ActionInfo(string(script_cbfun)));
}


inline
bool InputServer::RemoveActionCallback(ActionCallbackFun cbfun)
{
	Assert(cbfun);

	// find cbfun
	ActionIterator it;

	for (it = this->callback_action.begin(); it != this->callback_action.end(); it++)
	{
		if (it->function == cbfun)
		{
			this->callback_action.erase(it);
			return true;
		}
	}

	return false;
}


inline
bool InputServer::RemoveActionCallback(const char *script_cbfun)
{
	Assert(script_cbfun && *script_cbfun);

	// find cbfun
	ActionIterator it;

	for (it = this->callback_action.begin(); it != this->callback_action.end(); it++)
	{
		if (it->script_function == script_cbfun)
		{
			this->callback_action.erase(it);
			return true;
		}
	}

	return false;
}


inline
void InputServer::SetKeyCallback(KeyCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_key = cbfun;
}


inline
void InputServer::SetCharCallback(CharCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_char = cbfun;
}


inline
void InputServer::SetMousePosCallback(MousePosCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_position = cbfun;
}


inline
void InputServer::SetMouseRelPosCallback(MouseRelPosCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_relposition = cbfun;
}


inline
void InputServer::SetMouseWheelCallback(MouseWheelCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_wheel = cbfun;
}


inline
void InputServer::SetMouseRelWheelCallback(MouseRelWheelCallbackFun cbfun)
{
	Assert(cbfun);
	this->callback_relwheel = cbfun;
}


inline
bool InputServer::IsKeyPressed(KeyCode key)
{
	return this->keys[key].pressed;
}


inline
void InputServer::GetMousePosition(ushort_t &x, ushort_t &y)
{
	x = this->mouse_x;
	y = this->mouse_y;
}


inline
int InputServer::GetMouseWheel()
{
	return this->mouse_wheel;
}


inline
void InputServer::FixMouse(bool fix)
{
	this->mouse_fixed = fix;
}


inline
void InputServer::GetAbsMousePosition(int &x, int &y)
{
	x = this->mouse_absx;
	y = this->mouse_absy;
}


inline
void InputServer::GetRelMousePosition(int &dx, int &dy)
{
	dx = this->mouse_reldx;
	dy = this->mouse_reldy;
}


inline
void InputServer::ResetInput()
{
	mouse_x = mouse_y = 0;
	mouse_absx = mouse_absy = 0;
	mouse_reldx = mouse_reldy = 0;
	mouse_wheel = 0;
	mouse_fixed = false;
}


//=========================================================================
// Embeded methods
//=========================================================================

inline
InputServer::BindInfo::BindInfo(const char *def, const char *act) :
	definition(def),
	action(act)
{
	//
}



inline
InputServer::KeyInfo::KeyInfo() :
	pressed(false)
{
	for (int i = 0; i < KS_COUNT; i++)
		this->bind_info[i] = NULL;
}


inline
InputServer::SliderInfo::SliderInfo() :
	bind_info(NULL)
{
	//
}


inline
InputServer::ActionInfo::ActionInfo(ActionCallbackFun func, void *dat) :
	function(func),
	data(dat)
{
	//
}


inline
InputServer::ActionInfo::ActionInfo(string script_func) :
	function(NULL),
	script_function(script_func),
	data(NULL)
{
	//
}


#endif  // __inputserver_h__

// vim:ts=4:sw=4:
