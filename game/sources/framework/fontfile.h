#ifndef __fontfile_h__
#define __fontfile_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file fontfile.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/datafile.h"


//=========================================================================
// FontFile
//=========================================================================

/**
	@class FontFile
	@ingroup Framework_Module

	Provides read access to font file.

	Whole font is opened first and then you can query each character
	separetelly. In fact only those characters are loaded that you want to
	use. Characters are converted into small bitmap with 256 gray levels.
	You need to set character size and resolution first.
	<br>
	This class is the base for all font files.
*/

class FontFile : public DataFile {
//-- embeded
public:
	/**
		Contains all properies of one glyph - character definition.
	*/
	struct GlyphData {
		int left;             ///< Left margin.
		int top;              ///< Top margin.
		ushort_t width;       ///< Glyph width.
		ushort_t height;      ///< Glyph height.
		long advance_x;       ///< Horizontal shift.
		long advance_y;       ///< Vertical shift.
	};

	/**
		Size of character.
	*/
	struct CharSize {
		ushort_t width;       ///< Character width in points.
		ushort_t height;      ///< Character height in points.
		uint_t resolution_x;  ///< Horizontal reslution in pixels per inch.
		uint_t resolution_y;  ///< Vertical resolution in pixels per inch.
	};

	ulong_t glyphs_count;     ///< Number of all glyphs.

//-- variables
protected:
	GlyphData glyph_data;     ///< Data of one loaded glyph.
	CharSize char_size;       ///< Size of character.

	uchar_t *data;            ///< Loaded character converted into bitmap buffer.


//-- methods
public:
	FontFile();
	virtual ~FontFile();

	void SetCharSize(const FontFile::CharSize &char_size);

	virtual byte_t *GetData(ulong_t char_code, bool close = false);
	const GlyphData &GetGlyphData();
	ulong_t GetGlyphsCount();

protected:
	virtual void DeleteData();
};


//=========================================================================
// Methods
//=========================================================================

/**
	Returns glyph data of loaded character.

	@return Structure with glyph data.
*/
inline
const FontFile::GlyphData &FontFile::GetGlyphData()
{
	return this->glyph_data;
}


/**
	Sets character size. This information will be used to convert characters
	into bitmap.

	@param char_size Structure with character size and resolution.
*/
inline
void FontFile::SetCharSize(const FontFile::CharSize &char_size)
{
	Assert(this->TestState(LS_UNLOADED));
	this->char_size = char_size;
}


/**
	Returns number of glyphs (characters) in font file.

	@return Number of glyphs.
*/
inline
ulong_t FontFile::GetGlyphsCount()
{
	return this->glyphs_count;
}


#endif // __fontfile_h__

// vim:ts=4:sw=4:
