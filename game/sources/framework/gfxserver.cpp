//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file gfxserver.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Converting MinMagFilter to string and back.
	@version 1.2 - Rendering quads, color mask.
	@version 1.3 - Cursor models.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/gfxserver.h"
#include "framework/resourceserver.h"
#include "framework/shapenode.h"
#include "framework/inputserver.h"
#include "formats/tgaimagefile.h"
#include "formats/freetypefontfile.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"

PrepareScriptClass(GfxServer, "Root", s_NewGfxServer, s_InitGfxServer, s_InitGfxServer_cmds);


//=========================================================================
// GfxServer
//=========================================================================

GfxServer::GfxServer(const char *id) :
	RootServer<GfxServer>(id),
	minmag_filter(FILTER_NEAREST),
	mipmap_filter(FILTER_NONE),
	cursor_visibility(CURSOR_SYSTEM),
	cursor_model(NULL),
	extensions(EXT_NONE),
	blending(false),
	depth_test(false),
	color_filter(1.0f, 1.0f, 1.0f, 1.0f),
	display_opened(false),
	callback_close(NULL),
	callback_size(NULL),
	callback_refresh(NULL),
	mesh(NULL),
	texture(NULL),
	overlay(NULL),
	mapping(NULL),
	coords_buff(NULL),
	colors_buff(NULL),
	window_title("Application")
{
	if (!ResourceServer::GetInstance())
		ErrorMsg("ResourceServer has to be created before GfxServer");

    this->resource_server = ResourceServer::GetInstance();

	// init clear values
	this->clear_values.red = 0.0f;
	this->clear_values.green = 0.0f;
	this->clear_values.blue = 0.0f;
	this->clear_values.alpha = 0.0f;
	this->clear_values.depth = 1.0f;
	this->clear_values.stencil = 0;
}


GfxServer::~GfxServer()
{
	Assert(!this->display_opened);
	this->UnloadResources();
}


bool GfxServer::Trigger()
{
	return this->display_opened;
}


/**
	Creates a shared texture resource object.
*/
Texture *GfxServer::NewTexture(const string &/*file_name*/)
{
	ErrorMsg("Overwrite this method in subclass");
	return NULL;
}


/**
	Creates a shared texture resource object with overlay image.
*/
Texture *GfxServer::NewTexture(const string &/*file_name*/, const string &/*overlay_name*/, const vector3 &/*overlay_color*/)
{
	ErrorMsg("Overwrite this method in subclass");
	return NULL;
}


/**
	Creates a shared texture resource object from memory buffer.
*/
Texture *GfxServer::NewTexture(ImageFile *image_file)
{
	ErrorMsg("Overwrite this method in subclass");
	return NULL;
}


/**
	Creates a shared font resource object.
*/
Font *GfxServer::NewFont(const string &/*file_name*/, const FontFile::CharSize &/*char_size*/)
{
	ErrorMsg("Overwrite this method in subclass");
	return NULL;
}


/**
	Creates a shared mesh resource object.
*/
Mesh *GfxServer::NewMesh(const string &file_name)
{
	return (Mesh *)this->resource_server->NewResource("Mesh", file_name, Resource::RES_MESH);
}


/**
	Creates a shared model resource object.
*/
Model *GfxServer::NewModel(const string &file_name)
{
	return (Model *)this->resource_server->NewResource("Model", file_name, Resource::RES_MODEL);
}


/**
	Creates a shared model resource object.
*/
Model *GfxServer::NewModel(const string &file_name, const vector3 &overlay_color)
{
	if (!overlay_color.len())
		return NewModel(file_name);

	// generate modifier
	char modifier[10];
	vector3 color = overlay_color * 255.0f;
	sprintf(modifier, "%03d%03d%03d", byte_t(color.x), byte_t(color.y), byte_t(color.z));

	Model *model = (Model *)this->resource_server->NewResource("Model", file_name, Resource::RES_MODEL, modifier);

	if (model)
		model->SetOverlayColor(overlay_color);

	return model;
}


Animation *GfxServer::NewAnimation(const string &file_name)
{
	return (Animation *)this->resource_server->NewResource("Animation", file_name, Resource::RES_ANIMATION);
}


ImageFile *GfxServer::NewImageFile(const string &file_name, bool open, bool write)
{
	ImageFile *file;
	FileServer *fs = FileServer::GetInstance();

	// check extension
	string ext;
	FileServer::ExtractExtension(file_name, ext);

	if (ext == "tga")
	{
		file = NEW TgaImageFile();
		Assert(file);
	}

	else
	{
		LogError1("Unknown image format: '%s'", file_name.c_str());
		return NULL;
	}

	file->SetFileName(file_name);

	// open file
	if (open && !file->Open(write))
	{
		delete file;
		return NULL;
	}

	return file;
}


FontFile *GfxServer::NewFontFile(const string &file_name, bool open, bool write)
{
	FontFile *file;
	FileServer *fs = FileServer::GetInstance();

	// check extension
	string ext;
	FileServer::ExtractExtension(file_name, ext);

	if (ext == "ttf" || ext == "fon" || ext == "fnt" || ext == "pcf")  // !!! add all supported extensions
	{
		file = NEW FreeTypeFontFile();
		Assert(file);
	}

	else
	{
		LogError1("Unknown font format: '%s'", file_name.c_str());
		return NULL;
	}

	file->SetFileName(file_name);

	// open file
	if (open && !file->Open(write))
	{
		delete file;
		return NULL;
	}

	return file;
}


void GfxServer::SetProjection(const Projection& projection)
{
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::SetViewport(const Viewport& viewport)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::PushScreenCoordinates()
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::PopScreenCoordinates()
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


ushort_t GfxServer::GetVideoModes(VideoMode *modes, int max_count, VideoMode::Bpp bpp)
{
	ErrorMsg("Overwrite this method in subclass");
	return 0;
}


void GfxServer::SetWindowTitle(const string &window_title)
{
	this->window_title = window_title;
}


bool GfxServer::IsWindowMinimized()
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool GfxServer::IsWindowActive()
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


bool GfxServer::IsWindowAccelerated()
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


void GfxServer::SetWindowPosition(const DisplayMode::WidowPosition &position)
{
	this->display_mode.SetWindowPosition(position);
}


const DisplayMode::WidowPosition &GfxServer::GetWindowPosition() const
{
	return this->display_mode.GetWindowPosion();
}


void GfxServer::SetWindowSize(const DisplayMode::WidowSize &size)
{
	this->display_mode.SetModeSize(size);
}


const DisplayMode::WidowSize &GfxServer::GetWindowSize() const
{
	return this->display_mode.GetModeSize();
}


void GfxServer::SetWindowVerticalSync(bool vsync)
{
	this->display_mode.SetVerticalSync(vsync);
}


void GfxServer::MinimizeWindow()
{
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::RestoreWindow()
{
	ErrorMsg("Overwrite this method in subclass");
}


/** @brief Set the prefered display mode.
*/
void GfxServer::SetDisplayMode(const DisplayMode& mode)
{
	if (this->display_opened)
	{
		// unload resources stored in graphic device
		this->resource_server->UnloadResources(Resource::RES_FONT);
		this->resource_server->UnloadResources(Resource::RES_TEXTURE);

		// reopen display
		this->CloseDisplay();
		this->display_mode = mode;
		this->OpenDisplay();

		// reload resources
		this->resource_server->LoadResources(Resource::RES_FONT);
		this->resource_server->LoadResources(Resource::RES_TEXTURE);
	}

	else
		this->display_mode = mode;
}


void GfxServer::ToggleFullscreen()
{
	if (this->display_opened)
	{
		LogInfo("Toggling fullscreen");

		// unload resources stored in graphic device
		this->resource_server->UnloadResources(Resource::RES_FONT);
		this->resource_server->UnloadResources(Resource::RES_TEXTURE);

		// reopen display
		this->CloseDisplay();
		this->display_mode.ToggleFullscreen();
		this->OpenDisplay();

		// reload resources
		this->resource_server->LoadResources(Resource::RES_FONT);
		this->resource_server->LoadResources(Resource::RES_TEXTURE);
	}

	else
		this->display_mode.ToggleFullscreen();
}


bool GfxServer::OpenDisplay()
{
	Assert(!this->display_opened);

	// log info
	LogInfo("Opening GFX display");
	LogInfo("--- Display info ---");
	LogInfo3("Resolution:  %hux%hux%d", 
		this->display_mode.GetModeSize().width, 
		this->display_mode.GetModeSize().height, 
		VideoMode::BppToInt(this->display_mode.GetBpp())
	);
	LogInfo1("Mode:        %s", this->display_mode.IsFullscreen() ? "fullscreen" : "window");

	if (this->display_mode.GetRefreshRate())
		LogInfo1("Refresh:     %hu", (ushort_t)this->display_mode.GetRefreshRate());
	else
		LogInfo("Refresh:     default");

	InputServer *input_server = InputServer::GetInstance();
	if (input_server)
		input_server->ResetInput();

	this->display_opened = true;
	return true;
}


void GfxServer::CloseDisplay()
{
	Assert(this->display_opened);

	if (this->callback_close)
	{
		if (!this->callback_close())
			return;
	}

	LogInfo("Closing GFX display");

	this->display_opened = false;
}


bool GfxServer::LoadResources()
{
	// load all cursor models
	Model *model;
	bool ok = true;

	for (size_t i = 0; i < this->cursors.size(); i++)
	{
		model = this->cursors[i];
		if (model && !model->IsLoaded() && !model->Load())
		{
			SafeRelease(this->cursors[i]);
			ok = false;
		}
	}

	return ok;
}


void GfxServer::UnloadResources()
{
	// delete all cursor models
	for (size_t i = 0; i < this->cursors.size(); i++)
		SafeRelease(this->cursors[i]);
}


void GfxServer::RegisterCallbacks()
{
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Writes basic gfx information to log file.
*/
void GfxServer::WriteInfo()
{
	// empty
}


void GfxServer::CheckExtensions()
{
	// empty
}


void GfxServer::PresentScene()
{
	Assert(this->display_opened);

	// render mouse cursor
	if (this->cursor_model && this->cursor_visibility == CURSOR_CUSTOM)
	{
		this->PushScreenCoordinates();
		InputServer *input_server = InputServer::GetInstance();

		if (input_server)
		{
			ushort_t x, y;
			input_server->GetMousePosition(x, y);
			this->ResetMatrix();
			this->Translate(vector2(x, float(-y)));
		}

		this->cursor_model->Render();
		this->PopScreenCoordinates();
	}
}


void GfxServer::SetCursorVisibility(CursorVisibility type)
{
	this->cursor_visibility = type;
}


byte_t GfxServer::AddCursor(const string &model_file)
{
	Model *cursor_model = this->NewModel(model_file);
	if (!cursor_model)
	{
		LogWarning1("Error creating cursor model: %s", model_file);
		return 0;
	}

	// load model if display is opened
	if (this->display_opened)
	{
		if (!this->cursor_model->IsLoaded() && !this->cursor_model->Load())
		{
			SafeRelease(this->cursor_model);
			return 0;
		}
	}

	this->cursors.push_back(cursor_model);
	return byte_t(this->cursors.size() - 1);
}


bool GfxServer::SaveScreenshot(const string &file_name)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");

	return false;
}


bool GfxServer::SaveImage(const string &file_name, byte_t *data, ImageFile::Format format, ushort_t width, ushort_t height)
{
	ImageFile *image_file = this->NewImageFile(file_name, true, true);
	if (!image_file)
		return false;

	ulong_t size = width * height * ImageFile::GetBytesPerPixel(format);

	image_file->SetData(data, size, format, width, height);
	image_file->SaveData();

	delete image_file;

	return true;
}


void GfxServer::Translate(const vector2 &/*translation*/)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::Translate(const vector3 &/*translation*/)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::Rotate(float /*rotation*/)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::Rotate(const vector3 &/*rotation*/)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Sets values for clearing buffers.
*/
void GfxServer::SetClearValues(float red, float green, float blue, float alpha, float depth, int stencil)
{
	Assert(this->display_opened);
	
	this->clear_values.red = red;
	this->clear_values.green = green;
	this->clear_values.blue = blue;
	this->clear_values.alpha = alpha;
	this->clear_values.depth = depth;
	this->clear_values.stencil = stencil;
}


/**
	Sets current color.
*/
void GfxServer::SetColor(const vector3 &/*color*/)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Sets current color.
*/
void GfxServer::SetColor(const vector4 &/*color*/)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Sets current color mask.
*/
void GfxServer::SetColorMask(bool r, bool g, bool b, bool alpha)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::SetBlending(bool blending)
{
	this->blending = blending;
}


void GfxServer::SetDepthTest(bool depth_test)
{
	this->depth_test = depth_test;
}


/**
	Clears buffers.
*/
void GfxServer::ClearBuffers(byte_t /*buffers*/)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::RenderShape()
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::RenderPrimitive(PrimitiveType primitive, const vector2 *vertices, uint_t count)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::SetFont(Font *font)
{
	// if font is NULL, activate first one
	if (!font)
		this->font = (Font *)(ResourceServer::GetInstance()->GetResourceList(Resource::RES_FONT)->GetFront());
	else
		this->font = font;
}


void GfxServer::Print(const string &text)
{
	// if no font is active, activate first one
	if (!this->font.IsValid())
		this->SetFont(NULL);

	// no font was found
	if (!this->font.IsValid())
		return;

	this->font->Print(text);
}


void GfxServer::Print(const vector2 &/*position*/, const string &/*text*/)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::ReadPixels(ushort_t x, ushort_t y, ushort_t width, ushort_t height, byte_t *pixels)
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::ResetMatrix()
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::PushMatrix()
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


void GfxServer::PopMatrix()
{
	Assert(this->display_opened);
	ErrorMsg("Overwrite this method in subclass");
}


bool GfxServer::FilterToString(MinMagFilter filter, string &name)
{
	switch (filter)
	{
	case FILTER_NONE:     name = "none";     return true;
	case FILTER_NEAREST:  name = "nearest";  return true;
	case FILTER_LINEAR:   name = "linear";   return true;
	default:
		LogWarning1("Invalid minmag filter: %d", filter);
		return false;
	}
}


bool GfxServer::StringToFilter(const string &name, MinMagFilter &filter)
{
	if (name == "none")          filter = FILTER_NONE;
	else if (name == "nearest")  filter = FILTER_NEAREST;
	else if (name == "linear")   filter = FILTER_LINEAR;
	else
	{
		LogWarning1("Invalid minmag filter: %s", name.c_str());
		return false;
	}

	return true;
}


// vim:ts=4:sw=4:
