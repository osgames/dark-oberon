#ifndef __attributes_h__
#define __attributes_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================


/**
	@file attributes.h
	@ingroup Framework_Module

	@author Peter Knut
	@date 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/root.h"


//=========================================================================
// Attributes
//=========================================================================

/**
	@class Attributes
	@ingroup Network
	@brief Simple addributes node.
*/

class Attributes : public Root
{
//--- methods
public:
	Attributes(const char *id);
	virtual ~Attributes();

	void SetAttribute(const char *name, const char *value);
	const char *GetAttribute(const char *name);

	void SetDirty(bool dirty);
	bool IsDirty() const;

//--- variables
private:
	mutable bool dirty;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Returns and clears the dirty flag.
*/
inline
bool Attributes::IsDirty() const
{
	bool res = this->dirty;
	this->dirty = false;

	return res;
}

/**
	Sets dirty flag.
*/
inline
void Attributes::SetDirty(bool dirty)
{
	this->dirty = dirty;
}


#endif

// vim:ts=4:sw=4:
