//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sound.cpp
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "framework/sound.h"
#include "framework/audioserver.h"

PrepareClass(Sound, "Resource", s_NewSound, s_InitSound);


//=========================================================================
// Sound
//=========================================================================

/**
    Constructor.
*/
Sound::Sound(const char *id) :
	Resource(id),
	streaming(false),
	looping(false),
	priority(0),
	volume(1.0f),
	min_volume(0.0f),
	max_volume(1.0f),
	ref_distance(1.0f),
	max_distance(1000.0f),
	inner_cone_angle(360.0f),
	outer_cone_angle(360.0f),
	outer_cone_volume(1.0f),
	rolloff_factor(1.0f),
	pitch(1.0f),
	category(SND_EFFECT),
	volume_dirty(true),
	move_dirty(true),
	props_dirty(true)
{
	Assert(AudioServer::GetInstance());
}


/**
	Destructor.
*/
Sound::~Sound()
{
	if (this->loaded)
	{
        this->Stop();
        this->Unload();
    }
}


/**
	Copies audio attributes from another sound object.
*/
inline
void Sound::CopySoundAttrsFrom(const Sound* other)
{
	Assert(other);

	this->SetCategory(other->GetCategory());
	this->SetPriority(other->GetPriority());
	this->SetStreaming(other->IsStreaming());
	this->SetLooping(other->IsLooping());
	this->SetVolume(other->GetVolume());
	this->SetMinVolume(other->GetMinVolume());
	this->SetMaxVolume(other->GetMaxVolume());
	
	this->SetTransform(other->GetTransform());
	this->SetVelocity(other->GetVelocity());
	this->SetRefDistance(other->GetRefDistance());
	this->SetMaxDistance(other->GetMaxDistance());
	this->SetInnerConeAngle(other->GetInnerConeAngle());
	this->SetOuterConeAngle(other->GetOuterConeAngle());
	this->SetOuterConeVolume(other->GetOuterConeVolume());
	this->SetRolloffFactor(other->GetRolloffFactor());
	this->SetPitch(other->GetPitch());
}


/**
	Starts the sound.
*/
void Sound::Play()
{
	Assert(this->loaded);
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Stops the sound.
*/
void Sound::Stop()
{
	Assert(this->loaded);
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Pause the sound.
*/
void Sound::Pause()
{
	Assert(this->loaded);
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Play/Pause the sound.
*/
void Sound::PlayPause()
{
	if (!this->loaded)
		return;

	if (this->IsPlaying())
		this->Pause();
	else
		this->Play();
}


/**
	Updates the sound.
*/
void Sound::Update()
{
	Assert(this->loaded);
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Updates sound stream.
*/
void Sound::UpdateStream()
{
	Assert(this->streaming && this->loaded);
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Returns @c true if sound is playing.
*/
bool Sound::IsPlaying()
{
	Assert(this->loaded);
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


/**
*/
bool Sound::CategoryToString(Category category, string &name)
{
	switch (category)
	{
	case SND_EFFECT:  name = "effect";   return true;
	case SND_MUSIC:   name = "music";    return true;
	case SND_SPEECH:  name = "speech";   return true;
	case SND_AMBIENT: name = "ambient";  return true;
	default:
		LogWarning1("Invalid sound category: %d", category);
		return false;
	}
}


/**
*/
bool Sound::StringToCategory(const string &name, Category &category)
{
	if (name == "effect")       category = SND_EFFECT;
	else if (name == "music")   category = SND_MUSIC;
	else if (name == "speech")  category = SND_SPEECH;
	else if (name == "ambient") category = SND_AMBIENT;
	else
	{
		LogWarning1("Invalid sound category '%s'", name.c_str());
		return false;
	}

	return true;
}


// vim:ts=4:sw=4:
