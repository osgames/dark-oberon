//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file texture.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"
#include "framework/texture.h"
#include "framework/gfxserver.h"

PrepareClass(Texture, "Resource", s_NewTexture, s_InitTexture);


//=========================================================================
// Texture
//=========================================================================

/**
	Constructor.
*/
Texture::Texture(const char *id) :
	Resource(id),
	width(0),
	height(0),
	mipmap(false),
	allow_mipmap(true)
{
	Assert(GfxServer::GetInstance());

	this->type = RES_TEXTURE;
}


/**
	Destructor.
*/
Texture::~Texture()
{
	this->Unload();
}


/**
	Activates texture for drawing.
*/
void Texture::Apply()
{
	ErrorMsg("Overwrite this method in subclass");
}


/**
	Replaces image data.
*/
void Texture::ReplaceData(byte_t *data)
{
	ErrorMsg("Overwrite this method in subclass");
}


// vim:ts=4:sw=4:
