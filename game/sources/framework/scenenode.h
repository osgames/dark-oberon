#ifndef __scenenode_h__
#define __scenenode_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scenenode.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/root.h"


//=========================================================================
// SceneNode
//=========================================================================

/**
	@class SceneNode
	@ingroup Framework_Module

	@brief The SceneNode is the base class of all objects which can be attached
    to a scene managed by the SceneServer class. A scene node object
    may provide transform or geometry information.
*/

class SceneNode : public Root
{
//--- variables
protected:
	bool resources_valid;


//--- methods
public:
	SceneNode(const char *id);
	virtual ~SceneNode();

    virtual bool LoadResources();
    virtual void UnloadResources();
	bool PreloadResources();
    bool IsResourcesValid();

	virtual void Render();
	virtual void CreateAnimators();
};


//=========================================================================
// Methods
//=========================================================================

/**
*/
inline
bool SceneNode::IsResourcesValid()
{
	return this->resources_valid;
}


#endif  // __scenenode_h__

// vim:ts=4:sw=4:
