//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionclient.cpp
	@ingroup Framework_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2008

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sessionclient.h"
#include "framework/attributes.h"
#include "kernel/timeserver.h"
#include "kernel/kernelserver.h"

PrepareScriptClass(SessionClient, "IpcServer", s_NewSessionClient, s_InitSessionClient, s_InitSessionClient_cmds);


//=========================================================================
// SessionClient
//=========================================================================

/**
*/
SessionClient::SessionClient(const char *id) :
	IpcServer(id),
	opened(false),
	joined(false),
	join_denied(false),
	unique_number(0)
{
	AssertMsg(TimeServer::GetInstance(), "Create TimeServer before SessionClient");

	// create storage nodes
	this->kernel_server->PushCwd(this);
	this->attributes = (Attributes *)this->kernel_server->New("Attributes", "attributes");
	this->server_contexts = this->kernel_server->New("Root", "servers");
	this->kernel_server->PopCwd();

	Assert(this->attributes && this->server_contexts);
}


/**
*/
SessionClient::~SessionClient()
{
	if (this->opened)
		this->Close();
}


/**
	Set a client attribute, a new attribute will be created if it does
	not exist yet. Client attributes are distributed to the connected server,
	which in turn distributes them to all other joined clients.

	@param  name        an attribute name
	@param  val         the attribute value as string
*/
void SessionClient::SetClientAttr(const char *name, const char *val)
{
	this->attributes->SetAttribute(name, val);
}


/**
	Get a client attribute by name. Returns 0 if client attribute doesn't
	exist.

	@param  name    an attribute name
	@return         value string, or 0
*/
const char *SessionClient::GetClientAttr(const char *name)
{
	return this->attributes->GetAttribute(name);
}


/**
    Open the client. This will start discovering servers on the net,
    allow to connect the client to a server and exchange attributes with
    the server.
*/
bool SessionClient::Open()
{
	Assert(!this->opened);
	Assert(this->GetAppName());
	Assert(this->GetAppVersion());

	string port_name;
	IpcAddress address;

	LogInfo("Starting session client");

	// reset the unique number (used for naming server contexts)
	this->unique_number = 0;

	// initialie broadcast port
	port_name = this->GetAppName();
	port_name += "Broadcast";
	address.SetPortName(port_name.c_str());

	// start listening on UDP
	if (!this->StartListening(address.GetPortNum(), IpcChannel::PROTOCOL_UDP))
		return false;

	return this->opened = true;
}


/**
	Close the client, this will stop discovering servers on the net.
*/
void SessionClient::Close()
{
	Assert(this->opened);

	LogInfo("Closing session client");

	// currently joined to a session?
	if (this->joined)
		this->LeaveSession();

	// disconnect all sessions
	this->DisconnectAll();

	// release server context objects
	this->server_contexts->Release();
	Assert(!this->server_contexts.IsValid());

	this->opened = false;
}


/**
	Process pending messages. This must be called frequently (i.e.
	once per frame) while the session client is open.
*/
bool SessionClient::Trigger()
{
	Assert(this->opened);

	// remove session which have expired (which have not been updated
	// for some keepalive time).
	this->CleanupExpiredSessions();

	// send client attributes?
	if (this->joined && this->attributes->IsDirty())
		this->SendClientAttrs();

	return IpcServer::Trigger();
}


/**
	Send all client attributes to the joined server. This should only be called when
	the client attributes have actually changed!
*/
void SessionClient::SendClientAttrs()
{
	Assert(this->joined);

	char buffer[1024];
	Env *attr;

	for (attr = (Env *)this->attributes->GetFront(); attr; attr = (Env *)attr->GetNext())
	{
		LogDebug2("Sending session client attr '%s = %s' to join server", attr->GetID().c_str(), attr->GetS());
		snprintf(buffer, sizeof(buffer), "~clientattr %s [%s]", attr->GetID().c_str(), attr->GetS());

		this->Send(this->join_server->GetIpcChannelId(), buffer);
	}
}


/**
	This checks for incoming messages on the sniffer channel, where
	all open session servers broadcast their existence frequently. Once a
	new session is found, a new server context will be created which will
	track the server state on the client side.
*/
void SessionClient::ProcessData(uint_t channel_id, const IpcBuffer &buffer)
{
	Assert(this->opened);

	IpcAddress fromAddress;

	static char command[4096];
	const char *command_name;
	SessionServerContext *server_context;

	// extract the session data from the string, check if it is a valid
	// broadcast message, and extract the message data
	const char *message;
	for (message = buffer.GetFirstString(); message; message = buffer.GetNextString())
	{
		strcpy(command, message);
		command_name = strtok(command, " ");
		if (!command_name)
			continue;

		if (!strcmp(command_name, "~session"))
		{
			const char *guid = strtok(NULL, " ");
			const char *app_name = strtok(NULL, " ");
			const char *app_version = strtok(NULL, " ");
			const char *host = strtok(NULL, " ");
			const char *port_txt = strtok(NULL, " ");
			ushort_t port = (ushort_t)atoi(port_txt);

			if (!guid || !app_name || !app_version || !host || port <= 0)
			{
				// hmm, a broken message, do not fail hard, just continue
				LogWarning1("Broken session broadcast received: %s", message);
				continue;
			}

			// check if a session of that name has already been registered,
			// if yes, this simply updates the KeepAliveTimestamp
			server_context = this->FindServerContextByGuid(guid);
			if (server_context)
			{
				server_context->SetKeepAliveTime(TimeServer::GetInstance()->GetRealTime());
				continue;
			}

			// check app name and app version
			if (strcmp(app_name, this->GetAppName()) || strcmp(app_version, this->GetAppVersion())) {
				LogDebug1("Session broadcast mismatch: %s", message);
				continue;
			}

			// a new valid session
			this->RegisterSession(guid, host, port);
		}
		else if (!strcmp(command_name, "~serverattr"))
		{
			server_context = this->FindServerContext(channel_id);
			const char *attr_name = strtok(NULL, " ");
			const char *attr_value = strtok(NULL, " ");

			if (!server_context || !attr_name || !attr_value)
				continue;

			//LogDebug2("Session server attribute received: %s=%s", attr_name, attr_value);

			server_context->GetAttributes()->SetAttribute(attr_name, attr_value);
		}
		else if (!strcmp(command_name, "~closesession"))
		{
			server_context = this->FindServerContext(channel_id);
			if (!server_context)
				continue;

			LogInfo2("Session closed by server %s:%u", server_context->GetHostName(), server_context->GetPortNum());

			if (this->joined && this->join_server == server_context) {
				this->joined = this->join_denied = false;
				this->join_server = NULL;
			}
			server_context->Release();
		}
		else if (!strcmp(command_name, "~joinaccepted"))
		{
			server_context = this->FindServerContext(channel_id);
			const char *client_id = strtok(NULL, " ");

			if (!server_context || !client_id || this->join_server != server_context)
				continue;

			// our join request has been accepted!
			server_context->SetClientId(atoi(client_id));

			this->joined = true;

			LogInfo2("Session accepted by server %s:%u", server_context->GetHostName(), server_context->GetPortNum());
		}
		else if (!strcmp(command_name, "~joindenied"))
		{
			server_context = this->FindServerContext(channel_id);

			if (!server_context || !this->joined || this->join_server != server_context)
				continue;

			// our join request has been denied!
			this->join_denied = true;
			this->join_server = NULL;

			LogInfo2("Session denied by %s:%u", server_context->GetHostName(), server_context->GetPortNum());
		}
		else if (!strcmp(command_name, "~kick"))
		{
			server_context = this->FindServerContext(channel_id);

			if (!server_context || !this->joined || this->join_server != server_context)
				continue;

			// the server kicks us from the session
			this->joined = this->join_denied = false;
			this->join_server = NULL;

			LogInfo2("Session kicked by %s:%u", server_context->GetHostName(), server_context->GetPortNum());
		}
		else if (!strcmp(command_name, "~start"))
		{
			server_context = this->FindServerContext(channel_id);

			if (!server_context || !this->joined || this->join_server != server_context)
				continue;

			const char *server_host = strtok(NULL, " ");
			const char *server_port = strtok(NULL, " ");
			this->StartGame(server_host, server_port);
		}
	}
}


/**
	Finds server context by the channel id.
*/
SessionServerContext *SessionClient::FindServerContext(uint_t channel_id)
{
	Assert(channel_id);

	SessionServerContext *server_context = (SessionServerContext *)this->server_contexts->GetFront();
	for (; server_context; server_context = (SessionServerContext *)server_context->GetNext())
	{
		if (server_context->GetIpcChannelId() == channel_id)
			return server_context;
	}

	return NULL;
}


/**
	See if a server context for the given session guid exists.
*/
SessionServerContext *SessionClient::FindServerContextByGuid(const char *guid)
{
	Assert(guid);

	SessionServerContext *cur;
	for (cur = (SessionServerContext *)this->server_contexts->GetFront(); cur; cur = (SessionServerContext *)cur->GetNext())
	{
		if (!strcmp(cur->GetSessionGuid(), guid))
			return cur;
	}

	return NULL;
}


/**
	Checks session contexts for dead sessions due to timeout. Normally,
	session servers will correctly close the connection as part of the
	network protocol, if this fails however, a timeout will make sure
	that dead session contexts don't pile up.
*/
void SessionClient::CleanupExpiredSessions()
{
	const float session_timeout  = 5.0f;
	SessionServerContext *cur  = (SessionServerContext *)this->server_contexts->GetFront();
	SessionServerContext *next = 0;

	if (cur) do
	{
		next = (SessionServerContext *)cur->GetNext();
		double age = TimeServer::GetInstance()->GetRealTime() - cur->GetKeepAliveTime();
		if (age > session_timeout)
		{
			LogInfo2("Session timeout from server %s:%u", cur->GetHostName(), cur->GetPortNum());
			if (this->joined && this->join_server == cur) {
				this->joined = this->join_denied = false;
				this->join_server = NULL;
			}
			cur->Release();
		}
	} while (cur = next);
}


/**
	This registers a new session. A server context object is created,
	and a reliable connection is established to the session server.
*/
bool SessionClient::RegisterSession(const char *guid_string, const char *host, ushort_t port)
{
	Assert(guid_string && host && port);

	LogInfo2("Registering new session server %s:%u", host, port);

	// create a session server object
	char server_name[1024];
	sprintf(server_name, "server%d", this->unique_number++);

	this->kernel_server->PushCwd(this->server_contexts.Get());
	SessionServerContext *context = (SessionServerContext *)this->kernel_server->New("SessionServerContext", server_name);
	this->kernel_server->PopCwd();

	// initialize it
	context->SetSessionGuid(guid_string);
	context->SetHostName(host);
	context->SetPortNum(port);
	context->SetKeepAliveTime(TimeServer::GetInstance()->GetRealTime());

	// and establish the connection
	uint_t channel_id = this->Connect(context->GetAddress(), IpcChannel::PROTOCOL_TCP);
	if (!channel_id)
	{
		// failure
		LogError2("Error connecting to server %s:%u", host, port);
		context->Release();
		return false;
	}

	context->SetIpcChannelId(channel_id);
	
	LogInfo2("Session connected to server %s:%u", host, port);

	// query server for a current set of server attributes
	this->Send(channel_id, IpcBuffer("~queryserverattrs"));
	return true;
}


/**
	Get number of discovered servers.
*/
int SessionClient::GetServersCount() const
{
	int count = 0;

	if (this->server_contexts.IsValid())
	{
		SessionServerContext *context;
		for (context = (SessionServerContext *)this->server_contexts->GetFront(); context; context = (SessionServerContext *)context->GetNext())
			count++;
	}

	return count;
}


/**
	Get pointer to server context at index.
*/
SessionServerContext *SessionClient::GetServerAt(int index) const
{
	int i = 0;
	SessionServerContext * context;

	for (context = (SessionServerContext *)this->server_contexts->GetFront(); context; context = (SessionServerContext *)context->GetNext())
	{
		if (index == i++)
			return context;
	}

	return NULL;
}


/**
	Get pointer to joined server, or 0 if not joined.
*/
SessionServerContext *SessionClient::GetJoinedServer() const
{
	return this->joined ? this->join_server.Get() : NULL;
}


/**
	Join a session. This will simply send a join request to the
	server defined by the server index and return immediately.
*/
bool SessionClient::JoinSession(const char *guid)
{
	// currently joined?
	if (this->joined)
		this->LeaveSession();

	// dirtyfy client attributes
	this->attributes->SetDirty(true);

	// join to new session
	this->join_server = this->FindServerContextByGuid(guid);
	if (!this->join_server.IsValid())
	{
		// invalid server index
		LogWarning("Joining session failed, invalid server index");
		return false;
	}

	this->joined = this->join_denied = false;

	LogInfo2("Joining session to server %s:%u", this->join_server->GetHostName(), this->join_server->GetPortNum());
	return this->Send(this->join_server->GetIpcChannelId(), IpcBuffer( "~joinsession"));
}


/**
	Leave the currently joined session.
*/
bool SessionClient::LeaveSession()
{
	Assert(this->joined && this->join_server.IsValid());
	
	LogInfo2("Leaving session to server %s:%u", this->join_server->GetHostName(), this->join_server->GetPortNum());

	this->joined = this->join_denied = false;
	this->join_server = NULL;

	return this->Send(this->join_server->GetIpcChannelId(), IpcBuffer( "~leavesession"));;
}

/**
	Handles a start message from the session server. This will configure
	the net client object and open it. The job of the session client
	is finished, and it will gracefully exit.
*/
void SessionClient::StartGame(const char *server_host, const char *server_port)
{
	Assert(server_host);
	Assert(server_port);
	Assert(this->joined && this->join_server.IsValid());

	LogInfo2("Game started by server %s:%u", this->join_server->GetHostName(), this->join_server->GetPortNum());
/*
	NetClient* netClient = this->net_client.get();

	// configure the net client object
	netClient->SetClientGuid(this->GetClientGuid().Get());
	netClient->SetServerHostName(server_host);
	netClient->SetServerPortName(server_port);
	netClient->Open();
*/
}


// vim:ts=4:sw=4:
