//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file soundresource.cpp
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"
#include "framework/soundresource.h"
#include "framework/audioserver.h"

PrepareClass(SoundResource, "Resource", s_NewSoundResource, s_InitSoundResource);


//=========================================================================
// SoundResource
//=========================================================================

/**
	Constructor.
*/
SoundResource::SoundResource(const char *id) :
	Resource(id),
	streaming(false),
	looping(false),
	audio_file(NULL)
{
	Assert(AudioServer::GetInstance());
}


/**
	Destructor.
*/
SoundResource::~SoundResource()
{
	this->Unload();
}


bool SoundResource::UpdateStream()
{
	Assert(this->streaming);
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


void SoundResource::RewindStream()
{
	Assert(this->streaming);
	ErrorMsg("Overwrite this method in subclass");
}


// vim:ts=4:sw=4:
