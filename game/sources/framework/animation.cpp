//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file animation.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Added Deserialize method.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/animation.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "kernel/serializer.h"

PrepareClass(Animation, "Resource", s_NewAnimation, s_InitAnimation);


//=========================================================================
// Animation
//=========================================================================

/**
	Constructor.
*/
Animation::Animation(const char *id) :
	SerializedResource(id),
	type(AT_VECTROR2),
	loop_type(LT_NONE),
	samples(NULL),
	samples_count(0),
	indices_count(0),
	data_size(0),
	anim_time(0),
	random_begin(false)
{
	Assert(GfxServer::GetInstance());

	Resource::type = RES_ANIMATION;
}


/**
	Destructor.
*/
Animation::~Animation()
{
	this->Unload();
}


/**
*/
bool Animation::Deserialize(Serializer &serializer, bool first)
{
	// call ancestor
	if (!Root::Deserialize(serializer, first))
		return false;

	this->Clear();

	if (!this->ReadProperties(serializer))
	{
		LogError1("Error reading animation properties from: %s", this->file_name.c_str());

		// delete partially loaded data
		this->Clear();
		return false;
	}

	if (!this->ReadSamples(serializer))
	{
		LogError1("Error reading samples from: %s", this->file_name.c_str());

		// delete partially loaded data
		this->Clear();
		return false;
	}
	
	return true;
}


bool Animation::ReadProperties(Serializer &serializer)
{
	// read type [mandatory]
	string str_type;
	if (!serializer.GetAttribute("type", str_type))
	{
		LogError("Missing animation type");
		return false;
	}
	if (!Animation::StringToType(str_type, this->type))
	{
		LogError1("Invalid animation type: %s", str_type.c_str());
		return false;
	}

	// read loop [optional]
	this->loop_type = Animation::LT_NONE;
	if (serializer.GetAttribute("loop", str_type))
		Animation::StringToLoopType(str_type, this->loop_type);

	// read random [optional]
	this->random_begin = false;
	serializer.GetAttribute("random", this->random_begin);

	return true;
}


bool Animation::ReadSamples(Serializer &serializer)
{
	if (!serializer.GetGroup("samples"))
		return false;

	// read samples count [one mandatory]
	this->samples_count = serializer.GetGroupsCount();

	if (!this->samples_count || !serializer.GetGroup("sample"))
	{
		LogError("Missing at least one sample");
		return false;
	}

	// create samples array
	this->samples = NEW Animation::AnimationSample[this->samples_count];
	Assert(this->samples);

	// read samples [one mandatory]
	ushort_t i, j;
	ushort_t count;

	this->anim_time = 0;
	this->indices_count = 0;

	for (i = 0; i < this->samples_count; i++)
	{
		// read indices count [one mandatory]
		count = serializer.GetGroupsCount();
		if (!count)
		{
			LogError("Missing at least one sample index");
			return false;
		}

		// check and store indices count
		if (!this->indices_count)
			this->indices_count = count;	
		else if (count != this->indices_count)
		{
			LogError("Wrong number of sample indices");
			return false;
		}

		// read sample time [madatory]
		if (!serializer.GetAttribute("time", this->samples[i].sample_time))
		{
			LogError("Missing sample time");
			return false;
		}
		this->anim_time = this->samples[i].sample_time += this->anim_time;

		// read filter [optional]
		string str_type;
		this->samples[i].filter = Animation::AF_NONE;
		if (serializer.GetAttribute("filter", str_type))
			Animation::StringToFilter(str_type, this->samples[i].filter);

		// create indices array
		switch (this->type) {
			case Animation::AT_VECTROR2:
				this->samples[i].data = NEW vector2[this->indices_count];
				Assert(this->samples[i].data);
				break;
			case Animation::AT_VECTROR4:
				this->samples[i].data = NEW vector4[this->indices_count];
				Assert(this->samples[i].data);
				break;
			default:
				break;
		}

		if (!serializer.GetGroup("index"))
		{
			LogError("Indices mismatch");
			return false;
		}

		// load indices [one mandatory]
		vector2 *field_v2;
		vector4 *field_v4;
		
		for (j = 0; j < this->indices_count; j++)
		{
			// read data [mandatory]
			switch (this->type)
			{
			case Animation::AT_VECTROR2:
				field_v2 = (vector2 *)this->samples[i].data;
				Assert(field_v2);

				if (!serializer.GetAttribute("x", field_v2[j].x) ||
					!serializer.GetAttribute("y", field_v2[j].y))
				{
					LogError("Missing index data");
					return false;
				}
				break;

			case Animation::AT_VECTROR4:
				field_v4 = (vector4 *)this->samples[i].data;
				Assert(field_v4);

				if (!serializer.GetAttribute("x", field_v4[j].x) ||
					!serializer.GetAttribute("y", field_v4[j].y) ||
					!serializer.GetAttribute("z", field_v4[j].z) ||
					!serializer.GetAttribute("w", field_v4[j].w))
				{
					LogError("Missing index data");
					return false;
				}
				break;

			default:
				break;
			}


			// go to next index
			if ((j < this->indices_count - 1) && !serializer.GetNextGroup())
			{
				LogError("Indices mismatch");
				return false;
			}
		}

		serializer.EndGroup();

		// go to next sample
		if ((i < this->samples_count - 1) && !serializer.GetNextGroup())
		{
			LogError("Samples mismatch");
			return false;
		}
	}

	serializer.EndGroup();
	serializer.EndGroup();

	// compute data size
	this->data_size = sizeof(this->samples[0]) * this->samples_count;
	switch (this->type)
	{
	case Animation::AT_VECTROR2:
		this->data_size += sizeof(vector2) * this->samples_count * this->indices_count;
		break;
	case Animation::AT_VECTROR4:
		this->data_size += sizeof(vector4) * this->samples_count * this->indices_count;
		break;
	default:
		break;
	}

	return true;
}


void Animation::Clear()
{
	// delete animation
	for (ushort_t i = 0; i < this->samples_count; i++)
		SafeDeleteArray(this->samples[i].data);

	SafeDeleteArray(this->samples);

	this->samples_count = 0;
	this->indices_count = 0;
	this->data_size = 0;
}


ulong_t Animation::GetDataSize()
{
	return this->data_size;
}


bool Animation::TypeToString(AnimationType type, string &name)
{
	switch (type)
	{
	case AT_VECTROR2: name = "vector2";  return true;
	case AT_VECTROR4: name = "vector4";  return true;
	default:
		LogWarning1("Invalid animation type: %d", type);
		return false;
	}
}


bool Animation::StringToType(const string &name, AnimationType &type)
{
	if (name == "vector2")       type = AT_VECTROR2;
	else if (name == "vector4")  type = AT_VECTROR4;
	else
	{
		LogWarning1("Invalid animation type: %s", name.c_str());
		return false;
	}

	return true;
}


bool Animation::LoopTypeToString(LoopType loop_type, string &name)
{
	switch (loop_type)
	{
	case LT_NONE:   name = "none";    return true;
	case LT_CYCLE:  name = "cycle";   return true;
	case LT_ZIGZAG: name = "zigzag";  return true;
	default:
		LogWarning1("Invalid loop type: %d", loop_type);
		return false;
	}
}


bool Animation::StringToLoopType(const string &name, LoopType &loop_type)
{
	if (name == "none")         loop_type = LT_NONE;
	else if (name == "cycle")   loop_type = LT_CYCLE;
	else if (name == "zigzag")  loop_type = LT_ZIGZAG;
	else
	{
		LogWarning1("Invalid loop type: %s", name.c_str());
		return false;
	}

	return true;
}


bool Animation::FilterToString(AnimationFilter filter, string &name)
{
	switch (filter)
	{
	case AF_NONE:   name = "none";    return true;
	case AF_LINEAR: name = "linear";  return true;
	default:
		LogWarning1("Invalid animation filter: %d", filter);
		return false;
	}
}


bool Animation::StringToFilter(const string &name, AnimationFilter &filter)
{
	if (name == "none")         filter = AF_NONE;
	else if (name == "linear")  filter = AF_LINEAR;
	else
	{
		LogWarning1("Invalid animation filter: %s", name.c_str());
		return false;
	}

	return true;
}


// vim:ts=4:sw=4:
