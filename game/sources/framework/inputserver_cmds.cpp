//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file inputserver_cmds.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/inputserver.h"


//=========================================================================
// Commands
//=========================================================================

static void s_IsKeyPressed(void *slf, Cmd *cmd)
{
	InputServer *self = (InputServer *) slf;
	KeyCode code = self->GetKeyCode(string(cmd->In()->GetS()));
	cmd->Out()->SetB(self->IsKeyPressed(code));
}


static void s_GetMousePosition(void *slf, Cmd *cmd)
{
	InputServer *self = (InputServer *) slf;
	ushort_t x, y;
	self->GetMousePosition(x, y);
	cmd->Out()->SetI(x);
	cmd->Out()->SetI(y);
}


static void s_GetAbsMousePosition(void *slf, Cmd *cmd)
{
	InputServer *self = (InputServer *) slf;
	int x, y;
	self->GetAbsMousePosition(x, y);
	cmd->Out()->SetI(x);
	cmd->Out()->SetI(y);
}


static void s_GetMouseWheel(void *slf, Cmd *cmd)
{
	InputServer *self = (InputServer *) slf;
	cmd->Out()->SetI(self->GetMouseWheel());
}


static void s_Bind(void *slf, Cmd *cmd)
{
	InputServer *self = (InputServer *) slf;
	const char *def = cmd->In()->GetS();
	const char *action = cmd->In()->GetS();

	cmd->Out()->SetB(self->Bind(def, action));
}


static void s_UnbindAll(void *slf, Cmd *cmd)
{
	InputServer *self = (InputServer *) slf;
	self->UnbindAll();
}


static void s_AddActionCallback(void *slf, Cmd *cmd)
{
	InputServer *self = (InputServer *) slf;
	self->AddActionCallback(cmd->In()->GetS());
}

static void s_RemoveActionCallback(void *slf, Cmd *cmd)
{
	InputServer *self = (InputServer *) slf;
	bool result = self->RemoveActionCallback(cmd->In()->GetS());
	cmd->Out()->SetB(result);
}


void s_InitInputServer_cmds(Class *cl)
{
    cl->BeginCmds();

	cl->AddCmd("v_IsKeyPressed_s",         'IKPS', s_IsKeyPressed);
	cl->AddCmd("ii_GetMousePosition_v",    'GMSP', s_GetMousePosition);
	cl->AddCmd("ii_GetAbsMousePosition_v", 'GAMP', s_GetAbsMousePosition);
	cl->AddCmd("i_GetMouseWheel_v",        'GMSW', s_GetMouseWheel);
	cl->AddCmd("b_Bind_ss",                'BIND', s_Bind);
	cl->AddCmd("v_UnbindAll_v",            'UBNA', s_UnbindAll);
	cl->AddCmd("v_AddActionCallback_s",    'AACB', s_AddActionCallback);
	cl->AddCmd("b_RemoveActionCallback_s", 'RACB', s_RemoveActionCallback);

    cl->EndCmds();
}
