//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file gfxserver_cmds.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/gfxserver.h"


//=========================================================================
// Commands
//=========================================================================

static void s_SetWindowTitle(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	self->SetWindowTitle(string(cmd->In()->GetS()));
}


static void s_SetWindowPosition(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	int x = cmd->In()->GetI();
	int y = cmd->In()->GetI();
	self->SetWindowPosition(DisplayMode::WidowPosition(x, y));
}


static void s_SetWindowSize(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	int w = cmd->In()->GetI();
	int h = cmd->In()->GetI();
	self->SetWindowSize(DisplayMode::WidowSize(w, h));
}


static void s_GetWindowSize(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;

	cmd->Out()->SetI(self->GetWindowSize().width);
	cmd->Out()->SetI(self->GetWindowSize().height);
}


static void s_SetWindowVerticalSync(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	self->SetWindowVerticalSync(cmd->In()->GetB());
}


static void s_MinimizeWindow(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	self->MinimizeWindow();
}


static void s_RestoreWindow(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	self->RestoreWindow();
}


static void s_IsWindowMinimized(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	cmd->Out()->SetB(self->IsWindowMinimized());
}


static void s_IsWindowActive(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	cmd->Out()->SetB(self->IsWindowActive());
}


static void s_IsWindowAccelerated(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	cmd->Out()->SetB(self->IsWindowAccelerated());
}


static void s_OpenDisplay(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	cmd->Out()->SetB(self->OpenDisplay());
}


static void s_CloseDisplay(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	self->CloseDisplay();
}


static void s_PresentScene(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	self->PresentScene();
}


static void s_IsDisplayOpened(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	cmd->Out()->SetB(self->IsDisplayOpened());
}


static void s_SetCursorVisibility(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	string type = cmd->In()->GetS();

	if (type == "hidden")
		self->SetCursorVisibility(GfxServer::CURSOR_HIDDEN);
	else if (type == "system")
		self->SetCursorVisibility(GfxServer::CURSOR_SYSTEM);
	else if (type == "custom")
		self->SetCursorVisibility(GfxServer::CURSOR_CUSTOM);
}


static void s_GetCursorVisibility(void* slf, Cmd* cmd)
{
	GfxServer *self = (GfxServer *) slf;
	GfxServer::CursorVisibility type = self->GetCursorVisibility();

	switch (type)
	{
		case GfxServer::CURSOR_HIDDEN: cmd->Out()->SetS("hidden"); break;
		case GfxServer::CURSOR_SYSTEM: cmd->Out()->SetS("system"); break;
		case GfxServer::CURSOR_CUSTOM: cmd->Out()->SetS("custom"); break;
		default: break;
	}
}


void s_InitGfxServer_cmds(Class *cl)
{
    cl->BeginCmds();

	cl->AddCmd("v_SetWindowTitle_s",        'SWTL', s_SetWindowTitle);
	cl->AddCmd("v_SetWindowPosition_ii",    'SWPO', s_SetWindowPosition);
	cl->AddCmd("v_SetWindowSize_ii",        'SWSZ', s_SetWindowSize);
	cl->AddCmd("ii_GetWindowSize_v",        'GWSZ', s_GetWindowSize);
	cl->AddCmd("v_SetWindowVerticalSync_b", 'SWVS', s_SetWindowVerticalSync);
	cl->AddCmd("v_MinimizeWindow_v",        'MWND', s_MinimizeWindow);
	cl->AddCmd("v_RestoreWindow_v",         'RWND', s_RestoreWindow);
	cl->AddCmd("b_IsWindowMinimized_v",     'IWMM', s_IsWindowMinimized);
	cl->AddCmd("b_IsWindowActive_v",        'IWAC', s_IsWindowActive);
	cl->AddCmd("b_IsWindowAccelerated_v",   'IWAL', s_IsWindowAccelerated);
	cl->AddCmd("b_OpenDisplay_v",           'ODSP', s_OpenDisplay);
	cl->AddCmd("v_CloseDisplay_v",          'CDSP', s_CloseDisplay);
	cl->AddCmd("v_PresentScene_v",          'PSCN', s_PresentScene);
	cl->AddCmd("b_IsDisplayOpened_v",       'IDSO', s_IsDisplayOpened);
	cl->AddCmd("v_SetCursorVisibility_s",   'SCVS', s_SetCursorVisibility);
	cl->AddCmd("s_GetCursorVisibility_v",   'GCVS', s_GetCursorVisibility);

    cl->EndCmds();
}


// vim:ts=4:sw=4:
