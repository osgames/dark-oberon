//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file audiofile.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/audiofile.h"


//=========================================================================
// AudioFile
//=========================================================================

/**
	Constructor. Sets file mode to binary.
*/
AudioFile::AudioFile() :
	DataFile(),
	data(NULL),
	buff_size(0)
{
	this->binary = true;

	// initialize format structure
	this->format.bitrate = 0;
	this->format.bits = 0;
	this->format.channels = 0;
}


/**
	Destructor.
*/
AudioFile::~AudioFile()
{
	// empty
}


/**
	Returns loaded audio data.

	@param close  Whether file will be closed after returning the data.
	@return Format of audio data.
*/
char *AudioFile::GetData(bool close)
{
	// buffer is returned only when we have any data in it
	char *result = this->data_size > 0 ? this->data : NULL;

	// close file, but don't delete loaded data
	if (close) {
		this->data = NULL;
		this->Close();
	}

	return result;
}


/**
	Sets new audio data. Old data will be deleted.

	@param data Buffer with new data.
	@param size Size of buffer.
*/
void AudioFile::SetData(char *data, long size)
{
	Assert(data);
	Assert(size > 0);

	// delete old data
	this->DeleteData();

	// set new data
	this->data = data;
	this->data_size = this->buff_size = size;

	// set state to loaded
	this->state = LS_LOADED;
}


/**
	Deletes loaded data.
*/
void AudioFile::DeleteData()
{
	// delete data
	SafeDeleteArray(this->data);

	this->data_size = 0;
	this->buff_size = 0;
}


// vim:ts=4:sw=4:
