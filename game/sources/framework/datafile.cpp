//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file datafile.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/datafile.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"


//=========================================================================
// ImageFile
//=========================================================================

/**
	Constructor.
*/
DataFile::DataFile() :
	state(LS_UNLOADED),
	opened(false),
	binary(false),
	data_size(0),
	file(NULL)
{
	//
}


/**
	Destructor.
*/
DataFile::~DataFile()
{
	// Close() is virtual => put next lines into derived destructor!

	//	if (this->opened)
	//		this->Close();
}


/**
	Opens data file. Just creates File object for access to file in filesystem.

	@param write Whether file will be opened for writing.
	@return @c True if successful.
*/
bool DataFile::Open(bool write)
{
	Assert(!this->file_name.empty());
	Assert(FileServer::GetInstance());

	// close the file if it is opened
	if (this->opened)
		this->Close();

	// create data file
	this->file = FileServer::GetInstance()->NewFile();
	Assert(this->file);

	// choose mode
	char mode[3];
	mode[0] = write ? 'w' : 'r';
	mode[1] = this->binary ? 'b' : 't';
	mode[2] = 0;

	// open data file
	if (!this->file->Open(this->file_name, mode)) {
		LogError1("Error opening file: %s", this->file_name.c_str());
		SafeDelete(this->file);
		return false;
	}

	// all ok
	return this->opened = true;
}


/**
	Close data file. Deletes data and File object. Data file has to be opened before.
*/
void DataFile::Close()
{
	Assert(this->opened);

	// delete loaded data
	this->DeleteData();

	// close file
	SafeDelete(this->file);

	// set new states
	this->state = LS_UNLOADED;
	this->opened = false;
}


/**
	Loads data from opened file. Data has to be unloaded before.
	This function should be redefined in derived classes.

	@return Size of loaded data.
*/
ulong_t DataFile::LoadData()
{
	Assert(this->opened);
	Assert(this->state == LS_UNLOADED);

	LogWarning1("Loading is not implemented for data file: %s", this->file_name.c_str());

	this->state = LS_LOADED;
	return this->data_size;
}


/**
	Saves data into the file. Data has to be loaded before.
	This function can be redefined in derived classes.

	@return @c True if successful.
*/
bool DataFile::SaveData()
{
	Assert(this->opened);
	Assert(this->state == LS_LOADED);

	LogWarning1("Saving is not implemented for data file: %s", this->file_name.c_str());

	return false;
}


// vim:ts=4:sw=4:
