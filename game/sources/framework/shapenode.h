#ifndef __shapenode_h__
#define __shapenode_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file shapenode.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/root.h"
#include "kernel/ref.h"
#include "kernel/env.h"
#include "kernel/vector.h"
#include "framework/scenenode.h"

class Texture;
class Mesh;
class Animation;


//=========================================================================
// ShapeNode
//=========================================================================

/**
	@class ShapeNode
	@ingroup Framework_Module

	@brief The ShapeNode is the base class of all objects which can be attached
    to a scene managed by the SceneServer class. A scene node object
    may provide transform or geometry information.
*/

class ShapeNode : public SceneNode
{
//--- variables
protected:
	// basic shape
	string texture_file;
	string overlay_file;
	string mesh_file;
	vector3 overlay_color;       ///< Color used for overlay image.

	Ref<Texture> texture;
	Ref<Mesh> mesh;
	vector2 *mapping;
	bool absolute;               ///< Whether mapping coordinates are absolute [0..size] or relative [0..1].

	// animations
	string mapping_file;
	string coords_file;
	string colors_file;

	vector2 *mapping_buff;       ///< Buffer for animated mapping.
	vector2 *coords_buff;        ///< Buffer for animated coords.
	vector4 *colors_buff;        ///< Buffer for animated colors.

#ifdef USE_PROFILERS
	static Ref<Env> shapes_variable;
	static Ref<Env> triangles_variable;
	static ulong_t last_frame;
#endif


//--- methods
public:
	ShapeNode(const char *id);
	virtual ~ShapeNode();

    virtual bool LoadResources();
    virtual void UnloadResources();
	virtual void Render();
	virtual void CreateAnimators();

	void SetTextureFile(const string &textrure_file);
	void SetOverlayFile(const string &overlay_file);
	void SetOverlayColor(const vector3 &color);
	void SetMeshFile(const string &mesh_file, vector2 *mapping, bool absolute);
	Texture *GetTexture();
	Mesh *GetMesh();

	void SetMappingAnimation(const string &mapping_file);
	void SetCoordsAnimation(const string &coords_file);
	void SetColorsAnimation(const string &colors_file);

protected:
	bool LoadTexture();
	bool LoadMesh();

	void UnloadTexture();
	void UnloadMesh();
};


//=========================================================================
// Methods
//=========================================================================


inline
Texture *ShapeNode::GetTexture()
{
	return this->texture.GetUnsafe();
}


inline
Mesh *ShapeNode::GetMesh()
{
	return this->mesh.GetUnsafe();
}


inline
void ShapeNode::SetOverlayColor(const vector3 &color)
{
	this->overlay_color = color;
}


#endif  // __shapenode_h__

// vim:ts=4:sw=4:
