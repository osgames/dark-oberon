//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sceneserver.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sceneserver.h"
#include "framework/animator.h"
#include "kernel/kernelserver.h"

PrepareClass(SceneServer, "Root", s_NewSceneServer, s_InitSceneServer);

#define MAX_ID_LENGTH 50


//=========================================================================
// SceneServer
//=========================================================================

/**
	Constructor.
*/
SceneServer::SceneServer(const char *id) :
	RootServer<SceneServer>(id)
{
	this->kernel_server->PushCwd(this);
	this->scenes = this->kernel_server->New("Root", string("scenes"));
	Assert(this->scenes);
	this->kernel_server->PopCwd();
}


/**
	Destructor.
*/
SceneServer::~SceneServer()
{
	//
}


/**
*/
Scene *SceneServer::NewScene(const char* class_name)
{
	Assert(class_name);

	Scene *scene;
	static uint_t id_counter = 0;
	char buff[MAX_ID_LENGTH + 1];

	sprintf(buff, "scene_%u", id_counter++);

	this->kernel_server->PushCwd(this->scenes.Get());
	scene = (Scene *)this->kernel_server->New(class_name, string(buff));
	Assert(scene);
	this->kernel_server->PopCwd();

	return scene;
}


SceneObject *SceneServer::NewSceneObject(const char* class_name, const string *model_file)
{
	Assert(class_name);

	SceneObject *object;
	static uint_t id_counter = 0;
	char buff[MAX_ID_LENGTH + 1];

	sprintf(buff, "scene_object_%u", id_counter++);

	object = (SceneObject *)this->kernel_server->NewObject(class_name, buff);
	Assert(object);

	if (model_file)
		object->AddModel(*model_file);

	return object;
}


Animator *SceneServer::NewAnimator(const string &anim_file, void *buffer)
{
	Assert(!anim_file.empty() && buffer);

	Animator *animator;
	static uint_t id_counter = 0;
	char buff[MAX_ID_LENGTH + 1];

	sprintf(buff, "animator_%u", id_counter++);
	
	animator = (Animator *)this->kernel_server->New("Animator", string(buff));
	Assert(animator);

	animator->SetAnimation(anim_file);
	animator->SetBuffer(buffer);

	return animator;
}


// vim:ts=4:sw=4:
