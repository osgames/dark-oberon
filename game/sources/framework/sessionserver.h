#ifndef __sessionserver_h__
#define __sessionserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionserver.h
	@ingroup Framework_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2008

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/sessionclientcontext.h"
#include "kernel/ipcserver.h"
#include "kernel/ref.h"
#include "kernel/guid.h"

class Attributes;
//class NetServer;
//class NetClient;


//=========================================================================
// SessionServer
//=========================================================================

/**
	@class SessionServer
	@ingroup Framework_Module

	@brief A session server manages all necessary information about a network
	session, and eventually configures and opens the actual game network server.

	<h2>Session info broadcasting</h2>

	An opened session server periodically broadcasts its presence
	into the LAN approx once per second. The broadcast is sent on
	the IpcAddress <tt>self:BroadcastAPPNAME</tt>, where @c APPNAME is the
	application name set via SessionServer::SetAppName(). The message
	is a normal string of the following format:

@verbatim
	"~session Guid AppName AppVersion IpAddress IpPort"
@endverbatim

	The guid is an unique session id understood by the Nebula Guid
	class. @c Appname and @c Appversion are the respective application
	id strings set by SessionServer::SetAppName() and
	SessionServer::SetAppVersion(). Potential clients which
	want to know more about the session should initiate a
	normal IpcServer/nIpcClient connection to the session server
	using the @c IpAddress and @c IpPort fields of the session broadcast
	message.

	<h2>Protocol spec:</h2>

	Once a potential client has found out about a session server by
	listening on the above broadcast message it may establish a
	IpcServer/nIpcClient connection to get a reliable communication
	channel with the server and implement the following protocol:

@verbatim
	---
	client: "~queryserverattrs"
	server: ---
@endverbatim

	This message is sent by a new client to obtain a copy of the current
	server attributes. The server will reply with <tt>~serverattr</tt>
	messages to that client (see below).

@verbatim
	---
	server: "~serverattr AttrName [AttrValue]"
	client: ---
@endverbatim

	This message is sent by the server to all connected clients
	as soon as a refresh of one or more server attributes is necessary.
	Server attributes are Name/Value string pairs which contain info such
	as current number of players, max number of players, etc... It
	is totally up to the application which server attributes it defines.
	The server does not expect an answer from the clients.

@verbatim
	---
	server: "~closesession"
	client: ---
@endverbatim

	This message is sent by the server to all connected clients when
	the session is going to be closed/cancelled. This means the session
	will no longer exists. If a game is started from the session, all
	connected non-joined clients will receive a <tt>~closesession</tt>
	message, while all joined clients will receive a <tt>~start</tt>
	message from the server.

@verbatim
	---
	server: "~start IpAddress IpPort"
	client: ---
@endverbatim

	This message is sent by the server to all connected clients when
	the game session is about to start. The provided @c IpcAddress is
	the address of the GameServer which the client should contact
	right after receiving the <tt>~start</tt> message from the session
	server. The session server itself will stop its duty after
	the <tt>~start</tt> message has been sent, as the GameServer will
	take over from here on.

@verbatim
	---
	client: "~joinsession client_guid"
	server: "~joinaccepted" OR
			"~joindenied"
@endverbatim

	This message is sent by a client when it wants to join a session.
	The server responds either with a <tt>~joinaccepted clientName</tt>
	message, where @c clientName is a string which identifies the client
	for later communication, or a <tt>~joindenied</tt> message, the most
	common reason for <tt>~joindenied</tt> is that the session was full.

@verbatim
	---
	client: "~clientattr client_guid AttrName [AttrValue]"
	server: ---
@endverbatim

	This message is sent by joined clients to the server when a client attribute
	needs a refresh, or right after a session is joined. Client attributes
	contain client specific data like @c PlayerName, @c PlayerColor. It is
	totally up to the application which data is defined as client attribute.

	The server will convert client attributes to server attributes, which then
	get automatically distributed to all clients. The server will prefix
	the client attribute's name with a @c ClientX_ prefix, where X is a
	number from 0 to (MaxClients - 1).

@verbatim
	---
	server: "~kick"
	client: ---
@endverbatim

	This message is sent by the server to a joined client which has
	been kicked from the session by the host player. The client
	should simply reply with a <tt>~leavesession client_guid</tt> message.

@verbatim
	---
	client: "~leavesession client_guid"
	server: ---
@endverbatim

	This message is sent by a joined client to the server when it
	wants to leave the session. The client will still receive
	<tt>~serverattr</tt> messages from the server, but no longer join-specific
	messages.

@verbatim
	---
	NOT YET IMPLEMENTED

	server: "~ping client_guid"
	client: "~pong client_guid"
@endverbatim

	This is a keepalive message which the server sends to every joined
	client every few seconds just to keep the connection alive. The
	client should respond with a <tt>~pong client_id</tt> message. If no
	messages arrive for at least 10 seconds on the either the server or the
	client side, the connection should be considered dead by both sides.
*/

class SessionServer : public IpcServer
{
//--- embeded
private:
	enum
	{
		ALL_CLIENTS = uint_t(-1),
	};


//--- methods
public:
	SessionServer(const char *id);
	virtual ~SessionServer();

	void SetAppName(const char *name);
	const char *GetAppName() const;
	void SetAppVersion(const char *version);
	const char *GetAppVersion() const;
	void SetMaxClientsCount(ushort_t num);
	ushort_t GetMaxClientsCount() const;
	bool IsOpened() const;

	void CleanupDeadSessions();
	bool KickClient(const string &client_id);
	Attributes *GetAttributes() const;

	bool Open();
	void Close();
	bool Start();

	virtual bool Trigger();
	virtual void ProcessData(uint_t channel_id, const IpcBuffer &buffer);

private:
	void BroadcastIdentity();
	void SendServerAttrs(uint_t channel_id);
	void ProcessJoinSessionRequest(uint_t channel_id);
	void ProcessLeaveSessionRequest(uint_t channel_id);
	void ProcessClientAttribute(uint_t channel_id, const char *name, const char *value);
	void UpdateNumPlayersAttr();
	void UpdateServerClientAttrs();
	void UpdateMaxNumPlayersAttr();

	SessionClientContext *FindClientContext(uint_t channel_id);
	bool KickClient(SessionClientContext *client_context);


//--- variables
private:
	//Ref<NetServer> net_server;
	//Ref<NetClient> net_client;
	Ref<Root> client_contexts;
	Ref<Attributes> attributes;

	Guid session_guid;
	string app_name;
	string app_version;
	ushort_t max_clients_count;
	ushort_t clients_count;
	bool opened;
	IpcAddress session_address;
	double broadcast_time;
};


//=========================================================================
// Methods
//=========================================================================

inline
void SessionServer::SetAppName(const char *name)
{
	Assert(name);
	this->app_name = name;
}


inline
const char *SessionServer::GetAppName() const
{
	return this->app_name.empty() ? NULL : this->app_name.c_str();
}


inline
void SessionServer::SetAppVersion(const char *version)
{
	Assert(version);
	this->app_version = version;
}


inline
const char *SessionServer::GetAppVersion() const
{
	return this->app_version.empty() ? NULL : this->app_version.c_str();
}


inline
void SessionServer::SetMaxClientsCount(ushort_t count)
{
	Assert(count > 0);
	this->max_clients_count = count;

	// update the MaxNumPlayers server attribute
	this->UpdateMaxNumPlayersAttr();
	this->UpdateNumPlayersAttr();
	this->UpdateServerClientAttrs();
}


inline
ushort_t SessionServer::GetMaxClientsCount() const
{
	return this->max_clients_count;
}


inline
bool SessionServer::IsOpened() const
{
	return this->opened;
}


inline
Attributes *SessionServer::GetAttributes() const
{
	return this->attributes;
}


#endif

// vim:ts=4:sw=4:
