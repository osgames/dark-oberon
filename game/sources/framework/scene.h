#ifndef __scene_h__
#define __scene_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scene.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Scene doesn't release objects.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/root.h"
#include "kernel/ref.h"
#include "kernel/env.h"
#include "framework/sceneobject.h"
#include "framework/gfxstructures.h"

class Profiler;


//=========================================================================
// Scene
//=========================================================================

/**
	@class Scene
	@ingroup Framework_Module
*/

class Scene : public Root
{
	friend class SceneServer;

//--- variables
protected:
	Ref<Root> objects;
	uint_t objects_count;

	Viewport viewport;
	Projection projection;
	bool sorting;

#ifdef USE_PROFILERS
	static Ref<Profiler> sorting_profiler;     ///< Profiler for sorting objects in scene.
	static Ref<Profiler> animations_profiler;  ///< Profiler for objects animations.
	static Ref<Env> objects_variable;
	static ulong_t last_frame;
#endif

//--- methods
public:
	Scene(const char *id);
	virtual ~Scene();

	void SetViewport(const Viewport& viewport);
	void SetProjection(const Projection& projection);
	void SetSorting(bool sorting);

	virtual void AddObject(SceneObject *object);
	void RemoveObject(SceneObject *object);
	void RemoveAllObjects();
	bool HasObject(SceneObject *object);
	void RenderObject(SceneObject *object, bool set_projection = true, bool all_features = false);

protected:
	virtual void Render();
	virtual void SortObjects();
	virtual void CalculatePosition(const vector3 &object_position, vector3 &proj_position);
};


//=========================================================================
// Methods
//=========================================================================

inline
void Scene::SetViewport(const Viewport& viewport)
{
	this->viewport = viewport;
}


inline
void Scene::SetProjection(const Projection& projection)
{
	this->projection = projection;
}


inline
void Scene::SetSorting(bool sorting)
{
	this->sorting = sorting;
}


inline
void Scene::RemoveAllObjects()
{
	this->Lock();
	this->objects->ClearChildren();
	this->objects_count = 0;
	this->Unlock();
}


inline
bool Scene::HasObject(SceneObject *object)
{
	Assert(object);

	return (object->GetParent() == this->objects.Get());
}


#endif  // __scene_h__

// vim:ts=4:sw=4:
