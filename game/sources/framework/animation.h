#ifndef __animation_h__
#define __animation_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file animation.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Added Deserialize method.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/resource.h"


//=========================================================================
// Animation
//=========================================================================

/**
	@class Animation
	@ingroup Framework_Module
*/
    
class Animation : public SerializedResource
{
//--- embeded
public:
	enum AnimationType
	{
		AT_VECTROR2,
		AT_VECTROR4
	};

	enum AnimationFilter
	{
		AF_NONE,
		AF_LINEAR
	};

	enum LoopType
	{
		LT_NONE,
		LT_CYCLE,
		LT_ZIGZAG
	};

	struct AnimationSample
	{
		double sample_time;
		AnimationFilter filter;
		void *data;

		AnimationSample() : data(NULL) {};
	};

//--- variables
protected:
	AnimationType type;
	LoopType loop_type;
	AnimationSample *samples;

	ushort_t samples_count;
	ushort_t indices_count;
	double anim_time;
	bool random_begin;

	ulong_t data_size;

//--- methods
public:
	Animation(const char *id);
	virtual ~Animation();

	AnimationType GetType();
	bool TestType(AnimationType type);
	LoopType GetLoopType();
	bool TestLoopType(LoopType loop_type);
	ushort_t GetSamplesCount();
	ushort_t GetIndicesCount();
	AnimationSample *GetSamples();
	double GetAnimTime();
	bool IsRandomBegin();

	virtual ulong_t GetDataSize();

	static bool TypeToString(AnimationType type, string &name);
	static bool StringToType(const string &name, AnimationType &type);
	static bool LoopTypeToString(LoopType loop_type, string &name);
	static bool StringToLoopType(const string &name, LoopType &loop_type);
	static bool FilterToString(AnimationFilter filter, string &name);
	static bool StringToFilter(const string &name, AnimationFilter &filter);

protected:
	void Clear();

	// serializing
	virtual bool Deserialize(Serializer &serializer, bool first);
	bool ReadProperties(Serializer &serializer);
	bool ReadSamples(Serializer &serializer);
};


//=========================================================================
// Methods
//=========================================================================

inline
Animation::AnimationType Animation::GetType()
{
	return this->type;
}


inline
bool Animation::TestType(Animation::AnimationType type)
{
	return this->type == type;
}


inline
Animation::LoopType Animation::GetLoopType()
{
	return this->loop_type;
}


inline
bool Animation::TestLoopType(Animation::LoopType loop_type)
{
	return this->loop_type == loop_type;
}


inline
ushort_t Animation::GetSamplesCount()
{
	return this->samples_count;
}


inline
ushort_t Animation::GetIndicesCount()
{
	return this->indices_count;
}


inline
Animation::AnimationSample *Animation::GetSamples()
{
	return this->samples;
}


inline
double Animation::GetAnimTime()
{
	return this->anim_time;
}


inline
bool Animation::IsRandomBegin()
{
	return this->random_begin;
}


#endif // __animation_h__

// vim:ts=4:sw=4:
