//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file remoteserver.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/remoteserver.h"
#include "framework/remoteclient.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/messageserver.h"

PrepareClass(RemoteServer, "Root", s_NewRemoteServer, s_InitRemoteServer);


//=========================================================================
// RemoteServer
//=========================================================================

/**
	Saves pointer to message server.
*/
RemoteServer::RemoteServer(const char *id) :
	RootServer<RemoteServer>(id),
	master(true),
	max_clients(255)
{
	this->message_server = MessageServer::GetInstance();
	if (!this->message_server.IsValid())
		ErrorMsg("MessageServer has to be created before RemoteServer");
}


RemoteServer::~RemoteServer()
{
	//
}


bool RemoteServer::RegisterClient(RemoteClient *client)
{
	Assert(client);

	this->Lock();
	bool result = false;

	if (this->master)
	{
		static byte_t counter = 1;
		if (counter > this->max_clients)
			LogError("Maximum number of remote clients reached");

		else
		{
			this->clients[counter] = client;
			client->remote_id = counter++;
			client->local = true;
			result = true;

			/// @todo Creating clients in all slave servers.
		}
	}

	else
	{
		/// @todo Registerimg client in master server.
		ErrorMsg("Not implemented");
	}

	this->Unlock();

	return result;
}


bool RemoteServer::DeregisterClient(RemoteClient *client)
{
	Assert(client && client->remote_id && client->local);

	this->Lock();
	bool result = false;

	if (this->master)
	{
		// find client in hash map
		ClientsMap::iterator itClient = this->clients.find(client->remote_id);
		if (itClient == this->clients.end())
			LogError1("Deregistering not registered remote client: %hu", client->remote_id);

		else
		{
			this->clients.erase(itClient);
			client->remote_id = 0;
			client->local = false;
			result = true;

			/// @todo Deleting clients in all slave servers.
		}
	}

	else
	{
		/// @todo Deregisterimg client in master server.
		ErrorMsg("Not implemented");
	}

	this->Unlock();

	return result;
}


bool RemoteServer::CreateRemoteClient(byte_t client_id)
{
	ErrorMsg("Overwrite this method in subclass");
	return false;
}


void RemoteServer::SendRemoteMessage(BaseMessage *message, RemoteClient *sender, RemoteClient *destination, double delay)
{
	Assert(message && sender && destination);
	Assert(this->clients[sender->remote_id] == sender);
	Assert(this->clients[destination->remote_id] == destination);

	if (destination->IsLocal())
		this->message_server->SendMessage(message, sender, destination, delay);

	/// @todo Sending message to remote client.
	else
		ErrorMsg("Not implemented");
	/*
	else if (this->master)
		send_by_network_to_proper_slave();

	else
		send_by_network_to_master();
	*/
}


// vim:ts=4:sw=4:
