//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file model.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/model.h"
#include "framework/gfxserver.h"
#include "framework/shapenode.h"
#include "framework/transformnode.h"
#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "kernel/serializeserver.h"

PrepareClass(Model, "Resource", s_NewModel, s_InitModel);


//=========================================================================
// Model
//=========================================================================

/**
	Constructor.
*/
Model::Model(const char *id) :
	SerializedResource(id),
	data_size(0)
{
	Assert(GfxServer::GetInstance());

	this->type = RES_MODEL;
}


/**
	Destructor.
*/
Model::~Model()
{
	this->Unload();
}


/**
	Load the resource using SerializeServer.
*/
bool Model::LoadResource()
{
	Assert(!this->loaded);
	Assert(SerializeServer::GetInstance());

	if (!SerializeServer::GetInstance()->Deserialize(this->file_name, this))
	{
		LogError1("Error loading model from file: %s", this->file_name.c_str());
		return false;
	}

	if (!this->root_node->PreloadResources())
	{
		LogError1("Error loading model from file: %s", this->file_name.c_str());
		this->Clear();
		return false;
	}

	this->loaded = true;
	return true;
}


/**
*/
bool Model::Deserialize(Serializer &serializer, bool first)
{
	// call ancestor
	if (!Root::Deserialize(serializer, first))
		return false;

	this->Clear();

	KernelServer::GetInstance()->PushCwd(this);
	bool ok = this->ReadSceneNodes(serializer, true);
	KernelServer::GetInstance()->PopCwd();

	if (!ok)
	{
		LogError1("Error reading scene nodes from: %s", this->file_name.c_str());

		// delete partially loaded data
		this->Clear();
		return false;
	}

	return true;
}


/**
	Recursively loads all scene nodes.

	@param serializer  Serializer used to read data.
	@param root        Whether given element is an root element.
	@return @c True if successful.
*/
bool Model::ReadSceneNodes(Serializer &serializer, bool root)
{
	// go to first subgroup [optional]
	if (!serializer.GetGroup())
		return true;

	// read scene nodes
	do
	{
		// read shape node
		if (serializer.CheckGroupName("shape"))
		{
			if (!this->ReadShapeNode(serializer, root))
				return false;
		}

		// read transform node
		if (serializer.CheckGroupName("transform"))
		{
			if (!this->ReadTransformNode(serializer, root))
				return false;
		}

	} while (!root && serializer.GetNextGroup(true));

	// finish group
	serializer.EndGroup();
	return true;
}


bool Model::ReadShapeNode(Serializer &serializer, bool root)
{
	string texture, overlay, mesh;
	string mapping_anim, coords_anim, colors_anim;
	ushort_t vertices_count;
	vector2 *mapping = NULL;
	bool absolute = false;

	// read mesh [mandatory]
	if (!serializer.GetGroup("mesh"))
	{
		LogError("Missing shape mesh");
		return false;
	}

	// read mesh src [mandatory]
	if (!serializer.GetAttribute("src", mesh, false))
	{
		LogError("Missing mesh source");
		return false;
	}

	serializer.EndGroup();

	// read texture [optional]
	if (serializer.GetGroup("texture"))
	{
		// read src [mandatory]
		if (!serializer.GetAttribute("src", texture, false))
		{
			LogError("Missing texture source");
			return false;
		}

		serializer.EndGroup();
	}

	// read overlay [optional]
	if (serializer.GetGroup("overlay"))
	{
		// read src [mandatory]
		if (!serializer.GetAttribute("src", overlay, false))
		{
			LogError("Missing overlay source");
			return false;
		}

		serializer.EndGroup();
	}

	// read animations [optional]
	if (serializer.GetGroup("animations"))
	{
		// read mapping [optional]
		if (serializer.GetGroup("mapping"))
		{
			// read src [mandatory]
			if (!serializer.GetAttribute("src", mapping_anim, false))
			{
				LogError("Missing mapping animation source");
				return false;
			}

			serializer.EndGroup();
		}

		// read coords [optional]
		if (serializer.GetGroup("coords"))
		{
			// read src [mandatory]
			if (!serializer.GetAttribute("src", coords_anim, false))
			{
				LogError("Missing coords animation source");
				return false;
			}

			serializer.EndGroup();
		}

		// read colors [optional]
		if (serializer.GetGroup("colors"))
		{
			// read src [mandatory]
			if (!serializer.GetAttribute("src", colors_anim, false))
			{
				LogError("Missing colors animation source");
				return false;
			}

			serializer.EndGroup();
		}

		serializer.EndGroup();
	}

	// read mapping [optional]
	if (serializer.GetGroup("mapping"))
	{
		// read absolute [optional]
		serializer.GetAttribute("absolute", absolute);

		// read vertices count [one mandatory]
		vertices_count = serializer.GetGroupsCount();
		if (!vertices_count || !serializer.GetGroup("vertex"))
		{
			LogError("Missing at least one mapping vertex");
			return false;
		}

		// create array
		mapping = NEW vector2[vertices_count];
		Assert(mapping);
		this->data_size += sizeof(mapping[0]) * vertices_count;

		// read vertices [one mandatory]
		for (ushort_t i = 0; i < vertices_count; i++)
		{
			// read coords [mandatory]
			if (!serializer.GetAttribute("x", mapping[i].x) ||
				!serializer.GetAttribute("y", mapping[i].y))
			{
				LogError("Missing vertex coodinates");
				delete[] mapping;
				return false;
			}

			// go to next group
			if ((i < vertices_count - 1) && !serializer.GetNextGroup())
			{
				LogError("Vertices mismatch");
				delete[] mapping;
				return false;
			}
		}

		serializer.EndGroup();
		serializer.EndGroup();
	}

	// compute shape id
	static ulong_t counter = 0;
	char temp[50];
	sprintf(temp, "ShapeNode%lu", counter++);
	string node_id = temp;

	// create shape node
	ShapeNode *shape_node = (ShapeNode *)KernelServer::GetInstance()->New("ShapeNode", node_id);
	Assert(shape_node);
	shape_node->SetTextureFile(texture);
	shape_node->SetOverlayFile(overlay);
	shape_node->SetOverlayColor(this->overlay_color);
	shape_node->SetMeshFile(mesh, mapping, absolute);
	shape_node->SetMappingAnimation(mapping_anim);
	shape_node->SetCoordsAnimation(coords_anim);
	shape_node->SetColorsAnimation(colors_anim);

	if (root)
		this->root_node = shape_node;

	// child scene nodes
	KernelServer::GetInstance()->PushCwd(shape_node);
	bool ok = this->ReadSceneNodes(serializer, false);
	KernelServer::GetInstance()->PopCwd();

	return ok;
}


bool Model::ReadTransformNode(Serializer &serializer, bool root)
{
	vector2 translation;
	float rotation = 0;

	// read properties [optional]
	serializer.GetAttribute("x", translation.x);
	serializer.GetAttribute("y", translation.y);
	serializer.GetAttribute("angle", rotation);

	// compute shape id
	static ulong_t counter = 0;
	char temp[50];
	sprintf(temp, "TransformNode%lu", counter++);
	string node_id = temp;

	// create transform node
	TransformNode *transform_node = (TransformNode *)KernelServer::GetInstance()->New("TransformNode", node_id);
	Assert(transform_node);
	transform_node->SetTranslation(translation);
	transform_node->SetRotation(rotation);

	if (root)
		this->root_node = transform_node;

	// child scene nodes
	KernelServer::GetInstance()->PushCwd(transform_node);
	bool ok = this->ReadSceneNodes(serializer, false);
	KernelServer::GetInstance()->PopCwd();

	return ok;
}


/**
*/
void Model::Clear()
{
	if (this->root_node.IsValid())
		this->root_node->Release();

	this->root_node = NULL;
	this->data_size = 0;
}

ulong_t Model::GetDataSize()
{
	return this->data_size;
}


// vim:ts=4:sw=4:
