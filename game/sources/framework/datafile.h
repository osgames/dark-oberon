#ifndef __datafile_h__
#define __datafile_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file datafile.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"

class File;


//=========================================================================
// DataFile
//=========================================================================

/**
	@class DataFile
	@ingroup Framework_Module

	Provides read / write access to any data file, text or binary.

	This class is the base for all data files used in framework. All data files has to use File object for
	reading and writing the data. This File object is creted and file is opend/close here.

	Each descendant can define the type of data file (text / binary) in constructor and should define all
	virtual methods, especially LoadData and DeleteData.
*/

class DataFile
{
//-- embeded
public:
	/**
		Loading state of the file.
	*/
	enum LoadState
	{
		LS_UNLOADED,       ///< The file is not loaded yet.
		LS_BLOCK_LOADED,   ///< Block of data has been loaded from the file.
		LS_LOADED          ///< Whole file has been loaded.
	};


//-- variables
protected:
	File *file;            ///< File object. This object is automatically creted in Open method and destroyed in Close method.
	string file_name;      ///< File name with path (relative or absolute).

	LoadState state;       ///< Loading state of the file.
	bool opened;           ///< Whether file is opened or not.
	bool binary;           ///< Whether file will be opened in binary mode. Set this property before opening the file.
	ulong_t data_size;     ///< Whole size of loaded data. Data size should by calculated in LoadData method.


//-- methods
public:
	DataFile();
	virtual ~DataFile();

	virtual bool Open(bool write = false);
	virtual void Close();
	virtual ulong_t LoadData();
	virtual bool SaveData();

	void SetFileName(const string &file_name);
	const string &GetFileName();
	ulong_t GetDataSize();
	LoadState GetState();
	bool TestState(LoadState state);
	bool IsOpened();

protected:
	virtual void DeleteData() = 0;
};


//=========================================================================
// Methods
//=========================================================================

/**
	Sets file name with path (relative or absolute).

	@param file_name File name with path.
*/
inline
void DataFile::SetFileName(const string &file_name)
{
	Assert(!file_name.empty());
	this->file_name = file_name;
}


inline
const string &DataFile::GetFileName()
{
	return this->file_name;
}


/**
	Returns data size.

	@return Data size.
*/
inline
ulong_t DataFile::GetDataSize()
{
	return this->data_size;
}


/**
	Returns load state of the file.

	@return Load state.
*/
inline
DataFile::LoadState DataFile::GetState()
{
	return this->state;
}


/**
	Tests given state.

	@param state Loading state to be tested.
	@return @c True if given state is same as state of the file.
*/
inline
bool DataFile::TestState(LoadState state)
{
	return (this->state == state);
}


/**
	Returns opening state.

	@return Whether file is openes.
*/
inline
bool DataFile::IsOpened()
{
	return this->opened;
}


#endif // __datafile_h__

// vim:ts=4:sw=4:
