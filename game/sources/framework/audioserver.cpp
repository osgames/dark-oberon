//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file audioserver.cpp
	@ingroup Framework_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/audioserver.h"
#include "framework/sound.h"
#include "framework/resourceserver.h"
#include "formats/oggaudiofile.h"
#include "kernel/kernelserver.h"
#include "kernel/timeserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"

PrepareScriptClass(AudioServer, "Root", s_NewAudioServer, s_InitAudioServer, s_InitAudioServer_cmds);


//=========================================================================
// AudioServer
//=========================================================================

/**
	Constructor.
*/
AudioServer::AudioServer(const char *id) :
	RootServer<AudioServer>(id),
	device_opened(false),
	muted(false),
	listener(NULL),
	distance_model(DM_INVERSE_CLAMPED),
	master_volume(1.0f),
	doppler_factor(1.0f),
	speed_of_sound(343.0f),
	volume_dirty(true),
	props_dirty(true)
{
	if (!ResourceServer::GetInstance())
		ErrorMsg("ResourceServer has to be created before AudioServer");
	if (!TimeServer::GetInstance())
		ErrorMsg("TimeServer has to be created before AudioServer");

	this->resource_server = ResourceServer::GetInstance();
	this->sounds_list = this->resource_server->GetResourceList(Resource::RES_SOUND);

	for (int i = 0; i < Sound::SND_COUNT; i++) {
		this->category_volume[i] = 1.0f;
		this->category_allowed[i] = true;
	}
}


/**
	Destructor.
*/
AudioServer::~AudioServer()
{
	Assert(!this->device_opened);
}


/**
	Opens the audio device.
*/
bool AudioServer::OpenDevice()
{
	Assert(!this->device_opened);

	LogInfo("Opening audio device");

	this->device_opened = true;

	return true;
}


/**
	Close the audio device.
*/
void AudioServer::CloseDevice()
{
	LogInfo("Closing audio device");

	// release sound resources
	this->resource_server->UnloadResources(Resource::RES_SOUND);
	this->resource_server->UnloadResources(Resource::RES_SOUND_RESOURCE);

	this->device_opened = false;
}


/**
	Writes basic audio information to log file.
*/
void AudioServer::WriteInfo()
{
	// empty
}


bool AudioServer::Trigger()
{
	if (!this->device_opened)
		return false;

	// update state/properties
	this->UpdateAllSounds();
	this->UpdateListener();

	this->volume_dirty = this->props_dirty = false;

	// update streams
	static double last_stream_update = -10;
	static double real_time;

	real_time = TimeServer::GetInstance()->GetRealTime();
	if (real_time - last_stream_update > AUDIO_STREAM_TIME_CHECK) {
		this->UpdateAllStreams();
		last_stream_update = real_time;
	}

	return true;
}


/**
	Creates a shared sound resource object.
*/
SoundResource *AudioServer::NewSoundResource(const string &/*file_name*/)
{
	ErrorMsg("Overwrite this method in subclass");
	return NULL;
}


/**
	Creates a non-shared sound object.
*/
Sound *AudioServer::NewSound(const string &/*file_name*/)
{
	ErrorMsg("Overwrite this method in subclass");
	return NULL;
}


AudioFile *AudioServer::NewAudioFile(const string &file_name, bool open, bool write)
{
	AudioFile *file;
	FileServer *fs = FileServer::GetInstance();

	// check extension
	string ext;
	FileServer::ExtractExtension(file_name, ext);

	if (ext == "ogg")
		file = NEW OggAudioFile();

	else {
		LogError1("Unknown audio format: '%s'", file_name.c_str());
		return NULL;
	}

	file->SetFileName(file_name);

	// open file
	if (open && !file->Open(write)) {
		LogError1("Error opening audio file: %s", file_name.c_str());
		delete file;
		return NULL;
	}

	return file;
}


/**
	Mute all cateory volumes.
*/
void AudioServer::Mute()
{
	if (this->muted)
		return;

	this->muted = true;
	this->volume_dirty = true;
}


/**
	Restores all muted cateory volumes.
*/
void AudioServer::Unmute()
{
	if (!this->muted)
		return;

	this->muted = false;
	this->volume_dirty = true;
}


/**
	Sets whether sounds from category can be played.
*/
void AudioServer::SetCategoryAllowed(Sound::Category category, bool allowed)
{
	Assert(category >= 0 && category < Sound::SND_COUNT);
	if (this->category_allowed[category] == allowed)
		return;

	// set new value
	this->category_allowed[category] = allowed;

	// stop all sound in category
	if (!allowed) {
		Root *sounds_list = ResourceServer::GetInstance()->GetResourceList(Resource::RES_SOUND);

		for (Sound *snd = (Sound *)sounds_list->GetFront(); snd; snd = (Sound *)snd->GetNext()) {
			if (snd->GetCategory() == category)
				this->StopSound(snd);
		}
	}
}


/**
*/
bool AudioServer::DistanceModelToString(DistanceModel model, string &name)
{
	switch (model)
	{
	case DM_NONE:             name = "none";             return true;
	case DM_INVERSE:          name = "inverse";          return true;
	case DM_INVERSE_CLAMPED:  name = "inverseclamped";   return true;
	case DM_LINEAR:           name = "linear";           return true;
	case DM_LINEAR_CLAMPED:   name = "linearclamped";    return true;
	case DM_EXPONENT:         name = "exponent";         return true;
	case DM_EXPONENT_CLAMPED: name = "exponentclamped";  return true;
	default:
		LogWarning1("Invalid distance model: %d", model);
		return false;
	}
}


/**
*/
bool AudioServer::StringToDistanceModel(const string &name, DistanceModel &model)
{
	if (name == "none")                 model = DM_NONE;
	else if (name == "inverse")         model = DM_INVERSE;
	else if (name == "inverseclamped")  model = DM_INVERSE_CLAMPED;
	else if (name == "linear")          model = DM_LINEAR;
	else if (name == "linearclamped")   model = DM_LINEAR_CLAMPED;
	else if (name == "exponent")        model = DM_EXPONENT;
	else if (name == "exponentclamped") model = DM_EXPONENT_CLAMPED;
	else {
		LogWarning1("Invalid distance model: %s", name.c_str());
		return false;
	}

	return true;
}


// vim:ts=4:sw=4:
