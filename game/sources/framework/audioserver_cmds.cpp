//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file audioserver_cmds.cpp
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	Interface for script commands.

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/audioserver.h"


//=========================================================================
// Commands
//=========================================================================

static void s_SetDeviceName(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
    self->SetDeviceName(string(cmd->In()->GetS()));
}


static void s_GetDeviceName(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
	cmd->Out()->SetS(self->GetDeviceName().c_str());
}


/**
	Sets master volume (0.0 .. 1.0).
*/
static void s_SetMasterVolume(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
    self->SetMasterVolume(cmd->In()->GetF());
}


/**
	Gets master volume (0.0 .. 1.0).
*/
static void s_GetMasterVolume(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
    cmd->Out()->SetF(self->GetMasterVolume());
}


static void s_SetCategoryVolume(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;

	Sound::Category category;
	if (Sound::StringToCategory(string(cmd->In()->GetS()), category))
	{
		float volume = cmd->In()->GetF();
		self->SetCategoryVolume(category, volume);
	}
}


static void s_GetCategoryVolume(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;

	Sound::Category category;
	float volume = 0.0f;

	if (Sound::StringToCategory(string(cmd->In()->GetS()), category))
		volume = self->GetCategoryVolume(category);

	cmd->Out()->SetF(volume);
}


static void s_SetCategoryAllowed(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;

	Sound::Category category;
	if (Sound::StringToCategory(string(cmd->In()->GetS()), category))
	{
		bool allowed = cmd->In()->GetB();
		self->SetCategoryAllowed(category, allowed);
	}
}


static void s_GetCategoryAllowed(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;

	Sound::Category category;
	bool allowed = false;
	
	if (Sound::StringToCategory(string(cmd->In()->GetS()), category))
		allowed = self->GetCategoryAllowed(category);

    cmd->Out()->SetB(allowed);
}


static void s_SetDopplerFactor(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
    self->SetDopplerFactor(cmd->In()->GetF());
}


static void s_GetDopplerFactor(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
    cmd->Out()->SetF(self->GetDopplerFactor());
}


static void s_SetSpeedOfSound(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
    self->SetSpeedOfSound(cmd->In()->GetF());
}


static void s_GetSpeedOfSound(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
    cmd->Out()->SetF(self->GetSpeedOfSound());
}


static void s_Mute(void *slf, Cmd *cmd)
{
	AudioServer* self = (AudioServer *)slf;
	self->Mute();
}


static void s_Unmute(void *slf, Cmd *cmd)
{
	AudioServer* self = (AudioServer *)slf;
	self->Unmute();
}


static void s_IsMuted(void *slf, Cmd *cmd)
{
    AudioServer* self = (AudioServer *)slf;
    cmd->Out()->SetB(self->IsMuted());
}


/**
	Stops all playing sounds.
*/
static void s_StopAllSounds(void *slf, Cmd *cmd)
{
	AudioServer* self = (AudioServer *)slf;
	self->StopAllSounds();
}


void s_InitAudioServer_cmds(Class* cl)
{
    cl->BeginCmds();

	cl->AddCmd("v_SetDeviceName_s",       'SDVN', s_SetDeviceName);
	cl->AddCmd("s_GetDeviceName_v",       'GDVN', s_GetDeviceName);

    cl->AddCmd("v_SetMasterVolume_f",     'SMSV', s_SetMasterVolume);
    cl->AddCmd("f_GetMasterVolume_v" ,    'GMSV', s_GetMasterVolume);
	cl->AddCmd("v_SetCategoryVolume_sf",  'SCTV', s_SetCategoryVolume);
	cl->AddCmd("f_GetCategoryVolume_s",   'GCTV', s_GetCategoryVolume);
	cl->AddCmd("v_SetCategoryAllowed_sb", 'SCTA', s_SetCategoryAllowed);
	cl->AddCmd("b_GetCategoryAllowed_s",  'GCTA', s_GetCategoryAllowed);

	cl->AddCmd("v_SetDopplerFactor_f",    'SDPF', s_SetDopplerFactor);
    cl->AddCmd("f_GetDopplerFactor_v" ,   'GDPF', s_GetDopplerFactor);
	cl->AddCmd("v_SetSpeedOfSound_f",     'SSOS', s_SetSpeedOfSound);
    cl->AddCmd("f_GetSpeedOfSound_v" ,    'GSOS', s_GetSpeedOfSound);

	cl->AddCmd("v_Mute_v",                'MUTE', s_Mute);
	cl->AddCmd("v_Unmute_v",              'UMUT', s_Unmute);
	cl->AddCmd("b_IsMuted_v",             'IMUT', s_IsMuted);

	cl->AddCmd("v_StopAllSounds_v",       'SASN', s_StopAllSounds);

    cl->EndCmds();
}


// vim:ts=4:sw=4:
