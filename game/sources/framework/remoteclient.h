#ifndef __remoteclient_h__
#define __remoteclient_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file remoteclient.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/root.h"


//=========================================================================
// RemoteClient
//=========================================================================

/**
	@class RemoteClient
	@ingroup Framework_Module

	@brief RemoteClient.
*/

class RemoteClient : public Root
{
	friend class RemoteServer;

//--- methods
public:
	RemoteClient(const char *id);

	byte_t GetRemoteID() const;
	bool IsLocal() const;
	void SendRemoteMessage(BaseMessage *message, RemoteClient *destination, double delay = 0.0);


//--- variables
private:
	byte_t remote_id;
	bool local;
};


//=========================================================================
// Methods
//=========================================================================

inline
byte_t RemoteClient::GetRemoteID() const
{
	return this->remote_id;
}


inline
bool RemoteClient::IsLocal() const
{
	return this->local;
}


#endif  // __remoteclient_h__

// vim:ts=4:sw=4:
