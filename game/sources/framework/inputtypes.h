#ifndef __inputtypes_h__
#define	__inputtypes_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file inputtypes.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Defines
//=========================================================================

enum KeyCode
{
	// 0 - unused

	KEY_F1 = 1,
	KEY_F2,
	KEY_F3,
	KEY_F4,
	KEY_F5,
	KEY_F6,
	KEY_F7,
	KEY_F8,
	KEY_F9,
	KEY_F10,
	KEY_F11,
	KEY_F12,
	KEY_F13,
	KEY_F14,
	KEY_F15,
	KEY_F16,
	KEY_F17,
	KEY_F18,
	KEY_F19,
	KEY_F20,
	KEY_F21,
	KEY_F22,
	KEY_F23,
	KEY_F24,
	KEY_F25, // 25

	KEY_TAB, // 26
	KEY_BACKSPACE,
	KEY_ENTER,
	KEY_ESC,
	KEY_SPACE, // 30

	KEY_UP, // 31
	KEY_DOWN,
	KEY_LEFT,
	KEY_RIGHT, //34

	KEY_INSERT, // 35
	KEY_DEL,
	KEY_HOME,
	KEY_END,
	KEY_PAGEUP,
	KEY_PAGEDOWN, // 40

	KEY_LSHIFT, // 41
	KEY_RSHIFT,
	KEY_LCTRL,
	KEY_RCTRL,
	KEY_LALT,
	KEY_RALT, // 46

	// 47 - unused

	KEY_0 = '0', // 48
	KEY_1,
	KEY_2,
	KEY_3,
	KEY_4,
	KEY_5,
	KEY_6,
	KEY_7,
	KEY_8,
	KEY_9, // 57

	KEY_NP_DIVIDE, // 58
	KEY_NP_MULTIPLY,
	KEY_NP_SUBTRACT,
	KEY_NP_ADD,
	KEY_NP_DECIMAL,
	KEY_NP_EQUAL,
	KEY_NP_ENTER,  // 64

	KEY_A = 'A',  // 65
	KEY_B,
	KEY_C,
	KEY_D,
	KEY_E,
	KEY_F,
	KEY_G,
	KEY_H,
	KEY_I,
	KEY_J,
	KEY_K,
	KEY_L,
	KEY_M,
	KEY_N,
	KEY_O,
	KEY_P,
	KEY_Q,
	KEY_R,
	KEY_S,
	KEY_T,
	KEY_U,
	KEY_V,
	KEY_W,
	KEY_X,
	KEY_Y,
	KEY_Z, // 90

	KEY_LSQUOTE,   // `    91
	KEY_SQUOTE,    // '
	KEY_SUBTRACT,  // -
	KEY_EQUAL,     // =
	KEY_LBRACKET,  // [
	KEY_RBRACKET,  // ]
	KEY_SLASH,     // /
	KEY_BACKSLASH, // \ 
	KEY_COMMA,     // ,
	KEY_DOT,       // .
	KEY_SEMICOLON, // ;    101

	KEY_NP_0,  // 102
	KEY_NP_1,
	KEY_NP_2,
	KEY_NP_3,
	KEY_NP_4,
	KEY_NP_5,
	KEY_NP_6,
	KEY_NP_7,
	KEY_NP_8,
	KEY_NP_9,  // 111

	KEY_PRINT_SCREEN, // 112
	KEY_SCROLL_LOCK,
	KEY_PAUSE, // 114

	KEY_LWIN, // 115
	KEY_RWIN,
	KEY_MENU, // 117

	// mouse buttons
	MOUSE_BUTTON_LEFT, // 118
	MOUSE_BUTTON_RIGHT,
	MOUSE_BUTTON_MIDDLE,
	MOUSE_BUTTON_4,
	MOUSE_BUTTON_5,
	MOUSE_BUTTON_6,
	MOUSE_BUTTON_7,
	MOUSE_BUTTON_8,  // 125

	KEY_COUNT, // 126

	KEY_INVALID,
};


enum KeyState
{
	KS_UP,
	KS_DOWN,
	KS_PRESSED,
	KS_COUNT,
	KS_INVALID
};


enum SliderCode
{
	MOUSE_X,
	MOUSE_Y,
	MOUSE_WHEEL,
	MOUSE_RELX,
	MOUSE_RELY,
	MOUSE_RELWHEEL,
	SLIDER_COUNT,
	SLIDER_INVALID,
};


#endif  // __inputtypes_h__

// vim:ts=4:sw=4:
