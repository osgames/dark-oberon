//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionclientcontext_cmds.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2008

	@version 1.0 - Initial version.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/attributes.h"


//=========================================================================
// Commands
//=========================================================================

static void s_SetAttribute(void *slf, Cmd *cmd)
{
	Attributes *self = (Attributes *)slf;

	string name = cmd->In()->GetS();
	string value = cmd->In()->GetS();

	self->SetAttribute(name.c_str(), value.c_str());
}


static void s_GetAttribute(void *slf, Cmd *cmd)
{
	Attributes *self = (Attributes *)slf;
	cmd->Out()->SetS(self->GetAttribute(cmd->In()->GetS()));
}


void s_InitAttributes_cmds(Class *cl)
{
	cl->BeginCmds();
	cl->AddCmd("v_SetAttribute_ss",    'SATT', s_SetAttribute);
	cl->AddCmd("s_GetAttribute_s",     'GATT', s_GetAttribute);
	cl->EndCmds();
}


// vim:ts=4:sw=4:
