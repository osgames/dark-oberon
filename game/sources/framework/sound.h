#ifndef __sound_h__
#define __sound_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sound.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/matrix.h"
#include "kernel/vector.h"
#include "framework/resource.h"

class SoundResource;


//=========================================================================
// Sound
//=========================================================================

/**
	@class Sound
	@ingroup Framework_Module

	Hold parameters for a sound instance. Usually a "game object" holds
	one or more Sound objects for all the sounds it has to play.
	Although Sound is derived from Resource, Sound objects should
	never be shared (this is enforced by the factory method
	AudioServer::NewSound()).

	When opened, a Sound object will create a shared static or streaming
	sound resource, based on the settings of Sound.
*/

class Sound : public Resource
{
	friend class AudioServer;

//--- embeded
public:
	/// sound categories.
	enum Category
	{
		SND_EFFECT,
		SND_MUSIC,
		SND_SPEECH,
		SND_AMBIENT,
		SND_COUNT
	};

//-- variables
protected:
	Category category;
	matrix44 transform;
	vector3 velocity;

	bool streaming;
	bool looping;
	int priority;
	float volume;              ///< Indicate the gain (volume amplification) applied. [> 0.0]
	float min_volume;          ///< Indicate minimum source attenuation. [0.0 - 1.0]
	float max_volume;          ///< Indicate maximum source attenuation. [0.0 - 1.0]
	float ref_distance;        ///< Source specific reference distance. [> 0.0] At 0.0, no distance attenuation occurs.
	float max_distance;        ///< Indicate distance above which sources are not attenuated using the inverse clamped distance model. [> 0.0]
	float rolloff_factor;      ///< Source specific rolloff factor. [>0.0]
	float pitch;               ///< Specify the pitch to be applied at source. [0.5 - 2.0]
	float inner_cone_angle;    ///< Directional source, inner cone angle, in degrees. [0-360]
	float outer_cone_angle;    ///< Directional source, outer cone angle, in degrees. [0-360]
	float outer_cone_volume;   ///< Directional source, outer cone gain. [0.0 - 1.0]

	bool volume_dirty;
	bool move_dirty;
	bool props_dirty;

	Ref<SoundResource> sound_resource;

//-- methods
public:
	Sound(const char *id);
	virtual ~Sound();

	void SetCategory(Category category);
	void SetPriority(int pri);
	void SetStreaming(bool b);
	void SetLooping(bool b);
	void SetVolume(float v);
	void SetMinVolume(float v);
	void SetMaxVolume(float v);

	void SetTransform(const matrix44& m);
	void SetVelocity(const vector3& v);
	void SetRefDistance(float d);
	void SetMaxDistance(float d);
	void SetInnerConeAngle(float a);
	void SetOuterConeAngle(float a);
	void SetOuterConeVolume(float v);
	void SetRolloffFactor(float v);
	void SetPitch(float v);

	Category GetCategory() const;
	int   GetPriority() const;
	bool  IsStreaming() const;
	bool  IsLooping() const;
	float GetVolume() const;
	float GetMinVolume() const;
	float GetMaxVolume() const;

	const matrix44& GetTransform() const;
	const vector3& GetVelocity() const;
	float GetRefDistance() const;
	float GetMaxDistance() const;
	float GetInnerConeAngle() const;
	float GetOuterConeAngle() const;
	float GetOuterConeVolume() const;
	float GetRolloffFactor() const;
	float GetPitch() const;

	void CopySoundAttrsFrom(const Sound* other);

	static bool CategoryToString(Category category, string &name);
	static bool StringToCategory(const string &name, Category &category);

	virtual bool IsPlaying();

protected:
	virtual void Play();
	virtual void Stop();
	virtual void Pause();
	virtual void PlayPause();
	virtual void Update();
	virtual void UpdateStream();
};


//=========================================================================
// Methods
//=========================================================================

/**
	Sets static/streaming type.
*/
inline
void Sound::SetStreaming(bool b)
{
	Assert(!this->loaded);
	this->streaming = b;
}


/**
	Gets static/streaming type.
*/
inline
bool Sound::IsStreaming() const
{
	return this->streaming;
}


/**
	Sets the looping behaviour.
*/
inline
void Sound::SetLooping(bool b)
{
	this->looping = b;
	this->props_dirty = true;
}


/**
	Gets the looping behaviour.
*/
inline
bool Sound::IsLooping() const
{
	return this->looping;
}


/**
	Sets the sound priority for voice management.
*/
inline
void Sound::SetPriority(int p)
{
	this->priority = p;
	this->props_dirty = true;
}


/**
	Gets the sound priority for voice management.
*/
inline
int Sound::GetPriority() const
{
	return this->priority;
}


/**
	Sets the playback volume.

	Indicate the gain (volume amplification) applied. [> 0.0]
	A value of 1.0 means un-attenuated/unchanged.
	Each division by 2 equals an attenuation of -6dB.
	Each multiplicaton with 2 equals an amplification of +6dB.

	A value of 0.0 is meaningless with respect to a logarithmic scale;
	it is interpreted as zero volume - the channel is effectively disabled.
*/
inline
void Sound::SetVolume(float v)
{
	this->volume = v;
	this->volume_dirty = true;
}


/**
	Gets the playback volume.
*/
inline
float Sound::GetVolume() const
{
	return this->volume;
}


inline
void Sound::SetMinVolume(float v)
{
	this->min_volume = v;
	this->props_dirty = true;
}


inline
float Sound::GetMinVolume() const
{
	return this->min_volume;
}


inline
void Sound::SetMaxVolume(float v)
{
	this->max_volume = v;
	this->props_dirty = true;
}


inline
float Sound::GetMaxVolume() const
{
	return this->max_volume;
}


/**
	Sets world transform.
*/
inline
void Sound::SetTransform(const matrix44& m)
{
	this->transform = m;
	this->move_dirty = true;
}


/**
	Gets world transform.
*/
inline
const matrix44& Sound::GetTransform() const
{
	return this->transform;
}


/**
	Sets the velocity.
*/
inline
void Sound::SetVelocity(const vector3& v)
{
	this->velocity = v;
	this->move_dirty = true;
}


/**
	Gets the velocity.
*/
inline
const vector3& Sound::GetVelocity() const
{
	return this->velocity;
}


/**
	Sets the minimum distance (sound doesn't get louder when closer).
*/
inline
void Sound::SetRefDistance(float d)
{
	this->ref_distance = d;
	this->props_dirty = true;
}


/**
	Gets the mimimum distance.
*/
inline
float Sound::GetRefDistance() const
{
	return this->ref_distance;
}


/**
	Sets the maximum distance (sound will not be audible beyond that distance.
*/
inline
void Sound::SetMaxDistance(float d)
{
	this->max_distance = d;
	this->props_dirty = true;
}


/**
	Gets the maximum distance.
*/
inline
float Sound::GetMaxDistance() const
{
	return this->max_distance;
}


/**
	Sets the inside cone angle in degrees.
*/
inline
void Sound::SetInnerConeAngle(float a)
{
	this->inner_cone_angle = a;
	this->props_dirty = true;
}


/**
	Gets the inside cone angle.
*/
inline
float Sound::GetInnerConeAngle() const
{
	return this->inner_cone_angle;
}


/**
	Sets the outside cone angle in degrees.
*/
inline
void Sound::SetOuterConeAngle(float a)
{
	this->outer_cone_angle = a;
	this->props_dirty = true;
}


/**
	Gets the outside cone angle.
*/
inline
float Sound::GetOuterConeAngle() const
{
	return this->outer_cone_angle;
}


/**
	Sets the cone outside volume (0.0 .. 1.0).
*/
inline
void Sound::SetOuterConeVolume(float v)
{
	this->outer_cone_volume = v;
	this->props_dirty = true;
}


/**
	Gets the cone outside volume.
*/
inline
float Sound::GetOuterConeVolume() const
{
	return this->outer_cone_volume;
}


/**
	Sets category of sound.
*/
inline
void Sound::SetCategory(Category c)
{
	Assert(c >= 0 && c < SND_COUNT);
	this->category = c;
}


/**
	Gets category of sound.
*/
inline
Sound::Category Sound::GetCategory() const
{
	return this->category;
}


/**
	Sets the rolloff factor.
*/
inline
void Sound::SetRolloffFactor(float v)
{
	this->rolloff_factor = v;
	this->props_dirty = true;
}


/**
	Gets the rolloff factor.
*/
inline
float Sound::GetRolloffFactor() const
{
	return this->rolloff_factor;
}


/**
	Sets the rolloff factor.
*/
inline
void Sound::SetPitch(float v)
{
	this->pitch = v;
	this->props_dirty = true;
}


/**
	Gets the rolloff factor.
*/
inline
float Sound::GetPitch() const
{
	return this->pitch;
}


#endif // __sound_h__

// vim:ts=4:sw=4:
