#ifndef __remoteserver_h__
#define __remoteserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file remoteserver.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/rootserver.h"
#include "kernel/ref.h"

class MessageServer;
class RemoteClient;


//=========================================================================
// RemoteServer
//=========================================================================

/**
	@class RemoteServer
	@ingroup Framework_Module

	@brief RemoteServer.
*/

class RemoteServer : public RootServer<RemoteServer>
{
//--- embeded
private:
	typedef hash_map<byte_t, RemoteClient *> ClientsMap;


//--- methods
public:
	RemoteServer(const char *id);
	virtual ~RemoteServer();

	bool RegisterClient(RemoteClient *client);
	bool DeregisterClient(RemoteClient *client);
	RemoteClient *GetClient(byte_t remote_id) const;

	void SendRemoteMessage(BaseMessage *message, RemoteClient *sender, RemoteClient *desination, double delay = 0.0);

protected:
	virtual bool CreateRemoteClient(byte_t client_id);


//--- variables
private:
	bool master;
	byte_t max_clients;
	ClientsMap clients;

	Ref<MessageServer> message_server;
};


inline
RemoteClient *RemoteServer::GetClient(byte_t remote_id) const
{
	Assert(remote_id);
	ClientsMap::const_iterator itClient = this->clients.find(remote_id);
	return itClient == this->clients.end() ? NULL : itClient->second;
}


#endif  // __remoteserver_h__

// vim:ts=4:sw=4:
