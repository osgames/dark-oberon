//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file scene.cpp
	@ingroup Framework_Module

	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Scene doesn't release objects.
	@version 1.2 - Non-visible scene objects are not rendered.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/scene.h"
#include "framework/gfxserver.h"
#include "kernel/kernelserver.h"
#include "kernel/timeserver.h"

PrepareClass(Scene, "Root", s_NewScene, s_InitScene);

#ifdef USE_PROFILERS
Ref<Profiler> Scene::sorting_profiler;
Ref<Env> Scene::objects_variable;
ulong_t Scene::last_frame = 0;
#endif


//=========================================================================
// Scene
//=========================================================================

/**
	Constructor.
*/
Scene::Scene(const char *id) :
	Root(id),
	objects_count(0),
	sorting(true)
{
	this->kernel_server->PushCwd(this);
	this->objects = this->kernel_server->New("Root", string("objects"));
	this->kernel_server->PopCwd();

#ifdef USE_PROFILERS
	// create profilers
	Assert(TimeServer::GetInstance());
	if (!this->sorting_profiler.IsValid())
		this->sorting_profiler = TimeServer::GetInstance()->NewProfiler(string("% sorting"));

	// create system variable for objects count
	if (!this->objects_variable.IsValid())
		this->objects_variable = (Env *)kernel_server->New("Env", NOH_VARIABLES + "/# objects");

	this->objects_variable->SetI(0);
#endif
}


/**
	Destructor.
*/
Scene::~Scene()
{
	// don't release scene object
	this->RemoveAllObjects();
}


void Scene::AddObject(SceneObject *object)
{
	Assert(object);

	this->Lock();

	// object is assigned to some scene
	if (object->GetParent())
	{
		// object is already assigned to this scene
		if (object->GetParent() == this->objects)
		{
			this->Unlock();
			return;
		}

		// remove object from another scene
		else
			((Scene *)object->GetParent())->RemoveObject(object);
	}

	// add to list
	this->objects->PushFront(object);
	this->objects_count++;

	this->Unlock();
}


void Scene::RemoveObject(SceneObject *object)
{
	Assert(object);

	this->Lock();

	// check if scene object belongs to this scene
	if (object->GetParent() != this->objects)
	{
		this->Unlock();
		return;
	}

	// remove from list
	object->Remove();
	this->objects_count--;

	this->Unlock();
}


void Scene::CalculatePosition(const vector3 &object_position, vector3 &proj_position)
{
	proj_position = object_position;
}


/**
*/
void Scene::Render()
{
	this->Lock();

	// sort objects
	if (this->sorting)
		this->SortObjects();

	#ifdef USE_PROFILERS
	// reset objects counter with new frame
	if (TimeServer::GetInstance()->GetFrame() > this->last_frame)
	{
		this->objects_variable->SetI(0);
		this->last_frame = TimeServer::GetInstance()->GetFrame();
	}
	#endif

	// render all objects in order from back to front
	SceneObject *object = (SceneObject *)this->objects->GetBack();
	vector3 position;
	bool first = true;

	for (; object; object = (SceneObject *)object->GetPrev())
	{
		if (object->IsVisible())
		{
			this->RenderObject(object, first, true);
			first = false;
		}
	}

	this->Unlock();
}


void Scene::RenderObject(SceneObject *object, bool set_projection, bool all_features)
{
	Assert(object);
	Assert(object->IsVisible());

	// set projection and viewport
	GfxServer *gs = GfxServer::GetInstance();
	Assert(gs);

	if (set_projection)
	{
		gs->SetProjection(this->projection);
		gs->SetViewport(this->viewport);
	}

	// transfer object position to position in rendering frustum
	vector3 position;
	this->CalculatePosition(object->GetPosition(), position);

	// set object position
	gs->ResetMatrix();
	gs->Translate(position);

	// update animations
	if (all_features)
	{
		object->UpdateAnimators();

		#ifdef USE_PROFILERS
		this->objects_variable->SetI(this->objects_variable->GetI() + 1);
		#endif
	}

	// render object
	object->Render(all_features);
}


void Scene::SortObjects()
{
	//
}


// vim:ts=4:sw=4:
