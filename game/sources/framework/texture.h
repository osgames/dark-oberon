#ifndef __texture_h__
#define __texture_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file texture.h
	@ingroup Framework_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "kernel/vector.h"
#include "framework/resource.h"


//=========================================================================
// Texture
//=========================================================================

/**
	@class Texture
	@ingroup Framework_Module
*/
    
class Texture : public Resource
{
//--- variables
protected:
	// overlay image
	string  overlay_file_name;
	vector3 overlay_color;

	// properties
	ushort_t original_width;
	ushort_t original_height;
	ushort_t width;
	ushort_t height;
	bool mipmap;
	bool allow_mipmap;

//--- methods
public:
	Texture(const char *id);
	virtual ~Texture();

	void SetOverlayFile(const string& path);
	void SetOverlayColor(const vector3& color);

	ushort_t GetOriginalWidth();
	ushort_t GetOriginalHeight();
	ushort_t GetWidth();
	ushort_t GetHeight();

	void AllowMipMap(bool allow);
	bool IsMipMap() const;

	virtual void Apply();
	virtual void ReplaceData(byte_t *data);
};


//=========================================================================
// Methods
//=========================================================================


inline
void Texture::SetOverlayFile(const string& path)
{
	this->overlay_file_name = path;
}


inline
void Texture::SetOverlayColor(const vector3& color)
{
	this->overlay_color = color;
}


inline
ushort_t Texture::GetOriginalWidth()
{
	return this->original_width;
}


inline
ushort_t Texture::GetOriginalHeight()
{
	return this->original_height;
}


inline
ushort_t Texture::GetWidth()
{
	return this->width;
}


inline
ushort_t Texture::GetHeight()
{
	return this->height;
}


inline
void Texture::AllowMipMap(bool allow)
{
	this->allow_mipmap = allow;
}


inline
bool Texture::IsMipMap() const
{
	return this->mipmap;
}


#endif // __texture_h__

// vim:ts=4:sw=4:
