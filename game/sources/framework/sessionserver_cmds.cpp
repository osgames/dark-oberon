//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file sessionserver_cmds.cpp
	@ingroup Framework_Module

	@author RadonLabs GmbH, Peter Knut
	@date 2003, 2008

	@version 1.0 - Copy from Nebula Device.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/sessionserver.h"


//=========================================================================
// Commands
//=========================================================================

static void s_SetAppName(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	self->SetAppName(cmd->In()->GetS());
}


static void s_GetAppName(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	cmd->Out()->SetS(self->GetAppName());
}


static void s_SetAppVersion(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	self->SetAppVersion(cmd->In()->GetS());
}


static void s_GetAppVersion(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	cmd->Out()->SetS(self->GetAppVersion());
}


static void s_SetMaxClientsCount(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	self->SetMaxClientsCount(cmd->In()->GetI());
}


static void s_GetMaxClientsCount(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	cmd->Out()->SetI(self->GetMaxClientsCount());
}


static void s_Open(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	cmd->Out()->SetB(self->Open());
}


static void s_Close(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	self->Close();
}


static void s_IsOpened(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	cmd->Out()->SetB(self->IsOpened());
}


static void s_KickClient(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	cmd->Out()->SetB(self->KickClient(cmd->In()->GetS()));
}


static void s_Start(void *slf, Cmd *cmd)
{
	SessionServer *self = (SessionServer*) slf;
	cmd->Out()->SetB(self->Start());
}


void s_InitSessionServer_cmds(Class *cl)
{
	cl->BeginCmds();

	cl->AddCmd("v_SetAppName_s",               'SAPN', s_SetAppName);
	cl->AddCmd("s_GetAppName_v",               'GAPN', s_GetAppName);
	cl->AddCmd("v_SetAppVersios_s",            'SAPV', s_SetAppVersion);
	cl->AddCmd("s_GetAppVersios_v",            'GAPV', s_GetAppVersion);
	cl->AddCmd("v_SetMaxClientsCount_i",       'SMNC', s_SetMaxClientsCount);
	cl->AddCmd("i_GetMaxClientsCount_v",       'GMNC', s_GetMaxClientsCount);
	cl->AddCmd("b_Open_v",                     'OPEN', s_Open);
	cl->AddCmd("v_Close_v",                    'CLOS', s_Close);
	cl->AddCmd("b_IsOpened_v",                 'ISOP', s_IsOpened);
	cl->AddCmd("b_KickClient_s",               'KCKC', s_KickClient);
	cl->AddCmd("b_Start_v",                    'STRT', s_Start);

	cl->EndCmds();
}


// vim:ts=4:sw=4:
