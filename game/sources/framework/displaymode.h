#ifndef __displaymode_h__
#define __displaymode_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file displaymode.h
	@ingroup Framework_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2002, 2005, 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "framework/system.h"
#include "framework/gfxstructures.h"


//=========================================================================
// DisplayMode
//=========================================================================

/**
	@class DisplayMode
	@ingroup Framework_Module

	@brief Contains display mode parameters.
*/

class DisplayMode
{
//--- embeded
public:
	struct WidowPosition
	{
		short left;
		short top;

		WidowPosition() : left(0), top(0) {}
		WidowPosition(short l, short t) : left(l), top(t) {}
		void Set(short l, short t) { left = l; top = t; }
	};

	struct WidowSize
	{
		ushort_t width;
		ushort_t height;

		WidowSize() : width(0), height(0) {}
		WidowSize(ushort_t w, ushort_t h) : width(w), height(h) {}
		void Set(ushort_t w, ushort_t h) { width = w; height = h; }
	};


//--- variables
private:
	bool fullscreen;

	WidowPosition wnd_position;
	WidowSize wnd_size;
	WidowSize full_size;

	VideoMode::Bpp bpp;

	bool vertical_sync;
	byte_t refresh_rate;


//--- methods
public:
	DisplayMode();

	void SetWindowPosition(const DisplayMode::WidowPosition &position);
	void SetWindowSize(const DisplayMode::WidowSize &size);
	void SetFullscreenSize(const DisplayMode::WidowSize &size);
	void SetModeSize(const DisplayMode::WidowSize &size);
	void SetFullscreen(bool fullscreen);
	void SetBpp(VideoMode::Bpp depth);
	void SetVerticalSync(bool vsync);
	void SetRefreshRate(byte_t rrate);
	void ToggleFullscreen();

	const WidowPosition &GetWindowPosion() const;
	const WidowSize &GetWindowSize() const;
	const WidowSize &GetFullscreenSize() const;
	const WidowSize &GetModeSize() const;
	bool IsFullscreen() const;
	VideoMode::Bpp GetBpp() const;
	bool IsVerticalSync() const;
	byte_t GetRefreshRate() const;

	bool TestBpp(VideoMode::Bpp bpp);
};


/**
*/
inline
DisplayMode::DisplayMode() :
	fullscreen(false),
	bpp(VideoMode::BPP_24),
	vertical_sync(true),
	refresh_rate(0)       // 0 means system defaut
{
    this->wnd_size.Set(800, 600);
	this->full_size.Set(1024, 768);
}


/**
*/
inline
void DisplayMode::SetWindowPosition(const DisplayMode::WidowPosition &position)
{
	this->wnd_position = position;
}


/**
*/
inline
const DisplayMode::WidowPosition &DisplayMode::GetWindowPosion() const
{
	return this->wnd_position;
}


/**
*/
inline
void DisplayMode::SetWindowSize(const DisplayMode::WidowSize &size)
{
	this->wnd_size = size;
}


/**
*/
inline
const DisplayMode::WidowSize &DisplayMode::GetWindowSize() const
{
	return this->wnd_size;
}


/**
*/
inline
void DisplayMode::SetFullscreenSize(const DisplayMode::WidowSize &size)
{
	this->full_size = size;
}


/**
*/
inline
void DisplayMode::SetModeSize(const DisplayMode::WidowSize &size)
{
	if (this->fullscreen)
		this->full_size = size;
	else
		this->wnd_size = size;
}


/**
*/
inline
const DisplayMode::WidowSize &DisplayMode::GetFullscreenSize() const
{
	return this->full_size;
}


/**
*/
inline
const DisplayMode::WidowSize &DisplayMode::GetModeSize() const
{
	return this->fullscreen ? this->full_size : this->wnd_size;
}


/**
*/
inline
void DisplayMode::SetFullscreen(bool fullscreen)
{
	this->fullscreen = fullscreen;
}


/**
*/
inline
void DisplayMode::ToggleFullscreen()
{
	this->fullscreen = !this->fullscreen;
}


/**
*/
inline
bool DisplayMode::IsFullscreen() const
{
	return this->fullscreen;
}


/**
*/
inline
void DisplayMode::SetVerticalSync(bool vsync)
{
	this->vertical_sync = vsync;
}


/**
*/
inline
bool DisplayMode::IsVerticalSync() const
{
	return this->vertical_sync;
}


/**
*/
inline
void DisplayMode::SetBpp(VideoMode::Bpp depth)
{
	this->bpp = depth;
}


/**
*/
inline
VideoMode::Bpp DisplayMode::GetBpp() const
{
	return this->bpp;
}


/**
*/
inline
bool DisplayMode::TestBpp(VideoMode::Bpp bpp)
{
	return bpp == this->bpp;
}


/**
*/
inline
void DisplayMode::SetRefreshRate(byte_t rrate)
{
	this->refresh_rate = rrate;
}


/**
*/
inline
byte_t DisplayMode::GetRefreshRate() const
{
	return this->refresh_rate;
}


#endif  // __displaymode_h__

// vim:ts=4:sw=4:
