#ifndef __freetypefontfile_h__
#define __freetypefontfile_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file freetypefontfile.h
	@ingroup Formats_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes 
//=========================================================================

#include "framework/system.h"
#include "framework/fontfile.h"

#include <freetype/ft2build.h>
#include FT_FREETYPE_H


//=========================================================================
// FreeTypeFontFile
//=========================================================================

/**
	@class FreeTypeFontFile
	@ingroup Formats_Module

	Read access to many font formats.

	It supports both bitmap and scalable formats, including TrueType, OpenType, 
	Type1, CID, CFF, Windows FON/FNT, X11 PCF, and others. 
	Uses high-speed anti-aliased glyph bitmap generation with 256 gray levels.
*/

class FreeTypeFontFile : public FontFile {
//--- variables
protected:
	FT_Library library;      ///< FreeType library.
	FT_Face face;            ///< FreeType font face.
	FT_Byte *buffer;         ///< Buffer for image data.


//--- methods
public:
	FreeTypeFontFile();
	virtual ~FreeTypeFontFile();

	virtual bool Open(bool write = false);
	virtual void Close();
	virtual ulong_t LoadData();
	virtual byte_t *GetData(ulong_t char_code, bool close = false);
};


#endif // __freetypefontfile_h__

// vim:ts=4:sw=4:
