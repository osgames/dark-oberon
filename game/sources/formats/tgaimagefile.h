#ifndef __tgaimagefile_h__
#define __tgaimagefile_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file tgaimagefile.h
	@ingroup Formats_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes 
//=========================================================================

#include "framework/system.h"
#include "framework/imagefile.h"
#include "libtga/tga.h"


//=========================================================================
// TgaImageFile
//=========================================================================

/**
	@class TgaImageFile
	@ingroup Formats_Module

	Provides read / write access to TGA image file.

	Internal LibTGA library is used for decoding TGA files.
*/

class TgaImageFile : public ImageFile
{
//--- methods
public:
	TgaImageFile();
	virtual ~TgaImageFile();

	virtual ulong_t LoadData();
	virtual bool SaveData();
};


#endif // __tgaimagefile_h__

// vim:ts=4:sw=4:
