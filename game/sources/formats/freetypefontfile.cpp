//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file freetypefontfile.cpp
	@ingroup Formats_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "formats/freetypefontfile.h"
#include "kernel/fileserver.h"
#include "kernel/logserver.h"


//=========================================================================
// FreeTypeFontFile
//=========================================================================

/**
	Constructor.
*/
FreeTypeFontFile::FreeTypeFontFile() :
	FontFile(),
	library(NULL),
	face(NULL),
	buffer(NULL)
{
	//
}


/**
	Destructor. Closes opened file.
*/
FreeTypeFontFile::~FreeTypeFontFile()
{
	if (this->opened)
		this->Close();
}


/**
	Opens FreeType font file.
	
	Creates File object for access to file in filesystem and initialize FreeType library.

	@param write Whether file will be opened for writing.
	@return @c True if successful.
*/
bool FreeTypeFontFile::Open(bool write)
{
	// writing is not allowed
	if (write) {
		LogError("Writing of font files is not supported");
		return false;
	}

	// open data file
	if (!DataFile::Open(write)) {
		LogError1("Error opening font file: %s", this->file_name.c_str());
		return false;
	}

	// file is still not opened
	this->opened = false;

	// init FreeType library
	if (FT_Init_FreeType(&this->library)) {
		LogError("Error initialising FreeType library");
		SafeDelete(this->file);
		return false;
	}

	// all ok
	return this->opened = true;
}


/**
	Closes FreeType font file.
	Cleares FreeType structures, deletes data and File object. File has to be opened before.
*/
void FreeTypeFontFile::Close()
{
	Assert(this->opened);

	// clear FreeType structures
	if (this->face)
		FT_Done_Face(this->face);
	if (this->library)
		FT_Done_FreeType(this->library);

	// delete buffer
	SafeDeleteArray(this->buffer);

	// close font file
	FontFile::Close();
}


/**
	Loads font data from opened file.

	Whole file is loaded into memory buffer first.

	@return Size of loaded data.
*/
ulong_t FreeTypeFontFile::LoadData()
{
	Assert(this->opened);
	Assert(this->state == LS_UNLOADED);

	// read whole file into memory buffer
	this->buffer = (FT_Byte *)this->file->ReadAll();
	if (!this->buffer) {
		LogError1("Error reading data from: %s", this->file_name.c_str());
		return 0;
	}
	
	// load font from buffer
	if (FT_New_Memory_Face(this->library, this->buffer, this->file->GetSize(), 0, &this->face)) {
		LogError1("Error loading font file: %s", this->file_name.c_str());
		SafeDeleteArray(this->buffer);
		return 0;
	}

	// prepare font
	if (FT_Set_Char_Size(
			this->face,
			FT_F26Dot6(this->char_size.width) * 64, FT_F26Dot6(this->char_size.height) * 64,
			this->char_size.resolution_x, this->char_size.resolution_y)
	) {
		LogError1("Error setting size for font: %s", this->file_name.c_str());
		SafeDeleteArray(this->buffer);
		return 0;
	}

	// set glyphs count
	this->glyphs_count = this->face->num_glyphs;
	this->data_size = 0;

	// set state to loaded
	state = LS_LOADED;
	return this->file->GetSize();
}


/**
	Returns bitmap of one character.

	@param char_code Code of wanted character.
	@param close  Whether file will be closed after returning the data.

	@return Character bitmap.
*/
byte_t *FreeTypeFontFile::GetData(ulong_t char_code, bool close)
{
	// file has to be loaded
	if (!this->TestState(LS_LOADED))
		return NULL;

	// delete previous character data
	SafeDeleteArray(this->data);
	ulong_t glyph_data_size = 0;

	// load chracter bitmap
	if (FT_Load_Char(this->face, char_code, FT_LOAD_RENDER)) {
		LogError2("Error loading character '%u' from font: %s", char_code, this->file_name.c_str());
		return NULL;
	}

	FT_GlyphSlot slot = this->face->glyph;

	// fill glyph data
	this->glyph_data.left = slot->bitmap_left;
	this->glyph_data.top = slot->bitmap_top;
	this->glyph_data.width = slot->bitmap.width;
	this->glyph_data.height = slot->bitmap.rows;
	this->glyph_data.advance_x = slot->advance.x >> 6;
	this->glyph_data.advance_y = slot->advance.y >> 6;

	// copy new data
	glyph_data_size = this->glyph_data.width * this->glyph_data.height;
	this->data = NEW byte_t[glyph_data_size];
	Assert(this->data);
	memcpy(this->data, slot->bitmap.buffer, glyph_data_size);

	// return data
	byte_t *result = this->data;
	this->data_size += glyph_data_size;

	// close file, but don't delete loaded data
	if (close) {
		this->data = NULL;
		this->Close();
	}

	return result;
}


// vim:ts=4:sw=4:
