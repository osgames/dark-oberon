//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file tgaimagefile.cpp
	@ingroup Formats_Module

 	@author Peter Knut
	@date 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes 
//=========================================================================

#include "formats/tgaimagefile.h"
#include "kernel/fileserver.h"
#include "kernel/logserver.h"


//=========================================================================
// TgaImageFile
//=========================================================================

/**
	Constructor.
*/
TgaImageFile::TgaImageFile() :
	ImageFile()
{
	// empty
}


/**
	Destructor. Closes opened file.
*/
TgaImageFile::~TgaImageFile()
{
	if (this->opened)
		this->Close();
}


/**
	Loads image data from opened TGA file.

	Custom callback functions are used for accessing the file.

	@return Size of loaded data.
*/
ulong_t TgaImageFile::LoadData()
{
	Assert(this->opened);
	Assert(this->state == LS_UNLOADED);

	TGA tga;

	// set default callbacks
	tgaPrepare(&tga);

	// set new callback functions for file access
	tga.cb.read_func  = File::OnRead;
	tga.cb.write_func = File::OnWrite;
	tga.cb.seek_func  = File::OnSeek;
	tga.cb.tell_func  = File::OnTell;

	// read tga image data
	if (!tgaRead(this->file, &tga, 0)) {
		LogError1("Error reading TGA image file: %s", this->file_name.c_str());
		return false;
	}

	// set image format
	switch (tga.format) {
	case TGA_PIXFMT_GRAY:   this->format = IF_LUMINANCE; break;
	case TGA_PIXFMT_RGB:    this->format = IF_RGB;       break;
	case TGA_PIXFMT_RGBA:   this->format = IF_RGBA;      break;
	default:
		LogError1("Unknown TGA format of image file: %s", this->file_name.c_str());
		return false;
	}

	// set all image properties
	this->width = tga.width;
	this->height = tga.height;
	this->data = tga.data;
	this->data_size = tga.data_size;
	this->use_free = true;

	// set state to loaded
	this->state = LS_LOADED;
	return this->data_size;
}


/**
	Saves data into the file. Data has to be loaded before.

	Custom callback functions are used for accessing the file.

	@return @c True if successful.
*/
bool TgaImageFile::SaveData()
{
	Assert(this->opened);
	Assert(this->TestState(LS_LOADED));

	TGA tga;

	// set default callbacks
	tgaPrepare(&tga);

	// set new callback functions for file access
	tga.cb.read_func  = File::OnRead;
	tga.cb.write_func = File::OnWrite;
	tga.cb.seek_func  = File::OnSeek;
	tga.cb.tell_func  = File::OnTell;

	// fill tga format
	switch (this->format) {
	case IF_LUMINANCE: tga.format = TGA_PIXFMT_GRAY; break;
	case IF_RGB:       tga.format = TGA_PIXFMT_RGB;  break;
	case IF_RGBA:      tga.format = TGA_PIXFMT_RGBA; break;
	default:
		LogError1("Unknown TGA format of image file: %s", this->file_name.c_str());
		return false;
	}

	// fill tga properties
	tga.bpp = this->GetBytesPerPixel(this->format);
	tga.data = this->data;
	tga.data_size = this->data_size;
	tga.width = this->width;
	tga.height = this->height;

	// write tga image data
	if (!tgaWrite(this->file, &tga, 0)) {
		LogError1("Error writing TGA image file: %s", this->file_name.c_str());
		return false;
	}

	return true;
}


// vim:ts=4:sw=4:
