#ifndef __oggaudiofile_h__
#define __oggaudiofile_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file oggaudiofile.h
	@ingroup Formats_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005 - 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes 
//=========================================================================

#include "framework/system.h"
#include "framework/audiofile.h"

#include <vorbis/vorbisfile.h>


//=========================================================================
// OggAudioFile
//=========================================================================

/**
	@class OggAudioFile
	@ingroup Formats_Module

	Provides read access to vorbis OGG image file.

	Saving is not supported.
	External OGGVorbis library is used for decoding OGG files.
*/

class OggAudioFile : public AudioFile {
//--- variables
protected:
	OggVorbis_File ogg_file;         ///< Structure used for decoding OGG files.


//--- methods
public:
	OggAudioFile();
	virtual ~OggAudioFile();

	virtual bool Open(bool write = false);
	virtual void Close();
	virtual ulong_t LoadData();
	virtual ulong_t LoadBlock(ulong_t size, bool looping);
	virtual ulong_t LoadTimeBlock(double time, bool looping);
	virtual void Rewind();

protected:
	bool CheckOVError(const long &);
};


#endif // __oggaudiofile_h__

// vim:ts=4:sw=4:
