
//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file oggaudiofile.cpp
	@ingroup Formats_Module

 	@author RadonLabs GmbH, Peter Knut
	@date 2005 - 2006

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "formats/oggaudiofile.h"
#include "kernel/fileserver.h"
#include "kernel/logserver.h"


//=========================================================================
// OggAudioFile
//=========================================================================

/**
	Constructor.
*/
OggAudioFile::OggAudioFile() :
	AudioFile()
{
	//
}


/**
	Destructor. Closes opened file.
*/
OggAudioFile::~OggAudioFile()
{
	if (this->opened)
		this->Close();
}


/**
	Opens OGG audio file.
	
	Creates File object for access to file in filesystem. Then opens file using Vorbis codec and read its header.

	@param write Whether file will be opened for writing.
	@return @c True if successful.
*/
bool OggAudioFile::Open(bool write)
{
	// writing is not allowed
	if (write) {
		LogError("Writing of OGG files is not supported");
		return false;
	}

	// open data file
	if (!DataFile::Open(write)) {
		LogError1("Error opening OGG audio file: %s", this->file_name.c_str());
		return false;
	}

	// file is still not opened
	this->opened = false;

	// callback functions for file access
	ov_callbacks cb;
	cb.close_func = File::OnClose;
	cb.read_func  = File::OnRead;
	cb.seek_func  = File::OnSeek64;
	cb.tell_func  = File::OnTell;

	if (this->CheckOVError(ov_open_callbacks(this->file, &this->ogg_file, 0, -1, cb))) {
		SafeDelete(this->file);
		return false;
	}

	// read format
	vorbis_info *ogg_info = ov_info(&this->ogg_file, -1);
	if (!ogg_info) {
		SafeDelete(this->file);
		return false;
	}

	this->format.bits = 16;
	this->format.bitrate = ogg_info->rate;
	this->format.channels = ogg_info->channels;

	// all ok
	return this->opened = true;
}


/**
	Closes OGG audio file.
	Terminates Vorbis codec, deletes data and File object. File has to be opened before.
*/
void OggAudioFile::Close()
{
	Assert(this->opened);

	// clear ogg structures
	ov_clear(&this->ogg_file);

	// close audio file
	AudioFile::Close();
}


/**
	Loads audio data from opened OGG file.

	Custom callback functions are used for accessing the file.

	@return Size of loaded data.
*/
ulong_t OggAudioFile::LoadData()
{
	Assert(this->opened);
	Assert(this->state == LS_UNLOADED);

	ulong_t read_size;

	// get data size
	long size = (long)ov_pcm_total(&this->ogg_file, -1);
	if (this->CheckOVError(size))
		return -1;

	size *= this->format.bits / 8;
	size *= this->format.channels;

	// read whole wile
	read_size = this->LoadBlock((ulong_t)size, false);
	if (!read_size) {
		LogError1("Error loading block from audio file: %s", this->file_name.c_str());
		return 0;
	}

	// set state to loaded
	state = LS_LOADED;
	return read_size;
}


/**
	Loads one block of audio file. Length of this block is set by time in seconds.

	@param time Time length of audio block in seconds.
	@param looping Whether looping is enabled.

	@return Size of loaded data in bytes.
*/
ulong_t OggAudioFile::LoadTimeBlock(double time, bool looping)
{
	Assert(this->opened);
	Assert(this->state != LS_LOADED);

	// converts time length into lenght in bytes
	ulong_t size = ulong_t(time * this->format.bitrate);
	
	size *= this->format.bits / 8;
	size *= this->format.channels;

	// loads block
	return LoadBlock(size, looping);
}


/**
	Loads one block of audio file. Length of this block is set in bytes.

	@param size Length of audio block in bytes.
	@param looping Whether looping is enabled.

	@return Size of loaded data in bytes.
*/
ulong_t OggAudioFile::LoadBlock(ulong_t size, bool looping)
{
	Assert(this->opened);
	Assert(this->state != LS_LOADED);

	// expand data buffer if it is needed
	if (this->data && this->buff_size < size)
		SafeDelete(this->data);

	if (!this->data) {
		this->data = NEW char[size];
		Assert(this->data);
		this->buff_size = size;
	}

	// reset data size
	this->data_size = 0;

	// read new data in loop
	int bitstream = 0;
	ulong_t read_size = 0;
	long read_count = 0;

	while (read_size < size) {
		// read block
		read_count = ov_read(
			&this->ogg_file,
			this->data + read_size,
			size - read_size,
			0, 2, 1,
			&bitstream
		);

		if (this->CheckOVError(read_count))
			return 0;
	    
		// if end of file
		if (!read_count) {
			if (looping)
				ov_raw_seek(&this->ogg_file, 0);
			else
				break;
		}
		else
			read_size += read_count;
	}

	// set state to loaded or block-loaded
	if (read_size == size)
		this->state = LS_BLOCK_LOADED;
	else
		this->state = LS_LOADED;

	return this->data_size = read_size;
}


/**
	Rewinds current position to the file beginning.
*/
void OggAudioFile::Rewind()
{
	Assert(this->opened);

	// seek to begin
	long pos = ov_raw_seek(&this->ogg_file, 0);
	this->CheckOVError(pos);
}


/**
	Checks and reports error code returned by Vorbis codec.

	@param read_count Value returned by various OV functions.
	@return @c True if error occures.
*/
bool OggAudioFile::CheckOVError(const long &read_count)
{
	if (read_count >= 0)
		return false;

	// report error message
	string err_msg  = "OggVorbis error: ";

	switch(read_count) {
	case OV_FALSE:         err_msg += "OV_FALSE"; break;
	case OV_HOLE:          err_msg += "OV_HOLE"; break;
	case OV_EREAD:         err_msg += "OV_EREAD"; break;
	case OV_EFAULT:        err_msg += "OV_EFAULT"; break;
	case OV_EIMPL:         err_msg += "OV_EIMPL"; break;
	case OV_EINVAL:        err_msg += "OV_EINVAL"; break;
	case OV_ENOTVORBIS:    err_msg += "OV_ENOTVORBIS"; break;
	case OV_EBADHEADER:    err_msg += "OV_EBADHEADER"; break;
	case OV_EVERSION:      err_msg += "OV_EVERSION"; break;
	case OV_EBADLINK:      err_msg += "OV_EBADLINK"; break;
	case OV_ENOSEEK:       err_msg += "OV_ENOSEEK"; break;
	default:               err_msg += "Undefined error"; break;
	}

	LogError(err_msg.c_str());
	return true;
}


// vim:ts=4:sw=4:
