//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwgfxserver.cpp
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Opening display according to profile settings.
*/


//=========================================================================
// Includes
//=========================================================================

#include "glfwmodule/glfwgfxserver.h"
#include "framework/resourceserver.h"
#include "framework/inputserver.h"
#include "kernel/logserver.h"
#include "kernel/kernelserver.h"

PrepareScriptClass(GlfwGfxServer, "OpenGLGfxServer", s_NewGlfwGfxServer, s_InitGlfwGfxServer, s_InitGlfwGfxServer_cmds);


//=========================================================================
// OpenGLServer
//=========================================================================

/**
	Constructor.
*/
GlfwGfxServer::GlfwGfxServer(const char *id) :
	OpenGLGfxServer(id)
{
	// empty
}


ushort_t GlfwGfxServer::GetVideoModes(VideoMode *modes, int max_count, VideoMode::Bpp bpp)
{
	GLFWvidmode *list = NEW GLFWvidmode[max_count * 4];
	int count;
	ushort_t i, j;
	VideoMode::Bpp list_bpp;

	count = glfwGetVideoModes(list, max_count);

	// return only filtered modes
	for (i = 0, j = 0; i < count; i++)
	{
		list_bpp = VideoMode::IntToBpp(list[i].RedBits + list[i].GreenBits + list[i].BlueBits);

		if (bpp == VideoMode::BPP_UNDEFINED || list_bpp == bpp)
		{
			modes[j].width = list[i].Width;
			modes[j].height = list[i].Height;
			modes[j].bpp = list_bpp;
			j++;
		}
	}

	delete list;
	return j;
}


void GlfwGfxServer::SetWindowTitle(const string &window_title)
{
	// set window title
	if (this->display_opened)
		glfwSetWindowTitle(window_title.c_str());

	// store new value
	GfxServer::SetWindowTitle(window_title);
}


void GlfwGfxServer::SetWindowPosition(const DisplayMode::WidowPosition &position)
{
	// set window position
	if (this->display_opened)
		glfwSetWindowPos(position.left, position.top);

	// set new position to display mode
	GfxServer::SetWindowPosition(position);
}


void GlfwGfxServer::SetWindowSize(const DisplayMode::WidowSize &size)
{
	DisplayMode::WidowSize real_size = size;

	// change size
	if (this->display_opened)
	{
		glfwSetWindowSize(size.width, size.height);

		// correct real window size
		// this is necessary because setting of window size dont need to be successful
		int w, h;
		glfwGetWindowSize(&w, &h);
		real_size.width = w;
		real_size.height = h;
	}

	// set new size to display mode
	GfxServer::SetWindowSize(real_size);
}


void GlfwGfxServer::SetWindowVerticalSync(bool vsync)
{
	// We are not going to synchronise with monitor refresh rate on UNIX, because
    // it seems xorg does not support this feature and the program aborts. This
    // is a bug of glfw, which tries to set nonexistent feature.
	if (this->display_opened)
#ifdef WINDOWS
		glfwSwapInterval(vsync ? 1 : 0);
#else
		glfwSwapInterval(0);
#endif

	GfxServer::SetWindowVerticalSync(vsync);
}


void GlfwGfxServer::MinimizeWindow()
{
	Assert(this->display_opened);
	glfwIconifyWindow();
}


void GlfwGfxServer::RestoreWindow()
{
	Assert(this->display_opened);
	glfwRestoreWindow();
}


bool GlfwGfxServer::IsWindowMinimized()
{
	Assert(this->display_opened);
	return (glfwGetWindowParam(GLFW_ICONIFIED) == GL_TRUE);
}


bool GlfwGfxServer::IsWindowActive()
{
	Assert(this->display_opened);
	return (glfwGetWindowParam(GLFW_ACTIVE) == GL_TRUE);
}


bool GlfwGfxServer::IsWindowAccelerated()
{
	Assert(this->display_opened);
	return (glfwGetWindowParam(GLFW_ACCELERATED) == GL_TRUE);
}


void GlfwGfxServer::PresentScene()
{
	Assert(this->display_opened);

	GfxServer::PresentScene();
	glfwSwapBuffers();
}


void GlfwGfxServer::RegisterCallbacks()
{
	Assert(this->display_opened);

	// set callback functions
	glfwSetWindowCloseCallback(this->OnClose);
	glfwSetWindowSizeCallback(this->OnSize);
	glfwSetWindowRefreshCallback(this->OnRefresh);
}


bool GlfwGfxServer::OpenDisplay()
{
	if (!GfxServer::OpenDisplay())
		return false;

	this->display_opened = false;

	// create OpenGL window
	int red_bits, blue_bits, green_bits;
	int refresh_rate;
	int result;

	switch (this->display_mode.GetBpp())
	{
	case VideoMode::BPP_16:
		red_bits = 5;
		green_bits = 6;
		blue_bits = 5;
		break;

	case VideoMode::BPP_UNDEFINED:
	case VideoMode::BPP_24:
		red_bits = 8;
		green_bits = 8;
		blue_bits = 8;
		break;
	}

	// set refresh rate
	refresh_rate = this->display_mode.GetRefreshRate();
	if (refresh_rate)
		glfwOpenWindowHint(GLFW_REFRESH_RATE, refresh_rate);

	// open window
	result = glfwOpenWindow (
		this->display_mode.GetModeSize().width,
		this->display_mode.GetModeSize().height,
		red_bits,
		blue_bits,
		green_bits,
		0, // alphabits
		8, // depthbits
		0, // stencilbits
		this->display_mode.IsFullscreen() ? GLFW_FULLSCREEN : GLFW_WINDOW
	);

	if (result == GL_FALSE)
	{
		LogCritical("Can not open OpenGL window");
		return false;
	}

	this->display_opened = true;

	// disable auto polling
	glfwDisable(GLFW_AUTO_POLL_EVENTS);

	this->WriteInfo();
	this->CheckExtensions();

	if (!this->IsWindowAccelerated())
		LogWarning("GFX hardware acceleration is disabled");

	this->RegisterCallbacks();
	this->LoadResources();

	InputServer *input_server = InputServer::GetInstance();
	if (input_server)
		input_server->SetMousePosition(this->display_mode.GetModeSize().width / 2, this->display_mode.GetModeSize().height / 2);

	// set properties
	this->SetWindowVerticalSync(this->display_mode.IsVerticalSync());
	this->SetWindowTitle(this->window_title);
	this->SetBlending(this->blending);
	this->SetDepthTest(this->depth_test);
	GfxServer::SetClearValues(this->clear_values);
	this->SetCursorVisibility(this->cursor_visibility);

	return true;
}


void GlfwGfxServer::CloseDisplay()
{
	GfxServer::CloseDisplay();

	glfwCloseWindow();
}


bool GlfwGfxServer::Trigger()
{
	glfwPollEvents();
	return GfxServer::Trigger();
}


void GlfwGfxServer::SetCursorVisibility(CursorVisibility type)
{
	GfxServer::SetCursorVisibility(type);

	switch (type)
	{
	case CURSOR_HIDDEN:
		glfwDisable(GLFW_MOUSE_CURSOR);
		break;

	case CURSOR_SYSTEM:
		glfwEnable(GLFW_MOUSE_CURSOR);
		break;

	case CURSOR_CUSTOM:
		glfwDisable(GLFW_MOUSE_CURSOR);
		break;

	default:
		Error();
		break;
	}
}


void GlfwGfxServer::CheckExtensions()
{
	this->extensions = EXT_NONE;

#ifdef GL_ARB_texture_non_power_of_two
	if (glfwExtensionSupported("GL_ARB_texture_non_power_of_two"))
	{
		this->extensions |= EXT_NPOT_TEXTURES;
		LogInfo1("Extension:   %s", "EXT_NPOT_TEXTURES");
	}
#endif
}


// vim:ts=4:sw=4:
