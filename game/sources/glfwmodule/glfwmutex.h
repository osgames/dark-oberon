#ifndef __glfwmutex_h__
#define __glfwmutex_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwmutex.h
	@ingroup GLFW_Module

 	@author Peter Knut, Marian Cerny
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <glfw/glfw.h>

#include "framework/system.h"
#ifdef WINDOWS
#	undef APIENTRY
#	undef WINGDIAPI
#endif

#include "kernel/mutex.h"
#include "kernel/logserver.h"


//=========================================================================
// GlfwMutex
//=========================================================================

/**
	@class GlfwMutex
	@ingroup GLFW_Module

	@brief Mutex implemented with GLFW library.
*/

class GlfwMutex : public Mutex 
{
	friend class GlfwThreadServer;
	friend class GlfwCondition;

//--- methods
public:
	virtual ~GlfwMutex();

	virtual void Lock() const;
	virtual void Unlock() const;

protected:
	GlfwMutex();

//--- variables
private:
	GLFWmutex mutex;
};


inline
GlfwMutex::GlfwMutex() :
	Mutex()
{
	this->mutex = glfwCreateMutex();
	AssertMsg(this->mutex, "Can not create GLFW mutex");
}


inline
GlfwMutex::~GlfwMutex()
{
	glfwDestroyMutex(this->mutex);
}


/** @brief Locks the mutex.
*/
inline
void GlfwMutex::Lock() const
{
	glfwLockMutex(this->mutex);
}


/** @brief Unlocks the mutex.
*/
inline
void GlfwMutex::Unlock() const
{
	glfwUnlockMutex(this->mutex);
}


//=========================================================================
// GlfwRecMutex
//=========================================================================

/**
	@class GlfwRecMutex
	@ingroup GLFW_Module

	@brief Recursive mutex implemented with GLFW library.
*/

class GlfwRecMutex : public RecMutex
{
	friend class GlfwThreadServer;

//--- methods
public:
	virtual ~GlfwRecMutex();

	virtual void Lock() const;
	virtual void Unlock() const;

protected:
	GlfwRecMutex();

//--- variables
private:
	GLFWmutex mutex;
	mutable GLFWthread locked_by;
	mutable int locked_count;
};


inline
GlfwRecMutex::GlfwRecMutex() :
	RecMutex(),
	locked_by(-1),
	locked_count(0)
{
	this->mutex = glfwCreateMutex();
	AssertMsg(this->mutex, "Can not create GLFW mutex");
}


inline
GlfwRecMutex::~GlfwRecMutex()
{
	AssertMsg(this->locked_by == -1, "Can not destroy locked mutex");
	glfwDestroyMutex(this->mutex);
}


inline
void GlfwRecMutex::Lock() const
{
	GLFWthread myself = glfwGetThreadID();

	if (this->locked_by != myself)
	{
		glfwLockMutex(this->mutex);
		this->locked_by = myself;
	}

	this->locked_count++;
}


inline
void GlfwRecMutex::Unlock() const
{
	GLFWthread myself = glfwGetThreadID();

	AssertMsg(this->locked_by != -1, "Mutex is not locked");
	AssertMsg(this->locked_by == myself, "Can not unlock Mutex locked from different thread");

	this->locked_count--;

	if (!this->locked_count) {
		this->locked_by = -1;
		glfwUnlockMutex(this->mutex);
	}
}


#endif  // __glfwmutex_h__

// vim:ts=4:sw=4:
