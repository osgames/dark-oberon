#ifndef __glfwkernelserver_h__
#define __glfwkernelserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwkernelserver.h
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <glfw/glfw.h>

#include "framework/system.h"
#ifdef WINDOWS
#	undef APIENTRY
#	undef WINGDIAPI
#endif

#include "kernel/kernelserver.h"
#include "kernel/logserver.h"


//=========================================================================
// GlfwKernelServer
//=========================================================================

/**
	@class GlfwKernelServer
	@ingroup GLFW_Module

	@brief GLFW kernel server.
*/

class GlfwKernelServer : public KernelServer
{
//--- variables
private:

//--- methods
public:
	GlfwKernelServer();
	virtual ~GlfwKernelServer();
};


#endif  // __glfwkernelserver_h__

// vim:ts=4:sw=4:
