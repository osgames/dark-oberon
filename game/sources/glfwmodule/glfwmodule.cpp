//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwmodule.cpp
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/kernelserver.h"


//=========================================================================
// Glfwmodule module
//=========================================================================

extern "C" void GlfwModule();


extern bool s_InitGlfwTimeServer (Class *, KernelServer *);
extern Object *s_NewGlfwTimeServer (const char *name);
extern bool s_InitGlfwGfxServer (Class *, KernelServer *);
extern Object *s_NewGlfwGfxServer (const char *name);
extern bool s_InitGlfwInputServer (Class *, KernelServer *);
extern Object *s_NewGlfwInputServer (const char *name);


void GlfwModule()
{
	KernelServer::GetInstance()->RegisterClass("GlfwTimeServer", s_InitGlfwTimeServer, s_NewGlfwTimeServer);
	KernelServer::GetInstance()->RegisterClass("GlfwGfxServer", s_InitGlfwGfxServer, s_NewGlfwGfxServer);
	KernelServer::GetInstance()->RegisterClass("GlfwInputServer", s_InitGlfwInputServer, s_NewGlfwInputServer);
}


// vim:ts=4:sw=4:
