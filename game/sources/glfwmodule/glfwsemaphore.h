#ifndef __glfwsemaphore_h__
#define __glfwsemaphore_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================
/**
	@file glfwsemaphore.h
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <glfw/glfw.h>

#include "framework/system.h"
#ifdef WINDOWS
#	undef APIENTRY
#	undef WINGDIAPI
#endif

#include "kernel/semaphore.h"


//=========================================================================
// GlfwSemaphore
//=========================================================================

/**
	@class GlfwSemaphore
	@ingroup GLFW_Module

	@brief Mutex implemented with GLFW library.
*/

class GlfwSemaphore : public Semaphore
{
	friend class GlfwThreadServer;

//--- methods
public:
	virtual ~GlfwSemaphore();

	virtual void Wait(double timeout = -1.0);
	virtual void SendSignal();

protected:
	GlfwSemaphore();

//--- variables
private:
	GLFWcond cond;
	GLFWmutex mutex;
};


//=========================================================================
// Methods
//=========================================================================

inline
GlfwSemaphore::GlfwSemaphore() :
	Semaphore()
{
	this->cond = glfwCreateCond();
	AssertMsg(this->cond, "Can not create GLFW condition variable");

	this->mutex = glfwCreateMutex();
	AssertMsg(this->mutex, "Can not create GLFW mutex variable");
}


inline
GlfwSemaphore::~GlfwSemaphore()
{
	glfwDestroyCond(this->cond);
	glfwDestroyMutex(this->mutex);
}


inline
void GlfwSemaphore::Wait(double timeout)
{
	Assert(timeout == -1.0 || timeout > 0.0);
	if (timeout == -1.0)
		timeout = GLFW_INFINITY;

	glfwLockMutex(this->mutex);

	if (!this->singnals)
		glfwWaitCond(this->cond, this->mutex, timeout);

	if (this->singnals)
		this->singnals--;

	glfwUnlockMutex(this->mutex);
}


inline
void GlfwSemaphore::SendSignal()
{
	glfwLockMutex(this->mutex);
	this->singnals++;
	glfwUnlockMutex(this->mutex);

	glfwSignalCond(this->cond);
}


#endif  // __glfwsemaphore_h__

// vim:ts=4:sw=4:
