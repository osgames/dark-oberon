#ifndef __glfwthreadserver_h__
#define __glfwthreadserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwthreadserver.h
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "kernel/threadserver.h"
#include "glfwmodule/glfwmutex.h"
#include "glfwmodule/glfwthread.h"
#include "glfwmodule/glfwcondition.h"
#include "glfwmodule/glfwsemaphore.h"


//=========================================================================
// GlfwThreadServer
//=========================================================================

/**
	@class GlfwThreadServer
	@ingroup GLFW_Module

	@brief GLFW kernel server.
*/

class GlfwThreadServer : public ThreadServer
{
//--- methods
public:
	GlfwThreadServer();

	virtual Mutex* CreateMutex();
	virtual RecMutex* CreateRecMutex();
	virtual Thread* CreateThread();
	virtual Condition* CreateCondition();
	virtual Semaphore* CreateSemaphore();

	virtual int GetThreadID();
	virtual void Sleep(double time);
};


#endif  // __glfwthreadserver_h__

// vim:ts=4:sw=4:
