//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwtimeserver.cpp
	@ingroup GLFW_Module

 	@author ???
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "glfwmodule/glfwtimeserver.h"
#include "kernel/kernelserver.h"


PrepareScriptClass(GlfwTimeServer, "TimeServer", s_NewGlfwTimeServer, s_InitGlfwTimeServer, s_InitGlfwTimeServer_cmds);


//=========================================================================
// GlfwKernelServer
//=========================================================================

double GlfwTimeServer::GetRealTime()
{
	return glfwGetTime();
}


// vim:ts=4:sw=4:
