//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwinputserver.cpp
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005, 2007

	@version 1.0 - Initial.
	@version 1.1 - Fixed cursor position.
*/


//=========================================================================
// Includes
//=========================================================================

#include "glfwmodule/glfwinputserver.h"
#include "framework/gfxserver.h"
#include "kernel/logserver.h"
#include "kernel/kernelserver.h"
#include "kernel/mathlib.h"

PrepareScriptClass(GlfwInputServer, "InputServer", s_NewGlfwInputServer, s_InitGlfwInputServer, s_InitGlfwInputServer_cmds);


//=========================================================================
// OpenGLServer
//=========================================================================

/**
	Constructor.
*/
GlfwInputServer::GlfwInputServer(const char *id) :
	InputServer(id),
	has_callbacks(false)
{
	//
}


/**
	Destructor.
*/
GlfwInputServer::~GlfwInputServer()
{
	//
}


bool GlfwInputServer::Trigger()
{
	if (!this->has_callbacks)
		this->RegisterCallbacks();

	glfwPollEvents();

	return InputServer::Trigger();
}


void GlfwInputServer::WaitForEvent()
{
	glfwWaitEvents();
}


void GlfwInputServer::SetMousePosition(ushort_t x, ushort_t y)
{
	glfwSetMousePos(x, y);
}


void GlfwInputServer::RegisterCallbacks()
{
	glfwSetKeyCallback(this->CallbackKey);
	glfwSetCharCallback(this->CallbackChar);
	glfwSetMouseButtonCallback(this->CallbackButton);
	glfwSetMousePosCallback(this->CallbackPosition);
	glfwSetMouseWheelCallback(this->CallbackWheel);

	this->has_callbacks = true;
}


void GLFWCALL GlfwInputServer::CallbackKey(int key, int action)
{
	GlfwInputServer *me = (GlfwInputServer *)InputServer::GetInstance();

	KeyCode code = me->translateKey(key);
	if (code == KEY_INVALID)
		return;

	KeyState state = me->translateAction(action);
	if (state == KS_INVALID)
		return;

	me->keys[code].pressed = (state == KS_DOWN);

	// call callbacks
	bool res = false;
	if (me->gui_callback_key)
		res = me->gui_callback_key(code, state);
	if (!res && me->callback_key)
		me->callback_key(code, state);

	// call action callbacks
	if (me->keys[code].bind_info[state])
		me->EmitAction(me->keys[code].bind_info[state]->action, 0);
}


void GLFWCALL GlfwInputServer::CallbackChar(int character, int action)
{
	GlfwInputServer *me = (GlfwInputServer *)InputServer::GetInstance();

	KeyState state = me->translateAction(action);
	if (state == KS_INVALID)
		return;

	// call callbacks
	bool res = false;
	if (me->gui_callback_char)
		res = me->gui_callback_char(character, state);
	if (!res && me->callback_char)
		me->callback_char(character, state);
}


void GLFWCALL GlfwInputServer::CallbackButton(int button, int action)
{
	GlfwInputServer *me = (GlfwInputServer *)InputServer::GetInstance();

	KeyCode code = me->translateButton(button);
	if (code == KEY_INVALID)
		return;

	KeyState state = me->translateAction(action);
	if (state == KS_INVALID)
		return;

	me->keys[code].pressed = (state == KS_DOWN);

	// call callbacks
	bool res = false;
	if (me->gui_callback_key)
		res = me->gui_callback_key(code, state);
	if (!res && me->callback_key)
		me->callback_key(code, state);

	// call action callbacks
	if (me->keys[code].bind_info[state])
		me->EmitAction(me->keys[code].bind_info[state]->action, 0);
}


void GLFWCALL GlfwInputServer::CallbackPosition(int x, int y)
{
	if (!glfwGetWindowParam(GLFW_ACTIVE))
		return;

	GlfwInputServer *me = (GlfwInputServer *)InputServer::GetInstance();
	GfxServer *gfx_server = (GfxServer *)GfxServer::GetInstance();
	Assert(gfx_server);

	bool res;

	//LogDebug2("x: %d, y: %d", x, y);

	// get window size
	const DisplayMode::WidowSize &size = gfx_server->GetWindowSize();

	// reset positions when cursor changes visibility
	static GfxServer::CursorVisibility visibility = GfxServer::CURSOR_SYSTEM;
	if (gfx_server)
	{
		if (gfx_server->GetCursorVisibility() != visibility)
		{
			me->mouse_x = me->mouse_absx;
			me->mouse_y = me->mouse_absy;
			visibility = gfx_server->GetCursorVisibility();
		}
	}

	// compute mouse position and difference
	me->mouse_reldx = x - me->mouse_absx;
	me->mouse_reldy = y - me->mouse_absy;
	me->mouse_absx = x;
	me->mouse_absy = y;

	int dx, dy;
	if (me->mouse_fixed && visibility != GfxServer::CURSOR_SYSTEM)
	{
		dx = me->mouse_reldx;
		dy = me->mouse_reldy;
	}
	else
	{
		dx = me->mouse_x;
		dy = me->mouse_y;
		me->mouse_x = n_clamp(me->mouse_x + me->mouse_reldx, 0, size.width - 1);
		me->mouse_y = n_clamp(me->mouse_y + me->mouse_reldy, 0, size.height - 1);
		dx = me->mouse_x - dx;
		dy = me->mouse_y - dy;
	}

	// call callbacks
	if (dx || dy) {
		res = false;
		if (me->gui_callback_position)
			res = me->gui_callback_position(me->mouse_x, me->mouse_y);
		if (!res && me->callback_position)
			me->callback_position(me->mouse_x, me->mouse_y);
	}

	if (me->mouse_reldx || me->mouse_reldy) {
		res = false;
		if (me->gui_callback_relposition)
			res = me->gui_callback_relposition(me->mouse_reldx, me->mouse_reldy);
		if (!res && me->callback_relposition)
			me->callback_relposition(me->mouse_reldx, me->mouse_reldy);
	}

	// call action callbacks
	if (dx && me->sliders[MOUSE_X].bind_info)
		me->EmitAction(me->sliders[MOUSE_X].bind_info->action, me->mouse_x);
	if (dy && me->sliders[MOUSE_Y].bind_info)
		me->EmitAction(me->sliders[MOUSE_Y].bind_info->action, me->mouse_y);

	if (me->mouse_reldx && me->sliders[MOUSE_RELX].bind_info)
		me->EmitAction(me->sliders[MOUSE_RELX].bind_info->action, me->mouse_reldx);
	if (me->mouse_reldy && me->sliders[MOUSE_RELY].bind_info)
		me->EmitAction(me->sliders[MOUSE_RELY].bind_info->action, me->mouse_reldy);
}


void GLFWCALL GlfwInputServer::CallbackWheel(int pos)
{
	GlfwInputServer *me = (GlfwInputServer *)InputServer::GetInstance();

	// save value
	int dpos = pos - me->mouse_wheel;
	me->mouse_wheel = pos;

	// call callbacks
	bool res;
	
	res = false;
	if (me->gui_callback_wheel)
		res = me->gui_callback_wheel(pos);
	if (!res && me->callback_wheel)
		me->callback_wheel(pos);

	res = false;
	if (me->gui_callback_relwheel)
		res = me->gui_callback_relwheel(dpos);
	if (!res && me->callback_relwheel)
		me->callback_relwheel(dpos);

	// call action callbacks
	if (me->sliders[MOUSE_WHEEL].bind_info)
		me->EmitAction(me->sliders[MOUSE_WHEEL].bind_info->action, me->mouse_wheel);
	if (dpos && me->sliders[MOUSE_RELWHEEL].bind_info)
		me->EmitAction(me->sliders[MOUSE_RELWHEEL].bind_info->action, dpos);
}


// vim:ts=4:sw=4:
