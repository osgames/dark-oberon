#ifndef __glfwcondition_h__
#define __glfwcondition_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================
/**
	@file glfwcondition.h
	@ingroup GLFW_Module

 	@author Peter Knut, Marian Cerny
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <glfw/glfw.h>

#include "framework/system.h"
#ifdef WINDOWS
#	undef APIENTRY
#	undef WINGDIAPI
#endif

#include "kernel/condition.h"
#include "glfwmodule/glfwmutex.h"


//=========================================================================
// GlfwCondition
//=========================================================================

/**
	@class GlfwCondition
	@ingroup GLFW_Module

	@brief Mutex implemented with GLFW library.
*/

class GlfwCondition : public Condition
{
	friend class GlfwThreadServer;

//--- methods
public:
	virtual ~GlfwCondition();

	virtual void Wait(Mutex *mutex, double timeout = -1.0);
	virtual void SendSignal();
	virtual void SendBroadcast();

protected:
	GlfwCondition();

//--- variables
private:
	GLFWcond cond;
};


inline
GlfwCondition::GlfwCondition() :
	Condition()
{
	this->cond = glfwCreateCond();
	AssertMsg(this->cond, "Can not create GLFW condition variable");
}


inline
GlfwCondition::~GlfwCondition()
{
	glfwDestroyCond(this->cond);
}


inline
void GlfwCondition::Wait(Mutex *mutex, double timeout)
{
	Assert(mutex && (timeout == -1.0 || timeout > 0.0));
	if (timeout == -1.0)
		timeout = GLFW_INFINITY;

	glfwWaitCond(this->cond, static_cast<GlfwMutex *>(mutex)->mutex, timeout);
}


inline
void GlfwCondition::SendSignal()
{
	glfwSignalCond(this->cond);
}


inline
void GlfwCondition::SendBroadcast()
{
	glfwBroadcastCond(this->cond);
}


#endif  // __glfwcondition_h__

// vim:ts=4:sw=4:
