#ifndef __glfwgfxserver_h__
#define __glfwgfxserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwgfxserver.h
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <glfw/glfw.h>
#define OPENGL_INCLUDED

#include "framework/system.h"
#include "openglmodule/openglgfxserver.h"
#include "openglmodule/glext.h"


//=========================================================================
// GlfwGfxServer
//=========================================================================

/**
	@class GlfwGfxServer
	@ingroup GLFW_Module

	@brief OpenAL audio server.
*/

class GlfwGfxServer : public OpenGLGfxServer
{
//--- methods
public:
	GlfwGfxServer(const char *id);

	virtual void RegisterCallbacks();
	virtual bool Trigger();
	virtual ushort_t GetVideoModes(VideoMode *modes, int max_count, VideoMode::Bpp = VideoMode::BPP_UNDEFINED);

	// window
	virtual void SetWindowTitle(const string &window_title);
	virtual void SetWindowPosition(const DisplayMode::WidowPosition &position);
	virtual void SetWindowSize(const DisplayMode::WidowSize &size);
	virtual void SetWindowVerticalSync(bool vsync);
	virtual void MinimizeWindow();
	virtual void RestoreWindow();
	virtual bool IsWindowMinimized();
	virtual bool IsWindowActive();
	virtual bool IsWindowAccelerated();

	// display
	virtual bool OpenDisplay();
    virtual void CloseDisplay();
	virtual void PresentScene();

	virtual void SetCursorVisibility(CursorVisibility type);

private:
	static int GLFWCALL OnClose();
	static void GLFWCALL OnSize(int width, int height);
	static void GLFWCALL OnRefresh();
	
	virtual void CheckExtensions();
};


//=========================================================================
// Methods
//=========================================================================

inline
int GLFWCALL GlfwGfxServer::OnClose()
{
	// close display
	GfxServer::GetInstance()->CloseDisplay();

	// display is still opened?
	return GfxServer::GetInstance()->IsDisplayOpened() ? GL_FALSE : GL_TRUE;
}


inline
void GLFWCALL GlfwGfxServer::OnSize(int width, int height)
{
	GlfwGfxServer *me = (GlfwGfxServer *)(GfxServer::GetInstance());

	me->display_mode.SetModeSize(DisplayMode::WidowSize(ushort_t(width), ushort_t(height)));

	// call user callback
	if (me->callback_size)
		me->callback_size(ushort_t(width), ushort_t(height), me->display_mode.IsFullscreen());
}


inline
void GLFWCALL GlfwGfxServer::OnRefresh()
{
	GlfwGfxServer *me = (GlfwGfxServer *)(GfxServer::GetInstance());

	// call user callback
	if (me->callback_refresh)
		me->callback_refresh();
}


#endif  // __glfwgfxserver_h__

// vim:ts=4:sw=4:
