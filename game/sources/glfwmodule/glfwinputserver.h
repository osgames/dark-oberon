#ifndef __glfwinputserver_h__
#define __glfwinputserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwinputserver.h
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <glfw/glfw.h>

#include "framework/system.h"
#ifdef WINDOWS
#	undef APIENTRY
#	undef WINGDIAPI
#endif

#include "framework/inputserver.h"


//=========================================================================
// GlfwInputServer
//=========================================================================

/**
	@class GlfwInputServer
	@ingroup GLFW_Module

	@brief GLFW input server.
*/

class GlfwInputServer : public InputServer
{
//--- variables
private:
	bool has_callbacks;


//--- methods
public:
	GlfwInputServer(const char *id);
	virtual ~GlfwInputServer();

	virtual void RegisterCallbacks();
	virtual bool Trigger();
	virtual void WaitForEvent();

	virtual void SetMousePosition(ushort_t x, ushort_t y);

private:
	static void GLFWCALL CallbackKey(int key, int action);
	static void GLFWCALL CallbackChar(int character, int action);
	static void GLFWCALL CallbackButton(int button, int action);
	static void GLFWCALL CallbackPosition(int x, int y);
	static void GLFWCALL CallbackWheel(int pos);

	static KeyCode translateKey(int glwf_key);
	static KeyCode translateButton(int glwf_button);
	static KeyState translateAction(int glwf_action);
};


//=========================================================================
// Methods
//=========================================================================


inline
KeyCode GlfwInputServer::translateKey(int glwf_key)
{
	if (glwf_key >= 'A' && glwf_key <= 'Z')
		return KeyCode(glwf_key - 'A' + KEY_A);
	if (glwf_key >= '0' && glwf_key <= '9')
		return KeyCode(glwf_key - '0' + KEY_0);
	if (glwf_key >= GLFW_KEY_F1 && glwf_key <= GLFW_KEY_F25)
		return KeyCode(glwf_key - GLFW_KEY_F1 + KEY_F1);

	switch (glwf_key)
	{
	case GLFW_KEY_SPACE:           return KEY_SPACE;
	case GLFW_KEY_ESC:             return KEY_ESC;
	case GLFW_KEY_UP:              return KEY_UP;
	case GLFW_KEY_DOWN:            return KEY_DOWN;
	case GLFW_KEY_LEFT:            return KEY_LEFT;
	case GLFW_KEY_RIGHT:           return KEY_RIGHT;
	case GLFW_KEY_LSHIFT:          return KEY_LSHIFT;
	case GLFW_KEY_RSHIFT:          return KEY_RSHIFT;
	case GLFW_KEY_LCTRL:           return KEY_LCTRL;
	case GLFW_KEY_RCTRL:           return KEY_RCTRL;
	case GLFW_KEY_LALT:            return KEY_LALT;
	case GLFW_KEY_RALT:            return KEY_RALT;
	case GLFW_KEY_TAB:             return KEY_TAB;
	case GLFW_KEY_ENTER:           return KEY_ENTER;
	case GLFW_KEY_BACKSPACE:       return KEY_BACKSPACE;
	case GLFW_KEY_INSERT:          return KEY_INSERT;
	case GLFW_KEY_DEL:             return KEY_DEL;
	case GLFW_KEY_PAGEUP:          return KEY_PAGEUP;
	case GLFW_KEY_PAGEDOWN:        return KEY_PAGEDOWN;
	case GLFW_KEY_HOME:            return KEY_HOME;
	case GLFW_KEY_END:             return KEY_END;
	case GLFW_KEY_KP_0:            return KEY_NP_0;
	case GLFW_KEY_KP_1:            return KEY_NP_1;
	case GLFW_KEY_KP_2:            return KEY_NP_2;
	case GLFW_KEY_KP_3:            return KEY_NP_3;
	case GLFW_KEY_KP_4:            return KEY_NP_4;
	case GLFW_KEY_KP_5:            return KEY_NP_5;
	case GLFW_KEY_KP_6:            return KEY_NP_6;
	case GLFW_KEY_KP_7:            return KEY_NP_7;
	case GLFW_KEY_KP_8:            return KEY_NP_8;
	case GLFW_KEY_KP_9:            return KEY_NP_9;
	case GLFW_KEY_KP_DIVIDE:       return KEY_NP_DIVIDE;
	case GLFW_KEY_KP_MULTIPLY:     return KEY_NP_MULTIPLY;
	case GLFW_KEY_KP_SUBTRACT:     return KEY_NP_SUBTRACT;
	case GLFW_KEY_KP_ADD:          return KEY_NP_ADD;
	case GLFW_KEY_KP_DECIMAL:      return KEY_NP_DECIMAL;
	case GLFW_KEY_KP_EQUAL:        return KEY_NP_EQUAL;
	case GLFW_KEY_KP_ENTER:        return KEY_NP_ENTER;

	case '`':        return KEY_LSQUOTE;
	case '\'':       return KEY_SQUOTE;
	case '-':        return KEY_SUBTRACT;
	case '=':        return KEY_EQUAL;
	case '[':        return KEY_LBRACKET;
	case ']':        return KEY_RBRACKET;
	case '/':        return KEY_SLASH;
	case '\\':       return KEY_BACKSLASH;
	case ',':        return KEY_COMMA;
	case '.':        return KEY_DOT;
	case ';':        return KEY_SEMICOLON;

	default:         return KEY_INVALID;
	}
}


inline
KeyCode GlfwInputServer::translateButton(int glwf_button)
{
	switch (glwf_button)
	{
	case GLFW_MOUSE_BUTTON_LEFT:   return MOUSE_BUTTON_LEFT;
	case GLFW_MOUSE_BUTTON_RIGHT:  return MOUSE_BUTTON_RIGHT;
	case GLFW_MOUSE_BUTTON_MIDDLE: return MOUSE_BUTTON_MIDDLE;
	case GLFW_MOUSE_BUTTON_4:      return MOUSE_BUTTON_4;
	case GLFW_MOUSE_BUTTON_5:      return MOUSE_BUTTON_5;
	case GLFW_MOUSE_BUTTON_6:      return MOUSE_BUTTON_6;
	case GLFW_MOUSE_BUTTON_7:      return MOUSE_BUTTON_7;
	case GLFW_MOUSE_BUTTON_8:      return MOUSE_BUTTON_8;

	default:                       return KEY_INVALID;
	}
}


inline
KeyState GlfwInputServer::translateAction(int glwf_action)
{
	switch (glwf_action)
	{
	case GLFW_RELEASE:   return KS_UP;
	case GLFW_PRESS:     return KS_DOWN;
	default:             Error(); return KS_UP;
	}
}


#endif  // __glfwinputserver_h__

// vim:ts=4:sw=4:
