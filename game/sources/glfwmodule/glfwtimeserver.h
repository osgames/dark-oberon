#ifndef __glfwtimeserver_h__
#define __glfwtimeserver_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwtimeserver.h
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include <glfw/glfw.h>

#include "framework/system.h"
#ifdef WINDOWS
#	undef APIENTRY
#	undef WINGDIAPI
#endif

#include "kernel/timeserver.h"


//=========================================================================
// GlfwTimeServer
//=========================================================================

/**
	@class GlfwTimeServer
	@ingroup GLFW_Module

	@brief GLFW time server.
*/

class GlfwTimeServer : public TimeServer
{
//--- methods
public:
	GlfwTimeServer(const char *id);

	virtual double GetRealTime();
};


inline
GlfwTimeServer::GlfwTimeServer(const char *id) :
	TimeServer(id)
{
	//
}


#endif  // __glfwtimeserver_h__

// vim:ts=4:sw=4:
