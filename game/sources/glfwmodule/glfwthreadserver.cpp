//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwthreadserver.cpp
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "glfwmodule/glfwthreadserver.h"


//=========================================================================
// GlfwThreadServer
//=========================================================================

GlfwThreadServer::GlfwThreadServer() :
	ThreadServer()
{
	//
}


Mutex *GlfwThreadServer::CreateMutex()
{
	return NEW GlfwMutex();
}


RecMutex *GlfwThreadServer::CreateRecMutex()
{
	return NEW GlfwRecMutex();
}


Thread *GlfwThreadServer::CreateThread()
{
	return NEW GlfwThread();
}


Condition *GlfwThreadServer::CreateCondition()
{
	return NEW GlfwCondition();
}


Semaphore *GlfwThreadServer::CreateSemaphore()
{
	return NEW GlfwSemaphore();
}


int GlfwThreadServer::GetThreadID()
{
	return (int)glfwGetThreadID();
}


void GlfwThreadServer::Sleep(double time)
{
	glfwSleep(time);
}


// vim:ts=4:sw=4:
