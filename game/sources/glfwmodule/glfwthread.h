#ifndef __glfwthread_h__
#define __glfwthread_h__

#pragma once

//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwthread.h
	@ingroup GLFW_Module

 	@author Peter Knut, Marian Cerny
	@date 2005, 2007 - 2008

	@version 1.0 - Initial.
	@version 1.1 - Fixed IsRunning() method.
	@version 1.2 - Improved stopping and waiting for thread.
*/


//=========================================================================
// Includes
//=========================================================================

#include <glfw/glfw.h>

#include "framework/system.h"
#ifdef WINDOWS
#	undef APIENTRY
#	undef WINGDIAPI
#endif

#include "kernel/thread.h"


//=========================================================================
// GlfwThread
//=========================================================================

/**
	@class GlfwThread
	@ingroup GLFW_Module

	@brief Thread implemented with GLFW library.
*/

class GlfwThread : Thread
{
	friend class GlfwThreadServer;


//--- methods
public:
	virtual void Run(ThreadFun fun, ThreadFun wfun = NULL, void *data = NULL);
	virtual void Kill();
	virtual void Wait();

protected:
	GlfwThread();


//--- variables
private:
	GLFWthread thread;
};


//=========================================================================
// Methods
//=========================================================================

inline
GlfwThread::GlfwThread() :
	Thread(),
	thread(-1)
{
	//
}


inline
void GlfwThread::Run(ThreadFun fun, ThreadFun wfun, void *data)
{
	Assert(fun);
	Assert(!this->running);

	this->wakeup_fun = wfun;
	this->data = data;
	this->stop_requested = false;
	this->running = true;

	this->thread = glfwCreateThread((GLFWthreadfun)fun, this);

	AssertMsg(this->thread >= 0, "Can not create GLFW thread");
}


inline
void GlfwThread::Kill()
{
	Assert(this->running);


	glfwDestroyThread(this->thread);
	this->thread = -1;
	this->running = false;
}


inline
void GlfwThread::Wait()
{
	Assert(this->running);

	if (glfwWaitThread(this->thread, GLFW_WAIT) == GL_TRUE)
	{
		this->thread = -1;
		this->running = false;
	}
}


#endif  // __glfwthread_h__

// vim:ts=4:sw=4:
