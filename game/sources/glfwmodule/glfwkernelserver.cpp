//=========================================================================
// Copyright (C) Dark Oberon Team
// 
// This file is part of Dark Oberon, a free RTS game.
//
// Dark Oberon is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License.
//
// Dark Oberon is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
// See the ducuments/license/license.txt for more details.
//=========================================================================

/**
	@file glfwkernelserver.cpp
	@ingroup GLFW_Module

 	@author Peter Knut
	@date 2005

	@version 1.0 - Initial.
*/


//=========================================================================
// Includes
//=========================================================================

#include "glfwmodule/glfwkernelserver.h"
#include "glfwmodule/glfwthreadserver.h"


//=========================================================================
// GlfwKernelServer
//=========================================================================

/**
	Constructor.
*/
GlfwKernelServer::GlfwKernelServer() :
	KernelServer(false)
{
	// initialize GLFW
	if (!glfwInit())
		ErrorMsg("Can not initialize GLFW library");

	// create thread server
	NEW GlfwThreadServer();
	Assert(ThreadServer::GetInstance());

	this->Init();
}


/**
	Destructor.
*/
GlfwKernelServer::~GlfwKernelServer()
{
	this->Done();

	// terminate GLWF
	glfwTerminate();
}


// vim:ts=4:sw=4:
