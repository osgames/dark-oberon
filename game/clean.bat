@ECHO OFF

REM ==============================================================
REM  This helps you clean your Dark Oberon folder from unneeded
REM  intermediate files.
REM ==============================================================

ECHO ----------------------------------------
ECHO Deleting intermedate files...
ECHO.

ECHO Deleting logs\...
REM for WindowsNT, XP, 2k:

IF NOT "%OS%" == "Windows_NT" GOTO winnt
  IF NOT EXIST logs GOTO logs
    ECHO Deleting logs\...
    RMDIR /S /Q logs
  :logs

  GOTO win9x
:winnt

REM for Windows9x:
ECHO Deleting logs\...
DELtree /Y logs
:win9x

ECHO.
ECHO Done

CD build\msvc80
CALL clean.bat
CD ..\..
