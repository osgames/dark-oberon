

-- ============================
--  Dark Oberon - Setup Script
-- ============================

-- set assigns
sel ('/sys/servers/file')
call('SetAssign', "root",        "app:..")
call('SetAssign', "data",        "root:data")
call('SetAssign', "logs",        "root:logs")
call('SetAssign', "guis",        "root:guis")
call('SetAssign', "scripts",     "data:scripts")
call('SetAssign', "fonts",       "data:fonts")

call('SetAssign', "levels",      "root:levels")
call('SetAssign', "maps",        "root:maps")
call('SetAssign', "schemes",     "root:schemes")
call('SetAssign', "races",       "root:races")

call('SetAssign', "user",        "app:..")
call('SetAssign', "profiles",    "user:profiles")
call('SetAssign', "screenshots", "user:screenshots")


-- allow thunking
dofile(mangle('scripts:thunker.lua'))


-- create servers
new("ResourceServer", "/sys/servers/resource")
new("GlfwGfxServer", "/sys/servers/gfx")
new("GlfwTimeServer", "/sys/servers/time")
new("MessageServer", "/sys/servers/message")
new("GlfwInputServer", "/sys/servers/input")
new("SceneServer", "/sys/servers/scene")
new("SerializeServer", "/sys/servers/serialize")
new("OpenALAudioServer", "/sys/servers/audio")
new("WavePathServer", "/sys/servers/path")
new("PlayerRemoteServer", "/sys/servers/remote")

server = lookup("/sys/servers/serialize")
server:RegisterSerializer("XercesSerializer", "xml")
