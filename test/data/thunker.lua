-- ----------------------------------------
-- This metatable setup can be moved to C++
-- but I don't know if that would actually
-- improve performance.
-- ----------------------------------------

-- This resolves the relative path based on the index and returns a thunk
function relgrab( t, k )
	local path
	
	path = t:GetFullPath()
	path = path .. "/" .. k
	
	return lookup( path )
end


--  Try to find a script command that matches the key and return it, if that fails assume
--  the key is the name of a node and return a thunk (or nil if it turns out the node
--  doesn't exist).
function neb_index( t, k )
	if not t._class[k] then
--		logdebug( "relgrab: " .. tostring(t) .. ", " .. k )
		return relgrab( t, k )
	else
		return t._class[k]
	end
end


--  Build a metatable for neb thunks
_nebthunker = { __index = neb_index }

-- Create the default '/' NOH root thunk
kernel = lookup( '/' )


-- Remove zombie thunks from the _nebthunks table
function PruneNebThunks()

	local function KillZombies( t, dr )
		local zombies_count = 0
		
		-- find zombies
		for k, v in pairs( t ) do
			if iszombie( v ) then
				zombies_count = zombies_count + 1
				dr.n = dr.n + 1
				dr[dr.n] = k
				LogInfo( "Found eeevil zombie thunk " .. tostring(k) )
			end
		end
		
		-- kill zombies
		while ( zombies_count > 0 ) do
			rawset( t, dr[dr.n], nil )
			zombies_count = zombies_count - 1
			dr.n = dr.n - 1
		end
	end

	local death_row = { n = 0 }

	LogInfo( "Started kernel tree pruning" )
	KillZombies( _nebthunks, death_row )
	LogInfo( "Finished kernel tree pruning" )
end
