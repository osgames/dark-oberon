
new("Root", "/Name1");
p = lookup("/Name1");

pushcwd("/Name1");
	call("SetID", "Name2");
popcwd();

p:SetID("Name3");

kernel.Name3:SetID("Name4");

sel("Name4");
	call("SetID", "NewObjectFromScript");
sel("..");

loginfo("Info message");
logwarning("Warning message");
logerror("Error message");
logcritical("Critical message");
logdebug("Debug message");

--error("Abort from script");
