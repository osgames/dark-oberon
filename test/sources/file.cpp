
/**
	@file file.cpp

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"


//=========================================================================
// FileServer
//=========================================================================

void TestFileServer()
{
	FileServer *fs = FileServer::GetInstance();

	LogInfo("*** Testing file system... ***");

	LogInfo1("app: %s", fs->GetAssign(string("app")));
	LogInfo1("user: %s", fs->GetAssign(string("user")));

	// dir
	vector<string> files;
	size_t i;
	fs->ListDirectories(files, string("app:"));
	for (i = 0; i < files.size(); i++)
		LogInfo1("[%s]", files[i].c_str());
	fs->ListFiles(files, string("app:"));
	for (i = 0; i < files.size(); i++)
		LogInfo1("%s", files[i].c_str());
	
	fs->Release();

	LogInfo("Done");
}


// vim:ts=4:sw=4:
