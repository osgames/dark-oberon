
/**
	@file gfx.cpp

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/fileserver.h"
#include "kernel/serializeserver.h"
#include "framework/resourceserver.h"
#include "framework/sceneserver.h"
#include "glfwmodule/glfwkernelserver.h"
#include "glfwmodule/glfwgfxserver.h"
#include "glfwmodule/glfwtimeserver.h"

UseModule(Framework);
UseModule(GlfwModule);
UseModule(XercesModule);
UseModule(OpenGLModule);


//=========================================================================
// GfxServer, SceneServer
//=========================================================================

Scene *scene = NULL;


void DrawGfxScene()
{
	GfxServer *gs = GfxServer::GetInstance();
	SceneServer *ss = SceneServer::GetInstance();

	gs->ClearBuffers(GfxServer::BUFFER_COLOR);
	ss->RenderScene(scene);

	gs->SetColor(vector3(1, 1, 0));
	gs->Print(vector2(80, 40), string("Sample text to draw..."));

	gs->PresentScene();
}


void CallbackGfxSize(ushort_t width, ushort_t height, bool fullscreen)
{
	if (scene)
	{
		Viewport wp;
		wp.Set(0, 0, width, height);
		scene->SetViewport(wp);
	}

	DrawGfxScene();
}


void TestGfx()
{
	KernelServer::GetInstance()->RegisterModule(Framework);
	KernelServer::GetInstance()->RegisterModule(OpenGLModule);
	KernelServer::GetInstance()->RegisterModule(GlfwModule);
	KernelServer::GetInstance()->RegisterModule(XercesModule);

    FileServer *fs = FileServer::GetInstance();
	SerializeServer *ses = (SerializeServer *)KernelServer::GetInstance()->New("SerializeServer", NOH_SERVERS + "/serialize");
    ResourceServer *rs = (ResourceServer *)KernelServer::GetInstance()->New("ResourceServer", NOH_SERVERS + "/resource");
	GfxServer *gs = (GfxServer *)KernelServer::GetInstance()->New("GlfwGfxServer", NOH_SERVERS + "/gfx");
	SceneServer *ss = (SceneServer *)KernelServer::GetInstance()->New("SceneServer", NOH_SERVERS + "/scene");
	TimeServer *ts = (TimeServer *)KernelServer::GetInstance()->New("GlfwTimeServer", NOH_SERVERS + "/time");

	ses->RegisterSerializer(string("XercesSerializer"), string("xml"));

	LogInfo("*** Testing GFX... ***");

	// show all videomodes
	LogInfo("Video modes:");
	VideoMode modes[100];
	ushort_t count = gs->GetVideoModes(modes, 100, VideoMode::BPP_24);
	ushort_t i;

	for (i = 0; i < count; i++)
		LogInfo3("%ux%ux%d", modes[i].width, modes[i].height, VideoMode::BppToInt(modes[i].bpp));


	// open display
	DisplayMode mode;
	mode.SetFullscreen(false);
	mode.SetWindowSize(DisplayMode::WidowSize(450, 350));

	gs->SetDisplayMode(mode);
	gs->SetWindowSizeCallback(CallbackGfxSize);

	if (gs->OpenDisplay())
	{
        gs->SetWindowTitle(string("Gfx test"));
        gs->SetMinMagFilter(GfxServer::FILTER_LINEAR);
        gs->SetMipMapFilter(GfxServer::FILTER_LINEAR);
		//gs->SetCursorVisibility(GfxServer::CURSOR_HIDDEN);

        // create scene
		LogInfo("Creating scene");
		scene = ss->NewScene("IsoScene");

		SceneObject *object1 = ss->NewSceneObject("SceneObject", &string("app:data/model.xml"));
		object1->SetPosition(vector3(7, 0, 0));
		object1->SetOverlayColor(vector3(0.9f, 0.4f, 0.2f));
		scene->AddObject(object1);

		SceneObject *object2 = ss->NewSceneObject("SceneObject", &string("app:data/model.xml"));
		object2->SetPosition(vector3(14, 0, 0));
		object2->SetOverlayColor(vector3(0.2f, 0.8f, 0.0f));
		scene->AddObject(object2);

		SceneObject *object3 = ss->NewSceneObject("SceneObject", &string("app:data/model.xml"));
		object3->SetPosition(vector3(7, 7, 0));
		object3->SetOverlayColor(vector3(0.9f, 0.9f, 0.0f));
		scene->AddObject(object3);

		SceneObject *object4 = ss->NewSceneObject("SceneObject", &string("app:data/model.xml"));
		object4->SetPosition(vector3(14, 7, 0));
		object4->SetOverlayColor(vector3(0.2f, 0.0f, 0.9f));
		scene->AddObject(object4);

		// create font
		FontFile::CharSize char_size;
		char_size.width = char_size.height = 30;
		char_size.resolution_x = char_size.resolution_y = 96;

		Font *font = gs->NewFont(string("app:data/test.ttf"), char_size);
		font->SetShadow(2, 2, 128);

		// set projection
		Projection proj;
		proj.SetOrtho2D(0, 450, 50, 400);
		scene->SetProjection(proj);

		// set viewport
		Viewport wp;
		wp.Set(0, 0, 450, 350);
		scene->SetViewport(wp);

		// load resources
        LogInfo("Loading resources");
		if (!object1->LoadResources()
			|| !object2->LoadResources()
			|| !object3->LoadResources()
			|| !object4->LoadResources()
			|| !rs->LoadAllResources())
		{
			gs->CloseDisplay();
			return;
		}

        LogInfo2("Textures: %u, Size: %u", rs->GetResourcesCount(Resource::RES_TEXTURE), rs->GetResourcesSize(Resource::RES_TEXTURE));
		LogInfo2("Meshes: %u, Size: %u", rs->GetResourcesCount(Resource::RES_MESH), rs->GetResourcesSize(Resource::RES_MESH));
		LogInfo2("Models: %u, Size: %u", rs->GetResourcesCount(Resource::RES_MODEL), rs->GetResourcesSize(Resource::RES_MODEL));
		LogInfo2("Animations: %u, Size: %u", rs->GetResourcesCount(Resource::RES_ANIMATION), rs->GetResourcesSize(Resource::RES_ANIMATION));
		LogInfo2("Fonts: %u, Size: %u", rs->GetResourcesCount(Resource::RES_FONT), rs->GetResourcesSize(Resource::RES_FONT));

		gs->SetClearValues(0.0f, 0.4f, 0.0f);
		gs->SetBlending(true);

		ts->StartTime();
		ts->SetMinFrameTime(0.01);

		char title[100];
		/*
		bool toggled1 = false;
		bool toggled2 = false;
		*/

		while (gs->IsDisplayOpened())
		{
			/*
			if (ts->GetTime() > 1 && !toggled1)
			{
				toggled1 = true;
				gs->ToggleFullscreen();
			}
			if (ts->GetTime() > 8 && !toggled2)
			{
				toggled2 = true;
				gs->ToggleFullscreen();
			}*/

			DrawGfxScene();

			if (ts->IsFPSRefreshed())
			{
				sprintf(title, "Gfx test - FPS: %.2f", ts->GetFPS());
				gs->SetWindowTitle(string(title));
				//gs->SaveScreenshot(string("app:shot.tga"));
			}

			gs->Trigger();
			ts->Trigger();
		}

		object1->Release();
		object2->Release();
		object3->Release();
		object4->Release();
	}
	else
		LogError("Error opening GFX display");

	ts->Release();
	ss->Release();
    gs->Release();
    rs->Release();
    fs->Release();

	LogInfo("Done");
}


// vim:ts=4:sw=4:
