
/**
	@file test.cpp

 	@author Peter Knut
	@date 2005 - 2009

	Demo application for testing functionality of main servers.

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include <conio.h>

#include "glfwmodule/glfwkernelserver.h"
#include "kernel/fileserver.h"

extern void TestAudio();
extern void TestObjects();
extern void TestTime();
extern void TestFileServer();
extern void TestLua();
extern void TestGfx();
extern void TestInput();
extern void TestTga();
extern void TestIpcServer();
extern void TestIpcClient();
extern void TestSessionServer();
extern void TestSessionClient();


//=========================================================================
// main
//=========================================================================

int main(int argc, char *argv[])
{
	printf("1 - Test objects\n");
	printf("2 - Test time\n");
	printf("3 - Test FileServer\n");
	printf("4 - Test Lua\n");
	printf("5 - Test Gfx\n");
	printf("6 - Test input\n");
	printf("7 - Test audio\n");
	printf("8 - Test Tga\n");
	printf("9 - Test IPC - server\n");
	printf("0 - Test IPC - client\n");
	printf("a - Test sessions - server\n");
	printf("b - Test sessions - client\n");
	printf("\n");

	bool stop = false;

	while (!stop)
	{

#ifdef WINDOWS
		char ch = _getch();
#else
		char ch = getch();
#endif
		ch = toupper(ch);

		if ((ch < '0' || ch > '9') && (ch < 'A' || ch > 'B'))
			continue;

		NEW GlfwKernelServer();
		KernelServer::GetInstance()->ProcessArgs(argc, argv);

		KernelServer::GetInstance()->New("FileServer", NOH_SERVERS + "/file");
		LogServer::GetInstance()->RegisterFile(FileServer::GetStdOut(), LogServer::LL_ALL);

		switch (ch)
		{
		case '1': TestObjects(); break;
		case '2': TestTime(); break;
		case '3': TestFileServer(); break;
		case '4': TestLua(); break;
		case '5': TestGfx(); break;
		case '6': TestInput(); break;
		case '7': TestAudio(); break;
		case '8': TestTga(); break;
		case '9': TestIpcServer(); break;
		case '0': TestIpcClient(); break;
		case 'A': TestSessionServer(); break;
		case 'B': TestSessionClient(); break;
		default:  stop = false; break;
		}

		delete KernelServer::GetInstance();
		stop = true;
	}

	return 0;
}


// vim:ts=4:sw=4:
