
/**
	@file lua.cpp

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/kernelserver.h"
#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "kernel/scriptserver.h"
#include "kernel/serializeserver.h"

UseModule(LuaModule);


//=========================================================================
// LuaScriptLoader
//=========================================================================

void TestLua()
{
	KernelServer::GetInstance()->RegisterModule(LuaModule);

	FileServer *fs = FileServer::GetInstance();
	ScriptServer *ss = (ScriptServer *)KernelServer::GetInstance()->New("ScriptServer", NOH_SERVERS + "/script");

	ss->NewScriptLoader("LuaScriptLoader", "lua");

	LogInfo("*** Testing Lua scripts... ***");

	string result;
	Root *obj;

	if (ss->RunScript(string("app:data/setup.lua"), result)) {
		if (ss->RunScript(string("app:data/test.lua"), result)) {
			obj = KernelServer::GetInstance()->Lookup(string("/NewObjectFromScript"));
			if (obj)
				LogInfo(obj->GetID().c_str());
		}
	}

	ss->Release();

	LogInfo("Done");
}


// vim:ts=4:sw=4:
