
/**
	@file tga.cpp

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "formats/tgaimagefile.h"


//=========================================================================
// TgaImageFile
//=========================================================================

void TestTga()
{
	const int images_count = 4;
	char *images[images_count] = {
		"test8.tga",
		"test8rle.tga",
		"test32.tga",
		"test32rle.tga"
	};
	string file_path_r;
	string file_path_w;

	FileServer *fs = FileServer::GetInstance();

	TgaImageFile tga_r, tga_w;

	LogInfo("*** Testing tga... ***");

	for (int i = 0; i < images_count; i++) {
		file_path_r = string("app:data/") + images[i];
		file_path_w = string("app:data/_") + images[i];

		LogInfo1("Loading %s...", file_path_r.c_str());

		tga_r.SetFileName(file_path_r);
		if (tga_r.Open()) {
			if (!tga_r.LoadData())
				return;

			LogInfo1("Saving to %s...", file_path_w.c_str());

			tga_w.SetFileName(file_path_w);
			if (tga_w.Open(true)) {
				tga_w.CopyData(&tga_r);
				tga_w.SaveData();
				tga_w.Close();
			}
			tga_r.Close();
		}
	}

	LogInfo("Done");
}


// vim:ts=4:sw=4:
