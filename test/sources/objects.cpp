
/**
	@file objects.cpp

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/kernelserver.h"
#include "kernel/logserver.h"


//=========================================================================
// Object hierarchy
//=========================================================================

void TestObjects()
{
	LogInfo("*** Testing object hierarchy... ***");

	KernelServer::GetInstance()->New("Root", string("/hallo/how/are/you?"));
	
	Root *a, *b, *c, *d;

	a = KernelServer::GetInstance()->Lookup(string("/hallo"));
	LogInfo1("Name: %s", a->GetID().c_str());

	b = a->GetFront();
	LogInfo1("Name: %s", b->GetID().c_str());

	c = KernelServer::GetInstance()->Lookup(string("/hallo/how/are"));
	LogInfo1("Name: %s", c->GetID().c_str());

	c->AcquirePointer();
	a->Release();

	KernelServer::GetInstance()->PushCwd(c);
	d = KernelServer::GetInstance()->Lookup(string("you?"));
	LogInfo1("Name: %s", d->GetID().c_str());
	LogInfo1("Class: %s", d->GetClass()->GetName().c_str());
	KernelServer::GetInstance()->PopCwd();

	c->Release();

	LogInfo("Done");
}


// vim:ts=4:sw=4:
