
/**
	@file ipc.cpp

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "kernel/kernelserver.h"
#include "framework/sessionserver.h"
#include "framework/sessionclient.h"
#include "glfwmodule/glfwtimeserver.h"

UseModule(Framework);
UseModule(GlfwModule);
UseModule(OpenGLModule);


//=========================================================================
// Test sessions
//=========================================================================

void TestSessionServer()
{
	KernelServer::GetInstance()->RegisterModule(Framework);
	KernelServer::GetInstance()->RegisterModule(OpenGLModule);
	KernelServer::GetInstance()->RegisterModule(GlfwModule);
	
	LogInfo("*** Testing sussions - server... ***");

	TimeServer *ts = (TimeServer *)KernelServer::GetInstance()->New("GlfwTimeServer", NOH_SERVERS + "/time");
	SessionServer *ss = (SessionServer *)KernelServer::GetInstance()->New("SessionServer", NOH_SERVERS + "/session");

	ss->SetAppName("DarkOberonTest");
	ss->SetAppVersion("1.0");
	ss->SetMaxClientsCount(2);

	ss->Open();

	while (ss->Trigger()) {
		ts->Trigger();
		ThreadServer::GetInstance()->Sleep(0.2);
	}

	ss->Close();
	ss->Release();

	LogInfo("Done");
}


void TestSessionClient()
{
	KernelServer::GetInstance()->RegisterModule(Framework);
	KernelServer::GetInstance()->RegisterModule(OpenGLModule);
	KernelServer::GetInstance()->RegisterModule(GlfwModule);
	
	LogInfo("*** Testing sussions - client... ***");

	TimeServer *ts = (TimeServer *)KernelServer::GetInstance()->New("GlfwTimeServer", NOH_SERVERS + "/time");
	SessionClient *sc = (SessionClient *)KernelServer::GetInstance()->New("SessionClient", NOH_SERVERS + "/session");
	SessionServerContext *ssc;

	sc->SetAppName("DarkOberonTest");
	sc->SetAppVersion("1.0");

	sc->Open();

	while (sc->Trigger()) {
		if (sc->GetServersCount() && !sc->IsJoined() && !sc->IsJoinDenied()) {
			ssc = sc->GetServerAt(0);
			sc->JoinSession(ssc->GetSessionGuid());
		}

		if (sc->IsJoinDenied())
			break;

		ts->Trigger();
		ThreadServer::GetInstance()->Sleep(0.2);
	}

	sc->Close();
	sc->Release();

	LogInfo("Done");
}


// vim:ts=4:sw=4:
