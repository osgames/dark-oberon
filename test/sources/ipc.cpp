
/**
	@file ipc.cpp

 	@author Peter Knut
	@date 2007

	@version 1.0 - Initial.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/logserver.h"
#include "kernel/fileserver.h"
#include "kernel/ipcserver.h"
#include "kernel/kernelserver.h"


//=========================================================================
// MyIpcServer
//=========================================================================

class MyIpcServer : public IpcServer
{
//--- methods
public:
	MyIpcServer(const char *id) : IpcServer(id), running(true) {};

protected:
	virtual void ProcessData(uint_t channel_id, const IpcBuffer &buffer)
	{
		const char *message = buffer.GetFirstString();
		while (message)
		{
			ProcessMessage(channel_id, message);
			message = buffer.GetNextString();
		}
	}

	void ProcessMessage(uint_t channel_id, const char *message)
	{
		if (!strcmp(message, "~bye"))
		{
			IpcBuffer buff(1024);

			LogDebug("Received ~bye, sending ~bye-ok");
			buff.SetString("~bye-ok");
			this->Send(channel_id, buff);
		}
		else if (!strcmp(message, "~bye-ok"))
		{
			LogDebug("Received ~bye-ok, closing");
			this->running = false;
		}
		else
			LogDebug1("Received: %s", message);
	}


//--- variables
public:
	bool running;
};


PrepareClass(MyIpcServer, "IpcServer", s_NewMyIpcServer, s_InitMyIpcServer);


//=========================================================================
// Test IPC
//=========================================================================

void TestIpcServer()
{
	LogInfo("*** Testing IPC - server... ***");

	KernelServer::GetInstance()->RegisterClass("MyIpcServer", s_InitMyIpcServer, s_NewMyIpcServer);
	MyIpcServer *is = (MyIpcServer *)KernelServer::GetInstance()->New("MyIpcServer", string("sys/servers/ipc"));
	is->StartListening(9999, IpcChannel::PROTOCOL_TCP);
	is->StartReceiving();

	while (is->running)
		ThreadServer::GetInstance()->Sleep(0.2);

	is->Release();

	LogInfo("Done");
}


void TestIpcClient()
{
	IpcAddress address("self", 9999);
	IpcBuffer buff(1024);
	uint_t channel_id;

	LogInfo("*** Testing IPC - client");

	KernelServer::GetInstance()->RegisterClass("MyIpcServer", s_InitMyIpcServer, s_NewMyIpcServer);
	MyIpcServer *is = (MyIpcServer *)KernelServer::GetInstance()->New("MyIpcServer", string("sys/servers/ipc"));

	if ((channel_id = is->Connect(address, IpcChannel::PROTOCOL_TCP)))
	{
		LogDebug("Sending Message1");
		buff.SetString("Message1");
		is->Send(channel_id, buff);

		LogDebug("Sending Message2");
		buff.SetString("Message2");
		is->Send(channel_id, buff);

		LogDebug("Sending Message3");
		buff.SetString("Message3");
		is->Send(channel_id, buff);

		LogDebug("Sending ~bye");
		buff.SetString("~bye");
		is->Send(channel_id, buff);

		while (is->running)
		{
			is->Trigger();
			ThreadServer::GetInstance()->Sleep(0.2);
		}

		is->Disconnect(channel_id);
	}

	is->Release();

	LogInfo("Done");
}


// vim:ts=4:sw=4:
