
/**
	@file time.cpp

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "glfwmodule/glfwkernelserver.h"
#include "glfwmodule/glfwtimeserver.h"

UseModule(Framework);
UseModule(GlfwModule);
UseModule(OpenGLModule);


//=========================================================================
// TimeServer
//=========================================================================

void TestTime()
{
	KernelServer::GetInstance()->RegisterModule(Framework);
	KernelServer::GetInstance()->RegisterModule(OpenGLModule);
	KernelServer::GetInstance()->RegisterModule(GlfwModule);

	TimeServer *ts = (TimeServer *)KernelServer::GetInstance()->New("GlfwTimeServer", NOH_SERVERS + "/time");
	Profiler *prof = ts->NewProfiler("test_prof");

	LogInfo("*** Testing time... ***");

	ts->StartTime();
	ts->SetMinFrameTime(0.1);

	for (int i = 0; i < 20; i++) {
		LogInfo3("Time: %f, Frame: %f, FPS: %.2f", ts->GetTime(), ts->GetFrameTime(), ts->GetFPS());

		prof->Start();
		ThreadServer::GetInstance()->Sleep(0.03);	// simulate long working function
		prof->Stop();

		ts->Trigger();
		LogInfo2("Profiler: %.2f%%\tOthers: %.2f%%", prof->GetPercentage(), 100 - prof->GetPercentage());
	}

	ts->Release();

	LogInfo("Done");
}


// vim:ts=4:sw=4:
