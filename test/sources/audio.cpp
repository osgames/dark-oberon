
/**
	@file audio.cpp

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/fileserver.h"
#include "framework/resourceserver.h"
#include "glfwmodule/glfwkernelserver.h"
#include "glfwmodule/glfwtimeserver.h"
#include "openalmodule/openalaudioserver.h"

UseModule(Framework);
UseModule(GlfwModule);
UseModule(OpenALModule);
UseModule(OpenGLModule);


//=========================================================================
// OpenALAudioServer
//=========================================================================

void TestAudio()
{
	KernelServer::GetInstance()->RegisterModule(Framework);
	KernelServer::GetInstance()->RegisterModule(OpenALModule);
	KernelServer::GetInstance()->RegisterModule(OpenGLModule);
	KernelServer::GetInstance()->RegisterModule(GlfwModule);

	TimeServer *ts = (TimeServer *)KernelServer::GetInstance()->New("GlfwTimeServer", NOH_SERVERS + "/time");
	FileServer *fs = FileServer::GetInstance();
	ResourceServer *rs = (ResourceServer *)KernelServer::GetInstance()->New("ResourceServer", NOH_SERVERS + "/resource");
	OpenALAudioServer *as = (OpenALAudioServer *)KernelServer::GetInstance()->New("OpenALAudioServer", NOH_SERVERS + "/audio");

	LogInfo("*** Testing audio... ***");

	as->SetDeviceName("Generic Hardware");

	if (as->OpenDevice()) {
		// audio
		as->SetDistanceModel(AudioServer::DM_INVERSE_CLAMPED);

		// music
		Sound *snd1 = as->NewSound(string("app:data/test.ogg"));
		snd1->SetVolume(0.4f);
		snd1->SetLooping(true);
		snd1->SetStreaming(true);
		snd1->SetCategory(Sound::SND_MUSIC);

		// effect
		matrix44 transform;
		transform.translate(vector3(0, 0, -1));

		Sound *snd2 = as->NewSound(string("app:data/test2.ogg"));
		snd2->SetVolume(1.0f);
		snd2->SetCategory(Sound::SND_EFFECT);
		snd2->SetLooping(true);
		snd2->SetTransform(transform);

		LogInfo("Loading sounds...");
		rs->LoadAllResources();

		LogInfo2("Sounds: %u, Size: %u", rs->GetResourcesCount(Resource::RES_SOUND_RESOURCE), rs->GetResourcesSize(Resource::RES_SOUND_RESOURCE));

		LogInfo("Playing...");
		as->PlaySound(snd1);
		as->PlaySound(snd2);

		ts->SetMinFrameTime(0.2); // 50 fps
		ts->StartTime();
		int count = 0;

		// main loop
		while (ts->GetTime() < 10) {
			transform.rotate_y((float)ts->GetFrameTime());
		
			snd2->SetTransform(transform);

			ts->Trigger();
			as->Trigger();
		}

		// close and release all
		as->CloseDevice();
	}

	as->Release();
	rs->Release();
	fs->Release();
	ts->Release();

	LogInfo("Done");
}


// vim:ts=4:sw=4:
