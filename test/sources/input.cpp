
/**
	@file input.cpp

 	@author Peter Knut
	@date 2005 - 2007

	@version 1.0 - Initial.
	@version 1.1 - Testing functions are separated into different files.
*/


//=========================================================================
// Included files and modules
//=========================================================================

#include "kernel/fileserver.h"
#include "glfwmodule/glfwkernelserver.h"
#include "glfwmodule/glfwgfxserver.h"
#include "glfwmodule/glfwinputserver.h"

UseModule(Framework);
UseModule(GlfwModule);
UseModule(OpenGLModule);


//=========================================================================
// InputServer
//=========================================================================

void DrawInputScene()
{
	GfxServer *gs = GfxServer::GetInstance();

	glClear(GL_COLOR_BUFFER_BIT);
	glBegin(GL_QUADS);
		glColor3f(1, 0, 0);
		glVertex2d(0, 0);
		glColor3f(0, 1, 0);
		glVertex2d(1, 0);
		glColor3f(0, 0, 1);
		glVertex2d(1, 1);
		glColor3f(1, 1, 0);
		glVertex2d(0, 1);
	glEnd();

	gs->PresentScene();
}


void CallbackInputSize(ushort_t width, ushort_t height, bool fullscreen)
{
	GfxServer *gs = GfxServer::GetInstance();
	Viewport wp;

	wp.Set(0, 0, width, height);

	DrawInputScene();
}


bool ActionCallback(const string &action, int pos, void *data)
{
	DisplayMode::WidowSize size = GfxServer::GetInstance()->GetWindowSize();

	if (action == "left")
		if (size.width > 50) size.width -= 1;
	if (action == "right")
		size.width += 1;
	if (action == "up")
		if (size.height > 50) size.height -= 1;
	if (action == "down")
		size.height += 1;
	if (action == "reset")
		size.width = size.height = 200;
	if (action == "both")
	{
		if (size.width > 50) size.width -= pos * 10;
		if (size.height > 50) size.height -= pos * 10;
	}

	GfxServer::GetInstance()->SetWindowSize(size);

	return true;
}


bool KeyCallback(KeyCode key, KeyState state)
{
	if (state == KS_DOWN) {
		LogInfo1("Key: %s", InputServer::GetInstance()->GetKeyName(key));

		if (key == KEY_ESC)
			GfxServer::GetInstance()->CloseDisplay();
		return true;
	}

	return false;
}


bool MousePosCallback(ushort_t x, ushort_t y)
{
	if (InputServer::GetInstance()->IsKeyPressed(MOUSE_BUTTON_LEFT)) {
		int ax;
		int ay;
		InputServer::GetInstance()->GetAbsMousePosition(ax, ay);
		LogInfo4("Mouse: %u, %u  \tAbsMouse: %d, %d", x, y, ax, ay);
	}

	return true;
}


bool MouseRelPosCallback(int x, int y)
{
	if (InputServer::GetInstance()->IsKeyPressed(MOUSE_BUTTON_RIGHT)) {
		int ax;
		int ay;
		InputServer::GetInstance()->GetAbsMousePosition(ax, ay);
		LogInfo4("RelMouse: %d, %d  \tAbsMouse: %d, %d", x, y, ax, ay);
	}

	return true;
}


bool MouseRelWheelCallback(int pos)
{
	int wpos = InputServer::GetInstance()->GetMouseWheel();
	LogInfo2("Wheel: %d    \tRelWheel: %d", wpos, pos);

	return true;
}


void TestInput()
{
	KernelServer::GetInstance()->RegisterModule(Framework);
	KernelServer::GetInstance()->RegisterModule(OpenGLModule);
	KernelServer::GetInstance()->RegisterModule(GlfwModule);

	FileServer *fs = FileServer::GetInstance();
	KernelServer::GetInstance()->New("ResourceServer", NOH_SERVERS + "/resource");
	GfxServer *gs = (GfxServer *)KernelServer::GetInstance()->New("GlfwGfxServer", NOH_SERVERS + "/gfx");
	InputServer *is = (InputServer *)KernelServer::GetInstance()->New("GlfwInputServer", NOH_SERVERS + "/input");

	LogInfo("*** Testing input... ***");

	// set binding
	is->Bind("right:pressed", "right");
	is->Bind("left:pressed", "left");
	is->Bind("up:pressed", "up");
	is->Bind("down:pressed", "test");
	is->Bind("down:pressed", NULL);           // unbind
	is->Bind("down:pressed", "test");
	is->Bind("down:pressed", "down");         // rebind
	is->Bind("space:down", "reset");
	is->Bind("relwheel", "both");

	// set callbacks
	is->AddActionCallback(ActionCallback);
	is->SetKeyCallback(KeyCallback);
	is->SetMousePosCallback(MousePosCallback);
	is->SetMouseRelPosCallback(MouseRelPosCallback);
	is->SetMouseRelWheelCallback(MouseRelWheelCallback);

	// open display
	DisplayMode mode;
	mode.SetFullscreen(false);
	mode.SetWindowSize(DisplayMode::WidowSize(200, 200));

	gs->SetDisplayMode(mode);
	gs->SetWindowSizeCallback(CallbackInputSize);
	gs->OpenDisplay();
	gs->SetWindowTitle(string("Input test"));
	gs->SetCursorVisibility(GlfwGfxServer::CURSOR_HIDDEN);

	glClearColor(0, 0, 0, 0);
	glTranslatef(-0.5, -0.5, 0.0);

	while (gs->IsDisplayOpened()) {
		DrawInputScene();

		is->Trigger();
		gs->Trigger();
	}

	LogInfo("Done");
}


// vim:ts=4:sw=4:
