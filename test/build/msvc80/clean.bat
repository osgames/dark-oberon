@ECHO OFF

REM ==============================================================
REM  This helps you clean your Test folder from unneeded
REM  MS Visual C++ 7.1 intermediate files.
REM ==============================================================

ECHO ----------------------------------------
ECHO MSVC71 - Deleting intermediate files...
ECHO.

IF NOT EXIST ..\..\test-debug.exe GOTO debugexe
  ECHO Deleting test-debug.exe...
  DEL ..\..\test-debug.exe
:debugexe

IF NOT EXIST ..\..\test-release.exe GOTO releaseexe
  ECHO Deleting test-release...
  DEL ..\..\test-release.exe
:releaseexe

IF NOT EXIST ..\..\test-debug.ilk GOTO debugilk
  ECHO Deleting test-debug.ilk...
  DEL ..\..\test-debug.ilk
:debugilk

IF NOT EXIST ..\..\test-release.ilk GOTO releaseilk
  ECHO Deleting test-release.ilk...
  DEL ..\..\test-release.ilk
:releaseilk

IF NOT EXIST test.ncb GOTO ncb
  ECHO Deleting test.ncb...
  DEL test.ncb
:ncb

IF NOT EXIST test.plg GOTO plg
  ECHO Deleting test.plg...
  DEL test.plg
:plg

IF NOT EXIST test.bsc GOTO bsc
  ECHO Deleting test.bsc...
  DEL test.bsc
:bsc

DEL *.user

REM for WindowsNT, XP, 2k:
IF NOT "%OS%" == "Windows_NT" GOTO winnt

  IF NOT EXIST obj GOTO obj
    ECHO Deleting obj\...
    RMDIR /S /Q obj
  :obj

  IF NOT EXIST lib GOTO lib
    ECHO Deleting lib\...
    RMDIR /S /Q lib
  :lib

  GOTO win9x
:winnt

REM for Windows9x:
ECHO Deleting obj\...
DELtree /Y obj

ECHO Deleting lib\...
DELtree /Y lib
:win9x

ECHO.
ECHO MSVC71 - Done
