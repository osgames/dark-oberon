@ECHO OFF

REM ==============================================================
REM  This helps you clean your Test folder from unneeded
REM  intermediate files.
REM ==============================================================

ECHO ----------------------------------------
ECHO Deleting intermedate files...
ECHO.


CD build\msvc80
CALL clean.bat
CD ..\..

CD data
DEL _*.tga
CD ..\..
